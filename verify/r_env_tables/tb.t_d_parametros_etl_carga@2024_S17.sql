-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg
DO $$
DECLARE
    pmp_xlsx_actual_entries integer;
    pmp_xlsx_expected_entries integer;
BEGIN
	pmp_xlsx_expected_entries = 1;
    pmp_xlsx_actual_entries := (select count(*) from t_d_parametros_etl_carga tdpec where nombre_etl like 'ETL VLCi CDMGE PMP' and patron_fichero_origen like 'PMP_Resumen_Uds_Administrativas_*.xlsx' and filename like 'PMP_Resumen_Uds_Administrativas_yyyy.xlsx' and formato like 'xlsx');
    
    ASSERT pmp_xlsx_actual_entries = pmp_xlsx_expected_entries;
END $$;