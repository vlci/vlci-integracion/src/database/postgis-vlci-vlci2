-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg

BEGIN;

select 1/count(*) from vlci2.t_d_parametros_etl_carga where nombre_etl='py_calidadambiental_sonometros_zas_woo';
select 1/count(*) from vlci2.t_d_parametros_etl_carga where nombre_etl='py_calidadambiental_sonometros_zas_MAN';
select 1/count(*) from vlci2.t_d_parametros_etl_carga where nombre_etl='py_calidadambiental_sonometros_zas_SER';
select 1/count(*) from vlci2.t_d_parametros_etl_carga where nombre_etl='py_calidadambiental_sonometros_zas_HON';
select 1/count(*) from vlci2.t_d_parametros_etl_carga where nombre_etl='py_calidadambiental_sonometros_zas_CED';
select 1/count(*) from vlci2.t_d_parametros_etl_carga where nombre_etl='py_calidadambiental_sonometros_zas_PIN';
select 1/count(*) from vlci2.t_d_parametros_etl_carga where nombre_etl='py_calidadambiental_sonometros_zas_xlsx';
select 1/count(*) from vlci2.t_d_parametros_etl_carga where nombre_etl='py_calidadambiental_sonometros_zas_xls';

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) >= 109) THEN 1
        ELSE 0
    END as outvalue
from vlci2.t_d_parametros_etl_carga
) verify;

ROLLBACK;