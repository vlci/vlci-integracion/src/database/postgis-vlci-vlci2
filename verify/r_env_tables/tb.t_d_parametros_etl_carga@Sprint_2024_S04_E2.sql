-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg
DO $$
DECLARE
	current_db varchar(200);
	current_user varchar(200);
BEGIN
	SELECT current_database() INTO current_db;

	SELECT CURRENT_USER usr INTO current_user;

    IF (current_user = 'postgres') THEN
        PERFORM 1/count(*) FROM vlci2.t_d_parametros_etl_carga WHERE nombre_etl='VLCi_CDMGE_CONTRATACION' AND ruta_origen = '//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/contratacion/INT';
    ELSIF (current_db = 'sc_vlci_int') THEN
        PERFORM 1/count(*) FROM vlci2.t_d_parametros_etl_carga WHERE nombre_etl='VLCi_CDMGE_CONTRATACION' AND ruta_origen = '//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/contratacion/INT';    
    ELSIF (current_db = 'sc_vlci_pre') THEN
        PERFORM 1/count(*) FROM vlci2.t_d_parametros_etl_carga WHERE nombre_etl='VLCi_CDMGE_CONTRATACION' AND ruta_origen = '//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/contratacion/PRE';
    ELSE
        PERFORM 1/count(*) FROM vlci2.t_d_parametros_etl_carga WHERE nombre_etl='VLCi_CDMGE_CONTRATACION' AND ruta_origen = '//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/contratacion/PRO';
    END IF;

ROLLBACK;
END $$;
