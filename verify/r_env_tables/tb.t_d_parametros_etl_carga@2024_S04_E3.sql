-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg
DO $$
DECLARE
    file_extensions integer;
    file_extensions_expected integer;
BEGIN
	file_extensions_expected = 12;
    file_extensions := (SELECT COUNT(*) FROM vlci2.t_d_parametros_etl_carga where nombre_etl like 'py_calidadambiental_sonometros_zas%');
    ASSERT file_extensions = file_extensions_expected;
END $$;
