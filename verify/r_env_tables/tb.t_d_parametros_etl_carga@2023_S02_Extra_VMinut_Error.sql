-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg

BEGIN;

select 1/count(*) from vlci2.t_d_parametros_etl_carga where nombre_etl='VLCi_CDMGE_CONTRATACION';
select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) >= 99) THEN 1
        ELSE 0
    END as outvalue
from vlci2.t_d_parametros_etl_carga
) verify;

ROLLBACK;
