-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg

BEGIN;

SELECT 1 / COUNT(*)
FROM vlci2.t_d_parametros_etl_carga
WHERE nombre_extraccion like '%ISTIC12';
  AND query_extraccion LIKE 'Indicador ISTIC12','SELECT ta.autoridad ||'';''|| count(*) ||'';''|| TO_CHAR(fecha_acceso,''YYYY-MM-DD'') AS Num_Accesos FROM acceso_sede acs, sede_electro.tipo_acceso ta WHERE acs.id_tipo_acceso = ta.id_tipo_acceso  AND TO_CHAR(acs.FECHA_ACCESO, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') - 1, ''YYYY-MM-DD'') AND (acs.id_tipo_acceso is not null) AND acs.id_resultado_login = 1 GROUP BY ta.autoridad, TO_CHAR(fecha_acceso,''YYYY-MM-DD'') ORDER BY Num_accesos DESC','Autoridad,Num_Accesos,Fecha';

ROLLBACK;
