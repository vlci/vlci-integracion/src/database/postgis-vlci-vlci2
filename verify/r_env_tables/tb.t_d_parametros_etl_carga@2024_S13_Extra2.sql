-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg
DO $$
DECLARE
    zas_entries_old integer;
    zas_entries_new integer;
    zas_entries_expected integer;
BEGIN
	zas_entries_expected = 12;
    zas_entries_old := (select count(*) from t_d_parametros_etl_carga tdpec where nombre_etl like 'py_calidadambiental_sonometros_zas%' and ruta_destino like '%VLCI_ETL_SONOMETROS_ZAS%');
    zas_entries_new := (select count(*) from t_d_parametros_etl_carga tdpec where nombre_etl like 'py_calidadambiental_sonometros_zas%' and ruta_destino like '%VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS%');
    ASSERT zas_entries_new = zas_entries_expected;
    ASSERT zas_entries_old = 0;
END $$;
DO $$
DECLARE
    v_count INTEGER;
BEGIN
    SELECT COUNT(*)
    INTO v_count
    FROM vlci2.t_d_parametros_etl_carga
    WHERE nombre_etl IN (
        'VLCI_ETL_OCI_IND_REPORT_AIRWAVE_WIFI_AP',
        'VLCI_ETL_OCI_IND_REPORT_AIRWAVE_WIFI_CLIENTS',
        'VLCI_ETL_OCI_IND_REPORT_WIFI_DATOS_SESION_CLIENTE_AVC',
        'VLCI_ETL_OCI_IND_REPORT_WIFI_DATOS_SESION_CLIENTE_W4EU'
    );

    IF v_count > 0 THEN
        RAISE EXCEPTION 'Error: Existen % registros con los valores especificados en nombre_etl', v_count;
    END IF;
END $$;
