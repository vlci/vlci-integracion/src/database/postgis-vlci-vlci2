-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg
DO $$ 
DECLARE
    current_db varchar(200);
    current_user varchar(200);
BEGIN
    SELECT current_database()
    INTO current_db;

    SELECT CURRENT_USER
    INTO current_user;

    IF (current_user = 'postgres') THEN
        PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'py_personal_ayuntamiento'
            AND tipo = 'FTP'
            AND sistema_origen = 'FTP Sertic'
            AND patron_fichero_origen = '*.csv'
            AND ruta_origen = '//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal'
            AND ruta_destino = '/integracion/01.fichOrig/VLCI_PERSONAL_AYUNTAMIENTO/'
            AND ruta_backup = '/integracion/02.fichBackup/VLCI_PERSONAL_AYUNTAMIENTO/'
            AND filename = '*.CSV'
            AND borrar_origen = 'N'
            AND formato = 'CSV'
            AND obligatorio = 'N'
            AND destino = 'sftp'
            AND aud_user_ins = 'Ticket 3225');
    ELSIF (current_db = 'sc_vlci_int') THEN
        PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'py_personal_ayuntamiento'
            AND tipo = 'FTP'
            AND sistema_origen = 'FTP Sertic'
            AND patron_fichero_origen = '*.csv'
            AND ruta_origen = '//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal'
            AND ruta_destino = '/integracion/01.fichOrig/VLCI_PERSONAL_AYUNTAMIENTO/'
            AND ruta_backup = '/integracion/02.fichBackup/VLCI_PERSONAL_AYUNTAMIENTO/'
            AND filename = '*.CSV'
            AND borrar_origen = 'N'
            AND formato = 'CSV'
            AND obligatorio = 'N'
            AND destino = 'sftp'
            AND aud_user_ins = 'Ticket 3225');

    ELSIF (current_db = 'sc_vlci_pre') THEN
        PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'py_personal_ayuntamiento'
            AND tipo = 'FTP'
            AND sistema_origen = 'FTP Sertic'
            AND patron_fichero_origen = '*.csv'
            AND ruta_origen = '//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal'
            AND ruta_destino = '/pre/01.fichOrig/VLCI_PERSONAL_AYUNTAMIENTO/'
            AND ruta_backup = '/pre/02.fichBackup/VLCI_PERSONAL_AYUNTAMIENTO/'
            AND filename = '*.CSV'
            AND borrar_origen = 'N'
            AND formato = 'CSV'
            AND obligatorio = 'N'
            AND destino = 'sftp'
            AND aud_user_ins = 'Ticket 3225');
    ELSE
        PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'py_personal_ayuntamiento'
            AND tipo = 'FTP'
            AND sistema_origen = 'FTP Sertic'
            AND patron_fichero_origen = '*.csv'
            AND ruta_origen = '//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal'
            AND ruta_destino = '/prod/01.fichOrig/VLCI_PERSONAL_AYUNTAMIENTO/'
            AND ruta_backup = '/prod/02.fichBackup/VLCI_PERSONAL_AYUNTAMIENTO/'
            AND filename = '*.CSV'
            AND borrar_origen = 'S'
            AND formato = 'CSV'
            AND obligatorio = 'N'
            AND destino = 'sftp'
            AND aud_user_ins = 'Ticket 3225');
    END IF;
END $$ LANGUAGE plpgsql;