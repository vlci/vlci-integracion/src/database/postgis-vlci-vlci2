-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg
DO $$
DECLARE
    zas_entries_old integer;
    zas_entries_new integer;
    zas_entries_expected integer;
BEGIN
	zas_entries_expected = 12;
    zas_entries_old := (select count(*) from t_d_parametros_etl_carga tdpec where nombre_etl like 'py_calidadambiental_sonometros_zas%' and ruta_destino like '%VLCI_ETL_SONOMETROS_ZAS%');
    zas_entries_new := (select count(*) from t_d_parametros_etl_carga tdpec where nombre_etl like 'py_calidadambiental_sonometros_zas%' and ruta_destino like '%VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS%');
    ASSERT zas_entries_new = zas_entries_expected;
    ASSERT zas_entries_old = 0;
END $$;
DO $$
DECLARE
    v_count INTEGER;
BEGIN
    SELECT COUNT(*)
    INTO v_count
    FROM vlci2.t_d_parametros_etl_carga
    WHERE nombre_etl IN (
        'VLCI_ETL_OCI_IND_REPORT_AIRWAVE_WIFI_AP',
        'VLCI_ETL_OCI_IND_REPORT_AIRWAVE_WIFI_CLIENTS',
        'VLCI_ETL_OCI_IND_REPORT_WIFI_DATOS_SESION_CLIENTE_AVC',
        'VLCI_ETL_OCI_IND_REPORT_WIFI_DATOS_SESION_CLIENTE_W4EU'
    );

    IF v_count > 0 THEN
        RAISE EXCEPTION 'Error: Existen % registros con los valores especificados en nombre_etl', v_count;
    END IF;

END $$;

DO $$
DECLARE
    current_value TEXT;
BEGIN
    -- Retrieve the current value of query_extraccion
    SELECT query_extraccion
    INTO current_value
    FROM vlci2.t_d_parametros_etl_carga
    WHERE nombre_etl = 'py_trafico_intensidad_validada';

    -- Check if the current value is not equal
    IF current_value IS DISTINCT FROM 
			'SELECT concat(''Kpi-Accesos-Ciudad-Coches'', '';'', actual.idata, '';'', descripciones.descripcion, '';'', descripciones.descripcion_corta, '';'', descripciones.nombre_corto, '';'', actual.int_actual, '';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A373'',''A406'',''A72'',''A59'',''A1'',''A82'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and (vid.DiaAnomalo !=0 or vid.DiaAnomalo is NULL)
			group by afo.Descripcion,afo.idATA,vid.DÍA) actual
			inner join (
    			select 4 as idATA, ''Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)'' as descripcion, ''Accés Barcelona (entrada i eixida)'' as descripcion_corta, ''Accés Barcelona'' as nombre_corto union
    			select 57 as idATA, ''Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)''  as descripcion, ''Accés a Arxiduc Carles pel Camí nou de Picanya'' as descripcion_corta, ''Arxiduc Carles'' as nombre_corto union
    			select 70 as idATA, ''Av. del Cid (Entre V-30 i Tres Creus)''  as descripcion, ''Av. del Cid'' as descripcion_corta, ''Av. del Cid'' as nombre_corto union
    			select 78 as idATA, ''Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)''  as descripcion, ''Corts Valencianes (Accés per CV-35)'' as descripcion_corta, ''Corts Valencianes'' as nombre_corto union
    			select 349 as idATA, ''Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)''      as descripcion, ''Accés per V-31 (Pista de Silla)'' as descripcion_corta, ''Pista de Silla'' as nombre_corto union
    			select 379 as idATA, ''Prolongació Joan XXIII (Entre Germans Machado i Salvador Cerveró)''  as descripcion, ''Prolongació Joan XXIII'' as descripcion_corta, ''Joan XXIII'' as nombre_corto) as descripciones
    			on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Accesos-Vias-Coches'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion_corta,'';'', descripciones.nombre_corto,'';'', actual.int_actual,'';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A116'',''A121'',''A97'',''A117'',''A187'',''A51'') 
			and vid.DÍA = CAST(''#fecha#'' as datetime)   
			and (vid.DiaAnomalo !=0 or vid.DiaAnomalo is NULL)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 50 as idATA, ''Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)''  as descripcion, ''Av. Blasco Ibáñez'' as descripcion_corta, ''Blasco Ibáñez'' as nombre_corto union
				select 93 as idATA, ''Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)''  as descripcion, ''Av. Dr. Peset Aleixandre'' as descripcion_corta, ''Dr. Peset Aleixandre'' as nombre_corto union
				select 111 as idATA, ''Av. de Giorgeta (Entre Sant Vicent i Jesús)''  as descripcion, ''Av. de Giorgeta'' as descripcion_corta, ''Giorgeta'' as nombre_corto union
				select 112 as idATA, ''Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)''  as descripcion, ''Gran Via de Ferran el Catòlic'' as descripcion_corta, ''Ferran el Catòlic'' as nombre_corto union
				select 116 as idATA, ''Gran Via del Marqués del Túria (Entre Pont d''''Aragó i Hernan Cortés)''  as descripcion, ''Gran Via del Marqués del Túria'' as descripcion_corta, ''Marqués del Túria'' as nombre_corto union
				select 177 as idATA, ''Av. de Peris i Valero (Entre Ausiàs March i Sapadors)''  as descripcion, ''Av. de Peris i Valero'' as descripcion_corta, ''Peris i Valero'' as nombre_corto) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Accesos-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			FROM
			(select afo.Descripcion as descripcion,afo.idATA as idata,  sum(vid.IMD*afo.Coeficiente/100) as intactual,vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''F36'',''F37'',''F38'',''F39'',''F40'',''F41'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and (vid.DiaAnomalo !=0 or vid.DiaAnomalo is NULL)	
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 725 as idATA, ''Navarro Reverter'' as descripcion union
				select 726 as idATA, ''Pont de fusta'' as descripcion union
				select 727 as idATA, ''Pont de les arts'' as descripcion union
				select 728 as idATA, ''Pont del real'' as descripcion union
				select 729 as idATA, ''Carrer Alacant'' as descripcion union
				select 730 as idATA, ''Carrer Russafa'' as descripcion) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			from
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as intactual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo on 	vid.IdTA=afo.IdTA
			where afo.nombre in (''F31'',''F32'',''F33'',''F34'',''F35'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and (vid.DiaAnomalo !=0 or vid.DiaAnomalo is NULL)	
			group by 	afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join(
				select 720 as idATA, ''Carrer Colon'' as descripcion union
				select 721 as idATA, ''Comte de Trénor – Pont de fusta''  as descripcion union
				select 722 as idATA, ''Guillem de Castro''  as descripcion union
				select 723 as idATA, ''Xàtiva''  as descripcion union
				select 724 as idATA, ''Plaça Tetuan''  	as descripcion) as descripciones
			on actual.IdATA=descripciones.idATA
			'
             THEN
        RAISE EXCEPTION 'The value of query_extraccion is not equal for nombre_etl = ''py_trafico_intensidad_validada''. Current value: %', current_value;
    END IF;
END $$;

ROLLBACK;