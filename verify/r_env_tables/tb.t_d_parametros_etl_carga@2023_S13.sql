-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg
DO $$
DECLARE
    zas_entries_old integer;
    zas_entries_new integer;
    zas_entries_expected integer;
BEGIN
	zas_entries_expected = 12;
    zas_entries_old := (select count(*) from t_d_parametros_etl_carga tdpec where nombre_etl like 'py_calidadambiental_sonometros_zas%' and ruta_destino like '%VLCI_ETL_SONOMETROS_ZAS%');
    zas_entries_new := (select count(*) from t_d_parametros_etl_carga tdpec where nombre_etl like 'py_calidadambiental_sonometros_zas%' and ruta_destino like '%VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS%');
    ASSERT zas_entries_new = zas_entries_expected;
    ASSERT zas_entries_old = 0;
END $$;
