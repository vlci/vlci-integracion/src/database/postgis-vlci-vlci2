-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg
DO $$ 
DECLARE
    current_db varchar(200);
    current_user varchar(200);
BEGIN
    SELECT current_database()
    INTO current_db;

    SELECT CURRENT_USER
    INTO current_user;

    IF (current_user = 'postgres') THEN
        PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'py_trafico_intensidad_validada'
            AND query_extraccion like '%and afo.idATA NOT IN (%');
    ELSIF (current_db = 'sc_vlci_int') THEN
        PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'py_trafico_intensidad_validada'
            AND query_extraccion like '%and afo.idATA NOT IN (%');
    ELSIF (current_db = 'sc_vlci_pre') THEN
        PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'py_trafico_intensidad_validada'
            AND query_extraccion like '%and afo.idATA NOT IN (%');
    ELSE
         PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'py_trafico_intensidad_validada'
            AND query_extraccion like '%and afo.idATA NOT IN (%');
    END IF;
END $$ LANGUAGE plpgsql;