-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg
DO $$
DECLARE
	current_db varchar(200);
	current_user varchar(200);
BEGIN
	SELECT current_database() INTO current_db;
	SELECT CURRENT_USER usr INTO current_user;

    IF (current_user = 'postgres') THEN
        PERFORM 1/count(*) FROM vlci2.t_d_parametros_etl_carga WHERE nombre_etl='py_personal_ayuntamiento' AND patron_fichero_origen = '*.CSV' AND borrar_origen = 'S' AND ruta_origen = '//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal/PRE';
    ELSIF (current_db = 'sc_vlci_int') THEN
        PERFORM 1/count(*) FROM vlci2.t_d_parametros_etl_carga WHERE nombre_etl='py_personal_ayuntamiento' AND patron_fichero_origen = '*.CSV' AND borrar_origen = 'S' AND ruta_origen = '//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal/PRE';    
    ELSIF (current_db = 'sc_vlci_pre') THEN
        PERFORM 1/count(*) FROM vlci2.t_d_parametros_etl_carga WHERE nombre_etl='py_personal_ayuntamiento' AND patron_fichero_origen = '*.CSV' AND borrar_origen = 'S' AND ruta_origen = '//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal/PRE';
    ELSE
        PERFORM 1/count(*) FROM vlci2.t_d_parametros_etl_carga WHERE nombre_etl='py_personal_ayuntamiento' AND patron_fichero_origen = '*.CSV' AND borrar_origen = 'N' AND ruta_origen = '//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal';
    END IF;

END $$ LANGUAGE plpgsql;