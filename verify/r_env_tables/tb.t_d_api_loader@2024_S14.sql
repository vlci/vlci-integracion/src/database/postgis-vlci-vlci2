-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_api_loader.sql on pg
-- tarea PROY2100017N3-832
-- Comprueba que la cantidad de entradas coincide con la cantidad de api endpoints esperados
DO $$
DECLARE
    endpoints integer;
    endpoints_esperadas integer;
BEGIN
   endpoints_esperadas := 23;
   endpoints := (SELECT COUNT(*) FROM vlci2.t_d_api_loader);
   ASSERT endpoints = endpoints_esperadas;
END $$;
