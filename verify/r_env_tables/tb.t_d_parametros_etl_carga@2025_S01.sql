-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros_etl_carga.sql on pg
DO $$ 
DECLARE
    current_db varchar(200);
    current_user varchar(200);
BEGIN
    SELECT current_database()
    INTO current_db;

    SELECT CURRENT_USER
    INTO current_user;

    IF (current_user = 'postgres') THEN
        PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'ETL_VLCi_CDMGE_PMP_ENTIDAD'
            AND tipo = 'FTP'
            AND sistema_origen = 'FTP Sertic'
            AND patron_fichero_origen = 'Evolucion_PMP.xlsx'
            AND ruta_origen = '//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/Integradores'
            AND ruta_destino = '/integracion/01.fichOrig/VLCi_CDMGE_PMP_ENTIDAD/'
            AND ruta_backup = '/integracion/02.fichBackup/VLCi_CDMGE_PMP_ENTIDAD/'
            AND filename = 'Evolucion_PMP.xlsx'
            AND borrar_origen = 'N'
            AND formato = 'xlsx'
            AND obligatorio = 'N'
            AND destino = 'sftp');
    ELSIF (current_db = 'sc_vlci_int') THEN
        PERFORM 1 / (SELECT COUNT(*) 
               FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'ETL_VLCi_CDMGE_PMP_ENTIDAD'
            AND tipo = 'FTP'
            AND sistema_origen = 'FTP Sertic'
            AND patron_fichero_origen = 'Evolucion_PMP.xlsx'
            AND ruta_origen = '//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/Integradores'
            AND ruta_destino = '/integracion/01.fichOrig/VLCi_CDMGE_PMP_ENTIDAD/'
            AND ruta_backup = '/integracion/02.fichBackup/VLCi_CDMGE_PMP_ENTIDAD/'
            AND filename = 'Evolucion_PMP.xlsx'
            AND borrar_origen = 'N'
            AND formato = 'xlsx'
            AND obligatorio = 'N'
            AND destino = 'sftp');
    ELSIF (current_db = 'sc_vlci_pre') THEN
        PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'ETL_VLCi_CDMGE_PMP_ENTIDAD'
            AND tipo = 'FTP'
            AND sistema_origen = 'FTP Sertic'
            AND patron_fichero_origen = 'Evolucion_PMP.xlsx'
            AND ruta_origen = '//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/PRE'
            AND ruta_destino = '/pre/01.fichOrig/VLCi_CDMGE_PMP_ENTIDAD/'
            AND ruta_backup = '/pre/02.fichBackup/VLCi_CDMGE_PMP_ENTIDAD/'
            AND filename = 'Evolucion_PMP.xlsx'
            AND borrar_origen = 'N'
            AND formato = 'xlsx'
            AND obligatorio = 'N'
            AND destino = 'sftp');
    ELSE
        PERFORM 1 / (SELECT COUNT(*) 
        FROM t_d_parametros_etl_carga 
        WHERE nombre_etl = 'ETL_VLCi_CDMGE_PMP_ENTIDAD'
            AND tipo = 'FTP'
            AND sistema_origen = 'FTP Sertic'
            AND patron_fichero_origen = 'Evolucion_PMP.xlsx'
            AND ruta_origen = '//dades2/dades2/ayun/TESORERIA/Jefatura de Servicio/PMP/Smartcity'
            AND ruta_destino = '/prod/01.fichOrig/VLCi_CDMGE_PMP_ENTIDAD/'
            AND ruta_backup = '/prod/02.fichBackup/VLCi_CDMGE_PMP_ENTIDAD/'
            AND filename = 'Evolucion_PMP.xlsx'
            AND borrar_origen = 'N'
            AND formato = 'xlsx'
            AND obligatorio = 'N'
            AND destino = 'sftp');
    END IF;
END $$ LANGUAGE plpgsql;