-- Verify postgis-vlci-vlci2:v_migrations/3205-policia-categories-tipo-text on pg

BEGIN;

SELECT 1 / (
    SELECT CASE 
        WHEN (
            SELECT data_type
            FROM information_schema.columns
            WHERE
                table_name = 't_datos_cb_policia_intervenciones_lastdata'
                AND column_name = 'idcategory'
        ) = 'text'
        AND (
            SELECT data_type
            FROM information_schema.columns
            WHERE
                table_name = 't_datos_cb_policia_intervenciones_lastdata'
                AND column_name = 'idsubcategory'
        ) = 'text'
        AND (
            SELECT data_type
            FROM information_schema.columns
            WHERE
                table_name = 't_datos_cb_policia_intervenciones_lastdata'
                AND column_name = 'iddetailcategory'
        ) = 'text'
        THEN 1 
        ELSE 0
    END AS all_are_text
);

ROLLBACK;
