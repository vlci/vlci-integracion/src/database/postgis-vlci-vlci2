-- Verify postgis-vlci-vlci2:v_migrations/3211-agregar-campos-tabla-policia on pg
BEGIN;

SELECT
    1 / (
        SELECT
            CASE
                WHEN num_campos = 20 THEN 1
                ELSE 0
            END
        FROM
            (
                SELECT
                    COUNT(*) as num_campos
                FROM
                    information_schema.columns
                WHERE
                    table_schema = 'vlci2'
                    AND table_name = 't_datos_cb_policia_intervenciones_lastdata'
            ) as tabla_intervenciones
    );

ROLLBACK;