-- Verify postgis-vlci-vlci2:v_migrations/2998-cambiar-clave-primaria-aguas-desag on pg

BEGIN;

-- Verificar que la nueva clave primaria existe
SELECT conname 
FROM pg_constraint 
WHERE conname = 't_datos_cb_cia_inyeccion_agua_desag_corregida_pkey'
AND conrelid = 'vlci2.t_datos_cb_cia_inyeccion_agua_desag_corregida'::regclass;

ROLLBACK;
