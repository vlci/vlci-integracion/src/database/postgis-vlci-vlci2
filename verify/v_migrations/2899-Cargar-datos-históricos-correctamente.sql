-- Verify postgis-vlci-vlci2:v_migrations/2899-Cargar-datos-históricos-correctamente on pg

BEGIN;
DO $$
DECLARE
    table_anyo_count INT;
    table_mes_count INT;
BEGIN
    -- Verificar que la tabla t_datos_cb_trafico_kpi_validados_anyo existe y está vacía
	table_anyo_count := (SELECT count(*) FROM vlci2.t_datos_cb_trafico_kpi_validados_anyo);
    

    -- Verificar que la tabla t_datos_cb_trafico_kpi_validados_mes existe y está vacía
    table_mes_count := (SELECT count(*) FROM vlci2.t_datos_cb_trafico_kpi_validados_mes);
   
   ASSERT table_anyo_count = 0;
  ASSERT table_mes_count = 0;


END $$;
ROLLBACK;
