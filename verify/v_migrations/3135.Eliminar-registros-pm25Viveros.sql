-- Verify postgis-vlci-vlci2:v_migrations/3135.Eliminar-registros-pm25Viveros on pg

 BEGIN;

DO $$
DECLARE
    pm25_entries_remaining integer;
BEGIN

    pm25_entries_remaining := (
select  count (pm25value) 
from vlci2.t_datos_cb_medioambiente_airqualityobserved_va 
where refpointofinterest = 'A06_VIVERS' 
	AND dateobserved BETWEEN '2020-03-01 00:00:00.000' AND '2023-12-31 23:59:59.999'
	and (pm25value is not null or pm25valueflag is not null));

    ASSERT pm25_entries_remaining = 0;
END $$;

ROLLBACK;
