-- Verify postgis-vlci-vlci2:v_migrations/2349-fix-monthly-validated-data-table on pg

DO $$ 
BEGIN
    -- Check if the table exists
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables 
               WHERE schemaname = 'vlci2' 
               AND tablename = 't_datos_cb_medioambiente_kpi_temperatura_valid_mes') THEN
        
        -- Check if all required columns exist
        IF EXISTS (SELECT 1 FROM information_schema.columns 
                   WHERE table_schema = 'vlci2' 
                   AND table_name = 't_datos_cb_medioambiente_kpi_temperatura_valid_mes'
                   AND column_name IN ('recvtime', 'fiwareservicepath', 'entityid', 'entitytype',
                                       'originalentityid', 'originalentitytype', 'originalservicepath', 
                                       'calculationperiod', 'description', 'name', 'location', 
                                       'kpivalue', 'operationalStatus', 'updatedat', 'measureunit')) THEN
        ELSE
            RAISE EXCEPTION  'Table exists, but one or more columns are missing.';
        END IF;
    ELSE
        RAISE EXCEPTION  'Table does not exist.';
    END IF;
END $$;