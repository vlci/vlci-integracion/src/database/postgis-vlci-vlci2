-- Verify postgis-vlci-vlci2:v_migrations/3430.Delete_row_anomalos_trafico_anual on pg

BEGIN;

DO $$
DECLARE
    entries integer;
BEGIN

    entries := (select  count (*) from vlci2.t_datos_cb_trafico_kpi_validados_anyo where calculationperiod <= '2023-12-31');

    ASSERT entries >= 77677;
END $$;

ROLLBACK;
