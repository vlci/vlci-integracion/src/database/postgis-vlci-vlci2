-- Verify postgis-vlci-vlci2:v_migrations/2885.carga-datos-personal on pg

DO $$
DECLARE
    insert_in_personal integer;
BEGIN

    insert_in_personal := (SELECT count(*) FROM vlci2.t_datos_sql_personal_carga_excel);

    ASSERT insert_in_personal = 5301;
END $$;