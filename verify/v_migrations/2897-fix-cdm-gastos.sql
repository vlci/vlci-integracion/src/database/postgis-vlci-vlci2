-- Verify postgis-vlci-vlci2:v_migrations/2897-fix-cdm-gastos on pg

DO $$
DECLARE
    delegations_without_description integer;
	areas_without_description integer;
BEGIN

    delegations_without_description := (SELECT count(*) FROM vlci2.t_d_delegacion 
   		where desc_corta_cas is null 
   			or desc_corta_val is null
   			or desc_larga_cas is null
   			or desc_larga_val is null);
   		
   	areas_without_description := (SELECT count(*) FROM vlci2.t_d_area 
   		where desc_corta_cas is null 
   			or desc_corta_val is null
   			or desc_larga_cas is null
   			or desc_larga_val is null);

    ASSERT delegations_without_description = 0;
    ASSERT areas_without_description = 0;
END $$;