-- Verify postgis-vlci-vlci2:v_migrations/3430.Delete_row_anomalos_trafico_mes on pg

BEGIN;

DO $$
DECLARE
    entries integer;
BEGIN

    entries := (select  count (*) from vlci2.t_datos_cb_trafico_kpi_validados_mes where calculationperiod <= '2024-12-01');

    ASSERT entries >= 10398;
END $$;

ROLLBACK;
