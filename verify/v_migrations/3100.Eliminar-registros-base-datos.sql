-- Verify postgis-vlci-vlci2:v_migrations/3100.Eliminar-registros-base-datos on pg

BEGIN;
select 1/count (*) from vlci2.t_datos_cb_medioambiente_airqualityobserved_va
where refpointofinterest  = 'A02_BULEVARDSUD' and dateobserved > '2020-03-01 00:00:00.000'
and pm10value is null;

select 1/count (*) from vlci2.t_datos_cb_medioambiente_airqualityobserved_va
where refpointofinterest  = 'A06_VIVERS' and dateobserved > '2020-03-01 00:00:00.000'
and pm10value is null;

select 1/count (*) from vlci2.t_datos_cb_medioambiente_airqualityobserved_va
where refpointofinterest  = 'A02_BULEVARDSUD' 
and pm25value is null;

ROLLBACK;
