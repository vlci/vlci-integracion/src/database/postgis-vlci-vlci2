-- Verify postgis-vlci-vlci2:v_migrations/2942.Eliminar-wifi-campos-api-loader on pg
BEGIN;
DO $$
DECLARE
    delete_wifi integer;
BEGIN

    delete_wifi := (SELECT count(*) FROM vlci2.t_d_api_loader_campos_request);

    ASSERT delete_wifi = 48;
END $$;

ROLLBACK;
