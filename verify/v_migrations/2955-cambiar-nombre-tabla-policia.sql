-- Verify postgis-vlci-vlci2:v_migrations/2955-cambiar-nombre-tabla-policia on pg

BEGIN;

SELECT 
    CASE 
        WHEN (SELECT COUNT(*) FROM information_schema.tables
              WHERE table_name = 't_datos_cb_policia_intervenciones_lastdata'
              AND table_schema = 'vlci2') = 1 
        THEN 1
        ELSE 0
    END;

ROLLBACK;
