-- Verify postgis-vlci-vlci2:v_migrations/3121.Eliminar-registros-base-datos-O3-ValenciaCentro on pg
 BEGIN;

DO $$
DECLARE
    o3_entries_remaining integer;
BEGIN

    o3_entries_remaining := (select  count (o3value) from vlci2.t_datos_cb_medioambiente_airqualityobserved_va where refpointofinterest = 'A07_VALENCIACENTRE' and (o3value is not null or o3valueflag is not null));

    ASSERT o3_entries_remaining = 0;
END $$;

ROLLBACK;
