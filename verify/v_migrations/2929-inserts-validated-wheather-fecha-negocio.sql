-- Verify postgis-vlci-vlci2:v_migrations/2929-inserts-validated-wheather-fecha-negocio on pg

DO $$
DECLARE
    new_entries integer;
    expected_entries integer;
BEGIN

	expected_entries := 4;
	new_entries := (select count(*) from vlci2.t_d_fecha_negocio_etls where etl_nombre in ('W02_NAZARET_precipitaciones_va', 'W01_AVFRANCIA_temperatura_va', 'W02_NAZARET_precipitaciones_vm', 'W01_AVFRANCIA_temperatura_vm'));
	assert new_entries = expected_entries;
END $$;
