-- Verify postgis-vlci-vlci2:v_migrations/3441.borrar-tabla-t_d_parametros_etl_avisos on pg


BEGIN;

  SELECT 1/(case count(*) when 0 then 1 else 0 end)
  FROM pg_catalog.pg_tables
  WHERE schemaname = 'vlci2'
  AND tablename = 't_d_parametros_etl_avisos';


ROLLBACK;
