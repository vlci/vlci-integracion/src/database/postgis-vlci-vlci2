-- Verify postgis-vlci-vlci2:v_migrations/3135.Eliminar-registros-pm25Viveros-SO2Centro on pg

 BEGIN;

DO $$
DECLARE
    so2_entries_remaining integer;
BEGIN

    so2_entries_remaining := (select  count (so2value) from vlci2.t_datos_cb_medioambiente_airqualityobserved_va where refpointofinterest = 'A07_VALENCIACENTRE' and (so2value is not null or so2valueflag is not null));

    ASSERT so2_entries_remaining = 0;
END $$;

ROLLBACK;
