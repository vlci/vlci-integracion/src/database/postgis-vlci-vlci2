-- Verify postgis-vlci-vlci2:v_migrations/2349.Eliminacion-tablas-Postgres on pg

BEGIN;

DO $$
BEGIN
   
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables WHERE tablename = 't_f_text_kpi_precipitaciones_mensuales') THEN
        PERFORM 1 / 0;  
    END IF;

  
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables WHERE tablename = 't_f_text_kpi_precipitaciones_anuales') THEN
        PERFORM 1 / 0;  
    END IF;

   
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables WHERE tablename = 't_f_text_kpi_temperatura_minima_diaria') THEN
        PERFORM 1 / 0;  
    END IF;

    
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables WHERE tablename = 't_f_text_kpi_temperatura_maxima_diaria') THEN
        PERFORM 1 / 0;  
    END IF;
END $$;

ROLLBACK;
