-- Verify postgis-vlci-vlci2:v_migrations/1488-insertar-campo-timeinstant-aparcamientos on pg

BEGIN;

SELECT column_name, data_type 
FROM information_schema.columns 
WHERE table_name = 't_datos_cb_trafico_aparcamientos' AND column_name = 'TimeInstant';

SELECT 1/count(*) from vlci2.t_datos_cb_trafico_aparcamientos where recvtime = TimeInstant;

ROLLBACK;
