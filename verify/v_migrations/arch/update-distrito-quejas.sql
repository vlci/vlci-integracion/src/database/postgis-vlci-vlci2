-- Verify postgis-vlci-vlci2:v_migrations/update-distrito-quejas on pg

BEGIN;

select 1 / case when count(*) = 0 then 1 else 0 end
from vlci2.t_datos_etl_quejas_sugerencias 
where distrito_localizacion not in (
'01-Ciutat Vella',
'02-L''Eixample',
'03-Extramurs',
'04-Campanar',
'05-La Saidia',
'06-El Pla del Real',
'07-L''Olivereta',
'08-Patraix',
'09-Jesus',
'10-Quatre Carreres',
'11-Poblats Maritims',
'12-Camins al Grau',
'13-Algiros',
'14-Benimaclet',
'15-Rascanya',
'16-Benicalap',
'17-Pobles del Nord',
'18-Pobles de l''Oest',
'19-Pobles del Sud',
'98-Fora de Valencia',
'99-No consta'
);

ROLLBACK;
