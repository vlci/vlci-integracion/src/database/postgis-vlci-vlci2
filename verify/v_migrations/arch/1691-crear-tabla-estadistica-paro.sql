-- Verify postgis-vlci-vlci2:v_migrations/1691-crear-tabla-estadistica-paro on pg

BEGIN;

SELECT 1/count(*)
  FROM information_schema.tables
  WHERE table_schema = 'vlci2' 
    AND table_name = 't_datos_cb_estadistica_paro';

ROLLBACK;
