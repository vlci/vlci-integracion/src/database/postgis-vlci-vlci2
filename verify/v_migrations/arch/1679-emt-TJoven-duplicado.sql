-- Verify postgis-vlci-vlci2:v_migrations/1679-emt-TJoven-duplicado on pg

BEGIN;

select * from vlci2.t_datos_cb_emt_kpi 
where sliceanddice1='Titulo'
and sliceanddicevalue1='T Joven'
and calculationperiod >='2020-03-02'
and calculationperiod <='2020-03-24'
;

ROLLBACK;
