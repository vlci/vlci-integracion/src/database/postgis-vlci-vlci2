-- Verify postgis-vlci-vlci2:v_migrations/1525.eliminar-tablas-housekeeping on pg

BEGIN;

select 1/(4 - count(*)) from vlci2.housekeeping_config WHERE table_nam IN ('t_d_area_arch', 't_d_delegacion_arch', 't_d_servicio_arch', 't_d_usuario_servicio_arch');

ROLLBACK;
