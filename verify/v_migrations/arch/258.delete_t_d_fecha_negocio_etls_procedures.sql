-- Verify postgis-vlci-vlci2:v_migrations/258.delete_t_d_fecha_negocio_etls_procedures on pg

BEGIN;

select 1 / (case count(*) when 0 then 1 else 0 end)
from t_d_fecha_negocio_etls
where etl_nombre like '%PROCEDURE_PMP_SERV%';

ROLLBACK;
