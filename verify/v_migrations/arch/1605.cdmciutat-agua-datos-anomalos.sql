-- Verify postgis-vlci-vlci2:v_migrations/1605.cdmciutat-agua-datos-anomalos on pg

BEGIN;

  select 1/(1-count(*)) from t_f_text_cb_kpi_consumo_agua where calculationperiod ='2023-03-26' and name = 'Consumo_agua_total_valencia';

  select 1/(51-count(*)) from t_f_text_cb_kpi_consumo_agua where calculationperiod IN ('2021-05-19', '2021-05-20', '2021-09-05', '2021-09-06', '2021-09-07', '2021-09-08', '2021-09-09', '2021-09-10', '2021-09-11', '2021-09-12', '2021-09-13', '2021-09-14', '2021-09-15') and neighborhood = 'Perellonet';

  select 1/(4-count(*)) from t_f_text_cb_kpi_consumo_agua where calculationperiod ='2021-04-27' and neighborhood = 'Mestalla';

  select 1/(20-count(*)) from t_f_text_cb_kpi_consumo_agua where calculationperiod in ('2022-05-10', '2022-05-11', '2022-05-12', '2022-05-13', '2022-05-14') and neighborhood = 'Exposicio';

  select 1/(12-count(*)) from t_f_text_cb_kpi_consumo_agua where calculationperiod in ('2020-10-27', '2020-10-28', '2020-10-29') and neighborhood = 'Soternes';

  select 1/(13-count(*)) from t_f_text_cb_kpi_consumo_agua where calculationperiod in ('2021-03-21', '2021-03-23','2023-01-23') and neighborhood = 'Benimamet';

  select 1/(32-count(*)) from t_f_text_cb_kpi_consumo_agua where calculationperiod in ('2021-06-28', '2021-06-29', '2021-07-07', '2021-07-08', '2021-07-16', '2021-07-18', '2021-07-22', '2021-11-20') and neighborhood = 'Malilla';

ROLLBACK;
