-- Verify postgis-vlci-vlci2:v_migrations/1829-drop-tabla-medioambiente-visualizacion on pg

BEGIN;

select 1 / case when count(*) = 11 then 1 else 0 end from vlci2.t_ref_horizontal_visualizacion;

SELECT 1 / case when count(*) = 0 then 1 else 0 end
    FROM information_schema.tables 
WHERE table_name = 't_ref_medioambiente_visualizacion_estaciones';

ROLLBACK;
