-- Verify postgis-vlci-vlci2:v_migrations/1734.delete-data-istic12-2019 on pg

BEGIN;
SELECT 1 / CASE WHEN COUNT(*) = 0::BIGINT THEN 1 ELSE 0 END
FROM vlci2.t_f_indicador_valor
WHERE fk_indicador = 1110
  AND dc_periodicidad_muestreo_valor LIKE '%/2019%';
ROLLBACK;
