-- Verify postgis-vlci-vlci2:v_migrations/t_datos_etl_estadistica_log on pg

BEGIN;

SELECT *
  FROM vlci2.t_datos_etl_estadistica_log
 WHERE FALSE;

ROLLBACK;
