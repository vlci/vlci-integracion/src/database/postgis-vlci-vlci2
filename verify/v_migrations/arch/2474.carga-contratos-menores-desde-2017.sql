-- Verify postgis-vlci-vlci2:v_migrations/2474.carga-contratos-menores-desde-2017 on pg

DO $$
    DECLARE
        num_contratos integer;
        num_items integer;
        num_columns integer;
        precision_enteros integer;
        max_lenght_varchars varchar;
    BEGIN

        num_contratos := (SELECT COUNT(*) FROM vlci2.t_datos_etl_contratos_menores);
        ASSERT num_contratos = 13084;

        num_items := (select ARRAY_LENGTH(conkey, 1) from pg_constraint where conname = 't_datos_etl_contratos_menores_pkey');
        ASSERT num_items = 1;

        num_columns := (select count(*) from information_schema.columns where table_schema = 'vlci2' and table_name = 't_datos_etl_contratos_menores');
        ASSERT num_columns = 39;

        precision_enteros := 
        (
            select distinct numeric_precision 
            from information_schema.columns
            where table_schema = 'vlci2'
            and table_name = 't_datos_etl_contratos_menores'
            and column_name IN ('numero_propuestas', 'numero_ofertas', 'duracion_contrato_meses')
        );
        ASSERT precision_enteros = 3;

        max_lenght_varchars := 
        (
            select distinct character_maximum_length 
            from information_schema.columns
            where table_schema = 'vlci2'
            and table_name = 't_datos_etl_contratos_menores'
            and column_name IN ('num_expediente', 'objeto_contrato', 'centro_gastos')
        );
        ASSERT max_lenght_varchars IS NULL;

    ROLLBACK;
END $$;
