-- Verify postgis-vlci-vlci2:v_migrations/TLF028-correcion-datos-ruzafa on pg

BEGIN;

select 1 / case when count(*) > 0 then 0 else 1 end
from t_f_text_cb_sonometros_ruzafa_daily tftcsrd 
where cast(laeq as decimal)<0 or 
	cast(laeq_d as decimal)<0 or 
	cast(laeq_e as decimal)<0 or 
	cast(laeq_n as decimal)<0 or
	cast(laeq_den as decimal)<0;
    
ROLLBACK;
