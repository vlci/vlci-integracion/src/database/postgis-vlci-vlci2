-- Verify postgis-vlci-vlci2:v_migrations/2605-insert-unidades-organizativas-archivadas on pg
DO $$
DECLARE
    areas_not_insert integer;
    delegaciones_not_insert integer;
    servicios_not_insert integer;
BEGIN

   areas_not_insert := (
    select count(*)
    from vlci2.t_d_area_arch 
    where pk_area_id is not null
        and pk_area_id not in (select pk_area_id from vlci2.t_d_area));
   ASSERT areas_not_insert = 0;

   delegaciones_not_insert := (
    select count(*)
    from vlci2.t_d_delegacion_arch 
    where pk_delegacion_id is not null
        and pk_delegacion_id not in (select pk_delegacion_id from vlci2.t_d_delegacion));
   ASSERT delegaciones_not_insert = 0;

    servicios_not_insert := (
        select count(*)
        from vlci2.t_d_servicio_arch
        where codigo_organico  is not null
            and codigo_organico not in (select codigo_organico from vlci2.t_d_servicio));
    ASSERT servicios_not_insert = 0;

END $$;
