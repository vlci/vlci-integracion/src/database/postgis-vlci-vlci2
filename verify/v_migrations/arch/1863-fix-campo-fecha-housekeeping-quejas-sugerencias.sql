-- Verify postgis-vlci-vlci2:v_migrations/1863-fix-campo-fecha-housekeeping-quejas-sugerencias on pg

BEGIN;

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.housekeeping_config
where table_nam='t_datos_etl_quejas_sugerencias_hist' and colfecha='aud_user_ins';

select case when count(*)=19 then 1 else 1/(select 0) end as resultado  
from vlci2.housekeeping_config;


ROLLBACK;
