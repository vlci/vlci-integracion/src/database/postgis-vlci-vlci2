-- Verify postgis-vlci-vlci2:v_migrations/46.Anyadir_columnas_unidad_medida on pg

BEGIN;

select measureunitcas, measureunitval from vlci2.t_f_text_kpi_trafico_y_bicis; 
select measureunitcas, measureunitval from vlci2.t_f_text_cb_wifi_kpi_conexiones;
select measureunitcas, measureunitval from vlci2.t_datos_cb_emt_kpi;
select measurelandunit from vlci2.t_f_text_kpi_precipitaciones_anuales;
select measurelandunit from vlci2.t_f_text_kpi_precipitaciones_diarias;
select measurelandunit from vlci2.t_f_text_kpi_precipitaciones_mensuales;
select measurelandunit from vlci2.t_f_text_kpi_temperatura_maxima_diaria;
select measurelandunit from vlci2.t_f_text_kpi_temperatura_media_diaria;
select measurelandunit from vlci2.t_f_text_kpi_temperatura_minima_diaria;
select measurelandunit from vlci2.t_f_text_cb_kpi_consumo_agua;
select no2type, o3type, pm10type, pm25type, so2type from vlci2.t_datos_cb_medioambiente_airqualityobserved;
select measureunit from vlci2.t_f_text_cb_kpi_residuos;

ROLLBACK;
