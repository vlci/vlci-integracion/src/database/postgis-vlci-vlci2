-- Verify postgis-vlci-vlci2:v_migrations/2731.Arreglo_espacios_textoso on pg

BEGIN;

select 1/case when count(distinct(name)) = 23 then 1 else 0 end from vlci2.t_datos_cb_trafico_kpi_validados_mes;

select 1/case when count(distinct(name)) = 23 then 1 else 0 end from vlci2.t_datos_cb_trafico_kpi_validados_anyo;


ROLLBACK;
