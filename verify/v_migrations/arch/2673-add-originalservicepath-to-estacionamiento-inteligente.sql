-- Verify postgis-vlci-vlci2:v_migrations/2673-add-originalservicepath-to-estacionamiento-inteligente on pg

BEGIN;

SELECT 1/CASE WHEN COUNT(*) = 1 THEN 1 ELSE 0 END AS all_changes_applied
FROM
    information_schema.columns
WHERE 
	table_schema = 'vlci2' and
    table_name = 't_datos_cb_trafico_kpi_estacionamiento_inteligente'
    AND (column_name = 'originalservicepath' AND data_type = 'text');

ROLLBACK;
