-- Verify postgis-vlci-vlci2:v_migrations/2536.datos-anomalos-emt on pg

BEGIN;

SELECT 1 / case when count(*) = 0 THEN 1 ELSE 0 END
    FROM information_schema.tables 
WHERE table_name = 'carga_emt_csv';

SELECT 1 / CASE WHEN COUNT(*) = 65 THEN 1 ELSE 0 END
FROM vlci2.vlci_emt_routes;

SELECT 1 / CASE WHEN total = 5051552 THEN 1 ELSE 0 END
FROM (
     SELECT
     sum(emt.kpivalue) as total
     FROM vlci2.t_datos_cb_emt_kpi emt
     WHERE emt.recvtime = '2024-03-06 13:37:00.000'
) as total_viajeros;

ROLLBACK;