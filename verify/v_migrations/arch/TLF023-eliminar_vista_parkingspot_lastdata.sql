-- Verify postgis-vlci-vlci2:v_migrations/TLF023-eliminar_vista_parkingspot_lastdata on pg

BEGIN;

SELECT NOT EXISTS (SELECT 1 FROM pg_views WHERE viewname = 'vw_datos_cb_trafico_parkingspot_lastdata');

ROLLBACK;
