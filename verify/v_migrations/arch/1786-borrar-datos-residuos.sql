-- Verify postgis-vlci-vlci2:v_migrations/1787-borrar-datos-residuos on pg

BEGIN;

select case when count(*)=0 then 1 else 1/(select 0) end as resultado
from vlci2.t_f_text_cb_kpi_residuos 
where to_date(calculationperiod,'dd-mm-yyyy') >= '01-03-2020'
and to_date(calculationperiod,'dd-mm-yyyy') < '01-01-2022';

select case when count(*)=10392 then 1 else 1/(select 0) end as resultado
from vlci2.t_f_text_cb_kpi_residuos 
where to_date(calculationperiod,'dd-mm-yyyy') >= '01-01-2022'
and to_date(calculationperiod,'dd-mm-yyyy') < '01-06-2023';

ROLLBACK;
