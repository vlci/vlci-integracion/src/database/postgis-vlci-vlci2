DO $$
DECLARE
    unidades_tramitadoras_with_code integer;
BEGIN
	unidades_tramitadoras_with_code := (
    	select count(*) from vlci2.t_datos_etl_contratos_menores 
		where unidad_tramitadora like '% - %' and unidad_tramitadora not in ('GAB. JURIDICO, GEST.EC. Y RRHH  - POLICIA LOCAL', 'OF. GTE - AE'));

    ASSERT unidades_tramitadoras_with_code = 0;
END $$;