-- Verify postgis-vlci-vlci2:v_migrations/1841-datos-anomalos-EMT-072023 on pg

BEGIN;

select 1/ (case when count(*) = 297105 then 1 else 0 end)  from  vlci2.t_datos_cb_emt_kpi
where calculationperiod < '2023-09-01';

ROLLBACK;
