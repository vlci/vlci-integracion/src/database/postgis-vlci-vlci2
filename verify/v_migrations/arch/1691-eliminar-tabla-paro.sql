-- Verify postgis-vlci-vlci2:v_migrations/1691-eliminar-tabla-paro on pg

BEGIN;

 SELECT 1/case when count(*) = 0 then 1 else 0 end
   FROM   information_schema.tables
   WHERE  table_name = 't_datos_cb_estadistica_paro';

ROLLBACK;
