-- Verify postgis-vlci-vlci2:v_migrations/2450-gf-actualizar-patraix on pg

DO $$
DECLARE
    find_data_modified integer;
BEGIN

find_data_modified := (select count(*) from vlci2.t_d_fecha_negocio_etls WHERE etl_id=223 and etl_nombre ='A11_PATRAIX_24h' AND fen='2023-08-08 00:00:00.000');

ASSERT find_data_modified = 1;

ROLLBACK;
END $$;