-- Verify postgis-vlci-vlci2:v_migrations/TLF033-eliminar_resiudos_anomalos on pg
BEGIN;

SELECT
    1 / CASE
        WHEN count(*) = 532 THEN 1
        ELSE 0
    END
FROM
    vlci2.t_datos_cb_wastecontainer_lastdata;

ROLLBACK;