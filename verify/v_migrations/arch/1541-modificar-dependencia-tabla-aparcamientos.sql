-- Verify postgis-vlci-vlci2:v_migrations/1541-modificar-dependencia-tabla-aparcamientos on pg

BEGIN;

SELECT 1/(case is_nullable when 'YES' then 1 else 0 end)
FROM information_schema.columns 
WHERE table_name = 't_datos_cb_trafico_aparcamientos_lastdata' AND column_name = 'nombre';

ROLLBACK;
