-- Verify postgis-vlci-vlci2:v_migrations/1023.update-registros-wifi on pg

BEGIN;

select 1/count(*) from vlci2.t_f_text_cb_wifi_kpi_urbo where dlnumberofnewworkerscalculationperiodfrom::text like '2022-09-07%' and recvtime::text like '2022-09-08%';
select 1/count(*) from vlci2.t_f_text_cb_wifi_kpi_urbo where dlnumberofnewworkerscalculationperiodfrom::text like '2022-09-08%' and recvtime::text like '2022-09-09%';
select 1/count(*) from vlci2.t_f_text_cb_wifi_kpi_urbo where dlnumberofnewworkerscalculationperiodfrom::text like '2022-09-09%' and recvtime::text like '2022-09-10%';
select 1/count(*) from vlci2.t_f_text_cb_wifi_kpi_urbo where dlnumberofnewworkerscalculationperiodfrom::text like '2022-09-11%' and recvtime::text like '2022-09-12%';


ROLLBACK;
