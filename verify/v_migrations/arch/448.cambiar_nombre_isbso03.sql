-- Verify postgis-vlci-vlci2:v_migrations/448.cambiar_nombre_isbso03 on pg

BEGIN;

SELECT 1/COUNT(*) FROM vlci2.t_d_indicador
WHERE dc_identificador_indicador='IS.BSO.03' AND 
de_nombre_indicador_cas='Nº de visitas totales realizadas en el Servicio de Teleasistencia' and
de_nombre_indicador_val ='Nº de visites totals realitzades en el Servei de Teleassistència' and
de_descripcion_indicador_cas ='Nombre de visites totals realitzades en el Servei de Teleassistència.' and
de_descripcion_indicador_val ='Número de visitas totales realizadas en el Servicio de Teleasistencia.';

ROLLBACK;
