-- Verify postgis-vlci-vlci2:v_migrations/2864-new-pk-data-sonometros-zas on pg

DO $$
DECLARE
    columns_in_pk integer;
BEGIN

    columns_in_pk := (
        SELECT count(*)
        FROM pg_index i
        JOIN pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey)
        WHERE i.indrelid = 't_datos_etl_sonometros_zas_last_week'::regclass  -- Replace 'your_table' with your table name
            AND i.indisprimary
            AND a.attname IN ('originalentityid', 'dateobserved'));

    ASSERT columns_in_pk = 2;
END $$;