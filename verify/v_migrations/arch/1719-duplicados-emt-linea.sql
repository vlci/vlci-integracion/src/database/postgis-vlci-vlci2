-- Verify postgis-vlci-vlci2:v_migrations/1719-duplicados-emt-linea on pg

BEGIN;

-- Comprueba que se hayan insertado el numero de filas esperado.
select case when count(*)=61597 then 1 else 1/(select 0) end as resultado  
from vlci2.t_datos_cb_emt_kpi
where sliceanddice1 = 'Ruta'
and calculationperiod >= '2015-01-01'
and calculationperiod < '2018-01-01';

-- Comprueba que ya no existe ningun registro con doble comilla
select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_datos_cb_emt_kpi
where sliceanddice1 = 'Ruta'
and sliceanddicevalue1 like('%"%')
and calculationperiod >= '2015-01-01'
and calculationperiod < '2018-01-01'; 

ROLLBACK;
