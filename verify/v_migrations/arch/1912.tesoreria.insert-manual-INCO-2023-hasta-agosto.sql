-- Verify postgis-vlci-vlci2:v_migrations/1912.tesoreria.insert-manual-INCO-2023-hasta-agosto on pg

BEGIN;

select 1 / case when count(*) = 1165 then 1 else 0 end from vlci2.t_datos_etl_inco_ingresos;

ROLLBACK;
