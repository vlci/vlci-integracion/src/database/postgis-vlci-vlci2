-- Verify postgis-vlci-vlci2:v_migrations/559.addPositionOldItemSonometros on pg

BEGIN;

DO $$
DECLARE
    posicion_null integer;

BEGIN
   posicion_null := (SELECT COUNT(*) FROM vlci2.t_f_text_cb_sonometros_ruzafa_daily WHERE location is null);
   ASSERT posicion_null = 0;
END $$;

ROLLBACK;
