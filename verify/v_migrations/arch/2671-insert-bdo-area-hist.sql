-- Verify postgis-vlci-vlci2:v_migrations/2671-insert-bdo-area-hist on pg

DO $$
DECLARE
    insert_in_area integer;
BEGIN

    insert_in_area := (SELECT count(*) FROM vlci2.t_d_area);

    ASSERT insert_in_area = 1419;
END $$;
