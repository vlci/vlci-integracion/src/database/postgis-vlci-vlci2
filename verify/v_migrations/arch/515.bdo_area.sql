-- Verify postgis-vlci-vlci2:v_migrations/515.bdo_area on pg

BEGIN;

select 1 / (case count(*) when 11 then 1 else 0 end)
from vlci2.t_datos_etl_agregados_area;

select 1 / (case count(*) when 50 then 1 else 0 end)
from vlci2.t_datos_etl_agregados_delegacion;

select 1 / (case count(*) when 88 then 1 else 0 end)
from vlci2.t_datos_etl_agregados_servicio;

ROLLBACK;
