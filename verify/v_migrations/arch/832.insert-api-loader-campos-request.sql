-- Verify postgis-vlci-vlci2:v_migrations/832.insert-api-loader-campos-request on pg
-- Tarea PROY2100017N3-832

DO $$
DECLARE

	inserts_suma_carril_bici integer;
	inserts_total_reguladores_led integer;
    inserts_total_reguladores integer;
    inserts_total_paneles integer;
    inserts_total_camaras_web integer;
    inserts_total_aparcamotos integer;
    inserts_total_carga_descarga integer;
    inserts_total_paradas_taxi integer;
    inserts_suma_ejes_calle integer;
    inserts_suma_bici_aparcamiento integer;
    inserts_suma_minusvalidos integer;
    inserts__total_aparcamientos integer;

BEGIN

	SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_suma_carril_bici'
    INTO inserts_suma_carril_bici;
    assert inserts_suma_carril_bici > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_total_reguladores_led'
    INTO inserts_total_reguladores_led;
    assert inserts_total_reguladores_led > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_total_reguladores'
    INTO inserts_total_reguladores;
    assert inserts_total_reguladores > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_total_paneles'
    INTO inserts_total_paneles;
    assert inserts_total_paneles > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_total_camaras_web'
    INTO inserts_total_camaras_web;
    assert inserts_total_camaras_web > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_total_aparcamotos'
    INTO inserts_total_aparcamotos;
    assert inserts_total_aparcamotos > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_total_carga_descarga'
    INTO inserts_total_carga_descarga;
    assert inserts_total_carga_descarga > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_total_paradas_taxi'
    INTO inserts_total_paradas_taxi;
    assert inserts_total_paradas_taxi > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_suma_ejes_calle'
    INTO inserts_suma_ejes_calle;
    assert inserts_suma_ejes_calle > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_total_carril_bici'
    INTO inserts_suma_carril_bici;
    assert inserts_suma_carril_bici > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_suma_bici_aparcamiento'
    INTO inserts_suma_bici_aparcamiento;
    assert inserts_suma_bici_aparcamiento > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_suma_minusvalidos'
    INTO inserts_suma_minusvalidos;
    assert inserts_suma_minusvalidos > 0;

    SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_total_aparcamientos'
    INTO inserts__total_aparcamientos;
    assert inserts__total_aparcamientos > 0;

END
$$ LANGUAGE PLPGSQL;
