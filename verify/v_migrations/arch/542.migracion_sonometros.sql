-- Verify postgis-vlci-vlci2:v_migrations/542.migracion_sonometros on pg

BEGIN;

SELECT 1 / (CASE count(*) WHEN 0 THEN 0 ELSE 1 END)
FROM vlci2.t_f_text_cb_sonometros_ruzafa_daily
WHERE dateobserved < '2020-09-17' OR (dateobserved BETWEEN '2020-09-27' AND '2020-09-28' AND "name" = 'Sensor de ruido del barrio de ruzafa. Puerto Rico 21');

ROLLBACK;