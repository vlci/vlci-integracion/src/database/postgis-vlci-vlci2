-- Verify postgis-vlci-vlci2:v_migrations/1393.insertar-fila-t-d-parametros on pg

BEGIN;

select 1 / (case count(*) when 1 then 1 else 0 end)
from vlci2.t_d_parametros
where param_clave = 'fecha_inicial_datos_cdmciutat';

ROLLBACK;
