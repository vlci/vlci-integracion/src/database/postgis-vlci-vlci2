-- Verify postgis-vlci-vlci2:v_migrations/2078-alter-etl-sonometros-zas-table on pg

BEGIN;

SELECT
    CASE
        WHEN COUNT(*) = 8 THEN 1  -- Assuming there are 8 columns to be checked
        ELSE 0
    END AS all_changes_applied
FROM
    information_schema.columns
WHERE 
	table_schema = 'vlci2' and
    table_name = 't_datos_etl_sonometros_zas_last_week'
    AND (
        (column_name = 'laeq' AND data_type = 'numeric' AND numeric_precision is null) OR
        (column_name = 'laeqreal' AND data_type = 'numeric' AND numeric_precision is null) OR
        (column_name = 'lafmax' AND data_type = 'numeric' AND numeric_precision is null) OR
        (column_name = 'lafmaxreal' AND data_type = 'numeric' AND numeric_precision is null) OR
        (column_name = 'lafmin' AND data_type = 'numeric' AND numeric_precision is null) OR
        (column_name = 'lafminreal' AND data_type = 'numeric' AND numeric_precision is null) OR
        (column_name = 'laieq' AND data_type = 'numeric' AND numeric_precision is null) OR
        (column_name = 'laieqreal' AND data_type = 'numeric' AND numeric_precision is null)
    );

ROLLBACK;
