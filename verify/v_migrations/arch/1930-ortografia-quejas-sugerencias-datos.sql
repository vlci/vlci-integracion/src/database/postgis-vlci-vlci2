-- Verify postgis-vlci-vlci2:v_migrations/1930-ortografia-quejas-sugerencias-datos on pg

BEGIN;

select 1 / (case count(*) when 0 then 1 else 0 end)
from vlci2.t_datos_etl_quejas_sugerencias
WHERE tema='Tramitacion administrativa, tributos y sanciones';

select 1 / (case count(*) when 0 then 1 else 0 end)
from vlci2.t_datos_etl_quejas_sugerencias
WHERE tema='Via publica reparacion de deficiencias';

select 1 / (case count(*) when 45572 then 1 else 0 end)
from vlci2.t_datos_etl_quejas_sugerencias;

ROLLBACK;
