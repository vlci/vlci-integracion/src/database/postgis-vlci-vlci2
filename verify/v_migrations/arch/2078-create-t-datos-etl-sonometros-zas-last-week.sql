-- Verify postgis-vlci-vlci2:v_migrations/2078-create-t-datos-etl-sonometros-zas-last-week on pg

BEGIN;

SELECT 1/count(*)
    FROM information_schema.tables 
WHERE table_name = 't_datos_etl_sonometros_zas_last_week';


ROLLBACK;
