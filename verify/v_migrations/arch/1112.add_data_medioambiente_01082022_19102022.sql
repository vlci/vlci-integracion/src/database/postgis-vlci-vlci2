-- Verify postgis-vlci-vlci2:v_migrations/1112.add_data_medioambiente_01082022_19102022 on pg

BEGIN;


--vamos a ver tambien que esos estan entre las fechas indicadas 

select 1 / (case count(*) when 664+151 then 1 else 0 end)
from vlci2.t_datos_cb_medioambiente_airqualityobserved
where (recvtime >= '2022-08-01' and recvtime <= '2022-10-20');

--y vamos a comprobar que nos se han tocado las de menos de x fecha

select 1 / (case count(*) when 5859 then 1 else 0 end)
from vlci2.t_datos_cb_medioambiente_airqualityobserved
where recvtime < '2022-08-01' ;



ROLLBACK;
