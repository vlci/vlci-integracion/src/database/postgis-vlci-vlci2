-- Verify postgis-vlci-vlci2:v_migrations/1906-insert-inicial-quejas-sugerencias-mapeo on pg

BEGIN;

select 1 / case when count(*) = 133 then 1 else 0 end from vlci2.t_ref_etl_quejas_sugerencias_mapeo_temas;

ROLLBACK;
