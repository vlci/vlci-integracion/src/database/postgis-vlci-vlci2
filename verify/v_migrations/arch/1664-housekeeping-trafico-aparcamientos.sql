-- Verify postgis-vlci-vlci2:v_migrations/1664-housekeeping-trafico-aparcamientos on pg

BEGIN;

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 18) THEN 1
        ELSE 0
    END as outvalue
from vlci2.housekeeping_config
) verify;

ROLLBACK;
