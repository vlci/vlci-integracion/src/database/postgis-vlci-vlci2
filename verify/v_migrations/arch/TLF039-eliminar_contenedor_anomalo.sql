-- Verify postgis-vlci-vlci2:v_migrations/TLF039-eliminar_contenedor_anomalo on pg
BEGIN;

SELECT
    1 / case
        when count(*) = 0 THEN 1
        ELSE 0
    END
FROM
    t_datos_cb_wastecontainer_lastdata
WHERE
    entityid = 'wastecontainer:5ddbff4e6c54998b7f89fb4e';

ROLLBACK;