-- Verify postgis-vlci-vlci2:v_migrations/2376-alter-validacion-medioambiente on pg

BEGIN;

-- Tabla de validación Mensual
SELECT originalservicepath, originalentityid, originalentitytype FROM t_datos_cb_medioambiente_airqualityobserved_vm;
select CASE
        WHEN COUNT(*) = 3 THEN 1
        ELSE 0
    END 
FROM
    information_schema.key_column_usage 
WHERE 
table_schema = 'vlci2'
and	constraint_name  = 't_datos_cb_medioambiente_airqualityobserved_vm_pkey'
and column_name in ('dateobserved','operationalstatus','originalentityid');

-- Tabla de validación Anual
SELECT originalservicepath, originalentityid, originalentitytype FROM t_datos_cb_medioambiente_airqualityobserved_va;
select CASE
        WHEN COUNT(*) = 3 THEN 1
        ELSE 0
    END 
FROM
    information_schema.key_column_usage 
WHERE 
table_schema = 'vlci2'
and	constraint_name  = 't_datos_cb_medioambiente_airqualityobserved_va_pkey'
and column_name in ('dateobserved','operationalstatus','originalentityid');

ROLLBACK;
