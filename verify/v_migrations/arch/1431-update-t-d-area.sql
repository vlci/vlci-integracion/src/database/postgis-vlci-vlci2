-- Verify postgis-vlci-vlci2:v_migrations/1431-update-t-d-area on pg

BEGIN;

select case when count(*)=9 then 1 else 1/(select 0) end as resultado 
from vlci2.t_d_area where pk_area_id not in ('1A', '1P');

ROLLBACK;
