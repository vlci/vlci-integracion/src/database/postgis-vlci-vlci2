-- Verify postgis-vlci-vlci2:v_migrations/t_datos_etl_estadistica_definicion_indicadores on pg

BEGIN;

SELECT *
  FROM vlci2.t_datos_etl_estadistica_definicion_indicadores
 WHERE FALSE;

ROLLBACK;
