-- Verify postgis-vlci-vlci2:v_migrations/2676-eliminate-redundant-columns-contratos-menores on pg

DO $$
DECLARE
    redundant_columns integer;
BEGIN

    redundant_columns := (SELECT COUNT(*) FROM information_schema.columns 
        WHERE table_schema = 'vlci2'
            AND table_name = 't_datos_etl_contratos_menores'
            AND column_name in ('area', 'area_cod_org', 'area_desc_val', 'area_desc_corta_cas', 'area_desc_corta_val', 'servicio_cod_org'));

    ASSERT redundant_columns = 0;
END $$;
