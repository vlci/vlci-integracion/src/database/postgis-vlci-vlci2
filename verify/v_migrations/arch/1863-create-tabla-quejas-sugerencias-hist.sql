-- Verify postgis-vlci-vlci2:v_migrations/1863-create-tabla-quejas-sugerencias-hist on pg

BEGIN;

SELECT count(*) 
    FROM information_schema.tables 
WHERE table_name = 't_datos_etl_quejas_sugerencias_hist';

ROLLBACK;
