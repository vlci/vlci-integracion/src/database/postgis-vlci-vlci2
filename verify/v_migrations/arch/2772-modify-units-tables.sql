-- Verify postgis-vlci-vlci2:v_migrations/2772-modify-units-tables on pg

BEGIN;

--Se comprueba que existan las columnas que se añaden en este deploy

select fk_area_code, fk_delegacion_code, anyo, mes from vlci2.t_d_servicio;

select fk_area_code, codigo_organico, anyo, mes from vlci2.t_d_delegacion;

select codigo_organico, anyo, mes from vlci2.t_d_area;

ROLLBACK;
