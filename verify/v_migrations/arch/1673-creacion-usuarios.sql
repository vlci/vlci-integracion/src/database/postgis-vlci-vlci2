-- Verify postgis-vlci-vlci2:v_migrations/1673-creacion-usuarios on pg

BEGIN;

select 1/count(*) from vlci2.t_d_user_rol where pk_user = 'U19316' and cod_situacion = 'A';
select 1/count(*) from vlci2.t_d_user_rol where pk_user = 'U18587' and rol = 'ESTANDAR';

select 1/verify.outvalue from(
    select 
        case
            when(count(*) = 185) then 1
            else 0
        end as outvalue
    from vlci2.t_d_user_rol
)verify;

ROLLBACK;
