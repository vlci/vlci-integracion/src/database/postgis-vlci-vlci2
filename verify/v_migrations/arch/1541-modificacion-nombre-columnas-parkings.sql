-- Verify postgis-vlci-vlci2:v_migrations/1541-modificacion-nombre-columnas-parkings on pg

BEGIN;

SELECT 1/count(*) 
FROM information_schema.columns 
WHERE table_name = 't_datos_cb_trafico_aparcamientos_lastdata' 
AND column_name IN('totalspotnumber', 'availablespotnumber');

ROLLBACK;
