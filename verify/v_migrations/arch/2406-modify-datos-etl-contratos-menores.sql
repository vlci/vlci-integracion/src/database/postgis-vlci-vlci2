-- Verify postgis-vlci-vlci2:v_migrations/2406-modify-datos-etl-contratos-menores on pg

DO $$
DECLARE
    duplicated_rows integer;
    columns_pk integer;
    columns_expected integer;
    new_columns_count integer;
    deleted_columns_count integer;
    new_nullable_columns integer;
    new_triggers integer;
BEGIN

    duplicated_rows := (select case when sum(repetidas) is null then 0 else 1 end from ( 
                            select count(num_expediente)  as repetidas from vlci2.t_datos_etl_contratos_menores 
                            group by (num_expediente, objeto_contrato, adjudicatario) having count(num_expediente) > 1 ) as sq);

   columns_pk := (select count(*) from information_schema.table_constraints tc 
                    JOIN information_schema.key_column_usage kcu
                    ON tc.constraint_name = kcu.constraint_name
                    WHERE tc.table_name = 't_datos_etl_contratos_menores');

    new_columns_count := (select count(*) from information_schema.columns c
                                where c.table_schema = 'vlci2'
                                and c.table_name = 't_datos_etl_contratos_menores'
                                and c.column_name in ('servicio_cod_org', 
                                                    'area_desc_corta_val',
                                                    'area_desc_corta_cas',
                                                    'area_desc_val',
                                                    'area_desc_cas',
                                                    'area_cod_org'));

    deleted_columns_count := (select count(*) from information_schema.columns c
                                where c.table_schema = 'vlci2'
                                and c.table_name = 't_datos_etl_contratos_menores'
                                and c.column_name = 'nif_adjudicatario');

    new_nullable_columns := (SELECT count(*) FROM INFORMATION_SCHEMA.columns c
                                where c.table_schema = 'vlci2'
                                and c.table_name = 't_datos_etl_contratos_menores'
                                and c.column_name in ('centro_gastos', 'aplicacion_presupuestaria')
                                and is_nullable = 'YES');

    new_triggers := (select count(*) from pg_trigger where not tgisinternal
                        and tgrelid = 't_datos_etl_contratos_menores'::regclass
                        and tgname in ('t_datos_etl_contratos_menores_before_insert','t_datos_etl_contratos_menores_before_update'));

   ASSERT duplicated_rows = 0;
   ASSERT columns_pk = 3;
   ASSERT new_columns_count = 5;
   ASSERT deleted_columns_count = 0;
   ASSERT new_nullable_columns = 2;
   ASSERT new_triggers = 2;
END $$;