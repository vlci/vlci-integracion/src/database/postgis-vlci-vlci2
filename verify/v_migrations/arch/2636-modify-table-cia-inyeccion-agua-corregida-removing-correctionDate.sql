-- Verify postgis-vlci-vlci2:v_migrations/2636-modify-table-cia-inyeccion-agua-corregida-removing-correctionDate on pg

BEGIN;

-- Verify new column names
SELECT kpiValueOriginal FROM vlci2.t_datos_cb_cia_inyeccion_agua_corregida LIMIT 1;

-- Verify correctiondate is dropped
DO $$
BEGIN
    IF EXISTS (SELECT FROM information_schema.columns WHERE table_schema = 'vlci2' AND table_name = 't_datos_cb_cia_inyeccion_agua_corregida' AND column_name = 'correctiondate') THEN
        RAISE EXCEPTION 'correctiondate still exists';
    END IF;
END $$;

ROLLBACK;
