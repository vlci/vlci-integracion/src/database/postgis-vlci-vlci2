-- Verify postgis-vlci-vlci2:v_migrations/579.unidad_medida_albufera on pg

BEGIN;

select 1 / (case count(*) when 184 then 1 else 0 end)
from vlci2.t_m_unidad_medida;

ROLLBACK;
