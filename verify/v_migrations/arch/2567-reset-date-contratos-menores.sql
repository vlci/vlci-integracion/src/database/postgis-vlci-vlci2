-- Verify postgis-vlci-vlci2:v_migrations/2567-reset-date-contratos-menores on pg



DO $$
DECLARE
    sonometros_fecha_correcta integer;
BEGIN
	sonometros_fecha_correcta := (select count(*) from vlci2.t_d_fecha_negocio_etls where fen = '2024-03-01' and etl_nombre = 'py_contratacion_contratos_menores');

    ASSERT sonometros_fecha_correcta = 1;
END $$;