-- Verify postgis-vlci-vlci2:v_migrations/1567.historicos_EMT_mob006-01_2019 on pg

BEGIN;

select case when count(*)=11914 then 1 else 1/(select 0) end as resultado  
from vlci2.t_datos_cb_emt_kpi
where entityid = 'mob006-01'
and calculationperiod < '2020-01-01'
and calculationperiod >= '2019-01-01';

ROLLBACK;
