-- Verify postgis-vlci-vlci2:v_migrations/585.alta_user_cdm on pg

BEGIN;

select 1 / (case count(*) when 2 then 1 else 0 end)
from vlci2.t_d_user_rol
where pk_user in ('U301381', 'LJCG');;

select 1 / (case count(*) when 190 then 1 else 0 end)
from vlci2.t_d_usuario_servicio
where pk_user_id in ('U301381', 'LJCG');

ROLLBACK;
