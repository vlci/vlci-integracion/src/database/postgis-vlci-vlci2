-- Verify postgis-vlci-vlci2:v_migrations/838.eliminar-informacion-sensible on pg
DO $$
DECLARE
    number_of_rows integer;
BEGIN
   number_of_rows := (SELECT COUNT(*) FROM vlci2.t_d_api_loader_cabeceras WHERE nombre_api = 'Airwave_Wifi_login' AND cabecera in ('credential_0', 'credential_1'));
   ASSERT number_of_rows = 0;
END $$;