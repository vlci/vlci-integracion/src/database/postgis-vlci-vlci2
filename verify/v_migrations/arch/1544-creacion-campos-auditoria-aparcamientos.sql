-- Verify postgis-vlci-vlci2:v_migrations/1544-creacion-campos-auditoria-aparcamientos on pg

BEGIN;

SELECT 1/count(*)
FROM information_schema.columns
WHERE table_schema = 'vlci2'
  AND table_name = 't_datos_cb_trafico_aparcamientos'
  AND column_name IN ('aud_user_ins', 'aud_fec_upd', 'aud_user_upd', 'aud_fec_ins');


ROLLBACK;
