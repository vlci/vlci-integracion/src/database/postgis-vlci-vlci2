-- Verify postgis-vlci-vlci2:v_migrations/2409-add-contratos-menores-fecha-negocio on pg

DO $$
DECLARE
    contratos_menores_entrie integer;
BEGIN

    contratos_menores_entrie := (select count(*) from vlci2.t_d_fecha_negocio_etls where etl_nombre = 'py_contratacion_contratos_menores');

   ASSERT contratos_menores_entrie = 1;

END $$;