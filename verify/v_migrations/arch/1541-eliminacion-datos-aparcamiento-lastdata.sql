-- Verify postgis-vlci-vlci2:v_migrations/1541-eliminacion-datos-aparcamiento-lastdata on pg

BEGIN;

SELECT 1/ case when (count(*) = 0) then 1 else 0 end from vlci2.t_datos_cb_trafico_aparcamientos_lastdata;

ROLLBACK;
