-- Verify postgis-vlci-vlci2:v_migrations/1430.anyadir-unidad-medida-o3 on pg

BEGIN;

select 1/count(*) 
from vlci2.t_datos_cb_medioambiente_airqualityobserved where o3type = 'µg/m3';

ROLLBACK;
