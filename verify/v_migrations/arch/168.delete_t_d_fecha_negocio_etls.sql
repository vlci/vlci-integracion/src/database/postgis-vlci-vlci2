-- Verify postgis-vlci-vlci2:v_migrations/168.delete_t_d_fecha_negocio_etls on pg

BEGIN;

select 1 / (case count(*) when 0 then 1 else 0 end)
from t_d_fecha_negocio_etls
where etl_nombre like 'VLCI_ETL_OCI_IND_REPORT_WIFI_DATOS_SESION_CLIENTE_WIFI' and aud_fec_ins <= '2021-06-09 08:55:26.000';

ROLLBACK;
