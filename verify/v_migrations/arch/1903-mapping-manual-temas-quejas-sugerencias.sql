-- Verify postgis-vlci-vlci2:v_migrations/1903-mapping-manual-temas-quejas-sugerencias on pg

BEGIN;

select 1/case when count(*) = 0 then 1 else 0 end from vlci2.t_datos_etl_quejas_sugerencias where tema not in (
'Atención personal municipal',
'N/A',
'Vía Pública actividades molestas',
'No clasificados',
'Discrepancias con actuaciones municipales',
'Tramitacion administrativa, tributos y sanciones',
'Seguridad ciudadana',
'Organismos autónomos',
'Señalización viaria',
'Vía Pública jardinería',
'Vía Pública limpieza',
'Mejora de la ciudad',
'Via publica reparacion de deficiencias'
);

select 1/case when count(*) = 0 then 1 else 0 end from vlci2.t_datos_etl_quejas_sugerencias where aud_user_upd != 'Iván 1903';

ROLLBACK;
