-- Verify postgis-vlci-vlci2:v_migrations/2677.create-table-unidades-admin-areas on pg

BEGIN;

SELECT 1 / case when count(*) = 0 then 1 else 0 end
    FROM information_schema.tables 
WHERE table_name = 't_datos_etl_contratos_menores_bdo';

SELECT 1 / case when count(*) = 1 then 1 else 0 end
    FROM information_schema.tables 
WHERE table_name = 't_datos_etl_unidades_administrativas_areas';

ROLLBACK;
