-- Verify postgis-vlci-vlci2:v_migrations/65233.insert-unidades-medida on pg

BEGIN;

select 1/count(*) from vlci2.t_m_unidad_medida WHERE pk_unidad_medida = 146;
select 1/count(*) from vlci2.t_m_unidad_medida WHERE pk_unidad_medida = 147;
select 1/count(*) from vlci2.t_m_unidad_medida WHERE pk_unidad_medida = 148;

ROLLBACK;
