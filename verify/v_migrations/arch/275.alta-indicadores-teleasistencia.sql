-- Verify postgis-vlci-vlci2:v_migrations/275.alta-indicadores-teleasistencia on pg

BEGIN;

select 1 / (case count(*) when 900 then 1 else 0 end)
from vlci2.t_d_indicador
where fe_fecha_creacion < '2022-04-12';

ROLLBACK;
