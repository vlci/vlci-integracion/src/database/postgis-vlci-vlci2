-- Verify postgis-vlci-vlci2:v_migrations/376.eliminacion-columnas-metadatos on pg

BEGIN;

    SELECT 1/case when count(*)::integer=0 then 1 else 0 end
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME in ('t_f_text_kpi_precipitaciones_diarias',
                         't_f_text_kpi_precipitaciones_mensuales',
                         't_f_text_kpi_precipitaciones_anuales',
                         't_f_text_kpi_temperatura_media_diaria',
                         't_f_text_kpi_temperatura_minima_diaria',
                         't_f_text_kpi_temperatura_maxima_diaria',
                         't_f_text_cb_weatherobserved',
                         't_datos_cb_medioambiente_poi',
                         't_datos_cb_medioambiente_airqualityobserved',
                         't_f_text_cb_kpi_consumo_agua',
                         't_datos_cb_emt_kpi',
                         't_f_text_cb_kpi_residuos',
                         't_f_text_cb_sonometros_ruzafa_daily',
                         't_f_text_kpi_trafico_y_bicis',
                         't_datos_cb_parkingspot',
                         't_f_text_cb_wifi_kpi_conexiones',
                         't_f_text_cb_wifi_kpi_urbo') and column_name like '%_md';

ROLLBACK;
