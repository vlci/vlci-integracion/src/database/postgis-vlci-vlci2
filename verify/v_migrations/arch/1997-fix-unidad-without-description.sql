-- Verify postgis-vlci-vlci2:v_migrations/1997-fix-unidad-without-description on pg

BEGIN;

select 1 / case when count(*) = 11 then 1 else 0 end
FROM 
    information_schema.columns
WHERE 
	table_schema = 'vlci2'
    and table_name in ('t_d_servicio', 't_d_delegacion', 't_d_area', 't_d_servicio_arch', 't_d_delegacion_arch', 't_d_area_arch') 
    AND column_name in ('desc_corta_cas', 'desc_corta_val') 
    and character_maximum_length = 120
group by character_maximum_length;

ROLLBACK;
