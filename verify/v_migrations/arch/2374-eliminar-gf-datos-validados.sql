-- Verify postgis-vlci-vlci2:v_migrations/2374-eliminar-gf-datos-validados on pg

BEGIN;

SELECT 1/
  CASE 
    WHEN COUNT(*) = 0 THEN 1 
    ELSE 0 
  END FROM vlci2.t_d_fecha_negocio_etls WHERE etl_nombre like ('%10m_%') or etl_nombre like ('%60m_%');

  SELECT 1/
  CASE 
    WHEN COUNT(*) = 97 THEN 1 
    ELSE 0 
  END FROM vlci2.t_d_fecha_negocio_etls;

ROLLBACK;
