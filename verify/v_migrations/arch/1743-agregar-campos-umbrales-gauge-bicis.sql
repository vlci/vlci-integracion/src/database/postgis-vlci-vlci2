-- Verify postgis-vlci-vlci2:v_migrations/1743-agregar-campos-umbrales-gauge-bicis on pg

BEGIN;

SELECT 1/case when count(*) = 3 then 1 else 0 end
FROM information_schema.columns
WHERE table_name = 't_datos_cb_trafico_cuenta_bicis_lastdata'
      AND column_name IN ('lowerthresholdrt', 'middlethresholdrt', 'upperthresholdrt');

ROLLBACK;
