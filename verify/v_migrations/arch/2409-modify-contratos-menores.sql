-- Verify postgis-vlci-vlci2:v_migrations/2409-modify-contratos-menores on pg

DO $$
DECLARE
    indexes integer;
    default_columns integer;
BEGIN

    indexes := (
        SELECT count(indexname)
        FROM pg_indexes
        WHERE tablename = 't_datos_etl_contratos_menores' AND indexname = 'unique_contrato');

   default_columns := (
        SELECT count(column_default)
        FROM information_schema.columns
        WHERE table_schema = 'vlci2' AND table_name = 't_datos_etl_contratos_menores' AND column_name = 'fecha_carga');

   ASSERT indexes = 1;
   ASSERT default_columns = 1;

END $$;