-- Verify postgis-vlci-vlci2:v_migrations/197.establecer_pk_tablas_cdmciutat on pg

BEGIN;

-- EMT
select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_datos_cb_emt_kpi';

-- PK trafico y bicis
select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_kpi_trafico_y_bicis
WHERE entityid = 'Kpi.pruebamig';

select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_f_text_kpi_trafico_y_bicis';

select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_datos_cb_trafico_pm_diaria';

-- PK agua
select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_kpi_consumo_agua
WHERE calculationperiod is null;

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_kpi_consumo_agua
WHERE calculationperiod >= '2023-04-13' AND calculationperiod <='2023-05-07' AND kpiValue = '0.0000';

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_kpi_consumo_agua
WHERE calculationperiod = '2023-05-20' AND kpiValue = '0.0000';

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_kpi_consumo_agua
WHERE calculationperiod = '2021-09-24' AND recvtime in ('2021-09-28 10:18:09.791 +0200', '2021-09-28 10:18:06.781 +0200');

select case when count(*)=3 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_kpi_consumo_agua
WHERE calculationperiod = '2021-06-11' 
AND entityid in ('kpi-benimamet-industrial','kpi-exposicio-domestico','kpi-exposicio-industrial');

select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_f_text_cb_kpi_consumo_agua';


-- PK residuos
select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_kpi_residuos
WHERE calculationperiod = '31-12-2020' AND sliceanddicevalue1 = 'Zona 3' AND sliceanddicevalue2  = 'Mobles' AND kpivalue != 5460;

select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_f_text_cb_kpi_residuos';

-- PK medioambiente
select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_datos_cb_medioambiente_airqualityobserved
WHERE operationalstatus is NULL;

select case when count(*)=8 then 1 else 1/(select 0) end as resultado  
from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved in ('2023-04-30 04:00:00','2023-04-30 14:00:00','2023-04-30 11:00:00','2023-04-30 05:00:00','2023-04-27 10:00:00','2023-04-30 13:00:00','2023-04-30 12:00:00','2023-04-30 07:00:00') 
and entityid = 'A10_OLIVERETA_60m';

select case when count(*)=1 then 1 else 1/(select 0) end as resultado  
from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved = '2023-04-28 18:00:00' and entityid = 'A07_VALENCIACENTRE_60m';

select case when count(*)=1 then 1 else 1/(select 0) end as resultado  
from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved = '2023-05-07 04:00:00' and entityid = 'A02_BULEVARDSUD_60m';

select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_datos_cb_medioambiente_airqualityobserved';


-- PK temperatura
select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_kpi_temperatura_media_diaria
WHERE calculationperiod >= '2021-07-01' AND calculationperiod <= '2021-08-30'  AND recvtime > '2021-08-30';

select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_f_text_kpi_temperatura_media_diaria';

-- PK sonometros
select case when count(*)=1 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_sonometros_ruzafa_daily
WHERE dateobserved = '2021-06-30'AND entityid  = 'T248655-daily';

select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_f_text_cb_sonometros_ruzafa_daily';

select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_datos_cb_sonometros_hopvlci_daily';


-- PK wifi t_f_text_cb_wifi_kpi_urbo

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_wifi_kpi_urbo
WHERE  entityid = 'OCI.DL.002' AND recvtime IN ('2020-07-24 06:53:16.200 +0200','2020-06-05 17:00:13.497 +0200');

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_wifi_kpi_urbo
WHERE entityid = 'OCI.DL.001' AND recvtime = '2020-06-05 17:00:13.891 +0200';


select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_f_text_cb_wifi_kpi_urbo';


-- PK Wifi t_f_text_cb_wifi_kpi_conexiones

SELECT count(*) FROM information_schema.tables 
WHERE table_name = 't_f_text_cb_wifi_kpi_conexiones_backup';

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_wifi_kpi_conexiones
WHERE entityid = 'IS.OCI.007' AND recvtime = '2021-06-15 14:15:19.490 +0200' AND sliceanddicevalue1 = 'Wifi Valencia';

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_wifi_kpi_conexiones
WHERE entityid = 'IS.OCI.005.a1' AND recvtime = '2021-03-11 07:16:35.264 +0100' AND sliceanddicevalue1 = 'AVC';

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_wifi_kpi_conexiones
WHERE entityid = 'IS.OCI.005.a1' AND recvtime = '2021-03-11 07:16:35.698 +0100' AND sliceanddicevalue1 = 'wifivalencia';

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_wifi_kpi_conexiones
WHERE entityid = 'IS.OCI.007' AND recvtime = '2021-02-09 10:14:07.400 +0100' AND sliceanddicevalue1 = 'AVC';

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_wifi_kpi_conexiones
WHERE entityid = 'IS.OCI.007' AND recvtime = '2021-02-09 10:10:46.926 +0100' 
AND sliceanddicevalue1 = 'Wifi Valencia';

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_wifi_kpi_conexiones
WHERE entityid in ('KPI_Wifi_AVC_01','KPI_Wifi_AVC_02','KPI_Wifi_AVC_03','KPI_Wifi_AVC_04','KPI_Wifi_AVC_05',
'KPI_Wifi_AVC_06','KPI_Wifi_AVC_07','KPI_Wifi_AVC_08','KPI_Wifi_AVC_09','KPI_Wifi_AVC_10','KPI_Wifi_AVC_11',
'KPI_Wifi_AVC_12','KPI_Wifi_Valencia_01','KPI_Wifi_Valencia_02','KPI_Wifi_Valencia_03','KPI_Wifi_Valencia_04',
'KPI_Wifi_Valencia_05','KPI_Wifi_Valencia_06','KPI_Wifi_Valencia_07','KPI_Wifi_Valencia_08','KPI_Wifi_Valencia_09',
'KPI_Wifi_Valencia_10','KPI_Wifi_Valencia_11','KPI_Wifi_Valencia_12')
AND calculationperiod  = '08/02/2021'
AND recvtime < '2021-02-09 11:10:46.926 +0100';

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_wifi_kpi_conexiones
where sliceanddicevalue2 is null;

select case when count(*)=0 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_wifi_kpi_conexiones
where sliceanddicevalue1 is null;

select 1/count(*)
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu 
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
and kcu.table_name = 't_f_text_cb_wifi_kpi_conexiones';


ROLLBACK;
