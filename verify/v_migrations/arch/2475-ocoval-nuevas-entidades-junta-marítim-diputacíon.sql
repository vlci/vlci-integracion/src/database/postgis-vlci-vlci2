-- Verify postgis-vlci-vlci2:2475-ocoval-nuevas-entidades-junta-marítim-diputacíon on pg

BEGIN;

select 1/case when count(*) = 72 then 1 else 0 end from vlci2.vlci_ocoval_entidades;

ROLLBACK;
