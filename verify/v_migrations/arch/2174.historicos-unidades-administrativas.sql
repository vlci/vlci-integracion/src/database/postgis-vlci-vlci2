-- Verify postgis-vlci-vlci2:v_migrations/2174.historicos-unidades-administrativas on pg

BEGIN;
/*
Originalmente existina 89 filas en la tabla t_datos_etl_unidades_administrativas_areas correspondientes a abril de 2024
Al añadir los historicos desde enero de 2017 a marzo de 2024, entre ese periodo hay 87 meses.
87 meses * 89 datos = 7743 inserciones.
Total hasta abril 2024 = 7743 + 89 = 7832 filas.
*/
SELECT 
    1 / CASE 
            WHEN COUNT(*) = 7832 THEN 1 
            ELSE 0 
        END
FROM 
    vlci2.t_datos_etl_unidades_administrativas_areas
WHERE 
    TO_DATE(fecha_negocio, 'YYYYMM') < '2024-05-01'::date;


ROLLBACK;
