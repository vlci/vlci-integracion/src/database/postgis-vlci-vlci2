-- Verify postgis-vlci-vlci2:v_migrations/1203.Eliminar_datos_duplicados_cdmciutat_contaminacion on pg

BEGIN;

-- XXX Add verifications here.

select 1/(case count(*) when 0 then 1 else 0 end)   
	from vlci2.vw_datos_cb_cdmciutat_contaminantes c1 
        where (select count(*) from vlci2.vw_datos_cb_cdmciutat_contaminantes c2
                 where fecha= c1.fecha and estacion = c1.estacion) > 1;
ROLLBACK;

