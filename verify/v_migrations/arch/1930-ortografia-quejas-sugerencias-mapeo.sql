-- Verify postgis-vlci-vlci2:v_migrations/1930-ortografia-quejas-sugerencias-mapeo on pg

BEGIN;

select 1 / (case count(*) when 0 then 1 else 0 end)
from vlci2.t_ref_etl_quejas_sugerencias_mapeo_temas
WHERE tema_nuevo='Tramitacion administrativa, tributos y sanciones';

select 1 / (case count(*) when 0 then 1 else 0 end)
from vlci2.t_ref_etl_quejas_sugerencias_mapeo_temas
WHERE tema_nuevo='Via publica reparacion de deficiencias';

select 1 / (case count(*) when 133 then 1 else 0 end)
from vlci2.t_ref_etl_quejas_sugerencias_mapeo_temas;

ROLLBACK;
