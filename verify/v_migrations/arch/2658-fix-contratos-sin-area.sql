-- Verify postgis-vlci-vlci2:v_migrations/2658-fix-contratos-sin-area on pg


DO $$
DECLARE
    rows_without_area integer;
    rows_without_length integer;
    rows_without_code integer;
BEGIN

    rows_without_area := (select count(*) from vlci2.t_datos_etl_contratos_menores where area is null);
    rows_without_length := (select count(*) from vlci2.t_datos_etl_contratos_menores where duracion_contrato_meses is null);
    rows_without_code := (select count(*) from vlci2.t_datos_etl_contratos_menores where area_cod_org is null);

    ASSERT rows_without_area = 0;
    ASSERT rows_without_length = 0;
    ASSERT rows_without_code = 0;
END $$;