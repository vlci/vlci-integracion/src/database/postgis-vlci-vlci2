-- Verify postgis-vlci-vlci2:v_migrations/2662-add-area-fk-t-d-servicio on pg

BEGIN;

SELECT 1 / case when count(*) = 1 then 1 else 0 end
    FROM information_schema.columns 
WHERE table_schema='vlci2' AND table_name='t_d_servicio' AND column_name='fk_area_id';


ROLLBACK;
