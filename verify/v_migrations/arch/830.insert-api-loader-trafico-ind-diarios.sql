-- Verify postgis-vlci-vlci2:v_migrations/830.insert-api-loader-trafico-ind-diarios on pg

DO $$
DECLARE

	inserts_tramos_id_descripcion integer;

BEGIN

	SELECT count(*) FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api = 'Geoportal_tramos_id_descripcion'
    INTO inserts_tramos_id_descripcion;
    assert inserts_tramos_id_descripcion = 5;

END
$$ LANGUAGE PLPGSQL;
