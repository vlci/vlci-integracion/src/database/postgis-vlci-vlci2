-- Verify vlci-postgis:67845.ocoval-nuevaEntidad-iberdrola-consell on pg

BEGIN;

select 1/count(*) from vlci2.vlci_ocoval_entidades where compania='Compañías de Servicios' and entidad='Iberdrola Telecomunicaciones';
select 1/count(*) from vlci2.vlci_ocoval_entidades where compania='Otros' and entidad='Consell Agrari';
select case when count(*)=66 then 1 else 1/(select 0) end as resultado from vlci2.vlci_ocoval_entidades;

select 1/count(*) from vlci2.vlci_ocoval_companyia where compania='IBERDROLA' and entidad='Iberdrola Telecomunicaciones';
select case when count(*)=15 then 1 else 1/(select 0) end as resultado from vlci2.vlci_ocoval_companyia;

ROLLBACK;
