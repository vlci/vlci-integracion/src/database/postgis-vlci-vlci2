-- Verify postgis-vlci-vlci2:v_migrations/2425.corregir-datos-anomalos-emt on pg

BEGIN;

SELECT 1 / case when count(*) = 0 THEN 1 ELSE 0 END
    FROM information_schema.tables 
WHERE table_name = 'carga_emt_csv';

SELECT 1 / CASE WHEN COUNT(*) = 64 THEN 1 ELSE 0 END
FROM vlci2.vlci_emt_routes;

SELECT 1 / CASE WHEN total = 8045140 THEN 1 ELSE 0 END
FROM (
     SELECT
     sum(kpivalue) as total
     FROM vlci2.t_datos_cb_emt_kpi emt
     WHERE emt.recvtime = '2024-02-12 11:35:00.000'
) as total_viajeros;

ROLLBACK;
