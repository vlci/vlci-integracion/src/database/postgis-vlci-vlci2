-- Verify postgis-vlci-vlci2:v_migrations/1913.insert-tablas-referencia-inco on pg

BEGIN;


select 1 / case when count(*) = 9 then 1 else 0 end from vlci2.t_ref_etl_inco_capitulos;
select 1 / case when count(*) = 211 then 1 else 0 end from vlci2.t_ref_etl_inco_conceptos;

ROLLBACK;
