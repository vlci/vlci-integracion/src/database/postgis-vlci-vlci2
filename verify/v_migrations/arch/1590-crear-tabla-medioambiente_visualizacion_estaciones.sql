-- Verify postgis-vlci-vlci2:r_env_tables/tb.t_env_medioambiente_visualizacion_estaciones on pg

BEGIN;

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 11) THEN 1
        ELSE 0
    END as outvalue
from vlci2.t_ref_medioambiente_visualizacion_estaciones
) verify;

ROLLBACK;
