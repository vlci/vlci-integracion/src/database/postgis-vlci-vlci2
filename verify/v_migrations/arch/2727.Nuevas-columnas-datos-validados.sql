-- Verify postgis-vlci-vlci2:v_migrations/2727.Nuevas-columnas-datos-validados on pg

BEGIN;

SELECT 1/count(*)
    FROM information_schema.tables 
WHERE table_name = 't_datos_cb_trafico_kpi_validados_mes';

SELECT
    originalEntityId,
    originalEntityType,
    originalServicePath
from
    vlci2.t_datos_cb_trafico_kpi_validados_mes;

SELECT 1/count(*)
    FROM information_schema.tables 
WHERE table_name = 't_datos_cb_trafico_kpi_validados_anyo';

SELECT
    originalEntityId,
    originalEntityType,
    originalServicePath
from
    vlci2.t_datos_cb_trafico_kpi_validados_anyo;

ROLLBACK;
