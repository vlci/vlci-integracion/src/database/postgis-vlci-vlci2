-- Verify postgis-vlci-vlci2:v_migrations/1435-insert-values-mes-anyo-t-f-indicador-valor on pg
-- Comprueba que no quedan filas con mes o año vacio de los indicadores que se desean actualizar. 
BEGIN;

select 1/(case count(*) when 0 then 1 else 0 end)   
	from vlci2.t_f_indicador_valor 
    where fk_indicador in (1139, 1130, 1111, 1122, 1124, 1143) 
        and (mes is null or anyo is null) 
        and fk_periodicidad_muestreo = 6;;

ROLLBACK;
