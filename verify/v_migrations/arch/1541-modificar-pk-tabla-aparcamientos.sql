-- Verify postgis-vlci-vlci2:v_migrations/1541-modificar-pk-tabla-aparcamientos on pg

BEGIN;

SELECT
  1/(1-count(*))
FROM 
  information_schema.table_constraints tc 
  JOIN information_schema.key_column_usage kcu
    ON tc.constraint_name = kcu.constraint_name
WHERE 
  tc.table_name = 't_datos_cb_trafico_aparcamientos' AND 
  tc.constraint_type = 'PRIMARY KEY';

ROLLBACK;
