-- Verify postgis-vlci-vlci2:v_migrations/1526.restaurar-tabla-agregados-periodicidades on pg

BEGIN;

SELECT fk_indicador, fk_periodicidad, fk_indicador_agregado
  FROM vlci2.t_x_agregados_periodicidades
 WHERE FALSE;

ROLLBACK;
