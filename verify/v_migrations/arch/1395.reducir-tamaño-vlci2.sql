-- Verify postgis-vlci-vlci2:v_migrations/1395.reducir-tamaño-vlci2 on pg

BEGIN;

SELECT pg_catalog.has_schema_privilege('vlci2_arch', 'usage');

SELECT 
    fk_indicador,
	fk_unidad_medida,
	fk_periodicidad_muestreo,
	dc_periodicidad_muestreo_valor,
	nu_indicador_valor,
	dc_indicador_valor,
	dc_criterio_desagregacion1_valor,
	dc_criterio_desagregacion2_valor,
	dc_criterio_desagregacion3_valor,
	dc_criterio_desagregacion4_valor,
	nu_latitud,
	nu_longitud,
	fk_barrio,
	fe_fecha_carga,
	etl_nombre
  FROM vlci2_arch.t_f_indicador_valor_datos_deprecados
 WHERE FALSE;

ROLLBACK;
