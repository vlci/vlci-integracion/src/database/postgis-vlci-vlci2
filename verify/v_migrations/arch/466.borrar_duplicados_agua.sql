-- Verify postgis-vlci-vlci2:v_migrations/466.borrar_duplicados_agua on pg

BEGIN;

SELECT 1 / (CASE WHEN count(*) = 0 THEN 1 ELSE 0 END)
FROM t_f_text_cb_kpi_consumo_agua 
WHERE calculationperiod in ('2022-05-09', '2022-05-10', '2022-05-11', '2022-05-12', '2022-05-13', '2022-05-14', '2022-05-15', '2022-05-16', '2022-05-17', '2022-05-18')
GROUP BY calculationperiod, entityid
HAVING count(recvtime) > 1;

ROLLBACK;
