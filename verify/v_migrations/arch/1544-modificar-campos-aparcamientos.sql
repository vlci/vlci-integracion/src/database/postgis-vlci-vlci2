-- Verify postgis-vlci-vlci2:v_migrations/1544-modificar-campos-aparcamientos on pg

BEGIN;

SELECT 1/count(*)
FROM information_schema.columns
WHERE table_name = 't_datos_cb_trafico_aparcamientos'
AND column_name = 'recvtime'
AND data_type = 'timestamp without time zone';

ROLLBACK;

BEGIN;

SELECT 1/count(*)
FROM information_schema.columns
WHERE table_name = 't_datos_cb_trafico_aparcamientos_lastdata'
AND column_name = 'recvtime'
AND data_type = 'timestamp without time zone';

ROLLBACK;