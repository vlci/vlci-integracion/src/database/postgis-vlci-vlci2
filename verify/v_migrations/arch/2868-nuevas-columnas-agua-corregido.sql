-- verify/v_migrations/2868-nuevas-columnas-agua-corregido.sql
BEGIN;


SELECT 1/count(*)
    FROM information_schema.tables 
WHERE table_name = 't_datos_cb_cia_inyeccion_agua_corregida';

SELECT
    originalEntityId,
    originalEntityType,
    originalServicePath
from
    vlci2.t_datos_cb_cia_inyeccion_agua_corregida;

ROLLBACK;
