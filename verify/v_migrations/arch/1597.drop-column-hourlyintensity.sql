-- Verify postgis-vlci-vlci2:v_migrations/1597.drop-column-hourlyintensity on pg

BEGIN;

SELECT 1 / (case count(*) when 0 then 1 else 0 end) FROM information_schema.tables WHERE table_name = 'vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata';


ROLLBACK;
