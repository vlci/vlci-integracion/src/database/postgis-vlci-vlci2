-- Verify postgis-vlci-vlci2:v_migrations/566.cambiar_periodicidad_isbso04-05 on pg

BEGIN;

select 1/count(*) from vlci2.t_d_indicador where dc_identificador_indicador='IS.BSO.04a';
select 1/count(*) from vlci2.t_d_indicador where dc_identificador_indicador='IS.BSO.05a';

ROLLBACK;
