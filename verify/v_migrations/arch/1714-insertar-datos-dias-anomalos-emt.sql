-- Verify postgis-vlci-vlci2:v_migrations/1714-insertar-datos-dias-anomalos-emt on pg

BEGIN;

select 1/case when count(*) > 0 then 1 else 0 end from vlci2.t_datos_cb_emt_kpi  eak
where
	sliceanddice1 = 'Titulo' 
	and calculationperiod in ('2023/05/28', '2023/05/23', '2023/05/21', '2023/05/20', '2023/05/19'); 

SELECT 1 / CASE WHEN COUNT(*) > 290616 THEN 1 ELSE 0 END
FROM vlci2.t_datos_cb_emt_kpi;

ROLLBACK;
