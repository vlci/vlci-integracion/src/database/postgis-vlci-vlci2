-- Verify postgis-vlci-vlci2:v_migrations/2677.cambiar-tipo-dato-fecha-negocio on pg

BEGIN;

SELECT
    CASE
        WHEN COUNT(*) = 1 THEN 1
        ELSE 0
    END AS all_changes_applied
FROM
    information_schema.columns
WHERE 
	table_schema = 'vlci2' and
    table_name = 't_datos_etl_unidades_administrativas_areas'
    AND (column_name = 'fecha_negocio' AND data_type = 'varchar');

ROLLBACK;
