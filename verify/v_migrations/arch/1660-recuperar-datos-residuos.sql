-- Verify postgis-vlci-vlci2:v_migrations/1660-recuperar-datos-residuos on pg

BEGIN;

select case when count(*) >= 12652 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_cb_kpi_residuos
where TO_DATE(calculationperiod ,'DD-MM-YYYY') >= '2022-01-01';

ROLLBACK;
