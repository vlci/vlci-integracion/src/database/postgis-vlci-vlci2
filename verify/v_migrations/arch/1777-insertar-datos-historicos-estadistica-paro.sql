-- Verify postgis-vlci-vlci2:v_migrations/1777-insertar-datos-historicos-estadistica-paro on pg

BEGIN;

select 1 / case when count(*) = 28 then 1 else 0 end from vlci2.t_datos_cb_estadistica_paro;

ROLLBACK;
