-- Verify postgis-vlci-vlci2:v_migrations/TLF008-modificar_tipo_noiselevelobserved_lastdata on pg

BEGIN;

select 1/count(*) from vlci2.t_datos_cb_noiselevelobserved_lastdata tdcnl2 where pg_typeof(recvtime)::text='timestamp with time zone';

ROLLBACK;
