-- Verify postgis-vlci-vlci2:v_migrations/1431-update-t-d-delegacion on pg

BEGIN;

select case when count(*)=49 then 1 else 1/(select 0) end as resultado 
from vlci2.t_d_delegacion
where pk_delegacion_id not in('001', '999', '260', '290', '291', '292', '293', '294', '295');

ROLLBACK;
