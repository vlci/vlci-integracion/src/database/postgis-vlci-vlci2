-- Verify postgis-vlci-vlci2:v_migrations/2340.eliminar-tablas-lastdata on pg

BEGIN;

SELECT 1 / case when count(*) = 0 then 1 else 0 end
    FROM information_schema.tables
WHERE table_name = 't_datos_cb_airqualityobserved_lastdata_va';

SELECT 1 / case when count(*) = 0 then 1 else 0 end
    FROM information_schema.tables
WHERE table_name = 't_datos_cb_airqualityobserved_lastdata_vm';

ROLLBACK;
