-- Verify postgis-vlci-vlci2:v_migrations/TLF030-eliminar-residuos-anomalos-lastdata on pg

BEGIN;

SELECT 1/
  CASE 
    WHEN COUNT(*) = 535 THEN 1 
    ELSE 0 
  END
FROM t_datos_cb_wastecontainer_lastdata;

ROLLBACK;
