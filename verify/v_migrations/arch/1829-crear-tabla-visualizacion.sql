-- Verify postgis-vlci-vlci2:v_migrations/1829-crear-tabla-visualizacion on pg

BEGIN;

SELECT 1/count(*)
    FROM information_schema.tables 
WHERE table_name = 't_ref_horizontal_visualizacion';

ROLLBACK;
