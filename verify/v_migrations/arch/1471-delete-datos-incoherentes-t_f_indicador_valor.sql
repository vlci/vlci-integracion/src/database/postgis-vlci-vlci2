-- Verify postgis-vlci-vlci2:v_migrations/1472-delete-datos-incoherentes-t_f_indicador_valor on pg

BEGIN;

select 1/(case count(*) when 0 then 1 else 0 end)
from vlci2.t_f_indicador_valor a11
where fk_indicador in (1122,1124,1130,1138,1143) 
	and a11.fk_periodicidad_muestreo  = 6
	and to_date(a11.dc_periodicidad_muestreo_valor , 'dd/mm/yyyy') < '01/01/2017';

ROLLBACK;
