-- Verify postgis-vlci-vlci2:v_migrations/1453-housekeeping-parking-forecast on pg

BEGIN;

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 17) THEN 1
        ELSE 0
    END as outvalue
from vlci2.housekeeping_config
) verify;

ROLLBACK;
