-- Verify postgis-vlci-vlci2:v_migrations/2376-insert-historicos-medioambiente-va on pg

BEGIN;

SELECT
    1 / CASE
        WHEN count(*) = (select count(*) from vlci2.t_datos_cb_medioambiente_airqualityobserved where dateobserved < '2020-03-02 00:00:00') THEN 1
        ELSE 0
    END
FROM vlci2.t_datos_cb_medioambiente_airqualityobserved_va;

ROLLBACK;
