-- Verify postgis-vlci-vlci2:v_migrations/TLF037-nuevos_campos_offstreetparking_lastdata on pg

BEGIN;

SELECT address, project, owner, owneremail, respocitecnicoemail, timeinstant, location
FROM vlci2.t_datos_cb_trafico_aparcamientos_lastdata;

ROLLBACK;
