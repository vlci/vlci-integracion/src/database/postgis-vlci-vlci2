-- Verify postgis-vlci-vlci2:v_migrations/TLF018-eliminar_vm_emt on pg

BEGIN;

SELECT NOT EXISTS (SELECT 1 FROM pg_matviews WHERE matviewname = 'vw_t_datos_cb_emt_kpi_materialized');

ROLLBACK;
