-- Verify postgis-vlci-vlci2:v_migrations/1541-crear-tabla-parkings on pg

BEGIN;

  SELECT count(*) 
    FROM information_schema.tables 
  WHERE table_name = 't_datos_cb_trafico_aparcamientos_lastdata';

 SELECT 1/count(*) 
    FROM vlci2.t_datos_cb_trafico_aparcamientos_lastdata;

ROLLBACK;
