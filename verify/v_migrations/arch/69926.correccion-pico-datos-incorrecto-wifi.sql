-- Verify postgis-vlci-vlci2:v_migrations/69926.correccion-pico-datos-incorrecto-wifi on pg

BEGIN;

select
	1 /
	(case count(*)
   when 0 then 1
	else 0
end)
from
	t_f_text_cb_wifi_kpi_conexiones
where
	name = 'Usuarios conectados diariamente a la red WIFI'
	and calculationperiod = '17/01/2022'
	and recvtime >= '2022-01-20';
;


select
	1 / count(*)
from
	t_f_text_cb_wifi_kpi_conexiones
where
	name = 'Usuarios conectados diariamente a la red WIFI'
	and calculationperiod = '17/01/2022'
	and recvtime <= '2022-01-20';

ROLLBACK;
