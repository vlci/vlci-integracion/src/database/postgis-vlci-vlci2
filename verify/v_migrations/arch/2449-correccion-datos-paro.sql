-- Verify postgis-vlci-vlci2:v_migrations/2449-correccion-datos-paro on pg
DO $$
DECLARE
    count_result integer;
    find_data_modified integer;
BEGIN

count_result := (select count(*) from vlci2.t_datos_cb_estadistica_paro where recvtime <= '2024-02-01 12:15:07.476');
find_data_modified := (select count(*) from vlci2.t_datos_cb_estadistica_paro WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-12' AND sliceanddicevalue1='N/A' AND originalentityid='Tasa-Paro-Trimestral');

ASSERT count_result = 46;
ASSERT find_data_modified = 1;

ROLLBACK;
END $$;