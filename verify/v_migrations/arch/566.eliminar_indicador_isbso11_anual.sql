-- Verify postgis-vlci-vlci2:v_migrations/566.eliminar_indicador_isbso11_anual on pg

BEGIN;

select 1 / (case count(*) when 913 then 1 else 0 end)
from vlci2.t_d_indicador;

ROLLBACK;
