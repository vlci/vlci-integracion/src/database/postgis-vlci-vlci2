-- Verify postgis-vlci-vlci2:v_migrations/1691-crear-tabla-t_datos_cb_estadistica_paro on pg

BEGIN;

   SELECT 1/count(*)
   FROM   information_schema.tables
   WHERE  table_name = 't_datos_cb_estadistica_paro';

ROLLBACK;
