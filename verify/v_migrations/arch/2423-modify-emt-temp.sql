-- Verify postgis-vlci-vlci2:v_migrations/2423-modify-emt-temp on pg

DO $$
DECLARE
    linea_column integer;
BEGIN

linea_column:= (select count(*) 
                    from information_schema.columns c
                    where c.table_schema = 'vlci2'
                    and c.table_name = 'vlci2.vlci_emt_tmp'
                    and c.column_name = 'linea');

ASSERT linea_column = 0;

END $$;
