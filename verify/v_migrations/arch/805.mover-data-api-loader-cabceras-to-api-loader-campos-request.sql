-- Verify postgis-vlci-vlci2:v_migrations/805.mover-data-api-loader-cabceras-to-api-loader-campos-request on pg

BEGIN;

DO $$
DECLARE
    entradas_cabeceras integer;
    entradas_campos_request integer;
BEGIN
    entradas_cabeceras := (SELECT COUNT(*) FROM vlci2.t_d_api_loader_cabeceras);
    ASSERT entradas_cabeceras = 0;

    entradas_campos_request := (SELECT COUNT(*) FROM vlci2.t_d_api_loader_campos_request);
    ASSERT entradas_campos_request = 6;
END $$;

ROLLBACK;
