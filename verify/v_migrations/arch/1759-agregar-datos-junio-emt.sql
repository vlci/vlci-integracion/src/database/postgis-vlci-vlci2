-- Verify postgis-vlci-vlci2:v_migrations/1759-agregar-datos-junio-emt on pg

BEGIN;

select 1/case when count(*) = 241 then 1 else 0 end from vlci2.t_datos_cb_emt_kpi
    where calculationperiod in ('2023-06-09', '2023-06-10', '2023-06-11')
        and sliceanddice1 in('Titulo', 'Ruta');

select 1/case when count(*) >= 292610 then 1 else 0 end from vlci2.t_datos_cb_emt_kpi;

ROLLBACK;
