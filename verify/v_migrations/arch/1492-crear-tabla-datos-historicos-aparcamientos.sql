-- Verify postgis-vlci-vlci2:v_migrations/1492-crear-tabla-datos-historicos-aparcamientos on pg

BEGIN;

SELECT count(*) 
    FROM information_schema.tables 
WHERE table_name = 't_datos_cb_trafico_aparcamientos';

ROLLBACK;
