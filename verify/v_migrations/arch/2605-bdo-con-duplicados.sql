-- Verify postgis-vlci-vlci2:v_migrations/2605-bdo-con-duplicados on pg

BEGIN;

-- AREAS.
    select case 
    when count(tmp.pk_area_id) > 0 then false
    else true
    end
    from (
        select pk_area_id
        from vlci2.t_d_area
        group by pk_area_id
        having count(pk_area_id) > 1) as tmp;

-- DELEGACIONES.
    select case 
    when count(tmp.pk_delegacion_id) > 0 then false
    else true
    end
    from (
        select pk_delegacion_id
        from vlci2.t_d_delegacion
        group by pk_delegacion_id
        having count(pk_delegacion_id) > 1) as tmp;

-- SERVICIOS.
    select case 
    when count(tmp.codigo_organico) > 0 then false
    else true
    end
    from (
        select codigo_organico
        from vlci2.t_d_servicio
        group by codigo_organico
        having count(codigo_organico) > 1) as tmp;

ROLLBACK;
