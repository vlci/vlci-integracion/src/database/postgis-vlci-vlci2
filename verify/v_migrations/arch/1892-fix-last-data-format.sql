-- Verify postgis-vlci-vlci2:v_migrations/1892-fix-last-data-format on pg

BEGIN;

SELECT 1/count(*) 
    FROM
    information_schema.columns
where
	table_schema = 'vlci2' and
    table_name = 't_datos_cb_trafico_cuenta_bicis_lastdata' AND
    column_name = 'dateobserved' and
    data_type = 'timestamp with time zone';

SELECT 1/count(*) 
    FROM
    information_schema.columns
where
	table_schema = 'vlci2' and
    table_name = 't_datos_cb_trafico_cuenta_bicis_lastdata' AND
    column_name = 'dateobservedfrom' and
    data_type = 'timestamp with time zone';

SELECT 1/count(*) 
    FROM
    information_schema.columns
where
	table_schema = 'vlci2' and
    table_name = 't_datos_cb_trafico_cuenta_bicis_lastdata' AND
    column_name = 'dateobservedto' and
    data_type = 'timestamp with time zone';

ROLLBACK;
