-- Verify postgis-vlci-vlci2:v_migrations/1240.historico_sonometros_hopu on pg

BEGIN;

select case when count(*)=1306 then 1 else 1/(select 0) end as resultado 
from vlci2.t_datos_cb_sonometros_hopvlci_daily 
where dateobserved < '2022-12-20';

ROLLBACK;
