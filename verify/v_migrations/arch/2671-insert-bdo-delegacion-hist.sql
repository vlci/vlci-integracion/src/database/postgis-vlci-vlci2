-- Verify postgis-vlci-vlci2:v_migrations/2671-insert-bdo-delegacion-hist on pg

DO $$
DECLARE
    insert_in_delegacion integer;
BEGIN

    insert_in_delegacion := (SELECT count(*) FROM vlci2.t_d_delegacion);

    ASSERT insert_in_delegacion = 7323;
END $$;

