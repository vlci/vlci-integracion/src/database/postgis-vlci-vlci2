-- Verify postgis-vlci-vlci2:v_migrations/TLF0036-eliminar_entidad_anomala_parkingspot on pg

BEGIN;

SELECT
    1 / CASE
        WHEN entityid='parkingspot-urb99999' THEN 0
        ELSE 1
    END
FROM
    vlci2.t_datos_cb_parkingspot_lastdata;

ROLLBACK;
