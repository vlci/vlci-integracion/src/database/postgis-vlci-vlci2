-- Verify postgis-vlci-vlci2:v_migrations/2572.Add_gestion_fechas_intensidad_trafico_validada on pg

BEGIN;

SELECT 1 / CASE WHEN COUNT(*) = 99 THEN 1 ELSE 0 END
FROM vlci2.t_d_fecha_negocio_etls;

ROLLBACK;
