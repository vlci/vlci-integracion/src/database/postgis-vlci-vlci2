-- Verify postgis-vlci-vlci2:v_migrations/1753.crear-tablas-bdo on pg
BEGIN;

-- Comprobamos la creacion de las tablas
SELECT 1/case when count(table_name) = 3 then 1 else 0 end
FROM 
    information_schema."tables"
WHERE 
    table_schema = 'vlci2' AND
	table_name in ('t_d_servicio', 't_d_delegacion', 't_d_area');

ROLLBACK;