-- Verify postgis-vlci-vlci2:v_migrations/1619.residuos-insert-datos-historicos on pg

BEGIN;

    select 1/verify.outvalue from (
    select
        CASE
            WHEN (count(*) = 2512) THEN 1
            ELSE 0
        END as outvalue
    from vlci2.t_ref_residuos_datos_mensuales_historicos
    ) verify;

ROLLBACK;
