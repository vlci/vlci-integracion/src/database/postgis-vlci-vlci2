-- Verify postgis-vlci-vlci2:v_migrations/1753.archivar-tablas-bdo on pg

BEGIN;

SELECT 1/case when count(table_name) = 3 then 1 else 0 end
FROM 
    information_schema."tables"
WHERE 
    table_schema = 'vlci2' AND
	table_name in ('t_d_servicio_arch', 't_d_delegacion_arch', 't_d_area_arch');

ROLLBACK;
