-- Verify postgis-vlci-vlci2:v_migrations/1822-gf-corregir-fecha-pmp-semestral on pg

BEGIN;

select 1/verify.outvalue from (
select
    CASE
        WHEN (count(*) = 72) THEN 1
        ELSE 0
    END as outvalue
from vlci2.t_d_fecha_negocio_etls
) verify;

ROLLBACK;
