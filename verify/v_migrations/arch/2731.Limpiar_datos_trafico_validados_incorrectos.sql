-- Verify postgis-vlci-vlci2:v_migrations/2731.Limpiar_datos_trafico_validados_incorrectos on pg

BEGIN;

select 1/case when count(*) = 734 then 1 else 0 end from vlci2.t_datos_cb_trafico_kpi_validados_anyo
where calculationperiod >= '2023-04-26' and calculationperiod <= '2023-05-28' ;

ROLLBACK;
