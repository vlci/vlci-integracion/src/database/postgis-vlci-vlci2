-- Verify postgis-vlci-vlci2:v_migrations/804.insert-valores-legacy-t-d-api-loader on pg
-- tarea PROY2100017N3-804
-- Comprueba que la cantidad de entradas con nombres que empiezan por airwave o jardineria coinciden con las que tienen el campo legacy con valor S 
-- y que no hay otras que no empiecen por el nombre correcto y tengan el valor legacy S 
DO $$
DECLARE
    apis_legacy integer;
    apis_legacy_field_legacy_true integer;
    apis_legacy_field_true integer;
BEGIN
   apis_legacy := (SELECT COUNT(*) FROM vlci2.t_d_api_loader WHERE nombre_api LIKE 'Airwave%' OR nombre_api LIKE 'Jardineria%');
   apis_legacy_field_legacy_true := (SELECT COUNT(*) FROM vlci2.t_d_api_loader WHERE nombre_api LIKE 'Airwave%' OR nombre_api LIKE 'Jardineria%' AND legacy = 'S');
   apis_legacy_field_true := (SELECT COUNT(*) FROM vlci2.t_d_api_loader WHERE legacy = 'S');
   ASSERT apis_legacy = apis_legacy_field_legacy_true AND apis_legacy = apis_legacy_field_true;
END $$;
