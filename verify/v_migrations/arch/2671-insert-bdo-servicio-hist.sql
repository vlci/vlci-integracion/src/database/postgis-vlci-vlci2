-- Verify postgis-vlci-vlci2:v_migrations/2671-insert-bdo-servicio-hist on pg

DO $$
DECLARE
    insert_in_servicio integer;
BEGIN

    insert_in_servicio := (SELECT count(*) FROM vlci2.t_d_servicio);

    ASSERT insert_in_servicio = 5943;
END $$;

