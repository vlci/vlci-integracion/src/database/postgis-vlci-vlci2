-- Verify postgis-vlci-vlci2:v_migrations/1759-eliminar-datos-anomalos-junio-emt on pg

BEGIN;

select 1/ (case when count(*) = 0 then 1 else 0 end)  from  vlci2.t_datos_cb_emt_kpi
    where calculationperiod in ('2023-06-09', '2023-06-10', '2023-06-11');

select 1/(case when count(*) >= 292600 then 1 else 0 end) from vlci2.t_datos_cb_emt_kpi

ROLLBACK;
