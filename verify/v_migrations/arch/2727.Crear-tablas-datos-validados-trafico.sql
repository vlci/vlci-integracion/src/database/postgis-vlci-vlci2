-- Verify postgis-vlci-vlci2:v_migrations/2727.Crear-tablas-datos-validados-trafico on pg

BEGIN;


SELECT 1/count(*)
    FROM information_schema.tables 
WHERE table_name = 't_datos_cb_trafico_kpi_validados_mes';

SELECT 1/count(*)
    FROM information_schema.tables 
WHERE table_name = 't_datos_cb_trafico_kpi_validados_anyo';


ROLLBACK;
