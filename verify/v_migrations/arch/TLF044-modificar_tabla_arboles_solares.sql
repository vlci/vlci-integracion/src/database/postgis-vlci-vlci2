-- Verify postgis-vlci-vlci2:v_migrations/TLF044-modificar_tabla_arboles_solares on pg
BEGIN;

SELECT
    1 / (
        CASE
            WHEN data_type = 'timestamp without time zone' THEN 1
            ELSE 0
        end
    )
FROM
    information_schema.columns
WHERE
    table_schema = 'vlci2'
    AND table_name = 't_datos_cb_generacion_energia_solartree'
    AND column_name = 'dateobserved';

SELECT
    1 / (
        CASE
            WHEN data_type = 'timestamp without time zone' THEN 1
            ELSE 0
        end
    )
FROM
    information_schema.columns
WHERE
    table_schema = 'vlci2'
    AND table_name = 't_datos_cb_generacion_energia_solartree_lastdata'
    AND column_name = 'dateobserved';

ROLLBACK;