-- Verify postgis-vlci-vlci2:v_migrations/1714-datos-anomalos-emt-vlci2 on pg

BEGIN;

select 1/(case when count(*) = 0 then 1 else 0 end) from
	vlci2.t_datos_cb_emt_kpi
where
	sliceanddice1 in('Titulo', 'Ruta')
	and calculationperiod in ('2023/05/28', '2023/05/23', '2023/05/21', '2023/05/20', '2023/05/19'); 

select 1/(case when count(*) >= 290000 then 1 else 0 end) from vlci2.t_datos_cb_emt_kpi;


ROLLBACK;
