-- Verify postgis-vlci-vlci2:v_migrations/1402-pmDiaria-gestion-fechas on pg

BEGIN;

select 1 / (case count(*) when 1 then 1 else 0 end)
from vlci2.t_d_fecha_negocio_etls
where etl_nombre = 'etl_cdmciutat_trafico_pm_diaria';

select 1 / (case count(*) when 1 then 1 else 0 end)
from vlci2.t_d_fecha_negocio_etls
where etl_nombre = 'etl_cdmciutat_bicicletas_pm_diaria';

select 1 / (case count(*) when 75 then 1 else 0 end)
from vlci2.t_d_fecha_negocio_etls

ROLLBACK;
