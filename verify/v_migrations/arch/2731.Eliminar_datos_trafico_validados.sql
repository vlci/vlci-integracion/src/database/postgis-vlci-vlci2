-- Verify postgis-vlci-vlci2:v_migrations/2731.Eliminar_datos_trafico_validados on pg

BEGIN;

select 1/case when count(*) = 0 then 1 else 0 end from vlci2.t_f_text_kpi_trafico_y_bicis
where entityid in ('Kpi-Valid-Accesos-Ciudad-Coches','Kpi-Valid-Accesos-Vias-Coches', 'Kpi-Valid-Trafico-Bicicletas-Accesos-Anillo', 'Kpi-Valid-Trafico-Bicicletas-Anillo');

ROLLBACK;
