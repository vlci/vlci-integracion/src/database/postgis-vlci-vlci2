-- Verify postgis-vlci-vlci2:v_migrations/1606-insert-datos-febrero-bicis on pg

BEGIN;

select case when count(*)=140 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_kpi_trafico_y_bicis
where entityid = 'Kpi-Trafico-Bicicletas-Anillo'
	and to_date(calculationperiod, 'YYYY-MM-DD') > '2020/02/02'
	and to_date(calculationperiod, 'YYYY-MM-DD') < '2020/03/02';

ROLLBACK;

