-- Verify postgis-vlci-vlci2:v_migrations/2384.error-datos-agua on pg

BEGIN;

select 1 / case when count(*) > 0 then 0 else 1 end
from
	t_f_text_cb_kpi_consumo_agua
where
	calculationperiod like '%/%/%';

ROLLBACK;
