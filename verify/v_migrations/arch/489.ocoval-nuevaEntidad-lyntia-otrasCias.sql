-- Verify postgis-vlci-vlci2:v_migrations/489.ocoval-nuevaEntidad-lyntia-otrasCias on pg

BEGIN;

select 1/count(*) from vlci2.vlci_ocoval_entidades where compania='Compañías de Servicios' and entidad='Lyntia';
select case when count(*)=67 then 1 else 1/(select 0) end as resultado from vlci2.vlci_ocoval_entidades;

select 1/count(*) from vlci2.vlci_ocoval_companyia where compania='OTRAS CIAS' and entidad='Lyntia';
select 1/count(*) from vlci2.vlci_ocoval_companyia where compania='OTRAS CIAS' and entidad='Digi';
select case when count(*)=17 then 1 else 1/(select 0) end as resultado from vlci2.vlci_ocoval_companyia;

ROLLBACK;
