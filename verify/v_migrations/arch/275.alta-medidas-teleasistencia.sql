-- Verify postgis-vlci-vlci2:v_migrations/275.alta-medidas-teleasistencia on pg

BEGIN;

select 1 / (case count(*) when 4 then 1 else 0 end)
from vlci2.t_m_unidad_medida
where dc_unidad_medida_cas IN ('Usuarios/año', 'Índice satisfación anual', 'Nº de reclamaciones/quejas', 'Nº llamadas seguimiento/Mes');

ROLLBACK;
