-- Verify postgis-vlci-vlci2:v_migrations/1909.delete_15_agosto_xativa on pg

BEGIN;

select 1/ (case when count(*) = 0 then 1 else 0 end)  from  vlci2.t_f_text_kpi_trafico_y_bicis
    where calculationperiod = '2023-08-15' and tramo = 'Xàtiva';

ROLLBACK;
