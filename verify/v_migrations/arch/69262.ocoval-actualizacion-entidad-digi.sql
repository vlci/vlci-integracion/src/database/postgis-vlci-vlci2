-- Verify postgis-vlci-vlci2:v_migrations/69262.ocoval-actualizacion-entidad-digi on pg

BEGIN;

SELECT 1/COUNT(*) FROM vlci2.vlci_ocoval_entidades
	WHERE entidad='Digi' AND compania='Compañías de Servicios';

ROLLBACK;
