-- Verify postgis-vlci-vlci2:v_migrations/2636-modify-table-cia-inyeccion-agua-corregida on pg

BEGIN;

-- Attempt to select from the modified table to ensure it exists with the new column names
SELECT correctiondate, correctedkpivalue FROM vlci2.t_datos_cb_cia_inyeccion_agua_corregida LIMIT 1;

ROLLBACK;
