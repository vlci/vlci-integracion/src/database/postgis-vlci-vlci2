-- Verify postgis-vlci-vlci2:v_migrations/1568-datos-historicos-bsg on pg

BEGIN;

-- Comprueba que se hayan insertado el numero de filas esperado.
select case when count(*)=31841 then 1 else 1/(select 0) end as resultado  
from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved < '2020-03-02 00:00:00.000';

ROLLBACK;
