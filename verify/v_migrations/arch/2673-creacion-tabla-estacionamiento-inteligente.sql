-- Verify postgis-vlci-vlci2:v_migrations/2673-creacion-tabla-estacionamiento-inteligente on pg

BEGIN;

SELECT 1 / case when count(*) = 1 then 1 else 0 end
    FROM information_schema.tables 
WHERE table_name = 't_datos_cb_trafico_kpi_estacionamiento_inteligente';

ROLLBACK;
