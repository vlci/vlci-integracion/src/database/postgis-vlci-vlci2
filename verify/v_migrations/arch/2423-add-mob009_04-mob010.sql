-- Verify postgis-vlci-vlci2:v_migrations/2423-add-mob009_04-mob010 on pg

DO $$
DECLARE
    new_kpis integer;
BEGIN

    new_kpis:= (select count(*) 
                    from vlci2.t_d_indicador tdi
                    where tdi.pk_indicador in (1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930)
                        and tdi.dc_criterio_desagregacion1_cas = 'Línea');

    ASSERT new_kpis = 10;

END $$;
