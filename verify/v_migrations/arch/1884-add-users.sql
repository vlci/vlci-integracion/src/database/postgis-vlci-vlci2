-- Verify postgis-vlci-vlci2:v_migrations/1884-add-users on pg

BEGIN;

select 1/count(*) from vlci2.t_d_usuario_servicio WHERE pk_user_id='U301362';
select 1/count(*) from vlci2.t_d_usuario_servicio WHERE pk_user_id='U18761';

select 1/count(*) from vlci2.t_d_user_rol WHERE pk_user='U301362';
select 1/count(*) from vlci2.t_d_user_rol WHERE pk_user='U18761';


ROLLBACK;
