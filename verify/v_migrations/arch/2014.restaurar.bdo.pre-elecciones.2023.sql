-- Verify postgis-vlci-vlci2:v_migrations/2014.restaurar.bdo.pre-elecciones.2023 on pg

BEGIN;

  -- AREAS
  select 1 / case when count(*) = 11 then 1 else 0 end from vlci2.t_d_area;
  select 1 / case when count(*) = 9 then 1 else 0 end from information_schema.columns where table_name='t_d_area_arch' and table_schema = 'vlci2';

  -- DELEGACIONES
  select 1 / case when count(*) = 58 then 1 else 0 end from vlci2.t_d_delegacion;
  select 1 / case when count(*) = 10 then 1 else 0 end from information_schema.columns where table_name='t_d_delegacion_arch' and table_schema = 'vlci2';
  select fk_area_id from vlci2.t_d_delegacion_arch;

  -- SERVICIOS
  select 1 / case when count(*) = 50 then 1 else 0 end from (select distinct fk_delegacion_id from vlci2.t_d_servicio) del_servicio;
  select 1 / case when count(*) = 11 then 1 else 0 end from information_schema.columns where table_name='t_d_servicio_arch' and table_schema = 'vlci2';

ROLLBACK;
