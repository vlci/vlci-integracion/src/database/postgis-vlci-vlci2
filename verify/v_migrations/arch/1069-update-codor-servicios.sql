-- Verify postgis-vlci-vlci2:v_migrations/1069-update-codor-servicios on pg

BEGIN;

select case when count(*)=94 then 1 else 1/(select 0) end as resultado from vlci2.t_datos_etl_agregados_servicio;
select case when count(*)=53 then 1 else 1/(select 0) end as resultado from vlci2.t_datos_etl_agregados_delegacion;

ROLLBACK;
