-- Verify postgis-vlci-vlci2:v_migrations/1535-agregar-campo-areaServed on pg

BEGIN;

SELECT count(column_name) 
FROM information_schema.columns 
WHERE table_name = 't_datos_cb_sonometros_hopvlci_daily' AND column_name = 'areaServed';

ROLLBACK;
