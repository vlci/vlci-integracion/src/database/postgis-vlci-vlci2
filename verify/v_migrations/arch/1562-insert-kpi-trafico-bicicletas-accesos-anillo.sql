-- Verify postgis-vlci-vlci2:v_migrations/1562-insert-kpi-trafico-bicicletas-accesos-anillo on pg

BEGIN;

select case when count(*)=7254 then 1 else 1/(select 0) end as resultado  
from vlci2.t_f_text_kpi_trafico_y_bicis where to_date(calculationperiod, 'YYYY-MM-DD') < to_date('2020-02-03', 'YYYY-MM-DD') 
    and entityid = 'Kpi-Trafico-Bicicletas-Accesos-Anillo';

ROLLBACK;
