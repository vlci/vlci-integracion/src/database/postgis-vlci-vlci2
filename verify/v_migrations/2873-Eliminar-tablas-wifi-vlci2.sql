-- Verify postgis-vlci-vlci2:v_migrations/2873-Eliminar-tablas-wifi-vlci2 on pg

BEGIN;

DO $$
DECLARE
    table_count integer;
    view_count integer;
    function_count integer;
    trigger_count integer;
BEGIN
    -- Comprobar la existencia de tablas en el esquema vlci2
    SELECT COUNT(*)
    INTO table_count
    FROM information_schema.tables
    WHERE table_schema = 'vlci2'
      AND table_name IN (
        't_agg_conexiones_wifi_urbo', 
        't_d_ap_urbo', 
        't_d_poi_urbo', 
        't_f_ap', 
        't_f_ap_avisos',
        't_f_ap_detail_rt', 
        't_f_ap_servicio', 
        't_f_ap_servicio_tmp', 
        't_f_ap_urbo', 
        't_f_aps_agg',
        't_f_bbdd_org_ap', 
        't_f_bbdd_org_poi', 
        't_f_cliente_wifi', 
        't_f_conexiones_wifi_urbo', 
        't_f_num_altas_clientes_agg', 
        't_f_num_poi_agg', 
        't_f_poi', 
        't_f_poi_rt', 
        't_f_poi_urbo_di', 
        't_f_poi_urbo_hr', 
        't_f_poi_urbo_rt', 
        't_f_text_cb_wifi_kpi_conexiones', 
        't_f_text_cb_wifi_kpi_conexiones_backup',
        't_f_text_cb_wifi_kpi_urbo', 
        't_f_wifi_ap_report', 
        't_f_wifi_clientes_report', 
        't_f_wifi_clientes_report_tmp'
      );

    -- Si se encontró al menos una tabla, lanzar un error
    IF table_count > 0 THEN
        RAISE EXCEPTION 'Se encontraron % tablas existentes en el esquema vlci2', table_count;
    END IF;

    -- Comprobar la existencia de la vista materializada en el esquema vlci2
    SELECT COUNT(*)
    INTO view_count
    FROM pg_catalog.pg_matviews
    WHERE schemaname = 'vlci2'
      AND matviewname = 'vw_datos_cb_wifi_kpi_conexiones_materialized';

    -- Si se encontró al menos una vista materializada, lanzar un error
    IF view_count > 0 THEN
        RAISE EXCEPTION 'La vista materializada vw_datos_cb_wifi_kpi_conexiones_materialized ya existe en el esquema vlci2';
    END IF;

    -- Comprobar la existencia de funciones en el esquema vlci2
    SELECT COUNT(*)
    INTO function_count
    FROM information_schema.routines
    WHERE routine_schema = 'vlci2'
      AND routine_name IN (
            'fn_t_agg_conexiones_wifi_urbo_before_insert', 
            'fn_t_agg_conexiones_wifi_urbo_before_update',
            'fn_t_d_ap_urbo_before_insert', 
            'fn_t_d_ap_urbo_before_update', 
            'fn_t_d_poi_urbo_before_insert',
            'fn_t_d_poi_urbo_before_update', 
            'fn_t_f_ap_before_insert', 
            'fn_t_f_ap_before_update',
            'fn_t_f_ap_avisos_before_insert', 
            'fn_t_f_ap_avisos_before_update', 
            'fn_t_f_ap_detail_rt_before_insert',
            'fn_t_f_ap_detail_rt_before_update', 
            'fn_t_f_ap_servicio_before_insert', 
            'fn_t_f_ap_servicio_before_update',
            'fn_t_f_ap_urbo_before_insert', 
            'fn_t_f_ap_urbo_before_update', 
            'fn_t_f_aps_agg_before_insert',
            'fn_t_f_aps_agg_before_update', 
            'fn_t_f_bbdd_org_ap_before_insert', 
            'fn_t_f_bbdd_org_ap_before_update',
            'fn_t_f_bbdd_org_poi_before_insert', 
            'fn_t_f_bbdd_org_poi_before_update', 
            'fn_t_f_cliente_wifi_before_insert',
            'fn_t_f_cliente_wifi_before_update', 
            'fn_t_f_conexiones_wifi_urbo_before_insert', 
            'fn_t_f_conexiones_wifi_urbo_before_update',
            'fn_t_f_num_altas_clientes_agg_before_insert', 
            'fn_t_f_num_altas_clientes_agg_before_update', 
            'fn_t_f_num_poi_agg_before_insert',
            'fn_t_f_num_poi_agg_before_update', 
            'fn_t_f_poi_before_insert', 
            'fn_t_f_poi_before_update',
            'fn_t_f_poi_rt_before_insert', 
            'fn_t_f_poi_rt_before_update', 
            'fn_t_f_poi_urbo_di_before_insert',
            'fn_t_f_poi_urbo_di_before_update', 
            'fn_t_f_poi_urbo_hr_before_insert', 
            'fn_t_f_poi_urbo_hr_before_update',
            'fn_t_f_poi_urbo_rt_before_insert', 
            'fn_t_f_poi_urbo_rt_before_update', 
            'update_wifi_kpi_agregados', 
            'fn_t_f_text_cb_wifi_kpi_conexiones_before_insert'
      );

    -- Si se encontró al menos una función, lanzar un error
    IF function_count > 0 THEN
        RAISE EXCEPTION 'Se encontraron % funciones en el esquema vlci2', function_count;
    END IF;

    -- Comprobar la existencia de triggers en el esquema vlci2
    SELECT COUNT(*)
    INTO trigger_count
    FROM pg_trigger
    WHERE tgrelid IN (
        SELECT oid
        FROM pg_class
        WHERE relnamespace = (SELECT oid FROM pg_namespace WHERE nspname = 'vlci2')
          AND relname IN (
            't_agg_conexiones_wifi_urbo', 
            't_d_ap_urbo', 
            't_d_poi_urbo', 
            't_f_ap', 
            't_f_ap_avisos',
            't_f_ap_detail_rt', 
            't_f_ap_servicio', 
            't_f_ap_servicio_tmp', 
            't_f_ap_urbo', 
            't_f_aps_agg',
            't_f_bbdd_org_ap', 
            't_f_bbdd_org_poi', 
            't_f_cliente_wifi', 
            't_f_conexiones_wifi_urbo', 
            't_f_num_altas_clientes_agg',
            't_f_num_poi_agg', 
            't_f_poi', 
            't_f_poi_rt', 
            't_f_poi_urbo_di', 
            't_f_poi_urbo_hr', 
            't_f_poi_urbo_rt', 
            't_f_text_cb_wifi_kpi_conexiones', 
            't_f_text_cb_wifi_kpi_conexiones_backup',
            't_f_text_cb_wifi_kpi_urbo', 
            't_f_wifi_ap_report', 
            't_f_wifi_clientes_report', 
            't_f_wifi_clientes_report_tmp'
          )
    );

    -- Si se encontró al menos un trigger, lanzar un error
    IF trigger_count > 0 THEN
        RAISE EXCEPTION 'Se encontraron % triggers en el esquema vlci2', trigger_count;
    END IF;

    -- Si todas las comprobaciones pasan, no se lanza ninguna excepción y el bloque termina normalmente
END $$ LANGUAGE plpgsql;


ROLLBACK;
