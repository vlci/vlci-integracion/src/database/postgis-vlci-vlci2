-- Verify postgis-vlci-vlci2:v_migrations/2955-crear-tabla-intervenciones-policia on pg

BEGIN;

SELECT 1 / (
    CASE
        WHEN (SELECT COUNT(*) FROM information_schema.tables
              WHERE table_name = 't_datos_cb_policia_intervenciones_last_data'
                AND table_schema = 'vlci2') = 1
          AND (SELECT COUNT(*) FROM information_schema.columns
               WHERE table_name = 't_datos_cb_policia_intervenciones_last_data'
                 AND table_schema = 'vlci2') = 16
        THEN 1
        ELSE 0
    END
);

ROLLBACK;
