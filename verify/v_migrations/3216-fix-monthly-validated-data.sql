-- Verify postgis-vlci-vlci2:v_migrations/3216-fix-monthly-validated-data on pg

DO $$
declare
	entries_cleared_table integer;
    entries_january integer;
BEGIN

	entries_cleared_table := (select count(*) from vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes);
	assert entries_cleared_table = 0;

	entries_january := (select count(*) from vlci2.t_d_fecha_negocio_etls where fen = '2024-01-01 00:00:00.000' and etl_nombre in ('W01_AVFRANCIA_temperatura_vm', 'W02_NAZARET_precipitaciones_vm'));
	assert entries_january = 2;
END $$;