-- Verify postgis-vlci-vlci2:v_migrations/3226-crear-tabla-t_d_personal on pg

BEGIN;

DO $$
DECLARE
    missing_columns integer; 

	expected_columns text[] := ARRAY[
            'fecha_extraccion', 'num_emp', 'sexo', 'fecha_nacimiento', 'edad', 
            'grupo_titulacion', 'haberes', 'nivel_personal', 'compl_espec_personal', 
            'compl_prod_personal', 'compl_prod_espec_per', 'fecha_antiguedad', 
            'trienios_tipo_1', 'numer_tipo_1', 'trienios_tipo_2', 'numer_tipo_2', 
            'trienios_tipo_3', 'numer_tipo_3', 'trienios_tipo_4', 'numer_tipo_4', 
            'trienios_tipo_5', 'numer_tipo_5', 'trienios_tipo_6', 'numer_6', 
            'trienios_tipo_7', 'numer_tipo_7', 'trienios_tipo_8', 'numero_tipo_8', 
            'trienios_tipo_9', 'numer_tipo_9', 'ccc', 'categoria_personal', 
            'descripcion_categoria', 'vinc_laboral', 'desc_vinc_laboral', 
            'referencia', 'puesto', 'descripcion_puesto', 'gt_puesto', 
            'haberes_puesto', 'nivel_puesto', 'compl_espec_puesto', 
            'compl_prod_puesto', 'compl_prod_espec_pue', 'categoria_puesto', 
            'literal_categoria_puesto', 'sistema_provision_pu', 'organigrama', 
            'descripcion_organo', 'area', 'descripcion_area', 'delegacion', 
            'descripcion_delegacion', 'servicio', 'descripcion_servicio', 
            'seccion', 'descripcion_seccion', 'negociado', 'descripcion_negociado', 
            'aud_fec_ins', 'aud_user_ins', 'aud_fec_upd', 'aud_user_upd'
        ];

BEGIN
    SELECT COUNT(*) INTO missing_columns
    FROM (
        SELECT unnest(expected_columns) AS column_name
        EXCEPT
        SELECT column_name
        FROM information_schema.columns
        WHERE table_schema = 'vlci2'
          AND table_name = 't_d_personal'
    ) AS faltantes;

    IF missing_columns > 0 THEN
        RAISE EXCEPTION 'Discrepancia en las columnas: % columnas faltantes',
            missing_columns;
    ELSE
        RAISE NOTICE 'Todas las columnas coinciden correctamente.';
    END IF;
END $$;

ROLLBACK;
