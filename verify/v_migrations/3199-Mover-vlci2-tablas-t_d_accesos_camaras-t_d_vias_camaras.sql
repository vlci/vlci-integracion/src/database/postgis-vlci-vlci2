-- Verify postgis-vlci-vlci2:v_migrations/3199-Mover-vlci2-tablas-t_d_accesos_camaras-t_d_vias_camaras on pg

BEGIN;

DO $$
BEGIN
    IF EXISTS (SELECT 1 
               FROM pg_catalog.pg_tables 
               WHERE schemaname = 'cdmt' 
               AND tablename = 't_d_vias_camaras') THEN
        PERFORM 1 / 0; 
    END IF; 

    IF EXISTS (SELECT 1 
               FROM pg_catalog.pg_tables 
               WHERE schemaname = 'cdmt' 
               AND tablename = 't_d_accesos_camaras') THEN
        PERFORM 1 / 0;  
    END IF; 

    PERFORM 1 FROM vlci2.t_d_vias_camaras LIMIT 1; 
    PERFORM 1 FROM vlci2.t_d_accesos_camaras LIMIT 1;

END $$;

ROLLBACK;
