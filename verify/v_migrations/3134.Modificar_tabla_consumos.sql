-- Verify postgis-vlci-vlci2:v_migrations/3134.Modificar_tabla_consumos on pg

BEGIN;

DO $$ 
BEGIN

   IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables WHERE tablename = 't_datos_cb_energia_consumos_municipales_diarios') THEN
        PERFORM 1 / 0;  
   END IF;

   PERFORM 1 FROM vlci2.t_datos_cb_energia_datadis_consumos_municipales_diarios;
   
END $$;

ROLLBACK;

