-- Verify postgis-vlci-vlci2:v_migrations/2711-Eliminar-entradas-wifi-tabla-housekeeping on pg

BEGIN;

DO $$
DECLARE
    count_rows INTEGER;
BEGIN
    -- Count the number of matching rows
    SELECT COUNT(*)
    INTO count_rows
    FROM vlci2.housekeeping_config
    WHERE table_nam IN (
        't_agg_conexiones_wifi_urbo',
        't_f_aps_agg',
        't_f_ap_avisos',
        't_f_ap_detail_rt',
        't_f_ap_urbo',
        't_f_conexiones_wifi_urbo',
        't_f_num_poi_agg',
        't_f_poi_rt',
        't_f_poi_urbo_di',
        't_f_poi_urbo_hr',
        't_f_poi_urbo_rt'
    );

    -- Check if count_rows is greater than 0 and raise an error if true
    IF count_rows > 0 THEN
        RAISE EXCEPTION 'There are % matching rows found in vlci2.housekeeping_config for the specified table_nam values.', count_rows;
    END IF;
END $$;


ROLLBACK;
