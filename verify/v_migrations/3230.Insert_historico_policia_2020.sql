-- Verify postgis-vlci-vlci2:v_migrations/3230.Insert_historico_policia_2020 on pg

BEGIN;

DO $$
DECLARE
    entries integer;
BEGIN

    entries := (select  count (*) from vlci2.t_datos_cb_policia_intervenciones_lastdata where dateobserved >= '2020-01-01' and dateobserved < '2021-01-01');

    ASSERT entries >= 268277;
END $$;

ROLLBACK;
