-- Verify postgis-vlci-vlci2:v_migrations/TLF046-limpiar_datos_medioambiente on pg
BEGIN;

SELECT
    1 / CASE
        WHEN (
            SELECT
                data_type
            FROM
                information_schema.columns
            WHERE
                table_name = 't_f_text_cb_weatherobserved'
                AND column_name = 'temperaturevalue'
        ) = 'double precision' THEN 1
        ELSE 0
    END,
    1 / CASE
        WHEN (
            SELECT
                data_type
            FROM
                information_schema.columns
            WHERE
                table_name = 't_f_text_cb_weatherobserved'
                AND column_name = 'winddirectionvalue'
        ) = 'double precision' THEN 1
        ELSE 0
    END,
    1 / CASE
        WHEN (
            SELECT
                data_type
            FROM
                information_schema.columns
            WHERE
                table_name = 't_f_text_cb_weatherobserved'
                AND column_name = 'precipitationvalue'
        ) = 'double precision' THEN 1
        ELSE 0
    END,
    1 / CASE
        WHEN (
            SELECT
                data_type
            FROM
                information_schema.columns
            WHERE
                table_name = 't_f_text_cb_weatherobserved'
                AND column_name = 'windspeedvalue'
        ) = 'double precision' THEN 1
        ELSE 0
    END,
    1 / CASE
        WHEN (
            SELECT
                data_type
            FROM
                information_schema.columns
            WHERE
                table_name = 't_f_text_cb_weatherobserved'
                AND column_name = 'atmosphericpressurevalue'
        ) = 'double precision' THEN 1
        ELSE 0
    END,
    1 / CASE
        WHEN (
            SELECT
                data_type
            FROM
                information_schema.columns
            WHERE
                table_name = 't_f_text_cb_weatherobserved'
                AND column_name = 'relativehumidityvalue'
        ) = 'double precision' THEN 1
        ELSE 0
    END;

ROLLBACK;