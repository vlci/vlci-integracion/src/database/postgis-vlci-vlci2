-- Verify postgis-vlci-vlci2:v_migrations/3226-triggers_y_editar_t_d_personal on pg
DO $$
BEGIN

    -- Verificar tipo de aud_fec_ins
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.columns
        WHERE table_schema = 'vlci2'
          AND table_name = 't_d_personal'
          AND column_name = 'aud_fec_ins'
          AND data_type = 'timestamp without time zone'
    ) THEN
        RAISE EXCEPTION 'Column "aud_fec_ins" does not have type "timestamp"';
    END IF;

    -- Verificar tipo de aud_fec_upd
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.columns
        WHERE table_schema = 'vlci2'
          AND table_name = 't_d_personal'
          AND column_name = 'aud_fec_upd'
          AND data_type = 'timestamp without time zone'
    ) THEN
        RAISE EXCEPTION 'Column "aud_fec_upd" does not have type "timestamp"';
    END IF;

    -- Verificar tipo de aud_user_ins
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.columns
        WHERE table_schema = 'vlci2'
          AND table_name = 't_d_personal'
          AND column_name = 'aud_user_ins'
          AND data_type = 'character varying'
    ) THEN
        RAISE EXCEPTION 'Column "aud_user_ins" does not have type "varchar"';
    END IF;

    -- Verificar tipo de aud_user_upd
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.columns
        WHERE table_schema = 'vlci2'
          AND table_name = 't_d_personal'
          AND column_name = 'aud_user_upd'
          AND data_type = 'character varying'
    ) THEN
        RAISE EXCEPTION 'Column "aud_user_upd" does not have type "varchar"';
    END IF;

    -- Verificar trigger vlci2_t_d_personal_ins
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.triggers
        WHERE trigger_schema = 'vlci2'
          AND event_object_table = 't_d_personal'
          AND trigger_name = 'vlci2_t_d_personal_ins'
    ) THEN
        RAISE EXCEPTION 'Trigger "vlci2_t_d_personal_ins" does not exist';
    END IF;

    -- Verificar trigger vlci2_t_d_personal_upd
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.triggers
        WHERE trigger_schema = 'vlci2'
          AND event_object_table = 't_d_personal'
          AND trigger_name = 'vlci2_t_d_personal_upd'
    ) THEN
        RAISE EXCEPTION 'Trigger "vlci2_t_d_personal_upd" does not exist';
    END IF;

END $$;