-- Verify postgis-vlci-vlci2:v_migrations/3226-edicion-columnas-personal on pg

BEGIN;

DO $$
DECLARE
    col_name TEXT;
BEGIN
    FOR col_name IN 
        SELECT UNNEST(ARRAY[
            'grupo', 'subgrupo', 'area_adagp_extra', 'area_gip', 'delegacion_adagp', 
            'delegacion_gip', 'desc_provision_puesto', 'negociado_adagp', 'negociado_gip', 
            'organo_adagp', 'organo_gip', 'posicion_gip', 'puesto_gip', 'seccion_adagp', 
            'seccion_gip', 'servicio_adagp', 'servicio_gip'
        ])
    LOOP
        IF NOT EXISTS (
            SELECT 1 
            FROM information_schema.columns c
            WHERE c.table_name = 't_datos_etl_personal_ayuntamiento'
              AND c.table_schema = 'vlci2'
              AND c.column_name = col_name
        ) THEN
            RAISE EXCEPTION 'Falta la columna % en la tabla.', col_name;
        END IF;
    END LOOP;
END $$;

DO $$
BEGIN
    IF EXISTS (
        SELECT 1 
        FROM information_schema.columns c
        WHERE c.table_name = 't_datos_etl_personal_ayuntamiento'
          AND c.table_schema = 'vlci2'
          AND c.column_name = 'vinc_laboral'
    ) THEN
        RAISE EXCEPTION 'La columna vinc_laboral todavía existe en la tabla.';
    END IF;
END $$;

ROLLBACK;
