-- Verify postgis-vlci-vlci2:v_migrations/3134_New2_Modificar_Tabla_consumos on pg

BEGIN;

DO $$ 
DECLARE
    rec RECORD;
BEGIN
    FOR rec IN 
        SELECT column_name, is_nullable
        FROM information_schema.columns
        WHERE table_name = 't_datos_cb_energia_datadis_consumos_municipales_diarios' 
        AND column_name IN ('recvtime', 'entityid', 'entitytype', 'fiwareservicepath', 'address', 'obtainmethod', 'kpivalue', 'originalservicepath', 'originalentitytype')
        AND table_schema = 'vlci2'
    LOOP
        IF rec.is_nullable = 'NO' THEN
            RAISE EXCEPTION 'Column "%" is not nullable.', rec.column_name;
        END IF;
    END LOOP;
END $$;

ROLLBACK;
