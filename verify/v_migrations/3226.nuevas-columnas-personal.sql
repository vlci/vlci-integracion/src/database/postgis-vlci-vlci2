-- Verify postgis-vlci-vlci2:v_migrations/3226.nuevas-columnas-personal on pg

DO $$
BEGIN
    -- Verificar que la tabla ha sido renombrada correctamente
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.tables
        WHERE table_schema = 'vlci2'
          AND table_name = 't_datos_etl_personal_ayuntamiento'
    ) THEN
        RAISE EXCEPTION 'Table "vlci2.t_datos_etl_personal_ayuntamiento" does not exist';
    END IF;

    -- Verificar que la columna "ocupada_vacante" existe y tiene el tipo correcto
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.columns
        WHERE table_schema = 'vlci2'
          AND table_name = 't_datos_etl_personal_ayuntamiento'
          AND column_name = 'ocupada_vacante'
          AND data_type = 'character'
    ) THEN
        RAISE EXCEPTION 'Column "ocupada_vacante" does not exist or does not have type "character"';
    END IF;

    -- Verificar que la columna "numero" existe y tiene el tipo correcto
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.columns
        WHERE table_schema = 'vlci2'
          AND table_name = 't_datos_etl_personal_ayuntamiento'
          AND column_name = 'numero'
          AND data_type = 'integer'
    ) THEN
        RAISE EXCEPTION 'Column "numero" does not exist or does not have type "integer"';
    END IF;

    -- Verificar que la columna "area_adagp" existe y tiene el tipo correcto
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.columns
        WHERE table_schema = 'vlci2'
          AND table_name = 't_datos_etl_personal_ayuntamiento'
          AND column_name = 'area_adagp'
          AND data_type = 'character varying'
    ) THEN
        RAISE EXCEPTION 'Column "area_adagp" does not exist or does not have type "character varying"';
    END IF;

    -- Verificar trigger "vlci2_t_d_personal_ins"
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.triggers
        WHERE trigger_schema = 'vlci2'
          AND event_object_table = 't_datos_etl_personal_ayuntamiento'
          AND trigger_name = 'vlci2_t_d_personal_ins'
    ) THEN
        RAISE EXCEPTION 'Trigger "vlci2_t_d_personal_ins" does not exist';
    END IF;

    -- Verificar trigger "vlci2_t_d_personal_upd"
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.triggers
        WHERE trigger_schema = 'vlci2'
          AND event_object_table = 't_datos_etl_personal_ayuntamiento'
          AND trigger_name = 'vlci2_t_d_personal_upd'
    ) THEN
        RAISE EXCEPTION 'Trigger "vlci2_t_d_personal_upd" does not exist';
    END IF;

END $$;
