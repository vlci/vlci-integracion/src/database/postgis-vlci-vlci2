-- Verify postgis-vlci-vlci2:v_migrations/2962.eliminar_datos_antiguos_clima_rt on pg
BEGIN;

SELECT
  1 / (
    CASE
      WHEN (
        SELECT
          COUNT(*)
        FROM
          vlci2.housekeeping_config
      ) = 11
      AND (
        SELECT
          COUNT(*)
        FROM
          vlci2.housekeeping_config
        WHERE
          (
            table_nam = 't_f_text_kpi_temperatura_media_diaria'
            AND tiempo = 6
            AND colfecha = 'calculationperiod'
          )
          OR (
            table_nam = 't_f_text_kpi_precipitaciones_diarias'
            AND tiempo = 6
            AND colfecha = 'calculationperiod'
          )
      ) = 2 THEN 1
      ELSE 0
    END
  );

ROLLBACK;