-- Verify postgis-vlci-vlci2:r_code/views/vw_datos_cb_residuos_wastecontainer_lastdata on pg

BEGIN;

SELECT 1/ CASE
        WHEN project ISNULL THEN 0
        ELSE 1
    END AS project
FROM vw_datos_cb_residuos_wastecontainer_lastdata;

ROLLBACK;
