-- Verify postgis-vlci-vlci2:r_code/views/vw_t_f_text_kpi_trafico_vehiculos on pg

BEGIN;

DO $$
DECLARE
    count_name_distinct integer;
BEGIN

    count_name_distinct := (select count(distinct(name)) from vlci2.vw_t_f_text_kpi_trafico_vehiculos);

    ASSERT count_name_distinct = 12;
END $$;

ROLLBACK;
