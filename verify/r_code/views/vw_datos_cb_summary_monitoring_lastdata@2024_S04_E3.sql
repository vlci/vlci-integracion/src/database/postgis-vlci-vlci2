-- Verify postgis-vlci-vlci2:r_code/views/vw_datos_cb_summary_monitoring_lastdata on pg

BEGIN;

SELECT 1/
    CASE 
        WHEN COUNT(project) = COUNT(*) THEN 1
        ELSE 0
    END AS Resultado
FROM vw_datos_cb_summary_monitoring_lastdata vdcsml;

ROLLBACK;
