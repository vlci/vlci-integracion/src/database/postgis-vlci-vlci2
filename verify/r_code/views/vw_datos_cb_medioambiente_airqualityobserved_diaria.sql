-- Verify postgis-vlci-vlci2:v_migrations/2907-crear-vista-medioambiente-datos-validados-no-validados on pg

BEGIN;

DO $$
DECLARE
    count_views INTEGER;
BEGIN
    SELECT 1/count(*) INTO count_views
    FROM pg_catalog.pg_views
    WHERE viewname = 'vw_datos_cb_medioambiente_airqualityobserved_diaria';


END $$;

ROLLBACK;
