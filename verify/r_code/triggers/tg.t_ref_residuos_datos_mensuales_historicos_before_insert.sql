-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_ref_residuos_datos_mensuales_historicos_before_insert on pg

BEGIN;

    select 1/count(*)
    from pg_trigger
    where not tgisinternal
    and tgname = 't_ref_residuos_datos_mensuales_historicos_before_insert'
    and tgrelid = 't_ref_residuos_datos_mensuales_historicos'::regclass;

ROLLBACK;
