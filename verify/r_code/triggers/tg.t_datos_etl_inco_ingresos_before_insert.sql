-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_inco_ingresos_before_insert on pg

BEGIN;

SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_datos_etl_inco_ingresos_before_insert'
AND tgrelid = 't_datos_etl_inco_ingresos'::regclass;

ROLLBACK;
