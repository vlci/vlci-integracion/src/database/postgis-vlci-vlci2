-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_ref_etl_inco_capitulos_before_update on pg

BEGIN;

SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_ref_etl_inco_capitulos_before_update'
AND tgrelid = 't_ref_etl_inco_capitulos'::regclass;

ROLLBACK;
