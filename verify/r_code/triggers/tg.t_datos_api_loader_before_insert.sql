-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_datos_api_loader_before_insert on pg

BEGIN;

SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_datos_api_loader_campos_request_before_insert'
AND tgrelid = 't_d_api_loader_campos_request'::regclass;

ROLLBACK;
