-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_datos_cb_trafico_aparcamientos_lastdata_before_update on pg

BEGIN;

select 1/count(*)
    from pg_trigger
    where not tgisinternal
    and tgname = 't_datos_cb_trafico_aparcamientos_lastdata_before_update'
    and tgrelid = 't_datos_cb_trafico_aparcamientos_lastdata'::regclass;

ROLLBACK;
