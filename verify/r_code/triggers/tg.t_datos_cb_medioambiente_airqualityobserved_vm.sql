-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_datos_cb_medioambiente_airqualityobserved_vm on pg

BEGIN;

select 1/count(*)
    from pg_trigger
    where not tgisinternal
    and tgname = 't_datos_cb_medioambiente_airqualityobserved_vm_before_insert'
    and tgrelid = 't_datos_cb_medioambiente_airqualityobserved_vm'::regclass;

ROLLBACK;
