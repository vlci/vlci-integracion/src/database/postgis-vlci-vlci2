-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_contratos_menores_bdo_v2 on pg

BEGIN;

SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_datos_etl_contratos_menores_bdo_before_insert'
AND tgrelid = 'vlci2.t_datos_etl_unidades_administrativas_areas'::regclass;

SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_datos_etl_contratos_menores_bdo_update'
AND tgrelid = 'vlci2.t_datos_etl_unidades_administrativas_areas'::regclass;

ROLLBACK;
