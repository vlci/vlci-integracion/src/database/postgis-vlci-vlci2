-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_ref_etl_inco_conceptos_before_insert on pg

BEGIN;

SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_ref_etl_inco_conceptos_before_insert'
AND tgrelid = 't_ref_etl_inco_conceptos'::regclass;

ROLLBACK;
