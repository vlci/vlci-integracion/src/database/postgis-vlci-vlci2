-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_f_text_cb_wifi_kpi_conexiones_before_insert on pg

BEGIN;

select 1/count(*)
    from pg_trigger
    where not tgisinternal
    and tgname = 't_f_text_cb_wifi_kpi_conexiones_before_insert'
    and tgrelid = 't_f_text_cb_wifi_kpi_conexiones'::regclass;


ROLLBACK;
