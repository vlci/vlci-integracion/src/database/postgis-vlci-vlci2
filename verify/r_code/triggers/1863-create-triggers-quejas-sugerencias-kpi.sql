-- Verify postgis-vlci-vlci2:r_code/triggers/1863-create-triggers-quejas-sugerencias-kpi on pg

BEGIN;

 SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_datos_etl_quejas_sugerencias_KPI_insert'
AND tgrelid = 't_datos_etl_quejas_sugerencias_KPI'::regclass;

 SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_datos_etl_quejas_sugerencias_KPI_update'
AND tgrelid = 't_datos_etl_quejas_sugerencias_KPI'::regclass;

ROLLBACK;
