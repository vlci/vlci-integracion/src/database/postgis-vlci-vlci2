-- Verify postgis-vlci-vlci2:r_code/triggers/1753.crear-triggers-tablas-bdo on pg

BEGIN;

-- Comprobamos la creacion de los trigger de update
SELECT 1/case when count(trigger_name) = 3 then 1 else 0 end
FROM 
    information_schema."triggers"
WHERE 
    event_object_schema = 'vlci2' AND
	trigger_name in ('t_d_servicio_before_update', 't_d_delegacion_before_update', 't_d_area_before_update');

-- Comprobamos la creacion de los trigger de insert
SELECT 1/case when count(trigger_name) = 3 then 1 else 0 end
FROM 
    information_schema."triggers"
WHERE 
    event_object_schema = 'vlci2' AND
	trigger_name in ('t_d_servicio_before_insert', 't_d_delegacion_before_insert', 't_d_area_before_insert');



ROLLBACK;
