-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_contratos_menores_before_update on pg

BEGIN;

SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_datos_etl_contratos_menores_before_update'
AND tgrelid = 't_datos_etl_contratos_menores'::regclass;

ROLLBACK;
