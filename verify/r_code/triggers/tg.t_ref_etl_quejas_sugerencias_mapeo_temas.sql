-- Verify postgis-vlci-vlci2:r_code/triggers/tg.t_ref_etl_quejas_sugerencias_mapeo_temas on pg

BEGIN;

SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_ref_etl_quejas_sugerencias_mapeo_temas_before_insert'
AND tgrelid = 't_ref_etl_quejas_sugerencias_mapeo_temas'::regclass;

SELECT tgname
FROM pg_trigger
WHERE NOT tgisinternal
AND tgname =  't_ref_etl_quejas_sugerencias_mapeo_temas_before_update'
AND tgrelid = 't_ref_etl_quejas_sugerencias_mapeo_temas'::regclass;

ROLLBACK;
