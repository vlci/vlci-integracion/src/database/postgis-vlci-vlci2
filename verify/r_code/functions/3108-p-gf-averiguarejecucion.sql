-- Verify postgis-vlci-vlci2:r_code/functions/3108-p-gf-averiguarejecucion on pg

BEGIN;
-- Verify if 'MENSUAL' is present in the function p_gf_averiguarejecucion
SELECT 1
FROM pg_proc
WHERE proname = 'p_gf_averiguarejecucion'
  AND prosrc LIKE '%MENSUAL%';

ROLLBACK;
