-- Verify postgis-vlci-vlci2:r_code/functions/3108-p-gf-actualizarFecha on pg

BEGIN;
-- Verify if 'MENSUAL' is present in the procedure p_gf_actualizarFecha
SELECT 1
FROM pg_proc
WHERE proname = 'p_gf_actualizarFecha'
  AND prosrc LIKE '%MENSUAL%';

ROLLBACK;
