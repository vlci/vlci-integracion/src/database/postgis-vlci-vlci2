-- Verify postgis-vlci-vlci2:r_code/functions/f.aud_fec_ins on pg

BEGIN;

select exists(select * from pg_proc where proname = 'fn_aud_fec_ins');

ROLLBACK;
