-- Verify postgis-vlci-vlci2:r_code/functions/f.t_datos_etl_estadistica_definicion_indicadores_before_insert on pg

BEGIN;

SELECT has_function_privilege('fn_t_datos_etl_estadistica_definicion_indicadores_before_insert()', 'execute');

ROLLBACK;
