-- Verify postgis-vlci-vlci2:r_code/functions/f.t_ref_estadistica_harcoded_kpi_before_insert on pg

BEGIN;

SELECT has_function_privilege('fn_t_ref_estadistica_harcoded_kpi_before_insert()', 'execute');

ROLLBACK;
