-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_ref_etl_inco_conceptos_before_insert from pg

BEGIN;

DROP trigger IF EXISTS t_ref_etl_inco_conceptos_insert on t_ref_etl_inco_conceptos;

COMMIT;
