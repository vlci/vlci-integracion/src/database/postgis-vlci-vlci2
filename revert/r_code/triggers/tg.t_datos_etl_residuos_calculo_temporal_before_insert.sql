-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_residuos_calculo_temporal_before_insert from pg

BEGIN;

DROP TRIGGER t_datos_etl_residuos_calculo_temporal_before_insert
ON vlci2.t_datos_etl_residuos_calculo_temporal;

COMMIT;
