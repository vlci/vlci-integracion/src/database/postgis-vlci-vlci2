-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_contratos_menores_before_update from pg

BEGIN;

DROP trigger IF EXISTS t_datos_etl_contratos_menores_update on t_datos_etl_contratos_menores;

COMMIT;
