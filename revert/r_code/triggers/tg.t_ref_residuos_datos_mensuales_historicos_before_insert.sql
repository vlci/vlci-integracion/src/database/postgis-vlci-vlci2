-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_ref_residuos_datos_mensuales_historicos_before_insert from pg

BEGIN;

    DROP trigger IF EXISTS t_ref_residuos_datos_mensuales_historicos_before_insert on vlci2.t_ref_residuos_datos_mensuales_historicos;

COMMIT;
