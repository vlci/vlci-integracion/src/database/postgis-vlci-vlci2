-- Revert postgis-vlci-vlci2:r_code/triggers/1863-create-triggers-quejas-sugerencias-kpi from pg

BEGIN;

DROP TRIGGER t_datos_etl_quejas_sugerencias_KPI_insert ON vlci2.t_datos_etl_quejas_sugerencias_KPI;
DROP TRIGGER t_datos_etl_quejas_sugerencias_KPI_update ON vlci2.t_datos_etl_quejas_sugerencias_KPI;

COMMIT;
