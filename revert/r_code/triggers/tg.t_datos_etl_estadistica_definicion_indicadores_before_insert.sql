-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_estadistica_definicion_indicadores_before_insert from pg

BEGIN;

DROP trigger IF EXISTS t_datos_etl_estadistica_definicion_indicadores_before_insert on t_datos_etl_estadistica_definicion_indicadores;

COMMIT;
