-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_ref_etl_inco_conceptos_before_update from pg

BEGIN;

DROP trigger IF EXISTS t_ref_etl_inco_conceptos_update on t_ref_etl_inco_conceptos;

COMMIT;
