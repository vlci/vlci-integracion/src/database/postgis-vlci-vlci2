-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_estadistica_definicion_indicadores_before_update from pg

BEGIN;

DROP trigger IF EXISTS t_datos_etl_estadistica_definicion_indicadores_before_update on t_datos_etl_estadistica_definicion_indicadores;

COMMIT;
