-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_contratos_menores_bdo_v2 from pg

BEGIN;

DROP TRIGGER t_datos_etl_contratos_menores_bdo_before_insert ON vlci2.t_datos_etl_unidades_administrativas_areas;
DROP TRIGGER t_datos_etl_contratos_menores_bdo_update ON vlci2.t_datos_etl_unidades_administrativas_areas;

COMMIT;
