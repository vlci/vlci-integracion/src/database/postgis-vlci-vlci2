-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_ref_estadistica_harcoded_kpi_before_insert from pg

BEGIN;

DROP trigger IF EXISTS t_ref_estadistica_harcoded_kpi_before_insert on t_ref_estadistica_harcoded_kpi;

COMMIT;
