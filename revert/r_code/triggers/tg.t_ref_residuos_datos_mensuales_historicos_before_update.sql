-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_ref_residuos_datos_mensuales_historicos_before_update from pg

BEGIN;

    DROP trigger IF EXISTS t_ref_residuos_datos_mensuales_historicos_before_update on vlci2.t_ref_residuos_datos_mensuales_historicos;

COMMIT;
