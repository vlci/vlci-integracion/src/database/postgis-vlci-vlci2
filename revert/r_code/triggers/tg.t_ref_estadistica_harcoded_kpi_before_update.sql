-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_ref_estadistica_harcoded_kpi_before_update from pg

BEGIN;

DROP trigger IF EXISTS t_ref_estadistica_harcoded_kpi_before_update on t_ref_estadistica_harcoded_kpi;

COMMIT;
