-- Revert postgis-vlci-vlci2:r_code/triggers/1753.crear-triggers-tablas-bdo-arch from pg

BEGIN;

DROP TRIGGER IF EXISTS t_d_servicio_arch_before_insert;
DROP TRIGGER IF EXISTS t_d_servicio_arch_before_update;
DROP TRIGGER IF EXISTS t_d_delegacion_arch_before_insert;
DROP TRIGGER IF EXISTS t_d_delegacion_arch_before_update;
DROP TRIGGER IF EXISTS t_d_area_arch_before_insert;
DROP TRIGGER IF EXISTS t_d_area_arch_before_update;

COMMIT;