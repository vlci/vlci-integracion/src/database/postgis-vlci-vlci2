-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_datos_cb_medioambiente_airqualityobserved_va from pg

BEGIN;

DROP trigger IF EXISTS t_datos_cb_medioambiente_airqualityobserved_va_before_insert on vlci2.t_datos_cb_medioambiente_airqualityobserved_va;

COMMIT;
