-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_datos_api_loader_before_update from pg

BEGIN;

DROP trigger IF EXISTS t_datos_api_loader_campos_request_before_update on vlci2.t_d_api_loader_campos_request;

COMMIT;
