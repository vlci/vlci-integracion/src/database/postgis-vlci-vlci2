-- Revert postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_inco_ingresos_before_update from pg

BEGIN;

DROP trigger IF EXISTS t_datos_etl_inco_ingresos_update on t_datos_etl_inco_ingresos;

COMMIT;
