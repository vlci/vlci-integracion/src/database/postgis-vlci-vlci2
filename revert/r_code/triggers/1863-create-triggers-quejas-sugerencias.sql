-- Revert postgis-vlci-vlci2:r_code/triggers/1863-create-triggers-quejas-sugerencias from pg

BEGIN;

DROP TRIGGER t_datos_etl_quejas_sugerencias_before_insert ON vlci2.t_datos_etl_quejas_sugerencias;
DROP TRIGGER t_datos_etl_quejas_sugerencias_update ON vlci2.t_datos_etl_quejas_sugerencias;

COMMIT;
