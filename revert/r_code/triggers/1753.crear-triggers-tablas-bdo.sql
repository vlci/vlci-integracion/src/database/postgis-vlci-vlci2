-- Revert postgis-vlci-vlci2:r_code/triggers/1753.crear-triggers-tablas-bdo from pg

BEGIN;

DROP TRIGGER IF EXISTS t_d_servicio_before_insert;
DROP TRIGGER IF EXISTS t_d_servicio_before_updarte;
DROP TRIGGER IF EXISTS t_d_delegacion_before_insert;
DROP TRIGGER IF EXISTS t_d_delegacion_before_update;
DROP TRIGGER IF EXISTS t_d_area_before_insert;
DROP TRIGGER IF EXISTS t_d_area_before_update;

COMMIT;
