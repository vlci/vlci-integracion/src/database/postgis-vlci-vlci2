-- Revert postgis-vlci-vlci2:r_code/views/vw_datos_cb_trafico_vehiculos_materialized from pg

BEGIN;

DROP MATERIALIZED VIEW IF EXISTS vlci2.vw_datos_cb_trafico_vehiculos_materialized;

CREATE MATERIALIZED VIEW vlci2.vw_t_f_text_kpi_trafico_vehiculos_materialized
AS SELECT a11.id,
	a11.name,
    date_part('month'::text, to_date(a11.calculationperiod, 'YYYY-MM-DD'::text)) AS mes,
    date_part('year'::text, to_date(a11.calculationperiod, 'YYYY-MM-DD'::text)) AS anyo,
    sum(a11.kpivalue) AS value
   FROM vlci2.vw_t_f_text_kpi_trafico_vehiculos a11
  WHERE a11.id='Kpi-Accesos-Ciudad-Coches' or a11.id='Kpi-Accesos-Vias-Coches'
  GROUP BY mes, anyo, name, id
  ORDER BY anyo, mes;
COMMIT;
