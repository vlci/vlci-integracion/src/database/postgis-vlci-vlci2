-- Revert postgis-vlci-vlci2:r_code/views/vw_datos_cb_emt_kpi_materialized from pg

BEGIN;

DROP MATERIALIZED VIEW IF EXISTS vlci2.vw_datos_cb_emt_kpi_materialized;

CREATE MATERIALIZED VIEW vlci2.vw_datos_cb_emt_kpi_materalized AS
select sliceanddicevalue1 as name, 
	sliceanddice1 as type,
	kpivalue as value, 
	DATE_PART('year', calculationperiod::date) as anyo,
	DATE_PART('month', calculationperiod::date) as mes
from vlci2.t_datos_cb_emt_kpi tdcek ;

COMMIT;
