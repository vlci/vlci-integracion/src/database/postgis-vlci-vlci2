-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_manual_padron_nacimientos_agregado to pg

BEGIN;
drop view  vw_datos_manual_padron_nacimientos_agregado;
create or replace view vw_datos_manual_padron_nacimientos_agregado as
SELECT
    nac,
    SUM(CASE WHEN gender = 'Hombre' THEN total else 0 END) AS male,
    SUM(CASE WHEN gender = 'Mujer' THEN total else 0 END) AS female,
    SUM(CASE WHEN total notnull THEN total else 0 END) as total
FROM t_datos_manual_padron_nacimientos
GROUP BY
    nac
ORDER BY
    nac;

COMMIT;
