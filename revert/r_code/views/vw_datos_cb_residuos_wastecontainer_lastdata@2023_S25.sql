-- Revert postgis-vlci-vlci2:r_code/views/vw_datos_cb_residuos_wastecontainer_lastdata from pg

BEGIN;

DROP VIEW vlci2.vw_datos_cb_residuos_wastecontainer_lastdata;

CREATE OR REPLACE VIEW vlci2.vw_datos_cb_wastecontainer_lastdata AS
SELECT entityid, timeinstant, fillinglevel, maintenanceowner, location, temperature, 
       CASE WHEN timeinstant >= NOW() - INTERVAL '3 days' THEN 'ok' ELSE 'noData' END AS operationalstatus
FROM vlci2.t_datos_cb_wastecontainer_lastdata;

COMMIT;
