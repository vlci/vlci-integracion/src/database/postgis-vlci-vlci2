-- Revert postgis-vlci-vlci2:r_code/views/vw_datos_cb_cdmciutat_contaminantes from pg

BEGIN;

-- vlci2.vw_datos_cb_cdmciutat_contaminantes source
DROP VIEW vlci2.vw_datos_cb_cdmciutat_contaminantes;
CREATE OR REPLACE VIEW vlci2.vw_datos_cb_cdmciutat_contaminantes
AS WITH medioambiente_poi_latest AS (
         SELECT t_datos_cb_medioambiente_poi.entityid,
            max(t_datos_cb_medioambiente_poi.recvtime) AS latest_date
           FROM t_datos_cb_medioambiente_poi
          GROUP BY t_datos_cb_medioambiente_poi.entityid
        )
 SELECT poi.name AS estacion,
    to_char(aqo.dateobserved, 'dd-MM-YYYY'::text) AS fecha,
        CASE
            WHEN to_char(aqo.dateobserved, 'DY'::text) = 'MON'::text THEN 'L'::text
            WHEN to_char(aqo.dateobserved, 'DY'::text) = 'TUE'::text THEN 'M'::text
            WHEN to_char(aqo.dateobserved, 'DY'::text) = 'WED'::text THEN 'X'::text
            WHEN to_char(aqo.dateobserved, 'DY'::text) = 'THU'::text THEN 'J'::text
            WHEN to_char(aqo.dateobserved, 'DY'::text) = 'FRI'::text THEN 'V'::text
            WHEN to_char(aqo.dateobserved, 'DY'::text) = 'SAT'::text THEN 'S'::text
            WHEN to_char(aqo.dateobserved, 'DY'::text) = 'SUN'::text THEN 'D'::text
            ELSE NULL::text
        END AS dia_semana,
    aqo.pm10value AS pm10,
    aqo.no2value AS no2
   FROM t_datos_cb_medioambiente_airqualityobserved aqo
     LEFT JOIN (t_datos_cb_medioambiente_poi poi
     JOIN medioambiente_poi_latest latest ON latest.entityid::text = poi.entityid::text AND latest.latest_date = poi.recvtime) ON poi.entityid::text = aqo.refpointofinterest
  WHERE aqo.entityid::text ~~ '%24h'::text;

COMMIT;
