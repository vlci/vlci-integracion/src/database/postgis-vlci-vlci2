-- Revert postgis-vlci-vlci2:r_code/views/vw_f_text_datos_sonometros_daily from pg

BEGIN;
DROP VIEW vlci2.vw_f_text_datos_sonometros_daily;
CREATE OR REPLACE VIEW vlci2.vw_f_text_datos_sonometros_daily
AS SELECT row_number() OVER () AS idt_agg_son,
    sq.sonometro_direccion,
    sq.fecha,
    sq.dia_semana,
    round(sq.valor_manana::numeric, 2) AS valor_manana,
    round(sq.valor_tarde::numeric, 2) AS valor_tarde,
    round(sq.valor_noche::numeric, 2) AS valor_noche,
    sq.posicion
   FROM ( SELECT
                CASE
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248652-daily'::text THEN 'S4-Sueca Esq. Denia'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248655-daily'::text THEN 'S3-Cadiz 3'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248661-daily'::text THEN 'S6-General Prim Chaflan Donoso Cortes'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248669-daily'::text THEN 'S7-Dr.Serrano 21'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248670-daily'::text THEN 'S12-Carles Cervera, Chaflan Reina Dona Maria'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248671-daily'::text THEN 'S5-Cadiz 16'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248672-daily'::text THEN 'S8-PuertoRico 21'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248676-daily'::text THEN 'S15-Salvador Abril Chaflan Maestro Jose Serrano'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248677-daily'::text THEN 'S14-Vivons Chaflan Cadiz'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248678-daily'::text THEN 'S10-Carles Cervera 34'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248679-daily'::text THEN 'S13-Matias Perello Esq. Doctor Sumsi'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248680-daily'::text THEN 'S9-Sueca 32'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248682-daily'::text THEN 'S1-Cuba 3'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248683-daily'::text THEN 'S2-Sueca 2'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248684-daily'::text THEN 'S11-Sueca 61'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T251234-daily'::text THEN 'S16-Cura Femenía 14'::text
                    ELSE ''::text
                END AS sonometro_direccion,
            to_date(t_f_text_cb_sonometros_ruzafa_daily.dateobserved, 'yyyy-MM-dd'::text) AS fecha,
                CASE date_part('dow'::text, to_date(t_f_text_cb_sonometros_ruzafa_daily.dateobserved, 'yyyy-mm-dd'::text))
                    WHEN 1 THEN 'L'::text
                    WHEN 2 THEN 'M'::text
                    WHEN 3 THEN 'X'::text
                    WHEN 4 THEN 'J'::text
                    WHEN 5 THEN 'V'::text
                    WHEN 6 THEN 'S'::text
                    WHEN 0 THEN 'D'::text
                    ELSE NULL::text
                END AS dia_semana,
            t_f_text_cb_sonometros_ruzafa_daily.laeq_d AS valor_manana,
            t_f_text_cb_sonometros_ruzafa_daily.laeq_e AS valor_tarde,
            t_f_text_cb_sonometros_ruzafa_daily.laeq_n AS valor_noche,
            t_f_text_cb_sonometros_ruzafa_daily.location AS posicion
           FROM t_f_text_cb_sonometros_ruzafa_daily
          GROUP BY (
                CASE
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248652-daily'::text THEN 'S4-Sueca Esq. Denia'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248655-daily'::text THEN 'S3-Cadiz 3'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248661-daily'::text THEN 'S6-General Prim Chaflan Donoso Cortes'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248669-daily'::text THEN 'S7-Dr.Serrano 21'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248670-daily'::text THEN 'S12-Carles Cervera, Chaflan Reina Dona Maria'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248671-daily'::text THEN 'S5-Cadiz 16'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248672-daily'::text THEN 'S8-PuertoRico 21'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248676-daily'::text THEN 'S15-Salvador Abril Chaflan Maestro Jose Serrano'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248677-daily'::text THEN 'S14-Vivons Chaflan Cadiz'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248678-daily'::text THEN 'S10-Carles Cervera 34'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248679-daily'::text THEN 'S13-Matias Perello Esq. Doctor Sumsi'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248680-daily'::text THEN 'S9-Sueca 32'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248682-daily'::text THEN 'S1-Cuba 3'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248683-daily'::text THEN 'S2-Sueca 2'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248684-daily'::text THEN 'S11-Sueca 61'::text
                    WHEN t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T251234-daily'::text THEN 'S16-Cura Femenía 14'::text
                    ELSE ''::text
                END), t_f_text_cb_sonometros_ruzafa_daily.dateobserved, t_f_text_cb_sonometros_ruzafa_daily.laeq_d, t_f_text_cb_sonometros_ruzafa_daily.laeq_e, t_f_text_cb_sonometros_ruzafa_daily.laeq_n, t_f_text_cb_sonometros_ruzafa_daily.location
        UNION ALL
         SELECT
                CASE
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi1-daily'::text THEN 'S01 - Plaza del Cedro 12'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi2-daily'::text THEN 'S02 - Plaza del Cedro 1'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi3-daily'::text THEN 'S03 - Plaza del Cedro 10'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi4-daily'::text THEN 'S04 - Carrer Campoamor 54'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi5-daily'::text THEN 'S05 - Carrer Campoamor 60'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi6-daily'::text THEN 'S06 - Plaza Honduras 37 dcha'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi7-daily'::text THEN 'S07 - Plaza Honduras 37 centro'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi8-daily'::text THEN 'S08 - Plaza Honduras 37 izquierda'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi9-daily'::text THEN 'S09 - Plaza Honduras 37 exterior'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi10-daily'::text THEN 'S10 - Centro Servicio Social - Fachada Izq'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi11-daily'::text THEN 'S11 - Centro Servicio Social - Fachada Dch'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi12-daily'::text THEN 'S12 - Fuente en Benimaclet'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi13-daily'::text THEN 'S13 - Centro Servicio Social - Fachada Lat'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi14-daily'::text THEN 'S14 - Carrer Polo y Peyrolon 29'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi15-daily'::text THEN 'S15 - Carrer Polo y Peyrolon 13'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi16-daily'::text THEN 'S16 - Carrer Polo y Peyrolon 15'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi17-daily'::text THEN 'S17 - Carrer Polo y Peyrolon 43'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi18-daily'::text THEN 'S18 - Carrer Polo y Peyrolon 38'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi19-daily'::text THEN 'S19 - Carrer Polo y Peyrolon 53'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi20-daily'::text THEN 'S20 - Carrer Comte d Altea 20'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi21-daily'::text THEN 'S21 - Carrer Comte d Altea 21'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi22-daily'::text THEN 'S22 - Carrer Almirall Cadarso 30'::text
                    ELSE ''::text
                END AS sonometro_direccion,
            to_date(t_datos_cb_sonometros_hopvlci_daily.dateobserved, 'yyyy-MM-dd'::text) AS fecha,
                CASE date_part('dow'::text, to_date(t_datos_cb_sonometros_hopvlci_daily.dateobserved, 'yyyy-mm-dd'::text))
                    WHEN 1 THEN 'L'::text
                    WHEN 2 THEN 'M'::text
                    WHEN 3 THEN 'X'::text
                    WHEN 4 THEN 'J'::text
                    WHEN 5 THEN 'V'::text
                    WHEN 6 THEN 'S'::text
                    WHEN 0 THEN 'D'::text
                    ELSE NULL::text
                END AS dia_semana,
            t_datos_cb_sonometros_hopvlci_daily.laeq_d AS valor_manana,
            t_datos_cb_sonometros_hopvlci_daily.laeq_e AS valor_tarde,
            t_datos_cb_sonometros_hopvlci_daily.laeq_n AS valor_noche,
            t_datos_cb_sonometros_hopvlci_daily.location AS posicion
           FROM t_datos_cb_sonometros_hopvlci_daily
          GROUP BY (
                CASE
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi1-daily'::text THEN 'S01 - Plaza del Cedro 12'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi2-daily'::text THEN 'S02 - Plaza del Cedro 1'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi3-daily'::text THEN 'S03 - Plaza del Cedro 10'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi4-daily'::text THEN 'S04 - Carrer Campoamor 54'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi5-daily'::text THEN 'S05 - Carrer Campoamor 60'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi6-daily'::text THEN 'S06 - Plaza Honduras 37 dcha'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi7-daily'::text THEN 'S07 - Plaza Honduras 37 centro'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi8-daily'::text THEN 'S08 - Plaza Honduras 37 izquierda'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi9-daily'::text THEN 'S09 - Plaza Honduras 37 exterior'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi10-daily'::text THEN 'S10 - Centro Servicio Social - Fachada Izq'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi11-daily'::text THEN 'S11 - Centro Servicio Social - Fachada Dch'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi12-daily'::text THEN 'S12 - Fuente en Benimaclet'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi13-daily'::text THEN 'S13 - Centro Servicio Social - Fachada Lat'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi14-daily'::text THEN 'S14 - Carrer Polo y Peyrolon 29'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi15-daily'::text THEN 'S15 - Carrer Polo y Peyrolon 13'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi16-daily'::text THEN 'S16 - Carrer Polo y Peyrolon 15'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi17-daily'::text THEN 'S17 - Carrer Polo y Peyrolon 43'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi18-daily'::text THEN 'S18 - Carrer Polo y Peyrolon 38'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi19-daily'::text THEN 'S19 - Carrer Polo y Peyrolon 53'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi20-daily'::text THEN 'S20 - Carrer Comte d Altea 20'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi21-daily'::text THEN 'S21 - Carrer Comte d Altea 21'::text
                    WHEN t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi22-daily'::text THEN 'S22 - Carrer Almirall Cadarso 30'::text
                    ELSE ''::text
                END), t_datos_cb_sonometros_hopvlci_daily.dateobserved, t_datos_cb_sonometros_hopvlci_daily.laeq_d, t_datos_cb_sonometros_hopvlci_daily.laeq_e, t_datos_cb_sonometros_hopvlci_daily.laeq_n, t_datos_cb_sonometros_hopvlci_daily.location) sq;

COMMIT;
