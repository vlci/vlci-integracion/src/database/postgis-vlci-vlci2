-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_medioambiente_weatherobserved_10m_lastdata to pg
BEGIN;

CREATE
OR REPLACE VIEW vlci2.vw_datos_cb_medioambiente_weatherobserved_10m_lastdata AS
SELECT
    entityid,
    name,
    gisid,
    dateobserved,
    case
        when project isnull then 'Sin Proyecto'
        else project
    end as project,
    maintenanceowner,
    precipitationvalue,
    temperaturevalue,
    location,
    CASE
        WHEN dateobserved :: timestamp without time zone >= (now() - '01:00:00' :: interval) THEN 'ok' :: text
        ELSE 'noData' :: text
    END AS operationalstatus
FROM
    t_datos_cb_medioambiente_weatherobserved_10m_lastdata tdcmwml;

COMMIT;