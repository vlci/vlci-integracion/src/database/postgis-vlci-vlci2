-- Deploy postgis-vlci-vlci2:r_code/views/vw_t_f_text_kpi_trafico_bicis to pg

BEGIN;


DROP MATERIALIZED VIEW IF EXISTS vlci2.vw_datos_cb_kpi_trafico_bicis_materialized;
DROP VIEW vlci2.vw_t_f_text_kpi_trafico_bicis;
CREATE OR REPLACE VIEW vlci2.vw_t_f_text_kpi_trafico_bicis
AS SELECT t_f_text_kpi_trafico_y_bicis.entityid AS id,
    t_f_text_kpi_trafico_y_bicis.calculationperiod,
    t_f_text_kpi_trafico_y_bicis.diasemana,
    round(t_f_text_kpi_trafico_y_bicis.kpivalue::numeric, 2) AS kpivalue,
    t_f_text_kpi_trafico_y_bicis.tramo,
    t_f_text_kpi_trafico_y_bicis.name,
    measureunitval as measureunit
   FROM t_f_text_kpi_trafico_y_bicis
  WHERE t_f_text_kpi_trafico_y_bicis.entityid::text = ANY (ARRAY['Kpi-Trafico-Bicicletas-Accesos-Anillo'::character varying, 'Kpi-Trafico-Bicicletas-Anillo'::character varying]::text[]);

CREATE MATERIALIZED VIEW vlci2.vw_datos_cb_kpi_trafico_bicis_materialized
AS SELECT a11.id,
	a11.name,
    date_part('month'::text, to_date(a11.calculationperiod, 'YYYY-MM-DD'::text)) AS mes,
    date_part('year'::text, to_date(a11.calculationperiod, 'YYYY-MM-DD'::text)) AS anyo,
    sum(a11.kpivalue) AS value
   FROM vlci2.vw_t_f_text_kpi_trafico_bicis a11
  GROUP BY mes, anyo, name, id
  ORDER BY anyo, mes;

COMMIT;
