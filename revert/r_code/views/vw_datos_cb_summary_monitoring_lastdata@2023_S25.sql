-- Revert postgis-vlci-vlci2:r_code/views/vw_datos_cb_summary_monitoring_lastdata from pg

BEGIN;

DROP VIEW vlci2.vw_datos_cb_summary_monitoring_lastdata;
CREATE OR REPLACE VIEW vlci2.vw_datos_cb_summary_urbo_lastdata AS
SELECT  operationalstatus
FROM (
	SELECT operationalstatus 
	FROM vlci2.vw_datos_cb_sonometro_noiselevelobserved_lastdata
    UNION ALL
    SELECT
        CASE 
            WHEN operationalstatus = 'ok' THEN 'ok'
            WHEN operationalstatus = 'ko' THEN 'noData'
            ELSE 'noData'
        END AS operationalstatus
    FROM vlci2.t_datos_cb_parkingspot_lastdata
    UNION ALL
    SELECT operationalstatus
    FROM vw_datos_cb_medioambiente_airqualityobserved_lastdata
    UNION ALL
    SELECT operationalstatus
    FROM vw_datos_cb_medioambiente_emt_airquality_lastdata 
    UNION ALL
    SELECT operationalstatus
    FROM vw_datos_cb_residuos_wastecontainer_lastdata vdcrwl 
) AS countstatus;

COMMIT;
