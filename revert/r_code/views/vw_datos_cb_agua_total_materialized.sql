-- Revert postgis-vlci-vlci2:r_code/views/vw_datos_cb_agua_total_materialized from pg

BEGIN;

drop MATERIALIZED view vlci2.vw_datos_cb_agua_total_materialized;

CREATE MATERIALIZED VIEW vlci2.vw_t_agg_consumo_total_materialized AS
SELECT id,
       kpivalue,
       DATE_PART('year', calculationperiod::date) as anyo,
       DATE_PART('month', calculationperiod::date) as mes
FROM vlci2.vw_t_agg_consumo_agua_total a11;

COMMIT;
