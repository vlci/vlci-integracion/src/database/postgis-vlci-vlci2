-- Revert postgis-vlci-vlci2:r_code/views/vw_datos_cb_wifi_kpi_conexiones_materialized from pg

BEGIN;

DROP MATERIALIZED VIEW IF EXISTS vlci2.vw_datos_cb_wifi_kpi_conexiones_materialized;

CREATE MATERIALIZED VIEW vlci2.vw_t_f_text_cb_wifi_kpi_conexiones_materialized
AS SELECT
	case 
		when a11.sliceanddicevalue1 = 'AVC' then 'Nombre de funcionaris connectats a la WIFI' 
		when a11.sliceanddicevalue1 = 'WiFi4EU' then 'Nombre de ciutadans connectats a Wifi4EU'  
		else a11.sliceanddicevalue1 end as name,
	date_part('month'::text, to_date(a11.calculationperiod, 'DD-MM-YYYY'::text)) AS mes,
	date_part('year'::text, to_date(a11.calculationperiod, 'DD-MM-YYYY'::text)) AS anyo,
    sum(a11.kpivalue) AS value
from	vlci2.t_f_text_cb_wifi_kpi_conexiones	a11
where	a11.entityid in ('IS.OCI.005.a1') and a11.sliceanddicevalue1 != 'wifivalencia'
group by mes, anyo, case 
		when a11.sliceanddicevalue1 = 'AVC' then 'Nombre de funcionaris connectats a la WIFI' 
		when a11.sliceanddicevalue1 = 'WiFi4EU' then 'Nombre de ciutadans connectats a Wifi4EU'  
		else a11.sliceanddicevalue1 end
order by anyo, mes, name;

COMMIT;
