-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_alumbrado_device_lastdata to pg

BEGIN;

CREATE OR REPLACE VIEW vlci2.vw_datos_cb_alumbrado_device_lastdata AS
SELECT entityid, provider, timeinstant, datapow, name, location,
       CASE WHEN timeinstant >= NOW() - INTERVAL '1 hour' THEN 'ok' ELSE 'noData' END AS operationalstatus
FROM vlci2.t_datos_cb_alumbrado_device_lastdata;

COMMIT;
