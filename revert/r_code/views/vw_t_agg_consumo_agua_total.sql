-- Revert postgis-vlci-vlci2:r_code/views/vw_t_agg_consumo_agua_total from pg

BEGIN;

-- vlci2.vw_t_agg_consumo_agua_total source
DROP VIEW vlci2.vw_t_agg_consumo_agua_total;
CREATE OR REPLACE VIEW vlci2.vw_t_agg_consumo_agua_total
AS SELECT t_f_text_cb_kpi_consumo_agua.entityid AS id,
    t_f_text_cb_kpi_consumo_agua.calculationperiod,
        CASE
            WHEN date_part('dow'::text, to_date(t_f_text_cb_kpi_consumo_agua.calculationperiod, 'yyyy-mm-dd'::text)) = 0::double precision THEN 7::double precision
            ELSE date_part('dow'::text, to_date(t_f_text_cb_kpi_consumo_agua.calculationperiod, 'yyyy-mm-dd'::text))
        END AS dayofweek,
    round(t_f_text_cb_kpi_consumo_agua.kpivalue::numeric, 2) AS kpivalue,
    t_f_text_cb_kpi_consumo_agua.name,
    t_f_text_cb_kpi_consumo_agua.neighborhood,
    t_f_text_cb_kpi_consumo_agua.consumptiontype
   FROM t_f_text_cb_kpi_consumo_agua
  WHERE t_f_text_cb_kpi_consumo_agua.consumptiontype = 'N/A'::text;

COMMIT;
