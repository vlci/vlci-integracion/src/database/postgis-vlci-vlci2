-- Revert postgis-vlci-vlci2:r_code/functions/f.t_datos_cb_medioambiente_airqualityobserved_before_insert from pg

BEGIN;

DROP FUNCTION fn_t_datos_cb_medioambiente_airqualityobserved_before_insert() CASCADE;

COMMIT;
