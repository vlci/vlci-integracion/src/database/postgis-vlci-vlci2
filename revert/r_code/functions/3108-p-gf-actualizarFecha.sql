-- Revert postgis-vlci-vlci2:r_code/functions/3108-p-gf-actualizarFecha from pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2."p_gf_actualizarFecha"(IN in_id integer, IN in_campo character varying, IN in_fecha timestamp without time zone, INOUT out_status character varying)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $procedure$
declare

    v_control VARCHAR(2);
    v_count INT;
    v_fen TIMESTAMP;
    v_fen_inicio TIMESTAMP;
    v_fen_fin TIMESTAMP;
    v_periodicidad VARCHAR(200);
    v_desfase int;
    v_estado VARCHAR(200);
    v_nombre_etl VARCHAR(200);
begin

    v_control := 'OK';



    v_count := (SELECT COUNT(*) FROM t_d_fecha_negocio_etls WHERE etl_id=IN_ID);

    IF v_count = 0 THEN

        v_control := 'KO';
        OUT_STATUS := CONCAT('No se encuentra ninguna ETL con el id: ',IN_ID);

    END IF;



    IF v_control = 'OK' THEN

        IF IN_CAMPO NOT IN ('fen','fen_inicio','fen_fin') THEN

            v_control := 'KO';
            OUT_STATUS := CONCAT('El valor ',IN_CAMPO ,' para el parametro de entrada IN_CAMPO no contiene un valor valido. Valores permitidos: "fen", "fen_inicio", "fen_fin"');

        END IF;

    END IF;



    IF v_control = 'OK' THEN

        SELECT fen, fen_inicio, fen_fin, periodicidad, "offset", estado, etl_nombre INTO v_fen, v_fen_inicio, v_fen_fin,v_periodicidad, v_desfase, v_estado, v_nombre_etl FROM t_d_fecha_negocio_etls WHERE etl_id=IN_ID;

    END IF;



    IF v_control = 'OK' THEN

        IF v_periodicidad NOT IN ('DIEZMINUTAL','HORARIA','DIARIA','TRIMESTRAL','SEMESTRAL', 'ANUAL') THEN

            v_control := 'KO';
            OUT_STATUS := CONCAT('La periodicidad de la ETL "',v_nombre_etl,'" con ID: ',IN_ID,' es "',v_periodicidad,'". La versión actual del procedimiento solo permite realizar calculos sobre indicadores Diezminutales, Horarios, Diarios, Trimestrales, Semestrales o Anuales');

        END IF;

    END IF;



    IF v_control = 'OK' THEN


        IF (v_periodicidad IN ('DIEZMINUTAL','HORARIA','DIARIA') AND IN_CAMPO = 'fen' AND IN_FECHA IS NOT NULL) THEN

            OUT_STATUS := 'OK';
            UPDATE t_d_fecha_negocio_etls SET fen = IN_FECHA, aud_fec_upd=now(), aud_user_upd='p_gf_ActualizarFecha' WHERE etl_id=IN_ID;

        ELSEIF (v_periodicidad IN ('TRIMESTRAL','SEMESTRAL','ANUAL') AND IN_CAMPO = 'fen_inicio' AND IN_FECHA IS NOT NULL) THEN

            SET OUT_STATUS = 'OK';
            UPDATE t_d_fecha_negocio_etls SET fen_inicio = IN_FECHA, aud_fec_upd=now(), aud_user_upd='p_gf_ActualizarFecha' WHERE etl_id=IN_ID;

        ELSEIF (v_periodicidad IN ('TRIMESTRAL','SEMESTRAL','ANUAL') AND IN_CAMPO = 'fen_fin' AND IN_FECHA IS NOT NULL) THEN

            OUT_STATUS := 'OK';
            UPDATE t_d_fecha_negocio_etls SET fen_fin = IN_FECHA, aud_fec_upd=now(), aud_user_upd='p_gf_ActualizarFecha' WHERE etl_id=IN_ID;

        ELSEIF IN_FECHA IS NULL THEN
            v_control := 'KO';
            OUT_STATUS := CONCAT('El campo de entrada FECHA no puede ser nulo.');
        ELSE
            v_control := 'KO';
            OUT_STATUS := CONCAT('El campo ',IN_CAMPO,' no es el utilizado para gestionar fechas con periodicidad ',v_periodicidad ,'. Utiliza el campo adecuado.');
        END IF;
    END IF;
end;
$procedure$
;

COMMIT;
