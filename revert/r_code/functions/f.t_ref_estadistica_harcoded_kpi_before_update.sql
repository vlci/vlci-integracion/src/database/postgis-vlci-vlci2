-- Revert postgis-vlci-vlci2:r_code/functions/f.t_ref_estadistica_harcoded_kpi_before_update from pg

BEGIN;

DROP FUNCTION fn_t_ref_estadistica_harcoded_kpi_before_update() CASCADE;

COMMIT;
