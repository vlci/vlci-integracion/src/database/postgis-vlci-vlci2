-- Revert postgis-vlci-vlci2:r_code/functions/3108-p-gf-calcularnuevafecha from pg

BEGIN;

CREATE OR REPLACE FUNCTION vlci2.p_gf_calcularnuevafecha(in_id integer, OUT out_status character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
-- Declaracion de variables
	DECLARE 
		v_control VARCHAR(2);
    	v_count INTEGER;
    	v_nombre_etl VARCHAR(200);
		v_fen TIMESTAMP;
		v_fen_inicio TIMESTAMP;
		v_fen_fin TIMESTAMP;
		v_periodicidad VARCHAR(200);
		v_desfase INTEGER;
		v_estado VARCHAR(200);
		v_max_fen TIMESTAMP;
		v_anyo_mes_max_fen VARCHAR(200);
		v_anyo_mes_fen_fin VARCHAR(200);
		v_mes_fen_fin VARCHAR(200);
		v_nueva_fen_inicio VARCHAR(200);
		v_nueva_fen_fin VARCHAR(200);
		
BEGIN
    
    v_control = 'OK';
	
    -- Consulta si existe una etl con el id de entrada
	v_count = (SELECT COUNT(*) FROM t_d_fecha_negocio_etls WHERE etl_id=IN_ID);
    
    IF v_count = 0 THEN
    
		v_control = 'KO';
        OUT_STATUS = CONCAT('No se encuentra ninguna ETL con el id: ',IN_ID);
        
	END IF;
    
	
    IF v_control = 'OK' THEN
	
		-- Recuperar valores de la tabla

		SELECT fen, fen_inicio, fen_fin, periodicidad, "offset", estado, etl_nombre 
		INTO v_fen, v_fen_inicio, v_fen_fin,v_periodicidad, v_desfase, v_estado, v_nombre_etl 
		FROM t_d_fecha_negocio_etls 
		WHERE etl_id=IN_ID;
		
		-- Evaluar estado
        
        IF v_estado <> 'OK' THEN 
        
			v_control = 'KO';
            OUT_STATUS = CONCAT('El estado de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' es ',v_estado,'. Para poder actualizar una fecha el estado debe ser "OK"');
            
        END IF;
        
	END IF;
    
	-- Evalua que la periodicidad sea diezminutal, horaria, diaria, trimestral, semestral o anual
    
    IF v_control = 'OK' THEN
    
        IF v_periodicidad NOT IN ('DIEZMINUTAL','HORARIA','DIARIA','TRIMESTRAL','SEMESTRAL', 'ANUAL') THEN 
        
			v_control = 'KO';
            OUT_STATUS = CONCAT('La periodicidad de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' es ',v_periodicidad,'. La versión actual del procedimiento solo permite realizar calculos sobre indicadores Diezminutales, Horarios, Diarios, Trimestrales, Semestrales o Anuales');
        
        END IF;
        
    END IF;
    
	IF v_control = 'OK' THEN
	
		-- Si la periodicidad es Diezminutal
    
        IF v_periodicidad = 'DIEZMINUTAL' THEN 
        
            OUT_STATUS = 'OK';
            v_max_fen = (SELECT to_char(current_timestamp - CAST(concat(v_desfase*10,' MINUTE') AS INTERVAL), 'YYYY-MM-dd HH24:mi:00'));
            
            IF v_fen < v_max_fen THEN 
            
				UPDATE t_d_fecha_negocio_etls 
				SET fen = v_fen + CAST('10 MINUTE' AS INTERVAL), aud_fec_upd=now(), aud_user_upd='p_gf_calcularNuevaFecha' 
				WHERE etl_id=IN_ID;
               
			   
			ELSEIF v_fen IS NULL THEN 
            
				v_control = 'KO';
				OUT_STATUS = CONCAT('El campo FEN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
                
            END IF;
            
		-- Si la periodicidad es Horaria
        
        ELSEIF v_periodicidad = 'HORARIA' THEN 
        
            OUT_STATUS = 'OK';
            v_max_fen = (SELECT to_char(current_timestamp - CAST(concat(v_desfase,' HOUR') AS INTERVAL), 'YYYY-MM-dd HH24:mi:00'));
            
            IF v_fen < v_max_fen THEN 
				
				UPDATE t_d_fecha_negocio_etls 
				SET fen = v_fen + CAST('1 HOUR' AS INTERVAL), aud_fec_upd=now(), aud_user_upd='p_gf_calcularNuevaFecha' 
				WHERE etl_id=IN_ID;
                
			ELSEIF v_fen IS NULL THEN 
            
				v_control = 'KO';
				OUT_STATUS = CONCAT('El campo FEN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
                
            END IF;
    
		-- Si la periodicidad es Diaria
        
        ELSEIF v_periodicidad = 'DIARIA' THEN 
        
            OUT_STATUS = 'OK';
            v_max_fen = (SELECT CURRENT_DATE - CAST(concat(v_desfase,' day') AS INTERVAL));
            
			-- Si el campo FEN es menor que la fecha maxima
            
            IF v_fen < v_max_fen THEN 
				
				UPDATE t_d_fecha_negocio_etls 
				SET fen = v_fen + CAST('1 DAY' AS INTERVAL), aud_fec_upd=now(), aud_user_upd='p_gf_calcularNuevaFecha' 
				WHERE etl_id=IN_ID;
                
            -- Si el campo FEN es nulo  
            
			ELSEIF v_fen IS NULL THEN 
            
				v_control = 'KO';
				OUT_STATUS = CONCAT('El campo FEN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
                
            END IF;
            
		-- Si la periodicidad es trimestral 
        
        ELSEIF v_periodicidad = 'TRIMESTRAL' THEN 
        
			-- Si la fecha inicio o fin estan a null
            
			IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
            
				v_control = 'KO';
				OUT_STATUS = CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
			
			ELSE
            
				OUT_STATUS = 'OK';
				v_max_fen = (SELECT CURRENT_DATE - CAST(concat(v_desfase,' day') AS INTERVAL));
				v_anyo_mes_max_fen = (SELECT to_char(v_max_fen, 'YYYYMM'));
				v_anyo_mes_fen_fin = (SELECT to_char(v_fen_fin, 'YYYYMM'));
                
				-- Si el rango de la tabla es igual o inferior a la fecha actual teniendo en cuenta el offset.
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
                
					v_mes_fen_fin = (SELECT to_char(v_fen_fin,'MM'));
                    v_nueva_fen_inicio = NULL;
                    v_nueva_fen_fin = NULL;
                    
                    IF v_mes_fen_fin = '04' THEN 
						
						v_nueva_fen_inicio= (select concat(to_char(v_fen_fin,'YYYY'),'0401'));
                        v_nueva_fen_fin= (select concat(to_char(v_fen_fin,'YYYY'),'0701'));
                                        
                    ELSEIF v_mes_fen_fin = '07' THEN 
						
						v_nueva_fen_inicio= (select concat(to_char(v_fen_fin,'YYYY'),'0701'));
                        v_nueva_fen_fin= (select concat(to_char(v_fen_fin,'YYYY'),'1001'));
                    
                    ELSEIF v_mes_fen_fin = '10' THEN 
                    
						v_nueva_fen_inicio= (select concat(to_char(v_fen_fin,'YYYY'),'1001'));
                        v_nueva_fen_fin= (select concat(to_char(v_fen_fin + CAST('1 YEAR' AS INTERVAL),'YYYY'),'0101'));
                         
					ELSEIF v_mes_fen_fin = '01' THEN 
						
						v_nueva_fen_inicio= (select concat(to_char(v_fen_fin,'YYYY'),'0101'));
                        v_nueva_fen_fin= (select concat(to_char(v_fen_fin,'YYYY'),'0401'));
                        
                    END IF;
                    
                    IF v_nueva_fen_inicio IS NOT NULL AND v_nueva_fen_fin IS NOT NULL THEN
                    
						UPDATE t_d_fecha_negocio_etls 
						SET fen_inicio = cast(v_nueva_fen_inicio as timestamp), fen_fin = cast(v_nueva_fen_fin as timestamp), aud_fec_upd=now(), aud_user_upd='p_gf_calcularNuevaFecha' 
						WHERE etl_id=IN_ID;
                        
                    END IF;
                END IF;
			END IF;
        
		-- Si la periodicidad es semestral 
        
        ELSEIF v_periodicidad = 'SEMESTRAL' THEN 
        
			IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
            
				v_control = 'KO';
				OUT_STATUS = CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
			
			ELSE
            
				OUT_STATUS = 'OK';
				v_max_fen = (SELECT CURRENT_DATE - CAST(concat(v_desfase,' day') AS INTERVAL));
				v_anyo_mes_max_fen = (SELECT to_char(v_max_fen, 'YYYYMM'));
				v_anyo_mes_fen_fin = (SELECT to_char(v_fen_fin, 'YYYYMM'));
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
					
					v_mes_fen_fin = (SELECT to_char(v_fen_fin,'MM'));
                    v_nueva_fen_inicio = NULL;
                    v_nueva_fen_fin = NULL;
                                        
                    IF v_mes_fen_fin = '07' THEN 
						
						v_nueva_fen_inicio= (select concat(to_char(v_fen_fin,'YYYY'),'0701'));
                        v_nueva_fen_fin= (select concat(to_char(v_fen_fin + CAST('1 YEAR' AS INTERVAL),'YYYY'),'0101'));
                                       
					ELSEIF v_mes_fen_fin = '01' THEN 
						
						v_nueva_fen_inicio= (select concat(to_char(v_fen_fin,'YYYY'),'0101'));
                        v_nueva_fen_fin= (select concat(to_char(v_fen_fin,'YYYY'),'0701'));
                        
                    END IF;
                    
                    IF v_nueva_fen_inicio IS NOT NULL AND v_nueva_fen_fin IS NOT NULL THEN
                    
						UPDATE t_d_fecha_negocio_etls 
						SET fen_inicio = cast(v_nueva_fen_inicio as timestamp), fen_fin = cast(v_nueva_fen_fin as timestamp), aud_fec_upd=now(), aud_user_upd='p_gf_calcularNuevaFecha' 
						WHERE etl_id=IN_ID;
                        
                    END IF;
                END IF;
			END IF;
			
		-- Si la periodicidad es anual 
        
        ELSEIF v_periodicidad = 'ANUAL' THEN 
        
			IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
            
				v_control = 'KO';
				OUT_STATUS = CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
			
			ELSE
            
				OUT_STATUS = 'OK';				
                v_max_fen = (SELECT CURRENT_DATE - CAST(concat(v_desfase,' day') AS INTERVAL));
				v_anyo_mes_max_fen = (SELECT to_char(v_max_fen, 'YYYYMM'));
				v_anyo_mes_fen_fin = (SELECT to_char(v_fen_fin, 'YYYYMM'));
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
					
					v_nueva_fen_inicio= (select concat(to_char(v_fen_fin,'YYYY'),'0101'));
                    v_nueva_fen_fin= (select concat(to_char(v_fen_fin + CAST('1 YEAR' AS INTERVAL),'YYYY'),'0101'));
 
                    IF v_nueva_fen_inicio IS NOT NULL AND v_nueva_fen_fin IS NOT NULL THEN
                    
						UPDATE t_d_fecha_negocio_etls 
						SET fen_inicio = cast(v_nueva_fen_inicio as timestamp), fen_fin = cast(v_nueva_fen_fin as timestamp), aud_fec_upd=now(), aud_user_upd='p_gf_calcularNuevaFecha' 
						WHERE etl_id=IN_ID;
                        
                    END IF;
                END IF;
			END IF;
        END IF;
	END IF;
END;
$function$
;

COMMIT;
