-- Revert postgis-vlci-vlci2:r_code/functions/3108-p-gf-averiguarejecucion from pg

BEGIN;

CREATE OR REPLACE FUNCTION vlci2.p_gf_averiguarejecucion(in_id integer, OUT out_result boolean, OUT out_status character varying)
 RETURNS record
 LANGUAGE plpgsql
AS $function$
-- Declaracion de variables
	DECLARE 
		v_control VARCHAR(2);
    	v_count INTEGER;
    	v_fen TIMESTAMP;
    	v_fen_inicio TIMESTAMP;
    	v_fen_fin TIMESTAMP;
    	v_periodicidad VARCHAR(200);
    	v_desfase INTEGER;
    	v_estado VARCHAR(200);
    	v_nombre_etl VARCHAR(200);
		v_max_fen TIMESTAMP;
		v_anyo_mes_max_fen VARCHAR(200);
		v_anyo_mes_fen_fin VARCHAR(200);
		
BEGIN
    -- Asignar valor inicial a la variable de control.
    v_control = 'OK';
    OUT_RESULT=FALSE;
    
    -- Consulta si existe una etl con el id de entrada
	v_count = (SELECT COUNT(*) FROM t_d_fecha_negocio_etls WHERE etl_id=IN_ID);
    
    IF v_count = 0 THEN
    
		v_control = 'KO';
        OUT_STATUS = 'OK';
        
	END IF;
    
    IF v_control = 'OK' THEN
    
		-- Recuperar valores de la tabla
        
		SELECT fen, fen_inicio, fen_fin, periodicidad, "offset", estado, etl_nombre 
		INTO v_fen, v_fen_inicio, v_fen_fin,v_periodicidad, v_desfase, v_estado, v_nombre_etl 
		FROM t_d_fecha_negocio_etls 
		WHERE etl_id=IN_ID;
        
		-- Evaluar estado
        
		IF v_estado = 'EN PROCESO' THEN 
        
			v_control = 'KO';
			OUT_RESULT=FALSE;
			OUT_STATUS = CONCAT('El Estado de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' esta en Proceso.');

        ELSEIF v_estado NOT IN ('ERROR','OK') THEN
        
			v_control = 'KO';
            OUT_RESULT=FALSE;
			OUT_STATUS = CONCAT('El campo Estado de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene un valor no valido.');
            
        END IF;
    END IF;
    
    -- Evalua que la periodicidad sea diezminutal, horaria, diaria, trimestral, semestral o anual
    
    IF v_control = 'OK' THEN
    
		IF v_periodicidad NOT IN ('DIEZMINUTAL','HORARIA','DIARIA','TRIMESTRAL','SEMESTRAL','ANUAL') THEN 
        
			v_control = 'KO';
            OUT_RESULT=TRUE;
            OUT_STATUS = 'OK';
            
        END IF;
        
    END IF;
    
    IF v_control = 'OK' THEN 
		
		-- Si la periodicidad es Diezminutal
		
		IF v_periodicidad = 'DIEZMINUTAL' THEN 
        
            OUT_STATUS = 'OK';
            v_max_fen = (SELECT to_char(current_timestamp - CAST(concat(v_desfase*10,' MINUTE') AS INTERVAL), 'YYYY-MM-dd HH24:mi:00'));
            
            IF v_fen < v_max_fen THEN 
            
				OUT_RESULT=TRUE; 
                
			ELSEIF v_fen >= v_max_fen THEN 
            
				OUT_RESULT=FALSE; 
                
			ELSEIF v_fen IS NULL THEN 
            
				v_control = 'KO';
                OUT_RESULT=FALSE;
				OUT_STATUS = CONCAT('El campo FEN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
            END IF;
		
		-- Si la periodicidad es Horaria
		
		ELSEIF v_periodicidad = 'HORARIA' THEN 
        
            OUT_STATUS = 'OK';
            v_max_fen = (SELECT to_char(current_timestamp - CAST(concat(v_desfase,' HOUR') AS INTERVAL), 'YYYY-MM-dd HH24:mi:00'));
            
            IF v_fen < v_max_fen THEN 
            
				OUT_RESULT=TRUE; 
                
			ELSEIF v_fen >= v_max_fen THEN 
            
				OUT_RESULT=FALSE; 
                
			ELSEIF v_fen IS NULL THEN 
            
				v_control = 'KO';
                OUT_RESULT=FALSE;
				OUT_STATUS = CONCAT('El campo FEN de la ETL "',v_nombre_etl,'" con ID: ',IN_ID,' tiene valor nulo.');
            END IF;
		
		-- Si la periodicidad es Diaria
        
        ELSEIF v_periodicidad = 'DIARIA' THEN 
        
            OUT_STATUS = 'OK';
            v_max_fen = (SELECT CURRENT_DATE - CAST(concat(v_desfase,' day') AS INTERVAL));
			
            -- Si el campo FEN es menor que la fecha maxima
            
            IF v_fen < v_max_fen THEN 
            
				OUT_RESULT=TRUE; 
                
			-- Si el campo FEN es mayor o igual a la fecha maxima
            
			ELSEIF v_fen >= v_max_fen THEN 
            
				OUT_RESULT=FALSE; 
                
            -- Si el campo FEN es nulo   
            
			ELSEIF v_fen IS NULL THEN 
            
				v_control = 'KO';
                OUT_RESULT=FALSE;
				OUT_STATUS = CONCAT('El campo FEN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
            END IF;
            
		-- Si la periodicidad es trimestral 
        
        ELSEIF v_periodicidad = 'TRIMESTRAL' THEN 
        
			-- Si la fecha inicio o fin estan a null
            
			IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
            
				v_control = 'KO';
                OUT_RESULT=FALSE;
				OUT_STATUS = CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
                
			-- Si el rango de fechas no esta a nulo
            
			ELSE
            
				OUT_STATUS = 'OK';
				v_max_fen = (SELECT CURRENT_DATE - CAST(concat(v_desfase,' day') AS INTERVAL));
				v_anyo_mes_max_fen = (SELECT to_char(v_max_fen, 'YYYYMM'));
				v_anyo_mes_fen_fin = (SELECT to_char(v_fen_fin, 'YYYYMM'));
				
                -- Si el rango de la tabla es igual o inferior a la fecha actual teniendo en cuenta el offset.
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
                
					OUT_RESULT=TRUE; 
                    
				ELSE
                
                    OUT_RESULT=FALSE; 
                    
				END IF;
			END IF;
			-- Si la periodicidad es semestral 
        
        ELSEIF v_periodicidad = 'SEMESTRAL' THEN 
        
			-- Si la fecha inicio o fin estan a null
            
			IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
            
				v_control = 'KO';
                OUT_RESULT=FALSE;
				OUT_STATUS = CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
                
			-- Si el rango de fechas no esta a nulo
            
			ELSE
            
				OUT_STATUS = 'OK';
				v_max_fen = (SELECT CURRENT_DATE - CAST(concat(v_desfase,' day') AS INTERVAL));
				v_anyo_mes_max_fen = (SELECT to_char(v_max_fen, 'YYYYMM'));
				v_anyo_mes_fen_fin = (SELECT to_char(v_fen_fin, 'YYYYMM'));
				
                -- Si el rango de la tabla es igual o inferior a la fecha actual teniendo en cuenta el offset.
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
                
					OUT_RESULT=TRUE; 
                    
				ELSE
                
                    OUT_RESULT=FALSE; 
                    
				END IF;
			END IF;
		
		-- Si la periodicidad es anual 
		ELSEIF v_periodicidad = 'ANUAL' THEN 
        
			-- Si la fecha inicio o fin estan a null
            
			IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
            
				v_control = 'KO';
                OUT_RESULT=FALSE;
				OUT_STATUS = CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ',v_nombre_etl,' con ID: ',IN_ID,' tiene valor nulo.');
                
			-- Si el rango de fechas no esta a nulo
            
			ELSE
            
				OUT_STATUS = 'OK';
				v_max_fen = (SELECT CURRENT_DATE - CAST(concat(v_desfase,' day') AS INTERVAL));
				v_anyo_mes_max_fen = (SELECT to_char(v_max_fen, 'YYYYMM'));
				v_anyo_mes_fen_fin = (SELECT to_char(v_fen_fin, 'YYYYMM'));
				
                -- Si el rango de la tabla es igual o inferior a la fecha actual teniendo en cuenta el offset.
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
                
					OUT_RESULT=TRUE; 
                    
				ELSE
                
                    OUT_RESULT=FALSE; 
                    
				END IF;
			END IF;
		END IF;
	END IF;
END;
$function$
;

COMMIT;
