-- Revert postgis-vlci-vlci2:r_code/functions/f.t_datos_etl_estadistica_log_before_update from pg

BEGIN;

DROP FUNCTION fn_t_datos_etl_estadistica_log_before_update() CASCADE;

COMMIT;
