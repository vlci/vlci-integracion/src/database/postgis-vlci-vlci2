-- Revert postgis-vlci-vlci2:r_code/procedures/p.delete_from_table_by_id from pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.delete_from_table_by_id(
    IN in_table_name character varying,
    IN in_months integer,
    IN in_colfecha character varying
)
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $procedure$
begin
    EXECUTE format(
        'DELETE FROM %I WHERE 
        ' || IN_COLFECHA ||' < (NOW() - INTERVAL %L)',
        IN_TABLE_NAME, IN_MONTHS || ' MONTH');
END
$procedure$;

COMMIT;
