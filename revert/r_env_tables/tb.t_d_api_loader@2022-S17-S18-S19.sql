-- Revert postgis-vlci-vlci2:r_env_tables/tb.t_d_api_loader from pg

BEGIN;

DO $$
DECLARE
	current_db varchar(200);
BEGIN
	-- PRE-CONDITIONS
	ALTER TABLE vlci2.t_d_api_loader_cabeceras DROP CONSTRAINT IF EXISTS t_d_api_loader_cabeceras_ibfk_1;

	-- Data
	truncate table vlci2.t_d_api_loader;

	SELECT current_database()
	INTO current_db;

	IF (current_db = 'sc_vlci_int') THEN

		INSERT INTO vlci2.t_d_api_loader (orden,nombre_api,metodo,url_base,nombre_recurso,ruta_destino,ruta_destino_backup,fec_ultima_ejecucion,periodicidad,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
			(0,'Airwave_Wifi_login','POST','https://172.23.253.32/LOGIN','','','','2019-12-10 08:50:51','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(3,'Airwave_Wifi_ap_detail','GET','https://172.23.253.32/ap_detail.xml?','Wifi_ap_detail_YYYY-MM-DD-HH-MM-SS.xml','/integracion/04.fichGen/Wifi_API/','/integracion/02.fichBackup/Wifi_API/','2021-09-14 09:45:53.681711','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(4,'Airwave_Wifi_ap_detail_urbo','GET','https://172.23.253.32/ap_detail.xml?','Wifi_ap_detail_YYYY-MM-DD-HH-MM-SS.xml','/integracion/01.fichOrig/Wifi_API_urbo_POI_RT/','/integracion/02.fichBackup/Wifi_API_urbo_POI_RT/','2021-09-14 09:45:53.701761','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(1,'Airwave_Wifi_ap_list','GET','https://172.23.253.32/ap_list.xml','Wifi_ap_list_YYYY-MM-DD-HH-MM-SS.xml','/integracion/01.fichOrig/Wifi_API/','/integracion/02.fichBackup/Wifi_API/','2021-09-14 09:45:53.704497','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(2,'Airwave_Wifi_ap_list_urbo','GET','https://172.23.253.32/ap_list.xml','Wifi_ap_list_YYYY-MM-DD-HH-MM-SS.xml','/integracion/01.fichOrig/Wifi_API_urbo_AP/','/integracion/02.fichBackup/Wifi_API_urbo_AP/','2021-09-14 09:45:53.707648','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(7,'Airwave_Wifi_ap_list_urbo_poi','GET','https://172.23.253.32/ap_list.xml','Wifi_ap_list_YYYY-MM-DD-HH-MM-SS.xml','/integracion/01.fichOrig/Wifi_API_urbo_POI_RT/','/integracion/02.fichBackup/Wifi_API_urbo_POI_RT/','2021-09-14 09:45:53.712228','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(5,'Airwave_Wifi_client_detail','GET','https://172.23.253.32/client_detail.xml?','Wifi_client_detail_YYYY-MM-DD-HH-MM-SS.xml','/integracion/04.fichGen/Wifi_API/','/integracion/02.fichBackup/Wifi_API/','2021-09-14 09:45:53.717741','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(6,'Airwave_Wifi_client_detail_urbo','GET','https://172.23.253.32/client_detail.xml?','Wifi_client_detail_YYYY-MM-DD-HH-MM-SS.xml','/integracion/01.fichOrig/Wifi_API_urbo_POI_DR/','/integracion/02.fichBackup/Wifi_API_urbo_POI_DR/','2021-09-14 09:45:53.719822','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(NULL,'Jardineria_Inspeccion','POST','https://services5.arcgis.com/9PlvG9CMG7lt0I0D/ArcGIS/rest/services/Inspeccion_Jardineria_V4_VLCi/FeatureServer/0/query','Jardineria_Inspeccion_[Anyo]_[Trimestre].json','/integracion/01.fichOrig/Jardines_Calidad/','/integracion/02.fichBackup/Jardines_Calidad/','2021-09-14 09:45:53.721818','Trimestral','2019-03-20 09:32:33','Ivan','2019-03-20 09:32:33','Ivan');

	ELSIF (current_db = 'sc_vlci_pre' ) THEN

		INSERT INTO vlci2.t_d_api_loader (orden,nombre_api,metodo,url_base,nombre_recurso,ruta_destino,ruta_destino_backup,fec_ultima_ejecucion,periodicidad,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
			(0,'Airwave_Wifi_login','POST','https://172.23.253.32/LOGIN','','','','2019-11-25 08:29:40','Diaria','2019-11-25 08:29:40','34067','2019-11-25 08:29:40','34067'),
			(3,'Airwave_Wifi_ap_detail','GET','https://172.23.253.32/ap_detail.xml?','Wifi_ap_detail_YYYY-MM-DD-HH-MM-SS.xml','/pre/04.fichGen/Wifi_API/','/pre/02.fichBackup/Wifi_API/','2021-09-10 14:41:11.415585','Diaria','2019-11-25 08:29:40','34067','2019-11-25 08:29:40','34067'),
			(4,'Airwave_Wifi_ap_detail_urbo','GET','https://172.23.253.32/ap_detail.xml?','Wifi_ap_detail_YYYY-MM-DD-HH-MM-SS.xml','/pre/01.fichOrig/Wifi_API_urbo_POI_RT/','/pre/02.fichBackup/Wifi_API_urbo_POI_RT/','2021-09-10 14:41:11.438463','Diaria','2019-11-25 08:29:40','34067','2019-11-25 08:29:40','34067'),
			(1,'Airwave_Wifi_ap_list','GET','https://172.23.253.32/ap_list.xml','Wifi_ap_list_YYYY-MM-DD-HH-MM-SS.xml','/pre/01.fichOrig/Wifi_API/','/pre/02.fichBackup/Wifi_API/','2021-09-10 14:41:11.458308','Diaria','2019-11-25 08:29:40','34067','2019-11-25 08:29:40','34067'),
			(2,'Airwave_Wifi_ap_list_urbo','GET','https://172.23.253.32/ap_list.xml','Wifi_ap_list_YYYY-MM-DD-HH-MM-SS.xml','/pre/01.fichOrig/Wifi_API_urbo_AP/','/pre/02.fichBackup/Wifi_API_urbo_AP/','2021-09-10 14:41:11.472772','Diaria','2019-11-25 08:29:40','34067','2019-11-25 08:29:40','34067'),
			(7,'Airwave_Wifi_ap_list_urbo_poi','GET','https://172.23.253.32/ap_list.xml','Wifi_ap_list_YYYY-MM-DD-HH-MM-SS.xml','/pre/01.fichOrig/Wifi_API_urbo_POI_RT/','/pre/02.fichBackup/Wifi_API_urbo_POI_RT/','2021-09-10 14:41:11.488318','Diaria','2019-11-25 08:29:40','34067','2019-11-25 08:29:40','34067'),
			(5,'Airwave_Wifi_client_detail','GET','https://172.23.253.32/client_detail.xml?','Wifi_client_detail_YYYY-MM-DD-HH-MM-SS.xml','/pre/04.fichGen/Wifi_API/','/pre/02.fichBackup/Wifi_API/','2021-09-10 14:41:11.504411','Diaria','2019-11-25 08:29:40','34067','2019-11-25 08:29:40','34067'),
			(6,'Airwave_Wifi_client_detail_urbo','GET','https://172.23.253.32/client_detail.xml?','Wifi_client_detail_YYYY-MM-DD-HH-MM-SS.xml','/pre/01.fichOrig/Wifi_API_urbo_POI_DR/','/pre/02.fichBackup/Wifi_API_urbo_POI_DR/','2021-09-10 14:41:11.519123','Diaria','2019-11-25 08:29:40','34067','2019-11-25 08:29:40','34067'),
			(NULL,'Jardineria_Inspeccion','POST','https://services5.arcgis.com/9PlvG9CMG7lt0I0D/ArcGIS/rest/services/Inspeccion_Jardineria_V4_VLCi/FeatureServer/0/query','Jardineria_Inspeccion_[Anyo]_[Trimestre].json','/pre/01.fichOrig/Jardines_Calidad/','/pre/02.fichBackup/Jardines_Calidad/','2021-09-10 14:53:01.272648','Trimestral','2019-03-20 09:32:33','Ivan','2019-03-20 09:32:33','Ivan');

	ELSE 

		INSERT INTO vlci2.t_d_api_loader (orden,nombre_api,metodo,url_base,nombre_recurso,ruta_destino,ruta_destino_backup,fec_ultima_ejecucion,periodicidad,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
			(0,'Airwave_Wifi_login','POST','https://172.23.253.32/LOGIN','','','','2019-12-10 08:50:51','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(3,'Airwave_Wifi_ap_detail','GET','https://172.23.253.32/ap_detail.xml?','Wifi_ap_detail_YYYY-MM-DD-HH-MM-SS.xml','/prod/04.fichGen/Wifi_API/','/prod/02.fichBackup/Wifi_API/','2021-09-14 09:45:53.681711','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(4,'Airwave_Wifi_ap_detail_urbo','GET','https://172.23.253.32/ap_detail.xml?','Wifi_ap_detail_YYYY-MM-DD-HH-MM-SS.xml','/prod/01.fichOrig/Wifi_API_urbo_POI_RT/','/prod/02.fichBackup/Wifi_API_urbo_POI_RT/','2021-09-14 09:45:53.701761','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(1,'Airwave_Wifi_ap_list','GET','https://172.23.253.32/ap_list.xml','Wifi_ap_list_YYYY-MM-DD-HH-MM-SS.xml','/prod/01.fichOrig/Wifi_API/','/prod/02.fichBackup/Wifi_API/','2021-09-14 09:45:53.704497','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(2,'Airwave_Wifi_ap_list_urbo','GET','https://172.23.253.32/ap_list.xml','Wifi_ap_list_YYYY-MM-DD-HH-MM-SS.xml','/prod/01.fichOrig/Wifi_API_urbo_AP/','/prod/02.fichBackup/Wifi_API_urbo_AP/','2021-09-14 09:45:53.707648','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(7,'Airwave_Wifi_ap_list_urbo_poi','GET','https://172.23.253.32/ap_list.xml','Wifi_ap_list_YYYY-MM-DD-HH-MM-SS.xml','/prod/01.fichOrig/Wifi_API_urbo_POI_RT/','/prod/02.fichBackup/Wifi_API_urbo_POI_RT/','2021-09-14 09:45:53.712228','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(5,'Airwave_Wifi_client_detail','GET','https://172.23.253.32/client_detail.xml?','Wifi_client_detail_YYYY-MM-DD-HH-MM-SS.xml','/prod/04.fichGen/Wifi_API/','/prod/02.fichBackup/Wifi_API/','2021-09-14 09:45:53.717741','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(6,'Airwave_Wifi_client_detail_urbo','GET','https://172.23.253.32/client_detail.xml?','Wifi_client_detail_YYYY-MM-DD-HH-MM-SS.xml','/prod/01.fichOrig/Wifi_API_urbo_POI_DR/','/prod/02.fichBackup/Wifi_API_urbo_POI_DR/','2021-09-14 09:45:53.719822','Diaria','2019-12-10 08:50:51','34067','2019-12-10 08:50:51','34067'),
			(NULL,'Jardineria_Inspeccion','POST','https://services5.arcgis.com/9PlvG9CMG7lt0I0D/ArcGIS/rest/services/Inspeccion_Jardineria_V4_VLCi/FeatureServer/0/query','Jardineria_Inspeccion_[Anyo]_[Trimestre].json','/prod/01.fichOrig/Jardines_Calidad/','/prod/02.fichBackup/Jardines_Calidad/','2021-09-14 09:45:53.721818','Trimestral','2019-03-20 09:32:33','Ivan','2019-03-20 09:32:33','Ivan');

	END IF;

	-- POST-CONDITIONS
	ALTER TABLE vlci2.t_d_api_loader_cabeceras ADD CONSTRAINT t_d_api_loader_cabeceras_ibfk_1 FOREIGN KEY (nombre_api) REFERENCES vlci2.t_d_api_loader(nombre_api) ON DELETE RESTRICT ON UPDATE RESTRICT;
END
$$ LANGUAGE PLPGSQL;



COMMIT;
