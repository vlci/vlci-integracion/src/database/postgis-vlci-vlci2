-- Revert postgis-vlci-vlci2:r_env_tables/tb.t_d_parametros from pg

BEGIN;

DO $$
DECLARE
	current_db varchar(200);
BEGIN
	-- Data
	truncate table vlci2.t_d_parametros;

	SELECT current_database()
	INTO current_db;

	IF (current_db = 'sc_vlci_int') THEN

		INSERT INTO vlci2.t_d_parametros (param_clave,param_valor) VALUES
			('url_cdmu','https://dashboard.vlci.valencia.es/cdmge/asp/Main.aspx?Project=Valencia+CdMGE&evt=2048001&src=Main.aspx.2048001&documentID=85F5B3DC4EBE71BE3FA77E911B0EFF84&Server=PRO'),
			('url_cdmt','https://dashboard.vlci.valencia.es/cdmt/asp/Main.aspx?Project=Valencia+CdMT&evt=2048001&src=Main.aspx.2048001&documentID=B20E938043ECDBE6AD98309F19EE72F3&Server=PRO'),
			('url_cdmpm','https://dashboard-pre.vlci.valencia.es/cdmcovid19/asp/Main.aspx?Project=Valencia+COVID+19&evt=2048001&src=Main.aspx.2048001&documentID=BAAB60204F55B7B3287F48B474006EAD&Server=PRE'),
			('url_manual','https://opendata.vlci.valencia.es/dataset/c97f8f79-8e16-4b4d-9cfb-f06e7fc5e251/resource/3ec6e893-49d4-4246-8d78-46a78f41705f/download/manual-cdm-unificado-ciudad-v3.pptx');

	ELSIF (current_db = 'sc_vlci_pre' ) THEN

		INSERT INTO vlci2.t_d_parametros (param_clave,param_valor) VALUES
			('url_cdmu','https://dashboard.vlci.valencia.es/cdmge/asp/Main.aspx?Project=Valencia+CdMGE&evt=2048001&src=Main.aspx.2048001&documentID=85F5B3DC4EBE71BE3FA77E911B0EFF84&Server=PRO'),
			('url_cdmt','https://dashboard.vlci.valencia.es/cdmt/asp/Main.aspx?Project=Valencia+CdMT&evt=2048001&src=Main.aspx.2048001&documentID=B20E938043ECDBE6AD98309F19EE72F3&Server=PRO'),
			('url_cdmpm','https://dashboard-pre.vlci.valencia.es/cdmcovid19/asp/Main.aspx?Project=Valencia+COVID+19&evt=2048001&src=Main.aspx.2048001&documentID=BAAB60204F55B7B3287F48B474006EAD&Server=PRE'),
			('url_manual','https://opendata.vlci.valencia.es/dataset/c97f8f79-8e16-4b4d-9cfb-f06e7fc5e251/resource/3ec6e893-49d4-4246-8d78-46a78f41705f/download/manual-cdm-unificado-ciudad-v3.pptx');

	ELSE 

		INSERT INTO vlci2.t_d_parametros (param_clave,param_valor) VALUES
			('url_cdmu','https://dashboard.vlci.valencia.es/cdmge/asp/Main.aspx?Project=Valencia+CdMGE&evt=2048001&src=Main.aspx.2048001&documentID=85F5B3DC4EBE71BE3FA77E911B0EFF84&Server=PRO'),
			('url_cdmt','https://dashboard.vlci.valencia.es/cdmt/asp/Main.aspx?Project=Valencia+CdMT&evt=2048001&src=Main.aspx.2048001&documentID=B20E938043ECDBE6AD98309F19EE72F3&Server=PRO'),
			('url_cdmpm','https://dashboard-pre.vlci.valencia.es/cdmcovid19/asp/Main.aspx?Project=Valencia+COVID+19&evt=2048001&src=Main.aspx.2048001&documentID=BAAB60204F55B7B3287F48B474006EAD&Server=PRE'),
			('url_manual','https://opendata.vlci.valencia.es/dataset/c97f8f79-8e16-4b4d-9cfb-f06e7fc5e251/resource/3ec6e893-49d4-4246-8d78-46a78f41705f/download/manual-cdm-unificado-ciudad-v3.pptx');

	END IF;

END
$$ LANGUAGE PLPGSQL;


COMMIT;
