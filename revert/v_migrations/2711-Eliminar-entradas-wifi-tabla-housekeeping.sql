-- Revert postgis-vlci-vlci2:v_migrations/2711-Eliminar-entradas-wifi-tabla-housekeeping from pg

BEGIN;

INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_agg_conexiones_wifi_urbo', 1, 'fe_fecha_carga');
INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_f_aps_agg', 3, 'aud_fec_ins');
INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_f_ap_avisos', 3, 'fe_fecha_carga');
INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_f_ap_detail_rt', 1, 'fe_fecha_carga');
INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_f_ap_urbo', 1, 'ap_state_upd');
INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_f_conexiones_wifi_urbo', 1, 'aud_fec_ins');
INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_f_num_poi_agg', 3, 'aud_fec_ins');
INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_f_poi_rt', 1, 'fe_fecha_carga');
INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_f_poi_urbo_di', 1, 'dlNumberOfUsersConnectedUpdatedAt');
INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_f_poi_urbo_hr', 1, 'hrNumberOfUsersConnectedUpdatedAt');
INSERT INTO vlci2.housekeeping_config
(table_nam, tiempo, colfecha)
VALUES('t_f_poi_urbo_rt', 1, 'fe_fecha_carga');

COMMIT;
