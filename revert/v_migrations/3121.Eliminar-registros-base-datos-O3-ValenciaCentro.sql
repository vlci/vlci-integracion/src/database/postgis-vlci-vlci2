-- Revert postgis-vlci-vlci2:v_migrations/3121.Eliminar-registros-base-datos-O3-ValenciaCentro from pg

BEGIN;

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET o3value = 55,
    o3valueflag = 'V'
WHERE dateobserved BETWEEN '2021-07-18 00:00:00.000' AND '2022-12-31 23:59:59.999'
  AND refpointofinterest = 'A07_VALENCIACENTRE';

COMMIT;
