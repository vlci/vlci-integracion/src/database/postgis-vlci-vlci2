-- Revert postgis-vlci-vlci2:v_migrations/2349-fix-monthly-validated-data-table from pg

BEGIN;

drop table if EXISTS vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes;

-- MES
CREATE TABLE vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (
    campo VARCHAR(50),
    tipo VARCHAR(50),
    nulo VARCHAR(50),
    pk VARCHAR(50)
);

INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('recvtime', 'timestamptz', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('fiwareservicepath', 'text', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('entityid', 'text', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('entitytype', 'text', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('originalentityid', 'text', 'NOT NULL', 'pk');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('originalentitytype', 'text', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('originalservicepath', 'text', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('calculationperiod', 'text', 'NOT NULL', 'pk');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('description', 'text', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('name', 'text', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('location', 'geometry(point)', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('kpivalue', 'numeric', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('operationalStatus', 'text', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('updatedAt', 'timestamptz', 'NULL', '');
INSERT INTO vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (campo, tipo, nulo, pk) VALUES ('measurelandunit', 'text', 'NULL', '');


COMMIT;
