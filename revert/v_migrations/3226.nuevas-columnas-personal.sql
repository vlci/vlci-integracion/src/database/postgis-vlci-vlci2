-- Revert postgis-vlci-vlci2:v_migrations/3226.nuevas-columnas-personal from pg

BEGIN;

ALTER TABLE vlci2.t_datos_etl_personal_ayuntamiento
DROP COLUMN IF EXISTS ocupada_vacante,
DROP COLUMN IF EXISTS numero,
DROP COLUMN IF EXISTS area_adagp;

ALTER TABLE vlci2.t_datos_etl_personal_ayuntamiento
RENAME TO t_d_personal;

COMMIT;
