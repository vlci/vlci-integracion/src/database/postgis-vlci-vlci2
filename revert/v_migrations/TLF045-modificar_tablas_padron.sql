-- Revert postgis-vlci-vlci2:v_migrations/TLF045-modificar_tablas_padron from pg

BEGIN;

drop table t_datos_manual_padron_edad cascade ;
CREATE TABLE t_datos_manual_padron_edad AS 
    SELECT
    gender,
    age,
    SUM(h) AS total
    FROM (
    SELECT
        'Hombre' as gender,
        unnest(ARRAY[h_1, h_2, h_3, h_4, h_5, h_6, h_7, h_8, h_9, h_10, h_11, h_12, h_13, h_14, h_15, h_16, h_17, h_18, h_19]) AS h,
        unnest(ARRAY['0-4', '5-9', '10-14', '15-19', '20-24', '25-29', '30-34', '35-39', '40-44', '45-49', '50-54', '55-59', '60-64', '65-69', '70-74', '75-79', '80-84', '85-89', '90-94','+95']) AS age
    FROM t_datos_manual_padron
    ) AS unpivoted_table
    GROUP BY
    gender, age
    union 
    SELECT
    gender,
    age,
    SUM(m) AS total
    FROM (
    SELECT
        'Mujer' as gender,
        unnest(ARRAY[m_1, m_2, m_3, m_4, m_5, m_6, m_7, m_8, m_9, m_10, m_11, m_12, m_13, m_14, m_15, m_16, m_17, m_18, m_19]) AS m,
        unnest(ARRAY['0-4', '5-9', '10-14', '15-19', '20-24', '25-29', '30-34', '35-39', '40-44', '45-49', '50-54', '55-59', '60-64', '65-69', '70-74', '75-79', '80-84', '85-89', '90-94','+95']) AS age
    FROM t_datos_manual_padron
    ) AS unpivoted_table
    GROUP BY
    gender, age
    order by age;

drop table t_datos_manual_padron_estudios cascade ;
CREATE TABLE t_datos_manual_padron_estudios AS
    SELECT
    gender,
    academic_level,
    SUM(total) AS total
    FROM (
    SELECT
        'Hombre' as gender,
        unnest(ARRAY[hombre_estudios_0,hombre_estudios_1,hombre_estudios_2,hombre_estudios_3,hombre_estudios_4, hombre_estudios_9]) AS total,
        unnest(ARRAY['Menor 18 años', 'Ni leer ni escribir', 'Inferior Graduado', 'Graduado escolar', 'Bachiller/FP/Universidad', 'Desconocido']) AS academic_level
    FROM t_datos_manual_padron
    ) AS unpivoted_table
    GROUP BY
    gender, academic_level
    union
    SELECT
    gender,
    academic_level,
    SUM(total) AS total
    FROM (
    SELECT
        'Mujer' as gender,
        unnest(ARRAY[mujer_estudios_0,mujer_estudios_1,mujer_estudios_2,mujer_estudios_3,mujer_estudios_4, mujer_estudios_9]) AS total,
        unnest(ARRAY['Menor 18 años', 'Ni leer ni escribir', 'Inferior Graduado', 'Graduado escolar', 'Bachiller/FP/Universidad', 'Desconocido']) AS academic_level  
        FROM t_datos_manual_padron
    ) AS unpivoted_table
    GROUP BY
    gender, academic_level;

drop table t_datos_manual_padron_nacionalidad cascade ;
CREATE TABLE t_datos_manual_padron_nacionalidad AS 
    SELECT
    gender,
    nac,
    SUM(h) AS total
    FROM (
    SELECT
        'Hombre' as gender,
        unnest(ARRAY[hombre_nacionalidad_0,hombre_nacionalidad_1,hombre_nacionalidad_2,hombre_nacionalidad_3,hombre_nacionalidad_4,hombre_nacionalidad_5,hombre_nacionalidad_6,hombre_nacionalidad_7,hombre_nacionalidad_8]) AS h,
        unnest(ARRAY['Española', 'Resto UE(27)', 'Resto Europa', 'África', 'América del Norte', 'América Central', 'América del Sur', 'Asia', 'Oceanía y apátridas']) AS nac
    FROM t_datos_manual_padron
    ) AS unpivoted_table
    GROUP BY
    gender, nac
    union 
    SELECT
    gender,
    nac,
    SUM(m) AS total
    FROM (
    SELECT
        'Mujer' as gender,
        unnest(ARRAY[mujer_nacionalidad_0,mujer_nacionalidad_1,mujer_nacionalidad_2,mujer_nacionalidad_3,mujer_nacionalidad_4,mujer_nacionalidad_5,mujer_nacionalidad_6,mujer_nacionalidad_7,mujer_nacionalidad_8]) AS m,
        unnest(ARRAY['Española', 'Resto UE(27)', 'Resto Europa', 'África', 'América del Norte', 'América Central', 'América del Sur', 'Asia', 'Oceanía y apátridas']) AS nac  FROM t_datos_manual_padron
    ) AS unpivoted_table
    GROUP BY
    gender, nac
    order by nac;  

drop table t_datos_manual_padron_nacimientos cascade ;
CREATE TABLE t_datos_manual_padron_nacimientos AS 
    SELECT
    gender,
    nac,
    SUM(h) AS total
    FROM (
    SELECT
        'Hombre' as gender,
        unnest(ARRAY[hombre_nacimiento_1,hombre_nacimiento_2,hombre_nacimiento_3,hombre_nacimiento_4,hombre_nacimiento_5,hombre_nacimiento_6]) AS h,
        unnest(ARRAY['València', 'Resto de l`Horta', 'Resto de la Comunitat', 'Resto del Estado', 'En el extranjero', 'No consta']) AS nac
    FROM t_datos_manual_padron
    ) AS unpivoted_table
    GROUP BY
    gender, nac
    union 
    SELECT
    gender,
    nac,
    SUM(m) AS total
    FROM (
    SELECT
        'Mujer' as gender,
        unnest(ARRAY[mujer_nacimiento_1,mujer_nacimiento_2,mujer_nacimiento_3,mujer_nacimiento_4,mujer_nacimiento_5,mujer_nacimiento_6]) AS m,
        unnest(ARRAY['València', 'Resto de l`Horta', 'Resto de la Comunitat', 'Resto del Estado', 'En el extranjero', 'No consta']) AS nac
        FROM t_datos_manual_padron
    ) AS unpivoted_table
    GROUP BY
    gender, nac
    order by nac;


COMMIT;
