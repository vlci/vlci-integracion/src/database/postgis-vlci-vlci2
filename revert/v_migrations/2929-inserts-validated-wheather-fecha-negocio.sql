-- Revert postgis-vlci-vlci2:v_migrations/2929-inserts-validated-wheather-fecha-negocio from pg

BEGIN;

delete from vlci2.t_d_fecha_negocio_etls where etl_nombre in ('W02_NAZARET_precipitaciones_va', 'W01_AVFRANCIA_temperatura_va', 'W02_NAZARET_precipitaciones_vm', 'W01_AVFRANCIA_temperatura_vm');

COMMIT;
