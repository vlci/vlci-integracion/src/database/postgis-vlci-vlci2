-- Revert postgis-vlci-vlci2:v_migrations/3441.borrar-tabla-t_d_parametros_etl_avisos from pg

BEGIN;

CREATE TABLE vlci2.t_d_parametros_etl_avisos (
	servicio varchar(200) NOT NULL,
	nombre_largo_servicio varchar(200) NOT NULL,
	responsable varchar(200) NOT NULL,
	fec_ini_aviso date NOT NULL,
	fec_fin_aviso date NULL,
	periodicidad int4 NOT NULL,
	repeticiones int4 NOT NULL,
	recordatorio int4 NOT NULL,
	destinatario varchar(300) NOT NULL,
	nombre_destinatario varchar(200) NOT NULL,
	ruta_plantilla varchar(200) NOT NULL,
	nombre_plantilla varchar(200) NOT NULL,
	cdmge varchar(1) NULL,
	tabla varchar(100) NULL,
	CONSTRAINT idx_86618_primary PRIMARY KEY (servicio, nombre_plantilla)
);

INSERT INTO vlci2.t_d_parametros_etl_avisos (servicio,nombre_largo_servicio,responsable,fec_ini_aviso,fec_fin_aviso,periodicidad,repeticiones,recordatorio,destinatario,nombre_destinatario,ruta_plantilla,nombre_plantilla,cdmge,tabla) VALUES
	 ('Alumbrado','Servicio de Alumbrado','Vicente Mayans','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Vicente Mayans','//dades2/DADES2/ayun/SCT/Alumbrado/Plataforma VLCi /SERTIC/','DatosAnualesAlumbrado.xlsx',NULL,NULL),
	 ('Alumbrado','Servicio de Alumbrado','Vicente Mayans','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Vicente Mayans','//dades2/DADES2/ayun/SCT/Alumbrado/Plataforma VLCi /SERTIC/','Datos_VLCi_TablaContaminacion.xlsx',NULL,NULL),
	 ('Alumbrado','Servicio de Alumbrado','Vicente Mayans','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Vicente Mayans','//dades2/DADES2/ayun/SCT/Alumbrado/Plataforma VLCi /SERTIC/','Datos_VLCi_TablaLumenes.xlsx',NULL,NULL),
	 ('Bomberos','Inspector Jefe del Departamento de Bomberos, Prevención e Intervención en Emergencias y Protección Civil','Eloy Bonilla','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Eloy Bonilla','//speis-oes2/VOL1/ayun/Bombero/Plataforma VLCi','Datos_VLCi_Bomberos.xlsx',NULL,NULL),
	 ('CClimático','Servicio de Energias Renovables y Cambio Climático','Eloy Bonilla','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Eloy Bonilla','//dades1/DADES1/ayun/Cambio Climatico/VARIOS/Plataforma VLCi','Datos_VLCi_Cambio_Climatico.xlsx',NULL,NULL),
	 ('CIA','Servicio del Ciclo Integral del Agua','Eloy Bonilla','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Eloy Bonilla','//dades1/DADES1/ayun/Ciclo Integral Agua/Plataforma VLCi','Datos_VLCi_CicloAgua.xlsx',NULL,NULL),
	 ('Circulación','Servicio de Mobilidad Sostenible','Eloy Bonilla','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Eloy Bonilla','//dades2/DADES2/ayun/TRAFICO/Plataforma VLCi','Datos_VLCi_Circulacion_Indicadores.xlsx',NULL,NULL),
	 ('Contratacion','Servicio de Contratacion','Equipo Integracion','2021-09-28',NULL,0,0,0,'integracionvlci@valencia.es','','','Expedientes de contratación','S','t_f_expedientes_contratacion'),
	 ('Estadística','Oficina de Estadística','Eloy Bonilla','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Eloy Bonilla','//dades2/dades2/ayun/CENTEINF/Estadi/Plataforma VLCi','Datos_VLCi_Estadistica.xlsx',NULL,NULL),
	 ('GACO','Gasto Corriente','Equipo Integracion','2021-09-08',NULL,0,0,0,'integracionvlci@valencia.es','','','GACO','S','t_f_gasto_corriente');
INSERT INTO vlci2.t_d_parametros_etl_avisos (servicio,nombre_largo_servicio,responsable,fec_ini_aviso,fec_fin_aviso,periodicidad,repeticiones,recordatorio,destinatario,nombre_destinatario,ruta_plantilla,nombre_plantilla,cdmge,tabla) VALUES
	 ('OrdUrb','Servicio de Gestión Urbanística','Eloy Bonilla','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Eloy Bonilla','//dades3/DADES3/ayun/Planeamiento/Cartografia/Plataforma VLCi','Datos_VLCi_Ordenacion_Urbanistica.xlsx',NULL,NULL),
	 ('PMP','Pago Medio a Proveedores','Equipo Integracion','2021-09-25',NULL,0,0,0,'integracionvlci@valencia.es','','','PMP','S','t_f_pmp'),
	 ('PMP_E','Pago Medio a Proveedores por Entidad','Equipo Integracion','2021-09-25',NULL,0,0,0,'integracionvlci@valencia.es','','','PMP Entidad','S','t_agg_pmp_ua'),
	 ('Residuos','Servicio de Gestión de Residuos Urbanos y Limpieza','Eloy Bonilla','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Eloy Bonilla','//dades1/DADES1/ayun/Residuos/CENTRAL/Plataforma VLCi','Datos_VLCi_Residuos.xlsx',NULL,NULL),
	 ('SSociales','Servicio de Bienestar Social e Integración','Eloy Bonilla','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Eloy Bonilla','//dades2/DADES2/ayun/Sociales/Estudios y planificacion/PlataformaVLCi','Datos_VLCi_Bienestar.xlsx',NULL,NULL),
	 ('TBOR','Codigos Organicos','Equipo Integracion','2021-09-08',NULL,0,0,0,'integracionvlci@valencia.es','','','TBOR','S','t_d_codigos_organicos'),
	 ('Turismo','Servicio de Promoción Económica, Internacionalización y Turismo','Eloy Bonilla','2022-05-31','2022-06-30',7,4,365,'integracionvlci@valencia.es','Eloy Bonilla','//home/iot_val_01','Datos_VLCi_Turismo.xlsx',NULL,NULL);

COMMIT;
