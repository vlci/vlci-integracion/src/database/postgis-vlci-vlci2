-- Revert postgis-vlci-vlci2:v_migrations/2981-crear-tablas-temporales-datadis from pg

BEGIN;

DROP TABLE IF EXISTS vlci2.t_datos_etl_extraccion_datadis_suministros_temporal;
DROP TABLE IF EXISTS vlci2.t_datos_etl_extraccion_datadis_consumos_municipales_temporal;

COMMIT;
