-- Revert postgis-vlci-vlci2:v_migrations/2955-cambiar-nombre-tabla-policia from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_policia_intervenciones_lastdata RENAME TO t_datos_cb_policia_intervenciones_last_data;

COMMIT;
