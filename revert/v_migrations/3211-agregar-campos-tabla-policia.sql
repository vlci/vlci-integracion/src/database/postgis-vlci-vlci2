-- Revert postgis-vlci-vlci2:v_migrations/3211-agregar-campos-tabla-policia from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_policia_intervenciones_lastdata
DROP COLUMN planned,
DROP COLUMN alertSource,
DROP COLUMN externalIntervention,
DROP COLUMN isDeleted;

COMMIT;
