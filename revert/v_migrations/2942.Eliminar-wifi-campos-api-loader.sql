-- Revert postgis-vlci-vlci2:v_migrations/2942.Eliminar-wifi-campos-api-loader from pg

BEGIN;

INSERT INTO vlci2.t_d_api_loader_campos_request (nombre_api,campo,valor,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 ('Airwave_Wifi_login','destination','/','2022-10-10 09:15:44.95975',NULL,NULL,NULL),
	 ('Airwave_Wifi_login','login','Log In','2022-10-10 09:15:44.95975',NULL,NULL,NULL);

COMMIT;
