-- Revert postgis-vlci-vlci2:v_migrations/2998-cambiar-clave-primaria-aguas-desag from pg

BEGIN;

-- Eliminar la nueva clave primaria con originalentityid
ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_desag_corregida
    DROP CONSTRAINT t_datos_cb_cia_inyeccion_agua_desag_corregida_pkey;

-- Restaurar la clave primaria original con entityid
ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_desag_corregida
    ADD CONSTRAINT t_datos_cb_cia_inyeccion_agua_desag_corregida_pkey PRIMARY KEY (calculationperiod, entityid);

COMMIT;
