-- Revert postgis-vlci-vlci2:v_migrations/2962.eliminar_datos_antiguos_clima_rt from pg
BEGIN;

DELETE FROM vlci2.housekeeping_config
WHERE
  (
    table_nam = 't_f_text_kpi_temperatura_media_diaria'
    AND tiempo = 6
    AND colfecha = 'calculationperiod'
  )
  OR (
    table_nam = 't_f_text_kpi_precipitaciones_diarias'
    AND tiempo = 6
    AND colfecha = 'calculationperiod'
  );

COMMIT;