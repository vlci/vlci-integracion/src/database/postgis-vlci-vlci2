-- Revert postgis-vlci-vlci2:v_migrations/1240.historico_sonometros_hopu from pg

BEGIN;

DELETE FROM vlci2.t_datos_cb_sonometros_hopvlci_daily 
WHERE dateobserved < '2022-12-20';

COMMIT;
