-- Revert postgis-vlci-vlci2:v_migrations/2662-add-area-fk-t-d-servicio from pg

BEGIN;

ALTER TABLE vlci2.t_d_servicio drop column if exists fk_area_id;

COMMIT;
