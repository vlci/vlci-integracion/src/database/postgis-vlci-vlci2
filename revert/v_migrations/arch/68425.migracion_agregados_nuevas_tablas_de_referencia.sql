-- Revert postgis-vlci-vlci2:v_migrations/68425.migracion_agregados_nuevas_tablas_de_referencia from pg

BEGIN;

DROP TABLE IF EXISTS vlci2.t_m_combinados_temporales;
DROP TABLE IF EXISTS vlci2.t_m_combinados_criterios;
DROP TABLE IF EXISTS vlci2.t_m_combinados_ratios;
DROP TABLE IF EXISTS vlci2.t_x_organizacion_municipal_color;
DROP TABLE IF EXISTS vlci2.t_x_week_day;
DROP TABLE IF EXISTS vlci2.t_x_tipo_indicador;

COMMIT;
