-- Revert postgis-vlci-vlci2:v_migrations/1402-pmDiaria-gestion-fechas from pg

BEGIN;

DELETE FROM vlci2.t_d_fecha_negocio_etls WHERE etl_nombre = 'etl_cdmciutat_trafico_pm_diaria';
DELETE FROM vlci2.t_d_fecha_negocio_etls WHERE etl_nombre = 'etl_cdmciutat_bicicletas_pm_diaria';

COMMIT;
