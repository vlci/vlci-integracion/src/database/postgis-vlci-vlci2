-- Revert postgis-vlci-vlci2:v_migrations/376.eliminacion-columnas-metadatos from pg

BEGIN;

    alter table vlci2.t_f_text_kpi_precipitaciones_diarias
        add column calculationperiod_md text,
        add column kpivalue_md text;
    
    alter table vlci2.t_f_text_kpi_precipitaciones_mensuales
        add column calculationperiod_md text,
        add column kpivalue_md text;
    
    alter table vlci2.t_f_text_kpi_precipitaciones_anuales
        add column calculationperiod_md text,
        add column kpivalue_md text;
    
    alter table vlci2.t_f_text_kpi_temperatura_media_diaria
        add column calculationperiod_md text,
        add column kpivalue_md text;
    
    alter table vlci2.t_f_text_kpi_temperatura_minima_diaria
        add column calculationperiod_md text,
        add column kpivalue_md text;
    
    alter table vlci2.t_f_text_kpi_temperatura_maxima_diaria
        add column calculationperiod_md text,
        add column kpivalue_md text;
    
    alter table vlci2.t_f_text_cb_weatherobserved 
        add column windspeedvalueflag_md text,
        add column windspeedvalue_md text,
        add column winddirectionvalueflag_md text,
        add column winddirectionvalue_md text,
        add column temperaturevalueflag_md text,
        add column temperaturevalue_md text,
        add column relativehumidityvalueflag_md text,
        add column relativehumidityvalue_md text,
        add column precipitationvalueflag_md text,
        add column precipitationvalue_md text,
        add column gis_id_md text,
        add column dateobservedgmt0_md text,
        add column dateobserved_md text,
        add column atmosphericpressurevalueflag_md text,
        add column atmosphericpressurevalue_md text;

    alter table vlci2.t_datos_cb_medioambiente_poi
        add column bind_no_md text,
        add column bind_no2_md text,
        add column bind_nox_md text,
        add column bind_o3_md text,
        add column bind_pm1_md text,
        add column bind_pm10_md text,
        add column bind_pm25_md text,
        add column bind_so2_md text,
        add column bind_atmosphericpressure_md text,
        add column bind_precipitation_md text,
        add column bind_relativehumidity_md text,
        add column bind_temperature_md text,
        add column bind_winddirection_md text,
        add column bind_windspeed_md text,
        add column address_md text,
        add column category_md text,
        add column dataprovider_md text,
        add column description_md text,
        add column location_md text,
        add column name_md text,
        add column owner_md text;
    
    alter table vlci2.t_datos_cb_medioambiente_airqualityobserved 
        add column no2value_md text,
        add column no2valueflag_md text,
        add column pm10value_md text,
        add column pm10valueflag_md text,
        add column pm25value_md text,
        add column pm25valueflag_md text,
        add column dateobserved_md text,
        add column refpointofinterest_md text;

    alter table vlci2.t_f_text_cb_kpi_consumo_agua 
        add column calculationperiod_md text,
        add column kpivalue_md text,
        add column name_md text,
        add column neighborhood_md text,
        add column consumptiontype_md text;
    
    alter table vlci2.t_datos_cb_emt_kpi 
        add column calculationperiod_md text,
        add column kpivalue_md text,
        add column sliceanddice1_md text,
        add column sliceanddicevalue1_md text,
        add column diasemana_md text;

    alter table vlci2.t_f_text_cb_kpi_residuos 
        add column calculationperiod_md text,
        add column kpivalue_md text,
        add column updatedat_md text,
        add column sliceanddicevalue1_md text,
        add column sliceanddicevalue2_md text,
        add column numeratorvalue_md text,
        add column denominatorvalue_md text;

    alter table vlci2.t_f_text_cb_sonometros_ruzafa_daily 
        add column dateobserved_md text,
        add column name_md text,
        add column laeq_md text,
        add column laeq_d_md text,
        add column laeq_e_md text,
        add column laeq_n_md text,
        add column laeq_den_md text,
        add column error_status_md text,
        add column source_md text;

    alter table vlci2.t_f_text_kpi_trafico_y_bicis 
        add column calculationperiod_md text,
        add column diasemana_md text,
        add column kpivalue_md text,
        add column tramo_md text,
        add column name_md text;

    alter table vlci2.t_f_text_cb_wifi_kpi_conexiones 
        add column name_md text,
        add column description_md text,
        add column measureunit_md text,
        add column calculationfrequency_md text,
        add column kpivalue_md text,
        add column calculationperiod_md text,
        add column updatedat_md text,
        add column internationalstandard_md text,
        add column sliceanddicevalue1_md text,
        add column sliceanddicevalue2_md text,
        add column sliceanddicevalue3_md text,
        add column sliceanddicevalue4_md text,
        add column sliceanddice1_md text,
        add column sliceanddice2_md text,
        add column sliceanddice3_md text,
        add column sliceanddice4_md text,
        add column desiredtrend_md text,
        add column kpitype_md text,
        add column numerator_md text,
        add column servicename_md text;
    
    alter table vlci2.t_f_text_cb_wifi_kpi_urbo 
        add column calculationfrequency_md text,
        add column category_md text,
        add column dataprovider_md text,
        add column dlnumberofnewcitizens_md text,
        add column dlnumberofnewcitizenscalculationperiodfrom_md text,
        add column dlnumberofnewcitizenssource_md text,
        add column dlnumberofnewcitizensupdatedat_md text,
        add column dlnumberofnewuser_md text,
        add column dlnumberofnewuserscalculationperiodfrom_md text,
        add column dlnumberofnewuserssource_md text,
        add column dlnumberofnewusersupdatedat_md text,
        add column dlnumberofnewworkers_md text,
        add column dlnumberofnewworkerscalculationperiodfrom_md text,
        add column dlnumberofnewworkerssource_md text, 
        add column dlnumberofnewworkersupdatedat_md text,
        add column dlnumberofneww4eu_md text,
        add column dlnumberofneww4eucalculationperiodfrom_md text,
        add column dlnumberofneww4eusource_md text,
        add column dlnumberofneww4euupdatedat_md text,
        add column name_md text,
        add column organization_md text,
        add column process_md text, 
        add column source_md text;

    alter table vlci2.t_datos_cb_parkingspot 
        add category_md text,
        add datemodified_md text,
        add description_md text,
        add name_md text,
        add operationstatus_md text,
        add status_md text;

COMMIT;
