-- Revert postgis-vlci-vlci2:v_migrations/1983.contratos.menores.creacion-tabla from pg

BEGIN;

DROP INDEX fecha_carga_area_idx;
DROP INDEX fecha_carga_tipo_idx;
DROP INDEX fecha_carga_duracion_idx;

DROP TABLE vlci2.t_datos_etl_contratos_menores;

COMMIT;
