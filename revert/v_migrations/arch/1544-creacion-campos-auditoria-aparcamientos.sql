-- Revert postgis-vlci-vlci2:v_migrations/1544-creacion-campos-auditoria-aparcamientos from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos
DROP COLUMN aud_user_ins,
DROP COLUMN aud_fec_ins,
DROP COLUMN aud_fec_upd,
DROP COLUMN aud_user_upd;

COMMIT;
