-- revert/v_migrations/2868-nuevas-columnas-agua-corregido.sql

BEGIN;

ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida 
DROP COLUMN IF EXISTS originalEntityId,
DROP COLUMN IF EXISTS originalEntityType,
DROP COLUMN IF EXISTS originalServicePath;

COMMIT;
