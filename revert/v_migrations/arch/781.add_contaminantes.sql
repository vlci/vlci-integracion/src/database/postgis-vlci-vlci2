-- Revert postgis-vlci-vlci2:v_migrations/781.add_contaminantes from pg

BEGIN;

DELETE FROM vlci2.t_d_particulas_calidad_aire
WHERE particula_nombre in ('PM1','NO','NOX');

COMMIT;
