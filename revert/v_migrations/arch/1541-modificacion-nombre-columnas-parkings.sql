-- Revert postgis-vlci-vlci2:v_migrations/1541-modificacion-nombre-columnas-parkings from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata RENAME COLUMN totalSpotNumber TO plazastotales;
ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata RENAME COLUMN availableSpotNumber TO plazaslibres;	

COMMIT;
