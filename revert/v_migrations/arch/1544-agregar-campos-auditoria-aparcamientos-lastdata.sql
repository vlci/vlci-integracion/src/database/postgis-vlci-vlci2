-- Revert postgis-vlci-vlci2:v_migrations/1544-agregar-campos-auditoria-aparcamientos-lastdata from pg

BEGIN;

-- ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata DROP CONSTRAINT t_datos_cb_trafico_aparcamientos_lastdata_pkey;

-- ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata
-- RENAME COLUMN aud_fec_ins TO recvtime;

-- ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata
-- ADD CONSTRAINT t_datos_cb_trafico_aparcamientos_lastdata_pkey PRIMARY KEY (entityid, recvtime);

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata
DROP COLUMN aud_user_ins,
DROP COLUMN aud_fec_ins,
DROP COLUMN aud_fec_upd,
DROP COLUMN aud_user_upd;

COMMIT;
