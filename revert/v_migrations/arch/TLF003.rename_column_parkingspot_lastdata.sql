-- Revert postgis-vlci-vlci2:v_migrations/TLF003.rename_column_parkingspot_lastdata from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_parkingspot_lastdata RENAME COLUMN operationalstatus TO operationstatus;


COMMIT;
