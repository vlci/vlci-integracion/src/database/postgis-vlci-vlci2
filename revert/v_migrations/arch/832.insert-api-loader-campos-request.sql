-- Revert postgis-vlci-vlci2:v_migrations/832.insert-api-loader-campos-request from pg
-- Tarea PROY2100017N3-832

BEGIN;

DELETE FROM vlci2.t_d_api_loader_campos_request WHERE nombre_api IN 
    ('Geoportal_suma_carril_bici', 'Geoportal_total_reguladores_led', 'Geoportal_total_reguladores', 'Geoportal_total_paneles', 'Geoportal_total_camaras_web',
    'Geoportal_total_aparcamotos', 'Geoportal_total_carga_descarga', 'Geoportal_total_paradas_taxi', 'Geoportal_suma_ejes_calle', 'Geoportal_total_carril_bici', 
    'Geoportal_suma_bici_aparcamiento', 'Geoportal_suma_minusvalidos', 'Geoportal_total_aparcamientos');

COMMIT;
