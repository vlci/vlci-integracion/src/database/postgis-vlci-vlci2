-- Revert postgis-vlci-vlci2:v_migrations/2450-gf-actualizar-patraix from pg

BEGIN;

UPDATE vlci2.t_d_fecha_negocio_etls
	SET aud_user_upd='p_gf_registrarEstadoETL',aud_fec_upd='2024-01-31 06:02:34.356',estado='ERROR',fen='2023-08-05 00:00:00.000'
	WHERE etl_id=223 and etl_nombre ='A11_PATRAIX_24h';

COMMIT;
