-- Revert postgis-vlci-vlci2:v_migrations/1203.Eliminar_datos_duplicados_cdmciutat_contaminacion from pg

BEGIN;

INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved
(recvtime, fiwareservicepath, entityid, entitytype, no2value, no2valueflag, pm10value, pm10valueflag, pm25value, pm25valueflag, dateobserved, refpointofinterest, no2type, o3type, pm10type, pm25type, so2type, novalue, novalueflag, notype, noxvalue, noxvalueflag, noxtype, o3value, o3valueflag, pm1value, pm1valueflag, pm1type, so2value, so2valueflag)
VALUES('2020-05-12 09:21:11.000', '/MedioAmbiente', 'A01_AVFRANCIA_24h', 'AirQualityObserved', 4, 'V', 6, 'V', NULL, NULL, '2020-05-11 00:00:00.000', 'A01_AVFRANCIA', 'µg/m3', NULL, 'µg/m3', 'µg/m3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved
(recvtime, fiwareservicepath, entityid, entitytype, no2value, no2valueflag, pm10value, pm10valueflag, pm25value, pm25valueflag, dateobserved, refpointofinterest, no2type, o3type, pm10type, pm25type, so2type, novalue, novalueflag, notype, noxvalue, noxvalueflag, noxtype, o3value, o3valueflag, pm1value, pm1valueflag, pm1type, so2value, so2valueflag)
VALUES('2020-05-12 09:21:11.000', '/MedioAmbiente', 'A05_POLITECNIC_24h', 'AirQualityObserved', 4, 'V', 7, 'V', NULL, NULL, '2020-05-11 00:00:00.000', 'A05_POLITECNIC', 'µg/m3', NULL, 'µg/m3', 'µg/m3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved
(recvtime, fiwareservicepath, entityid, entitytype, no2value, no2valueflag, pm10value, pm10valueflag, pm25value, pm25valueflag, dateobserved, refpointofinterest, no2type, o3type, pm10type, pm25type, so2type, novalue, novalueflag, notype, noxvalue, noxvalueflag, noxtype, o3value, o3valueflag, pm1value, pm1valueflag, pm1type, so2value, so2valueflag)
VALUES('2020-05-12 09:21:11.000', '/MedioAmbiente', 'A03_MOLISOL_24h', 'AirQualityObserved', 1, 'V', 5, 'V', NULL, NULL, '2020-05-11 00:00:00.000', 'A03_MOLISOL', 'µg/m3', NULL, 'µg/m3', 'µg/m3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved
(recvtime, fiwareservicepath, entityid, entitytype, no2value, no2valueflag, pm10value, pm10valueflag, pm25value, pm25valueflag, dateobserved, refpointofinterest, no2type, o3type, pm10type, pm25type, so2type, novalue, novalueflag, notype, noxvalue, noxvalueflag, noxtype, o3value, o3valueflag, pm1value, pm1valueflag, pm1type, so2value, so2valueflag)
VALUES('2020-03-24 11:13:27.000', '/MedioAmbiente', 'A04_PISTASILLA_24h', 'AirQualityObserved', 14, 'V', 14, 'V', NULL, NULL, '2020-03-05 00:00:00.000', 'A04_PISTASILLA', 'µg/m3', NULL, 'µg/m3', 'µg/m3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved
(recvtime, fiwareservicepath, entityid, entitytype, no2value, no2valueflag, pm10value, pm10valueflag, pm25value, pm25valueflag, dateobserved, refpointofinterest, no2type, o3type, pm10type, pm25type, so2type, novalue, novalueflag, notype, noxvalue, noxvalueflag, noxtype, o3value, o3valueflag, pm1value, pm1valueflag, pm1type, so2value, so2valueflag)
VALUES('2020-03-24 11:13:27.000', '/MedioAmbiente', 'A05_POLITECNIC_24h', 'AirQualityObserved', 8, 'V', 9, 'V', NULL, NULL, '2020-03-05 00:00:00.000', 'A05_POLITECNIC', 'µg/m3', NULL, 'µg/m3', 'µg/m3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved
(recvtime, fiwareservicepath, entityid, entitytype, no2value, no2valueflag, pm10value, pm10valueflag, pm25value, pm25valueflag, dateobserved, refpointofinterest, no2type, o3type, pm10type, pm25type, so2type, novalue, novalueflag, notype, noxvalue, noxvalueflag, noxtype, o3value, o3valueflag, pm1value, pm1valueflag, pm1type, so2value, so2valueflag)
VALUES('2020-03-24 11:13:27.000', '/MedioAmbiente', 'A03_MOLISOL_24h', 'AirQualityObserved', 5, 'V', 14, 'V', NULL, NULL, '2020-03-05 00:00:00.000', 'A03_MOLISOL', 'µg/m3', NULL, 'µg/m3', 'µg/m3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved
(recvtime, fiwareservicepath, entityid, entitytype, no2value, no2valueflag, pm10value, pm10valueflag, pm25value, pm25valueflag, dateobserved, refpointofinterest, no2type, o3type, pm10type, pm25type, so2type, novalue, novalueflag, notype, noxvalue, noxvalueflag, noxtype, o3value, o3valueflag, pm1value, pm1valueflag, pm1type, so2value, so2valueflag)
VALUES('2020-05-12 09:21:11.000', '/MedioAmbiente', 'A04_PISTASILLA_24h', 'AirQualityObserved', 6, 'V', 2, 'V', NULL, NULL, '2020-05-11 00:00:00.000', 'A04_PISTASILLA', 'µg/m3', NULL, 'µg/m3', 'µg/m3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


COMMIT;
