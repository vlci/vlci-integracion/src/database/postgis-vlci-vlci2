-- Revert postgis-vlci-vlci2:v_migrations/1437.insertar-datos-t_datos_cb_emt_kpi from pg

BEGIN;

delete from vlci2.t_datos_cb_emt_kpi where calculationperiod >= '2020-03-02' and calculationperiod <= '2021-08-29';

COMMIT;
