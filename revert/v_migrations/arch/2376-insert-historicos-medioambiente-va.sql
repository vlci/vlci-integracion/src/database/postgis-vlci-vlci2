-- Revert postgis-vlci-vlci2:v_migrations/2376-insert-historicos-medioambiente-va from pg

BEGIN;

delete from vlci2.t_datos_cb_medioambiente_airqualityobserved_va where dateobserved < '2020-03-02 00:00:00';

COMMIT;
