-- Revert postgis-vlci-vlci2:v_migrations/647.añadir_columnas_contaminantes from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN novalue;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN novalueflag;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN notype;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN noxvalue;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN noxvalueflag;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN noxtype;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN o3value;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN o3valueflag;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN pm1value;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN pm1valueflag;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN pm1type;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN so2value;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN so2valueflag;

COMMIT;
