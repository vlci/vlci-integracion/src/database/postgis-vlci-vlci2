-- Revert postgis-vlci-vlci2:v_migrations/275.alta-medidas-teleasistencia from pg

BEGIN;

DELETE FROM vlci2.t_m_unidad_medida
WHERE dc_unidad_medida_cas IN ('Usuarios/año', 'Índice satisfación anual', 'Nº de reclamaciones/quejas', 'Nº llamadas seguimiento/Mes');

COMMIT;
