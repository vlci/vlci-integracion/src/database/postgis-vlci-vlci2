-- Revert postgis-vlci-vlci2:v_migrations/1581.recuperar-datos-faltantes-bicis from pg

BEGIN;

INSERT INTO t_f_text_kpi_trafico_y_bicis (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,diasemana,kpivalue,tramo,"name",measureunitcas,measureunitval) VALUES
	 ('2021-02-02 01:00:13.685+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2021-02-01','L','2181','Navarro Reverter','Navarro Reverter','Bicicletas','Bicicletes'),
	 ('2021-02-02 01:00:14.822+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2021-02-01','L','1881','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
	 ('2021-02-02 01:00:15.517+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2021-02-01','L','2345','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
	 ('2021-02-02 01:00:15.993+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2021-02-01','L','1211','Pont del real','Pont del real','Bicicletas','Bicicletes'),
	 ('2021-02-02 01:00:17.429+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2021-02-01','L','2219','Carrer Alacant','Carrer Alacant','Bicicletas','Bicicletes'),
	 ('2021-02-02 01:00:17.759+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2021-02-01','L','2240','Carrer Russafa','Carrer Russafa','Bicicletas','Bicicletes'),
	 ('2021-07-06 00:00:16.329+02','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2021-07-05','L','3053','Navarro Reverter','Navarro Reverter','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:06.236+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-03-09','X','3712','Navarro Reverter','Navarro Reverter','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:06.279+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-03-09','X','3418','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:06.342+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-03-09','X','5306','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:06.382+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-03-09','X','1971','Pont del real','Pont del real','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:06.435+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-03-09','X','3934','Carrer Alacant','Carrer Alacant','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:06.463+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-03-09','X','3625','Carrer Russafa','Carrer Russafa','Bicicletas','Bicicletes');


   INSERT INTO t_f_text_kpi_trafico_y_bicis (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,diasemana,kpivalue,tramo,"name",measureunitcas,measureunitval) VALUES
	 ('2021-02-02 01:00:07.246+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-02-01','L','3281','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
	 ('2021-02-02 01:00:11.542+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-02-01','L','1604','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
	 ('2021-02-02 01:00:12.466+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-02-01','L','3393','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
	 ('2021-02-02 01:00:12.764+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-02-01','L','4354','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
	 ('2021-02-02 01:00:13.413+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-02-01','L','1354','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
	 ('2021-07-06 00:00:12.972+02','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-07-05','L','4574','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
	 ('2021-07-06 00:00:15.225+02','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-07-05','L','4245','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
	 ('2021-07-06 00:00:16.99+02','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-07-05','L','6009','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:05.432+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2022-03-09','X','5762','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:06.5+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2022-03-09','X','3669','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:06.99+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2022-03-09','X','5613','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:06.151+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2022-03-09','X','7471','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
	 ('2022-03-10 05:16:06.189+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2022-03-09','X','2127','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes');


delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-19') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-21') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-21') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-22') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-23') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-24') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-24') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-25') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-27') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-30') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-01') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-03') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-10') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-11') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-14') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-19') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-20') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-21') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-22') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-23') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-30') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-06-22') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-06-23') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-06-24') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2021-09-03') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-04-15') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-04-15') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-04-16') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-04-16') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-04-17') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-04-17') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-05-01') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-05-01') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-05-01') and k.tramo = 'Pont del real'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-05-04') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-05-05') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-05-06') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-05-07') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-05-08') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-05-09') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-11-03') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2022-11-04') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-03-23', '2021-08-24', '2021-08-25', '2021-08-26') and k.tramo = 'Carrer Colon'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;
delete  from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2020-04-01') and k.tramo = 'Xàtiva'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

COMMIT;
