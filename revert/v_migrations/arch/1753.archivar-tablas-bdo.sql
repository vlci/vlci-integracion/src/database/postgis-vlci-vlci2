-- Revert postgis-vlci-vlci2:v_migrations/1753.archivar-tablas-bdo from pg

BEGIN;

ALTER TABLE vlci2.t_d_servicio_arch
  RENAME TO t_d_servicio;

ALTER TABLE vlci2.t_d_delegacion_arch
  RENAME TO t_d_delegacion;

ALTER TABLE vlci2.t_d_area_arch
  RENAME TO t_d_area;

COMMIT;
