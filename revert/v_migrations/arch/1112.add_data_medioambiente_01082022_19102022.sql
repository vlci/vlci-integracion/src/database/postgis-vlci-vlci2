-- Revert postgis-vlci-vlci2:v_migrations/1112.add_data_medioambiente_01082022_19102022 from pg

BEGIN;

DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A08_DR_LLUCH_24h' AND recvtime::date='2022-08-02';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-02';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-03';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-04';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-05';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-06';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-07';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-08';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-09';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-08-09';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-08-09';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-08-09';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-08-10';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-08-10';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-10';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-11';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-12';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-08-13';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-13';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-08-13';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-08-14';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-14';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-15';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-08-15';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-16';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-17';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-18';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-19';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-20';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-21';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A08_DR_LLUCH_24h' AND recvtime::date='2022-08-21';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-08-22';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-22';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-23';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-24';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-08-24';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-25';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-26';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-08-27';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-27';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-08-28';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-28';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-29';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A08_DR_LLUCH_24h' AND recvtime::date='2022-08-29';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-30';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-08-31';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-01';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-09-02';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-02';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-02';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-03';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-09-03';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-04';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A08_DR_LLUCH_24h' AND recvtime::date='2022-09-04';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-09-05';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-05';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-06';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-09-06';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-07';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-07';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-09-07';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-08';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-09';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-10';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-11';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-09-12';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-12';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A01_AVFRANCIA_24h' AND recvtime::date='2022-09-12';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-12';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A01_AVFRANCIA_24h' AND recvtime::date='2022-09-13';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-13';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-13';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-09-13';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-14';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-14';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A08_DR_LLUCH_24h' AND recvtime::date='2022-09-15';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-15';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-16';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-16';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-17';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-17';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-18';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-18';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A01_AVFRANCIA_24h' AND recvtime::date='2022-09-18';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-09-18';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-19';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-19';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A01_AVFRANCIA_24h' AND recvtime::date='2022-09-19';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-09-19';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-09-20';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-20';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-20';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A08_DR_LLUCH_24h' AND recvtime::date='2022-09-21';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-21';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-22';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-09-22';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A01_AVFRANCIA_24h' AND recvtime::date='2022-09-22';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-22';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-23';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-09-23';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-23';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-24';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-09-25';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-25';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-25';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A01_AVFRANCIA_24h' AND recvtime::date='2022-09-25';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-26';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-26';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A01_AVFRANCIA_24h' AND recvtime::date='2022-09-26';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-09-26';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-27';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-28';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-29';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-09-30';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-09-30';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-09-30';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-01';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-10-01';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-02';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-03';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-10-03';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-10-04';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A01_AVFRANCIA_24h' AND recvtime::date='2022-10-04';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-04';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-10-04';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-10-05';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-05';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A08_DR_LLUCH_24h' AND recvtime::date='2022-10-06';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-06';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A01_AVFRANCIA_24h' AND recvtime::date='2022-10-07';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A03_MOLISOL_24h' AND recvtime::date='2022-10-07';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-07';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-10-07';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-08';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A08_DR_LLUCH_24h' AND recvtime::date='2022-10-08';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-09';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-10-09';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-10';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A04_PISTASILLA_24h' AND recvtime::date='2022-10-10';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A08_DR_LLUCH_24h' AND recvtime::date='2022-10-11';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-11';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-12';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-10-13';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-13';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-14';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-10-14';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A05_POLITECNIC_24h' AND recvtime::date='2022-10-15';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-15';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-16';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-17';
DELETE FROM vlci2.t_datos_cb_medioambiente_airqualityobserved 
 WHERE  entityid='A07_VALENCIACENTRE_24h' AND recvtime::date='2022-10-18';
COMMIT;
