-- Revert postgis-vlci-vlci2:v_migrations/1777-insertar-datos-historicos-estadistica-paro from pg

BEGIN;

DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-06' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-06' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-07' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-07' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-08' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-08' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-09' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-09' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-10' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-10' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-11' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-11' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-12' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-12' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-01' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-01' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-02' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-02' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-03' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-03' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-04' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-04' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-05' AND sliceanddicevalue1='Femení';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-05' AND sliceanddicevalue1='Masculí';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-06' AND sliceanddicevalue1='N/A';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-09' AND sliceanddicevalue1='N/A';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2022-12' AND sliceanddicevalue1='N/A';
DELETE FROM vlci2.t_datos_cb_estadistica_paro
WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-03' AND sliceanddicevalue1='N/A';


COMMIT;
