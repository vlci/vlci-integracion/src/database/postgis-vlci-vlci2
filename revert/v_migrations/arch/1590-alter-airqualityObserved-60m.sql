-- Revert postgis-vlci-vlci2:v_migrations/1590-alter-airqualityObserved-60m from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN calidad_ambiental;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN gis_id;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN address;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN operationalstatus;

COMMIT;
