-- Revert postgis-vlci-vlci2:v_migrations/2673-add-originalservicepath-to-estacionamiento-inteligente from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_kpi_estacionamiento_inteligente
DROP COLUMN originalservicepath;

COMMIT;
