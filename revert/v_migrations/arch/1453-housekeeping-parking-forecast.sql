-- Revert postgis-vlci-vlci2:v_migrations/1453-housekeeping-parking-forecast from pg

BEGIN;

DELETE FROM vlci2.housekeeping_config
	WHERE table_nam='t_datos_cb_trafico_offstreetparking_forecast';

COMMIT;
