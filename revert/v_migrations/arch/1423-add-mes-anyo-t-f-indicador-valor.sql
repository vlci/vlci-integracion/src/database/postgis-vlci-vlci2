-- Revert postgis-vlci-vlci2:v_migrations/1423-add-mes-anyo-t-f-indicador-valor from pg

BEGIN;

ALTER TABLE vlci2.t_f_indicador_valor DROP COLUMN mes;
ALTER TABLE vlci2.t_f_indicador_valor DROP COLUMN anyo;

COMMIT;
