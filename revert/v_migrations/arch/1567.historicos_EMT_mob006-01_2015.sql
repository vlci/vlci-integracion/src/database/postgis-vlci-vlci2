-- Revert postgis-vlci-vlci2:v_migrations/1567.historicos_EMT_mob006-01_2015 from pg

BEGIN;

DELETE FROM vlci2.t_datos_cb_emt_kpi WHERE entityid = 'mob006-01' 
and calculationperiod < '2016-01-01'
and calculationperiod >= '2015-01-01';

COMMIT;
