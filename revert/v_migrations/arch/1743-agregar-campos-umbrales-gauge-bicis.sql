-- Revert postgis-vlci-vlci2:v_migrations/1743-agregar-campos-umbrales-gauge-bicis from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata
DROP COLUMN lowerthresholdrt,
DROP COLUMN middlethresholdrt,
DROP COLUMN upperthresholdrt;

COMMIT;
