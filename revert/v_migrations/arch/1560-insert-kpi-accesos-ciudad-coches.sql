-- Revert postgis-vlci-vlci2:v_migrations/1560-insert-kpi-accesos-ciudad-coches from pg

BEGIN;

delete from vlci2.t_f_text_kpi_trafico_y_bicis where to_date(calculationperiod, 'YYYY-MM-DD') < to_date('2020-02-03', 'YYYY-MM-DD') and entityid = 'Kpi-Accesos-Ciudad-Coches';

COMMIT;
