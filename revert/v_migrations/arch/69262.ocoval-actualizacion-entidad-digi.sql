-- Revert postgis-vlci-vlci2:v_migrations/69262.ocoval-actualizacion-entidad-digi from pg

BEGIN;

UPDATE vlci2.vlci_ocoval_entidades
	SET compania='Otros'
	WHERE entidad='Digi' AND compania='Compañías de Servicios';

COMMIT;
