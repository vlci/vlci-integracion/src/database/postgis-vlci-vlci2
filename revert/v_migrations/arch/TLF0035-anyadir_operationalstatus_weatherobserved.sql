-- Revert postgis-vlci-vlci2:v_migrations/TLF0035-anyadir_operationalstatus_weatherobserved from pg
BEGIN;

ALTER TABLE vlci2.t_datos_cb_medioambiente_weatherobserved_10m_lastdata
DROP COLUMN operationalstatus;

COMMIT;