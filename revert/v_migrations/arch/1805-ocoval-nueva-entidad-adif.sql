-- Revert postgis-vlci-vlci2:v_migrations/1805-ocoval-nueva-entidad-adif from pg

BEGIN;

DELETE FROM vlci2.vlci_ocoval_entidades WHERE compania = 'Otros' and entidad = 'Adif';

COMMIT;
