-- Revert postgis-vlci-vlci2:v_migrations/1525.eliminar-tablas-housekeeping from pg

BEGIN;

INSERT INTO vlci2.housekeeping_config (table_nam,tiempo,colfecha) VALUES
	 ('t_d_area_arch',6,'fe_fecha_carga'),
	 ('t_d_delegacion_arch',6,'fe_fecha_carga'),
	 ('t_d_servicio_arch',6,'fe_fecha_carga'),
	 ('t_d_usuario_servicio_arch',6,'fe_fecha_carga');


COMMIT;
