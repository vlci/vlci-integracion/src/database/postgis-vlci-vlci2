-- Revert postgis-vlci-vlci2:v_migrations/1759-agregar-datos-junio-emt from pg

BEGIN;

delete from vlci2.t_datos_cb_emt_kpi
    where  sliceanddice1 in('Titulo', 'Ruta') and 
        calculationperiod in ('2023-06-09', '2023-06-10', '2023-06-11');


COMMIT;
