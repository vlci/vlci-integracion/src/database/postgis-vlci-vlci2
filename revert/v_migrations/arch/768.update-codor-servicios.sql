-- Revert postgis-vlci-vlci2:v_migrations/768.update-codor-servicios from pg

BEGIN;

update vlci2.t_d_servicio set codigo_organico = 'AC020',aud_user_upd='ETL BDO',aud_fec_upd='2022-06-08 20:34:59.953' where codigo_organico ='A0020' and desc_corta_cas ='SERV. DE PROTOCOLO';
update vlci2.t_d_servicio set codigo_organico = 'AC180',aud_user_upd='ETL BDO',aud_fec_upd='2022-06-08 20:34:59.953' where codigo_organico ='A0180' and desc_corta_cas ='SR.GABINETE DE NORMALIZACION LINGUISTICA';
update vlci2.t_d_servicio set codigo_organico = 'AC1G0',aud_user_upd='ETL BDO',aud_fec_upd='2022-06-08 20:34:59.953' where codigo_organico ='A01G0' and desc_corta_cas ='OF.DE DEL.DE PROTECCION DATOS PERSONALES';
update vlci2.t_d_servicio set codigo_organico = 'AE1F0',aud_user_upd='ETL BDO',aud_fec_upd='2022-06-08 20:34:59.953' where codigo_organico ='P61F0' and desc_corta_cas ='SERV. SECRETARIA DEL JURADO TRIBUTARIO';
update vlci2.t_d_servicio set codigo_organico = 'AL090',aud_user_upd='ETL BDO',aud_fec_upd='2022-06-08 20:34:59.953' where codigo_organico ='CC090' and desc_corta_cas ='SR.EVALUAC.SR.Y PERSONAS Y GEST.CALIDAD';
update vlci2.t_d_servicio set codigo_organico = 'AL400',aud_user_upd='ETL BDO',aud_fec_upd='2022-06-08 20:34:59.953' where codigo_organico ='CC400' and desc_corta_cas ='SR. FORMACION Y GESTION DEL CONOCIMIENTO';

delete from vlci2.t_d_delegacion where pk_delegacion_id in ('202','266','001');
delete from vlci2.t_d_servicio where pk_servicio_id in ('61J','656','677','653','663','266');

COMMIT;
