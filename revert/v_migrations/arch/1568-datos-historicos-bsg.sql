-- Revert postgis-vlci-vlci2:v_migrations/1568-datos-historicos-bsg from pg

BEGIN;

delete from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved < '2020-03-02 00:00:00.000';

COMMIT;
