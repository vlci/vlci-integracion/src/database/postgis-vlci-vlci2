-- Revert postgis-vlci-vlci2:v_migrations/2374-correccion-gf-datos-validados-24h from pg

BEGIN;

UPDATE vlci2.t_d_fecha_negocio_etls
	SET periodicidad='DIEZMINUTAL', "offset"=4320, aud_fec_upd ='2024-02-22 12:05:57.138', aud_user_upd ='Iván 2302'
	WHERE etl_nombre like ('%24h_vm%');
	
UPDATE vlci2.t_d_fecha_negocio_etls
	SET periodicidad='DIEZMINUTAL', "offset"=12960, aud_fec_upd ='2024-02-22 12:05:57.138', aud_user_upd ='Iván 2302'
	WHERE etl_nombre like ('%24h_va%');

COMMIT;
