-- Revert postgis-vlci-vlci2:v_migrations/466.borrar_duplicados_agua from pg

BEGIN;

INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-18 12:43:02.685','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-09','261.1931','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-18 12:43:02.718','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-09','120.3209','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-18 12:43:02.754','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-09','0.5137','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-18 12:43:02.787','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-09','3831.0672','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-18 12:43:02.819','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2022-05-09','2022.4668','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2022-05-18 12:43:02.858','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2022-05-09','174.0442','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2022-05-18 12:43:03.103','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2022-05-09','94.4104','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2022-05-18 12:43:03.588','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-09','2808.1000','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-18 12:43:03.638','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2022-05-09','34.7503','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2022-05-18 12:43:03.762','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2022-05-09','2.4197','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-18 12:43:03.804','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2022-05-09','0.3055','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2022-05-18 12:43:03.842','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2022-05-09','370.0651','Consumo_agua_total_mestalla','Mestalla','N/A','m3'),
	 ('2022-05-18 12:43:03.885','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2022-05-09','460.2932','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2022-05-18 12:43:03.922','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2022-05-09','97.5367','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2022-05-18 12:43:04.348','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2022-05-09','12.3975','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2022-05-18 12:43:04.381','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-10','2664.4150','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-18 12:43:04.416','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-11','2813.8400','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-18 12:43:04.451','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-12','2716.1150','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-18 12:43:04.689','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-13','2778.8875','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-18 12:43:04.986','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-14','2558.8850','Consumo_agua_total_malilla','Malilla','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-18 12:43:05.516','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-15','2413.6500','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-18 12:43:05.549','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-16','2825.1924','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-18 12:43:05.784','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-17','124664.9150','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-18 12:43:05.819','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-17','2193.7424','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-18 12:43:05.851','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-17','2632.3674','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-18 12:43:05.885','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-17','834.8000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-18 12:43:05.916','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-17','1004.9200','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-18 12:47:44.826','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2022-05-10','34.0898','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2022-05-18 12:47:44.866','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2022-05-10','102.1209','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2022-05-18 12:47:45.107','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-10','3951.9313','Consumo_agua_total_exposicio','Exposicio','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-18 12:47:45.149','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2022-05-10','9.6847','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2022-05-18 12:47:45.188','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2022-05-10','338.9437','Consumo_agua_total_mestalla','Mestalla','N/A','m3'),
	 ('2022-05-18 12:47:45.224','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2022-05-10','95.0200','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2022-05-18 12:47:45.269','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2022-05-10','3.6178','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2022-05-18 12:47:45.313','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-10','0.9993','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-18 12:47:45.553','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2022-05-10','461.5493','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2022-05-18 12:47:45.587','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-10','276.4017','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-18 12:47:45.623','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-10','158.3509','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-18 12:47:45.654','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2022-05-10','0.3005','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2022-05-18 12:47:45.691','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2022-05-10','2016.7129','Consumo_agua_domestico_malilla','Malilla','domestico','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-18 12:47:45.740','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2022-05-10','183.8012','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2022-05-18 12:52:15.915','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-11','276.9702','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-18 12:52:15.951','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2022-05-11','3.7189','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2022-05-18 12:52:15.986','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2022-05-11','182.7282','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2022-05-18 12:52:16.360','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2022-05-11','2007.5812','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2022-05-18 12:52:16.910','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2022-05-11','92.6254','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2022-05-18 12:52:16.125','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-11','3894.6482','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-18 12:52:16.157','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2022-05-11','456.3490','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2022-05-18 12:52:16.198','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-11','175.5014','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-18 12:52:16.231','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2022-05-11','12.8750','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-18 12:52:16.269','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2022-05-11','35.9373','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2022-05-18 12:52:16.303','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-11','1.2332','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-18 12:52:16.340','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2022-05-11','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2022-05-18 12:52:16.388','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2022-05-11','333.8768','Consumo_agua_total_mestalla','Mestalla','N/A','m3'),
	 ('2022-05-18 12:52:16.435','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2022-05-11','96.0740','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:31.295','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-09','1957.1008','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-19 14:10:31.323','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-09','4.6349','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-19 14:10:31.359','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2022-05-09','1697.5529','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2022-05-19 14:10:31.383','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2022-05-09','30.8755','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:31.408','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2022-05-09','110.5441','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:31.436','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-09','1482.9416','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-19 14:10:31.459','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2022-05-09','521.1860','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2022-05-19 14:10:31.481','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2022-05-09','222.4661','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:31.504','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2022-05-09','2221.5350','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2022-05-19 14:10:31.526','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2022-05-09','70.0193','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:31.555','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2022-05-10','310.1959','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2022-05-19 14:10:31.577','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2022-05-10','103.3405','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2022-05-19 14:10:31.600','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2022-05-11','223.1449','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:31.623','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2022-05-11','2209.8798','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2022-05-19 14:10:31.647','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2022-05-11','597.2273','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:31.671','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2022-05-10','111.2770','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2022-05-19 14:10:31.695','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-11','3894.6482','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-19 14:10:31.716','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2022-05-10','1342.3013','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2022-05-19 14:10:31.737','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2022-05-11','24.9484','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:31.762','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-12','130325.0125','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-19 14:10:31.792','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-10','1930.7463','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-19 14:10:31.815','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2022-05-10','2220.0169','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2022-05-19 14:10:31.839','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-12','1926.9917','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-19 14:10:31.863','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-12','451.1925','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-19 14:10:31.890','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-10','2664.4150','Consumo_agua_total_malilla','Malilla','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:31.912','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2022-05-12','314.4960','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2022-05-19 14:10:31.932','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2022-05-12','1716.2803','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2022-05-19 14:10:31.954','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-11','1969.7473','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-19 14:10:31.977','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2022-05-11','34.4105','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:31.997','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2022-05-11','12.8750','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2022-05-19 14:10:32.180','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-12','891.5013','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:32.380','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2022-05-12','95.1610','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:32.620','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2022-05-12','94.6286','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2022-05-19 14:10:32.860','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-13','401.3039','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-19 14:10:32.113','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2022-05-13','32.9587','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:32.140','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-13','951.5575','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-19 14:10:32.170','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-14','3270.5154','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-19 14:10:32.191','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2022-05-12','394.3536','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2022-05-19 14:10:32.213','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2022-05-14','35.4578','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:32.234','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-14','909.5050','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-19 14:10:32.256','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-15','1219.1521','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-19 14:10:32.291','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2022-05-15','578.0764','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2022-05-19 14:10:32.313','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-16','946.9075','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-19 14:10:32.342','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-18','981.1600','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-19 14:10:32.432','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-09','120397.1400','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:32.460','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2022-05-09','1378.2221','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2022-05-19 14:10:32.557','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-09','873.0433','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:32.584','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2022-05-09','490.2685','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2022-05-19 14:10:32.624','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2022-05-09','99.5573','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2022-05-19 14:10:32.663','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2022-05-09','12.3975','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2022-05-19 14:10:32.726','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-10','908.4708','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:32.797','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2022-05-13','338.0970','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2022-05-19 14:10:32.817','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2022-05-10','96.0956','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:32.820','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2022-05-13','23.8102','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:32.846','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2022-05-14','222.7273','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:32.854','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2022-05-10','352.6643','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2022-05-19 14:10:32.869','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-14','2558.8850','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-19 14:10:32.893','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2022-05-14','20.7716','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:32.919','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2022-05-15','222.0825','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:32.922','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-10','736.2000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-19 14:10:32.942','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-15','2413.6500','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-19 14:10:32.984','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-10','941.4250','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-19 14:10:33.290','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2022-05-11','1346.7955','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2022-05-19 14:10:33.560','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2022-05-11','126.2213','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2022-05-19 14:10:33.143','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-11','92.1198','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:33.167','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2022-05-15','518.8535','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2022-05-19 14:10:33.194','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-17','124664.9150','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-19 14:10:33.220','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2022-05-12','121.2604','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2022-05-19 14:10:33.245','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2022-05-13','137.0924','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2022-05-19 14:10:33.495','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2022-05-13','96.3120','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:33.520','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-13','932.8000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-19 14:10:33.540','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-14','832.1814','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:33.565','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2022-05-14','560.4640','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2022-05-19 14:10:33.587','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2022-05-15','1382.4115','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2022-05-19 14:10:33.599','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2022-05-11','148.3807','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:33.613','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2022-05-15','207.6386','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2022-05-19 14:10:33.641','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2022-05-15','22.7180','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:33.652','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2022-05-11','94.5620','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2022-05-19 14:10:33.663','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-16','853.2000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-19 14:10:33.691','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-18','1066.0000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-19 14:10:33.705','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-11','901.1921','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:33.729','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-11','448.7670','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-19 14:10:33.733','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2022-05-09','302.8701','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2022-05-19 14:10:33.757','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2022-05-09','2718.1907','Consumo_agua_total_mestalla','Mestalla','N/A','m3'),
	 ('2022-05-19 14:10:33.760','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2022-05-11','97.1386','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:33.776','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2022-05-09','95.4407','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:33.799','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2022-05-09','361.5648','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2022-05-19 14:10:33.811','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-11','2813.8400','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-19 14:10:33.848','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-09','2808.1000','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-19 14:10:33.870','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2022-05-09','91.2765','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2022-05-19 14:10:33.894','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-09','939.8300','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-19 14:10:33.912','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2022-05-11','333.8768','Consumo_agua_total_mestalla','Mestalla','N/A','m3'),
	 ('2022-05-19 14:10:33.917','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2022-05-10','27.5869','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:33.964','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2022-05-10','222.9889','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:33.992','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-10','3951.9313','Consumo_agua_total_exposicio','Exposicio','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:34.100','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2022-05-12','127.0124','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2022-05-19 14:10:34.150','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2022-05-10','69.4434','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:34.750','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2022-05-12','2200.5634','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2022-05-19 14:10:34.201','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2022-05-12','33.5010','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:34.282','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2022-05-11','1712.7490','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2022-05-19 14:10:34.348','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2022-05-11','484.9063','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2022-05-19 14:10:34.391','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-10','11.4942','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-19 14:10:34.411','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-12','3872.7905','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-19 14:10:34.462','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2022-05-11','542.3234','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2022-05-19 14:10:34.478','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2022-05-10','9.6847','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:34.547','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2022-05-12','1338.9822','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2022-05-19 14:10:34.569','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-12','89.9179','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-19 14:10:34.594','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2022-05-12','599.3127','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2022-05-19 14:10:34.617','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2022-05-12','331.5745','Consumo_agua_total_mestalla','Mestalla','N/A','m3'),
	 ('2022-05-19 14:10:34.643','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2022-05-13','222.9207','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:34.665','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2022-05-13','2156.8988','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2022-05-19 14:10:34.687','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2022-05-14','122.8219','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2022-05-19 14:10:34.693','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-11','998.5000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-19 14:10:34.709','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2022-05-14','81.0075','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:34.734','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2022-05-14','142.5810','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:34.759','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2022-05-15','105.8551','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2022-05-19 14:10:34.782','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2022-05-15','79.4913','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:34.804','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-15','1061.0000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-19 14:10:34.828','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2022-05-15','5.4720','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2022-05-19 14:10:34.853','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2022-05-12','6.6930','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2022-05-19 14:10:34.854','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2022-05-12','27.6954','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:34.872','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-17','2632.3674','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-19 14:10:34.892','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-17','1004.9200','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-19 14:10:34.915','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-13','1912.5616','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-19 14:10:34.939','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2022-05-13','1671.8989','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:34.960','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2022-05-13','105.0132','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2022-05-19 14:10:34.980','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-14','61.4094','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-19 14:10:35.200','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2022-05-14','610.7595','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2022-05-19 14:10:35.230','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2022-05-14','68.4185','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2022-05-19 14:10:35.460','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-15','280.6934','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-19 14:10:35.680','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2022-05-15','29.3542','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2022-05-19 14:10:35.860','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-15','918.4675','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-19 14:10:35.109','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-18','2855.7000','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-19 14:10:35.134','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2022-05-12','485.9881','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2022-05-19 14:10:35.153','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-13','2778.8875','Consumo_agua_total_malilla','Malilla','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:35.153','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-13','884.3823','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:35.258','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-12','786.1000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-19 14:10:35.288','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2022-05-13','487.1790','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2022-05-19 14:10:35.369','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-12','918.1025','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-19 14:10:35.376','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2022-05-13','607.9122','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2022-05-19 14:10:35.391','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2022-05-14','1329.5981','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2022-05-19 14:10:35.399','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2022-05-13','8.9200','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2022-05-19 14:10:35.423','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2022-05-14','233.8279','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2022-05-19 14:10:35.442','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-14','1031.8000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-19 14:10:35.466','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-15','1967.2467','Consumo_agua_total_benimamet','Benimamet','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:35.479','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-14','1955.8342','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-19 14:10:35.483','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2022-05-15','1611.3548','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2022-05-19 14:10:35.505','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2022-05-15','61.0593','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2022-05-19 14:10:35.531','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-17','2193.7424','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-19 14:10:35.542','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2022-05-14','333.8246','Consumo_agua_total_mestalla','Mestalla','N/A','m3'),
	 ('2022-05-19 14:10:35.589','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-13','88.9564','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-19 14:10:35.614','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-13','3790.0871','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-19 14:10:35.669','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-09','707.1000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-19 14:10:35.688','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2022-05-13','355.7855','Consumo_agua_total_mestalla','Mestalla','N/A','m3'),
	 ('2022-05-19 14:10:35.710','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-09','355.0509','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:35.713','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2022-05-13','379.5994','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2022-05-19 14:10:35.755','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-10','122073.3675','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-19 14:10:35.774','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2022-05-10','124.4132','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2022-05-19 14:10:35.791','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2022-05-14','2211.7003','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2022-05-19 14:10:35.795','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2022-05-10','1735.3346','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2022-05-19 14:10:35.814','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2022-05-10','338.9437','Consumo_agua_total_mestalla','Mestalla','N/A','m3'),
	 ('2022-05-19 14:10:35.816','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2022-05-14','1594.0666','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2022-05-19 14:10:35.839','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-11','122248.6790','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-19 14:10:35.867','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2022-05-14','494.6523','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2022-05-19 14:10:35.893','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-15','124364.6175','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:35.919','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2022-05-10','491.0506','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2022-05-19 14:10:36.300','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-11','899.9825','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-19 14:10:36.260','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-10','427.8308','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-19 14:10:36.460','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2022-05-10','600.1847','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2022-05-19 14:10:36.650','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-12','2716.1150','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-19 14:10:36.107','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2022-05-11','313.6516','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2022-05-19 14:10:36.148','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2022-05-12','222.9217','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2022-05-19 14:10:36.214','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-13','130041.2125','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-19 14:10:36.259','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2022-05-13','223.9789','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2022-05-19 14:10:36.321','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-14','332.2175','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:36.328','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-15','64.5894','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-19 14:10:36.393','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-15','802.1259','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:36.411','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2022-05-15','527.8281','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2022-05-19 14:10:36.436','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-16','1981.7208','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-19 14:10:36.457','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2022-05-13','1330.0958','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2022-05-19 14:10:36.520','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-14','123120.4300','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-19 14:10:36.607','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2022-05-15','2316.7588','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2022-05-19 14:10:36.626','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2022-05-15','186.9432','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2022-05-19 14:10:36.650','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2022-05-16','2825.1924','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2022-05-19 14:10:36.656','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2022-05-15','2367.0205','Consumo_agua_total_mestalla','Mestalla','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:36.766','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-17','834.8000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-19 14:10:36.794','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2022-05-14','6.4030','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2022-05-19 14:10:36.861','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-16','132171.6100','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-11 10:31:21.378','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-09','1957.1008','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-11 10:31:21.457','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-09','120397.1400','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-11 10:31:21.483','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-10','122073.3675','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-11 10:31:21.893','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-10','1930.7463','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2022-05-11 10:31:21.952','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-10','941.4250','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-11 10:31:22.551','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-09','707.1000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-11 10:31:22.744','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-09','939.8300','Consumo_agua_total_soternes','Soternes','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-11 10:31:23.341','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-10','736.2000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-12 10:02:20.270','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2022-05-11','998.5000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2022-05-12 10:02:21.546','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2022-05-11','899.9825','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2022-05-12 10:02:21.929','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2022-05-11','122248.6790','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3'),
	 ('2022-05-12 10:02:22.349','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2022-05-11','1969.7473','Consumo_agua_total_benimamet','Benimamet','N/A','m3');

COMMIT;
