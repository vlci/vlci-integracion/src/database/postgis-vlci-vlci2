-- Revert postgis-vlci-vlci2:v_migrations/448.cambiar_nombre_isbso03 from pg

BEGIN;

update vlci2.t_d_indicador set 
de_nombre_indicador_cas='Nº de usuarios con visitas de seguimiento en el Servicio de Teleasistencia',
de_nombre_indicador_val ='Nº d´usuaris amb visites de seguiment al Servei de Teleassistència',
de_descripcion_indicador_cas ='Número de personas usuarias a las que se han realizado visitas de seguimiento.',
de_descripcion_indicador_val ='Nombre de persones usuàries a les quals s´ han realitzat visites de seguiment.'
where dc_identificador_indicador ='IS.BSO.03';

COMMIT;
