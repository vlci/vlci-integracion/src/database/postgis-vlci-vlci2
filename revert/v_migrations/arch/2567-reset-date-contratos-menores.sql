-- Revert postgis-vlci-vlci2:v_migrations/2567-reset-date-contratos-menores from pg

BEGIN;

update vlci2.t_d_fecha_negocio_etls set fen = CURRENT_DATE where etl_nombre = 'py_contratacion_contratos_menores';

COMMIT;
