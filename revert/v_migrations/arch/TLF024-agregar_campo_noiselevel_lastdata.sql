-- Revert postgis-vlci-vlci2:v_migrations/TLF024-agregar_campo_noiselevel_lastdata from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_noiselevelobserved_lastdata drop column inactive;

COMMIT;
