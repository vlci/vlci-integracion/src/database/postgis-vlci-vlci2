-- Revert postgis-vlci-vlci2:v_migrations/48176.migracion_gis_trafico from pg

BEGIN;

-- Rollback 20201210_50056_update_t_d_parametros_etl_carga_trafico_gis.sql

update t_d_indicador 
set is_visible = 'S'
where dc_identificador_indicador in ('IE.CIR.011', 'IE.CIR.012');

COMMIT;
