-- Revert postgis-vlci-vlci2:v_migrations/1541-modificar-dependencia-tabla-aparcamientos from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata ALTER COLUMN nombre SET NOT NULL;

COMMIT;
