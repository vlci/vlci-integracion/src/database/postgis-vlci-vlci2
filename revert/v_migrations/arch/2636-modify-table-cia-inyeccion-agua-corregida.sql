-- Revert postgis-vlci-vlci2:v_migrations/2636-modify-table-cia-inyeccion-agua-corregida from pg

BEGIN;

-- Renaming columns in the table
ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida
RENAME COLUMN correctiondate TO correction_date;

ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida
RENAME COLUMN correctedkpivalue TO corrected_kpivalue;

COMMIT;
