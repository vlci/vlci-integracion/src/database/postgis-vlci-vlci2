-- Revert postgis-vlci-vlci2:v_migrations/1759-eliminar-datos-anomalos-junio-emt from pg

BEGIN;

INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-10 20:34:04.743','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',47187,'Titulo','Abono Especial','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:09.104','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',418,'Titulo','Abono Violeta','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:08.58','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',11174,'Titulo','Amb Tu','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:09.47','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',259,'Titulo','BB Personalizado Especial','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:10.3','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',597,'Titulo','BB Personalizado General','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:10.204','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',9046,'Titulo','Billete Ordinario','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:06.9','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',22066,'Titulo','Bonobús','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:07.528','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',3693,'Titulo','Bono Infantil','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:11.956','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',33832,'Titulo','BonoOro','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:07.21','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',1387,'Titulo','EMTicket','V','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-10 20:34:10.15','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',33,'Titulo','EMT Mascota','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:07.134','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',392,'Titulo','EMV','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:09.567','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',66,'Titulo','Estudiante FGV','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:04.971','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',512,'Titulo','Familiar Empleado EMT','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:12.8','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',94,'Titulo','Familiar Pensionista EMT','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:10.804','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',111,'Titulo','FGV Pase Empleado','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:12.72','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',11,'Titulo','Jubilado FGV','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:09.39','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',1,'Titulo','ND','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:11.15','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',165,'Titulo','Pase Ayuntamiento','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:05.396','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',438,'Titulo','Pase Empleado EMT','V','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-10 20:34:12.354','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',3,'Titulo','Pase Estudiante EMT','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:10.734','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',8,'Titulo','Pase Evento','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:05.794','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',4,'Titulo','Pase Guardia Civil','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:11.551','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',3,'Titulo','Pase Ministerio Interior','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:10.897','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',83,'Titulo','Pensionista EMT','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:10.376','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',39767,'Titulo','SUMA 10','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:11.696','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',14258,'Titulo','SUMA Mensual','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:10.438','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',111,'Titulo','SUMA Mensual Jove','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:07.595','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',75,'Titulo','SUMA T-1','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:11.228','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',54,'Titulo','SUMA T-1+','V','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-10 20:34:08.82','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',35,'Titulo','SUMA T-2','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:09.254','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',85,'Titulo','SUMA T-2+','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:09.159','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',90,'Titulo','SUMA T-3','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:11.82','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',1058,'Titulo','SUMA T-3+','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:08.668','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',616,'Titulo','T Joven  ','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:12.404','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09',1,'Titulo','Valencia Card 3 días','V','Viajes','Viatges'),
	 ('2023-06-10 20:34:12.967','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',10,'Ruta','1','V',NULL,NULL),
	 ('2023-06-10 20:34:18.63','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',8519,'Ruta','10','V',NULL,NULL),
	 ('2023-06-10 20:34:18.368','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',3174,'Ruta','11','V',NULL,NULL),
	 ('2023-06-10 20:34:12.46','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',191,'Ruta','12','V',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-10 20:34:12.522','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',1966,'Ruta','13','V',NULL,NULL),
	 ('2023-06-10 20:34:12.66','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',4270,'Ruta','14','V',NULL,NULL),
	 ('2023-06-10 20:34:12.778','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',2397,'Ruta','18','V',NULL,NULL),
	 ('2023-06-10 20:34:12.852','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',92,'Ruta','19','V',NULL,NULL),
	 ('2023-06-10 20:34:15.296','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',866,'Ruta','23','V',NULL,NULL),
	 ('2023-06-10 20:34:15.44','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',1642,'Ruta','24','V',NULL,NULL),
	 ('2023-06-10 20:34:15.697','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',4020,'Ruta','25','V',NULL,NULL),
	 ('2023-06-10 20:34:15.762','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',316,'Ruta','26','V',NULL,NULL),
	 ('2023-06-10 20:34:15.824','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',8783,'Ruta','27','V',NULL,NULL),
	 ('2023-06-10 20:34:15.869','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',1477,'Ruta','28','V',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-10 20:34:17.953','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',173,'Ruta','32','V',NULL,NULL),
	 ('2023-06-10 20:34:12.585','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',385,'Ruta','35','V',NULL,NULL),
	 ('2023-06-10 20:34:13.92','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',316,'Ruta','4','V',NULL,NULL),
	 ('2023-06-10 20:34:14.32','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',1635,'Ruta','40','V',NULL,NULL),
	 ('2023-06-10 20:34:13.348','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',3476,'Ruta','6','V',NULL,NULL),
	 ('2023-06-10 20:34:14.181','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',213,'Ruta','60','V',NULL,NULL),
	 ('2023-06-10 20:34:14.258','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',4929,'Ruta','62','V',NULL,NULL),
	 ('2023-06-10 20:34:14.442','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',1536,'Ruta','63','V',NULL,NULL),
	 ('2023-06-10 20:34:15.11','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',9638,'Ruta','64','V',NULL,NULL),
	 ('2023-06-10 20:34:15.354','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',2386,'Ruta','67','V',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-10 20:34:13.447','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',3667,'Ruta','7','V',NULL,NULL),
	 ('2023-06-10 20:34:16.312','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',4732,'Ruta','70','V',NULL,NULL),
	 ('2023-06-10 20:34:17.11','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',8196,'Ruta','71','V',NULL,NULL),
	 ('2023-06-10 20:34:17.162','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',3855,'Ruta','72','V',NULL,NULL),
	 ('2023-06-10 20:34:17.812','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',5602,'Ruta','73','V',NULL,NULL),
	 ('2023-06-10 20:34:13.763','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',4452,'Ruta','8','V',NULL,NULL),
	 ('2023-06-10 20:34:14.78','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',402,'Ruta','81','V',NULL,NULL),
	 ('2023-06-10 20:34:14.17','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',12799,'Ruta','9','V',NULL,NULL),
	 ('2023-06-10 20:34:16.583','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',6626,'Ruta','92','V',NULL,NULL),
	 ('2023-06-10 20:34:16.697','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',16219,'Ruta','93','V',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-10 20:34:17.84','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',1014,'Ruta','94','V',NULL,NULL),
	 ('2023-06-10 20:34:17.702','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',10566,'Ruta','95','V',NULL,NULL),
	 ('2023-06-10 20:34:17.865','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',88,'Ruta','96','V',NULL,NULL),
	 ('2023-06-10 20:34:18.179','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',4141,'Ruta','98','V',NULL,NULL),
	 ('2023-06-10 20:34:15.913','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',15,'Ruta','98E','V',NULL,NULL),
	 ('2023-06-10 20:34:18.306','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',19655,'Ruta','99','V',NULL,NULL),
	 ('2023-06-10 20:34:15.963','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',2671,'Ruta','C1','V',NULL,NULL),
	 ('2023-06-10 20:34:16.18','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09',20623,'Ruta','C3','V',NULL,NULL),
	 ('2023-06-11 20:34:05.57','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',29076,'Titulo','Abono Especial','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:08.94','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',221,'Titulo','Abono Violeta','S','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-11 20:34:07.873','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',6903,'Titulo','Amb Tu','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:08.882','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',123,'Titulo','BB Personalizado Especial','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:09.693','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',266,'Titulo','BB Personalizado General','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:09.558','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',8589,'Titulo','Billete Ordinario','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:07.542','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',13639,'Titulo','Bonobús','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:07.744','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',2132,'Titulo','Bono Infantil','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:11.342','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',21858,'Titulo','BonoOro','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:07.673','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',1395,'Titulo','EMTicket','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:09.416','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',31,'Titulo','EMT Mascota','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:07.613','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',340,'Titulo','EMV','S','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-11 20:34:09.88','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',32,'Titulo','Estudiante FGV','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:04.558','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',271,'Titulo','Familiar Empleado EMT','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:12.367','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',55,'Titulo','Familiar Pensionista EMT','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:10.489','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',57,'Titulo','FGV Pase Empleado','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:12.768','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',10,'Titulo','Jubilado FGV','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:10.654','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',14,'Titulo','Pase Ayuntamiento','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:06.989','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',260,'Titulo','Pase Empleado EMT','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:12.843','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',1,'Titulo','Pase Estudiante EMT','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:10.418','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',3,'Titulo','Pase Evento','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:07.271','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',1,'Titulo','Pase Guardia Civil','S','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-11 20:34:11.17','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',4,'Titulo','Pase Ministerio Interior','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:10.55','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',59,'Titulo','Pensionista EMT','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:09.899','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',28962,'Titulo','SUMA 10','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:11.266','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',8119,'Titulo','SUMA Mensual','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:10.163','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',63,'Titulo','SUMA Mensual Jove','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:07.816','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',67,'Titulo','SUMA T-1','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:10.962','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',55,'Titulo','SUMA T-1+','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:08.31','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',44,'Titulo','SUMA T-2','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:08.152','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',174,'Titulo','SUMA T-2+','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:08.209','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',89,'Titulo','SUMA T-3','S','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-11 20:34:10.908','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',946,'Titulo','SUMA T-3+','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:07.944','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',269,'Titulo','T Joven  ','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:12.919','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10',12,'Titulo','Valencia Card 3 días','S','Viajes','Viatges'),
	 ('2023-06-11 20:34:13.7','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1,'Ruta','1','S',NULL,NULL),
	 ('2023-06-11 20:34:18.448','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',5778,'Ruta','10','S',NULL,NULL),
	 ('2023-06-11 20:34:18.837','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1017,'Ruta','11','S',NULL,NULL),
	 ('2023-06-11 20:34:13.206','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1276,'Ruta','13','S',NULL,NULL),
	 ('2023-06-11 20:34:13.261','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',2746,'Ruta','14','S',NULL,NULL),
	 ('2023-06-11 20:34:13.311','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',429,'Ruta','16','S',NULL,NULL),
	 ('2023-06-11 20:34:13.386','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1657,'Ruta','18','S',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-11 20:34:13.644','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1758,'Ruta','19','S',NULL,NULL),
	 ('2023-06-11 20:34:15.481','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',757,'Ruta','23','S',NULL,NULL),
	 ('2023-06-11 20:34:15.839','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1513,'Ruta','24','S',NULL,NULL),
	 ('2023-06-11 20:34:16.116','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',4268,'Ruta','25','S',NULL,NULL),
	 ('2023-06-11 20:34:16.197','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',783,'Ruta','26','S',NULL,NULL),
	 ('2023-06-11 20:34:16.26','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',6201,'Ruta','27','S',NULL,NULL),
	 ('2023-06-11 20:34:16.779','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1724,'Ruta','28','S',NULL,NULL),
	 ('2023-06-11 20:34:13.758','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',247,'Ruta','4','S',NULL,NULL),
	 ('2023-06-11 20:34:15.16','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',635,'Ruta','40','S',NULL,NULL),
	 ('2023-06-11 20:34:14.13','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1843,'Ruta','6','S',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-11 20:34:14.939','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',602,'Ruta','60','S',NULL,NULL),
	 ('2023-06-11 20:34:15.88','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',3711,'Ruta','62','S',NULL,NULL),
	 ('2023-06-11 20:34:15.423','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',6798,'Ruta','64','S',NULL,NULL),
	 ('2023-06-11 20:34:15.782','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',2192,'Ruta','67','S',NULL,NULL),
	 ('2023-06-11 20:34:14.66','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',2902,'Ruta','7','S',NULL,NULL),
	 ('2023-06-11 20:34:17.416','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',3210,'Ruta','70','S',NULL,NULL),
	 ('2023-06-11 20:34:17.554','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',3340,'Ruta','71','S',NULL,NULL),
	 ('2023-06-11 20:34:17.712','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',2362,'Ruta','72','S',NULL,NULL),
	 ('2023-06-11 20:34:17.904','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',2660,'Ruta','73','S',NULL,NULL),
	 ('2023-06-11 20:34:14.322','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',3737,'Ruta','8','S',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-11 20:34:14.572','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1988,'Ruta','81','S',NULL,NULL),
	 ('2023-06-11 20:34:14.786','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',7526,'Ruta','9','S',NULL,NULL),
	 ('2023-06-11 20:34:17.477','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',5101,'Ruta','92','S',NULL,NULL),
	 ('2023-06-11 20:34:17.647','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',6779,'Ruta','93','S',NULL,NULL),
	 ('2023-06-11 20:34:17.784','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1450,'Ruta','94','S',NULL,NULL),
	 ('2023-06-11 20:34:17.839','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',5712,'Ruta','95','S',NULL,NULL),
	 ('2023-06-11 20:34:18.164','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',50,'Ruta','96','S',NULL,NULL),
	 ('2023-06-11 20:34:18.714','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',3639,'Ruta','98','S',NULL,NULL),
	 ('2023-06-11 20:34:16.831','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',14,'Ruta','98E','S',NULL,NULL),
	 ('2023-06-11 20:34:18.774','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',11220,'Ruta','99','S',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-11 20:34:17.88','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',1775,'Ruta','C1','S',NULL,NULL),
	 ('2023-06-11 20:34:17.14','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',14484,'Ruta','C3','S',NULL,NULL),
	 ('2023-06-11 20:34:16.523','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10',255,'Ruta','E','S',NULL,NULL),
	 ('2023-06-12 20:34:04.794','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',12664,'Titulo','Abono Especial','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:08.826','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',125,'Titulo','Abono Violeta','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:08.429','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',2865,'Titulo','Amb Tu','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:09.66','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',46,'Titulo','BB Personalizado Especial','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:27.164','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',83,'Titulo','BB Personalizado General','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:26.59','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',4497,'Titulo','Billete Ordinario','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:06.223','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',4781,'Titulo','Bonobús','D','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-12 20:34:08.256','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',846,'Titulo','Bono Infantil','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:29.558','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',8562,'Titulo','BonoOro','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:07.875','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',1057,'Titulo','EMTicket','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:26.902','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',19,'Titulo','EMT Mascota','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:24.972','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',8,'Titulo','Estudiante FGV','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:05.876','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',61,'Titulo','Familiar Empleado EMT','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:29.767','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',18,'Titulo','Familiar Pensionista EMT','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:27.62','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',18,'Titulo','FGV Pase Empleado','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:29.818','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',3,'Titulo','Jubilado FGV','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:28.66','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',3,'Titulo','Pase Ayuntamiento','D','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-12 20:34:05.96','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',87,'Titulo','Pase Empleado EMT','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:27.556','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',1,'Titulo','Pase Evento','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:28.829','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',2,'Titulo','Pase Ministerio Interior','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:28.593','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',17,'Titulo','Pensionista EMT','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:27.223','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',12612,'Titulo','SUMA 10','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:29.488','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',3425,'Titulo','SUMA Mensual','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:27.294','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',31,'Titulo','SUMA Mensual Jove','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:08.34','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',76,'Titulo','SUMA T-1','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:28.763','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',42,'Titulo','SUMA T-1+','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:08.702','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',26,'Titulo','SUMA T-2','D','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-12 20:34:08.95','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',154,'Titulo','SUMA T-2+','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:08.886','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',59,'Titulo','SUMA T-3','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:28.711','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',485,'Titulo','SUMA T-3+','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:08.577','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',133,'Titulo','T Joven  ','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:29.88','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11',1,'Titulo','Valencia Card 3 días','D','Viajes','Viatges'),
	 ('2023-06-12 20:34:30.76','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',1,'Ruta','1','D',NULL,NULL),
	 ('2023-06-12 20:34:33.845','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',403,'Ruta','10','D',NULL,NULL),
	 ('2023-06-12 20:34:30.182','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',885,'Ruta','14','D',NULL,NULL),
	 ('2023-06-12 20:34:30.449','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',462,'Ruta','18','D',NULL,NULL),
	 ('2023-06-12 20:34:30.708','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',529,'Ruta','19','D',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-12 20:34:31.655','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',649,'Ruta','23','D',NULL,NULL),
	 ('2023-06-12 20:34:31.703','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',1610,'Ruta','24','D',NULL,NULL),
	 ('2023-06-12 20:34:31.965','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',3282,'Ruta','25','D',NULL,NULL),
	 ('2023-06-12 20:34:32.27','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',2471,'Ruta','27','D',NULL,NULL),
	 ('2023-06-12 20:34:31.28','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',265,'Ruta','60','D',NULL,NULL),
	 ('2023-06-12 20:34:31.345','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',181,'Ruta','62','D',NULL,NULL),
	 ('2023-06-12 20:34:31.603','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',2967,'Ruta','64','D',NULL,NULL),
	 ('2023-06-12 20:34:30.917','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',1369,'Ruta','7','D',NULL,NULL),
	 ('2023-06-12 20:34:32.57','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',657,'Ruta','70','D',NULL,NULL),
	 ('2023-06-12 20:34:32.871','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',747,'Ruta','71','D',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-12 20:34:33.185','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',732,'Ruta','72','D',NULL,NULL),
	 ('2023-06-12 20:34:33.318','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',192,'Ruta','73','D',NULL,NULL),
	 ('2023-06-12 20:34:31.165','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',489,'Ruta','8','D',NULL,NULL),
	 ('2023-06-12 20:34:31.223','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',2090,'Ruta','9','D',NULL,NULL),
	 ('2023-06-12 20:34:32.509','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',4328,'Ruta','92','D',NULL,NULL),
	 ('2023-06-12 20:34:32.82','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',6225,'Ruta','93','D',NULL,NULL),
	 ('2023-06-12 20:34:33.128','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',599,'Ruta','94','D',NULL,NULL),
	 ('2023-06-12 20:34:33.263','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',5198,'Ruta','95','D',NULL,NULL),
	 ('2023-06-12 20:34:33.576','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',41,'Ruta','96','D',NULL,NULL),
	 ('2023-06-12 20:34:33.901','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',7745,'Ruta','99','D',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-06-12 20:34:32.458','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',8348,'Ruta','C3','D',NULL,NULL),
	 ('2023-06-12 20:34:32.384','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11',342,'Ruta','E','D',NULL,NULL);


COMMIT;
