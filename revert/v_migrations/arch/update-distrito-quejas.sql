-- Revert postgis-vlci-vlci2:v_migrations/update-distrito-quejas from pg

BEGIN;

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Ciutat Vella'
where distrito_localizacion='01-Ciutat Vella';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='L''Eixample'
where distrito_localizacion='02-L''Eixample';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Extramurs'
where distrito_localizacion='03-Extramurs';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Campanar'
where distrito_localizacion='04-Campanar';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='La Saidia'
where distrito_localizacion='05-La Saidia';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='El Pla del Real'
where distrito_localizacion='06-El Pla del Real';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='L''Olivereta'
where distrito_localizacion='07-L''Olivereta';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Patraix'
where distrito_localizacion='08-Patraix';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Jesus'
where distrito_localizacion='09-Jesus';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Quatre Carreres'
where distrito_localizacion='10-Quatre Carreres';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Poblats Maritims'
where distrito_localizacion='11-Poblats Maritims';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Camins al Grau'
where distrito_localizacion='12-Camins al Grau';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Algiros'
where distrito_localizacion='13-Algiros';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Benimaclet'
where distrito_localizacion='14-Benimaclet';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Rascanya'
where distrito_localizacion='15-Rascanya';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Benicalap'
where distrito_localizacion='16-Benicalap';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Pobles del Nord'
where distrito_localizacion='17-Pobles del Nord';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Pobles de l''Oest'
where distrito_localizacion='18-Pobles de l''Oest';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Pobles del Sud'
where distrito_localizacion='19-Pobles del Sud';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='Fora de Valencia'
where distrito_localizacion='98-Fora de Valencia';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='No consta'
where distrito_localizacion='99-No consta';

COMMIT;
