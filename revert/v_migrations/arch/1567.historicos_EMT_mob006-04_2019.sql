-- Revert postgis-vlci-vlci2:v_migrations/1567.historicos_EMT_mob006-04_2019 from pg

BEGIN;

DELETE FROM vlci2.t_datos_cb_emt_kpi WHERE entityid = 'mob006-04' 
and calculationperiod < '2020-01-01'
and calculationperiod >= '2019-01-01';

COMMIT;
