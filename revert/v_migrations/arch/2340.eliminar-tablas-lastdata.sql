-- Revert postgis-vlci-vlci2:v_migrations/2340.eliminar-tablas-lastdata from pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_airqualityobserved_lastdata_va (
	recvtime timestamptz NOT NULL,
	fiwareservicepath text NULL,
	entitytype text NULL,
	entityid text NOT NULL,
	dateobserved timestamptz NOT NULL,
	address text NULL,
	project text NULL,
	maintenanceowner text NULL,
	operationalstatus text NULL,
	"location" public.geometry(point) NULL,
	calidad_ambiental text NULL,
	no2value float4 NULL,
	no2type text NULL,
	novalue float4 NULL,
	notype text NULL,
	noxvalue float4 NULL,
	noxtype text NULL,
	o3value float4 NULL,
	o3type text NULL,
	pm1value float4 NULL,
	pm1type text NULL,
	pm10value float4 NULL,
	pm10type text NULL,
	pm25value float4 NULL,
	pm25type text NULL,
	so2value float4 NULL,
	so2type text NULL,
	inactive text NULL
);

CREATE TABLE vlci2.t_datos_cb_airqualityobserved_lastdata_vm (
	recvtime timestamptz NOT NULL,
	fiwareservicepath text NULL,
	entitytype text NULL,
	entityid text NOT NULL,
	dateobserved timestamptz NOT NULL,
	address text NULL,
	project text NULL,
	maintenanceowner text NULL,
	operationalstatus text NULL,
	"location" public.geometry(point) NULL,
	calidad_ambiental text NULL,
	no2value float4 NULL,
	no2type text NULL,
	novalue float4 NULL,
	notype text NULL,
	noxvalue float4 NULL,
	noxtype text NULL,
	o3value float4 NULL,
	o3type text NULL,
	pm1value float4 NULL,
	pm1type text NULL,
	pm10value float4 NULL,
	pm10type text NULL,
	pm25value float4 NULL,
	pm25type text NULL,
	so2value float4 NULL,
	so2type text NULL,
	inactive text NULL
);

COMMIT;
