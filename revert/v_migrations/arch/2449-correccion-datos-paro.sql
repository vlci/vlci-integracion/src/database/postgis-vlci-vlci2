-- Revert postgis-vlci-vlci2:v_migrations/2449-correccion-datos-paro from pg

BEGIN;

UPDATE vlci2.t_datos_cb_estadistica_paro
	SET calculationperiod='2024-12'
	WHERE entityid='t_datos_cb_estadistica' AND calculationperiod='2023-12' AND sliceanddicevalue1='N/A';
    
COMMIT;
