-- Revert postgis-vlci-vlci2:v_migrations/566.cambiar_periodicidad_isbso04-05 from pg

BEGIN;

UPDATE vlci2.t_d_indicador SET dc_identificador_indicador='IS.BSO.04' WHERE dc_identificador_indicador='IS.BSO.04a';
UPDATE vlci2.t_d_indicador SET dc_identificador_indicador='IS.BSO.05' WHERE dc_identificador_indicador='IS.BSO.05a';

COMMIT;
