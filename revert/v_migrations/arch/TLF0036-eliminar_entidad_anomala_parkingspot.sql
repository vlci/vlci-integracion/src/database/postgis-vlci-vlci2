-- Revert postgis-vlci-vlci2:v_migrations/TLF0036-eliminar_entidad_anomala_parkingspot from pg
BEGIN;

INSERT INTO
    vlci2.t_datos_cb_parkingspot_lastdata (
        recvtime,
        fiwareservicepath,
        entitytype,
        entityid,
        category,
        datemodified,
        description,
        "name",
        operationalstatus,
        status,
        "location",
        maintenanceowner,
        project
    )
VALUES
(
        '2024-02-22 15:04:35.214',
        '/t_datos_cb_parkingspot',
        'ParkingSpot',
        'parkingspot-urb99999',
        'pmr',
        '2024-02-22T15:04:34.832Z',
        'Passeig de neptu prueba',
        'prueba',
        NULL,
        'free',
        'POINT (-0.324008241 39.464978956)' :: public.geometry,
        '',
        'impulsoVLCi-red.es'
    );

COMMIT;