-- Revert postgis-vlci-vlci2:v_migrations/2677.cambiar-tipo-dato-fecha-negocio from pg

BEGIN;

alter table vlci2.t_datos_etl_unidades_administrativas_areas
alter column fecha_negocio type timestamp
USING fecha_negocio::timestamp without time zone;

COMMIT;
