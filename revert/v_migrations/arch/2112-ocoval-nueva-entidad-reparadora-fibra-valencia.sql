-- Revert postgis-vlci-vlci2:v_migrations/2112-ocoval-nueva-entidad-reparadora-fibra-valencia from pg

BEGIN;

DELETE FROM vlci2.vlci_ocoval_entidades WHERE compania = 'Compañías de Servicios' and entidad = 'Fibra Valencia';

COMMIT;
