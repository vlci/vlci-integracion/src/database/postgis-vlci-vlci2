-- Revert postgis-vlci-vlci2:v_migrations/2572.Add_gestion_fechas_intensidad_trafico_validada_edit from pg

BEGIN;

update vlci2.t_d_fecha_negocio_etls
set fen = now()
where etl_id = 300;

update vlci2.t_d_fecha_negocio_etls
set fen = now()
where etl_id = 301;

COMMIT;
