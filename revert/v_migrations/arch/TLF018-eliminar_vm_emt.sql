-- Revert postgis-vlci-vlci2:v_migrations/TLF018-eliminar_vm_emt from pg

BEGIN;

CREATE MATERIALIZED VIEW vlci2.vw_t_datos_cb_emt_kpi_materialized AS
SELECT  kpivalue,
		DATE_PART('year', CAST(calculationperiod AS DATE)) AS anyo,
		DATE_PART('month', CAST(calculationperiod AS DATE)) as mes
FROM vlci2.t_datos_cb_emt_kpi a11 
where a11.sliceanddice1 = 'Titulo';

COMMIT;
