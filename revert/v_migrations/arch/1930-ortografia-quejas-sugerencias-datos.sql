-- Revert postgis-vlci-vlci2:v_migrations/1930-ortografia-quejas-sugerencias-datos from pg

BEGIN;

UPDATE vlci2.t_datos_etl_quejas_sugerencias
	SET tema='Tramitacion administrativa, tributos y sanciones'
	WHERE tema='Tramitación administrativa, tributos y sanciones';

UPDATE vlci2.t_datos_etl_quejas_sugerencias
	SET tema='Via publica reparacion de deficiencias'
	WHERE tema='Vía pública reparación de deficiencias';

COMMIT;
