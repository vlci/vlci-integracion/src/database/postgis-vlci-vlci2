-- Revert postgis-vlci-vlci2:v_migrations/TLF027-Anyadir-campo-project from pg
BEGIN;

ALTER TABLE
    vlci2.t_datos_cb_alumbrado_device_lastdata DROP COLUMN IF EXISTS project;

COMMIT;