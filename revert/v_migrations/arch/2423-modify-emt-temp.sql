-- Revert postgis-vlci-vlci2:v_migrations/2423-modify-emt-temp from pg

BEGIN;

ALTER TABLE vlci2.vlci_emt_tmp RENAME COLUMN linea TO ruta;

COMMIT;
