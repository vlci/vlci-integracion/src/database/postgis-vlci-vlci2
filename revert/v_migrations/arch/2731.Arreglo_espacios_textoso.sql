-- Revert postgis-vlci-vlci2:v_migrations/2731.Arreglo_espacios_textoso from pg

BEGIN;

update vlci2.t_datos_cb_trafico_kpi_validados_mes set name = 'Accés Barcelona  entrada i eixida ' where tramo = 'Accés Barcelona  entrada i eixida  Entre V-21 i Rotonda ';
update vlci2.t_datos_cb_trafico_kpi_validados_mes set name = 'Accés per V-31  Pista de Silla ' where tramo = 'Accés per V-31  Pista de Silla  Entre Bulevard Sud i V-31 ';
update vlci2.t_datos_cb_trafico_kpi_validados_mes set name = 'Corts Valencianes  Accés per CV-35 ' where tramo = 'Corts Valencianes  Accés per CV-35  Entre Camp del Túria i La Safor ';
update vlci2.t_datos_cb_trafico_kpi_validados_mes set name = 'Prolongació Joan XXIII' where tramo = 'Prolongació Joan XXIII  Entre Germans Machado i Salvador Cerveró ';
update vlci2.t_datos_cb_trafico_kpi_validados_mes set name = 'Accés a Arxiduc Carles pel Camí nou de Picanya' where tramo = 'Accés a Arxiduc Carles pel Camí nou de Picanya  Entre V-30 i Pedrapiquers ';

update vlci2.t_datos_cb_trafico_kpi_validados_anyo set name = 'Accés Barcelona  entrada i eixida ' where tramo = 'Accés Barcelona  entrada i eixida  Entre V-21 i Rotonda ';
update vlci2.t_datos_cb_trafico_kpi_validados_anyo set name = 'Accés per V-31  Pista de Silla ' where tramo = 'Accés per V-31  Pista de Silla  Entre Bulevard Sud i V-31 ';
update vlci2.t_datos_cb_trafico_kpi_validados_anyo set name = 'Corts Valencianes  Accés per CV-35 ' where tramo = 'Corts Valencianes  Accés per CV-35  Entre Camp del Túria i La Safor ';
update vlci2.t_datos_cb_trafico_kpi_validados_anyo set name = 'Prolongació Joan XXIII' where tramo = 'Prolongació Joan XXIII  Entre Germans Machado i Salvador Cerveró ';
update vlci2.t_datos_cb_trafico_kpi_validados_anyo set name = 'Accés a Arxiduc Carles pel Camí nou de Picanya' where tramo = 'Accés a Arxiduc Carles pel Camí nou de Picanya  Entre V-30 i Pedrapiquers ';


COMMIT;
