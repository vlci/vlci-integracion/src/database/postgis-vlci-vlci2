-- Revert postgis-vlci-vlci2:v_migrations/TLF028-correcion-datos-ruzafa from pg

BEGIN;

UPDATE t_f_text_cb_sonometros_ruzafa_daily
set laeq_d = '-30' || laeq_d
where recvtime>='2023-12-01 08:00:18.600 +0100' and recvtime <= '2023-12-13 08:00:24.900 +0100';

UPDATE t_f_text_cb_sonometros_ruzafa_daily
set laeq = '-30' || laeq
where recvtime>='2023-12-01 08:00:18.600 +0100' and recvtime <= '2023-12-13 08:00:24.900 +0100';

UPDATE t_f_text_cb_sonometros_ruzafa_daily
set laeq_e ='-30' || laeq_e
where recvtime>='2023-12-01 08:00:18.600 +0100' and recvtime <= '2023-12-13 08:00:24.900 +0100';

UPDATE t_f_text_cb_sonometros_ruzafa_daily
set laeq_n ='-30' || laeq_n
where recvtime>='2023-12-01 08:00:18.600 +0100' and recvtime <= '2023-12-13 08:00:24.900 +0100';

UPDATE t_f_text_cb_sonometros_ruzafa_daily
set laeq_den  ='-30' || laeq_den 
where recvtime>='2023-12-01 08:00:18.600 +0100' and recvtime <= '2023-12-13 08:00:24.900 +0100';

COMMIT;
