-- Revert postgis-vlci-vlci2:v_migrations/1863-fix-campo-fecha-housekeeping-quejas-sugerencias from pg

BEGIN;

UPDATE vlci2.housekeeping_config
	SET colfecha='aud_user_ins'
	WHERE table_nam='t_datos_etl_quejas_sugerencias_hist';
COMMIT;
