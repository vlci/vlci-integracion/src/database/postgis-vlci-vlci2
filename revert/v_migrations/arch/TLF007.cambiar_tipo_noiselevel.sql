-- Revert postgis-vlci-vlci2:v_migrations/TLF007.cambiar_tipo_noiselevel from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_noiselevelobserved_lastdata ALTER COLUMN laeq TYPE int4;

COMMIT;
