-- Revert postgis-vlci-vlci2:v_migrations/1544-modificar-campos-aparcamientos from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos ALTER COLUMN recvtime TYPE timestamptz USING recvtime::timestamptz;

COMMIT;

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata ALTER COLUMN recvtime TYPE timestamptz USING recvtime::timestamptz;

COMMIT;