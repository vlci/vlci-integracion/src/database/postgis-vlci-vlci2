-- Revert postgis-vlci-vlci2:v_migrations/TLF044-modificar_tabla_arboles_solares from pg

BEGIN;
ALTER TABLE vlci2.t_datos_cb_generacion_energia_solartree ALTER COLUMN dateobserved TYPE timestamp with time zone USING dateobserved::timestamp with time zone;
ALTER TABLE vlci2.t_datos_cb_generacion_energia_solartree_lastdata ALTER COLUMN dateobserved TYPE timestamp with time zone USING dateobserved::timestamp with time zone;


COMMIT;
