-- Revert postgis-vlci-vlci2:v_migrations/1673-creacion-usuarios from pg

BEGIN;

delete from vlci2.t_d_user_rol where pk_user = 'U19316' and rol = 'ESTANDAR';
INSERT INTO vlci2.t_d_user_rol (pk_user,rol,cod_situacion,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	('U19316','ESTANDAR','B','2021-04-22 22:20:11','ETL BDO','2021-10-18 20:34:06.993667','ETL BDO');
DELETE FROM vlci2.t_d_user_rol WHERE pk_user= 'U18587' and rol = 'ESTANDAR';

COMMIT;
