-- Revert postgis-vlci-vlci2:v_migrations/1714-datos-anomalos-emt-vlci2 from pg

BEGIN;

INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-20 20:34:05.72','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',32,'Titulo','Familiar Empleado EMT','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:06.123','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',2450,'Titulo','Abono Especial','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:06.175','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',37,'Titulo','Pase Empleado EMT','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:06.214','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',1254,'Titulo','Bonobús','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:06.251','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',198,'Titulo','Bono Infantil','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:06.289','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',675,'Titulo','Amb Tu','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:06.328','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',39,'Titulo','T Joven  ','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:06.627','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',2,'Titulo','SUMA T-2','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:06.815','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',21,'Titulo','Abono Violeta','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:06.888','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',3,'Titulo','SUMA T-3','V','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-20 20:34:07.39','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',10,'Titulo','SUMA T-2+','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:07.141','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',17,'Titulo','BB Personalizado Especial','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:07.389','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',3,'Titulo','Estudiante FGV','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:07.422','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',406,'Titulo','Billete Ordinario','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:07.655','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',2,'Titulo','EMT Mascota','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:09.167','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',49,'Titulo','BB Personalizado General','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:09.211','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',2185,'Titulo','SUMA 10','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:09.366','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',8,'Titulo','SUMA Mensual Jove','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:09.411','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',3,'Titulo','FGV Pase Empleado','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:09.491','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',3,'Titulo','Pensionista EMT','V','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-20 20:34:09.622','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',10,'Titulo','Pase Ayuntamiento','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:09.726','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',57,'Titulo','SUMA T-3+','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:24.511','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',6,'Titulo','SUMA T-1+','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:26.825','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',821,'Titulo','SUMA Mensual','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:27.382','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',1657,'Titulo','BonoOro','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:27.584','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19',6,'Titulo','Familiar Pensionista EMT','V','Viajes','Viatges'),
	 ('2023-05-20 20:34:27.7','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',283,'Ruta','12','V',NULL,NULL),
	 ('2023-05-20 20:34:27.757','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',229,'Ruta','18','V',NULL,NULL),
	 ('2023-05-20 20:34:28.422','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',351,'Ruta','19','V',NULL,NULL),
	 ('2023-05-20 20:34:28.671','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',29,'Ruta','1','V',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-20 20:34:28.734','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',677,'Ruta','8','V',NULL,NULL),
	 ('2023-05-20 20:34:28.997','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',380,'Ruta','9','V',NULL,NULL),
	 ('2023-05-20 20:34:29.34','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',306,'Ruta','60','V',NULL,NULL),
	 ('2023-05-20 20:34:29.264','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',200,'Ruta','62','V',NULL,NULL),
	 ('2023-05-20 20:34:30.36','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',158,'Ruta','40','V',NULL,NULL),
	 ('2023-05-20 20:34:30.15','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',120,'Ruta','63','V',NULL,NULL),
	 ('2023-05-20 20:34:30.187','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',168,'Ruta','64','V',NULL,NULL),
	 ('2023-05-20 20:34:30.417','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',20,'Ruta','67','V',NULL,NULL),
	 ('2023-05-20 20:34:30.463','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',281,'Ruta','25','V',NULL,NULL),
	 ('2023-05-20 20:34:30.884','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',326,'Ruta','27','V',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-20 20:34:30.968','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',489,'Ruta','28','V',NULL,NULL),
	 ('2023-05-20 20:34:31.29','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',22,'Ruta','98E','V',NULL,NULL),
	 ('2023-05-20 20:34:31.67','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',421,'Ruta','C2','V',NULL,NULL),
	 ('2023-05-20 20:34:31.119','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',576,'Ruta','C3','V',NULL,NULL),
	 ('2023-05-20 20:34:31.158','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',751,'Ruta','70','V',NULL,NULL),
	 ('2023-05-20 20:34:31.429','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',837,'Ruta','93','V',NULL,NULL),
	 ('2023-05-20 20:34:31.583','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',240,'Ruta','94','V',NULL,NULL),
	 ('2023-05-20 20:34:31.646','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',640,'Ruta','95','V',NULL,NULL),
	 ('2023-05-20 20:34:31.712','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',350,'Ruta','30','V',NULL,NULL),
	 ('2023-05-20 20:34:31.747','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',478,'Ruta','98','V',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-20 20:34:31.799','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19',1622,'Ruta','99','V',NULL,NULL),
	 ('2023-05-21 20:34:04.977','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',98,'Titulo','Abono Especial','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:05.4','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',1,'Titulo','Familiar Empleado EMT','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:05.779','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',1,'Titulo','BB Personalizado General','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:06.71','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',61,'Titulo','SUMA 10','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:07.291','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',29,'Titulo','Bonobús','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:07.356','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',2,'Titulo','Bono Infantil','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:07.481','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',19,'Titulo','Amb Tu','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:07.729','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',9,'Titulo','SUMA T-3+','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:07.967','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',1,'Titulo','T Joven  ','S','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-21 20:34:08.173','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',2,'Titulo','Abono Violeta','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:08.432','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',35,'Titulo','SUMA Mensual','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:08.495','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',12,'Titulo','BonoOro','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:08.557','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20',19,'Titulo','Billete Ordinario','S','Viajes','Viatges'),
	 ('2023-05-21 20:34:08.596','/emt','KeyPerformanceIndicator','mob006-04','2023-05-20',2,'Ruta','1','S',NULL,NULL),
	 ('2023-05-21 20:34:08.652','/emt','KeyPerformanceIndicator','mob006-04','2023-05-20',7,'Ruta','25','S',NULL,NULL),
	 ('2023-05-21 20:34:08.712','/emt','KeyPerformanceIndicator','mob006-04','2023-05-20',90,'Ruta','19','S',NULL,NULL),
	 ('2023-05-21 20:34:08.75','/emt','KeyPerformanceIndicator','mob006-04','2023-05-20',27,'Ruta','9','S',NULL,NULL),
	 ('2023-05-21 20:34:09.102','/emt','KeyPerformanceIndicator','mob006-04','2023-05-20',163,'Ruta','C3','S',NULL,NULL),
	 ('2023-05-22 20:34:05.477','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21',44,'Titulo','Abono Especial','D','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-22 20:34:05.583','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21',1,'Titulo','SUMA T-1','D','Viajes','Viatges'),
	 ('2023-05-22 20:34:05.651','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21',6,'Titulo','Amb Tu','D','Viajes','Viatges'),
	 ('2023-05-22 20:34:05.92','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21',1,'Titulo','Abono Violeta','D','Viajes','Viatges'),
	 ('2023-05-22 20:34:05.994','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21',24,'Titulo','SUMA 10','D','Viajes','Viatges'),
	 ('2023-05-22 20:34:07.524','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21',1,'Titulo','FGV Pase Empleado','D','Viajes','Viatges'),
	 ('2023-05-22 20:34:08.644','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21',3,'Titulo','SUMA Mensual','D','Viajes','Viatges'),
	 ('2023-05-22 20:34:08.714','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21',7,'Titulo','Bonobús','D','Viajes','Viatges'),
	 ('2023-05-22 20:34:08.772','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21',1,'Titulo','BonoOro','D','Viajes','Viatges'),
	 ('2023-05-22 20:34:08.813','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21',39,'Titulo','Billete Ordinario','D','Viajes','Viatges'),
	 ('2023-05-22 20:34:08.861','/emt','KeyPerformanceIndicator','mob006-04','2023-05-21',2,'Ruta','1','D',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-22 20:34:08.921','/emt','KeyPerformanceIndicator','mob006-04','2023-05-21',80,'Ruta','19','D',NULL,NULL),
	 ('2023-05-22 20:34:08.982','/emt','KeyPerformanceIndicator','mob006-04','2023-05-21',16,'Ruta','73','D',NULL,NULL),
	 ('2023-05-22 20:34:09.23','/emt','KeyPerformanceIndicator','mob006-04','2023-05-21',29,'Ruta','C3','D',NULL,NULL),
	 ('2023-05-24 20:34:05.441','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',13421,'Titulo','Abono Especial','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:06.579','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',156,'Titulo','Familiar Empleado EMT','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:06.804','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',161,'Titulo','Pase Empleado EMT','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.21','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',1,'Titulo','Valencia Card 1 día','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.72','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',7464,'Titulo','Bonobús','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.109','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',163,'Titulo','EMV','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.256','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',1281,'Titulo','EMTicket','M','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-24 20:34:08.324','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',1028,'Titulo','Bono Infantil','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.379','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',15,'Titulo','SUMA T-1','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.415','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',3489,'Titulo','Amb Tu','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.445','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',182,'Titulo','T Joven  ','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.491','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',18,'Titulo','SUMA T-2','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.533','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',42,'Titulo','SUMA T-3','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.636','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',148,'Titulo','Abono Violeta','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.669','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',45,'Titulo','SUMA T-2+','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.702','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',102,'Titulo','BB Personalizado Especial','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:08.754','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',26,'Titulo','Estudiante FGV','M','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-24 20:34:09.24','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',2317,'Titulo','Billete Ordinario','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:09.8','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',3,'Titulo','EMT Mascota','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:09.164','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',237,'Titulo','BB Personalizado General','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:09.25','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',12466,'Titulo','SUMA 10','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:09.318','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',48,'Titulo','SUMA Mensual Jove','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:09.412','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',1,'Titulo','Pase Evento','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:09.476','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',35,'Titulo','FGV Pase Empleado','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:09.545','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',21,'Titulo','Pensionista EMT','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:10.5','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',83,'Titulo','Pase Ayuntamiento','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:10.182','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',311,'Titulo','SUMA T-3+','M','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-24 20:34:10.255','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',22,'Titulo','SUMA T-1+','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:10.287','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',1,'Titulo','Pase Ministerio Interior','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:10.64','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',4528,'Titulo','SUMA Mensual','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:10.922','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',19,'Titulo','Familiar Pensionista EMT','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:10.955','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',11449,'Titulo','BonoOro','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:10.983','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',4,'Titulo','Jubilado FGV','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:11.9','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23',3,'Titulo','Pase Estudiante EMT','M','Viajes','Viatges'),
	 ('2023-05-24 20:34:11.36','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',938,'Ruta','12','M',NULL,NULL),
	 ('2023-05-24 20:34:11.62','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',419,'Ruta','35','M',NULL,NULL),
	 ('2023-05-24 20:34:11.296','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',1546,'Ruta','18','M',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-24 20:34:11.372','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',262,'Ruta','19','M',NULL,NULL),
	 ('2023-05-24 20:34:11.414','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',70,'Ruta','1','M',NULL,NULL),
	 ('2023-05-24 20:34:11.465','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',691,'Ruta','4','M',NULL,NULL),
	 ('2023-05-24 20:34:11.566','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',4007,'Ruta','6','M',NULL,NULL),
	 ('2023-05-24 20:34:11.609','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',2112,'Ruta','7','M',NULL,NULL),
	 ('2023-05-24 20:34:11.64','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',881,'Ruta','8','M',NULL,NULL),
	 ('2023-05-24 20:34:11.739','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',717,'Ruta','81','M',NULL,NULL),
	 ('2023-05-24 20:34:11.818','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',959,'Ruta','9','M',NULL,NULL),
	 ('2023-05-24 20:34:12.56','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',823,'Ruta','60','M',NULL,NULL),
	 ('2023-05-24 20:34:12.81','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',388,'Ruta','40','M',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-24 20:34:12.177','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',240,'Ruta','62','M',NULL,NULL),
	 ('2023-05-24 20:34:12.442','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',104,'Ruta','63','M',NULL,NULL),
	 ('2023-05-24 20:34:12.61','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',1138,'Ruta','64','M',NULL,NULL),
	 ('2023-05-24 20:34:12.941','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',464,'Ruta','67','M',NULL,NULL),
	 ('2023-05-24 20:34:13.64','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',241,'Ruta','25','M',NULL,NULL),
	 ('2023-05-24 20:34:13.99','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',3479,'Ruta','27','M',NULL,NULL),
	 ('2023-05-24 20:34:13.147','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',399,'Ruta','28','M',NULL,NULL),
	 ('2023-05-24 20:34:13.236','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',19,'Ruta','98E','M',NULL,NULL),
	 ('2023-05-24 20:34:13.354','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',2870,'Ruta','C1','M',NULL,NULL),
	 ('2023-05-24 20:34:13.425','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',1954,'Ruta','C3','M',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-24 20:34:13.559','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',1559,'Ruta','92','M',NULL,NULL),
	 ('2023-05-24 20:34:14.198','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',1909,'Ruta','70','M',NULL,NULL),
	 ('2023-05-24 20:34:14.349','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',5537,'Ruta','93','M',NULL,NULL),
	 ('2023-05-24 20:34:14.61','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',3530,'Ruta','71','M',NULL,NULL),
	 ('2023-05-24 20:34:14.787','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',207,'Ruta','94','M',NULL,NULL),
	 ('2023-05-24 20:34:14.899','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',3582,'Ruta','95','M',NULL,NULL),
	 ('2023-05-24 20:34:15.69','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',3004,'Ruta','73','M',NULL,NULL),
	 ('2023-05-24 20:34:15.441','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',342,'Ruta','30','M',NULL,NULL),
	 ('2023-05-24 20:34:15.71','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',13,'Ruta','96','M',NULL,NULL),
	 ('2023-05-24 20:34:15.85','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',244,'Ruta','31','M',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-24 20:34:15.892','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',1537,'Ruta','10','M',NULL,NULL),
	 ('2023-05-24 20:34:15.919','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',681,'Ruta','98','M',NULL,NULL),
	 ('2023-05-24 20:34:15.966','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',357,'Ruta','32','M',NULL,NULL),
	 ('2023-05-24 20:34:16.27','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',10500,'Ruta','99','M',NULL,NULL),
	 ('2023-05-24 20:34:16.109','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23',1567,'Ruta','11','M',NULL,NULL),
	 ('2023-05-29 20:34:06.319','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',27849,'Titulo','Abono Especial','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:07.86','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',195,'Titulo','Familiar Empleado EMT','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:08.122','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',303,'Titulo','Pase Empleado EMT','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:08.166','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',12008,'Titulo','Bonobús','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:08.208','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',138,'Titulo','EMV','D','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-29 20:34:08.464','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',1707,'Titulo','Bono Infantil','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:08.498','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',123,'Titulo','SUMA T-1','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:08.535','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',131,'Titulo','SUMA T-2','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:08.634','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',6194,'Titulo','Amb Tu','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:08.898','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',324,'Titulo','T Joven  ','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:09.169','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',202,'Titulo','Abono Violeta','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:09.212','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',235,'Titulo','SUMA T-3','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:10.68','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',454,'Titulo','SUMA T-2+','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:10.132','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',120,'Titulo','BB Personalizado Especial','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:10.435','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',18,'Titulo','Estudiante FGV','D','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-29 20:34:10.476','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',11257,'Titulo','Billete Ordinario','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:10.536','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',22,'Titulo','EMT Mascota','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:10.607','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',198,'Titulo','BB Personalizado General','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:10.85','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',27013,'Titulo','SUMA 10','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:10.904','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',57,'Titulo','SUMA Mensual Jove','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:11.22','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',5,'Titulo','Pase Evento','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:11.303','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',50,'Titulo','FGV Pase Empleado','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:11.579','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',37,'Titulo','Pensionista EMT','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:11.62','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',10,'Titulo','Pase Ayuntamiento','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:11.654','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',2023,'Titulo','SUMA T-3+','D','Viajes','Viatges');
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-29 20:34:11.711','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',125,'Titulo','SUMA T-1+','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:12.11','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',4,'Titulo','Pase Ministerio Interior','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:12.257','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',7346,'Titulo','SUMA Mensual','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:12.299','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',22735,'Titulo','BonoOro','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:12.364','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',49,'Titulo','Familiar Pensionista EMT','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:12.402','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',7,'Titulo','Jubilado FGV','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:12.504','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',2,'Titulo','Pase Estudiante EMT','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:12.571','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28',7,'Titulo','Valencia Card 3 días','D','Viajes','Viatges'),
	 ('2023-05-29 20:34:12.85','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1552,'Ruta','12','D',NULL,NULL),
	 ('2023-05-29 20:34:13.119','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',2671,'Ruta','35','D',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-29 20:34:13.182','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',474,'Ruta','13','D',NULL,NULL),
	 ('2023-05-29 20:34:13.223','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1523,'Ruta','14','D',NULL,NULL),
	 ('2023-05-29 20:34:13.273','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',2644,'Ruta','16','D',NULL,NULL),
	 ('2023-05-29 20:34:13.323','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',814,'Ruta','18','D',NULL,NULL),
	 ('2023-05-29 20:34:13.37','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',5041,'Ruta','19','D',NULL,NULL),
	 ('2023-05-29 20:34:13.445','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',19,'Ruta','1','D',NULL,NULL),
	 ('2023-05-29 20:34:13.5','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',4189,'Ruta','4','D',NULL,NULL),
	 ('2023-05-29 20:34:13.957','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',3343,'Ruta','6','D',NULL,NULL),
	 ('2023-05-29 20:34:14.309','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1853,'Ruta','7','D',NULL,NULL),
	 ('2023-05-29 20:34:14.582','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1773,'Ruta','8','D',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-29 20:34:14.637','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',3638,'Ruta','9','D',NULL,NULL),
	 ('2023-05-29 20:34:14.7','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',2789,'Ruta','81','D',NULL,NULL),
	 ('2023-05-29 20:34:15.284','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',3154,'Ruta','60','D',NULL,NULL),
	 ('2023-05-29 20:34:15.354','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',2279,'Ruta','62','D',NULL,NULL),
	 ('2023-05-29 20:34:15.588','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1091,'Ruta','40','D',NULL,NULL),
	 ('2023-05-29 20:34:15.623','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',3009,'Ruta','64','D',NULL,NULL),
	 ('2023-05-29 20:34:15.651','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1007,'Ruta','67','D',NULL,NULL),
	 ('2023-05-29 20:34:16.155','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',250,'Ruta','23','D',NULL,NULL),
	 ('2023-05-29 20:34:16.19','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',820,'Ruta','24','D',NULL,NULL),
	 ('2023-05-29 20:34:16.256','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',3317,'Ruta','25','D',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-29 20:34:16.327','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',720,'Ruta','26','D',NULL,NULL),
	 ('2023-05-29 20:34:16.384','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',3836,'Ruta','27','D',NULL,NULL),
	 ('2023-05-29 20:34:16.442','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',149,'Ruta','E','D',NULL,NULL),
	 ('2023-05-29 20:34:16.506','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',2622,'Ruta','28','D',NULL,NULL),
	 ('2023-05-29 20:34:16.543','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',12,'Ruta','98E','D',NULL,NULL),
	 ('2023-05-29 20:34:16.576','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1654,'Ruta','C1','D',NULL,NULL),
	 ('2023-05-29 20:34:16.828','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1695,'Ruta','C2','D',NULL,NULL),
	 ('2023-05-29 20:34:16.89','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',13945,'Ruta','C3','D',NULL,NULL),
	 ('2023-05-29 20:34:17.168','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',2870,'Ruta','70','D',NULL,NULL),
	 ('2023-05-29 20:34:17.413','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',4191,'Ruta','92','D',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-29 20:34:17.644','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',3950,'Ruta','93','D',NULL,NULL),
	 ('2023-05-29 20:34:17.697','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',2762,'Ruta','71','D',NULL,NULL),
	 ('2023-05-29 20:34:17.94','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1299,'Ruta','72','D',NULL,NULL),
	 ('2023-05-29 20:34:17.99','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1762,'Ruta','94','D',NULL,NULL),
	 ('2023-05-29 20:34:18.26','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1649,'Ruta','73','D',NULL,NULL),
	 ('2023-05-29 20:34:18.55','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',6061,'Ruta','95','D',NULL,NULL),
	 ('2023-05-29 20:34:18.288','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',780,'Ruta','30','D',NULL,NULL),
	 ('2023-05-29 20:34:18.334','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',11,'Ruta','96','D',NULL,NULL),
	 ('2023-05-29 20:34:18.367','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',2525,'Ruta','31','D',NULL,NULL),
	 ('2023-05-29 20:34:18.414','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',1786,'Ruta','98','D',NULL,NULL);
INSERT INTO vlci2.t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-05-29 20:34:18.476','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',2988,'Ruta','10','D',NULL,NULL),
	 ('2023-05-29 20:34:18.561','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',4426,'Ruta','32','D',NULL,NULL),
	 ('2023-05-29 20:34:18.684','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',2982,'Ruta','11','D',NULL,NULL),
	 ('2023-05-29 20:34:18.733','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28',9023,'Ruta','99','D',NULL,NULL);


COMMIT;
