-- Revert postgis-vlci-vlci2:v_migrations/168.delete_t_d_fecha_negocio_etls from pg

BEGIN;

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id, etl_nombre, periodicidad, "offset", fen, fen_inicio, fen_fin, estado, formato_fen, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd) VALUES
    (160, 'VLCI_ETL_OCI_IND_REPORT_WIFI_DATOS_SESION_CLIENTE_WIFI', 'DIARIA', 0, '2022-02-01 00:00:00.000' , NULL, NULL, 'ERROR', 'YYYY-MM-dd', '2021-01-19 08:38:19.000', 'Alex 43390', '2022-02-03 19:13:22.822', 'p_gf_registrarEstadoETL');

COMMIT;
