-- Revert postgis-vlci-vlci2:v_migrations/1431-update-t-d-area from pg

BEGIN;

truncate table vlci2.t_d_area;

INSERT INTO vlci2.t_d_area (pk_area_id,desc_corta_cas,desc_corta_val,desc_larga_cas,desc_larga_val,estado_url,de_color,icono_url,activo_mask,nu_indicadores,nu_rojo,nu_ambar,nu_verde,nu_gris,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 ('1P','PLENO MUNICIPAL','PLÉ MUNICIPAL','PLENO MUNICIPAL','PLÉ MUNICIPAL','Images/VLCI/psincolorbl.png','AR_GRIS','Images/VLCI/ple-municipal.svg','S',0,0,0,0,0,'2021-09-14 12:19:48.157352','ETL BDO','2022-06-08 20:36:04.479594','ETL BDO'),
	 ('1M','ÁREA EDUCACIÓN','ÀREA EDUCACIÓ','ÁREA DE EDUCACIÓN, CULTURA Y DEPORTES','ÀREA D''EDUCACIÓ, CULTURA I ESPORTS','Images/VLCI/pambarbl.png','AR_AMBAR','Images/VLCI/esports.svg','S',10,5,0,4,1,'2021-09-14 12:19:49.14258','ETL BDO','2022-06-08 20:36:04.900899','ETL BDO'),
	 ('1L','ÁREA MOVILIDAD','ÀREA DE MOBILITAT','ÁREA DE MOVILIDAD SOSTENIBLE Y ESPACIO PÚBLICO','ÀREA DE MOBILITAT SOSTENIBLE I ESPAI PÚBLIC','Images/VLCI/pverdebl.png','AR_VERDE','Images/VLCI/mobilitat.svg','S',76,13,0,41,22,'2021-09-14 12:19:48.723046','ETL BDO','2022-06-08 20:36:03.669559','ETL BDO'),
	 ('1K','ÁREA BIENESTAR','ÀREA BENESTAR','ÁREA DE BIENESTAR Y DERECHOS SOCIALES','ÀREA DE BENESTAR I DRETS SOCIALS','Images/VLCI/projobl.png','AR_ROJO','Images/VLCI/desenvolupament-huma.svg','S',16,9,1,4,2,'2021-09-14 12:19:47.95615','ETL BDO','2022-06-08 20:36:04.1927','ETL BDO'),
	 ('1J','ÁREA DE PARTICIPACIÓN','ÀREA DE PARTICIPACIÓ','ÁREA DE PARTICIPACIÓN, DERECHOS E INNOVACIÓN DE LA DEMOCRACIA','ÀREA DE PARTICIPACIÓ, DRETS I INNOVACIÓ DE LA DEMOCRÀTICA','Images/VLCI/pambarbl.png','AR_AMBAR','Images/VLCI/participacio-ciutadana.svg','S',6,1,0,3,2,'2021-09-14 12:19:49.507203','ETL BDO','2022-06-08 20:36:03.988612','ETL BDO'),
	 ('1I','ÁREA DESARROLLO INNOVADOR','ÀREA DESENV. INNOVADOR','ÁREA DE DESARROLLO INNOVADOR DE LOS SECTORES ECONÓMICOS Y OCUPACIÓN','ÀREA DE DESENVOLUPAMENT INNOVADOR DELS SECTORS ECONÒMICS I OCUPACIÓ','Images/VLCI/pverdebl.png','AR_VERDE','Images/VLCI/desenvolupament-economic.svg','S',20,3,0,15,2,'2021-09-14 12:19:48.339272','ETL BDO','2022-06-08 20:36:04.679721','ETL BDO'),
	 ('1G','ÁREA DESARROLLO URBANO','ÀREA DE DESENVOLUPAMENT URBÀ','ÁREA DE DESARROLLO Y RENOVACIÓN URBANA Y VIVIENDA','ÀREA DE DESENVOLUPAMENT I RENOVACIÓ URBANA I HABITATGE','Images/VLCI/pambarbl.png','AR_AMBAR','Images/VLCI/vivenda.svg','S',32,4,1,15,12,'2021-09-14 12:19:48.912957','ETL BDO','2022-06-08 20:36:04.33709','ETL BDO'),
	 ('1F','ÁREA ECOLOGÍA URBANA','ÀREA ECOLOGIA URBANA','ÁREA DE ECOLOGÍA URBANA, EMERGENCIA CLIMÁTICA Y TRANSICIÓN ENERGÉTICA','ÀREA D''ECOLOGIA URBANA, EMERGÈNCIA CLIMÀTICA I TRANSICIÓ ENERGÈTICA','Images/VLCI/pambarbl.png','AR_AMBAR','Images/VLCI/clima.svg','S',106,14,0,16,76,'2021-09-14 12:19:49.90733','ETL BDO','2022-06-08 20:36:05.182656','ETL BDO'),
	 ('1D','ÁREA PROTECCIÓN CIUDADANA','ÀREA PROTECCIÓ CIUTADANA','ÁREA DE PROTECCIÓN CIUDADANA','ÀREA DE PROTECCIÓ CIUTADANA','Images/VLCI/pverdebl.png','AR_VERDE','Images/VLCI/proteccio-ciutadana.svg','S',8,1,1,6,0,'2021-09-14 12:19:50.099644','ETL BDO','2022-06-08 20:36:05.469776','ETL BDO'),
	 ('1C','ÁREA RECURSOS','ÀREA RECURSOS','ÁREA DE GESTIÓN DE RECURSOS','ÀREA DE GESTIÓ DE RECURSOS','Images/VLCI/pambarbl.png','AR_AMBAR','Images/VLCI/govern-interior.svg','S',93,16,1,33,43,'2021-09-14 12:19:49.316835','ETL BDO','2022-06-08 20:36:03.808224','ETL BDO');
INSERT INTO vlci2.t_d_area (pk_area_id,desc_corta_cas,desc_corta_val,desc_larga_cas,desc_larga_val,estado_url,de_color,icono_url,activo_mask,nu_indicadores,nu_rojo,nu_ambar,nu_verde,nu_gris,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 ('1A','ALCALDÍA','ALCALDIA','ALCALDÍA','ALCALDIA','Images/VLCI/pambarbl.png','AR_AMBAR','Images/VLCI/alcaldia.svg','S',43,10,0,19,14,'2021-09-14 12:19:48.531882','ETL BDO','2022-06-08 20:36:05.333761','ETL BDO');

COMMIT;
