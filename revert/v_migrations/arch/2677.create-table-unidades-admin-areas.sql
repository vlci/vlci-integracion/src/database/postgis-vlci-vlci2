-- Revert postgis-vlci-vlci2:v_migrations/2677.create-table-unidades-admin-areas from pg

BEGIN;

DROP TABLE vlci2.t_datos_etl_unidades_administrativas_areas;

CREATE TABLE vlci2.t_datos_etl_contratos_menores_bdo (
    unidad_administrativa varchar(255) PRIMARY KEY,
    unidad_cod_org_original varchar(255) NULL,
    unidad_cod_org_actual varchar(255) NULL,
    area_cod_org_original varchar(255) NULL,
    area_cod_org_actual varchar(255) NULL,
    area_desc_cas_original varchar(255) NULL,
    area_desc_cas_actual varchar(255) NULL,
    area_desc_val_original varchar(255) NULL,
    area_desc_val_actual varchar(255) NULL,
    area_desc_corta_cas_original varchar(255) NULL,
    area_desc_corta_cas_actual varchar(255) NULL,
    area_desc_corta_val_original varchar(255) NULL,
    area_desc_corta_val_actual varchar(255) NULL,
    aud_fec_ins timestamp NULL,
    aud_user_ins varchar(255) NULL,
    aud_fec_upd timestamp NULL,
    aud_user_upd varchar(255) NULL
);

create trigger t_datos_etl_contratos_menores_bdo_before_insert before
insert
    on
    vlci2.t_datos_etl_contratos_menores_bdo for each row execute function fn_aud_fec_ins();

create trigger t_datos_etl_contratos_menores_bdo_update before
update
    on
    vlci2.t_datos_etl_contratos_menores_bdo for each row execute function fn_aud_fec_upd();

COMMIT;
