-- Revert postgis-vlci-vlci2:v_migrations/804.nueva-columna-legacy-t-d-api-loader from pg
-- tarea PROY2100017N3-804

BEGIN;

ALTER TABLE vlci2.t_d_api_loader DROP CONSTRAINT check_value;
ALTER TABLE vlci2.t_d_api_loader DROP COLUMN legacy;

COMMIT;