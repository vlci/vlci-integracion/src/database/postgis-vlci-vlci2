-- Revert postgis-vlci-vlci2:v_migrations/1725-monitorizacion-parkings from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata
DROP COLUMN operationalstatus,
DROP COLUMN inactive;

COMMIT;
