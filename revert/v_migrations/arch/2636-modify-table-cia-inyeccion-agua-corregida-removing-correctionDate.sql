-- Revert postgis-vlci-vlci2:v_migrations/2636-modify-table-cia-inyeccion-agua-corregida-removing-correctionDate from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida ADD COLUMN correctiondate TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida RENAME COLUMN kpiValueOriginal TO correctedkpivalue;

COMMIT;
