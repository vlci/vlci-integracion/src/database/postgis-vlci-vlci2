-- Revert postgis-vlci-vlci2:v_migrations/1535-agregar-campo-areaServed from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_sonometros_hopvlci_daily DROP COLUMN areaServed;

COMMIT;
