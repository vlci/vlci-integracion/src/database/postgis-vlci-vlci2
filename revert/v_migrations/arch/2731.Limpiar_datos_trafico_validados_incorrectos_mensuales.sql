-- Revert postgis-vlci-vlci2:v_migrations/2731.Limpiar_datos_trafico_validados_incorrectos_mensuales from pg

BEGIN;

delete from vlci2.t_datos_cb_trafico_kpi_validados_mes 
where calculationperiod >= '2024-02-24' and calculationperiod <= '2024-04-27' ;

COMMIT;
