-- Revert postgis-vlci-vlci2:v_migrations/1892-fix-last-data-format from pg

BEGIN;

DROP TABLE vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata;

CREATE TABLE vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata (
	recvtime timestamptz NULL,
	fiwareservicepath varchar(100) NULL,
	entityid varchar(100) NOT NULL,
	entitytype varchar(100) NULL,
	description varchar(200) NULL,
	descriptioncas varchar(200) NULL,
	dateobserved varchar(200) NULL,
	dateobservedfrom varchar(200) NULL,
	dateobservedto varchar(200) NULL,
	intensity int4 NULL,
	"location" public.geometry NULL,
	"name" varchar(200) NULL,
	operationalstatus varchar(200) NULL,
	locationforgisvisualization public.geometry NULL,
	lowerthresholdrt numeric NULL,
	middlethresholdrt numeric NULL,
	upperthresholdrt numeric NULL,
	CONSTRAINT t_datos_cb_trafico_cuenta_bicis_lastdata_pkey PRIMARY KEY (entityid)
);

COMMIT;
