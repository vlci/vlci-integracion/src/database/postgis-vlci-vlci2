-- Revert postgis-vlci-vlci2:v_migrations/1930-ortografia-quejas-sugerencias-mapeo from pg

BEGIN;

UPDATE vlci2.t_ref_etl_quejas_sugerencias_mapeo_temas
	SET tema_nuevo='Tramitacion administrativa, tributos y sanciones'
	WHERE tema_nuevo='Tramitación administrativa, tributos y sanciones';

UPDATE vlci2.t_ref_etl_quejas_sugerencias_mapeo_temas
	SET tema_nuevo='Via publica reparacion de deficiencias'
	WHERE tema_nuevo='Vía pública reparación de deficiencias';

COMMIT;
