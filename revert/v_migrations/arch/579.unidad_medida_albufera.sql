-- Revert postgis-vlci-vlci2:v_migrations/579.unidad_medida_albufera from pg

BEGIN;

DELETE FROM vlci2.t_m_unidad_medida
WHERE dc_unidad_medida_cas = 'cm';

COMMIT;
