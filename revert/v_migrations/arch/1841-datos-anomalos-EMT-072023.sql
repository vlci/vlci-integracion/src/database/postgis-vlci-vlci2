-- Revert postgis-vlci-vlci2:v_migrations/1841-datos-anomalos-EMT-072023 from pg

BEGIN;

INSERT INTO t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-07-29 20:34:05.346','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28',2,'Titulo','Abono Especial','V','Viajes','Viatges'),
	 ('2023-07-29 20:34:07.263','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28',1,'Titulo','Billete Ordinario','V','Viajes','Viatges'),
	 ('2023-07-29 20:34:06.607','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28',4,'Titulo','Pase Empleado EMT','V','Viajes','Viatges'),
	 ('2023-07-29 20:34:06.936','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28',3,'Titulo','SUMA 10','V','Viajes','Viatges'),
	 ('2023-07-29 20:34:07.8','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28',1,'Titulo','SUMA Mensual','V','Viajes','Viatges'),
	 ('2023-07-29 20:34:06.878','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28',2,'Titulo','SUMA T-2+','V','Viajes','Viatges'),
	 ('2023-07-29 20:34:07.33','/emt','KeyPerformanceIndicator','mob006-04','2023-07-28',13,'Ruta','1','V',NULL,NULL),
	 ('2023-07-30 20:34:06.587','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',78,'Titulo','Abono Especial','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:06.992','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',24,'Titulo','Amb Tu','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:08.212','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',1,'Titulo','BB Personalizado Especial','S','Viajes','Viatges');
INSERT INTO t_datos_cb_emt_kpi (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,kpivalue,sliceanddice1,sliceanddicevalue1,diasemana,measureunitcas,measureunitval) VALUES
	 ('2023-07-30 20:34:06.729','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',4,'Titulo','BB Personalizado General','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:09.59','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',9,'Titulo','Billete Ordinario','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:09.11','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',52,'Titulo','Bonobús','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:09.114','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',6,'Titulo','Bono Infantil','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:08.732','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',107,'Titulo','BonoOro','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:04.915','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',1,'Titulo','Familiar Empleado EMT','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:06.654','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',1,'Titulo','Pase Empleado EMT','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:08.148','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',59,'Titulo','SUMA 10','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:08.464','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29',2,'Titulo','SUMA Mensual','S','Viajes','Viatges'),
	 ('2023-07-30 20:34:09.233','/emt','KeyPerformanceIndicator','mob006-04','2023-07-29',94,'Ruta','40','S',NULL,NULL),
	 ('2023-07-30 20:34:09.177','/emt','KeyPerformanceIndicator','mob006-04','2023-07-29',250,'Ruta','70','S',NULL,NULL);

COMMIT;
