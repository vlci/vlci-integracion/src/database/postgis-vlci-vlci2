-- Revert postgis-vlci-vlci2:2475-ocoval-nuevas-entidades-junta-marítim-diputacíon from pg

BEGIN;

DELETE FROM vlci2.vlci_ocoval_entidades WHERE compania = 'Otros' and entidad = 'Junta Municipal Marítim';
DELETE FROM vlci2.vlci_ocoval_entidades WHERE compania = 'Otros' and entidad = 'Diputación de Valencia';

COMMIT;
