-- Revert postgis-vlci-vlci2:v_migrations/1909.delete_15_agosto_xativa from pg

BEGIN;

INSERT INTO vlci2.t_f_text_kpi_trafico_y_bicis (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,diasemana,kpivalue,tramo,"name",measureunitcas,measureunitval) VALUES
	 ('2023-08-16 05:14:06.177+02','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2023-08-15','M','8197','Xàtiva','Xàtiva','Bicicletas','Bicicletes');


COMMIT;
