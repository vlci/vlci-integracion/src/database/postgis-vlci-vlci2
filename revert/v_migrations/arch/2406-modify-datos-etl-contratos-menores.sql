-- Revert postgis-vlci-vlci2:v_migrations/2406-modify-datos-etl-contratos-menores from pg

BEGIN;

ALTER TABLE vlci2.t_datos_etl_contratos_menores 
DROP CONSTRAINT IF EXISTS t_datos_etl_contratos_menores_pkey;

ALTER TABLE vlci2.t_datos_etl_contratos_menores DROP COLUMN area_cod_org;
ALTER TABLE vlci2.t_datos_etl_contratos_menores DROP COLUMN area_desc_val;
ALTER TABLE vlci2.t_datos_etl_contratos_menores DROP COLUMN area_desc_corta_cas;
ALTER TABLE vlci2.t_datos_etl_contratos_menores DROP COLUMN area_desc_corta_val;
ALTER TABLE vlci2.t_datos_etl_contratos_menores DROP COLUMN servicio_cod_org;

--Aquí se puede volver a crear la columna pero los datos se van a perder
ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD COLUMN nif_adjudicatario varchar(50);

ALTER TABLE vlci2.t_datos_etl_contratos_menores ALTER column centro_gastos set not null;
ALTER TABLE vlci2.t_datos_etl_contratos_menores ALTER column aplicacion_presupuestaria set not null;

DROP TRIGGER t_datos_etl_contratos_menores_before_insert ON vlci2.t_datos_etl_contratos_menores;
DROP TRIGGER t_datos_etl_contratos_menores_before_update ON vlci2.t_datos_etl_contratos_menores;

INSERT INTO vlci2.t_datos_etl_contratos_menores (fecha_carga, num_expediente, objeto_contrato, fecha_adjudicacion, centro_gastos, unidad_administrativa, aplicacion_presupuestaria, area, tipo_contrato, importe_sin_iva, iva, organo_contratacion, unidad_tramitadora, numero_propuestas, numero_ofertas, adjudicatario, estado, fecha_inicio_contrato, fecha_fin_contrato, duracion_contrato_meses, fecha_factura, fecha_pago, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd, nif_adjudicatario) VALUES('2023-09-20', 'E8RE05202300000900', 'ADQUISICIÓN LANYARDS PARA CREDENCIALES MASCLETÀ FALLAS 2023', '2023-01-25', 'A0020', '8RE05', 'G2023A00209120022199', 'ALCALDÍA', 'SUMINISTROS', 1368.00, 287.28, 'JUNTA DE GOBIERNO LOCAL', '8RE05 - PROTOCOLO ALCALDIA', 2, 1, 'ALBERTO BASTERRA RODRIGO', 'EJECUTADO', '2023-01-26', '2023-02-10', 0, '2023-01-27', '2023-02-08', '2023-09-20 07:43:46.238', 'xavi-1983', '2023-09-20 07:43:46.238', 'xavi-1983', '*****937W');
INSERT INTO vlci2.t_datos_etl_contratos_menores (fecha_carga, num_expediente, objeto_contrato, fecha_adjudicacion, centro_gastos, unidad_administrativa, aplicacion_presupuestaria, area, tipo_contrato, importe_sin_iva, iva, organo_contratacion, unidad_tramitadora, numero_propuestas, numero_ofertas, adjudicatario, estado, fecha_inicio_contrato, fecha_fin_contrato, duracion_contrato_meses, fecha_factura, fecha_pago, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd, nif_adjudicatario) VALUES('2023-09-20', 'E02310202300002500', 'ORGANITZACIÓ ESPORTIVA EXHIBICIÓ REGATA DE VELA LLATINA EL PALMAR', '2023-01-25', '057', '02310', 'G2023IE9709240022609', 'HACIENDA Y PARTICIPACIÓN', 'SERVICIOS', 400.00, 0.00, 'JUNTA DE GOBIERNO LOCAL', '02310 - SERVICI DE POBLES DE VALÈNCIA', 1, 1, 'CLUB DE VELA LATINA VALENCIANA', 'EJECUTADO', '2023-03-25', '2023-03-25', 0, '2023-04-05', '2023-04-14', '2023-09-20 07:43:46.238', 'xavi-1983', '2023-09-20 07:43:46.238', 'xavi-1983', 'G97931877');


COMMIT;
