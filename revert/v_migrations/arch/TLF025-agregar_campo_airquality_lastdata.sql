-- Revert postgis-vlci-vlci2:v_migrations/TLF025-agregar_campo_airquality_lastdata from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_airqualityobserved_lastdata drop column inactive;

COMMIT;
