-- Revert postgis-vlci-vlci2:v_migrations/1541-modificacion-columna-nombre from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata RENAME COLUMN "name" TO nombre;

COMMIT;
