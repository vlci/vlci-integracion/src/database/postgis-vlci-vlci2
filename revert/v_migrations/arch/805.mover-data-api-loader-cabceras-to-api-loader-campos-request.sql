-- Revert postgis-vlci-vlci2:v_migrations/805.mover-data-api-loader-cabceras-to-api-loader-campos-request from pg

BEGIN;

    INSERT INTO vlci2.t_d_api_loader_cabeceras (nombre_api, cabecera, valor) (
        SELECT nombre_api, campo, valor FROM vlci2.t_d_api_loader_campos_request
    );
    TRUNCATE vlci2.t_d_api_loader_campos_request;

COMMIT;
