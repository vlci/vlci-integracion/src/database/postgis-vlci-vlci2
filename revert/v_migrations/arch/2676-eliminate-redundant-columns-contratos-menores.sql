-- Revert postgis-vlci-vlci2:v_migrations/2676-eliminate-redundant-columns-contratos-menores from pg

BEGIN;

ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD area varchar;
ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD area_cod_org varchar;
ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD area_desc_val varchar;
ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD area_desc_corta_cas varchar;
ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD area_desc_corta_val varchar;
ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD servicio_cod_org varchar;

COMMIT;
