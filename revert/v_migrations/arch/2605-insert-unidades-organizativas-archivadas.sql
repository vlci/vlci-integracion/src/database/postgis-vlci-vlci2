-- Revert postgis-vlci-vlci2:v_migrations/2605-insert-unidades-organizativas-archivadas from pg

BEGIN;

delete from vlci2.t_d_area where aud_user_ins = '2605 - JULIAN';
delete from vlci2.t_d_delegacion where aud_user_ins = '2605 - JULIAN';
delete from vlci2.t_d_servicio where aud_user_ins = '2605 - JULIAN';

COMMIT;
