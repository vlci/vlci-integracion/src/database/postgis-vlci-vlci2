-- Revert postgis-vlci-vlci2:v_migrations/1829-insert-datos-tabla-visualizacion from pg

BEGIN;

DELETE from vlci2.t_ref_horizontal_visualizacion WHERE vertical in ('CUENTA_BICIS','PARKINGS','PARKINGS_TENDENCIA','PARKINGS_PREDICCION');

COMMIT;
