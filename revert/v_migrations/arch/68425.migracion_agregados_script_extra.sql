-- Revert postgis-vlci-vlci2:v_migrations/68425.migracion_agregados_script_extra from pg

BEGIN;

ALTER TABLE t_f_raw_from_context_broker ALTER COLUMN description TYPE VARCHAR(255);
ALTER TABLE t_f_raw_from_context_broker ALTER COLUMN name TYPE VARCHAR(255);

COMMIT;
