-- Revert postgis-vlci-vlci2:v_migrations/1884-add-users from pg

BEGIN;

DELETE FROM vlci2.t_d_user_rol where pk_user in ('U18761', 'U301362');
DELETE FROM vlci2.t_d_usuario_servicio where pk_user_id in ('U18761', 'U301362');

COMMIT;
