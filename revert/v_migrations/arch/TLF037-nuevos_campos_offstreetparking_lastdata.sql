-- Revert postgis-vlci-vlci2:v_migrations/TLF037-nuevos_campos_offstreetparking_lastdata from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata
    DROP COLUMN address,
    DROP COLUMN project,
    DROP COLUMN owner,
    DROP COLUMN owneremail,
    DROP COLUMN respocitecnicoemail,
    DROP COLUMN timeinstant,
    DROP COLUMN location;


COMMIT;
