-- Revert postgis-vlci-vlci2:v_migrations/1541-eliminacion-datos-aparcamiento-lastdata from pg

BEGIN;

INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, "name", totalspotnumber, availablespotnumber)
VALUES('parking_120', 'CENTRE HISTÒRIC-MERCAT CENTRAL', 431, 100);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, "name", totalspotnumber, availablespotnumber)
VALUES('parking_13', 'SANTA MARÍA MICAELA', 229, 200);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, "name", totalspotnumber, availablespotnumber)
VALUES('parking_5', 'PLAÇA DE LA REINA', 234, 9);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, "name", totalspotnumber, availablespotnumber)
VALUES('parking_50', 'PARKING LA FE S.L.', 377, 100);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, "name", totalspotnumber, availablespotnumber)
VALUES('parking_6', 'GLORIETA - PAZ', 372, 120);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, "name", totalspotnumber, availablespotnumber)
VALUES('parking_7', 'PORTA DE LA MAR - COLON', 201, 111);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, "name", totalspotnumber, availablespotnumber)
VALUES('parking_75', 'AVINGUDA DE L’OEST', 270, 200);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, "name", totalspotnumber, availablespotnumber)
VALUES('parking_79', 'PROFESOR TIERNO GALVÁN', 375, 10);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, "name", totalspotnumber, availablespotnumber)
VALUES('parking_8', 'REGNE', 192, 180);


COMMIT;
