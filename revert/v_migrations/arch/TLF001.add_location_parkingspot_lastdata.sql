-- Revert postgis-vlci-vlci2:v_migrations/TLF001.add_location_parkingspot_lastdata from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_parkingspot_lastdata DROP COLUMN "location";

COMMIT;
