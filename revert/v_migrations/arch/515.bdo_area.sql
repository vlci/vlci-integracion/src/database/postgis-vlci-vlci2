-- Revert postgis-vlci-vlci2:v_migrations/515.bdo_area from pg

BEGIN;

INSERT INTO vlci2.t_datos_etl_agregados_area
(pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, icono_url, activo_mask, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES('210', 'DELEGACIÓN PREV INCENDIOS', 'DELEGACIÓ PREV INCENDIS', 'DELEGACIÓN DE PREVENCIÓN Y EXTINCIÓN DE INCENDIOS', 'DELEGACIÓ DE PREVENCIÓ I EXTINCIÓ D''INCENDIS', NULL, 'S', '2022-05-30 14:30:42.650', 'ETL BDO', NULL, 'ETL BDO');

INSERT INTO vlci2.t_datos_etl_agregados_delegacion
(pk_delegacion_id, pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, activo_mask, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES('1J', '1A', 'ÁREA DE PARTICIPACIÓN', 'ÀREA DE PARTICIPACIÓ', 'ÁREA DE PARTICIPACIÓN, DERECHOS E INNOVACIÓN DE LA DEMOCRACIA', 'ÀREA DE PARTICIPACIÓ, DRETS I INNOVACIÓ DE LA DEMOCRÀTICA', 'S', '2021-09-14 12:19:42.089', 'ETL BDO', '2022-06-08 20:36:02.428', 'ETL BDO');
INSERT INTO vlci2.t_datos_etl_agregados_delegacion
(pk_delegacion_id, pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, activo_mask, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES('667', '210', 'DPT. BOMBEROS,PREV.E INTERV.EMERG.Y P.C.', 'DPT. BOMBERS,PREV.,INTERV.EMERG.I P.C.', 'DEPARTAMENTO DE BOMBEROS, PREVENCIÓN, INTERVENCIÓN EMERGENCIAS Y PROTECCIÓN CIVIL', 'DEPARTAMENT DE BOMBERS, PREVENCIÓ, INTERVENCIÓ EMERGÈNCIES I PROTECCIÓ CIVIL', 'S', '2021-09-14 12:19:31.494', 'ETL BDO', '2022-06-08 20:35:55.732', 'ETL BDO');

INSERT INTO vlci2.t_datos_etl_agregados_servicio
(pk_servicio_id, fk_delegacion_id, codigo_organico, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, activo_mask, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES('61B', '667', 'DD1B0', 'UNIDAD DE INTERVENCIÓN DE BOMBEROS', 'UNITAT D''INTERVENCIÓ DE BOMBERS', 'UNIDAD DE INTERVENCIÓN DE BOMBEROS', 'UNITAT D''INTERVENCIÓ DE BOMBERS', 'S', '2021-09-14 12:18:05.702', 'ETL BDO', '2022-06-08 20:34:24.443', 'ETL BDO');
INSERT INTO vlci2.t_datos_etl_agregados_servicio
(pk_servicio_id, fk_delegacion_id, codigo_organico, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, activo_mask, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES('61C', '667', 'DD1C0', 'UNIDAD DE LOGÍSTICA DE BOMBEROS', 'UNITAT DE LOGÍSTICA DE BOMBERS', 'UNIDAD DE LOGÍSTICA DE BOMBEROS', 'UNITAT DE LOGÍSTICA DE BOMBERS', 'S', '2021-09-14 12:19:07.030', 'ETL BDO', '2022-06-08 20:35:43.332', 'ETL BDO');
INSERT INTO vlci2.t_datos_etl_agregados_servicio
(pk_servicio_id, fk_delegacion_id, codigo_organico, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, activo_mask, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES('61A', '667', 'DD1A0', 'UNIDAD DE PREVENCION Y PROTECCION CIVIL', 'UNITAT DE PREVENCIO I PROTECCIO CIVIL', 'UNIDAD DE PREVENCION Y PROTECCION CIVIL', 'UNITAT DE PREVENCIO I PROTECCIO CIVIL', 'S', '2021-09-14 12:18:46.986', 'ETL BDO', '2022-06-08 20:35:37.118', 'ETL BDO');


DELETE FROM vlci2.t_datos_etl_agregados_delegacion WHERE pk_delegacion_id = '999';

update vlci2.t_datos_etl_agregados_servicio 
set fk_delegacion_id = '1J'
where fk_delegacion_id = '999';

COMMIT;
