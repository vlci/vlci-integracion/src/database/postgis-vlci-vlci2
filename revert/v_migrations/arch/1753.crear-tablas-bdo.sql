-- Revert postgis-vlci-vlci2:v_migrations/1753.crear-tablas-bdo from pg

BEGIN;

drop table vlci2.t_d_servicio;
drop table vlci2.t_d_delegacion;
drop table vlci2.t_d_area;

COMMIT;
