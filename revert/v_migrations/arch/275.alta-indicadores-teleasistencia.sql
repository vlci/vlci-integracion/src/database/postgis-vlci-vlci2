-- Revert postgis-vlci-vlci2:v_migrations/275.alta-indicadores-teleasistencia from pg

BEGIN;

DELETE FROM vlci2.t_d_indicador
WHERE dc_identificador_indicador IN 
        ('IS.BSO.01', 'IS.BSO.02', 'IS.BSO.03', 'IS.BSO.04', 'IS.BSO.05', 'IS.BSO.06', 'IS.BSO.07','IS.BSO.08', 'IS.BSO.09', 'IS.BSO.10', 'IS.BSO.11', 'IS.BSO.12');

COMMIT;
