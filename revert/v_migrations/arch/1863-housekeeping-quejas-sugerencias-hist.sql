-- Revert postgis-vlci-vlci2:v_migrations/1863-housekeeping-quejas-sugerencias-hist from pg

BEGIN;

DELETE FROM vlci2.housekeeping_config
	WHERE table_nam='t_datos_etl_quejas_sugerencias_hist';
    
COMMIT;
