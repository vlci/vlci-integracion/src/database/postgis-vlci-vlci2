-- Revert postgis-vlci-vlci2:v_migrations/1720.sustituir-linea-199 from pg

BEGIN;

update vlci2.t_datos_cb_emt_kpi set sliceanddicevalue1  = '199' 
where sliceanddicevalue1  = '99'
and calculationperiod in ('2018-01-02','2018-01-03','2018-01-04','2018-01-05');

COMMIT;
