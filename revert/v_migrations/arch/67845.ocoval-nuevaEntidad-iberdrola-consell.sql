-- Revert vlci-postgis:67845.ocoval-nuevaEntidad-iberdrola-consell from pg

BEGIN;

delete from vlci2.vlci_ocoval_entidades where compania='Compañías de Servicios' and entidad='Iberdrola Telecomunicaciones';
delete from vlci2.vlci_ocoval_entidades where compania='Otros' and entidad='Consell Agrari';
delete from vlci2.vlci_ocoval_companyia where compania='IBERDROLA' and entidad='Iberdrola Telecomunicaciones';

COMMIT;
