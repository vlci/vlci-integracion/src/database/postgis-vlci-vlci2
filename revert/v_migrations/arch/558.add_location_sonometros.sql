-- Revert postgis-vlci-vlci2:v_migrations/558.add_location_sonometros from pg

BEGIN;

ALTER TABLE vlci2.t_f_text_cb_sonometros_ruzafa_daily DROP COLUMN location;

COMMIT;
