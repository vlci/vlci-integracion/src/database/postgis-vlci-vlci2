-- Revert postgis-vlci-vlci2:v_migrations/197.establecer_pk_tablas_cdmciutat from pg

BEGIN;

-- EMT
ALTER TABLE vlci2.t_datos_cb_emt_kpi
DROP CONSTRAINT t_datos_cb_emt_kpi_pkey;
ALTER TABLE vlci2.t_datos_cb_emt_kpi
ADD PRIMARY KEY(recvtime,fiwareservicepath,entitytype,entityid);

-- trafico y bicis
ALTER TABLE vlci2.t_f_text_kpi_trafico_y_bicis
DROP CONSTRAINT t_f_text_kpi_trafico_y_bicis_pkey;

ALTER TABLE vlci2.t_datos_cb_trafico_pm_diaria
DROP CONSTRAINT t_datos_cb_trafico_pm_diaria_pkey;

INSERT INTO vlci2.t_f_text_kpi_trafico_y_bicis (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,diasemana,kpivalue,tramo,"name",measureunitcas,measureunitval) VALUES
	 ('2021-07-07 17:14:30.773+02','/trafico','Kpi.pruebamig','KeyPerformanceIndicator','2021-07-06','M','98176','Prolongació Joan XXIII [Entre Germans Machado i Salvador Cerveró]','Prolongació Joan XXIII',NULL,NULL),
	 ('2021-07-07 17:15:43.866+02','/trafico','Kpi.pruebamig','KeyPerformanceIndicator','2021-07-06','M','98177','Prolongació Joan XXIII [Entre Germans Machado i Salvador Cerveró]','Prolongació Joan XXIII',NULL,NULL),
	 ('2021-09-16 08:41:56.297+02','/trafico','Kpi.pruebamig','KeyPerformanceIndicator','2021-07-06','M','98177','Prolongació Joan XXIII [Entre Germans Machado i Salvador Cerveró]','Prolongació Joan XXIII',NULL,NULL);


-- Agua
ALTER TABLE vlci2.t_f_text_cb_kpi_consumo_agua
DROP CONSTRAINT t_f_text_cb_kpi_consumo_agua_pkey;

INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-18 13:58:52.963+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-18 13:58:54.374+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-18 13:58:54.602+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-18 13:58:54.636+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-18 13:58:54.677+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-18 13:58:54.737+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-18 13:58:55.1+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-18 13:58:55.33+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-18 13:58:59.891+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-18 13:59:00.216+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-18 13:59:00.244+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-18 13:59:00.28+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-18 13:59:00.504+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-18 13:59:00.755+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-18 13:59:00.784+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-18 13:59:00.812+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-18 13:59:00.84+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-18 13:59:00.88+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-18 13:59:13.75+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-18 13:59:13.781+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-18 13:59:14.237+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-18 13:59:14.264+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-18 13:59:14.293+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-18 13:59:14.326+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-18 13:59:14.355+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-18 13:59:14.394+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-18 13:59:17.945+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-18 13:59:18.44+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-18 13:59:18.491+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-18 13:59:18.724+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-18 13:59:18.978+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-13','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-18 13:59:19.6+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-18 13:59:19.32+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-18 13:59:19.62+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-18 13:59:19.298+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-18 13:59:19.528+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-14','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-19 10:58:35.389+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-19 10:58:35.831+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-19 10:58:35.964+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-19 10:58:36.139+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-19 10:58:36.369+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-19 10:58:36.4+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-19 10:58:36.64+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-19 10:58:36.874+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-19 10:58:36.9+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-19 10:58:36.938+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-19 10:58:36.966+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-19 10:58:36.996+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-19 10:58:37.34+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-19 10:58:37.285+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-19 10:58:37.337+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-19 10:58:37.38+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-19 10:58:37.426+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-19 10:58:37.464+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-15','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-20 10:01:32.744+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-20 10:01:32.782+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-20 10:01:32.812+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-20 10:01:32.843+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-20 10:01:33.62+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-20 10:01:33.92+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-20 10:01:33.12+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-20 10:01:33.348+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-20 10:01:33.373+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-20 10:01:33.597+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-20 10:01:33.646+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-20 10:01:33.671+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-20 10:01:33.685+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-20 10:01:34.125+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-20 10:01:34.361+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-20 10:01:34.582+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-20 10:01:34.607+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-20 10:01:34.826+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-16','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-25 14:14:48.29+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-25 14:14:48.6+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-25 14:14:48.305+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-25 14:14:48.388+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-25 14:14:48.669+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-25 14:14:48.966+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-25 14:14:49.3+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-25 14:14:49.77+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-25 14:14:49.118+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-25 14:14:49.142+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-25 14:14:49.163+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-25 14:14:49.185+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-25 14:14:49.209+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-25 14:14:49.43+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-25 14:14:49.449+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-25 14:14:49.468+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-25 14:14:49.488+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-25 14:14:49.507+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-25 14:14:49.527+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-25 14:14:49.547+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-25 14:14:49.567+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-25 14:14:49.585+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-25 14:14:49.609+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-25 14:14:49.828+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-25 14:14:49.848+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-25 14:14:49.869+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-25 14:14:49.893+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-25 14:14:49.912+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-25 14:14:49.94+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-25 14:14:50.155+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-25 14:14:50.18+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-25 14:14:50.213+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-25 14:14:50.233+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-25 14:14:50.254+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-25 14:14:50.276+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-25 14:14:50.299+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-25 14:14:50.32+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-25 14:14:50.343+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-25 14:14:50.364+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-25 14:14:50.387+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-25 14:14:50.408+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-25 14:14:50.427+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-25 14:14:50.447+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-25 14:14:50.671+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-25 14:14:50.694+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-25 14:14:50.714+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-25 14:14:50.734+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-25 14:14:50.753+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-25 14:14:50.775+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-25 14:14:50.796+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-25 14:14:50.814+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-25 14:14:50.835+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-17','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-25 14:14:50.856+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-25 14:14:50.875+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-25 14:14:50.896+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-25 14:14:50.921+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-25 14:14:50.944+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-25 14:14:50.969+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-25 14:14:50.993+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-25 14:14:51.16+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-25 14:14:51.39+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-25 14:14:51.59+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-25 14:14:51.78+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-18','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-25 14:14:51.302+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-25 14:14:51.324+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-25 14:14:51.345+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-25 14:14:51.366+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-25 14:14:51.387+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-25 14:14:51.41+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-25 14:14:51.429+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-25 14:14:51.452+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-25 14:14:51.673+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-19','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-25 14:14:51.698+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-25 14:14:51.915+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-25 14:14:51.937+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-25 14:14:51.96+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-25 14:14:51.986+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-25 14:14:52.11+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-25 14:14:52.28+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-25 14:14:52.53+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-25 14:14:52.74+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-25 14:14:52.97+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-25 14:14:52.121+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-25 14:14:52.143+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-25 14:14:52.167+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-25 14:14:52.188+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-20','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-25 14:14:52.211+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-25 14:14:52.23+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-25 14:14:52.25+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-25 14:14:52.275+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-21','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-26 12:41:13.86+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-26 12:41:13.887+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-26 12:41:13.917+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-26 12:41:13.938+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-26 12:41:13.959+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-26 12:41:13.987+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-26 12:41:14.4+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-26 12:41:14.32+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-26 12:41:14.55+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-26 12:41:14.81+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-26 12:41:14.99+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-26 12:41:14.13+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-26 12:41:14.344+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-26 12:41:14.371+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-26 12:41:14.386+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-26 12:41:14.405+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-26 12:41:14.428+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-26 12:41:14.449+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-22','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-27 10:19:48.537+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-27 10:19:48.569+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-27 10:19:48.791+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-27 10:19:48.81+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-27 10:19:48.829+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-27 10:19:48.85+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-27 10:19:48.869+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-27 10:19:48.888+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-27 10:19:48.905+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-27 10:19:48.927+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-27 10:19:48.944+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-27 10:19:48.963+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-27 10:19:49.64+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-27 10:19:49.82+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-27 10:19:49.301+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-27 10:19:49.322+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-27 10:19:49.544+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-27 10:19:49.565+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-23','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-28 10:20:55.88+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-28 10:20:55.912+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-28 10:20:56.163+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-28 10:20:56.214+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-28 10:20:56.243+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-28 10:20:56.295+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-28 10:20:56.337+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-28 10:20:56.386+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-28 10:20:56.429+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-28 10:20:56.465+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-28 10:20:56.497+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-28 10:20:56.534+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-28 10:20:56.565+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-28 10:20:56.601+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-28 10:20:56.638+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-28 10:20:56.687+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-28 10:20:56.728+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-28 10:20:56.759+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-24','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-29 10:17:32.883+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-29 10:17:32.914+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-29 10:17:32.936+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-29 10:17:32.954+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-29 10:17:32.977+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-04-29 10:17:33.199+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-29 10:17:33.219+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-29 10:17:33.439+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-29 10:17:33.466+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-29 10:17:33.491+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-29 10:17:33.512+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-29 10:17:33.731+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-04-29 10:17:33.749+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-29 10:17:33.772+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-29 10:17:33.992+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-29 10:17:34.12+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-29 10:17:34.32+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-29 10:17:34.53+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-25','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-04-30 10:17:09.976+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-04-30 10:17:10.338+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-04-30 10:17:10.338+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-04-30 10:17:10.338+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-04-30 10:17:10.341+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-04-30 10:17:10.341+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-30 10:17:10.341+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-04-30 10:17:10.532+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-04-30 10:17:10.554+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-04-30 10:17:10.579+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-04-30 10:17:10.605+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-04-30 10:17:10.826+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-04-30 10:17:10.84+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-04-30 10:17:10.858+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-04-30 10:17:10.877+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-04-30 10:17:10.897+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-04-30 10:17:11.116+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-04-30 10:17:11.335+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-26','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-01 10:17:41.586+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-01 10:17:41.605+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-05-01 10:17:41.627+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-05-01 10:17:41.651+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-01 10:17:41.675+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-05-01 10:17:41.699+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-01 10:17:41.912+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-01 10:17:41.935+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-01 10:17:41.955+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-05-01 10:17:41.977+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-01 10:17:42.11+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-05-01 10:17:42.237+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-05-01 10:17:42.461+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-05-01 10:17:42.48+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-01 10:17:42.502+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-01 10:17:42.721+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-01 10:17:42.746+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-01 10:17:42.772+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-27','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-02 10:17:12.82+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-02 10:17:12.85+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-02 10:17:12.876+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-02 10:17:13.94+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-05-02 10:17:13.117+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-02 10:17:13.138+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-02 10:17:13.838+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-05-02 10:17:13.859+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-02 10:17:13.878+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-02 10:17:14.97+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-02 10:17:14.114+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-05-02 10:17:14.132+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-05-02 10:17:14.15+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-02 10:17:14.178+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-05-02 10:17:14.398+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-05-02 10:17:14.418+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-05-02 10:17:14.436+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-05-02 10:17:14.455+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-28','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-03 10:17:20.96+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-05-03 10:17:21.8+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-03 10:17:21.35+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-03 10:17:21.66+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-05-03 10:17:21.83+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-03 10:17:21.109+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-03 10:17:21.134+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-05-03 10:17:21.161+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-05-03 10:17:21.183+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-03 10:17:21.212+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-03 10:17:21.432+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-05-03 10:17:21.453+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-03 10:17:21.673+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-05-03 10:17:21.693+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-03 10:17:21.72+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-03 10:17:21.746+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-03 10:17:21.976+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-03 10:17:22.217+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-29','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-05-04 10:17:19.932+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-04 10:17:19.962+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-04 10:17:19.982+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-04 10:17:20.8+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-04 10:17:20.239+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-04 10:17:20.267+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-05-04 10:17:20.287+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-04 10:17:20.318+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-05-04 10:17:20.338+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-05-04 10:17:20.366+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-05-04 10:17:20.392+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-04 10:17:20.616+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-04 10:17:20.636+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-05-04 10:17:20.657+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-04 10:17:20.881+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-05-04 10:17:21.106+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-05-04 10:17:21.132+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-04 10:17:21.154+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-04-30','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-05 10:17:16.234+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-05 10:17:16.458+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-05 10:17:16.478+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-05 10:17:16.498+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-05-05 10:17:16.719+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-05 10:17:16.74+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-05 10:17:16.757+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-05 10:17:16.778+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-05-05 10:17:16.798+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-05-05 10:17:16.821+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-05-05 10:17:16.842+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-05 10:17:16.863+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-05 10:17:16.887+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-05-05 10:17:16.914+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-05-05 10:17:16.942+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-05-05 10:17:16.966+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-05 10:17:16.986+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-05 10:17:17.9+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-05-01','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-06 10:17:12.452+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-05-06 10:17:12.477+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-05-06 10:17:12.502+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-06 10:17:12.726+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-06 10:17:12.744+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-06 10:17:12.768+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-05-06 10:17:12.787+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-05-06 10:17:13.16+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-06 10:17:13.44+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-06 10:17:13.265+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-06 10:17:13.288+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-05-06 10:17:13.307+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-05-06 10:17:13.334+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-05-06 10:17:13.556+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-06 10:17:13.576+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-06 10:17:13.596+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-06 10:17:13.82+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-05-06 10:17:14.39+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-05-02','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-07 10:17:05.984+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-07 10:17:06.13+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-07 10:17:06.336+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-07 10:17:06.373+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-05-07 10:17:06.404+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-07 10:17:06.426+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-05-07 10:17:06.453+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-07 10:17:06.473+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-05-07 10:17:06.496+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-05-07 10:17:06.519+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-07 10:17:06.546+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-07 10:17:06.77+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-07 10:17:06.803+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-05-07 10:17:06.822+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-05-07 10:17:06.846+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-05-07 10:17:07.76+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-05-07 10:17:07.302+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-07 10:17:07.523+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-05-03','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-08 10:18:15.388+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-08 10:18:15.637+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-08 10:18:15.67+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-05-08 10:18:15.698+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-08 10:18:15.935+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-05-08 10:18:16.636+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-05-08 10:18:16.673+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-08 10:18:16.911+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-05-08 10:18:17.143+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-08 10:18:17.36+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-08 10:18:17.591+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-08 10:18:17.832+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-08 10:18:18.56+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-08 10:18:18.327+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-05-08 10:18:18.548+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-05-08 10:18:18.575+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-05-08 10:18:18.804+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-09 10:17:22.56+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-05-04','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-09 10:17:22.58+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-09 10:17:22.602+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-09 10:17:22.623+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-09 10:17:22.651+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-09 10:17:22.682+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-09 10:17:22.906+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-05-09 10:17:23.125+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-09 10:17:23.145+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-05-09 10:17:23.168+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-05-09 10:17:23.189+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-05-09 10:17:23.42+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-09 10:17:23.448+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-09 10:17:23.478+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-05-09 10:17:23.502+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-09 10:17:23.525+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-05-09 10:17:23.756+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-05-09 10:17:23.976+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-09 10:17:23.999+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-05-05','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-10 10:17:26.682+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-10 10:17:26.73+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-10 10:17:26.751+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-10 10:17:26.773+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-05-10 10:17:26.795+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-10 10:17:26.815+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-10 10:17:26.837+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-10 10:17:26.862+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-05-10 10:17:26.888+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-05-10 10:17:26.911+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-05-10 10:17:26.93+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-10 10:17:26.956+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-10 10:17:26.979+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2023-05-10 10:17:26.998+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-05-10 10:17:27.28+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2023-05-10 10:17:27.48+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-10 10:17:27.67+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-10 10:17:27.288+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-05-06','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-11 10:17:17.454+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2023-05-11 10:17:17.475+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2023-05-11 10:17:17.501+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3'),
	 ('2023-05-11 10:17:17.53+02','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2023-05-11 10:17:17.553+02','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-05-11 10:17:17.576+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2023-05-11 10:17:17.598+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2023-05-11 10:17:17.623+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-11 10:17:17.844+02','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2023-05-11 10:17:17.868+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-05-11 10:17:17.889+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2023-05-11 10:17:17.908+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2023-05-11 10:17:17.93+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2023-05-11 10:17:17.951+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2023-05-11 10:17:17.97+02','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-05-11 10:17:17.989+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2023-05-11 10:17:18.15+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2023-05-11 10:17:18.33+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2023-05-07','0.0000','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-05-24 10:17:32.15+02','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2023-05-20','0.0000','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2021-09-28 10:18:06.781+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2021-09-24','763.9844','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2021-09-28 10:18:09.791+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2021-09-24','145.4615','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3');
DELETE from vlci2.t_f_text_cb_kpi_consumo_agua WHERE calculationperiod = '2021-06-11' 
AND entityid in ('kpi-benimamet-industrial','kpi-exposicio-domestico','kpi-exposicio-industrial');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2021-06-15 08:16:43.245+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2021-06-11','131.6813','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2021-06-15 08:16:43.335+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2021-06-11','717.3505','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2021-06-15 08:16:43.57+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2021-06-11','315.2992','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2021-06-15 08:16:43.245+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2021-06-11','131.6813','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2021-06-15 08:16:43.335+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2021-06-11','717.3505','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2021-06-15 08:16:43.57+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2021-06-11','315.2992','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3');


-- Residuos
ALTER TABLE vlci2.t_f_text_cb_kpi_residuos
DROP CONSTRAINT t_f_text_cb_kpi_residuos_pkey;

INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:19:39.921+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',666,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:28:06.224+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',2,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:28:06.503+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',4,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:28:04.923+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',1,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:28:06.37+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',3,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:28:06.842+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',5,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:23.823+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',1,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:24.371+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',3,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:24.651+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',5,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:25.82+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',7,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:25.868+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',10,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:26.312+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',12,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:26.765+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',14,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:27.38+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',16,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:27.318+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',18,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:27.582+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',20,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:25.729+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',9,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:26.18+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',11,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:26.437+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',13,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:26.891+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',15,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:27.188+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',17,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:27.441+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',19,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:27.919+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',21,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:28.342+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',23,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:28.822+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',25,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:29.11+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',27,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:29.533+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',29,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:28.17+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',22,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:28.689+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',24,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:28.975+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',26,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:29.444+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',28,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:29.861+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',30,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:30.331+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',32,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:23.958+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',2,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:24.508+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',4,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:24.966+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',6,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:25.394+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',8,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:29.99+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',31,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:30.548+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',33,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:31.19+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',35,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:31.251+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',37,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:31.698+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',39,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:31.931+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',41,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:30.897+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',34,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:31.142+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',36,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:31.564+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',38,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:31.804+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',40,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:32.261+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',42,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:32.734+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',44,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:33.12+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',46,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:32.4+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',43,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:32.852+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',45,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:33.139+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',47,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:33.394+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',49,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:33.615+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',51,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:34.33+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',53,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:34.481+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',55,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:33.244+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',48,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:33.501+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',50,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:33.717+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',52,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:34.35+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',54,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:34.809+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',56,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:35.261+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',58,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:35.131+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',57,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:35.542+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',59,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:36.1+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',61,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:36.747+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',63,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:37.217+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',65,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:37.879+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',67,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:38.353+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',69,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:39.98+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',71,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:39.754+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',73,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:38.19+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',68,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:38.731+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',70,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:39.427+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',72,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:39.874+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',74,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:40.3+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',75,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:40.483+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',77,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:41.11+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',79,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:41.53+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',81,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:41.966+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',83,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:40.37+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',76,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:40.772+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',78,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:41.212+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',80,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:41.841+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',82,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:42.257+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',84,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:42.602+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',85,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:42.867+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',87,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:43.146+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',89,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:43.457+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',91,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:43.926+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',93,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:44.553+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',95,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:42.734+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',86,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:43.2+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',88,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:43.276+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',90,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:43.789+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',92,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:44.234+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',94,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:44.704+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',96,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:35.677+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',60,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:36.337+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',62,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:37.75+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',64,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:37.52+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',66,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:45.176+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',98,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:45.434+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',100,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:45.95+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',102,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:46.231+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',104,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:46.478+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',106,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:46.948+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',108,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:45.54+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',97,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:45.328+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',99,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:45.583+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',101,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:46.88+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',103,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:46.362+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',105,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:46.617+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',107,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:47.89+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',109,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:47.414+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',110,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:47.858+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',112,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:48.351+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',114,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:48.815+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',116,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:49.383+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',118,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:47.527+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',111,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:48.22+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',113,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:48.482+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',115,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:49.201+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',117,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:49.714+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',119,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:49.877+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',120,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:50.519+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',122,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:50.985+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',124,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:51.447+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',126,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:52.83+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',128,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:54.645+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',145,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:54.911+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',147,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:55.704+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',149,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:56.182+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',151,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:56.661+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',153,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:52.341+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',130,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:52.757+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',132,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:52.986+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',134,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:53.403+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',136,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:53.622+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',138,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:53.828+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',140,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:54.258+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',142,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:54.508+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',144,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:54.786+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',146,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:55.227+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',148,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:56.52+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',150,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:56.293+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',152,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:56.995+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',154,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:57.628+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',156,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:58.94+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',158,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:57.313+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',155,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:57.751+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',157,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:58.242+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',159,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:58.867+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',161,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:59.532+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',163,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:50.175+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',121,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:50.839+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',123,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:51.322+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',125,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:51.714+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',127,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:52.2+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',129,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:58.567+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',160,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:59.216+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',162,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:59.851+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',164,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:00.344+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',166,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:00+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',165,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:00.491+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',167,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:00.713+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',169,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:02.588+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',171,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:52.63+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',131,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:52.862+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',133,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:30:53.293+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',135,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:53.54+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',137,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:53.732+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',139,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:53.93+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',141,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:30:54.386+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',143,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:03.164+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',174,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:03.422+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',176,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:03.88+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',178,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:04.226+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',180,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:04.726+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',182,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:31:05.213+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',184,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:06.51+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',186,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:06.311+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',188,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:06.523+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',190,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:06.785+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',192,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:07.252+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',194,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:07.485+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',196,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:09.484+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',198,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:00.606+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',168,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:00.823+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',170,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:31:02.903+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',172,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:06.173+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',187,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:06.44+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',189,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:06.657+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',191,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:06.923+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',193,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:07.37+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',195,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:07.631+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',197,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:09.842+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',199,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:10.182+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',200,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:10.52+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',202,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:31:10.754+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',204,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:11.231+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',206,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:11.685+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',208,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:10.385+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',201,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:10.65+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',203,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:10.84+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',205,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:11.578+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',207,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:11.822+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',209,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:12.64+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',211,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:03.54+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',173,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:31:03.288+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',175,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:03.531+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',177,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:04.62+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',179,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:04.578+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',181,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:04.885+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',183,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:05.717+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',185,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:11.952+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',210,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:12.185+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',212,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:12.388+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',214,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:12.643+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',216,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:31:13.33+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',218,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:13.803+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',220,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:14.226+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',222,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:12.277+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',213,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:12.506+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',215,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:12.997+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',217,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:13.46+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',219,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:14.112+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',221,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:14.421+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',223,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:14.74+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',224,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:31:15.108+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',226,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:15.577+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',228,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:16.37+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',230,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:16.698+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',232,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:15.21+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',225,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:15.471+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',227,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:15.704+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',229,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:16.358+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',231,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:16.836+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',233,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:16.977+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',234,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:31:17.226+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',236,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:17.49+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',238,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:17.74+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',240,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:18.132+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',242,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:18.551+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',244,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:18.998+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',246,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:17.105+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',235,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:17.365+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',237,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:17.624+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',239,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:18.32+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',241,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-26 09:31:18.448+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',243,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:18.875+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',245,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:19.32+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',247,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-26 09:31:19.454+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',248,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:38.719+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',1,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:40.481+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',3,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:40.959+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',5,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:41.718+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',7,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:40.137+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',2,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:40.61+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',4,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:47:41.278+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',6,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:42.5+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',8,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:42.725+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',10,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:42.402+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',9,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:42.855+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',11,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:43.185+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',13,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:43.59+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',15,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:43.828+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',17,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:44.58+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',19,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:44.498+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',21,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:47:47.484+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',23,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:47.906+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',25,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:48.154+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',27,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:42.988+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',12,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:43.283+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',14,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:43.701+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',16,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:43.949+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',18,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:44.167+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',20,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:44.644+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',22,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:48.406+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',29,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:47:48.843+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',31,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:49.87+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',33,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:49.809+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',35,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:50.296+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',37,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:50.993+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',39,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:51.537+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',42,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:51.787+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',44,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:52.465+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',46,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:53.13+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',48,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:53.599+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',50,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:47:54.4+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',53,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:54.44+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',55,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:55.196+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',57,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:55.513+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',59,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:55.766+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',61,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:56.45+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',63,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:51.442+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',41,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:51.661+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',43,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:52.146+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',45,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:52.769+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',47,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:47:53.258+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',49,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:53.737+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',51,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:56.485+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',65,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:56.71+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',67,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:57.27+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',69,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:57.561+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',71,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:58.17+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',73,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:58.305+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',75,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:58.782+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',77,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:59.239+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',79,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:47:59.495+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',81,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:00.186+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',83,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:00.423+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',85,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:01.23+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',87,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:01.922+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',89,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:47.83+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',24,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:48.52+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',26,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:48.274+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',28,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:48.702+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',30,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:48.985+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',32,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:47:49.412+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',34,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:49.967+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',36,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:50.672+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',38,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:51.336+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',40,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:53.881+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',52,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:54.335+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',54,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:54.795+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',56,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:55.328+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',58,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:55.639+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',60,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:55.908+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',62,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:47:56.173+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',64,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:02.246+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',91,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:02.914+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',93,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:03.478+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',95,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:03.904+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',97,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:04.582+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',99,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:01.57+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',88,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:02.42+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',90,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:02.576+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',92,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:03.386+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',94,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:03.597+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',96,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:04.227+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',98,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:04.927+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',100,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:05.468+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',102,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:05.914+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',104,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:06.391+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',106,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:56.579+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',66,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:56.929+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',68,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:57.425+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',70,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:57.688+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',72,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:47:58.165+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',74,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:58.643+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',76,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:05.72+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',101,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:05.808+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',103,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:06.38+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',105,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:06.726+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',107,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:07.182+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',109,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:06.845+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',108,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:07.325+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',110,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:07.586+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',112,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:08.64+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',114,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:08.604+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',116,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:59.118+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',78,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:59.372+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',80,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:47:59.823+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',82,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:00.302+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',84,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:00.778+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',86,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:07.448+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',111,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:07.703+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',113,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:08.297+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',115,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:08.915+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',117,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:09.154+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',119,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:09.759+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',121,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:09.44+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',118,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:09.453+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',120,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:09.892+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',122,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:10.117+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',124,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:10.891+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',126,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:09.995+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',123,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:10.541+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',125,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:11.17+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',127,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:11.471+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',129,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:11.712+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',131,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:12.184+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',133,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:11.351+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',128,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:11.602+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',130,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:12.43+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',132,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:12.333+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',134,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:13.9+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',136,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:13.455+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',138,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:12.684+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',135,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:13.34+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',137,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:13.783+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',139,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:14.245+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',141,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:14.749+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',143,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:14.112+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',140,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:14.387+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',142,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:14.848+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',144,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:15.632+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',146,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:16.141+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',148,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:15.205+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',145,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:15.97+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',147,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:16.488+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',149,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:16.894+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',151,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:16.569+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',150,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:17.31+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',152,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:17.492+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',154,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:17.746+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',156,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:18.155+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',158,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:18.424+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',160,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:18.707+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',162,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:17.339+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',153,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:17.626+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',155,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:18.65+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',157,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:18.272+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',159,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:18.567+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',161,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:18.844+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',163,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:19.107+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',165,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:18.963+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',164,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:19.254+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',166,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:19.537+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',168,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:20.2+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',170,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:20.726+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',172,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:21.121+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',174,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:19.396+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',167,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:19.877+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',169,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:20.377+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',171,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:21.11+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',173,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:21.233+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',175,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:21.69+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',177,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:21.56+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',176,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:21.81+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',178,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:22.57+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',180,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:22.388+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',182,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:22.855+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',184,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:23.491+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',186,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:23.746+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',188,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:21.936+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',179,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:22.193+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',181,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:22.719+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',183,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:23.183+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',185,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:23.609+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',187,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:23.872+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',189,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:24.4+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',190,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:24.253+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',192,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:24.53+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',194,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:24.82+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',196,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:25.289+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',198,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:25.764+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',200,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:26.1+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',202,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:24.11+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',191,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:24.376+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',193,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:24.683+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',195,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:25.163+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',197,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:25.637+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',199,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:25.883+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',201,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:26.343+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',203,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:26.454+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',204,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:26.931+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',206,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:27.364+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',208,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:27.628+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',210,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:27.924+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',212,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:28.147+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',214,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:26.573+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',205,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:27.239+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',207,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:27.484+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',209,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:27.78+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',211,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:28.44+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',213,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:28.479+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',215,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:29.201+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',217,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:28.817+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',216,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:29.371+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',218,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:29.818+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',220,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:30.453+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',222,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:29.499+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',219,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:30.131+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',221,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:30.767+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',223,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:31.447+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',225,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:31.113+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',224,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:31.582+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',226,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:32.222+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',228,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:32.433+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',230,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:32.704+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',232,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:33.6+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',234,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:33.454+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',236,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:31.89+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',227,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:32.328+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',229,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:32.569+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',231,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:32.859+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',233,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:33.329+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',235,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:33.808+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',237,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:34.141+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',238,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:34.608+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',240,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:34.915+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',242,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:35.359+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',244,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:35.775+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',246,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:34.285+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',239,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:34.783+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',241,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:35.236+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',243,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:35.676+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',245,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');
INSERT INTO vlci2.t_f_text_cb_kpi_residuos (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,updatedat,sliceanddicevalue1,sliceanddicevalue2,numeratorvalue,denominatorvalue,measureunit) VALUES
	 ('2021-01-27 07:48:35.899+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',247,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs'),
	 ('2021-01-27 07:48:36.33+01','/residuos','is-res-002-kgs-mobles','KeyPerformanceIndicator','31-12-2020',248,'2020-10-27T00:00:00.000Z','Zona 3','Mobles','','','Kgs');


-- Medioambiente

ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved
DROP CONSTRAINT t_datos_cb_medioambiente_airqualityobserved_pkey;

delete from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved in ('2023-04-30 04:00:00','2023-04-30 14:00:00','2023-04-30 11:00:00','2023-04-30 05:00:00','2023-04-27 10:00:00','2023-04-30 13:00:00','2023-04-30 12:00:00','2023-04-30 07:00:00') 
and entityid = 'A10_OLIVERETA_60m';
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved = '2023-04-28 18:00:00' and entityid = 'A07_VALENCIACENTRE_60m';
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved = '2023-05-07 04:00:00' and entityid = 'A02_BULEVARDSUD_60m';

INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-03 05:54:28.955','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.969','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.99','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.3','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.59','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.69','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.83','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.96','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.115','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.168','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-03 05:54:29.185','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.202','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.228','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.259','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:29.28','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-03 05:54:27.43','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',13,NULL,22,NULL,12,NULL,'2023-04-30 11:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.106','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',13,NULL,22,NULL,12,NULL,'2023-04-30 11:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.464','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',13,NULL,22,NULL,12,NULL,'2023-04-30 11:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.465','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',13,NULL,22,NULL,12,NULL,'2023-04-30 11:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-04-30 12:20:25.539','/MedioAmbiente','A07_VALENCIACENTRE_60m','AirQualityObserved',14,NULL,25,NULL,16,NULL,'2023-04-28 18:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Centro','VALÈNCIA CENTRE','ok'),
	 ('2023-04-30 12:20:25.645','/MedioAmbiente','A07_VALENCIACENTRE_60m','AirQualityObserved',14,NULL,25,NULL,16,NULL,'2023-04-28 18:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Centro','VALÈNCIA CENTRE','ok'),
	 ('2023-04-30 12:20:25.673','/MedioAmbiente','A07_VALENCIACENTRE_60m','AirQualityObserved',14,NULL,25,NULL,16,NULL,'2023-04-28 18:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Centro','VALÈNCIA CENTRE','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-08 05:20:17.241','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok'),
	 ('2023-05-08 05:20:17.321','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok'),
	 ('2023-05-08 05:20:17.356','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok'),
	 ('2023-05-08 05:20:17.38','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok'),
	 ('2023-05-08 05:20:17.392','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok'),
	 ('2023-05-08 05:20:17.41','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok'),
	 ('2023-05-08 05:20:17.43','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok'),
	 ('2023-05-08 05:20:17.463','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok'),
	 ('2023-05-08 05:20:17.469','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok'),
	 ('2023-05-08 05:20:17.478','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-08 05:20:17.494','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok'),
	 ('2023-05-08 05:20:17.525','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-03 05:54:24.685','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',26,NULL,26,NULL,13,NULL,'2023-04-30 05:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:24.686','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',26,NULL,26,NULL,13,NULL,'2023-04-30 05:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:24.724','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',26,NULL,26,NULL,13,NULL,'2023-04-30 05:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:24.75','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',26,NULL,26,NULL,13,NULL,'2023-04-30 05:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:24.775','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',26,NULL,26,NULL,13,NULL,'2023-04-30 05:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:26.828','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',26,NULL,26,NULL,13,NULL,'2023-04-30 05:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:27.803','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',26,NULL,26,NULL,13,NULL,'2023-04-30 05:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:27.826','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',26,NULL,26,NULL,13,NULL,'2023-04-30 05:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:27.826','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',26,NULL,26,NULL,13,NULL,'2023-04-30 05:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-03 05:53:46.57','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',58,NULL,30,NULL,18,NULL,'2023-04-27 10:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:53:46.619','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',58,NULL,30,NULL,18,NULL,'2023-04-27 10:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-03 05:54:28.546','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,19,NULL,10,NULL,'2023-04-30 13:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.556','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,19,NULL,10,NULL,'2023-04-30 13:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.571','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,19,NULL,10,NULL,'2023-04-30 13:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.752','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,19,NULL,10,NULL,'2023-04-30 13:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.773','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,19,NULL,10,NULL,'2023-04-30 13:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.846','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,19,NULL,10,NULL,'2023-04-30 13:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.868','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,19,NULL,10,NULL,'2023-04-30 13:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-03 05:54:28.143','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.162','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.179','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.253','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.27','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.299','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.313','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.354','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.372','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.389','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-03 05:54:28.467','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.494','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.494','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:28.494','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-03 05:54:25.446','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,25,NULL,13,NULL,'2023-04-30 07:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:27.805','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,25,NULL,13,NULL,'2023-04-30 07:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
	 ('2023-05-03 05:54:26.826','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',31,NULL,26,NULL,14,NULL,'2023-04-30 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok'),
	 ('2023-05-03 05:54:26.827','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',31,NULL,26,NULL,14,NULL,'2023-04-30 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');


-- Temperatura
ALTER TABLE vlci2.t_f_text_kpi_temperatura_media_diaria
DROP CONSTRAINT t_f_text_kpi_temperatura_media_diaria_pkey;

INSERT INTO vlci2.t_f_text_kpi_temperatura_media_diaria (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,measurelandunit) VALUES
	 ('2021-08-31 13:21:07.515+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-30',28.610416667,'ºC'),
	 ('2021-08-31 13:21:19.432+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-01',26.147916667,'ºC'),
	 ('2021-08-31 13:21:21.928+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-06',28.667361111,'ºC'),
	 ('2021-08-31 13:21:28.477+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-09',27.94375,'ºC'),
	 ('2021-08-31 13:21:29.36+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-11',28.514583333,'ºC'),
	 ('2021-08-31 13:21:29.729+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-12',30.884722222,'ºC'),
	 ('2021-08-31 13:21:34.664+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-20',28.11884058,'ºC'),
	 ('2021-08-31 13:21:36.236+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-22',28.154861111,'ºC'),
	 ('2021-08-31 13:21:10.862+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-31',28.807638889,'ºC'),
	 ('2021-08-31 13:21:19.942+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-02',27.18125,'ºC');
INSERT INTO vlci2.t_f_text_kpi_temperatura_media_diaria (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,measurelandunit) VALUES
	 ('2021-08-31 13:21:20.914+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-04',28.816666667,'ºC'),
	 ('2021-08-31 13:21:23.828+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-07',30.825,'ºC'),
	 ('2021-08-31 13:21:32.518+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-17',27.309027778,'ºC'),
	 ('2021-08-31 13:21:39.66+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-26',28.197222222,'ºC'),
	 ('2021-08-31 13:21:41.475+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-27',29.690972222,'ºC'),
	 ('2021-08-31 13:21:20.24+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-03',27.675,'ºC'),
	 ('2021-08-31 13:21:21.398+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-05',28.947222222,'ºC'),
	 ('2021-08-31 13:21:28.927+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-10',26.963888889,'ºC'),
	 ('2021-08-31 13:21:30.754+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-14',29.509722222,'ºC'),
	 ('2021-08-31 13:21:31.303+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-15',30.6375,'ºC');
INSERT INTO vlci2.t_f_text_kpi_temperatura_media_diaria (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,measurelandunit) VALUES
	 ('2021-08-31 13:21:31.668+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-16',29.379166667,'ºC'),
	 ('2021-08-31 13:21:33.269+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-19',28.330769231,'ºC'),
	 ('2021-08-31 13:21:37.729+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-24',28.268055556,'ºC'),
	 ('2021-08-31 13:21:42.493+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-29',28.40625,'ºC'),
	 ('2021-08-31 13:21:24.286+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-08',26.873611111,'ºC'),
	 ('2021-08-31 13:21:30.309+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-13',29.022222222,'ºC'),
	 ('2021-08-31 13:21:32.885+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-18',28.263194444,'ºC'),
	 ('2021-08-31 13:21:38.235+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-25',25.652083333,'ºC'),
	 ('2021-08-31 13:21:42.863+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-30',26.194444444,'ºC'),
	 ('2021-08-30 08:20:46.421+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-29',28.40625,'ºC');
INSERT INTO vlci2.t_f_text_kpi_temperatura_media_diaria (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,measurelandunit) VALUES
	 ('2021-08-31 13:19:44.56+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-04',29.997916667,'ºC'),
	 ('2021-08-31 13:19:43.535+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-03',26.461111111,'ºC'),
	 ('2021-08-31 13:19:44.98+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-05',28.315172414,'ºC'),
	 ('2021-08-31 13:19:41.736+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-02',26.68705036,'ºC'),
	 ('2021-08-31 13:19:49.307+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-10',27.438297872,'ºC'),
	 ('2021-08-31 13:19:45.658+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-06',31.49375,'ºC'),
	 ('2021-08-31 13:19:46.13+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-07',27.538888889,'ºC'),
	 ('2021-08-31 13:19:46.539+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-08',26.805555556,'ºC'),
	 ('2021-08-31 13:19:47.5+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-09',26.855862069,'ºC'),
	 ('2021-08-31 13:19:49.881+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-11',27.802083333,'ºC');
INSERT INTO vlci2.t_f_text_kpi_temperatura_media_diaria (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,measurelandunit) VALUES
	 ('2021-08-31 13:19:50.841+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-12',29.767142857,'ºC'),
	 ('2021-08-31 13:19:51.355+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-13',26.472916667,'ºC'),
	 ('2021-08-31 13:19:54.812+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-14',25.50625,'ºC'),
	 ('2021-08-31 13:20:02.316+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-15',25.567361111,'ºC'),
	 ('2021-08-31 13:20:03.67+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-16',26.0375,'ºC'),
	 ('2021-08-31 13:20:08.618+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-17',26.825,'ºC'),
	 ('2021-08-31 13:20:13.955+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-18',27.252083333,'ºC'),
	 ('2021-08-31 13:20:16.856+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-19',28.095833333,'ºC'),
	 ('2021-08-31 13:20:23.99+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-21',28.593055556,'ºC'),
	 ('2021-08-31 13:20:24.46+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-22',28.947916667,'ºC');
INSERT INTO vlci2.t_f_text_kpi_temperatura_media_diaria (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,measurelandunit) VALUES
	 ('2021-08-31 13:20:22.963+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-20',28.213888889,'ºC'),
	 ('2021-08-31 13:20:25.148+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-23',28.804166667,'ºC'),
	 ('2021-08-31 13:20:34.138+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-24',28.246527778,'ºC'),
	 ('2021-08-31 13:20:39.29+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-25',28.369444444,'ºC'),
	 ('2021-08-31 13:20:50.269+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-26',25.811111111,'ºC'),
	 ('2021-08-31 13:21:00.121+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-27',26.495833333,'ºC'),
	 ('2021-08-31 13:21:05.921+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-29',28.438888889,'ºC'),
	 ('2021-08-31 13:21:05.144+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-07-28',28.08125,'ºC'),
	 ('2021-08-31 13:21:35.688+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-21',28.242361111,'ºC'),
	 ('2021-08-31 13:21:36.566+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-23',28.627083333,'ºC');
INSERT INTO vlci2.t_f_text_kpi_temperatura_media_diaria (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,measurelandunit) VALUES
	 ('2021-08-31 13:21:42.97+02','/MedioAmbiente','Kpi-Temperatura-Media-Diaria','KeyPerformanceIndicator','2021-08-28',28.548611111,'ºC');


-- Sonometros
ALTER TABLE vlci2.t_f_text_cb_sonometros_ruzafa_daily
DROP CONSTRAINT t_f_text_cb_sonometros_ruzafa_daily_pkey;

ALTER TABLE vlci2.t_datos_cb_sonometros_hopvlci_daily
DROP CONSTRAINT t_datos_cb_sonometros_hopvlci_daily_pkey;

DELETE from vlci2.t_f_text_cb_sonometros_ruzafa_daily 
WHERE dateobserved = '2021-06-30'AND entityid  = 'T248655-daily';
INSERT INTO vlci2.t_f_text_cb_sonometros_ruzafa_daily (recvtime,fiwareservicepath,entityid,entitytype,dateobserved,"name",laeq,laeq_d,laeq_e,laeq_n,laeq_den,error_status,"source","location") VALUES
	 ('2021-07-01 00:00:34.477+02','/sonometros','T248655-daily','NoiseLevelObserved','2021-06-30','Sensor de ruido del barrio de ruzafa. Cadiz 3','67.0','68.9','64.5','58.7','68.9',NULL,'CESVA','01010000002635B401D800D8BFEA3015D165BB4340'),
	 ('2021-07-01 00:00:34.477+02','/sonometros','T248655-daily','NoiseLevelObserved','2021-06-30','Sensor de ruido del barrio de ruzafa. Cadiz 3','67.0','68.9','64.5','58.7','68.9',NULL,'CESVA','01010000002635B401D800D8BFEA3015D165BB4340');


-- Wifi
ALTER TABLE vlci2.t_f_text_cb_wifi_kpi_urbo
DROP CONSTRAINT t_f_text_cb_wifi_kpi_urbo_pkey;


UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo 
SET dlnumberofnewworkersupdatedat = '2022-10-24', dlnumberofnewworkerscalculationperiodfrom = '2022-10-23T00:00:00.000Z'
WHERE  entityid = 'OCI.DL.002' AND recvtime = '2022-10-25 05:25:06.285 +0200';
UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo 
SET dlnumberofnewworkersupdatedat = '2022-05-13', dlnumberofnewworkerscalculationperiodfrom = '2022-05-12T00:00:00.000Z'
WHERE  entityid = 'OCI.DL.002' AND recvtime = '2022-05-14 05:25:06.894 +0200';
UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo 
SET dlnumberofnewcitizenscalculationperiodfrom = '2021-05-31T00:00:00.000Z', dlnumberofnewuserscalculationperiodfrom = '2021-05-31T00:00:00.000Z',
dlnumberofnewworkerscalculationperiodfrom = '2021-05-31T00:00:00.000Z'
WHERE entityid = 'OCI.DL.002' AND recvtime = '2021-06-02 15:00:32.135 +0200';
UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo 
SET dlnumberofnewcitizenscalculationperiodfrom = '2020-11-30T00:00:00.000Z', dlnumberofnewuserscalculationperiodfrom = '2020-11-30T00:00:00.000Z',
dlnumberofnewworkerscalculationperiodfrom = '2020-11-30T00:00:00.000Z', dlnumberofnewcitizensupdatedat = '2020-12-01',dlnumberofnewusersupdatedat = '2020-12-01',
dlnumberofnewworkersupdatedat = '2020-12-01'
WHERE  entityid = 'OCI.DL.001' AND recvtime = '2020-12-03 16:00:12.343 +0100';
UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo 
SET dlnumberofnewcitizenscalculationperiodfrom = '2021-05-31T00:00:00.000Z', dlnumberofnewuserscalculationperiodfrom = '2021-05-31T00:00:00.000Z',
dlnumberofnewworkerscalculationperiodfrom = '2021-05-31T00:00:00.000Z'
WHERE  entityid = 'OCI.DL.001' AND recvtime = '2021-06-02 15:00:32.484 +0200';

INSERT INTO vlci2.t_f_text_cb_wifi_kpi_urbo (recvtime,fiwareservicepath,entityid,entitytype,calculationfrequency,category,dataprovider,dlnumberofnewcitizens,dlnumberofnewcitizenscalculationperiodfrom,dlnumberofnewcitizenssource,dlnumberofnewcitizensupdatedat,dlnumberofnewuser,dlnumberofnewuserscalculationperiodfrom,dlnumberofnewuserssource,dlnumberofnewusersupdatedat,dlnumberofnewworkers,dlnumberofnewworkerscalculationperiodfrom,dlnumberofnewworkerssource,dlnumberofnewworkersupdatedat,dlnumberofneww4eu,dlnumberofneww4eucalculationperiodfrom,dlnumberofneww4eusource,dlnumberofneww4euupdatedat,"name",organization,process,"source") VALUES
	 ('2020-06-05 17:00:13.497+02','/wifi','OCI.DL.002','KeyPerformanceIndicatorURBO','daily','["process"]','URL eWAS',30392,'2020-06-02T00:00:00.00Z','eWas. Fichero: alta_total_usuarios','2020-06-05',34012,'2020-06-02T00:00:00.00Z','eWas. Ficheros: alta_total_usuarios y f_alta_total_usuarios','2020-06-05',3620,'2020-06-02T00:00:00.00Z','eWas. Fichero: f_alta_total_usuarios','2020-06-05',NULL,NULL,NULL,NULL,'Número de usuarios total dados de alta en Wifi Valencia y AVC','OCI','WIFI','eWAS'),
	 ('2020-07-24 06:53:16.2+02','/wifi','OCI.DL.002','KeyPerformanceIndicatorURBO','daily','["process"]','URL eWAS',30482,'2020-07-22T00:00:00.00Z','eWas. Fichero: alta_total_usuarios','2020-07-24',34149,'2020-07-22T00:00:00.00Z','eWas. Ficheros: alta_total_usuarios y f_alta_total_usuarios','2020-07-24',3667,'2020-07-22T00:00:00.00Z','eWas. Fichero: f_alta_total_usuarios','2020-07-24',NULL,NULL,NULL,NULL,'Número de usuarios total dados de alta en Wifi Valencia y AVC','OCI','WIFI','eWAS');
INSERT INTO vlci2.t_f_text_cb_wifi_kpi_urbo (recvtime,fiwareservicepath,entityid,entitytype,calculationfrequency,category,dataprovider,dlnumberofnewcitizens,dlnumberofnewcitizenscalculationperiodfrom,dlnumberofnewcitizenssource,dlnumberofnewcitizensupdatedat,dlnumberofnewuser,dlnumberofnewuserscalculationperiodfrom,dlnumberofnewuserssource,dlnumberofnewusersupdatedat,dlnumberofnewworkers,dlnumberofnewworkerscalculationperiodfrom,dlnumberofnewworkerssource,dlnumberofnewworkersupdatedat,dlnumberofneww4eu,dlnumberofneww4eucalculationperiodfrom,dlnumberofneww4eusource,dlnumberofneww4euupdatedat,"name",organization,process,"source") VALUES
	 ('2020-06-05 17:00:13.891+02','/wifi','OCI.DL.001','KeyPerformanceIndicatorURBO','daily','["process"]','URL eWAS',7,'2020-06-02T00:00:00.00Z','eWas. Fichero: alta_usuarios','2020-06-05',8,'2020-06-02T00:00:00.00Z','eWas. Ficheros: alta_usuarios y f_ alta_usuarios','2020-06-05',1,'2020-06-02T00:00:00.00Z','eWas. Fichero: f_alta_usuarios','2020-06-05',NULL,NULL,NULL,NULL,'Número de usuarios dados de alta en Wifi Valencia y AVC','OCI','WIFI','eWAS');




ALTER TABLE vlci2.t_f_text_cb_wifi_kpi_conexiones
DROP CONSTRAINT t_f_text_cb_wifi_kpi_conexiones_pkey;

TRUNCATE vlci2.t_f_text_cb_wifi_kpi_conexiones;
insert into vlci2.t_f_text_cb_wifi_kpi_conexiones
select * from vlci2.t_f_text_cb_wifi_kpi_conexiones_backup
where sliceanddicevalue1 is not null
and sliceanddicevalue2 is not null;

drop table vlci2.t_f_text_cb_wifi_kpi_conexiones_backup;

COMMIT;
