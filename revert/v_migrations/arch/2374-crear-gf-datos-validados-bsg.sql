-- Revert postgis-vlci-vlci2:v_migrations/2302-crear-gf-datos-validados from pg

BEGIN;

delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('A%10m_vm%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('A%10m_va%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('A%60m_vm%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('A%60m_va%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('A%24h_vm%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('A%24h_va%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('W%10m_vm%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('W%10m_va%');

COMMIT;
