-- Revert postgis-vlci-vlci2:v_migrations/2102-ocoval-nueva-entidad-reparadora-correos-buzones from pg

BEGIN;

DELETE FROM vlci2.vlci_ocoval_entidades WHERE compania = 'Otros' and entidad = 'Correos Buzones';

COMMIT;
