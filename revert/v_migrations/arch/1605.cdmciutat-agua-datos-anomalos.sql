-- Revert postgis-vlci-vlci2:v_migrations/1605.cdmciutat-agua-datos-anomalos from pg

BEGIN;

  INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-03-27 10:17:53.579+02','/aguas','kpi-valencia-ciudad-total','KeyPerformanceIndicator','2023-03-26','9140.2667','Consumo_agua_total_valencia','Valencia ciudad','N/A','m3');

INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2021-05-20 08:01:00.95+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-05-19','22163.8000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-05-23 08:01:02.551+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-05-19','228.7283','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-05-23 08:01:02.229+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-05-19','502.6598','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-05-23 08:01:02.868+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-05-19','45.2460','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2021-05-21 08:01:22.996+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-05-20','10489.0610','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-05-24 08:03:44.51+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-05-20','257.0961','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-05-24 08:03:44.381+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-05-20','438.3215','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-05-24 08:03:44.839+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-05-20','44.5386','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-09-06 10:17:54.185+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-09-05','4482.4000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-09-07 10:17:21.35+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-09-06','4784.0000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-09-09 10:17:02.242+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-05','1006.2096','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-09-09 10:17:03.831+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-05','422.3009','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-09-09 10:17:19.189+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-09-08','5895.6000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-09-08 10:17:09.689+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-09-07','5209.4000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-09-09 10:17:06.39+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-05','28.1190','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-09-10 10:17:03.771+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-06','813.7762','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-09-10 10:17:03.853+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-06','49.5992','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-09-10 10:17:04.656+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-09-09','6284.7000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-09-10 10:17:03.819+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-06','364.6465','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-09-11 10:16:58.2+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-07','773.3573','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-09-11 10:16:58.949+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-09-10','7040.3000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-09-11 10:16:58.285+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-07','35.1442','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-09-11 10:16:58.239+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-07','244.2389','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-09-13 10:02:19.776+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-09','279.2493','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-09-14 10:17:05.216+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-10','328.1432','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-09-14 10:17:07.94+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-10','756.4599','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-09-14 10:17:08.786+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-09-13','8163.3000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-09-14 10:17:10.55+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-10','46.5377','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-09-15 10:17:05.401+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-11','25.5387','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-09-15 10:17:05.155+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-11','310.6392','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-09-16 10:17:11.278+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-12','272.3215','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-09-16 10:17:10.755+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-12','923.8368','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-09-16 10:17:10.842+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-09-15','1843.2000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-09-16 10:17:11.808+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-12','28.4640','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-09-17 10:17:55.945+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-13','47.5184','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-09-18 10:16:57.67+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-14','596.2713','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-09-18 10:16:57.949+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-14','33.0084','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-09-18 10:16:57.712+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-14','314.8722','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-09-19 10:16:59.4+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-15','613.6466','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-09-13 10:02:19.622+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-09','699.8187','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-09-13 10:02:19.926+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-09','36.9917','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-09-13 10:02:24.431+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-09-12','8115.5000','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-09-15 10:17:05.123+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-11','1020.9722','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-09-15 10:17:06.36+02','/aguas','kpi-perellonet-total','KeyPerformanceIndicator','2021-09-14','4959.3600','Consumo_agua_total_perellonet','Perellonet','N/A','m3'),
	 ('2021-09-17 10:17:55.685+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-13','386.6008','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-09-17 10:17:51.513+02','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-13','658.0621','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-09-19 10:16:59.438+02','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-15','342.2208','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3'),
	 ('2021-09-19 10:16:59.489+02','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-15','45.0637','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-11-25 11:37:40.81+01','/aguas','kpi-perellonet-domestico','KeyPerformanceIndicator','2021-09-08','761.4466','Consumo_agua_domestico_perellonet','Perellonet','domestico','m3'),
	 ('2021-11-25 11:37:40.717+01','/aguas','kpi-perellonet-servMunicipal','KeyPerformanceIndicator','2021-09-08','49.2766','Consumo_agua_ServMunicipal_perellonet','Perellonet','ServMunicipal','m3'),
	 ('2021-11-25 11:37:40.426+01','/aguas','kpi-perellonet-industrial','KeyPerformanceIndicator','2021-09-08','259.7829','Consumo_agua_industrial_perellonet','Perellonet','industrial','m3');

INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2021-05-01 08:01:03.32+02','/aguas','kpi-mestalla-industrial','KeyPerformanceIndicator','2021-04-27','3425.1514','Consumo_agua_industrial_mestalla','Mestalla','industrial','m3'),
	 ('2021-05-01 08:01:03.391+02','/aguas','kpi-mestalla-total','KeyPerformanceIndicator','2021-04-27','3243.0426','Consumo_agua_total_mestalla','Mestalla','N/A','m3'),
	 ('2021-05-01 08:01:09.132+02','/aguas','kpi-mestalla-domestico','KeyPerformanceIndicator','2021-04-27','1801.1238','Consumo_agua_domestico_mestalla','Mestalla','domestico','m3'),
	 ('2021-05-01 08:01:09.29+02','/aguas','kpi-mestalla-servMunicipal','KeyPerformanceIndicator','2021-04-27','36.7351','Consumo_agua_ServMunicipal_mestalla','Mestalla','ServMunicipal','m3');

INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:33.181+02','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-11','3894.6482','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-19 14:10:34.48+02','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-12','3872.7905','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-19 14:10:34.26+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-11','901.1921','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:34.5+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-11','92.1198','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-19 14:10:35.102+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-12','451.1925','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-19 14:10:35.503+02','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-14','3270.5154','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-19 14:10:35.858+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-11','448.7670','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-19 14:10:35.879+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-10','908.4708','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:36.475+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-13','88.9564','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-19 14:10:36.713+02','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-13','3790.0871','Consumo_agua_total_exposicio','Exposicio','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2022-05-19 14:10:37.406+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-10','427.8308','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-19 14:10:37.434+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-10','11.4942','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-19 14:10:37.462+02','/aguas','kpi-exposicio-total','KeyPerformanceIndicator','2022-05-10','3951.9313','Consumo_agua_total_exposicio','Exposicio','N/A','m3'),
	 ('2022-05-19 14:10:40.646+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-12','891.5013','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:40.693+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-12','89.9179','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3'),
	 ('2022-05-19 14:10:41.87+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-14','332.2175','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-19 14:10:41.608+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-13','884.3823','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:41.63+02','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2022-05-13','401.3039','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3'),
	 ('2022-05-19 14:10:41.717+02','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2022-05-14','832.1814','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2022-05-19 14:10:41.742+02','/aguas','kpi-exposicio-servMunicipal','KeyPerformanceIndicator','2022-05-14','61.4094','Consumo_agua_ServMunicipal_exposicio','Exposicio','ServMunicipal','m3');

INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2020-12-22 15:26:11.523+01','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2020-10-28','16.7717','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2020-12-22 15:26:13.114+01','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2020-10-29','1389.0175','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2020-12-22 15:27:26.577+01','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2020-10-27','486.2692','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2020-12-22 15:27:30.209+01','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2020-10-28','79.5771','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2020-12-22 15:27:32.986+01','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2020-10-29','492.1429','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2020-12-22 15:40:15.678+01','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2020-10-27','74.2381','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2020-12-22 15:40:16.377+01','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2020-10-27','1540.3925','Consumo_agua_total_soternes','Soternes','N/A','m3'),
	 ('2020-12-22 15:40:18.769+01','/aguas','kpi-soternes-domestico','KeyPerformanceIndicator','2020-10-28','487.2332','Consumo_agua_domestico_soternes','Soternes','domestico','m3'),
	 ('2020-12-22 15:40:23.222+01','/aguas','kpi-soternes-industrial','KeyPerformanceIndicator','2020-10-29','83.0017','Consumo_agua_industrial_soternes','Soternes','industrial','m3'),
	 ('2020-12-22 15:40:23.608+01','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2020-10-29','10.8417','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2020-12-22 15:41:16.572+01','/aguas','kpi-soternes-servMunicipal','KeyPerformanceIndicator','2020-10-27','11.6537','Consumo_agua_ServMunicipal_soternes','Soternes','ServMunicipal','m3'),
	 ('2020-12-22 15:41:19.298+01','/aguas','kpi-soternes-total','KeyPerformanceIndicator','2020-10-28','1828.7975','Consumo_agua_total_soternes','Soternes','N/A','m3');

INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2021-03-22 09:01:43.324+01','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2021-03-21','2340.8800','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2021-03-24 09:01:32.81+01','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2021-03-23','2286.1792','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2021-03-24 09:01:32.81+01','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2021-03-23','2286.1792','Consumo_agua_total_benimamet','Benimamet','N/A','m3'),
	 ('2021-03-25 09:01:11.755+01','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2021-03-21','94.4123','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2021-03-25 09:01:14.94+01','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2021-03-21','1359.4791','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2021-03-25 09:01:14.568+01','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2021-03-21','126.4059','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2021-03-27 09:00:55.454+01','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2021-03-23','119.7522','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2021-03-27 09:00:55.99+01','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2021-03-23','1340.8648','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2021-03-27 09:00:55.804+01','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2021-03-23','149.4633','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3'),
	 ('2023-02-25 11:38:40.765+01','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','2023-01-23','2172.9268','Consumo_agua_total_benimamet','Benimamet','N/A','m3');
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2023-02-25 11:53:11.575+01','/aguas','kpi-benimamet-domestico','KeyPerformanceIndicator','2023-01-23','0.0000','Consumo_agua_domestico_benimamet','Benimamet','domestico','m3'),
	 ('2023-02-25 11:53:11.61+01','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2023-01-23','0.0000','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2023-02-25 11:53:11.637+01','/aguas','kpi-benimamet-servMunicipal','KeyPerformanceIndicator','2023-01-23','0.0000','Consumo_agua_ServMunicipal_benimamet','Benimamet','ServMunicipal','m3');
	 
INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2021-07-11 16:16:26.484+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2021-07-07','267.4226','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2021-07-11 16:16:26.895+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2021-07-07','109.1116','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2021-06-30 11:07:03.43+02','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2021-06-28','5781.1426','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2021-06-30 11:07:04.506+02','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2021-06-29','5826.6850','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2021-07-02 08:16:59.941+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2021-06-28','2084.1042','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2021-07-02 08:17:00.646+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2021-06-28','109.1588','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2021-07-02 08:17:00.113+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2021-06-28','212.5316','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2021-07-03 08:16:57.698+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2021-06-29','2091.3988','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2021-07-03 08:16:57.955+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2021-06-29','109.5494','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2021-07-03 08:16:57.784+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2021-06-29','242.9349','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2021-07-22 10:02:19.604+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2021-07-18','166.0945','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2021-07-08 08:16:51.962+02','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2021-07-07','5993.6323','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2021-07-11 16:16:25.921+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2021-07-07','2029.1730','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2021-07-13 10:16:30.87+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2021-07-08','2033.9630','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2021-07-13 10:16:30.889+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2021-07-08','270.0241','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2021-07-13 10:16:30.907+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2021-07-08','109.0908','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2021-07-17 10:00:46.903+02','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2021-07-16','5715.6600','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2021-07-19 10:02:11.485+02','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2021-07-18','5548.6724','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2021-07-22 10:01:29.158+02','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2021-07-08','5639.9375','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2021-07-22 10:01:34.975+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2021-07-16','1985.1457','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2021-07-22 10:02:13.206+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2021-07-16','262.8887','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2021-07-22 10:02:30.198+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2021-07-16','109.0165','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2021-07-22 10:03:02.294+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2021-07-18','1981.7842','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2021-07-22 10:03:02.705+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2021-07-18','108.6086','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2021-07-23 13:14:59.4+02','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2021-07-22','5484.6450','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2021-07-26 10:17:27.359+02','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2021-07-22','108.8661','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2021-07-26 10:17:27.336+02','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2021-07-22','245.6243','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2021-07-26 10:17:27.109+02','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2021-07-22','1986.0959','Consumo_agua_domestico_malilla','Malilla','domestico','m3'),
	 ('2021-11-21 10:17:05.372+01','/aguas','kpi-malilla-total','KeyPerformanceIndicator','2021-11-20','1117.4400','Consumo_agua_total_malilla','Malilla','N/A','m3'),
	 ('2021-11-25 08:59:36.222+01','/aguas','kpi-malilla-industrial','KeyPerformanceIndicator','2021-11-20','226.8255','Consumo_agua_industrial_malilla','Malilla','industrial','m3'),
	 ('2021-11-25 08:59:36.753+01','/aguas','kpi-malilla-servMunicipal','KeyPerformanceIndicator','2021-11-20','110.3749','Consumo_agua_ServMunicipal_malilla','Malilla','ServMunicipal','m3'),
	 ('2021-11-25 08:59:36.988+01','/aguas','kpi-malilla-domestico','KeyPerformanceIndicator','2021-11-20','2153.3488','Consumo_agua_domestico_malilla','Malilla','domestico','m3');

COMMIT;
