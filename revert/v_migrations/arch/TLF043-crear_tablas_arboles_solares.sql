-- Revert postgis-vlci-vlci2:v_migrations/TLF043-crear_tablas_arboles_solares from pg

BEGIN;

drop table if exists vlci2.t_datos_cb_generacion_energia_solartree_lastdata;
drop table if exists vlci2.t_datos_cb_generacion_energia_solartree;

COMMIT;
