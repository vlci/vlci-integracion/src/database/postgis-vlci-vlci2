-- Revert postgis-vlci-vlci2:v_migrations/TLF039-eliminar_contenedor_anomalo from pg

BEGIN;

INSERT INTO vlci2.t_datos_cb_wastecontainer_lastdata
(recvtime, fiwareservicepath, entitytype, entityid, timeinstant, fillinglevel, maintenanceowner, "location", temperature, project)
VALUES('2024-01-11 10:24:39.382', '/t_datos_cb_wastecontainer', 'WasteContainer', 'wastecontainer:5ddbff4e6c54998b7f89fb4e', '2020-12-09 16:01:05.751', 0.13, 'Wellness ', 'SRID=4326;POINT (-6.0069 37.402)'::public.geometry, 14.0, 'impulsoVLCi-red.es');

COMMIT;
