-- Revert postgis-vlci-vlci2:v_migrations/585.alta_user_cdm from pg

BEGIN;

DELETE FROM vlci2.t_d_user_rol where pk_user in ('U301381', 'LJCG');
DELETE FROM vlci2.t_d_usuario_servicio where pk_user_id in ('U301381', 'LJCG');

COMMIT;
