-- Revert postgis-vlci-vlci2:v_migrations/2384.error-datos-agua from pg

BEGIN;

INSERT INTO t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2024-01-11 14:10:51.435+01','/aguas','kpi-benimamet-total','KeyPerformanceIndicator','10/01/2024','22.501.000','Consumo_agua_total_benimamet','Benimamet','N/A','m3');


COMMIT;
