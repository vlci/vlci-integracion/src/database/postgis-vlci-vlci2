-- Revert postgis-vlci-vlci2:v_migrations/1912.crear-tablas-referencia-INCO from pg

BEGIN;

DROP TABLE vlci2.t_ref_etl_inco_conceptos;
DROP TABLE vlci2.t_ref_etl_inco_capitulos;

COMMIT;
