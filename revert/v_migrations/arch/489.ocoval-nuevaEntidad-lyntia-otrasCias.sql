-- Revert postgis-vlci-vlci2:v_migrations/489.ocoval-nuevaEntidad-lyntia-otrasCias from pg

BEGIN;

delete from vlci2.vlci_ocoval_entidades where compania='Compañías de Servicios' and entidad='Lyntia';
delete from vlci2.vlci_ocoval_companyia where compania='OTRAS CIAS' and entidad='Lyntia';
delete from vlci2.vlci_ocoval_companyia where compania='OTRAS CIAS' and entidad='Digi';

COMMIT;
