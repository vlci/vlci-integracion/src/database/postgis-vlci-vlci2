-- Revert postgis-vlci-vlci2:v_migrations/TLF023-eliminar_vista_parkingspot_lastdata from pg

BEGIN;

CREATE OR REPLACE VIEW vlci2.vw_datos_cb_trafico_parkingspot_lastdata AS
SELECT name,description, category, project, maintenanceowner, datemodified, entityid, location,
       CASE WHEN datemodified::timestamp >= NOW() - INTERVAL '3 days' THEN 'ok' ELSE 'ko' END AS operationalstatus
FROM vlci2.t_datos_cb_parkingspot_lastdata;


COMMIT;
