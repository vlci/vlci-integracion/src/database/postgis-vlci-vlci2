-- Revert postgis-vlci-vlci2:v_migrations/2243.add-subconceptos-inco from pg

BEGIN;

DELETE FROM vlci2.t_ref_etl_inco_conceptos
WHERE id_concepto IN (10001,21001,22010,22011,22012,22013,22014,36004,42001,42190,45001,45005,45150,46700,49744,49746,75087,75101, 79722);


COMMIT;
