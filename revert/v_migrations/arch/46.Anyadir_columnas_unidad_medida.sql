-- Revert postgis-vlci-vlci2:v_migrations/46.Anyadir_columnas_unidad_medida from pg

BEGIN;

ALTER TABLE vlci2.t_f_text_kpi_trafico_y_bicis DROP COLUMN measureunitcas;
ALTER TABLE vlci2.t_f_text_kpi_trafico_y_bicis DROP COLUMN measureunitval;

ALTER TABLE vlci2.t_f_text_cb_wifi_kpi_conexiones DROP COLUMN measureunitcas;
ALTER TABLE vlci2.t_f_text_cb_wifi_kpi_conexiones DROP COLUMN measureunitval;

ALTER TABLE vlci2.t_datos_cb_emt_kpi DROP COLUMN measureunitcas;
ALTER TABLE vlci2.t_datos_cb_emt_kpi DROP COLUMN measureunitval;

ALTER TABLE vlci2.t_f_text_kpi_precipitaciones_anuales DROP COLUMN measurelandunit;
ALTER TABLE vlci2.t_f_text_kpi_precipitaciones_diarias DROP COLUMN measurelandunit;
ALTER TABLE vlci2.t_f_text_kpi_precipitaciones_mensuales DROP COLUMN measurelandunit;
ALTER TABLE vlci2.t_f_text_kpi_temperatura_maxima_diaria DROP COLUMN measurelandunit;
ALTER TABLE vlci2.t_f_text_kpi_temperatura_media_diaria DROP COLUMN measurelandunit;
ALTER TABLE vlci2.t_f_text_kpi_temperatura_minima_diaria DROP COLUMN measurelandunit;

ALTER TABLE vlci2.t_f_text_cb_kpi_consumo_agua DROP COLUMN measurelandunit;

ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN no2type;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN o3type;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN pm10type;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN pm25type;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved DROP COLUMN so2type;

ALTER TABLE vlci2.t_f_text_cb_kpi_residuos DROP COLUMN measureunit;

COMMIT;
