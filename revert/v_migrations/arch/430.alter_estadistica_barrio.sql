-- Revert postgis-vlci-vlci2:v_migrations/430.alter_estadistica_barrio from pg

BEGIN;

ALTER TABLE vlci2.t_datos_etl_estadistica_valores_indicadores ALTER COLUMN barrio TYPE varchar(4) USING barrio::varchar;
ALTER TABLE vlci2.t_datos_etl_estadistica_valores_indicadores_tmp ALTER COLUMN barrio TYPE varchar(4) USING barrio::varchar;

COMMIT;
