-- Revert postgis-vlci-vlci2:v_migrations/65233.insert-unidades-medida from pg

BEGIN;

DELETE FROM vlci2.t_m_unidad_medida
WHERE pk_unidad_medida = 146;

DELETE FROM vlci2.t_m_unidad_medida
WHERE pk_unidad_medida = 147;

DELETE FROM vlci2.t_m_unidad_medida
WHERE pk_unidad_medida = 148;

COMMIT;
