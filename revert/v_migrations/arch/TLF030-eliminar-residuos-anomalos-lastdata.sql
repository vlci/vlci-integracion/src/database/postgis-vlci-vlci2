-- Revert postgis-vlci-vlci2:v_migrations/TLF030-eliminar-residuos-anomalos-lastdata from pg

BEGIN;

INSERT INTO vlci2.t_datos_cb_wastecontainer_lastdata (recvtime, fiwareservicepath, entitytype, entityid, timeinstant, fillinglevel, maintenanceowner, "location", temperature, project) 
VALUES('2023-10-15 14:01:49.301', '/t_datos_cb_wastecontainer', 'WasteContainer', 'wastecontainer:wastecontainer:5ddbff446c54998b7f89fb0e', '2023-10-15 14:01:49.283', 0.15, NULL, NULL, 45.0, NULL),
    ('2023-10-27 02:02:43.575', '/t_datos_cb_wastecontainer', 'WasteContainer', 'wastecontainer:wastecontainer:5ddbff4f6c54998b7f89fb53', '2023-10-27 02:02:43.557', 0.0, NULL, NULL, 19.0, NULL),
    ('2023-11-13 17:02:46.606', '/t_datos_cb_wastecontainer', 'WasteContainer', 'wastecontainer:866039046360328', '2023-11-13 17:02:46.592', 0.0, NULL, NULL, 19.0, NULL);


COMMIT;
