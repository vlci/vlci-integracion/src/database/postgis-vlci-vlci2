-- Revert postgis-vlci-vlci2:v_migrations/566.eliminar_indicador_isbso11_anual from pg

BEGIN;

INSERT INTO vlci2.t_d_indicador
VALUES(1921 , 'IS.BSO.11', 'IS', 0, 0, 615, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Porcentaje de Averías en el Servicio de Teleasistencia', 'Percentatge d´ avaries en el Servei de Teleassistència', 33, 2, 'SECC. DE SERVICIOS SOCIALES GENERALES', 'Porcentaje de averías en el equipamiento de la persona usuaria que impiden la comunicación de ésta con el CA', 'Percentatge d´ avaries en l´ equipament de la persona usuària que impedeixen la comunicació d´ aquesta amb el CA', 'VLCi', 'VLCi', 'Atenzia - Servicios de Teleasistencia S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'AVG', NULL, NULL, NULL, NULL, NULL, 615, 'S', 'S', NULL, NULL);

COMMIT;
