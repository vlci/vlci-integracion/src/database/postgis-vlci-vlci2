-- Revert postgis-vlci-vlci2:v_migrations/1395.reducir-tamaño-vlci2 from pg

BEGIN;

ALTER TABLE vlci2_arch.aux_data_from_context_broker SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.aux_get_pk_indicador SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_a_indicador_cubo_cas_hist_old_agregados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_a_indicador_cubo_cas_old_agregados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_a_indicador_cubo_idioma_hist SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_a_indicador_cubo_val_hist_old_agregados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_a_indicador_cubo_val_old_agregados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_alertado SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_area_001 SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_area_arch SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_delegacion_001 SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_delegacion_arch SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_indicadores_destino_contextbroker SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_indicadores_insercion_contextbroker SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_monitorizacion SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_servicio_001 SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_servicio_arch SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_d_usuario_servicio_arch SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_f_indicador_valor_datos_deprecados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_f_indicador_valor_proceso_old_agregados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_m_combinados_criterios SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_m_combinados_ratios SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_m_combinados_temporales SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_m_metodo_agg SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_aggregados_001 SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_aggregados_002 SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_agregados_periodicidades SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_agregados_por_combinados_aux SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_alertas_indicadores_excluir SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_clave_combinacion_criterios SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_clave_combinacion_ratios SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_con_ratios SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_constantes_old_agregados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_conversion SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_estados_aux SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_hechos SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_indicador_color SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_indicador_purgado SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_indicador_texto SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_indicador_valor_agg_old_agregados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_indicador_valor_old_agregados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_indicador_valor_proceso_old_agregados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_indicadores_silencio SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_organizacion_municipal_color SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_organizacion_municipal_color_old_agregados SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_periodicidad_purgado SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_tendencias_aux SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_tipo_indicador SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_traduccion SET SCHEMA vlci2;
ALTER TABLE vlci2_arch.t_x_week_day SET SCHEMA vlci2;

ALTER TABLE vlci2_arch.t_x_gestion_fechas_borrar SET SCHEMA vlci2;

COMMIT;
