-- Revert postgis-vlci-vlci2:v_migrations/2772-modify-units-tables from pg

BEGIN;

--restaurar tabla servicios

DROP TABLE vlci2.t_d_servicio;

CREATE TABLE vlci2.t_d_servicio (
	fk_delegacion_id varchar(11) NULL,
	codigo_organico varchar(20) NULL,
	desc_corta_cas varchar(120) NULL,
	desc_larga_cas varchar(200) NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(45) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(45) NULL,
	fk_area_id varchar(11) NULL
);

-- Table Triggers

create trigger t_d_servicio_before_insert before
insert
    on
    vlci2.t_d_servicio for each row execute function fn_aud_fec_ins();
create trigger t_d_servicio_before_update before
update
    on
    vlci2.t_d_servicio for each row execute function fn_aud_fec_upd();

INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('238', 'AI1D0', 'SE.CIUDAD INTELIGENTE Y GESTION DE DATOS', 'SE.CIUD.INTELIG.G.D.', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('999', 'BP730', 'SE.RECURSOS CULTURALES', 'SE.RECURSOS CULTURAL', '1B');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('999', 'AA770', 'SE.ALCALDIA', 'SE.ALCALDIA', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('206', 'OF001', 'OF. DE BIENESTAR ANIMAL', 'OF.BIENESTAR ANIMAL', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('235', 'OE470', 'SE.INSP.TRIBUTOS Y RENTAS', 'SE.INSP.TRIBUTOS Y R', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('204', 'SC100', 'SE.PERSONAL', 'SE.PERSONAL', '1S');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('260', 'A7790', 'SERV.ORGANO DE APOYO JGL Y CONC.SEC(SGAM', 'SE.ORG.APOYO JGL Y C', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('222', 'WK890', 'SE.CEMENTERIOS Y SERV.FUNERARIOS', 'SE.CEMENTERIOS Y SER', '1W');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('999', 'BP260', 'SE.DE ACCIÃN CULTURAL', 'SE.DE ACCIÃN CULTURA', '1B');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('204', 'SC1E0', 'SE.SALUD LABORAL', 'SE.SALUD LABORAL', '1S');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('999', 'BP180', 'SE. NORMALIZACIÃN LINGÃÃSTICA', 'SE. NORMALIZACIÃN LI', '1B');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('299', 'VL920', 'SE.DE PLAYAS', 'SE.DE PLAYAS', '1V');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('210', 'TD670', 'DPT.BOMBEROS, PREVENC. E INT.EMERG Y PRO', 'DPT.BOMBEROS, PREVEN', '1T');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('235', 'OE460', 'SE.TESORERÃA', 'SE.TESORERÃA', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('206', 'OF1R0', 'SE. REGISTRO Y POBLACIÃN', 'SE. REGISTRO Y POBLA', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('235', 'OE540', 'SE.ECONOMICO-PRESUPUESTAR', 'SE.ECONOMICO-PRESUPU', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('235', 'OE1Q0', 'SE.ADMTVO.DE LA INTERVENCIÃN GRAL.MPAL.', 'SE.ADMTVO.DE LA INTE', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('235', 'OE420', 'SE.CONTABILIDAD', 'SE.CONTABILIDAD', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('238', 'AI008', 'OF.ESTADÃSTICA', 'OF.ESTADÃSTICA', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('235', 'OE860', 'SE.FINANCIERO', 'SE.FINANCIERO', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('235', 'OE500', 'SE.G.TRIB.ESPEC.-ACT.EC.', 'SE.G.TRIB.ESPEC.-ACT', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('208', 'OU130', 'SE.DESCENTRALIZACIÃN Y PARTICIPACIÃN CIU', 'SE.DESCENTRALIZACIÃN', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('208', 'OU970', 'SE.DE PEDANÃAS', 'SE.DE PEDANÃAS', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('221', 'WJ300', 'SE. LIMPIEZA Y RECOGIDA DE RESIDUOS', 'SE. LIMPIEZA Y RECOG', '1W');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('205', 'SD110', 'SE.ARQUITECTURA Y SERVICIOS CENTRALES TÃ', 'SE.ARQUITECTURA Y SE', '1S');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('225', 'UC320', 'SE.PLANEAMIENTO', 'SE.PLANEAMIENTO', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('294', 'A5000', 'ASESORIA JURIDICA MUNICIPAL', 'ASESORIA JURIDICA MP', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('241', 'SY1S0', 'SE.RESPONSABILIDAD PATRIMONIAL', 'SE.RESPONSABILIDAD P', '1S');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('269', 'UH220', 'SE.INSPECCIÃN MUNICIPAL', 'SE.INSPECCIÃN MUNICI', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('207', 'VG009', 'SERVICIO DE AGRICULTURA', 'SE.DE AGRICULTURA', '1V');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('238', 'AI080', 'SE.TECNOLOG.INF.Y COMUNIC', 'SE.TECNOLOG.INF.Y CO', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('253', 'TJ160', 'SE.MOVILIDAD', 'SE.MOVILIDAD', '1T');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('250', 'TN240', 'SE.OCUPAC.DOMIN.PÃBL.MPAL', 'SE.OCUPAC.DOMIN.PÃBL', '1T');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('236', 'RF650', 'SE.DE EMPLEO Y FORMACIÃN', 'SE.DE EMPLEO Y FORMA', '1R');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('235', 'OE1H0', 'SE.COORDINACIÃN TRIBUTARIA', 'SE.COORDINACIÃN TRIB', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('219', 'XH200', 'SE. SANIDAD Y CONSUMO', 'SE. SANIDAD Y CONSUM', '1X');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('237', 'TB520', 'SE.COMERCIO Y MERCADOS', 'SE.COMERCIO Y MERCAD', '1T');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('238', 'AI001', 'OF. INVERSIONES E INTERNACIONALIZACIÃN', 'OF. INVERSIONES E IN', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('234', 'AK740', 'SE. TURISMO', 'SE. TURISMO', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('238', 'AI640', 'SE.INNOVACIÃN', 'SE.INNOVACIÃN', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('238', 'AI1O0', 'SE.PROYECTOS EUROPEOS', 'SE.PROYECTOS EUROPEO', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('999', 'BP630', 'SERV. DE BANDA SIMFONICA', 'SERV. DE BANDA SIMFO', '1B');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('212', 'YC1V0', 'SE.ATENCIÃN PRIMARIA SERV.SOCIALES', 'SE.ATENC.PRIM.S.SOC.', '1Y');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('214', 'BE280', 'SERV.EDUCACIÃN', 'SE.EDUCACIÃN', '1B');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('265', 'XK550', 'SERV.DE MAYORES', 'SE.DE MAYORES', '1X');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('212', 'YC150', 'SE.ATENCIÃN ESPECIALIZADA SERVIC.SOCIAL.', 'SE.ATENC.ESPEC.S.SOC', '1Y');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('258', 'RC1K0', 'SERV. DE EMPRENDIMIENTO', 'SE.EMPRENDIMIENTO', '1R');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('215', 'XF580', 'SERV.FIESTAS Y TRADICIONES', 'SE.FIESTAS Y TRADIC.', '1X');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('204', 'SC090', 'EXTINT SR.AVALUAC.SR.I PERS.I GEST.QUAL', 'EXTINT SR.AVALUAC.SR', '1S');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('226', 'UD660', 'SE.VIVIENDA', 'SE.VIVIENDA', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('225', 'UC570', 'SE.ASESORAMIENTO URBANÃSTICO', 'SE.ASESORAMIENTO URB', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('225', 'UC330', 'SE.GESTIÃN URBANÃSTICA', 'SE.GESTIÃN URBANÃSTI', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('225', 'UC380', 'SE.DISCIPLINA URBANÃSTICA', 'SE.DISCIPLINA URBANÃ', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('241', 'SY510', 'SE.DE PATRIMONIO', 'SE.DE PATRIMONIO', '1S');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('223', 'VP760', 'SE.DEVESA-ALBUFERA', 'SE.DEVESA-ALBUFERA', '1V');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('1A', 'A01U0', 'SE. PROTECCIÃN DE DATOS Y PERSONAS', 'SE. PROTECCIÃN DE DA', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('269', 'UH390', 'SE. LICENCIAS DE ACTIVIDADES', 'SE. LICENCIAS DE ACT', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('202', 'AG530', 'SERV. COMUNICACIÃN', 'SE.COMUNICACIÃN', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('270', 'XA720', 'SE. JUVENTUD E INFANCIA', 'SE. JUVENTUD E INFAN', '1X');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('224', 'WU290', 'SE.CICLO INTEGRAL DEL AGUA', 'SE.CICLO INTEGRAL DE', '1W');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('211', 'TE140', 'SE.POLICÃA LOCAL', 'SE.POLICÃA LOCAL', '1T');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('235', 'OE960', 'SE.GEST.EMISIÃN.Y RECAUD.', 'SE.GEST.EMISIÃN.Y RE', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('232', 'BJ700', 'SE.DEPORTES', 'SE.DEPORTES', '1B');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('212', 'YC800', 'SE. ADICCIONES', 'SE. ADICCIONES', '1Y');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('225', 'UC620', 'SE.GEST.CENTRO HISTÃRICO', 'SE.GEST.CENTRO HISTÃ', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('225', 'UC340', 'SE.PROYECTOS URBANOS', 'SE.PROYECTOS URBANOS', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('206', 'OF690', 'SE.SOCIEDAD DE LA INFORMACIÃN ,TRANSPA.I', 'SE.SOCIEDAD DE LA IN', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('259', 'YD610', 'SE. IGUALDAD', 'SE. IGUALDAD', '1Y');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('235', 'OE480', 'SE.G.TRIBUTARIA ESP.CAT.', 'SE.G.TRIBUTARIA ESP.', '1O');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('999', 'UG230', 'SE.OBRAS Y MANTENIMIENTO DE INFRAESTRUCT', 'SE.OBRAS Y MANTENIMI', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('290', 'A1780', 'SERV.SECRETARIA GENERAL Y DEL P', 'SE.SECR.GRAL.PLE', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('295', 'P61F0', 'SERV.SECRETARIA DEL JURADO TRIBUTARIO', 'SE.SECRET.JURADO TRI', '1P');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('269', 'UH360', 'SE.LICENCIAS URBANÃSTICAS', 'SE.LICENCIAS URBANÃS', '1U');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('999', 'BP250', 'SE.PATRIM.HTCO.ARTÃSTICO', 'SE.PATRIM.HTCO.ARTÃS', '1B');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('276', 'BQ580', 'SE.FIESTAS Y TRADICIONES-FALLAS', 'SE.FIESTAS T-FALLAS', '1B');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('999', 'AA020', 'SE.PROTOCOLO', 'SE.PROTOCOLO', '1A');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('217', 'VD310', 'SE.PARQUES Y JARDINES', 'SE.PARQUES Y JARDINE', '1V');
INSERT INTO vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, fk_area_id) VALUES('999', 'WV1T0', 'SE. MEJORA CLIMÃTICA', 'SE. MEJORA CLIMÃTICA', '1W');

---- Se restauran las delegaciones 

DROP TABLE vlci2.t_d_delegacion;

CREATE TABLE vlci2.t_d_delegacion (
	pk_delegacion_id varchar(11) NULL,
	fk_area_id varchar(11) NULL,
	desc_corta_cas varchar(120) NULL,
	desc_corta_val varchar(120) NULL,
	desc_larga_cas varchar(200) NULL,
	desc_larga_val varchar(200) NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(45) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(45) NULL
);

create trigger t_d_delegacion_before_insert before
insert
    on
    vlci2.t_d_delegacion for each row execute function fn_aud_fec_ins();
create trigger t_d_delegacion_before_update before
update
    on
    vlci2.t_d_delegacion for each row execute function fn_aud_fec_upd();

INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('226', '1U', 'DELEGACIÓN DE VIVIENDA', 'DELEGACIÓ D''HABITATGE', 'DELEGACIÓN DE VIVIENDA', 'DELEGACIÓ D''HABITATGE');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('999', '999', 'NO CONSTA', 'NO CONSTA', 'NO CONSTA', 'NO CONSTA');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('223', '1V', 'DELEGACIÓN DE DEVESA-ALBUFERA', 'DELEGACIÓ DE DEVESA-ALBUFERA', 'DELEGACIÓN DE DEVESA-ALBUFERA', 'DELEGACIÓ DE DEVESA-ALBUFERA');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('206', '1O', 'DELEGACIÓN DE TRANSPARENCIA, INFORMACIÓN Y DEFENSA DE LA CIUDADANÍA', 'DELEGACIÓ DE TRANSPARÈNCIA, INFORMACIÓ I DEFENSA DE LA CIUTADANIA', 'DELEGACIÓN DE TRANSPARENCIA, INFORMACIÓN Y DEFENSA DE LA CIUDADANÍA', 'DELEGACIÓ DE TRANSPARÈNCIA, INFORMACIÓ I DEFENSA DE LA CIUTADANIA');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('224', '1W', 'DELEGACIÓN DE CICLO INTEGRAL DEL AGUA', 'DELEGACIÓ DE CICLE INTEGRAL DE L''AIGUA', 'DELEGACIÓN DE CICLO INTEGRAL DEL AGUA', 'DELEGACIÓ DE CICLE INTEGRAL DE L''AIGUA');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('276', '1B', 'DELEGACIÓN DE FALLAS', 'DELEGACIÓ DE FALLES', 'DELEGACIÓN DE FALLAS', 'DELEGACIÓ DE FALLES');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('222', '1W', 'DELEGACIÓN DE CEMENTERIOS Y SERVICIOS FUNERARIOS', 'DELEGACIÓ DE CEMENTERIS I SERVICIS FUNERARIS', 'DELEGACIÓN DE CEMENTERIOS Y SERVICIOS FUNERARIOS', 'DELEGACIÓ DE CEMENTERIS I SERVICIS FUNERARIS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('241', '1S', 'DELEGACIÓN DE PATRIMONIO', 'DELEGACIÓ DE PATRIMONI', 'DELEGACIÓN DE PATRIMONIO', 'DELEGACIÓ DE PATRIMONI');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('236', '1R', 'DELEGACIÓN DE EMPLEO Y FORMACIÓN', 'DELEGACIÓ D''OCUPACIÓ I FORMACIÓ', 'DELEGACIÓN DE EMPLEO Y FORMACIÓN', 'DELEGACIÓ D''OCUPACIÓ I FORMACIÓ');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('202', '1A', 'DELEGACIÓN DE COMUNICACIÓN Y RELACIONES CON LOS MEDIOS', 'DELEGACIÓ DE COMUNICACIÓ I RELACIONS AMB ELS MITJANS', 'DELEGACIÓN DE COMUNICACIÓN Y RELACIONES CON LOS MEDIOS', 'DELEGACIÓ DE COMUNICACIÓ I RELACIONS AMB ELS MITJANS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('265', '1X', 'DELEGACIÓN DE MAYORES', 'DELEGACIÓ DE MAJORS', 'DELEGACIÓN DE MAYORES', 'DELEGACIÓ DE MAJORS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('237', '1T', 'DELEGACIÓN DE COMERCIO Y MERCADOS', 'DELEGACIÓ DE COMERÇ I MERCATS', 'DELEGACIÓN DE COMERCIO Y MERCADOS', 'DELEGACIÓ DE COMERÇ I MERCATS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('260', '260', 'TITULAR ORGANO DE APOYO A LA JGL', 'TITULAR ORGAN DE SUPORT A JGL', 'TITULAR ORGANO DE APOYO A LA JGL', 'TITULAR ORGAN DE SUPORT A JGL');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('215', '1X', 'DELEGACIÓN DE FIESTAS Y TRADICIONES', 'DELEGACIÓ DE FESTES I TRADICIONS', 'DELEGACIÓN DE FIESTAS Y TRADICIONES', 'DELEGACIÓ DE FESTES I TRADICIONS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('207', '1V', 'DELEGACIÓN DE AGRICULTURA', 'DELEGACIÓ D''AGRICULTURA', 'DELEGACIÓN DE AGRICULTURA', 'DELEGACIÓ D''AGRICULTURA');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('299', '1V', 'DELEGACIÓN DE PLAYAS', 'DELEGACIÓ DE PLATGES', 'DELEGACIÓN DE PLAYAS', 'DELEGACIÓ DE PLATGES');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('250', '1T', 'DELEGACIÓN DE ESPACIO PÚBLICO', 'DELEGACIÓ D''ESPAI PÚBLIC', 'DELEGACIÓN DE ESPACIO PÚBLICO', 'DELEGACIÓ D''ESPAI PÚBLIC');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('259', '1Y', 'DELEGACIÓN DE IGUALDAD', 'DELEGACIÓ D''IGUALTAT', 'DELEGACIÓN DE IGUALDAD', 'DELEGACIÓ D''IGUALTAT');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('205', '1S', 'DELEGACIÓN DE SERVICIOS CENTRALES TÉCNICOS', 'DELEGACIÓ DE SERVEIS CENTRALS TÈCNICS', 'DELEGACIÓN DE SERVICIOS CENTRALES TÉCNICOS', 'DELEGACIÓ DE SERVEIS CENTRALS TÈCNICS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('238', '1A', 'DELEGACIÓN DE INNOVACIÓN, TECNOLOGÍA, AGENDA DIGITAL Y CAPTACIÓN DE INVERSIONES', 'DELEGACIÓ D''INNOVACIÓ, TECNOLOGIA, AGENDA DIGITAL I CAPTACIÓ D''INVERSIONS', 'DELEGACIÓN DE INNOVACIÓN, TECNOLOGÍA, AGENDA DIGITAL Y CAPTACIÓN DE INVERSIONES', 'DELEGACIÓ D''INNOVACIÓ, TECNOLOGIA, AGENDA DIGITAL I CAPTACIÓ D''INVERSIONS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('210', '1T', 'DELEGACIÓN DE PREVENCIÓN Y EXTINCIÓN DE INCENDIOS', 'DELEGACIÓ DE PREVENCIÓ I EXTINCIÓ D''INCENDIS', 'DELEGACIÓN DE PREVENCIÓN Y EXTINCIÓN DE INCENDIOS', 'DELEGACIÓ DE PREVENCIÓ I EXTINCIÓ D''INCENDIS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('217', '1V', 'DELEGACIÓN DE PARQUES Y JARDINES', 'DELEGACIÓ DE PARCS I JARDINS', 'DELEGACIÓN DE PARQUES Y JARDINES', 'DELEGACIÓ DE PARCS I JARDINS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1A', '1A', 'ÁREA DE ALCALDÍA', 'ÀREA D''ALCALDIA', 'ÁREA DE ALCALDÍA', 'ÀREA D''ALCALDIA');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('221', '1W', 'DELEGACIÓN DE LIMPIEZA Y RECOGIDA DE RESIDUOS', 'DELEGACIÓ DE NETEJA I RECOLLIDA DE RESIDUS', 'DELEGACIÓN DE LIMPIEZA Y RECOGIDA DE RESIDUOS', 'DELEGACIÓ DE NETEJA I RECOLLIDA DE RESIDUS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('253', '1T', 'DELEGACIÓN DE MOVILIDAD', 'DELEGACIÓ DE MOBILITAT', 'DELEGACIÓN DE MOVILIDAD', 'DELEGACIÓ DE MOBILITAT');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('211', '1T', 'DELEGACIÓN DE POLICÍA LOCAL DE VALENCIA', 'DELEGACIÓ DE POLICIA LOCAL DE VALÈNCIA', 'DELEGACIÓN DE POLICÍA LOCAL DE VALENCIA', 'DELEGACIÓ DE POLICIA LOCAL DE VALÈNCIA');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('225', '1U', 'DELEGACIÓN DE PLANIFICACIÓN Y GESTIÓN URBANA', 'DELEGACIÓ DE PLANIFICACIÓ I GESTIÓ URBANA', 'DELEGACIÓN DE PLANIFICACIÓN Y GESTIÓN URBANA', 'DELEGACIÓ DE PLANIFICACIÓ I GESTIÓ URBANA');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('212', '1Y', 'DELEGACIÓN DE SERVICIOS SOCIALES', 'DELEGACIÓ DE SERVEIS SOCIALS', 'DELEGACIÓN DE SERVICIOS SOCIALES', 'DELEGACIÓ DE SERVEIS SOCIALS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('234', '1A', 'DELEGACIÓN DE TURISMO', 'DELEGACIÓ DE TURISME', 'DELEGACIÓN DE TURISMO', 'DELEGACIÓ DE TURISME');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('270', '1X', 'DELEGACIÓN DE FAMILIA, JUVENTUD E INFANCIA', 'DELEGACIÓ DE FAMÍLIA, JOVENTUT I INFÀNCIA', 'DELEGACIÓN DE FAMILIA, JUVENTUD E INFANCIA', 'DELEGACIÓ DE FAMÍLIA, JOVENTUT I INFÀNCIA');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('294', '1A', 'ASESORIA JURIDICA MUNICIPAL', 'ASSESSORIA JURIDICA MUNICIPAL', 'ASESORIA JURIDICA MUNICIPAL', 'ASSESSORIA JURIDICA MUNICIPAL');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('290', '1A', 'SECRETARIA GENERAL Y DEL PLENO', 'SECRETARIA GENERAL I DEL PLE', 'SECRETARIA GENERAL Y DEL PLENO', 'SECRETARIA GENERAL I DEL PLE');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('208', '1O', 'DELEGACIÓN DE PEDANÍAS, PARTICIPACIÓN Y ACCIÓN VECINAL', 'DELEGACIÓ DE PEDANIES, PARTICIPACIÓ I ACCIÓ VEÏNAL', 'DELEGACIÓN DE PEDANÍAS, PARTICIPACIÓN Y ACCIÓN VECINAL', 'DELEGACIÓ DE PEDANIES, PARTICIPACIÓ I ACCIÓ VEÏNAL');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('258', '1R', 'DELEGACIÓN DE EMPRENDIMIENTO', 'DELEGACIÓ D''EMPRENEDORIA', 'DELEGACIÓN DE EMPRENDIMIENTO', 'DELEGACIÓ D''EMPRENEDORIA');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('295', '1P', 'JURADO TRIBUTARIO', 'JURAT TRIBUTARI', 'JURADO TRIBUTARIO', 'JURAT TRIBUTARI');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('204', '1S', 'DELEGACIÓN DE RECURSOS HUMANOS', 'DELEGACIÓ DE RECURSOS HUMANS', 'DELEGACIÓN DE RECURSOS HUMANOS', 'DELEGACIÓ DE RECURSOS HUMANS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('232', '1B', 'DELEGACIÓN DE DEPORTES', 'DELEGACIÓ D''ESPORTS', 'DELEGACIÓN DE DEPORTES', 'DELEGACIÓ D''ESPORTS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('235', '1O', 'DELEGACIÓN DE HACIENDA Y PRESUPUESTOS', 'DELEGACIÓN DE HACIENDA Y PRESUPUESTOS', 'DELEGACIÓN DE HACIENDA Y PRESUPUESTOS', 'DELEGACIÓN DE HACIENDA Y PRESUPUESTOS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('269', '1U', 'DELEGACIÓN DE LICENCIAS URBANÍSTICAS-ACTIVIDADES', 'DELEGACIÓ DE LLICÈNCIES URBANÍSTIQUES-ACTIVITATS', 'DELEGACIÓN DE LICENCIAS URBANÍSTICAS-ACTIVIDADES', 'DELEGACIÓ DE LLICÈNCIES URBANÍSTIQUES-ACTIVITATS');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('219', '1X', 'DELEGACIÓN DE SANIDAD Y CONSUMO', 'DELEGACIÓ DE SANITAT I CONSUM', 'DELEGACIÓN DE SANIDAD Y CONSUMO', 'DELEGACIÓ DE SANITAT I CONSUM');
INSERT INTO vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('214', '1B', 'DELEGACIÓN DE EDUCACIÓN', 'DELEGACIÓ D''EDUCACIÓ', 'DELEGACIÓN DE EDUCACIÓN', 'DELEGACIÓ D''EDUCACIÓ');


-- Se restauran las areas

DROP TABLE vlci2.t_d_area;

CREATE TABLE vlci2.t_d_area (
	pk_area_id varchar(11) NULL,
	desc_corta_cas varchar(120) NULL,
	desc_corta_val varchar(120) NULL,
	desc_larga_cas varchar(200) NULL,
	desc_larga_val varchar(200) NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(45) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(45) NULL
);

create trigger t_d_area_before_insert before
insert
    on
    vlci2.t_d_area for each row execute function fn_aud_fec_ins();
create trigger t_d_area_before_update before
update
    on
    vlci2.t_d_area for each row execute function fn_aud_fec_upd();


INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1O', 'ÁREA DE HACIENDA Y PARTICIPACIÓN', 'ÀREA D''HISENDA I PARTICIPACIÓ', 'ÁREA DE HACIENDA Y PARTICIPACIÓN', 'ÀREA D''HISENDA I PARTICIPACIÓ');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('999', 'NO CONSTA', 'NO CONSTA', 'NO CONSTA', 'NO CONSTA');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1A', 'ÁREA DE ALCALDÍA', 'ÀREA D''ALCALDIA', 'ÁREA DE ALCALDÍA', 'ÀREA D''ALCALDIA');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1B', 'ÁREA DE CULTURA, EDUCACIÓN, DEPORTES Y FALLAS', 'ÀREA DE CULTURA, EDUCACIÓ, ESPORTS I FALLES', 'ÁREA DE CULTURA, EDUCACIÓN, DEPORTES Y FALLASS', 'ÀREA DE CULTURA, EDUCACIÓ, ESPORTS I FALLES');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1S', 'ÁREA DE PATRIMONIO, RECURSOS HUMANOS Y TÉCNICOS', 'ÀREA DE PATRIMONI, RECURSOS HUMANS I TÈCNICS', 'ÁREA DE PATRIMONIO, RECURSOS HUMANOS Y TÉCNICOS', 'ÀREA DE PATRIMONI, RECURSOS HUMANS I TÈCNICS');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1T', 'ÁREA DE SEGURIDAD Y MOVILIDAD', 'ÀREA DE SEGURETAT I MOBILITAT', 'ÁREA DE SEGURIDAD Y MOVILIDAD', 'ÀREA DE SEGURETAT I MOBILITAT');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1R', 'ÁREA DE EMPLEO, FORMACIÓN Y EMPRENDIMIENTO', 'ÀREA D''OCUPACIÓ, FORMACIÓ I EMPRENEDORIA', 'ÁREA DE EMPLEO, FORMACIÓN Y EMPRENDIMIENTO', 'ÀREA D''OCUPACIÓ, FORMACIÓ I EMPRENEDORIA');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1P', 'PLENO MUNICIPAL', 'PLE MUNICIPAL', 'PLENO MUNICIPAL', 'PLE MUNICIPAL');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1W', 'ÁREA DE RESIDUOS, MEJORA CLIMÁTICA Y GESTIÓN DEL AGUA', 'ÀREA DE RESIDUS, MILLORA CLIMÀTICA I GESTIÓ DE L''AIGUA', 'ÁREA DE RESIDUOS, MEJORA CLIMÁTICA Y GESTIÓN DEL AGUA', 'ÀREA DE RESIDUS, MILLORA CLIMÀTICA I GESTIÓ DE L''AIGUA');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1V', 'ÁREA DE PARQUES, JARDINES Y ESPACIOS NATURALES', 'ÀREA DE PARCS, JARDINS I ESPAIS NATURALS', 'ÁREA DE PARQUES, JARDINES Y ESPACIOS NATURALES', 'ÀREA DE PARCS, JARDINS I ESPAIS NATURALS');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1U', 'ÁREA DE URBANISMO, VIVIENDA Y LICENCIAS', 'ÀREA D''URBANISME, HABITATGE I LLICÈNCIES', 'ÁREA DE URBANISMO, VIVIENDA Y LICENCIAS', 'ÀREA D''URBANISME, HABITATGE I LLICÈNCIES');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1X', 'ÁREA DE FAMILIA, MAYORES Y TRADICIONES', 'ÀREA DE FAMÍLIA, MAJORS I TRADICIONS', 'ÁREA DE FAMILIA, MAYORES Y TRADICIONES', 'ÀREA DE FAMÍLIA, MAJORS I TRADICIONS');
INSERT INTO vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val) VALUES('1Y', 'ÁREA DE BIENESTAR SOCIAL', 'ÀREA DE BENESTAR SOCIAL', 'ÁREA DE BIENESTAR SOCIAL', 'ÀREA DE BENESTAR SOCIAL');


COMMIT;
