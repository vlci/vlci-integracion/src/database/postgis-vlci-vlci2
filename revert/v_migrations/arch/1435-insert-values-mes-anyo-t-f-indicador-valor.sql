-- Revert postgis-vlci-vlci2:v_migrations/1435-insert-values-mes-anyo-t-f-indicador-valor from pg

with rows_to_update as (
    select * from vlci2.t_f_indicador_valor
    where (mes is not null or not anyo is null)
        and fk_periodicidad_muestreo = 6
        and fk_indicador in (1139, 1130, 1111, 1122, 1124, 1143))
update vlci2.t_f_indicador_valor 
set mes = null,
    anyo = null
from rows_to_update
WHERE t_f_indicador_valor.fk_indicador = rows_to_update.fk_indicador
    and t_f_indicador_valor.fk_unidad_medida = rows_to_update.fk_unidad_medida
    and t_f_indicador_valor.fk_periodicidad_muestreo = rows_to_update.fk_periodicidad_muestreo
    AND t_f_indicador_valor.dc_periodicidad_muestreo_valor = rows_to_update.dc_periodicidad_muestreo_valor
    AND t_f_indicador_valor.dc_criterio_desagregacion1_valor = rows_to_update.dc_criterio_desagregacion1_valor;

