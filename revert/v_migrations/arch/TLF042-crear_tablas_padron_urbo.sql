-- Revert postgis-vlci-vlci2:v_migrations/TLF042-crear_tablas_padron_urbo from pg

BEGIN;

drop table t_datos_manual_padron_edad;
drop table t_datos_manual_padron_estudios;
drop table t_datos_manual_padron_nacimientos;
drop table t_datos_manual_padron_nacionalidad;

COMMIT;
