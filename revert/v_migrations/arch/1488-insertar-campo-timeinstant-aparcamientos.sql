-- Revert postgis-vlci-vlci2:v_migrations/1488-insertar-campo-timeinstant-aparcamientos from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos DROP COLUMN TimeInstant;

COMMIT;
