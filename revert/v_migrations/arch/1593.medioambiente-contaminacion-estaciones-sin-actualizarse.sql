-- Revert postgis-vlci-vlci2:v_migrations/1593.medioambiente-contaminacion-estaciones-sin-actualizarse from pg

BEGIN;

INSERT INTO t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,fen_inicio,fen_fin,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (209,'A11_PATRAIX_10m','DIEZMINUTAL',0,'2023-03-26 04:20:00',NULL,NULL,'ERROR','YYYYMMddHH24MISS','2022-02-15 13:25:20.174814','VLCI_ETL_MEDIOAMBIENTE_AIRE_10','2023-03-28 12:10:47.992385','p_gf_registrarEstadoETL'),
	 (227,'A11_PATRAIX_60m','HORARIA',0,'2023-03-26 05:00:00',NULL,NULL,'ERROR','YYYYMMddHH24MISS','2022-08-02 12:48:07.026035','VLCI_ETL_MEDIOAMBIENTE_AIRE_60','2023-03-28 11:20:20.051216','p_gf_registrarEstadoETL');

COMMIT;
