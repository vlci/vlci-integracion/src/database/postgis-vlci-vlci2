-- Revert postgis-vlci-vlci2:v_migrations/TLF008-modificar_parkingspot_lastdata from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_parkingspot_lastdata DROP COLUMN maintenanceowner;
ALTER TABLE vlci2.t_datos_cb_parkingspot_lastdata DROP COLUMN project;

COMMIT;
