-- Revert postgis-vlci-vlci2:v_migrations/2078-alter-etl-sonometros-zas-table from pg

BEGIN;

ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN laeq TYPE numeric(5, 2) USING laeq::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN laeqreal TYPE numeric(5, 2) USING laeqreal::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN lafmax TYPE numeric(5, 2) USING lafmax::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN lafmaxreal TYPE numeric(5, 2) USING lafmaxreal::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN lafmin TYPE numeric(5, 2) USING lafmin::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN lafminreal TYPE numeric(5, 2) USING lafminreal::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week RENAME COLUMN laieq TO laleq;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN laleq TYPE numeric(5, 2) USING laleq::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week RENAME COLUMN laieqreal TO laleqreal;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN laleqreal TYPE numeric(5, 2) USING laleqreal::numeric;

COMMIT;
