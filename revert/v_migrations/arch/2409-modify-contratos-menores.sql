-- Revert postgis-vlci-vlci2:v_migrations/2409-modify-contratos-menores from pg

BEGIN;

DROP INDEX IF EXISTS unique_contrato;
ALTER TABLE vlci2.t_datos_etl_contratos_menores ALTER COLUMN fecha_carga DROP DEFAULT;

COMMIT;