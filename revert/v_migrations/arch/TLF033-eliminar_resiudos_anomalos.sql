-- Revert postgis-vlci-vlci2:v_migrations/TLF033-eliminar_resiudos_anomalos from pg
BEGIN;

INSERT INTO
    vlci2.t_datos_cb_wastecontainer_lastdata (
        recvtime,
        fiwareservicepath,
        entitytype,
        entityid,
        timeinstant,
        fillinglevel,
        maintenanceowner,
        "location",
        temperature,
        project
    )
VALUES
    (
        '2023-02-15 12:50:42.246',
        '/residuos',
        'WasteContainer',
        'wastecontainer:5e7cf89c0b790dfd57ad3dd3',
        NULL,
        NULL,
        'Wellness ',
        'POINT (-0.4060356 39.5373401)' :: public.geometry,
        NULL,
        NULL
    ),
    (
        '2023-02-15 12:50:44.580',
        '/residuos',
        'WasteContainer',
        'wastecontainer:602179604be006adb7765683',
        NULL,
        NULL,
        'Wellness ',
        'POINT (-0.323325216 39.3532881)' :: public.geometry,
        NULL,
        NULL
    ),
    (
        '2023-02-15 12:52:07.970',
        '/residuos',
        'WasteContainer',
        'wastecontainer:6213b9af720d9dfe994b715e',
        NULL,
        NULL,
        'Wellness ',
        'POINT (-0.364426 39.484643)' :: public.geometry,
        NULL,
        NULL
    );

COMMIT;