-- Revert postgis-vlci-vlci2:v_migrations/1541-modificar-pk-tabla-aparcamientos from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos
DROP CONSTRAINT t_datos_cb_trafico_aparcamientos_pkey,
ADD PRIMARY KEY (entityid);

COMMIT;
