-- Revert postgis-vlci-vlci2:v_migrations/258.delete_t_d_fecha_negocio_etls_procedures from pg

BEGIN;

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id, etl_nombre, periodicidad, "offset", fen, fen_inicio, fen_fin, estado, formato_fen, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd) VALUES
    (108, 'VLCI_VALENCIA_CdMGE_CALL_PROCEDURE_PMP_SERV_SEMESTRAL', 'SEMESTRAL', 0, NULL, '2021-07-01 00:00:00.000', '2022-01-01 00:00:00.000', 'OK', 'YYYY-MM-dd', '2019-06-25 09:36:48.000', 'etl call procedure', '2021-07-26 22:25:14.000', 'p_gf_calcularNuevaFecha'),
    (109, 'VLCI_VALENCIA_CdMGE_CALL_PROCEDURE_PMP_SERV_ANUAL', 'ANUAL', 0, NULL, '2021-07-01 00:00:00.000', '2022-01-01 00:00:00.000', 'OK', 'YYYY-MM-dd', '2019-06-25 09:36:48.000', 'etl call procedure', '2021-01-21 22:22:06.000', 'p_gf_calcularNuevaFecha');
     		                                                    		                            	                                                	                                            
COMMIT;
