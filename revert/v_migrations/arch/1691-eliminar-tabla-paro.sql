-- Revert postgis-vlci-vlci2:v_migrations/1691-eliminar-tabla-paro from pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_estadistica_paro (
	entityid varchar NOT NULL,
	entitytype varchar NOT NULL,
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar NOT NULL,
	calculationperiod varchar NULL,
	calculationperioddescription varchar NULL,
	calculationperioddescriptioncas varchar NULL,
	kpivalue int4 NULL,
	sliceanddice1 varchar NULL,
	sliceanddice1cas varchar NULL,
	sliceanddicevalue1 varchar NULL,
	sliceanddicevalue1cas varchar NULL,
	updatedat timestamp NULL,
	description varchar NULL,
	descriptioncas varchar NULL,
	"name" varchar NULL,
	namecas varchar NULL,
	measureunit varchar NULL,
	aud_user_ins varchar(45) NOT NULL,
	aud_fec_ins timestamp NOT NULL,
	aud_fec_upd timestamp NOT NULL,
	aud_user_upd varchar(45) NOT NULL,
	timeinstant timestamp NOT NULL,
	CONSTRAINT t_datos_cb_estadistica_paro_pkey PRIMARY KEY (recvtime, fiwareservicepath, entitytype, entityid)
);

-- Table Triggers

create trigger t_datos_cb_estadistica_paro_before_insert before
insert
    on
    vlci2.t_datos_cb_estadistica_paro for each row execute function fn_aud_fec_ins();
create trigger t_datos_cb_estadistica_paro_update before
update
    on
    vlci2.t_datos_cb_estadistica_paro for each row execute function fn_aud_fec_upd();

COMMIT;
