-- Revert postgis-vlci-vlci2:v_migrations/68425.migracion_agregados_nuevas_tablas_auxiliares from pg

BEGIN;

DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker;
DROP TABLE IF EXISTS vlci2.t_x_indicadores_silencio;

COMMIT;
