-- Revert postgis-vlci-vlci2:v_migrations/3134_New2_Modificar_Tabla_consumos from pg

BEGIN;

DROP TABLE IF EXISTS vlci2.t_datos_cb_energia_datadis_consumos_municipales_diarios;


CREATE TABLE vlci2.t_datos_cb_energia_datadis_consumos_municipales_diarios (
    recvTime TEXT NOT NULL,
    entityId TEXT NOT NULL,
    entityType TEXT NOT NULL,
    fiwareServicePath TEXT NOT NULL,
    address TEXT NOT NULL,
    obtainMethod TEXT NULL,
    calculationperiod TEXT NOT NULL,
    kpivalue NUMERIC NOT NULL,
    sliceanddicevalue1 TEXT NOT NULL,
    originalservicepath TEXT NOT NULL,
    originalentitytype TEXT NOT NULL,
    originalentityid TEXT NOT null,
    PRIMARY KEY (sliceanddicevalue1, calculationperiod, originalentityid)
);

COMMIT;
