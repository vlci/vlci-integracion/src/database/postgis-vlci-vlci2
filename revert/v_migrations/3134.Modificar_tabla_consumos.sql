-- Revert postgis-vlci-vlci2:v_migrations/3134.Modificar_tabla_consumos from pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_energia_consumos_municipales_diarios (
    address TEXT NOT NULL,
    obtainMethod TEXT NULL,
    calculationperiod TEXT NOT NULL,
    kpivalue NUMERIC NOT NULL,
    sliceanddicevalue1 TEXT NOT NULL,
    originalservicepath TEXT NOT NULL,
    originalentitytype TEXT NOT NULL,
    originalentityid TEXT NOT NULL, 
    PRIMARY KEY (sliceanddicevalue1, calculationperiod, originalentityid)
);

DROP TABLE IF EXISTS vlci2.t_datos_cb_energia_datadis_consumos_municipales_diarios;

COMMIT;
