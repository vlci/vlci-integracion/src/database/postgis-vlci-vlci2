-- Revert postgis-vlci-vlci2:v_migrations/3199-Mover-vlci2-tablas-t_d_accesos_camaras-t_d_vias_camaras from pg

BEGIN;

DROP TABLE IF EXISTS vlci2.t_d_vias_camaras;
DROP TABLE IF EXISTS vlci2.t_d_accesos_camaras;


-- Step 1: Create the table
CREATE TABLE cdmt.t_d_accesos_camaras (
    idt_camaras_acceso INT PRIMARY KEY,
    tramo VARCHAR(255),
    tramo_camara VARCHAR(255),
    camara INT,
    aud_user_ins VARCHAR(50),
    aud_fec_ins TIMESTAMP,
    aud_user_upd VARCHAR(50),
    aud_fec_upd TIMESTAMP
);

-- Step 2: Insert the data
INSERT INTO cdmt.t_d_accesos_camaras (idt_camaras_acceso, tramo, tramo_camara, camara, aud_user_ins, aud_fec_ins, aud_user_upd, aud_fec_upd)
VALUES
(14, 'Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)', 'Pont de Picanya (Arxiduc Carles)', 2702, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(15, 'Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)', 'Cº.Nº. Picanya (CV-36)', 4303, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(16, 'Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)', 'V-21 (Port Saplaya)', 2405, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(17, 'Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)', 'Av. Catalunya (V-21)', 14102, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(18, 'Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)', 'Av. Catalunya (V-21)', 2302, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(19, 'Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)', 'Ausias March (V-31)', 12104, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(20, 'Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)', 'Ausias March (Peris i Valero)', 12102, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(21, 'Av. del Cid (Entre V-30 i Tres Creus)', 'Pont de Xirivella (A-3)', 4803, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(22, 'Av. del Cid (Entre V-30 i Tres Creus)', 'Av. del Cid (Nou d''Octubre)', 4802, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(23, 'Av. del Cid (Entre V-30 i Tres Creus)', 'Av. del Cid (A-3)', 1705, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(24, 'Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)', 'Corts Valencianes (CV-35)', 7302, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(25, 'Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)', 'Corts Valencianes (PÍO XII)', 7303, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(26, 'Prolongació Joan XXIII (Entre Germans Machado i Salvador Cerveró)', 'Germans Machado (CV-30)', 11102, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000');


-- Step 1: Create the table if it does not exist
CREATE TABLE cdmt.t_d_vias_camaras (
    idt_camaras_vias INT PRIMARY KEY,
    tramo VARCHAR(255),
    tramo_camara VARCHAR(255),
    camara INT,
    aud_user_ins VARCHAR(50),
    aud_fec_ins TIMESTAMP,
    aud_user_upd VARCHAR(50),
    aud_fec_upd TIMESTAMP
);

-- Step 2: Insert the data
INSERT INTO cdmt.t_d_vias_camaras
(idt_camaras_vias, tramo, tramo_camara, camara, aud_user_ins, aud_fec_ins, aud_user_upd, aud_fec_upd)
VALUES
(16, 'Av. de Giorgeta (Entre Sant Vicent i Jesús)', 'Giorgeta (Pérez Galdós)', 1105, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(17, 'Av. de Giorgeta (Entre Sant Vicent i Jesús)', 'Giorgeta (Peris i Valero)', 1202, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(18, 'Gran Via del Marqués del Túria (Entre Pont d''Aragó i Hernan Cortés)', 'Marqués del Túria (Germanies)', 6505, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(19, 'Gran Via del Marqués del Túria (Entre Pont d''Aragó i Hernan Cortés)', 'Marqués del Túria - Comte de Salvatierra (Germanies)', 6506, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(20, 'Gran Via del Marqués del Túria (Entre Pont d''Aragó i Hernan Cortés)', 'Marqués del Túria - Ciscar (Pont d''Aragó)', 6507, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(21, 'Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)', 'Dr. Peset Aleixandre (Primat Reig)', 11002, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(22, 'Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)', 'Dr. Peset Aleixandre (General Avilés)', 4703, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(23, 'Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)', 'F. Catòlic - Petxina d''eixida (Pl. Espanya)', 1003, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(24, 'Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)', 'F.Catòlic [Entrada] - Quart (Pl. Espanya)', 201, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(25, 'Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)', 'F.Catòlic [Eixida] - Quart (Pl. Espanya)', 202, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(26, 'Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)', 'F. Catòlic Entrada (Pius XII)', 902, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(27, 'Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)', 'F. Catòlic d''eixida (Pius XII)', 907, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(28, 'Av. de Peris i Valero (Entre Ausiàs March i Sapadors)', 'Peris i Valero (Eduard Boscá)', 2604, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(29, 'Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)', 'Blasco Ibáñez (General Elio)', 2303, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000'),
(30, 'Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)', 'Blasco Ibáñez (Av. Suècia)', 2304, 'Julia', '2020-04-17 07:45:10.000', 'Julia', '2020-04-17 07:45:10.000');



COMMIT;
