-- Revert postgis-vlci-vlci2:v_migrations/3230.Insert_historico_policia_2024 from pg

BEGIN;

delete from vlci2.t_datos_cb_policia_intervenciones_lastdata
where dateobserved >= '2024-01-01' and dateobserved <= '2024-10-31 14:30:01.000';

COMMIT;
