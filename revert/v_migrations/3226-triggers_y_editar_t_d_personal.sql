-- Revert postgis-vlci-vlci2:v_migrations/3226-triggers_y_editar_t_d_personal from pg

BEGIN;

ALTER TABLE vlci2.t_d_personal
    ALTER COLUMN aud_fec_ins TYPE varchar USING aud_fec_ins::varchar,
    ALTER COLUMN aud_fec_upd TYPE varchar USING aud_fec_upd::varchar,
    ALTER COLUMN aud_user_ins TYPE timestamp USING aud_user_ins::timestamp,
    ALTER COLUMN aud_user_upd TYPE timestamp USING aud_user_upd::timestamp;

DROP TRIGGER IF EXISTS vlci2_t_d_personal_ins ON vlci2.t_d_personal;
DROP TRIGGER IF EXISTS vlci2_t_d_personal_upd ON vlci2.t_d_personal;

COMMIT;
