-- Revert postgis-vlci-vlci2:v_migrations/3135.Eliminar-registros-pm25Viveros-SO2Centro from pg

BEGIN;

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET so2value = 4,
    so2valueflag = 'V'
WHERE dateobserved BETWEEN '2021-07-18 00:00:00.000' AND '2023-12-31 23:59:59.999'
  AND refpointofinterest = 'A07_VALENCIACENTRE';

COMMIT;
