-- Revert postgis-vlci-vlci2:v_migrations/3100.Eliminar-registros-base-datos from pg

BEGIN;
UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET pm10value = 21,
    pm10valueflag = 'V'
WHERE dateobserved > '2020-03-01 00:00:00.000' and refpointofinterest  = 'A02_BULEVARDSUD';

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET pm10value = 3,
    pm10valueflag = 'V'
WHERE dateobserved > '2020-03-01 00:00:00.000'and dateobserved < '2020-04-26 00:00:00.000'  and refpointofinterest  = 'A06_VIVERS';

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET pm10value = 14,
    pm10valueflag = 'V'
WHERE dateobserved > '2020-04-26 00:00:00.000'and dateobserved < '2020-12-21 00:00:00.000'  and refpointofinterest  = 'A06_VIVERS';

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET pm25value = 19,
    pm25valueflag = 'V'
WHERE dateobserved > '2021-04-22 00:00:00.000' and refpointofinterest  = 'A02_BULEVARDSUD';

COMMIT;
