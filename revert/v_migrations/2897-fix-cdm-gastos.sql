-- Revert postgis-vlci-vlci2:v_migrations/2897-fix-cdm-gastos from pg

BEGIN;

update vlci2.t_d_area 
	set desc_corta_cas=null,
	desc_corta_val=null,
	desc_larga_cas=null,
	desc_larga_val=null
where anyo = 2024
	and mes = 06;
	
update vlci2.t_d_delegacion 
	set desc_corta_cas = null,
	desc_corta_val  = null,
	desc_larga_cas  = null,
	desc_larga_val  = null
where anyo = 2024
	and mes = 06;

COMMIT;
