-- Revert postgis-vlci-vlci2:v_migrations/1391.add-column-locationForGISVisualization from pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata DROP COLUMN locationforgisvisualization;

COMMIT;
