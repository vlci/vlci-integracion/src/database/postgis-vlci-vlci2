-- Revert postgis-vlci-vlci2:v_migrations/3226-edicion-columnas-personal from pg

BEGIN;

ALTER TABLE vlci2.t_datos_etl_personal_ayuntamiento
DROP COLUMN IF EXISTS grupo,
DROP COLUMN IF EXISTS subgrupo,
DROP COLUMN IF EXISTS area_adagp_extra,
DROP COLUMN IF EXISTS area_gip,
DROP COLUMN IF EXISTS delegacion_adagp,
DROP COLUMN IF EXISTS delegacion_gip,
DROP COLUMN IF EXISTS desc_provision_puesto,
DROP COLUMN IF EXISTS negociado_adagp,
DROP COLUMN IF EXISTS negociado_gip,
DROP COLUMN IF EXISTS organo_adagp,
DROP COLUMN IF EXISTS organo_gip,
DROP COLUMN IF EXISTS posicion_gip,
DROP COLUMN IF EXISTS puesto_gip,
DROP COLUMN IF EXISTS seccion_adagp,
DROP COLUMN IF EXISTS seccion_gip,
DROP COLUMN IF EXISTS servicio_adagp,
DROP COLUMN IF EXISTS servicio_gip;

ALTER TABLE vlci2.t_datos_etl_personal_ayuntamiento
ADD COLUMN vinc_laboral VARCHAR; 


COMMIT;
