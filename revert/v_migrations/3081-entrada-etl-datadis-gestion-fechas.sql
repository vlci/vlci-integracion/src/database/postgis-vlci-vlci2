-- Revert postgis-vlci-vlci2:v_migrations/3081-entrada-etl-datadis-gestion-fechas from pg

BEGIN;

DELETE FROM vlci2.t_d_fecha_negocio_etls
WHERE etl_id = 303 
AND etl_nombre = 'py_energia_extraccion_api_datadis_consumos_municipales';

COMMIT;
