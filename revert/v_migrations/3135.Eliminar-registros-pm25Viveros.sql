-- Revert postgis-vlci-vlci2:v_migrations/3135.Eliminar-registros-pm25Viveros from pg

BEGIN;

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET pm25value = 11,
    pm25valueflag = 'V'
WHERE dateobserved BETWEEN '2020-04-26 00:00:00.000' AND '2023-12-31 23:59:59.999'
  AND refpointofinterest = 'A06_VIVERS';

COMMIT;
