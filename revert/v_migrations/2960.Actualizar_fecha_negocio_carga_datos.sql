-- Revert postgis-vlci-vlci2:v_migrations/2960.Actualizar_fecha_negocio_carga_datos from pg

BEGIN;

UPDATE t_datos_sql_personal_carga_excel
SET fecha_negocio = '2023-12-31';

COMMIT;
