DO $$
DECLARE
	current_db varchar(200);
	current_user varchar(200);
BEGIN
	-- Data
	truncate table vlci2.t_d_parametros_etl_carga;

	SELECT current_database()
	INTO current_db;

	SELECT CURRENT_USER usr
	INTO current_user;

	-- Base de datos LOCAL, su usuario debe ser 'postgres'
	IF (current_user = 'postgres') THEN

		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_MOBILIS','HTTP','HTTP',NULL,NULL,'/integracion/01.fichOrig/EMT_Mobilis/','/integracion/02.fichBackup/EMT_Mobilis/','http://www.emtvalencia.es/ciudadano/google_transit/datos/carga','mobilis',NULL,'txt',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2021-08-10 08:20:29.88841','ETL Loader','2021-07-27 13:55:20',NULL,29),
			('VLCI_ETL_EMT_MOBILIS_CARGAREFERENCIAS','HTTP','HTTP',NULL,NULL,'/integracion/01.fichOrig/EMT_Mobilis_Transit/','/integracion/02.fichBackup/EMT_Mobilis_Transit/','http://www.emtvalencia.es/ciudadano/google_transit/google_transit.zip','google_transit',NULL,'zip',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2021-08-10 08:20:29.80237','ETL Loader','2021-07-27 13:55:20',NULL,27),
			('VLCI_ETL_EMT_MOBILIS_CARGATITULOS','HTTP','HTTP',NULL,NULL,'/integracion/01.fichOrig/EMT_Mobilis_Titulos/','/integracion/02.fichBackup/EMT_Mobilis_Titulos/','http://www.emtvalencia.es/ciudadano/google_transit/datos/titulos.txt','titulos',NULL,'txt',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2021-08-10 08:20:29.845153','ETL Loader','2021-07-27 13:55:20',NULL,28),
			('CDMGE GACO','FTP','FTP Sertic','SMC-GACO-*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/SIEM/Integradores','/integracion/01.fichOrig/CDMGE_GACO/','/integracion/02.fichBackup/CDMGE_GACO/',NULL,'SMC-GACO-_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.404222',NULL,'2021-06-23 11:03:57',NULL,18),
			('CDMGE Codigo Organico','FTP','FTP Sertic','SMC-TBOR-*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/SIEM/Integradores','/integracion/01.fichOrig/CDMGE_TBOR/','/integracion/02.fichBackup/CDMGE_TBOR/',NULL,'SMC-TBOR-_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.446839',NULL,'2021-06-23 11:03:42',NULL,19),
			('ETL VLCi IBI','FTP','FTP Sertic','Informacion_tributaria_IBI_*','//DADES2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/Integradores','/integracion/01.fichOrig/VLCi_IBI/','/integracion/02.fichBackup/VLCi_IBI/',NULL,'Informacion_tributaria_IBI_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.489668','ETL Loader','2021-07-27 13:55:20',NULL,20),
			('MESAS SILLAS','FTP','FTP Sertic','MESAS_Y_SILLAS_*','//dades2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/Integradores','/integracion/01.fichOrig/VLCi_MESAS_SILLAS/','/integracion/02.fichBackup/VLCi_MESAS_SILLAS/',NULL,'MESAS_Y_SILLAS_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.532625',NULL,'2021-07-27 13:55:20',NULL,21),
			('VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS','FTP','FTP Sertic','*listadoUnidadesAdministrativas.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/Integradores','/integracion/01.fichOrig/VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS/','/integracion/02.fichBackup/VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS/',NULL,'*listadoUnidadesAdministrativas.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.575779',NULL,'2021-07-12 08:49:19',NULL,22),
			('VLCi_CDMGE_CONTRATACION','FTP','FTP Sertic','*listado_expedientes_contratacion.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/contratacion/INT','/integracion/01.fichOrig/VLCi_CONTRATACION/','/integracion/02.fichBackup/VLCi_CONTRATACION/',NULL,'*listado_expedientes_contratacion.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.618861','ETL Loader','2023-11-22 11:03:50','Cecilia - 2108', NULL);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_REALIZADAS','FTP','FTP Sertic','*_tareasRealizadas.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/Integradores','/integracion/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/integracion/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_tareasRealizadas.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.661792','ETL Loader','2021-06-23 11:03:30',NULL,24),
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_PENDIENTES','FTP','FTP Sertic','*_tareasPendientes.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/Integradores','/integracion/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/integracion/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_tareasPendientes.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.704898','ETL Loader','2021-06-23 11:03:30',NULL,25),
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_USUARIOS','FTP','FTP Sertic','*_usuariosPorUnidad.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/Integradores','/integracion/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/integracion/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_usuariosPorUnidad.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.747808','ETL Loader','2021-06-23 11:03:30',NULL,26),
			('VLCi Sertic Red Pagos','FTP','FTP Sertic','*.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integradores','/integracion/01.fichOrig/VLCi_Sertic_Red_Pagos/','/integracion/02.fichBackup/VLCi_Sertic_Red_Pagos/',NULL,'yyyy.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.275703','ETL Loader','2021-07-27 13:55:20',NULL,15),
			('ETL VLCi Vados','FTP','FTP Sertic','Informacion_tributaria_Vados_*','//dades2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/Integradores','/integracion/01.fichOrig/VLCi_Vados/','/integracion/02.fichBackup/VLCi_Vados/',NULL,'Informacion_tributaria_Vados_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.36129','ETL Loader','2021-07-27 13:55:20',NULL,17);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_VALENCIA_ETL_IVTM','FTP','FTP Sertic','Informacion_tributaria_IVTM*','//dades2/dades2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/Integradores','/integracion/01.fichOrig/VLCI_VALENCIA_ETL_IVTM/','/integracion/02.fichBackup/VLCI_VALENCIA_ETL_IVTM/',NULL,'Informacion_tributaria_IVTM*','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:31.373547','ETL Loader','2021-07-27 13:55:20',NULL,64),
			('VLCi_Valencia_ETL_IAE','FTP','FTP Sertic','Informacion_tributaria_IAE_*.txt','//dades2/dades2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/Integradores','/integracion/01.fichOrig/IAE/','/integracion/02.fichBackup/IAE/',NULL,'Informacion_tributaria_IAE_yyyy.txt','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.709989','ETL Loader','2021-07-27 13:55:20',NULL,1),
			('VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL','FTP','FTP Sertic','residuos_oci.xlsx','//dades1/dades1/ayun/Residuos/LIMPIEZA/isabel/00 OCI/Integradores','/integracion/01.fichOrig/VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL/','/integracion/02.fichBackup/VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL/',NULL,'residuos_oci.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:33.618966','Ticket-45251','2021-07-27 13:55:20',NULL,NULL),
			('VLCI_ETL_SERTIC_SEDE_SWEB1','FTP','FTP Sertic','access.log-*','//soes-aux1/FTPVOL/LogsWeb/slogserver1/','/integracion/01.fichOrig/VLCi_Sertic_SedeSWeb1/','/integracion/02.fichBackup/VLCi_Sertic_SedeSWeb1/',NULL,'access.log-*','N','xz',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:33.228868','Ticket-39365','2021-07-27 13:55:20',NULL,155),
			('VLCI_ETL_JARDINERIA_IND_INVENTARIO','FTP','FTP Sertic','JARDINES*','//DADES2/DADES2/ayun/JARDINERIA/JardineriaVLCi/Integradores','/integracion/01.fichOrig/Jardines_Inventario/','/integracion/02.fichBackup/Jardines_Inventario/',NULL,'JARDINES','N','mdb',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:32.144823','Ivan 31459','2021-07-27 13:55:20','Ivan 31459',85),
			('VLCI_ETL_COORDVIAPUB_IND_INCIDENCIAS_SIGO','FTP','FTP Sertic','INCIDENCIAS*','//dades1/DADES1/ayun/OCOVAL/Integradores','/integracion/01.fichOrig/OCOVAL/','/integracion/02.fichBackup/OCOVAL/',NULL,'INCIDENCIAS.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:32.102146','Toni 28621','2021-07-27 13:55:20','Toni 28621',84);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Valencia_ETL_PMP_Facturas','FTP','FTP Sertic','PMP_UA_*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/integracion/','/integracion/01.fichOrig/PMP_facturas/','/integracion/02.fichBackup/PMP_facturas/',NULL,'PMP_UA_yyyy.txt','N','zip',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.754693','ETL Loader','2021-07-27 13:55:20',NULL,2),
			('ETL_Trafico_aparcamientos','FTP','FTP Sertic','Consulta aparcamientos por tipo y plazas.xls','//dades2/DADES2/ayun/TRAFICO/Plataforma VLCi/Integradores','/integracion/01.fichOrig/VLCi_Trafico_Aparcamientos/','/integracion/02.fichBackup/VLCi_Trafico_Aparcamientos/',NULL,'Consulta aparcamientos por tipo y plazas.xls','N','xls',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.840985','ETL Loader','2021-07-27 13:55:20',NULL,4),
			('ETL_VLCi_CDMGE_PMP_ENTIDAD','FTP','FTP Sertic','Evolucion_PMP.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/Integradores','/integracion/01.fichOrig/VLCi_CDMGE_PMP_ENTIDAD/','/integracion/02.fichBackup/VLCi_CDMGE_PMP_ENTIDAD/',NULL,'Evolucion_PMP.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.969863','ETL Loader','2021-07-12 08:49:29',NULL,7),
			('ETL VLCi CDMGE PMP','FTP','FTP Sertic','PMP_Resumen_Uds_Administrativas_*.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/Integradores','/integracion/01.fichOrig/VLCi_CDMGE_PMP/','/integracion/02.fichBackup/VLCi_CDMGE_PMP/',NULL,'PMP_Resumen_Uds_Administrativas_yyyy.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.060836','ETL Loader','2021-07-12 08:49:37',NULL,10);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A8',NULL,'csv','Indicador IC110A8.csv','WITH SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T56143.CODPAIS as c4,      case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  as c5,      T56143.DPAIS as c6,      T6789.CODIGO as c7,      T18685.ANYO as c33 from       V_LU_GEO_PAIS_NACION T56143,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T56143.IDIOMA and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T17978.DTBA = T18685.DISBAR and T18685.CODPOS = T21622.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18685.PAINACDAD = T56143.CODPAIS and T18685.PAINACDAD <> 108 and T56143.CODPAIS <> 108)  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T56143.CODPAIS, T56143.DPAIS, case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end, T18685.ANYO ) select 0 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c1 ||'';''||      D1.c6 ||'';''||      case  when row_number() OVER (PARTITION BY D1.c3 ORDER BY D1.c1 DESC) < 7 then row_number() OVER (PARTITION BY D1.c3 ORDER BY D1.c1 DESC) else 10 end  ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH0 D1  order by D1.c2, D1.c7','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.242798','Lozano','2021-08-26 08:08:43.185664',NULL,61),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC015A1',NULL,'csv','Indicador IC015A1.csv','with pob as ( select SUM(T21230.NUMPERSONAS) personas, T7594.ANO ano from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Valenciano'' and T10746.CODIGO = T21230.SEXO )  GROUP BY T7594.ANO ), cons as ( select sum(T60833.VALOR) consumo, T7594.ANO ano from       LU_VIII_IBER_TIPOENERGIA_IDI T61176,      LU_VIII_IBER_TIPCONS_IDI T33126,      LU_VIII_IBER_INDICADOR_IDI T33120,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_IBER_DATOSANUAL T60833 where  ( T6789.CODIGO = T61176.IDIOMA and T6789.CODIGO = T33120.IDIOMA and T33120.CODIGO = T60833.INDICADOR and T7594.ANO = T60833.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T60833.MUNICIPIO and T6789.CODIGO = T33126.IDIOMA and T33126.CODIGO = T60833.TIPCONS and T6789.DENOMINACION = ''Valenciano'' and T33120.CODIGO = 1 and T33126.CODIGO = 1 and T60833.TIPENERGIA = T61176.CODIGO and T61176.CODIGO = 1 )  GROUP BY  T7594.ANO  ) select ((d1.consumo/p1.personas) / 12.0)*1000 ||'';''|| p1.ano ano from pob p1, cons d1 where p1.ano = d1.ano','consumo_anual,ano','N','sftp',NULL,NULL,'2021-08-10 08:20:30.059754','Lozano','2021-07-27 13:55:20',NULL,35),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC019A1',NULL,'csv','Indicador IC019A1.csv','with pob as (  select SUM(T21230.NUMPERSONAS) personas, T7594.ANO ano from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Valenciano'' and T10746.CODIGO = T21230.SEXO ) GROUP BY T7594.ANO ),  cons as (select sum(T60833.VALOR) consumo, T7594.ANO ano from       LU_VIII_IBER_TIPOENERGIA_IDI T61176,      LU_VIII_IBER_TIPCONS_IDI T33126,      LU_VIII_IBER_INDICADOR_IDI T33120,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_IBER_DATOSANUAL T60833 where  ( T6789.CODIGO = T61176.IDIOMA and T6789.CODIGO = T33120.IDIOMA and T33120.CODIGO = T60833.INDICADOR and T7594.ANO = T60833.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T60833.MUNICIPIO and T6789.CODIGO = T33126.IDIOMA and T33126.CODIGO = T60833.TIPCONS and T6789.DENOMINACION = ''Valenciano''  and T33120.CODIGO = 1 and T33126.CODIGO <> 9 and T60833.TIPENERGIA = T61176.CODIGO and T61176.CODIGO = 1 )  GROUP BY  T7594.ANO  ) select ((d1.consumo/p1.personas) / 365.0)*1000 ||'';''|| p1.ano ano from pob p1, cons d1 where p1.ano = d1.ano','consumo_anual,ano','N','sftp',NULL,NULL,'2021-08-10 08:20:30.102884','Lozano','2021-07-27 13:55:20',NULL,36),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC001A1',NULL,'csv','Indicador IC001A1.csv','WITH  SAWITH0 AS (select sum(T10607.VALOR) as c1,      T10686.CODIGO as c2,      T10686.DENOMINACION as c3,      T10783.ANO as c4,      T10783.TRIMESTRE as c5,      T10783.ANOTRIMESTRE as c6,      T6789.CODIGO as c7 from       LU_VII_EPAV_EDADAGR2_IDI T10680,      LU_VII_EPAV_INDICADOR_IDI T10686,      LU_VII_EPAV_METODO_IDI T10692,      V_LU_TIEMPO_ANOTRIMESTRE T10783,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_VII_EPAV_SEXOAGREDAD2 T10607 where  ( T6789.CODIGO = T10680.IDIOMA and T6789.CODIGO = T10686.IDIOMA and T10607.EDADAGR2 = T10680.CODIGO and T6789.CODIGO = T10692.IDIOMA and T10607.INDICADOR = T10686.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10607.METODO = T10692.CODIGO and T10607.ANO = T10783.ANO and T10607.SEXO = T10746.CODIGO and T6789.DENOMINACION = ''Castellano'' and T10607.EDADAGR2 = 9 and T10607.INDICADOR = 26 and T10607.METODO = 4 and T10607.TRIMESTRE = T10783.TRIMESTRE  and T10607.SEXO = 9  and T10680.CODIGO = 9 and T10686.CODIGO = 26 and T10692.CODIGO = 4 and T10746.CODIGO = 9  )  group by T6789.CODIGO, T6789.DENOMINACION, T10686.CODIGO, T10686.DENOMINACION, T10783.ANO, T10783.ANOTRIMESTRE, T10783.TRIMESTRE), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c5 as c6,      D1.c1 as c7,      D1.c6 as c8,      D1.c7 as c9 from       SAWITH0 D1), SAWITH2 AS (select T58019.DENOMINACION as c1,      T58019.CODIGO as c2,      T58019.IDIOMA as c3 from       LU_TRIMESTRE_IDI T58019 /* LU_TRIMESTRE_LOOKUP */ ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D1.c4 as c4,                D2.c1 as c5,                D1.c5 as c6,                D1.c6 as c7,                D1.c7 as c8,                D1.c8 as c9,                D1.c9 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c8, D1.c9, D2.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c8 ASC, D1.c9 ASC, D2.c1 ASC) as c11           from                 SAWITH1 D1 inner join SAWITH2 D2 On D1.c5 = D2.c2 and D1.c9 = D2.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 as c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH3 D1 order by c1, c4, c6, c7, c2, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:30.146152','Lozano','2021-07-27 13:55:20',NULL,37),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC101A1',NULL,'csv','Indicador IC101A1.csv','WITH  SAWITH0 AS (select sum(T13116.VALOR) as c1,      T73182.CODIGO as c2,      T73182.DENOMINACION as c3,      T13200.CODIGO as c4,      T13200.DENOMINACION as c5,      T13212.CODIGO as c6,      T6789.DENOMINACION as c7,      T7595.ANO as c8,      T7595.MES as c9,      T7595.ANOMES as c10,      T6789.CODIGO as c11,      T7595.ANO as c33 from       LU_VI_EOH_TIPEST_IDI T73182,      LU_VI_EOH_RESIDENCIA_IDI T13212,      LU_VI_EOH_INDICADORDEM_IDI T13200,      V_LU_TIEMPO_ANOMES T7595,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VI_EOH_DEMTURMUNI T13116 where  ( T6789.CODIGO = T73182.IDIOMA and T6789.CODIGO = T13200.IDIOMA and T13116.INDICADOR = T13200.CODIGO and T6789.CODIGO = T13212.IDIOMA and T13116.RESIDENCIA = T13212.CODIGO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T13116.MUNICIPIO and T7595.ANO = T13116.ANO and T7595.MES = T13116.MES and T6789.DENOMINACION = ''Castellano'' and T6910.CODMUNICIPIO = 46250 and T13116.INDICADOR = 2 and T13116.MUNICIPIO = 46250 and T13116.RESIDENCIA = 9 and T13116.TIPEST = T73182.CODIGO and T13200.CODIGO = 2 and T13212.CODIGO = 9 and T13116.INDICADOR <> 3 and T13200.CODIGO <> 3 )  group by T6789.CODIGO, T6789.DENOMINACION, T7595.ANO, T7595.MES, T7595.ANOMES, T13200.CODIGO, T13200.DENOMINACION, T13212.CODIGO, T73182.CODIGO, T73182.DENOMINACION), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c9 as c10,      D1.c1 as c11,      D1.c10 as c12,      D1.c11 as c13,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T57997.DENOMINACION as c1,      T57997.CODIGO as c2,      T57997.IDIOMA as c3 from       LU_MES_IDI T57997 /* LU_MES_LOOKUP */ ), SAWITH3 AS (select D1.c1 as c1,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c14 as c14,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c4 as c4,                D1.c5 as c5,                D1.c6 as c6,                D1.c7 as c7,                D1.c8 as c8,                D2.c1 as c9,                D1.c9 as c10,                D1.c10 as c11,                D1.c11 as c12,                D1.c12 as c13,                D1.c13 as c14,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c9, D1.c10, D1.c12, D1.c13, D2.c1,D1.c33 ORDER BY D1.c2 ASC,  D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c9 ASC, D1.c10 ASC, D1.c12 ASC, D1.c13 ASC, D2.c1 ASC) as c15           from                 SAWITH1 D1 inner join SAWITH2 D2 On D1.c9 = D2.c2 and D1.c13 = D2.c3      ) D1 where  ( D1.c15 = 1 ) ) select D1.c1 ||'';''||  D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      sum(D1.c12) as c12,      D1.c33 as c33 from       SAWITH3 D1 group by c1,c4,c5,c6,c7,c8,c9,c10,c11,c33 order by c1, c7, c8, c11, c10, c4, c6,  c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.19047','Lozano','2021-07-27 13:55:20',NULL,38),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152',NULL,'csv','Indicador IC152.csv','WITH  SAWITH0 AS (select count(T101251.ID) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';'' || D1.c3||'';'' || D1.c4||'';'' || D1.c5||'';'' || D1.c6||'';'' || D1.c7||'';''|| D1.c8||'';''|| D1.c9||'';''|| D1.c10||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.234049','Lozano','2021-07-27 13:55:20',NULL,39),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A1',NULL,'csv','Indicador IC152A1.csv','WITH  SAWITH0 AS (select sum(T101251.SUPPARCELA) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';'' || D1.c3||'';'' || D1.c4||'';'' || D1.c5||'';'' || D1.c6||'';'' || D1.c7||'';''|| D1.c8||'';''|| D1.c9||'';''|| D1.c10||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.277606','Lozano','2021-07-27 13:55:20',NULL,40),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A2',NULL,'csv','Indicador IC152A2.csv','WITH  SAWITH0 AS (select sum(T101251.SUPCONSTR) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end , T101251.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select   	       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10, 	 D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.321098','Lozano','2021-07-27 13:55:20',NULL,41);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A3',NULL,'csv','Indicador IC152A3.csv','WITH  SAWITH0 AS (select sum(T101251.VALORINVENT) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.366692','Lozano','2021-07-27 13:55:20',NULL,42),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A1',NULL,'csv','Indicador IC153A1.csv','WITH  SAWITH0 AS (select count(T16529.ORDNUM) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T16638.CODIGO as c4,      T16638.DENOMINACION as c5,      T16701.CODIGO as c6,      T16701.DENOMINACION as c7,      T16722.CODIGO as c8,      T16722.DENOMINACION as c9,      T6789.CODIGO as c10,      T7594.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_POTTU2_IDI T16701,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16701.IDIOMA and T16529.DTBA = T17978.DTBA and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.POT_TU2 = T16701.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16722.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T16638.CODIGO, T16638.DENOMINACION, T16701.CODIGO, T16701.DENOMINACION, T16722.CODIGO, T16722.DENOMINACION, T17978.DTBA, T55787.DT,T7594.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c1 as c10,      D1.c10 as c12,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D2.c1 as c2,      D3.c1 as c3,      D1.c2 as c4,      D1.c3 as c5,      D1.c4 as c6,      D1.c5 as c7,      D1.c6 as c8,      D1.c7 as c9,      D1.c8 as c10,      D1.c9 as c11,      D1.c10 as c12,      D1.c12 as c14,      D1.c33 as c33,      ROW_NUMBER() OVER (PARTITION BY D1.c4, D1.c5, D1.c6, D1.c7, D1.c3, D1.c2, D2.c1, D3.c1, D1.c8, D1.c9,D1.c33 ORDER BY D1.c4 DESC, D1.c5 DESC, D1.c6 DESC, D1.c7 DESC, D1.c3 DESC, D1.c2 DESC, D2.c1 DESC, D3.c1 DESC, D1.c8 DESC, D1.c9 DESC) as c15 from       (           SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c12 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c12 = D3.c3), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c14 as c14,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D1.c4 as c4,                D1.c5 as c5,                D1.c6 as c6,                D1.c7 as c7,                D1.c8 as c8,                D1.c9 as c9,                D1.c10 as c10,                D1.c11 as c11,                D1.c12 as c12,                sum(case D1.c15 when 1 then D1.c12 else NULL end ) over (partition by D1.c6, D1.c7, D1.c8, D1.c9, D1.c5, D1.c4, D1.c2, D1.c3,D1.c2,D1.c33)  as c13,                D1.c14 as c14,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c9, D1.c10, D1.c11, D1.c14, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c9 ASC, D1.c10 ASC, D1.c11 ASC, D1.c14 ASC) as c15           from                 SAWITH4 D1      ) D1 where  ( D1.c15 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4  ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c13 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c4, c2, c5, c3, c6, c7, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.413657','Lozano','2021-07-27 13:55:20',NULL,43),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A2',NULL,'csv','Indicador IC153A2.csv','WITH  SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T17978.DTBA as c2,      T6789.CODIGO as c3,      T6789.DENOMINACION as c4,      T16529.ANO as c33 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T6789.CODIGO = T16638.IDIOMA and T7594.ANO = T16529.ANO and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Valenciano''  and T16529.DTBA = T17978.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T16529.ANO), SAWITH1 AS (select sum(T21230.NUMPERSONAS) as c1,      T17978.DTBA as c2,      T6789.CODIGO as c3,      T6789.DENOMINACION as c4 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T17978.DT = T21230.DIS and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T21230.SEXO and T6789.DENOMINACION = ''Valenciano'' and T17978.DTBA = T21230.DISBAR and T21230.ANYO = 2016 and T21230.ANYO = T24742.ANO and T21230.DIA = T24742.DIADELMES and T21230.MES = T24742.MES and T24742.ANO = 2016 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      cast(D1.c1 * 100 / nullif( D2.c1, 0) as  DOUBLE PRECISION  ) as c3,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c4,      D1.c33 as c33 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c3 = D2.c3 and D1.c2 = D2.c2 and  SYS_OP_MAP_NONNULL(D1.c4) = SYS_OP_MAP_NONNULL(D2.c4) ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D1.c3 as c4,                D1.c4 as c5,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c4, D2.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c4 ASC, D2.c1 ASC) as c6           from                 SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c4 = D2.c3      ) D1 where  ( D1.c6 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c3, c2 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.45704','Lozano','2021-07-27 13:55:20',NULL,44),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A3',NULL,'csv','Indicador IC153A3.csv','WITH  SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T6789.CODIGO as c4, 	 T7594.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_TIPODNI_IDI T16713,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16713.IDIOMA and T16529.DTBA = T17978.DTBA and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.TIPODNI = T16713.CODIGO and T6789.DENOMINACION = ''Castellano''  and T16529.TIPODNI = 1 and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16713.CODIGO = 1 and T16722.CODIGO = 1 and T16529.ANYMAT <= 2015 - 11 and T16638.CODIGO <= 2015 - 11 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T55787.DT, T7594.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4,      D1.c4 as c5, 	 D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7, 	 D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7, 			   D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6, 	 D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.501004','Lozano','2021-07-27 13:55:20',NULL,45),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A4',NULL,'csv','Indicador IC153A4.csv','WITH  SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T6789.CODIGO as c4, 	 T7594.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_TIPODNI_IDI T16713,      LU_III_MAT_POTTU2_IDI T16701,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16713.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.CODIGO = T16701.IDIOMA and T16529.POT_TU2 = T16701.CODIGO and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.TIPODNI = T16713.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.TIPODNI = 1 and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16713.CODIGO = 1 and T16722.CODIGO = 1 and T16529.POT_TU2 >= 4 and T16701.CODIGO >= 4 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T55787.DT, T7594.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4,      D1.c4 as c5, 	 D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7, 	 D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7, 			   D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6, 	 D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.544781','Lozano','2021-07-27 13:55:20',NULL,46),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A6',NULL,'csv','Indicador IC153A6.csv','WITH  SAWITH0 AS (select avg(T58565.EDADM) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T6789.CODIGO as c4,      T58565.AÑO as c33 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_III_MAT_EDADM_DTBA T58565 where  ( T7594.ANO = T58565.AÑO and T6789.DENOMINACION = ''Castellano''  and T17978.DTBA = T58565.DTBA  )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T58565.AÑO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4,      D1.c4 as c5,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.588726','Lozano','2021-07-27 13:55:20',NULL,47),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153',NULL,'csv','Indicador IC153.csv','WITH  SAWITH0 AS (select count(T16529.ORDNUM) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T16722.CODIGO as c4,      T16722.DENOMINACION as c5,      T6789.CODIGO as c6,      T16529.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.DENOMINACION = ''Castellano''  and T16529.TIPOFIN = T16722.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T16722.CODIGO, T16722.DENOMINACION, T17978.DTBA, T55787.DT, T16529.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c6, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.631828','Lozano','2021-07-27 13:55:20',NULL,48),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A5',NULL,'csv','Indicador IC153A5.csv','WITH  SAWITH0 AS (select count(T16529.ORDNUM) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T16674.CODIGO as c4,      T16674.DENOMINACION as c5,      T6789.CODIGO as c6,      T16529.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_CILMO2_IDI T16674,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16674.IDIOMA and T16529.CIL_MO2 = T16674.CODIGO and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.DENOMINACION = ''Castellano''  and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 6 and T16722.CODIGO = 6 )  group by T6789.CODIGO, T6789.DENOMINACION, T16674.CODIGO, T16674.DENOMINACION, T17978.DTBA, T55787.DT, T16529.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.675131','Lozano','2021-07-27 13:55:20',NULL,49),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A1',NULL,'csv','Indicador IC006A1.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T54922.CODIGO as c4,      T54922.DENOMINACION as c5,      T6789.CODIGO as c6,       T55155.ANO as c33 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T6789.DENOMINACION = ''Castellano'' and T54922.CODIGO = T55155.GGRUP and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T54922.CODIGO, T54922.DENOMINACION, T55155.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c5, c2, c3, c6, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.718765','Lozano','2021-07-27 13:55:20',NULL,50),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A2',NULL,'csv','Indicador IC006A2.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T55112.DSECCDIV as c4,      T55112.SECCDIV as c5,      T6789.CODIGO as c6,      T55155.ANO as c33 from       V_LU_VI_IAE_RELSECCIONES T55112,      LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 2 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 2 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO), SAWITH1 AS (select count(T55155.NUMREF) as c1 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 2  and T55155.GGRUP = 2 ) ), SAWITH2 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D2.c1 as c7,      D1.c6 as c8,      D1.c33 as c33 from       SAWITH0 D1,      SAWITH1 D2), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c7, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.762029','Lozano','2021-07-27 13:55:20',NULL,51);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A3',NULL,'csv','Indicador IC006A3.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T55112.DSECCDIV as c4,      T55112.SECCDIV as c5,      T6789.CODIGO as c6,      T55155.ANO as c33 from       V_LU_VI_IAE_RELSECCIONES T55112,      LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 4 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 4 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO), SAWITH1 AS (select count(T55155.NUMREF) as c1 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 4  and T55155.GGRUP = 4 ) ), SAWITH2 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D2.c1 as c7,      D1.c6 as c8,      D1.c33 as c33 from       SAWITH0 D1,      SAWITH1 D2), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c7, c6, c4, c2, c3, c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.805391','Lozano','2021-07-27 13:55:20',NULL,52),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A4',NULL,'csv','Indicador IC006A4.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T55112.DSECCDIV as c4,      T55112.SECCDIV as c5,      T6789.CODIGO as c6,      T55155.ANO as c33 from       V_LU_VI_IAE_RELSECCIONES T55112,      LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 5 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 5 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO), SAWITH1 AS (select count(T55155.NUMREF) as c1 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 5  and T55155.GGRUP = 5 ) ), SAWITH2 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D2.c1 as c7,      D1.c6 as c8,      D1.c33 as c33 from       SAWITH0 D1,      SAWITH1 D2), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c7, c6, c4, c2, c3, c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.848579','Lozano','2021-07-27 13:55:20',NULL,53),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A2',NULL,'csv','Indicador IC110A2.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T18894.CODIGO as c4,      T18894.DENOMINACION as c5,      T6789.CODIGO as c6,      T18685.ANYO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T17978.DTBA = T18685.DISBAR and T18685.CODPOS = T21622.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18685.TIPLOC = T18894.CODIGO and T18685.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, T18894.DENOMINACION, T18685.ANYO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.89237','Lozano','2021-07-27 13:55:20',NULL,55),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A3',NULL,'csv','Indicador IC110A3.csv','WITH  SAWITH0 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDAMEN5 = 0 then ''Cap de 0-4'' else ''Algú de 0-4'' end  else case  when T18579.INDAMEN5 = 0 then ''Nadie de 0-4'' else ''Alguien de 0-4'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18579.INDAMEN5 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDAMEN5 = 0 then ''Cap de 0-4'' else ''Algú de 0-4'' end  else case  when T18579.INDAMEN5 = 0 then ''Nadie de 0-4'' else ''Alguien de 0-4'' end  end,T18579.ANO), OBICOMMON0 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), OBICOMMON1 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH1 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH0 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH2 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA0509 = 0 then ''Cap de 5-9'' else ''Algú de 5-9'' end  else case  when T18579.INDA0509 = 0 then ''Nadie de 5-9'' else ''Alguien de 5-9'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano'' and T18579.INDA0509 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA0509 = 0 then ''Cap de 5-9'' else ''Algú de 5-9'' end  else case  when T18579.INDA0509 = 0 then ''Nadie de 5-9'' else ''Alguien de 5-9'' end  end, T18579.ANO ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH4 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA1014 = 0 then ''Cap de 10-14'' else ''Algú de 10-14'' end  else case  when T18579.INDA1014 = 0 then ''Nadie de 10-14'' else ''Alguien de 10-14'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA1014 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA1014 = 0 then ''Cap de 10-14'' else ''Algú de 10-14'' end  else case  when T18579.INDA1014 = 0 then ''Nadie de 10-14'' else ''Alguien de 10-14'' end  end, T18579.ANO ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH4 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH6 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA1519 = 0 then ''Cap de 15-19'' else ''Algú de 15-19'' end  else case  when T18579.INDA1519 = 0 then ''Nadie de 15-19'' else ''Alguien de 15-19'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA1519 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA1519 = 0 then ''Cap de 15-19'' else ''Algú de 15-19'' end  else case  when T18579.INDA1519 = 0 then ''Nadie de 15-19'' else ''Alguien de 15-19'' end  end, T18579.ANO ), SAWITH7 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH6 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH8 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA2024 = 0 then ''Cap de 20-24'' else ''Algú de 20-24'' end  else case  when T18579.INDA2024 = 0 then ''Nadie de 20-24'' else ''Alguien de 20-24'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA2024 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA2024 = 0 then ''Cap de 20-24'' else ''Algú de 20-24'' end  else case  when T18579.INDA2024 = 0 then ''Nadie de 20-24'' else ''Alguien de 20-24'' end  end, T18579.ANO ), SAWITH9 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH8 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH10 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA2529 = 0 then ''Cap de 25-29'' else ''Algú de 25-29'' end  else case  when T18579.INDA2529 = 0 then ''Nadie de 25-29'' else ''Alguien de 25-29'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA2529 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA2529 = 0 then ''Cap de 25-29'' else ''Algú de 25-29'' end  else case  when T18579.INDA2529 = 0 then ''Nadie de 25-29'' else ''Alguien de 25-29'' end  end, T18579.ANO ), SAWITH11 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH10 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH12 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA3034 = 0 then ''Cap de 30-34'' else ''Algú de 30-34'' end  else case  when T18579.INDA3034 = 0 then ''Nadie de 30-34'' else ''Alguien de 30-34'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA3034 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA3034 = 0 then ''Cap de 30-34'' else ''Algú de 30-34'' end  else case  when T18579.INDA3034 = 0 then ''Nadie de 30-34'' else ''Alguien de 30-34'' end  end, T18579.ANO ), SAWITH13 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH12 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH14 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA3539 = 0 then ''Cap de 35-39'' else ''Algú de 35-39'' end  else case  when T18579.INDA3539 = 0 then ''Nadie de 35-39'' else ''Alguien de 35-39'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA3539 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA3539 = 0 then ''Cap de 35-39'' else ''Algú de 35-39'' end  else case  when T18579.INDA3539 = 0 then ''Nadie de 35-39'' else ''Alguien de 35-39'' end  end, T18579.ANO ), SAWITH15 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH14 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH16 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA4044 = 0 then ''Cap de 40-44'' else ''Algú de 40-44'' end  else case  when T18579.INDA4044 = 0 then ''Nadie de 40-44'' else ''Alguien de 40-44'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA4044 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA4044 = 0 then ''Cap de 40-44'' else ''Algú de 40-44'' end  else case  when T18579.INDA4044 = 0 then ''Nadie de 40-44'' else ''Alguien de 40-44'' end  end, T18579.ANO ), SAWITH17 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH16 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH18 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA4549 = 0 then ''Cap de 45-49'' else ''Algú de 45-49'' end  else case  when T18579.INDA4549 = 0 then ''Nadie de 45-49'' else ''Alguien de 45-49'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA4549 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA4549 = 0 then ''Cap de 45-49'' else ''Algú de 45-49'' end  else case  when T18579.INDA4549 = 0 then ''Nadie de 45-49'' else ''Alguien de 45-49'' end  end, T18579.ANO ), SAWITH19 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH18 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH20 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA5054 = 0 then ''Cap de 50-54'' else ''Algú de 50-54'' end  else case  when T18579.INDA5054 = 0 then ''Nadie de 50-54'' else ''Alguien de 50-54'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA5054 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA5054 = 0 then ''Cap de 50-54'' else ''Algú de 50-54'' end  else case  when T18579.INDA5054 = 0 then ''Nadie de 50-54'' else ''Alguien de 50-54'' end  end, T18579.ANO ), SAWITH21 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH20 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH22 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA5559 = 0 then ''Cap de 55-59'' else ''Algú de 55-59'' end  else case  when T18579.INDA5559 = 0 then ''Nadie de 55-59'' else ''Alguien de 55-59'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA5559 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA5559 = 0 then ''Cap de 55-59'' else ''Algú de 55-59'' end  else case  when T18579.INDA5559 = 0 then ''Nadie de 55-59'' else ''Alguien de 55-59'' end  end, T18579.ANO ), SAWITH23 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH22 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH24 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA6064 = 0 then ''Cap de 60-64'' else ''Algú de 60-64'' end  else case  when T18579.INDA6064 = 0 then ''Nadie de 60-64'' else ''Alguien de 60-64'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA6064 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA6064 = 0 then ''Cap de 60-64'' else ''Algú de 60-64'' end  else case  when T18579.INDA6064 = 0 then ''Nadie de 60-64'' else ''Alguien de 60-64'' end  end , T18579.ANO ), SAWITH25 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH24 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH26 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA6569 = 0 then ''Cap de 65-69'' else ''Algú de 65-69'' end  else case  when T18579.INDA6569 = 0 then ''Nadie de 65-69'' else ''Alguien de 65-69'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA6569 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA6569 = 0 then ''Cap de 65-69'' else ''Algú de 65-69'' end  else case  when T18579.INDA6569 = 0 then ''Nadie de 65-69'' else ''Alguien de 65-69'' end  end, T18579.ANO ), SAWITH27 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH26 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH28 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA7074 = 0 then ''Cap de 70-74'' else ''Algú de 70-74'' end  else case  when T18579.INDA7074 = 0 then ''Nadie de 70-74'' else ''Alguien de 70-74'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA7074 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA7074 = 0 then ''Cap de 70-74'' else ''Algú de 70-74'' end  else case  when T18579.INDA7074 = 0 then ''Nadie de 70-74'' else ''Alguien de 70-74'' end  end,T18579.ANO ), SAWITH29 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH28 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH30 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA7579 = 0 then ''Cap de 75-79'' else ''Algú de 75-79'' end  else case  when T18579.INDA7579 = 0 then ''Nadie de 75-79'' else ''Alguien de 75-79'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA7579 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA7579 = 0 then ''Cap de 75-79'' else ''Algú de 75-79'' end  else case  when T18579.INDA7579 = 0 then ''Nadie de 75-79'' else ''Alguien de 75-79'' end  end, T18579.ANO ), SAWITH31 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH30 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH32 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDAMAY80 = 0 then ''Cap major de 80 i més'' else ''Algun major de 80 i més'' end  else case  when T18579.INDAMAY80 = 0 then ''Ningún mayor de 80 y más'' else ''Algún mayor de 80 y más'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDAMAY80 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDAMAY80 = 0 then ''Cap major de 80 i més'' else ''Algun major de 80 i més'' end  else case  when T18579.INDAMAY80 = 0 then ''Ningún mayor de 80 y más'' else ''Algún mayor de 80 y más'' end  end, T18579.ANO ), SAWITH33 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH32 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) (select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH1 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH3 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH5 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH7 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH9 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH11 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH13 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH15 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH17 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH19 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH21 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH23 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH25 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH27 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH29 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH31 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH33 D1)','c1,c2,c3,c4,c5,c6,c7,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.01781','Lozano','2021-07-27 13:55:20',NULL,56),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC145A1',NULL,'csv','Indicador IC145A1.csv','select T6789.DENOMINACION ||'';''||      T8148.CODIGO ||'';''||      T8160.CODIGO ||'';''||      T8160.DENOMINACION ||'';''||      T7595.ANO ||'';''||      T7595.MES ||'';''||      T6789.CODIGO ||'';''||      replace(T8109.VALOR , '','' , ''.'') as c9 from       LU_I_MET_ESTMETEO T8148,      V_LU_TIEMPO_ANOMES T7595,      LU_IDIOMA T6789,      LU_I_MET_INDICADOR_IDI T8160,      FACT_I_MET_METEOANOMES T8109 where  ( T6789.CODIGO = T8160.IDIOMA and T8109.ESTMETEO = T8148.CODIGO and T7595.ANO = T8109.ANO and T7595.MES = T8109.MES and T6789.DENOMINACION = ''Castellano''  and T8109.ESTMETEO = 1 and T8109.INDICADOR = T8160.CODIGO and T8109.INDICADOR = 7 and T8148.CODIGO = 1 and T8160.CODIGO = 7 )','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2021-08-10 08:20:31.285943','Lozano','2021-07-27 13:55:20',NULL,62),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A4',NULL,'csv','Indicador IC110A4.csv','WITH  SAWITH0 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDS0024 = 0 then ''Cap membre de 0-24 anys'' else ''Només membres de 0-24 anys'' end  else case  when T18579.INDS0024 = 0 then ''Ningún miembro de 0-24 años'' else ''Solo miembros de 0-24 años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18579.INDS0024 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDS0024 = 0 then ''Cap membre de 0-24 anys'' else ''Només membres de 0-24 anys'' end  else case  when T18579.INDS0024 = 0 then ''Ningún miembro de 0-24 años'' else ''Solo miembros de 0-24 años'' end  end , T18579.ANO), OBICOMMON0 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), OBICOMMON1 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH1 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1, D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH0 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH2 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDS2564 = 0 then ''Cap membre de 25-64 anys'' else ''Només membres de 25-64 anys'' end  else case  when T18579.INDS2564 = 0 then ''Ningún miembro de 25-64 años'' else ''Solo miembros de 25-64 años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDS2564 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDS2564 = 0 then ''Cap membre de 25-64 anys'' else ''Només membres de 25-64 anys'' end  else case  when T18579.INDS2564 = 0 then ''Ningún miembro de 25-64 años'' else ''Solo miembros de 25-64 años'' end  end, T18579.ANO ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH2 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH4 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY65 = 0 then ''Cap membre de 65 i més anys'' else ''Només membres de 65 i més anys'' end  else case  when T18579.INDSMAY65 = 0 then ''Ningún miembro de 65 y más años'' else ''Solo miembros de 65 y más años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDSMAY65 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY65 = 0 then ''Cap membre de 65 i més anys'' else ''Només membres de 65 i més anys'' end  else case  when T18579.INDSMAY65 = 0 then ''Ningún miembro de 65 y más años'' else ''Solo miembros de 65 y más años'' end  end, T18579.ANO ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH4 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH6 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY80 = 0 then ''Cap membre de 80 i més anys'' else ''Només membres de 80 i més anys'' end  else case  when T18579.INDSMAY80 = 0 then ''Ningún miembro de 80 y más años'' else ''Solo miembros de 80 y más años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDSMAY80 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY80 = 0 then ''Cap membre de 80 i més anys'' else ''Només membres de 80 i més anys'' end  else case  when T18579.INDSMAY80 = 0 then ''Ningún miembro de 80 y más años'' else ''Solo miembros de 80 y más años'' end  end, T18579.ANO ), SAWITH7 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH6 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH8 AS ((select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH1 D1 union select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH3 D1 union select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH5 D1 union select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH7 D1)) select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c33 as c33 from       SAWITH8 D1 order by c1, c2, c3, c4, c5','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.067184','Lozano','2021-07-27 13:55:20',NULL,57),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A5',NULL,'csv','Indicador IC110A5.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T10746.CODIGO as c4,      T10746.DENOMINACION as c5,      case  when T19562.CODIGO < 19 then T19562.CODIGO else 19 end  as c6,      case  when T19562.CODIGO < 19 then T19562.DENOMINACION else case  when T6789.DENOMINACION = ''Castellano'' then ''90 y más'' else ''90 i més'' end  end  as c7,      T6789.CODIGO as c8,      T18685.ANYO as c33 from       LU_EDADAGR20PAD_IDI T19562,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T19562.IDIOMA and T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T6789.DENOMINACION = ''Castellano''  and T18685.EDADAGR = T19562.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T10746.CODIGO, T10746.DENOMINACION, T17978.DT, T17978.DTBA, case  when T19562.CODIGO < 19 then T19562.DENOMINACION else case  when T6789.DENOMINACION = ''Castellano'' then ''90 y más'' else ''90 i més'' end  end , case  when T19562.CODIGO < 19 then T19562.CODIGO else 19 end, T18685.ANYO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c1 as c9,      D1.c8 as c10,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c10, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c10 ASC, D2.c1 ASC, D3.c1 ASC) as c13           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c10 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c10 = D3.c3      ) D1 where  ( D1.c13 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c5, c3, c6, c7, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.111276','Lozano','2021-07-27 13:55:20',NULL,58),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A6',NULL,'csv','Indicador IC110A6.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T18846.CODIGO as c4,      T18846.DENOMINACION as c5,      T10746.CODIGO as c6,      T10746.DENOMINACION as c7,      T6789.CODIGO as c8,      T18685.ANYO as c33 from       LU_II_PAD_LUGNAC_IDI T18846,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T18846.IDIOMA and T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T6789.DENOMINACION = ''Castellano''  and T18685.LUGNAC = T18846.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T10746.CODIGO, T10746.DENOMINACION, T17978.DT, T17978.DTBA, T18846.CODIGO, T18846.DENOMINACION, T18685.ANYO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c7, c4, c2, c5, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.155505','Lozano','2021-07-27 13:55:20',NULL,59),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A7',NULL,'csv','Indicador IC110A7.csv','WITH  SAWITH0 AS (select distinct count(T18685.IDPADRON) over (partition by T6789.DENOMINACION, T10746.CODIGO, T17978.DTBA, T19568.CODIGO)  as c1,      T10746.CODIGO as c2,      T10746.DENOMINACION as c3,      T17978.DTBA as c4,      T19568.CODIGO as c5,      T19568.DENOMINACION as c6,      T17978.DT as c7,      T6789.CODIGO as c8,      count(T18685.IDPADRON) over (partition by T6789.DENOMINACION, T10746.CODIGO, T17978.DTBA, T56143.CODPAIS, T19568.CODIGO)  as c9,      T56143.CODPAIS as c10,      T18685.ANYO as c33 from       LU_EDADAGR3PAD_IDI T19568,      V_LU_GEO_PAIS_NACION T56143,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T19568.IDIOMA and T17978.DTBA = T18685.DISBAR and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T56143.IDIOMA and T18685.EDADAGR3 = T19568.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18685.PAINACDAD = T56143.CODPAIS and T18685.PAINACDAD <> 108 and T56143.CODPAIS <> 108 ) ), SAWITH1 AS (select 0 as c1,      D1.c7 as c2,      D1.c4 as c3,      D1.c6 as c4,      D1.c5 as c5,      D1.c10 as c6,      D1.c2 as c7,      D1.c3 as c8,      D1.c9 as c9,      D1.c1 as c10,      D1.c8 as c11,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c11 as c13,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c11, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3      ) D1 where  ( D1.c14 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c9, c10, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.198691','Lozano','2021-07-27 13:55:20',NULL,60),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC146A1',NULL,'csv','Indicador IC146A1.csv','select T6789.DENOMINACION ||'';''||      T8148.CODIGO ||'';''||      T8160.CODIGO ||'';''||      T8160.DENOMINACION ||'';''||      T7595.ANO ||'';''||      T7595.MES ||'';''||      T6789.CODIGO ||'';''||      replace(T8109.VALOR , '','' , ''.'') as c9 from       LU_I_MET_ESTMETEO T8148,      V_LU_TIEMPO_ANOMES T7595,      LU_IDIOMA T6789,      LU_I_MET_INDICADOR_IDI T8160,      FACT_I_MET_METEOANOMES T8109 where  ( T6789.CODIGO = T8160.IDIOMA and T8109.ESTMETEO = T8148.CODIGO and T7595.ANO = T8109.ANO and T7595.MES = T8109.MES and T6789.DENOMINACION = ''Castellano''  and T8109.ESTMETEO = 4 and T8109.INDICADOR = T8160.CODIGO and T8148.CODIGO = 4 and (T8109.INDICADOR in (28)) and (T8160.CODIGO in (28)) ) order by T7595.ANO, T7595.MES','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2021-08-10 08:20:31.330113','Lozano','2021-07-27 13:55:20',NULL,63);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A1',NULL,'csv','Indicador IC151A1.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select distinct T103896.NUMVIV1800 as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103896.DTBA ) ), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  as c4,      D1.c1 / nullif( D2.c1, 0) as c5,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  as c6 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.231006','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',87),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110',NULL,'csv','Indicador IC110.csv','WITH  SAWITH0 AS (select sum(T21230.NUMPERSONAS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Castellano'' and T10746.CODIGO = T21230.SEXO and T17978.DTBA = T21230.DISBAR )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c6 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.277715','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',88),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9',NULL,'csv','Indicador IC110A9.csv','WITH  SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T19610.IDIOMA and T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T18685.ANYO and T18685.EDAD100 = T19610.CODIGO )  group by T7594.ANO) select D1.c1 ||'';'' || D1.c2 || '';'' || D1.c3 from ( select 0 as c1,      D1.c2 as c2,      D1.c1 as c3 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3','N','sftp',NULL,NULL,'2021-08-10 08:20:32.320996','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',89),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9B',NULL,'csv','Indicador IC110A9B.csv','WITH  SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,      T10746.DENOMINACION as c2,      T7594.ANO as c3 from       LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T10746.IDIOMA and T6789.CODIGO = T19610.IDIOMA and T7594.ANO = T18685.ANYO and T6789.DENOMINACION = ''Castellano'' and T10746.CODIGO = T18685.SEXO and T18685.EDAD100 = T19610.CODIGO )  group by T7594.ANO, T10746.DENOMINACION) select D1.c1 || '';'' || D1.c2 || '';'' || D1.c3 || '';'' || D1.c4  from ( select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4 from       SAWITH0 D1 order by c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2021-08-10 08:20:32.364082','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',90),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9C',NULL,'csv','Indicador IC110A9C.csv','WITH  SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T10746.DENOMINACION as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T19610.IDIOMA and T18685.DIS = T55787.DT and T6789.DENOMINACION = ''Castellano'' and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T18685.EDAD100 = T19610.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T10746.DENOMINACION, T17978.DTBA, T55787.DT), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH4 D1 order by c1, c7, c4, c2, c5, c3, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,C8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.407032','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',91),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A1',NULL,'csv','Indicador IC149A1.csv','WITH  SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      case  when T104038.ANO <= 1800 then ''<=1800'' else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then ''1801-1900'' else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then ''1901-1920'' else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then ''1921-1940'' else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then ''1941-1960'' else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then ''1961-1970'' else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then ''1971-1980'' else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then ''1981-1990'' else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then ''1991-2000'' else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then ''2001-10'' else case  when T104038.ANO > 2010 then ''>2010'' end  end  end  end  end  end  end  end  end  end  end  as c5,      case  when T104038.ANO <= 1800 then 1 else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then 2 else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then 3 else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then 4 else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then 5 else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then 6 else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then 7 else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then 8 else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then 9 else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then 10 else case  when T104038.ANO > 2010 then 11 end  end  end  end  end  end  end  end  end  end  end  as c6,      T6789.CODIGO as c7 from       V_LU_TIEMPO_ANO T104038 /* LU_IV_CAT_ANY1 */ ,      V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_IV_CAT_VIV T103797 where  ( T17978.DTBA = T103797.DTBA and T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T103797.DTSC and T55787.DT = T103797.DT and T6789.DENOMINACION = ''Castellano'' and T103797.ANY1 = T104038.ANO )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT, case  when T104038.ANO <= 1800 then ''<=1800'' else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then ''1801-1900'' else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then ''1901-1920'' else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then ''1921-1940'' else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then ''1941-1960'' else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then ''1961-1970'' else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then ''1971-1980'' else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then ''1981-1990'' else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then ''1991-2000'' else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then ''2001-10'' else case  when T104038.ANO > 2010 then ''>2010'' end  end  end  end  end  end  end  end  end  end  end , case  when T104038.ANO <= 1800 then 1 else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then 2 else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then 3 else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then 4 else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then 5 else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then 6 else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then 7 else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then 8 else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then 9 else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then 10 else case  when T104038.ANO > 2010 then 11 end  end  end  end  end  end  end  end  end  end  end ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c1 as c7,      D1.c7 as c8 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3, c8, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2021-08-10 08:20:32.450449','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',92),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A2',NULL,'csv','Indicador IC149A2.csv','WITH  SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,      T103931.DENOMINACION as c2,      T55787.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       LU_IV_CAT_SUPERF_IDI T103931,      LU_IV_CAT_ANT1800_IDI T103907,      V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_IV_CAT_VIV T103797 where  ( T6789.CODIGO = T103931.IDIOMA and T55787.DT = T103797.DT and T17978.DTBA = T103797.DTBA and T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T103797.DTSC and T6789.CODIGO = T103907.IDIOMA and T103797.ANT1800 = T103907.CODIGO and T6789.DENOMINACION = ''Castellano'' and T103797.ANT1800 = 0 and T103797.SUPERF = T103931.CODIGO and T103907.CODIGO = 0 )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT, T103931.DENOMINACION), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c4 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH4 D1 order by c1, c7, c2, c5, c3, c6, c4 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.494064','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',93),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3',NULL,'csv','Indicador IC149A3.csv','WITH  SAWITH0 AS (select T103896.NUMVIV1800 as c1,      sum(T103896.VALCAT_TOT) as c2,      sum(T103896.VCATS_MED) as c3,      sum(T103896.VALCAT_MED) as c4,      sum(T103896.VCATC_MED) as c5,      T17978.DT as c6,      T17978.DTBA as c7,      T7594.ANO as c8,      T6789.CODIGO as c9 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103896.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103896.NUMVIV1800), SAWITH1 AS (select 0 as c1,      D1.c6 as c2,      D1.c7 as c3,      D1.c8 as c4,      D1.c5 as c5,      D1.c4 as c6,      D1.c3 as c7,      D1.c2 as c8,      D1.c2 / nullif( D1.c1, 0) as c9,      D1.c1 as c10,      D1.c9 as c11 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c11 as c13,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c11, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3      ) D1 where  ( D1.c14 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2021-08-10 08:20:32.537665','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',94),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A4',NULL,'csv','Indicador IC149A4.csv','WITH  SAWITH0 AS (select sum(T103867.NUMVIV1800) as c1,      T103955.DENOMINACION as c2,      T17978.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_IV_CAT_VCATA_IDI T103955,      FACT_IV_CAT_VIV_AGRDTBAVC T103867 where  ( T6789.CODIGO = T103955.IDIOMA and T7594.ANO = T103867.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103867.DTBA and T103867.VALCATA = T103955.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103955.DENOMINACION), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c4 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH4 D1 order by c1, c7, c5, c3, c6, c4, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.581011','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',95),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150',NULL,'csv','Indicador IC150.csv','WITH  SAWITH0 AS (select sum(T102892.NUMSOL) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRDTBA T102892 where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D2.c1 as c2,      D3.c1 as c3,      D1.c2 as c4,      D1.c3 as c5,      D1.c4 as c6,      D1.c5 as c7,      D1.c7 as c9,      ROW_NUMBER() OVER (PARTITION BY D1.c4, D1.c3, D1.c2, D2.c1, D3.c1 ORDER BY D1.c4 DESC, D1.c3 DESC, D1.c2 DESC, D2.c1 DESC, D3.c1 DESC) as c10 from       (           SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D1.c4 as c4,                D1.c5 as c5,                D1.c6 as c6,                D1.c7 as c7,                sum(case D1.c10 when 1 then D1.c7 else NULL end ) over ()  as c8,                D1.c9 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c9 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c9 ASC) as c10           from                 SAWITH4 D1      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.624064','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',96);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A2',NULL,'csv','Indicador IC150A2.csv','WITH  SAWITH0 AS (select sum(T120156.NUMSOL) as c1,      sum(T120156.VCATS) as c2,      T7594.ANO as c3 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRTOTALDT T120156 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120156.ANO )  group by T7594.ANO) select D1.c1 || '';'' || D1.c2 || '';'' || D1.c3  from ( select distinct 0 as c1,      D1.c3 as c2,      round(D1.c2 / nullif( D1.c1, 0) , 2) as c3 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3','N','sftp',NULL,NULL,'2021-08-10 08:20:32.667228','Lozano-32054','2021-07-27 13:55:20','Lozano-32054',97),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A2B',NULL,'csv','Indicador IC150A2B.csv','WITH  SAWITH0 AS (select sum(T102892.NUMSOL) as c1,      sum(T102892.VCATS) as c2,      T17978.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRDTBA T102892 where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c3 as c2,      D1.c4 as c3,      D1.c5 as c4,      D1.c1 as c5,      D1.c2 as c6,      round(D1.c2 / nullif( D1.c1, 0) , 2) as c7,      D1.c6 as c8 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH4 D1 order by c1, c6, c2, c4, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.71015','Lozano-32054','2021-07-27 13:55:20','Lozano-32054',98),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A3',NULL,'csv','Indicador IC150A3.csv','WITH  SAWITH0 AS (select sum(T120156.SUPSOL) as c1,      sum(T120156.VCATS) as c2,      T7594.ANO as c3 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRTOTALDT T120156 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120156.ANO )  group by T7594.ANO) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 from ( select 0 as c1,      D1.c3 as c2,      D1.c1 as c3,      D1.c2 as c4,      D1.c2 / nullif( D1.c1, 0) as c5 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5','N','sftp',NULL,NULL,'2021-08-10 08:20:32.753223','Lozano-32054','2021-07-27 13:55:20','Lozano-32054',99),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A3B',NULL,'csv','Indicador IC150A3B.csv','WITH  SAWITH0 AS (select sum(T102892.SUPSOL) as c1,      sum(T102892.VCATS) as c2,      T17978.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRDTBA T102892 where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c3 as c2,      D1.c4 as c3,      D1.c5 as c4,      D1.c1 as c5,      D1.c2 as c6,      D1.c2 / nullif( D1.c1, 0) as c7,      D1.c6 as c8 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.796307','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',100),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151',NULL,'csv','Indicador IC151.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c6 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.839367','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',101),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A2',NULL,'csv','Indicador IC151A2.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.DT = T55787.DT and T16529.DTBA = T17978.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  as c4,      D1.c1 / nullif( D2.c1, 0) as c5,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  as c6 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.882501','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',102),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC098',NULL,'csv','Indicador IC098.csv','WITH  SAWITH0 AS (select sum(T43966.VALOR) as c1,      T43989.DENOMINACION as c2,      T55787.DT as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       LU_VIII_AGPO_IND_IDI T43989,      V_LU_DIVMUN_DT T55787,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_AGPO_FACTDT T43966 where  ( T6789.CODIGO = T43989.IDIOMA and T7594.ANO = T43966.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T43966.MUNICIPIO and T43966.DISTRITO = T55787.DT and T6789.DENOMINACION = ''Castellano'' and T43966.INDICADOR = T43989.CODIGO and T43966.INDICADOR = 3 and T43989.CODIGO = 3 )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T43989.DENOMINACION, T55787.DT), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c6 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D1.c4 as c5,                D1.c5 as c6,                D1.c6 as c7,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC) as c8           from                 SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c6 = D2.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6 from       SAWITH3 D1 order by c1, c5, c4, c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6','N','sftp',NULL,NULL,'2021-08-10 08:20:32.925899','Lozano-32016','2021-07-27 13:55:20','Lozano-32016',103),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A1',NULL,'csv','Indicador IC110A1.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T18894.DENOMINACION as c2,      T7594.ANO as c3 from       LU_EDADES T22191,      LU_II_PAD_TIPVIV_IDI T18894,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T18685.EDAD = T22191.EDAD and T7594.ANO = T18685.ANYO and T6789.CODIGO = T19610.IDIOMA and T18685.EDAD100 = T19610.CODIGO and T6789.CODIGO = T18894.IDIOMA and T18685.TIPLOC = T18894.CODIGO and T6789.DENOMINACION = ''Castellano'' and T18894.DENOMINACION = ''Vivienda familiar'' and T19610.CODIGO = T22191.EDAD100 and T18685.EDAD <= 17 and T22191.EDAD <= 17 )  group by T7594.ANO, T18894.DENOMINACION) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4  from ( select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4 from       SAWITH0 D1 order by c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2021-08-10 08:20:32.96903','Lozano-32016','2021-07-27 13:55:20','Lozano-32016',104),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149',NULL,'csv','Indicador IC49.csv','WITH  SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_IV_CAT_VIV T103797 where  ( T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T17978.DTBA = T103797.DTBA and T22801.DTSC = T103797.DTSC and T6789.DENOMINACION = ''Castellano''  and T55787.DT = T103797.DT  )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT), SAWITH1 AS (select sum(T103843.NUMVIV) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRANYDTBA T103843 where  ( T7594.ANO = T103843.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103843.DTBA  )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH2 AS (select distinct T103896.NUMVIV1800 as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano''  and T17978.DTBA = T103896.DTBA ) ), SAWITH3 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 when D3.c2 is not null then D3.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 when D3.c3 is not null then D3.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 when D3.c4 is not null then D3.c4 end  as c4,      D1.c1 as c5,      D2.c1 as c6,      D3.c1 as c7,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 when D3.c5 is not null then D3.c5 end  as c8 from       (           SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ) full outer join SAWITH2 D3 On D3.c2 = case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  and D3.c3 = case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  and D3.c4 = case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  and D3.c5 = case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  and  SYS_OP_MAP_NONNULL(D3.c6) = SYS_OP_MAP_NONNULL(case  when D1.c6 is not null then D1.c6 when D2.c6 is not null then D2.c6 end ) ), SAWITH4 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH5 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH6 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH3 D1 inner join SAWITH4 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH5 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH6 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2021-08-10 08:20:33.011967','Lozano-32016','2021-07-27 13:55:20','Lozano-32016',105),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A21',NULL,'csv','Indicador IC151A21.csv','select sup_aparcamientos.c2 ||'';''|| sup_aparcamientos.c1/num_turismos.c1 from (select sum(T120150.SUPCONS) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRTOTALDT T120150 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120150.ANO )  group by T7594.ANO order by c2) sup_aparcamientos,  (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T6789.CODIGO = T16638.IDIOMA and T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T16529.ANO and T16529.ANYMAT = T16638.CODIGO )  group by T7594.ANO order by c2) num_turismos where sup_aparcamientos.c2=num_turismos.c2','c1,c2','N','sftp',NULL,NULL,'2021-08-10 08:20:33.055405','Lozano-32055','2021-07-27 13:55:20','Lozano-32055',106);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A2',NULL,'csv','Indicador IC151A2.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.DT = T55787.DT and T16529.DTBA = T17978.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  as c4,      D1.c1 / nullif( D2.c1, 0) as c5,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  as c6 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:33.098404','Lozano-32055','2021-07-27 13:55:20','Lozano-32055',107),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3',NULL,'csv','Indicador IC149A3.csv','WITH  SAWITH0 AS (select T103896.NUMVIV1800 as c1,      sum(T103896.VALCAT_TOT) as c2,      sum(T103896.VCATS_MED) as c3,      sum(T103896.VALCAT_MED) as c4,      sum(T103896.VCATC_MED) as c5,      T17978.DT as c6,      T17978.DTBA as c7,      T7594.ANO as c8,      T6789.CODIGO as c9 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103896.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103896.NUMVIV1800), SAWITH1 AS (select 0 as c1,      D1.c6 as c2,      D1.c7 as c3,      D1.c8 as c4,      D1.c5 as c5,      D1.c4 as c6,      D1.c3 as c7,      D1.c2 as c8,      D1.c2 / nullif( D1.c1, 0) as c9,      D1.c1 as c10,      D1.c9 as c11 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c11 as c13,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c11, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3      ) D1 where  ( D1.c14 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2021-08-10 08:20:33.141728','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',108),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3_1',NULL,'csv','Indicador IC149A3_1.csv','WITH  SAWITH0 AS (select sum(T120168.VALCAT_MED) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCTOTALDT T120168 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120168.ANO )  group by T7594.ANO) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 from ( select 0 as c1,      D1.c2 as c2,      D1.c1 as c3 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2021-08-10 08:20:33.18599','Lozano-32055','2021-07-27 13:55:20','Lozano-32055',109),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC097',NULL,'csv','Indicador IC097.csv','WITH  SAWITH0 AS (select sum(T43974.VALOR) as c1,      T43989.DENOMINACION as c2,      T44001.CODIGO as c3,      T44001.DENOMINACION as c4,      T7594.ANO as c5 from       LU_VIII_AGPO_TIPAB_IDI T44001,      LU_VIII_AGPO_IND_IDI T43989,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_AGPO_INFABONADOS T43974 where  ( T6789.CODIGO = T44001.IDIOMA and T7594.ANO = T43974.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T43974.MUNICIPIO and T6789.CODIGO = T43989.IDIOMA and T43974.INDICADOR = T43989.CODIGO and T6789.DENOMINACION = ''Castellano'' and T43974.TIPOABONADO = T44001.CODIGO and T43974.TIPOABONADO = 1 and T43989.DENOMINACION = ''Agua facturada miles m3'' and T44001.CODIGO = 1 )  group by T7594.ANO, T43989.DENOMINACION, T44001.CODIGO, T44001.DENOMINACION) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6  from ( select distinct 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 * 1000000 / 365 as c6 from       SAWITH0 D1 order by c5, c2, c4, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6','N','sftp',NULL,NULL,'2021-08-10 08:20:32.18786','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',86),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SEDE',NULL,NULL,'/integracion/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/integracion/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC15',NULL,'csv','Indicador ISTIC15','select count(*) ||'';''||  TO_CHAR(dindate, ''YYYY-MM'') from pro2_ocs.docmeta d, pro2_ocs.revisions r where xaytoestadoelaboracionNTI = ''EE02'' and ddoctitle like ''Padron%'' and dindate >= (SELECT trunc(trunc(sysdate,''month'')-1,''month'') FROM DUAL) AND dindate < (SELECT trunc((sysdate),''month'') FROM DUAL) and d.did = r.did group by TO_CHAR(dindate, ''YYYY-MM'')','resultado,fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.501965','Ticket-23401','2021-07-27 13:55:20',NULL,69),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC01_#ddMMyyyy#',NULL,'csv','Indicador ISTIC01','select count(*)||'';''||TO_CHAR(Fecha_acceso, ''YYYY-MM'') from acceso_sede where fecha_acceso >=trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'')  AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''), ''MM'') group by TO_CHAR(Fecha_acceso, ''YYYY-MM'')','Accesos_autenticados,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:31.844888','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',77),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC10_#ddMMyyyy#',NULL,'csv','Indicador ISTIC10','select count(*) ||'';''|| TO_CHAR(Fecha_acceso, ''YYYY-MM'') from acceso_sede  where EXTRACT(month FROM Fecha_acceso) = (EXTRACT(month FROM TO_DATE(''#fecha#'',''yyyyMMdd''))-1) and EXTRACT(year FROM Fecha_acceso) = EXTRACT(year FROM TO_DATE(''#fecha#'',''yyyyMMdd'')) group by TO_CHAR(Fecha_acceso, ''YYYY-MM'')','Accesos_autenticados,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:31.888053','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',79),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC12_#ddMMyyyy#',NULL,'csv','Indicador ISTIC12','SELECT ta.autoridad ||'';''|| count(*) ||'';''|| TO_CHAR(fecha_acceso,''YYYY-MM-DD'') AS Num_Accesos FROM acceso_sede acs, sede_electro.tipo_acceso ta WHERE acs.id_tipo_acceso = ta.id_tipo_acceso  AND TO_CHAR(acs.FECHA_ACCESO, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') - 1, ''YYYY-MM-DD'') AND (acs.id_tipo_acceso is not null) AND acs.id_resultado_login = 1 GROUP BY ta.autoridad, TO_CHAR(fecha_acceso,''YYYY-MM-DD'') ORDER BY Num_accesos DESC','Autoridad,Num_Accesos,Fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.930958','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',80),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC13_#ddMMyyyy#',NULL,'csv','Indicador ISTIC13','select Asunto ||'';''|| count(asunto) ||'';''|| TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') from instancia_presentada where TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') -1, ''YYYY-MM-DD'')  group by Asunto, TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') order by TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') desc','Asunto,Instancias,Fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.973724','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',81),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC14_#ddMMyyyy#',NULL,'csv','Indicador ISTIC14','select count(*) ||'';''|| TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') from tramite  where TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') -1, ''YYYY-MM-DD'') group by TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') order by TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') desc','Tramites,Fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:32.016525','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',82);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC16_#ddMMyyyy#',NULL,'csv','Indicador ISTIC16','select TO_CHAR(primer_acceso, ''YYYY-MM'') ||'';''|| count(*) from (       select min(fecha_acceso) as primer_acceso, num_documento_usuario        from  acceso_sede        where id_resultado_login = 1        group by num_documento_usuario) where  primer_acceso >=trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'')  AND primer_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''), ''MM'') group by TO_CHAR(primer_acceso, ''YYYY-MM'') order by TO_CHAR(primer_acceso, ''YYYY-MM'') desc','Primer_acceso,Numero','S','sftp',NULL,NULL,'2021-08-10 08:20:32.059316','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',83),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/integracion/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC53',NULL,'csv','Indicador ISTIC53','SELECT count(*) ||'';''||  TO_CHAR(fecha_registro, ''YYYY-MM'') FROM instancia_presentada WHERE nif_interesado <> nif_tramitador AND fecha_registro >=(SELECT trunc(trunc(sysdate,''month'')-1,''month'') FROM DUAL) AND fecha_registro  < (SELECT trunc((sysdate),''month'') FROM DUAL) GROUP BY TO_CHAR(fecha_registro, ''YYYY-MM'')','resultado,fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.587895','Ticket-23401','2021-07-27 13:55:20',NULL,71),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02C_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02C_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad = ''DNIe'' group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.446975','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',162),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02C_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02C_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad = ''DNIe'' group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.49008','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',163),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02B_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02B_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad = (''Cl@ve'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.403496','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',161),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02D_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02D_mensual','SELECT autoridad ||'';''|| replace(porcentaje,'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad not in (''FNMT-Ceres'',''ACCV'',''Cl@ve'',''DNIe'')','Autoridad,Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.532994','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',164),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02D_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02D_anual','SELECT autoridad ||'';''|| replace(porcentaje,'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad not in (''FNMT-Ceres'',''ACCV'',''Cl@ve'',''DNIe'')','Autoridad,Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.576146','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',165),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/integracion/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC20',NULL,'csv','Indicador ISTIC20','SELECT * FROM (select Asunto ||'';''||  count(asunto) ||'';''||  TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') from instancia_presentada where TO_CHAR(Fecha_Registro, ''YYYYMMDD'') = TO_CHAR(sysdate -1, ''YYYYMMDD'') group by Asunto, TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') order by count(asunto) desc) WHERE ROWNUM <= 10','asunto,instancias,fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.54484','Ticket-23401','2021-07-27 13:55:20',NULL,70),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02A_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02A_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad IN (''FNMT-Ceres'',''ACCV'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.27443','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',158),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02A_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02A_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad IN (''FNMT-Ceres'',''ACCV'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.31713','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',159);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02B_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02B_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(Fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(Fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad = (''Cl@ve'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.359934','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',160),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','PostGis_Plataforma',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_TRAMOS_CONGESTION/','/integracion/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_TRAMOS_CONGESTION/',NULL,'tramos_congestion',NULL,'csv','tramos_congestion','select concat(id_tramo,'';'',desc_tramo_cas,'';'', desc_tramo_val) from t_d_trafico_tramos_congestiones;','id_tramo, desc_tramo_cas, desc_tramo_val','S','sftp',NULL,NULL,'2021-08-10 08:20:31.758499','toni-28427','2021-07-27 13:55:20',NULL,75),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','SQL_Server_Trafico',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_CONGESTION/','/integracion/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_CONGESTION/',NULL,'congestion_diaria',NULL,'csv','congestion_diaria','SELECT concat( rec.Rec, '';'',''#fecha#'', '';'', round(convert(float,sum([PorcC]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' , round(convert(float,sum([PorcD]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' , round(convert(float,sum([PorcFX]))/convert(float,(SELECT count(rec2.Rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' , ( 100-(round(convert(float,sum([PorcC]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0)+  round(convert(float,sum([PorcD]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0)+  round(sum(convert(float,[PorcFX]))/convert(float,(SELECT count(rec2.Rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0))) ) FROM [EstadisticasGIP].[dbo].[Recorr] rec WHERE convert(date,DATEADD(HOUR,-1,Hora))=CAST(''#fecha#'' as date) group by rec.Rec order by rec.Rec;','recorrido, dia, porc_denso, porc_congestion, porc_cortado, porc_fluido','S','sftp',NULL,NULL,'2021-08-10 08:20:31.715839','toni-28427','2021-07-27 13:55:20',NULL,74),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','SQL_Server_Trafico',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_INTENSIDAD/','/integracion/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_INTENSIDAD/',NULL,'intensidad_diaria',NULL,'csv','intensidad_diaria','SELECT concat(dea.nombre,'';'', convert(date,dapm.FechaHora),'';'', (sum(dapm.intensidad)/[PROPERTY_TRAFICO_NUM_MEDIDAS_HOR])) FROM  [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado] dea,  [WEB_TRAFICO].[dbo].[Datos_Aforados_PM] dapm where dea.idTA=dapm.iDTA and dea.estado=''A''and convert(date,dapm.FechaHora)=CAST(''#fecha#'' as date) group by dea.nombre, convert(date,dapm.FechaHora)','id_tramo, fecha, intensidad_horaria','S','sftp',NULL,NULL,'2021-08-10 08:20:31.630582','toni-28427','2021-07-27 13:55:20','Ivan 34096',72),
			('VLCi_ETL_INCO','FTP','FTP Sertic','SMC*INCO*','//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi','/integracion/01.fichOrig/VLCi_ETL_INCO/','/integracion/02.fichBackup/VLCi_ETL_INCO/',NULL,'SMC-INCO-yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2023-09-22 08:20:29.404222','Ticket 1914','2023-09-22 08:20:29.404222','Ticket 1914',230);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('py_calidadambiental_sonometros_zas_WOO', 'FTP', 'FTP Sertic', '*WOO', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.WOO', 'N', 'WOO', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_MAN', 'FTP', 'FTP Sertic', '*MAN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.MAN', 'N', 'MAN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_SER', 'FTP', 'FTP Sertic', '*SER', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.SER', 'N', 'SER', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_HON', 'FTP', 'FTP Sertic', '*HON', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.HON', 'N', 'HON', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_CED', 'FTP', 'FTP Sertic', '*CED', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.CED', 'N', 'CED', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_PIN', 'FTP', 'FTP Sertic', '*PIN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.PIN', 'N', 'PIN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_SEN', 'FTP', 'FTP Sertic', '*SEN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.SEN', 'N', 'SEN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_QUA', 'FTP', 'FTP Sertic', '*QUA', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.QUA', 'N', 'QUA', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_BUE', 'FTP', 'FTP Sertic', '*BUE', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.BUE', 'N', 'BUE', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_UNI', 'FTP', 'FTP Sertic', '*UNI', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.UNI', 'N', 'UNI', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_xlsx', 'FTP', 'FTP Sertic', '*xlsx', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.xlsx', 'N', 'xlsx', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_xls', 'FTP', 'FTP Sertic', '*xls', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.xls', 'N', 'xls', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('py_trafico_intensidad_validada', 'BBDD', 'SQL_Server_Trafico', NULL, NULL, '/integracion/01.fichOrig/VLCI_PY_TRAFICO_VALIDADADO/', '/integracion/02.fichBackup/VLCI_PY_TRAFICO_VALIDADADO/', NULL, 'intensidad_trafico_validada_30_#ddMMyyyyHHmmss#', NULL, 'csv', 'intensidad_trafico_validada_30', 
			'SELECT concat(''Kpi-Accesos-Ciudad-Coches'', '';'', actual.idata, '';'', descripciones.descripcion, '';'', descripciones.descripcion_corta, '';'', descripciones.nombre_corto, '';'', actual.int_actual, '';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A373'',''A406'',''A72'',''A59'',''A1'',''A82'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA,vid.DÍA) actual
			inner join (
    			select 4 as idATA, ''Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)'' as descripcion, ''Accés Barcelona (entrada i eixida)'' as descripcion_corta, ''Accés Barcelona'' as nombre_corto union
    			select 57 as idATA, ''Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)''  as descripcion, ''Accés a Arxiduc Carles pel Camí nou de Picanya'' as descripcion_corta, ''Arxiduc Carles'' as nombre_corto union
    			select 70 as idATA, ''Av. del Cid (Entre V-30 i Tres Creus)''  as descripcion, ''Av. del Cid'' as descripcion_corta, ''Av. del Cid'' as nombre_corto union
    			select 78 as idATA, ''Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)''  as descripcion, ''Corts Valencianes (Accés per CV-35)'' as descripcion_corta, ''Corts Valencianes'' as nombre_corto union
    			select 349 as idATA, ''Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)''      as descripcion, ''Accés per V-31 (Pista de Silla)'' as descripcion_corta, ''Pista de Silla'' as nombre_corto union
    			select 379 as idATA, ''Prolongació Joan XXIII (Entre Germans Machado i Salvador Cerveró)''  as descripcion, ''Prolongació Joan XXIII'' as descripcion_corta, ''Joan XXIII'' as nombre_corto) as descripciones
    			on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Accesos-Vias-Coches'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion_corta,'';'', descripciones.nombre_corto,'';'', actual.int_actual,'';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A116'',''A121'',''A97'',''A117'',''A187'',''A51'') 
			and vid.DÍA = CAST(''#fecha#'' as datetime)   
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 50 as idATA, ''Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)''  as descripcion, ''Av. Blasco Ibáñez'' as descripcion_corta, ''Blasco Ibáñez'' as nombre_corto union
				select 93 as idATA, ''Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)''  as descripcion, ''Av. Dr. Peset Aleixandre'' as descripcion_corta, ''Dr. Peset Aleixandre'' as nombre_corto union
				select 111 as idATA, ''Av. de Giorgeta (Entre Sant Vicent i Jesús)''  as descripcion, ''Av. de Giorgeta'' as descripcion_corta, ''Giorgeta'' as nombre_corto union
				select 112 as idATA, ''Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)''  as descripcion, ''Gran Via de Ferran el Catòlic'' as descripcion_corta, ''Ferran el Catòlic'' as nombre_corto union
				select 116 as idATA, ''Gran Via del Marqués del Túria (Entre Pont d''''Aragó i Hernan Cortés)''  as descripcion, ''Gran Via del Marqués del Túria'' as descripcion_corta, ''Marqués del Túria'' as nombre_corto union
				select 177 as idATA, ''Av. de Peris i Valero (Entre Ausiàs March i Sapadors)''  as descripcion, ''Av. de Peris i Valero'' as descripcion_corta, ''Peris i Valero'' as nombre_corto) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Accesos-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			FROM
			(select afo.Descripcion as descripcion,afo.idATA as idata,  sum(vid.IMD*afo.Coeficiente/100) as intactual,vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''F36'',''F37'',''F38'',''F39'',''F40'',''F41'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 725 as idATA, ''Navarro Reverter'' as descripcion union
				select 726 as idATA, ''Pont de fusta'' as descripcion union
				select 727 as idATA, ''Pont de les arts'' as descripcion union
				select 728 as idATA, ''Pont del real'' as descripcion union
				select 729 as idATA, ''Carrer Alacant'' as descripcion union
				select 730 as idATA, ''Carrer Russafa'' as descripcion) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			from
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as intactual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo on 	vid.IdTA=afo.IdTA
			where afo.nombre in (''F31'',''F32'',''F33'',''F34'',''F35'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by 	afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join(
				select 720 as idATA, ''Carrer Colon'' as descripcion union
				select 721 as idATA, ''Comte de Trénor – Pont de fusta''  as descripcion union
				select 722 as idATA, ''Guillem de Castro''  as descripcion union
				select 723 as idATA, ''Xàtiva''  as descripcion union
				select 724 as idATA, ''Plaça Tetuan''  	as descripcion) as descripciones
			on actual.IdATA=descripciones.idATA
			', 'tipo,idata,descripcion, descripcion_corta, nombre_corto,int_actual,calculation_period', 'N', 'sftp', NULL, NULL, now(), 'TICKET 2572', now(), 'TICKET 2572', 300),
			('py_trafico_intensidad_validada', 'BBDD', 'SQL_Server_Trafico', NULL, NULL, '/integracion/01.fichOrig/VLCI_PY_TRAFICO_VALIDADADO/', '/integracion/02.fichBackup/VLCI_PY_TRAFICO_VALIDADADO/', NULL, 'intensidad_trafico_validada_365_#ddMMyyyyHHmmss#', NULL, 'csv', 'intensidad_trafico_validada_365', 
			'SELECT concat(''Kpi-Accesos-Ciudad-Coches'', '';'', actual.idata, '';'', descripciones.descripcion, '';'', descripciones.descripcion_corta, '';'', descripciones.nombre_corto, '';'', actual.int_actual, '';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A373'',''A406'',''A72'',''A59'',''A1'',''A82'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA,vid.DÍA) actual
			inner join (
    			select 4 as idATA, ''Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)'' as descripcion, ''Accés Barcelona (entrada i eixida)'' as descripcion_corta, ''Accés Barcelona'' as nombre_corto union
    			select 57 as idATA, ''Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)''  as descripcion, ''Accés a Arxiduc Carles pel Camí nou de Picanya'' as descripcion_corta, ''Arxiduc Carles'' as nombre_corto union
    			select 70 as idATA, ''Av. del Cid (Entre V-30 i Tres Creus)''  as descripcion, ''Av. del Cid'' as descripcion_corta, ''Av. del Cid'' as nombre_corto union
    			select 78 as idATA, ''Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)''  as descripcion, ''Corts Valencianes (Accés per CV-35)'' as descripcion_corta, ''Corts Valencianes'' as nombre_corto union
    			select 349 as idATA, ''Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)''      as descripcion, ''Accés per V-31 (Pista de Silla)'' as descripcion_corta, ''Pista de Silla'' as nombre_corto union
    			select 379 as idATA, ''Prolongació Joan XXIII (Entre Germans Machado i Salvador Cerveró)''  as descripcion, ''Prolongació Joan XXIII'' as descripcion_corta, ''Joan XXIII'' as nombre_corto) as descripciones
    			on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Accesos-Vias-Coches'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion_corta,'';'', descripciones.nombre_corto,'';'', actual.int_actual,'';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A116'',''A121'',''A97'',''A117'',''A187'',''A51'') 
			and vid.DÍA = CAST(''#fecha#'' as datetime)   
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 50 as idATA, ''Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)''  as descripcion, ''Av. Blasco Ibáñez'' as descripcion_corta, ''Blasco Ibáñez'' as nombre_corto union
				select 93 as idATA, ''Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)''  as descripcion, ''Av. Dr. Peset Aleixandre'' as descripcion_corta, ''Dr. Peset Aleixandre'' as nombre_corto union
				select 111 as idATA, ''Av. de Giorgeta (Entre Sant Vicent i Jesús)''  as descripcion, ''Av. de Giorgeta'' as descripcion_corta, ''Giorgeta'' as nombre_corto union
				select 112 as idATA, ''Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)''  as descripcion, ''Gran Via de Ferran el Catòlic'' as descripcion_corta, ''Ferran el Catòlic'' as nombre_corto union
				select 116 as idATA, ''Gran Via del Marqués del Túria (Entre Pont d''''Aragó i Hernan Cortés)''  as descripcion, ''Gran Via del Marqués del Túria'' as descripcion_corta, ''Marqués del Túria'' as nombre_corto union
				select 177 as idATA, ''Av. de Peris i Valero (Entre Ausiàs March i Sapadors)''  as descripcion, ''Av. de Peris i Valero'' as descripcion_corta, ''Peris i Valero'' as nombre_corto) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Accesos-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			FROM
			(select afo.Descripcion as descripcion,afo.idATA as idata,  sum(vid.IMD*afo.Coeficiente/100) as intactual,vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''F36'',''F37'',''F38'',''F39'',''F40'',''F41'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 725 as idATA, ''Navarro Reverter'' as descripcion union
				select 726 as idATA, ''Pont de fusta'' as descripcion union
				select 727 as idATA, ''Pont de les arts'' as descripcion union
				select 728 as idATA, ''Pont del real'' as descripcion union
				select 729 as idATA, ''Carrer Alacant'' as descripcion union
				select 730 as idATA, ''Carrer Russafa'' as descripcion) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			from
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as intactual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo on 	vid.IdTA=afo.IdTA
			where afo.nombre in (''F31'',''F32'',''F33'',''F34'',''F35'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by 	afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join(
				select 720 as idATA, ''Carrer Colon'' as descripcion union
				select 721 as idATA, ''Comte de Trénor – Pont de fusta''  as descripcion union
				select 722 as idATA, ''Guillem de Castro''  as descripcion union
				select 723 as idATA, ''Xàtiva''  as descripcion union
				select 724 as idATA, ''Plaça Tetuan''  	as descripcion) as descripciones
			on actual.IdATA=descripciones.idATA
			', 'tipo,idata,descripcion, descripcion_corta, nombre_corto,int_actual,calculation_period', 'N', 'sftp', NULL, NULL, now(), 'TICKET 2572', now(), 'TICKET 2572', 301);
		INSERT INTO t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) 
		VALUES ('py_personal_ayuntamiento','FTP','FTP Sertic','*.CSV','//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal/PRE','/integracion/01.fichOrig/VLCI_PERSONAL_AYUNTAMIENTO/','/integracion/02.fichBackup/VLCI_PERSONAL_AYUNTAMIENTO/',NULL,'*.CSV','S','CSV',NULL,NULL,NULL,'N','sftp',NULL,NULL,NOW(),'Ticket 3225',NULL,NULL);
	
			
	ELSIF (current_db = 'sc_vlci_int') THEN

		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_MOBILIS','HTTP','HTTP',NULL,NULL,'/integracion/01.fichOrig/EMT_Mobilis/','/integracion/02.fichBackup/EMT_Mobilis/','http://www.emtvalencia.es/ciudadano/google_transit/datos/carga','mobilis',NULL,'txt',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2021-08-10 08:20:29.88841','ETL Loader','2021-07-27 13:55:20',NULL,29),
			('VLCI_ETL_EMT_MOBILIS_CARGAREFERENCIAS','HTTP','HTTP',NULL,NULL,'/integracion/01.fichOrig/EMT_Mobilis_Transit/','/integracion/02.fichBackup/EMT_Mobilis_Transit/','http://www.emtvalencia.es/ciudadano/google_transit/google_transit.zip','google_transit',NULL,'zip',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2021-08-10 08:20:29.80237','ETL Loader','2021-07-27 13:55:20',NULL,27),
			('VLCI_ETL_EMT_MOBILIS_CARGATITULOS','HTTP','HTTP',NULL,NULL,'/integracion/01.fichOrig/EMT_Mobilis_Titulos/','/integracion/02.fichBackup/EMT_Mobilis_Titulos/','http://www.emtvalencia.es/ciudadano/google_transit/datos/titulos.txt','titulos',NULL,'txt',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2021-08-10 08:20:29.845153','ETL Loader','2021-07-27 13:55:20',NULL,28),
			('CDMGE GACO','FTP','FTP Sertic','SMC-GACO-*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/SIEM/Integradores','/integracion/01.fichOrig/CDMGE_GACO/','/integracion/02.fichBackup/CDMGE_GACO/',NULL,'SMC-GACO-_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.404222',NULL,'2021-06-23 11:03:57',NULL,18),
			('CDMGE Codigo Organico','FTP','FTP Sertic','SMC-TBOR-*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/SIEM/Integradores','/integracion/01.fichOrig/CDMGE_TBOR/','/integracion/02.fichBackup/CDMGE_TBOR/',NULL,'SMC-TBOR-_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.446839',NULL,'2021-06-23 11:03:42',NULL,19),
			('ETL VLCi IBI','FTP','FTP Sertic','Informacion_tributaria_IBI_*','//DADES2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/Integradores','/integracion/01.fichOrig/VLCi_IBI/','/integracion/02.fichBackup/VLCi_IBI/',NULL,'Informacion_tributaria_IBI_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.489668','ETL Loader','2021-07-27 13:55:20',NULL,20),
			('MESAS SILLAS','FTP','FTP Sertic','MESAS_Y_SILLAS_*','//dades2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/Integradores','/integracion/01.fichOrig/VLCi_MESAS_SILLAS/','/integracion/02.fichBackup/VLCi_MESAS_SILLAS/',NULL,'MESAS_Y_SILLAS_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.532625',NULL,'2021-07-27 13:55:20',NULL,21),
			('VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS','FTP','FTP Sertic','*listadoUnidadesAdministrativas.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/Integradores','/integracion/01.fichOrig/VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS/','/integracion/02.fichBackup/VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS/',NULL,'*listadoUnidadesAdministrativas.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.575779',NULL,'2021-07-12 08:49:19',NULL,22),
			('VLCi_CDMGE_CONTRATACION','FTP','FTP Sertic','*listado_expedientes_contratacion.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/contratacion/INT','/integracion/01.fichOrig/VLCi_CONTRATACION/','/integracion/02.fichBackup/VLCi_CONTRATACION/',NULL,'*listado_expedientes_contratacion.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.618861','ETL Loader','2023-11-22 11:03:50','Cecilia - 2108', NULL);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_REALIZADAS','FTP','FTP Sertic','*_tareasRealizadas.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/Integradores','/integracion/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/integracion/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_tareasRealizadas.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.661792','ETL Loader','2021-06-23 11:03:30',NULL,24),
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_PENDIENTES','FTP','FTP Sertic','*_tareasPendientes.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/Integradores','/integracion/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/integracion/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_tareasPendientes.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.704898','ETL Loader','2021-06-23 11:03:30',NULL,25),
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_USUARIOS','FTP','FTP Sertic','*_usuariosPorUnidad.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/Integradores','/integracion/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/integracion/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_usuariosPorUnidad.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.747808','ETL Loader','2021-06-23 11:03:30',NULL,26),
			('VLCi Sertic Red Pagos','FTP','FTP Sertic','*.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integradores','/integracion/01.fichOrig/VLCi_Sertic_Red_Pagos/','/integracion/02.fichBackup/VLCi_Sertic_Red_Pagos/',NULL,'yyyy.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.275703','ETL Loader','2021-07-27 13:55:20',NULL,15),
			('ETL VLCi Vados','FTP','FTP Sertic','Informacion_tributaria_Vados_*','//dades2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/Integradores','/integracion/01.fichOrig/VLCi_Vados/','/integracion/02.fichBackup/VLCi_Vados/',NULL,'Informacion_tributaria_Vados_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.36129','ETL Loader','2021-07-27 13:55:20',NULL,17);			
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_VALENCIA_ETL_IVTM','FTP','FTP Sertic','Informacion_tributaria_IVTM*','//dades2/dades2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/Integradores','/integracion/01.fichOrig/VLCI_VALENCIA_ETL_IVTM/','/integracion/02.fichBackup/VLCI_VALENCIA_ETL_IVTM/',NULL,'Informacion_tributaria_IVTM*','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:31.373547','ETL Loader','2021-07-27 13:55:20',NULL,64),
			('VLCi_Valencia_ETL_IAE','FTP','FTP Sertic','Informacion_tributaria_IAE_*.txt','//dades2/dades2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/Integradores','/integracion/01.fichOrig/IAE/','/integracion/02.fichBackup/IAE/',NULL,'Informacion_tributaria_IAE_yyyy.txt','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.709989','ETL Loader','2021-07-27 13:55:20',NULL,1),
			('VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL','FTP','FTP Sertic','residuos_oci.xlsx','//dades1/dades1/ayun/Residuos/LIMPIEZA/isabel/00 OCI/Integradores','/integracion/01.fichOrig/VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL/','/integracion/02.fichBackup/VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL/',NULL,'residuos_oci.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:33.618966','Ticket-45251','2021-07-27 13:55:20',NULL,NULL),
			('VLCI_ETL_SERTIC_SEDE_SWEB1','FTP','FTP Sertic','access.log-*','//soes-aux1/FTPVOL/LogsWeb/slogserver1/','/integracion/01.fichOrig/VLCi_Sertic_SedeSWeb1/','/integracion/02.fichBackup/VLCi_Sertic_SedeSWeb1/',NULL,'access.log-*','N','xz',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:33.228868','Ticket-39365','2021-07-27 13:55:20',NULL,155),
			('VLCI_ETL_JARDINERIA_IND_INVENTARIO','FTP','FTP Sertic','JARDINES*','//DADES2/DADES2/ayun/JARDINERIA/JardineriaVLCi/Integradores','/integracion/01.fichOrig/Jardines_Inventario/','/integracion/02.fichBackup/Jardines_Inventario/',NULL,'JARDINES','N','mdb',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:32.144823','Ivan 31459','2021-07-27 13:55:20','Ivan 31459',85),
			('VLCI_ETL_COORDVIAPUB_IND_INCIDENCIAS_SIGO','FTP','FTP Sertic','INCIDENCIAS*','//dades1/DADES1/ayun/OCOVAL/Integradores','/integracion/01.fichOrig/OCOVAL/','/integracion/02.fichBackup/OCOVAL/',NULL,'INCIDENCIAS.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:32.102146','Toni 28621','2021-07-27 13:55:20','Toni 28621',84);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Valencia_ETL_PMP_Facturas','FTP','FTP Sertic','PMP_UA_*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/integracion/','/integracion/01.fichOrig/PMP_facturas/','/integracion/02.fichBackup/PMP_facturas/',NULL,'PMP_UA_yyyy.txt','N','zip',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.754693','ETL Loader','2021-07-27 13:55:20',NULL,2),
			('ETL_Trafico_aparcamientos','FTP','FTP Sertic','Consulta aparcamientos por tipo y plazas.xls','//dades2/DADES2/ayun/TRAFICO/Plataforma VLCi/Integradores','/integracion/01.fichOrig/VLCi_Trafico_Aparcamientos/','/integracion/02.fichBackup/VLCi_Trafico_Aparcamientos/',NULL,'Consulta aparcamientos por tipo y plazas.xls','N','xls',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.840985','ETL Loader','2021-07-27 13:55:20',NULL,4),
			('ETL_VLCi_CDMGE_PMP_ENTIDAD','FTP','FTP Sertic','Evolucion_PMP.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/Integradores','/integracion/01.fichOrig/VLCi_CDMGE_PMP_ENTIDAD/','/integracion/02.fichBackup/VLCi_CDMGE_PMP_ENTIDAD/',NULL,'Evolucion_PMP.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.969863','ETL Loader','2021-07-12 08:49:29',NULL,7),
			('ETL VLCi CDMGE PMP','FTP','FTP Sertic','PMP_Resumen_Uds_Administrativas_*.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/Integradores','/integracion/01.fichOrig/VLCi_CDMGE_PMP/','/integracion/02.fichBackup/VLCi_CDMGE_PMP/',NULL,'PMP_Resumen_Uds_Administrativas_yyyy.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.060836','ETL Loader','2021-07-12 08:49:37',NULL,10);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A8',NULL,'csv','Indicador IC110A8.csv','WITH SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T56143.CODPAIS as c4,      case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  as c5,      T56143.DPAIS as c6,      T6789.CODIGO as c7,      T18685.ANYO as c33 from       V_LU_GEO_PAIS_NACION T56143,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T56143.IDIOMA and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T17978.DTBA = T18685.DISBAR and T18685.CODPOS = T21622.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18685.PAINACDAD = T56143.CODPAIS and T18685.PAINACDAD <> 108 and T56143.CODPAIS <> 108)  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T56143.CODPAIS, T56143.DPAIS, case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end, T18685.ANYO ) select 0 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c1 ||'';''||      D1.c6 ||'';''||      case  when row_number() OVER (PARTITION BY D1.c3 ORDER BY D1.c1 DESC) < 7 then row_number() OVER (PARTITION BY D1.c3 ORDER BY D1.c1 DESC) else 10 end  ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH0 D1  order by D1.c2, D1.c7','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.242798','Lozano','2021-08-26 08:08:43.185664',NULL,61),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC015A1',NULL,'csv','Indicador IC015A1.csv','with pob as ( select SUM(T21230.NUMPERSONAS) personas, T7594.ANO ano from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Valenciano'' and T10746.CODIGO = T21230.SEXO )  GROUP BY T7594.ANO ), cons as ( select sum(T60833.VALOR) consumo, T7594.ANO ano from       LU_VIII_IBER_TIPOENERGIA_IDI T61176,      LU_VIII_IBER_TIPCONS_IDI T33126,      LU_VIII_IBER_INDICADOR_IDI T33120,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_IBER_DATOSANUAL T60833 where  ( T6789.CODIGO = T61176.IDIOMA and T6789.CODIGO = T33120.IDIOMA and T33120.CODIGO = T60833.INDICADOR and T7594.ANO = T60833.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T60833.MUNICIPIO and T6789.CODIGO = T33126.IDIOMA and T33126.CODIGO = T60833.TIPCONS and T6789.DENOMINACION = ''Valenciano'' and T33120.CODIGO = 1 and T33126.CODIGO = 1 and T60833.TIPENERGIA = T61176.CODIGO and T61176.CODIGO = 1 )  GROUP BY  T7594.ANO  ) select ((d1.consumo/p1.personas) / 12.0)*1000 ||'';''|| p1.ano ano from pob p1, cons d1 where p1.ano = d1.ano','consumo_anual,ano','N','sftp',NULL,NULL,'2021-08-10 08:20:30.059754','Lozano','2021-07-27 13:55:20',NULL,35),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC019A1',NULL,'csv','Indicador IC019A1.csv','with pob as (  select SUM(T21230.NUMPERSONAS) personas, T7594.ANO ano from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Valenciano'' and T10746.CODIGO = T21230.SEXO ) GROUP BY T7594.ANO ),  cons as (select sum(T60833.VALOR) consumo, T7594.ANO ano from       LU_VIII_IBER_TIPOENERGIA_IDI T61176,      LU_VIII_IBER_TIPCONS_IDI T33126,      LU_VIII_IBER_INDICADOR_IDI T33120,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_IBER_DATOSANUAL T60833 where  ( T6789.CODIGO = T61176.IDIOMA and T6789.CODIGO = T33120.IDIOMA and T33120.CODIGO = T60833.INDICADOR and T7594.ANO = T60833.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T60833.MUNICIPIO and T6789.CODIGO = T33126.IDIOMA and T33126.CODIGO = T60833.TIPCONS and T6789.DENOMINACION = ''Valenciano''  and T33120.CODIGO = 1 and T33126.CODIGO <> 9 and T60833.TIPENERGIA = T61176.CODIGO and T61176.CODIGO = 1 )  GROUP BY  T7594.ANO  ) select ((d1.consumo/p1.personas) / 365.0)*1000 ||'';''|| p1.ano ano from pob p1, cons d1 where p1.ano = d1.ano','consumo_anual,ano','N','sftp',NULL,NULL,'2021-08-10 08:20:30.102884','Lozano','2021-07-27 13:55:20',NULL,36),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC001A1',NULL,'csv','Indicador IC001A1.csv','WITH  SAWITH0 AS (select sum(T10607.VALOR) as c1,      T10686.CODIGO as c2,      T10686.DENOMINACION as c3,      T10783.ANO as c4,      T10783.TRIMESTRE as c5,      T10783.ANOTRIMESTRE as c6,      T6789.CODIGO as c7 from       LU_VII_EPAV_EDADAGR2_IDI T10680,      LU_VII_EPAV_INDICADOR_IDI T10686,      LU_VII_EPAV_METODO_IDI T10692,      V_LU_TIEMPO_ANOTRIMESTRE T10783,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_VII_EPAV_SEXOAGREDAD2 T10607 where  ( T6789.CODIGO = T10680.IDIOMA and T6789.CODIGO = T10686.IDIOMA and T10607.EDADAGR2 = T10680.CODIGO and T6789.CODIGO = T10692.IDIOMA and T10607.INDICADOR = T10686.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10607.METODO = T10692.CODIGO and T10607.ANO = T10783.ANO and T10607.SEXO = T10746.CODIGO and T6789.DENOMINACION = ''Castellano'' and T10607.EDADAGR2 = 9 and T10607.INDICADOR = 26 and T10607.METODO = 4 and T10607.TRIMESTRE = T10783.TRIMESTRE  and T10607.SEXO = 9  and T10680.CODIGO = 9 and T10686.CODIGO = 26 and T10692.CODIGO = 4 and T10746.CODIGO = 9  )  group by T6789.CODIGO, T6789.DENOMINACION, T10686.CODIGO, T10686.DENOMINACION, T10783.ANO, T10783.ANOTRIMESTRE, T10783.TRIMESTRE), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c5 as c6,      D1.c1 as c7,      D1.c6 as c8,      D1.c7 as c9 from       SAWITH0 D1), SAWITH2 AS (select T58019.DENOMINACION as c1,      T58019.CODIGO as c2,      T58019.IDIOMA as c3 from       LU_TRIMESTRE_IDI T58019 /* LU_TRIMESTRE_LOOKUP */ ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D1.c4 as c4,                D2.c1 as c5,                D1.c5 as c6,                D1.c6 as c7,                D1.c7 as c8,                D1.c8 as c9,                D1.c9 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c8, D1.c9, D2.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c8 ASC, D1.c9 ASC, D2.c1 ASC) as c11           from                 SAWITH1 D1 inner join SAWITH2 D2 On D1.c5 = D2.c2 and D1.c9 = D2.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 as c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH3 D1 order by c1, c4, c6, c7, c2, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:30.146152','Lozano','2021-07-27 13:55:20',NULL,37),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC101A1',NULL,'csv','Indicador IC101A1.csv','WITH  SAWITH0 AS (select sum(T13116.VALOR) as c1,      T73182.CODIGO as c2,      T73182.DENOMINACION as c3,      T13200.CODIGO as c4,      T13200.DENOMINACION as c5,      T13212.CODIGO as c6,      T6789.DENOMINACION as c7,      T7595.ANO as c8,      T7595.MES as c9,      T7595.ANOMES as c10,      T6789.CODIGO as c11,      T7595.ANO as c33 from       LU_VI_EOH_TIPEST_IDI T73182,      LU_VI_EOH_RESIDENCIA_IDI T13212,      LU_VI_EOH_INDICADORDEM_IDI T13200,      V_LU_TIEMPO_ANOMES T7595,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VI_EOH_DEMTURMUNI T13116 where  ( T6789.CODIGO = T73182.IDIOMA and T6789.CODIGO = T13200.IDIOMA and T13116.INDICADOR = T13200.CODIGO and T6789.CODIGO = T13212.IDIOMA and T13116.RESIDENCIA = T13212.CODIGO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T13116.MUNICIPIO and T7595.ANO = T13116.ANO and T7595.MES = T13116.MES and T6789.DENOMINACION = ''Castellano'' and T6910.CODMUNICIPIO = 46250 and T13116.INDICADOR = 2 and T13116.MUNICIPIO = 46250 and T13116.RESIDENCIA = 9 and T13116.TIPEST = T73182.CODIGO and T13200.CODIGO = 2 and T13212.CODIGO = 9 and T13116.INDICADOR <> 3 and T13200.CODIGO <> 3 )  group by T6789.CODIGO, T6789.DENOMINACION, T7595.ANO, T7595.MES, T7595.ANOMES, T13200.CODIGO, T13200.DENOMINACION, T13212.CODIGO, T73182.CODIGO, T73182.DENOMINACION), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c9 as c10,      D1.c1 as c11,      D1.c10 as c12,      D1.c11 as c13,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T57997.DENOMINACION as c1,      T57997.CODIGO as c2,      T57997.IDIOMA as c3 from       LU_MES_IDI T57997 /* LU_MES_LOOKUP */ ), SAWITH3 AS (select D1.c1 as c1,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c14 as c14,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c4 as c4,                D1.c5 as c5,                D1.c6 as c6,                D1.c7 as c7,                D1.c8 as c8,                D2.c1 as c9,                D1.c9 as c10,                D1.c10 as c11,                D1.c11 as c12,                D1.c12 as c13,                D1.c13 as c14,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c9, D1.c10, D1.c12, D1.c13, D2.c1,D1.c33 ORDER BY D1.c2 ASC,  D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c9 ASC, D1.c10 ASC, D1.c12 ASC, D1.c13 ASC, D2.c1 ASC) as c15           from                 SAWITH1 D1 inner join SAWITH2 D2 On D1.c9 = D2.c2 and D1.c13 = D2.c3      ) D1 where  ( D1.c15 = 1 ) ) select D1.c1 ||'';''||  D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      sum(D1.c12) as c12,      D1.c33 as c33 from       SAWITH3 D1 group by c1,c4,c5,c6,c7,c8,c9,c10,c11,c33 order by c1, c7, c8, c11, c10, c4, c6,  c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.19047','Lozano','2021-07-27 13:55:20',NULL,38),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152',NULL,'csv','Indicador IC152.csv','WITH  SAWITH0 AS (select count(T101251.ID) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';'' || D1.c3||'';'' || D1.c4||'';'' || D1.c5||'';'' || D1.c6||'';'' || D1.c7||'';''|| D1.c8||'';''|| D1.c9||'';''|| D1.c10||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.234049','Lozano','2021-07-27 13:55:20',NULL,39),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A1',NULL,'csv','Indicador IC152A1.csv','WITH  SAWITH0 AS (select sum(T101251.SUPPARCELA) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';'' || D1.c3||'';'' || D1.c4||'';'' || D1.c5||'';'' || D1.c6||'';'' || D1.c7||'';''|| D1.c8||'';''|| D1.c9||'';''|| D1.c10||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.277606','Lozano','2021-07-27 13:55:20',NULL,40),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A2',NULL,'csv','Indicador IC152A2.csv','WITH  SAWITH0 AS (select sum(T101251.SUPCONSTR) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end , T101251.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select   	       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10, 	 D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.321098','Lozano','2021-07-27 13:55:20',NULL,41);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A3',NULL,'csv','Indicador IC152A3.csv','WITH  SAWITH0 AS (select sum(T101251.VALORINVENT) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.366692','Lozano','2021-07-27 13:55:20',NULL,42),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A1',NULL,'csv','Indicador IC153A1.csv','WITH  SAWITH0 AS (select count(T16529.ORDNUM) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T16638.CODIGO as c4,      T16638.DENOMINACION as c5,      T16701.CODIGO as c6,      T16701.DENOMINACION as c7,      T16722.CODIGO as c8,      T16722.DENOMINACION as c9,      T6789.CODIGO as c10,      T7594.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_POTTU2_IDI T16701,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16701.IDIOMA and T16529.DTBA = T17978.DTBA and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.POT_TU2 = T16701.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16722.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T16638.CODIGO, T16638.DENOMINACION, T16701.CODIGO, T16701.DENOMINACION, T16722.CODIGO, T16722.DENOMINACION, T17978.DTBA, T55787.DT,T7594.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c1 as c10,      D1.c10 as c12,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D2.c1 as c2,      D3.c1 as c3,      D1.c2 as c4,      D1.c3 as c5,      D1.c4 as c6,      D1.c5 as c7,      D1.c6 as c8,      D1.c7 as c9,      D1.c8 as c10,      D1.c9 as c11,      D1.c10 as c12,      D1.c12 as c14,      D1.c33 as c33,      ROW_NUMBER() OVER (PARTITION BY D1.c4, D1.c5, D1.c6, D1.c7, D1.c3, D1.c2, D2.c1, D3.c1, D1.c8, D1.c9,D1.c33 ORDER BY D1.c4 DESC, D1.c5 DESC, D1.c6 DESC, D1.c7 DESC, D1.c3 DESC, D1.c2 DESC, D2.c1 DESC, D3.c1 DESC, D1.c8 DESC, D1.c9 DESC) as c15 from       (           SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c12 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c12 = D3.c3), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c14 as c14,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D1.c4 as c4,                D1.c5 as c5,                D1.c6 as c6,                D1.c7 as c7,                D1.c8 as c8,                D1.c9 as c9,                D1.c10 as c10,                D1.c11 as c11,                D1.c12 as c12,                sum(case D1.c15 when 1 then D1.c12 else NULL end ) over (partition by D1.c6, D1.c7, D1.c8, D1.c9, D1.c5, D1.c4, D1.c2, D1.c3,D1.c2,D1.c33)  as c13,                D1.c14 as c14,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c9, D1.c10, D1.c11, D1.c14, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c9 ASC, D1.c10 ASC, D1.c11 ASC, D1.c14 ASC) as c15           from                 SAWITH4 D1      ) D1 where  ( D1.c15 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4  ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c13 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c4, c2, c5, c3, c6, c7, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.413657','Lozano','2021-07-27 13:55:20',NULL,43),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A2',NULL,'csv','Indicador IC153A2.csv','WITH  SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T17978.DTBA as c2,      T6789.CODIGO as c3,      T6789.DENOMINACION as c4,      T16529.ANO as c33 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T6789.CODIGO = T16638.IDIOMA and T7594.ANO = T16529.ANO and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Valenciano''  and T16529.DTBA = T17978.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T16529.ANO), SAWITH1 AS (select sum(T21230.NUMPERSONAS) as c1,      T17978.DTBA as c2,      T6789.CODIGO as c3,      T6789.DENOMINACION as c4 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T17978.DT = T21230.DIS and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T21230.SEXO and T6789.DENOMINACION = ''Valenciano'' and T17978.DTBA = T21230.DISBAR and T21230.ANYO = 2016 and T21230.ANYO = T24742.ANO and T21230.DIA = T24742.DIADELMES and T21230.MES = T24742.MES and T24742.ANO = 2016 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      cast(D1.c1 * 100 / nullif( D2.c1, 0) as  DOUBLE PRECISION  ) as c3,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c4,      D1.c33 as c33 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c3 = D2.c3 and D1.c2 = D2.c2 and  SYS_OP_MAP_NONNULL(D1.c4) = SYS_OP_MAP_NONNULL(D2.c4) ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D1.c3 as c4,                D1.c4 as c5,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c4, D2.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c4 ASC, D2.c1 ASC) as c6           from                 SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c4 = D2.c3      ) D1 where  ( D1.c6 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c3, c2 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.45704','Lozano','2021-07-27 13:55:20',NULL,44),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A3',NULL,'csv','Indicador IC153A3.csv','WITH  SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T6789.CODIGO as c4, 	 T7594.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_TIPODNI_IDI T16713,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16713.IDIOMA and T16529.DTBA = T17978.DTBA and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.TIPODNI = T16713.CODIGO and T6789.DENOMINACION = ''Castellano''  and T16529.TIPODNI = 1 and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16713.CODIGO = 1 and T16722.CODIGO = 1 and T16529.ANYMAT <= 2015 - 11 and T16638.CODIGO <= 2015 - 11 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T55787.DT, T7594.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4,      D1.c4 as c5, 	 D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7, 	 D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7, 			   D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6, 	 D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.501004','Lozano','2021-07-27 13:55:20',NULL,45),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A4',NULL,'csv','Indicador IC153A4.csv','WITH  SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T6789.CODIGO as c4, 	 T7594.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_TIPODNI_IDI T16713,      LU_III_MAT_POTTU2_IDI T16701,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16713.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.CODIGO = T16701.IDIOMA and T16529.POT_TU2 = T16701.CODIGO and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.TIPODNI = T16713.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.TIPODNI = 1 and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16713.CODIGO = 1 and T16722.CODIGO = 1 and T16529.POT_TU2 >= 4 and T16701.CODIGO >= 4 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T55787.DT, T7594.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4,      D1.c4 as c5, 	 D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7, 	 D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7, 			   D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6, 	 D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.544781','Lozano','2021-07-27 13:55:20',NULL,46),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A6',NULL,'csv','Indicador IC153A6.csv','WITH  SAWITH0 AS (select avg(T58565.EDADM) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T6789.CODIGO as c4,      T58565.AÑO as c33 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_III_MAT_EDADM_DTBA T58565 where  ( T7594.ANO = T58565.AÑO and T6789.DENOMINACION = ''Castellano''  and T17978.DTBA = T58565.DTBA  )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T58565.AÑO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4,      D1.c4 as c5,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.588726','Lozano','2021-07-27 13:55:20',NULL,47),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153',NULL,'csv','Indicador IC153.csv','WITH  SAWITH0 AS (select count(T16529.ORDNUM) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T16722.CODIGO as c4,      T16722.DENOMINACION as c5,      T6789.CODIGO as c6,      T16529.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.DENOMINACION = ''Castellano''  and T16529.TIPOFIN = T16722.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T16722.CODIGO, T16722.DENOMINACION, T17978.DTBA, T55787.DT, T16529.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c6, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.631828','Lozano','2021-07-27 13:55:20',NULL,48),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A5',NULL,'csv','Indicador IC153A5.csv','WITH  SAWITH0 AS (select count(T16529.ORDNUM) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T16674.CODIGO as c4,      T16674.DENOMINACION as c5,      T6789.CODIGO as c6,      T16529.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_CILMO2_IDI T16674,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16674.IDIOMA and T16529.CIL_MO2 = T16674.CODIGO and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.DENOMINACION = ''Castellano''  and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 6 and T16722.CODIGO = 6 )  group by T6789.CODIGO, T6789.DENOMINACION, T16674.CODIGO, T16674.DENOMINACION, T17978.DTBA, T55787.DT, T16529.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.675131','Lozano','2021-07-27 13:55:20',NULL,49),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A1',NULL,'csv','Indicador IC006A1.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T54922.CODIGO as c4,      T54922.DENOMINACION as c5,      T6789.CODIGO as c6,       T55155.ANO as c33 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T6789.DENOMINACION = ''Castellano'' and T54922.CODIGO = T55155.GGRUP and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T54922.CODIGO, T54922.DENOMINACION, T55155.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c5, c2, c3, c6, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.718765','Lozano','2021-07-27 13:55:20',NULL,50),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A2',NULL,'csv','Indicador IC006A2.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T55112.DSECCDIV as c4,      T55112.SECCDIV as c5,      T6789.CODIGO as c6,      T55155.ANO as c33 from       V_LU_VI_IAE_RELSECCIONES T55112,      LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 2 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 2 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO), SAWITH1 AS (select count(T55155.NUMREF) as c1 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 2  and T55155.GGRUP = 2 ) ), SAWITH2 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D2.c1 as c7,      D1.c6 as c8,      D1.c33 as c33 from       SAWITH0 D1,      SAWITH1 D2), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c7, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.762029','Lozano','2021-07-27 13:55:20',NULL,51);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A3',NULL,'csv','Indicador IC006A3.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T55112.DSECCDIV as c4,      T55112.SECCDIV as c5,      T6789.CODIGO as c6,      T55155.ANO as c33 from       V_LU_VI_IAE_RELSECCIONES T55112,      LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 4 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 4 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO), SAWITH1 AS (select count(T55155.NUMREF) as c1 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 4  and T55155.GGRUP = 4 ) ), SAWITH2 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D2.c1 as c7,      D1.c6 as c8,      D1.c33 as c33 from       SAWITH0 D1,      SAWITH1 D2), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c7, c6, c4, c2, c3, c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.805391','Lozano','2021-07-27 13:55:20',NULL,52),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A4',NULL,'csv','Indicador IC006A4.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T55112.DSECCDIV as c4,      T55112.SECCDIV as c5,      T6789.CODIGO as c6,      T55155.ANO as c33 from       V_LU_VI_IAE_RELSECCIONES T55112,      LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 5 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 5 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO), SAWITH1 AS (select count(T55155.NUMREF) as c1 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 5  and T55155.GGRUP = 5 ) ), SAWITH2 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D2.c1 as c7,      D1.c6 as c8,      D1.c33 as c33 from       SAWITH0 D1,      SAWITH1 D2), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c7, c6, c4, c2, c3, c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.848579','Lozano','2021-07-27 13:55:20',NULL,53),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A2',NULL,'csv','Indicador IC110A2.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T18894.CODIGO as c4,      T18894.DENOMINACION as c5,      T6789.CODIGO as c6,      T18685.ANYO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T17978.DTBA = T18685.DISBAR and T18685.CODPOS = T21622.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18685.TIPLOC = T18894.CODIGO and T18685.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, T18894.DENOMINACION, T18685.ANYO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.89237','Lozano','2021-07-27 13:55:20',NULL,55),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A3',NULL,'csv','Indicador IC110A3.csv','WITH  SAWITH0 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDAMEN5 = 0 then ''Cap de 0-4'' else ''Algú de 0-4'' end  else case  when T18579.INDAMEN5 = 0 then ''Nadie de 0-4'' else ''Alguien de 0-4'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18579.INDAMEN5 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDAMEN5 = 0 then ''Cap de 0-4'' else ''Algú de 0-4'' end  else case  when T18579.INDAMEN5 = 0 then ''Nadie de 0-4'' else ''Alguien de 0-4'' end  end,T18579.ANO), OBICOMMON0 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), OBICOMMON1 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH1 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH0 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH2 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA0509 = 0 then ''Cap de 5-9'' else ''Algú de 5-9'' end  else case  when T18579.INDA0509 = 0 then ''Nadie de 5-9'' else ''Alguien de 5-9'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano'' and T18579.INDA0509 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA0509 = 0 then ''Cap de 5-9'' else ''Algú de 5-9'' end  else case  when T18579.INDA0509 = 0 then ''Nadie de 5-9'' else ''Alguien de 5-9'' end  end, T18579.ANO ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH4 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA1014 = 0 then ''Cap de 10-14'' else ''Algú de 10-14'' end  else case  when T18579.INDA1014 = 0 then ''Nadie de 10-14'' else ''Alguien de 10-14'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA1014 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA1014 = 0 then ''Cap de 10-14'' else ''Algú de 10-14'' end  else case  when T18579.INDA1014 = 0 then ''Nadie de 10-14'' else ''Alguien de 10-14'' end  end, T18579.ANO ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH4 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH6 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA1519 = 0 then ''Cap de 15-19'' else ''Algú de 15-19'' end  else case  when T18579.INDA1519 = 0 then ''Nadie de 15-19'' else ''Alguien de 15-19'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA1519 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA1519 = 0 then ''Cap de 15-19'' else ''Algú de 15-19'' end  else case  when T18579.INDA1519 = 0 then ''Nadie de 15-19'' else ''Alguien de 15-19'' end  end, T18579.ANO ), SAWITH7 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH6 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH8 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA2024 = 0 then ''Cap de 20-24'' else ''Algú de 20-24'' end  else case  when T18579.INDA2024 = 0 then ''Nadie de 20-24'' else ''Alguien de 20-24'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA2024 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA2024 = 0 then ''Cap de 20-24'' else ''Algú de 20-24'' end  else case  when T18579.INDA2024 = 0 then ''Nadie de 20-24'' else ''Alguien de 20-24'' end  end, T18579.ANO ), SAWITH9 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH8 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH10 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA2529 = 0 then ''Cap de 25-29'' else ''Algú de 25-29'' end  else case  when T18579.INDA2529 = 0 then ''Nadie de 25-29'' else ''Alguien de 25-29'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA2529 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA2529 = 0 then ''Cap de 25-29'' else ''Algú de 25-29'' end  else case  when T18579.INDA2529 = 0 then ''Nadie de 25-29'' else ''Alguien de 25-29'' end  end, T18579.ANO ), SAWITH11 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH10 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH12 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA3034 = 0 then ''Cap de 30-34'' else ''Algú de 30-34'' end  else case  when T18579.INDA3034 = 0 then ''Nadie de 30-34'' else ''Alguien de 30-34'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA3034 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA3034 = 0 then ''Cap de 30-34'' else ''Algú de 30-34'' end  else case  when T18579.INDA3034 = 0 then ''Nadie de 30-34'' else ''Alguien de 30-34'' end  end, T18579.ANO ), SAWITH13 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH12 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH14 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA3539 = 0 then ''Cap de 35-39'' else ''Algú de 35-39'' end  else case  when T18579.INDA3539 = 0 then ''Nadie de 35-39'' else ''Alguien de 35-39'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA3539 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA3539 = 0 then ''Cap de 35-39'' else ''Algú de 35-39'' end  else case  when T18579.INDA3539 = 0 then ''Nadie de 35-39'' else ''Alguien de 35-39'' end  end, T18579.ANO ), SAWITH15 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH14 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH16 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA4044 = 0 then ''Cap de 40-44'' else ''Algú de 40-44'' end  else case  when T18579.INDA4044 = 0 then ''Nadie de 40-44'' else ''Alguien de 40-44'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA4044 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA4044 = 0 then ''Cap de 40-44'' else ''Algú de 40-44'' end  else case  when T18579.INDA4044 = 0 then ''Nadie de 40-44'' else ''Alguien de 40-44'' end  end, T18579.ANO ), SAWITH17 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH16 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH18 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA4549 = 0 then ''Cap de 45-49'' else ''Algú de 45-49'' end  else case  when T18579.INDA4549 = 0 then ''Nadie de 45-49'' else ''Alguien de 45-49'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA4549 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA4549 = 0 then ''Cap de 45-49'' else ''Algú de 45-49'' end  else case  when T18579.INDA4549 = 0 then ''Nadie de 45-49'' else ''Alguien de 45-49'' end  end, T18579.ANO ), SAWITH19 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH18 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH20 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA5054 = 0 then ''Cap de 50-54'' else ''Algú de 50-54'' end  else case  when T18579.INDA5054 = 0 then ''Nadie de 50-54'' else ''Alguien de 50-54'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA5054 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA5054 = 0 then ''Cap de 50-54'' else ''Algú de 50-54'' end  else case  when T18579.INDA5054 = 0 then ''Nadie de 50-54'' else ''Alguien de 50-54'' end  end, T18579.ANO ), SAWITH21 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH20 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH22 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA5559 = 0 then ''Cap de 55-59'' else ''Algú de 55-59'' end  else case  when T18579.INDA5559 = 0 then ''Nadie de 55-59'' else ''Alguien de 55-59'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA5559 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA5559 = 0 then ''Cap de 55-59'' else ''Algú de 55-59'' end  else case  when T18579.INDA5559 = 0 then ''Nadie de 55-59'' else ''Alguien de 55-59'' end  end, T18579.ANO ), SAWITH23 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH22 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH24 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA6064 = 0 then ''Cap de 60-64'' else ''Algú de 60-64'' end  else case  when T18579.INDA6064 = 0 then ''Nadie de 60-64'' else ''Alguien de 60-64'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA6064 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA6064 = 0 then ''Cap de 60-64'' else ''Algú de 60-64'' end  else case  when T18579.INDA6064 = 0 then ''Nadie de 60-64'' else ''Alguien de 60-64'' end  end , T18579.ANO ), SAWITH25 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH24 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH26 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA6569 = 0 then ''Cap de 65-69'' else ''Algú de 65-69'' end  else case  when T18579.INDA6569 = 0 then ''Nadie de 65-69'' else ''Alguien de 65-69'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA6569 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA6569 = 0 then ''Cap de 65-69'' else ''Algú de 65-69'' end  else case  when T18579.INDA6569 = 0 then ''Nadie de 65-69'' else ''Alguien de 65-69'' end  end, T18579.ANO ), SAWITH27 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH26 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH28 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA7074 = 0 then ''Cap de 70-74'' else ''Algú de 70-74'' end  else case  when T18579.INDA7074 = 0 then ''Nadie de 70-74'' else ''Alguien de 70-74'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA7074 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA7074 = 0 then ''Cap de 70-74'' else ''Algú de 70-74'' end  else case  when T18579.INDA7074 = 0 then ''Nadie de 70-74'' else ''Alguien de 70-74'' end  end,T18579.ANO ), SAWITH29 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH28 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH30 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA7579 = 0 then ''Cap de 75-79'' else ''Algú de 75-79'' end  else case  when T18579.INDA7579 = 0 then ''Nadie de 75-79'' else ''Alguien de 75-79'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA7579 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA7579 = 0 then ''Cap de 75-79'' else ''Algú de 75-79'' end  else case  when T18579.INDA7579 = 0 then ''Nadie de 75-79'' else ''Alguien de 75-79'' end  end, T18579.ANO ), SAWITH31 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH30 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH32 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDAMAY80 = 0 then ''Cap major de 80 i més'' else ''Algun major de 80 i més'' end  else case  when T18579.INDAMAY80 = 0 then ''Ningún mayor de 80 y más'' else ''Algún mayor de 80 y más'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDAMAY80 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDAMAY80 = 0 then ''Cap major de 80 i més'' else ''Algun major de 80 i més'' end  else case  when T18579.INDAMAY80 = 0 then ''Ningún mayor de 80 y más'' else ''Algún mayor de 80 y más'' end  end, T18579.ANO ), SAWITH33 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH32 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) (select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH1 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH3 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH5 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH7 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH9 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH11 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH13 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH15 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH17 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH19 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH21 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH23 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH25 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH27 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH29 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH31 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH33 D1)','c1,c2,c3,c4,c5,c6,c7,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.01781','Lozano','2021-07-27 13:55:20',NULL,56),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC145A1',NULL,'csv','Indicador IC145A1.csv','select T6789.DENOMINACION ||'';''||      T8148.CODIGO ||'';''||      T8160.CODIGO ||'';''||      T8160.DENOMINACION ||'';''||      T7595.ANO ||'';''||      T7595.MES ||'';''||      T6789.CODIGO ||'';''||      replace(T8109.VALOR , '','' , ''.'') as c9 from       LU_I_MET_ESTMETEO T8148,      V_LU_TIEMPO_ANOMES T7595,      LU_IDIOMA T6789,      LU_I_MET_INDICADOR_IDI T8160,      FACT_I_MET_METEOANOMES T8109 where  ( T6789.CODIGO = T8160.IDIOMA and T8109.ESTMETEO = T8148.CODIGO and T7595.ANO = T8109.ANO and T7595.MES = T8109.MES and T6789.DENOMINACION = ''Castellano''  and T8109.ESTMETEO = 1 and T8109.INDICADOR = T8160.CODIGO and T8109.INDICADOR = 7 and T8148.CODIGO = 1 and T8160.CODIGO = 7 )','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2021-08-10 08:20:31.285943','Lozano','2021-07-27 13:55:20',NULL,62),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A4',NULL,'csv','Indicador IC110A4.csv','WITH  SAWITH0 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDS0024 = 0 then ''Cap membre de 0-24 anys'' else ''Només membres de 0-24 anys'' end  else case  when T18579.INDS0024 = 0 then ''Ningún miembro de 0-24 años'' else ''Solo miembros de 0-24 años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18579.INDS0024 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDS0024 = 0 then ''Cap membre de 0-24 anys'' else ''Només membres de 0-24 anys'' end  else case  when T18579.INDS0024 = 0 then ''Ningún miembro de 0-24 años'' else ''Solo miembros de 0-24 años'' end  end , T18579.ANO), OBICOMMON0 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), OBICOMMON1 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH1 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1, D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH0 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH2 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDS2564 = 0 then ''Cap membre de 25-64 anys'' else ''Només membres de 25-64 anys'' end  else case  when T18579.INDS2564 = 0 then ''Ningún miembro de 25-64 años'' else ''Solo miembros de 25-64 años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDS2564 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDS2564 = 0 then ''Cap membre de 25-64 anys'' else ''Només membres de 25-64 anys'' end  else case  when T18579.INDS2564 = 0 then ''Ningún miembro de 25-64 años'' else ''Solo miembros de 25-64 años'' end  end, T18579.ANO ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH2 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH4 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY65 = 0 then ''Cap membre de 65 i més anys'' else ''Només membres de 65 i més anys'' end  else case  when T18579.INDSMAY65 = 0 then ''Ningún miembro de 65 y más años'' else ''Solo miembros de 65 y más años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDSMAY65 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY65 = 0 then ''Cap membre de 65 i més anys'' else ''Només membres de 65 i més anys'' end  else case  when T18579.INDSMAY65 = 0 then ''Ningún miembro de 65 y más años'' else ''Solo miembros de 65 y más años'' end  end, T18579.ANO ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH4 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH6 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY80 = 0 then ''Cap membre de 80 i més anys'' else ''Només membres de 80 i més anys'' end  else case  when T18579.INDSMAY80 = 0 then ''Ningún miembro de 80 y más años'' else ''Solo miembros de 80 y más años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDSMAY80 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY80 = 0 then ''Cap membre de 80 i més anys'' else ''Només membres de 80 i més anys'' end  else case  when T18579.INDSMAY80 = 0 then ''Ningún miembro de 80 y más años'' else ''Solo miembros de 80 y más años'' end  end, T18579.ANO ), SAWITH7 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH6 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH8 AS ((select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH1 D1 union select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH3 D1 union select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH5 D1 union select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH7 D1)) select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c33 as c33 from       SAWITH8 D1 order by c1, c2, c3, c4, c5','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.067184','Lozano','2021-07-27 13:55:20',NULL,57),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A5',NULL,'csv','Indicador IC110A5.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T10746.CODIGO as c4,      T10746.DENOMINACION as c5,      case  when T19562.CODIGO < 19 then T19562.CODIGO else 19 end  as c6,      case  when T19562.CODIGO < 19 then T19562.DENOMINACION else case  when T6789.DENOMINACION = ''Castellano'' then ''90 y más'' else ''90 i més'' end  end  as c7,      T6789.CODIGO as c8,      T18685.ANYO as c33 from       LU_EDADAGR20PAD_IDI T19562,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T19562.IDIOMA and T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T6789.DENOMINACION = ''Castellano''  and T18685.EDADAGR = T19562.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T10746.CODIGO, T10746.DENOMINACION, T17978.DT, T17978.DTBA, case  when T19562.CODIGO < 19 then T19562.DENOMINACION else case  when T6789.DENOMINACION = ''Castellano'' then ''90 y más'' else ''90 i més'' end  end , case  when T19562.CODIGO < 19 then T19562.CODIGO else 19 end, T18685.ANYO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c1 as c9,      D1.c8 as c10,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c10, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c10 ASC, D2.c1 ASC, D3.c1 ASC) as c13           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c10 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c10 = D3.c3      ) D1 where  ( D1.c13 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c5, c3, c6, c7, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.111276','Lozano','2021-07-27 13:55:20',NULL,58),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A6',NULL,'csv','Indicador IC110A6.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T18846.CODIGO as c4,      T18846.DENOMINACION as c5,      T10746.CODIGO as c6,      T10746.DENOMINACION as c7,      T6789.CODIGO as c8,      T18685.ANYO as c33 from       LU_II_PAD_LUGNAC_IDI T18846,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T18846.IDIOMA and T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T6789.DENOMINACION = ''Castellano''  and T18685.LUGNAC = T18846.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T10746.CODIGO, T10746.DENOMINACION, T17978.DT, T17978.DTBA, T18846.CODIGO, T18846.DENOMINACION, T18685.ANYO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c7, c4, c2, c5, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.155505','Lozano','2021-07-27 13:55:20',NULL,59),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A7',NULL,'csv','Indicador IC110A7.csv','WITH  SAWITH0 AS (select distinct count(T18685.IDPADRON) over (partition by T6789.DENOMINACION, T10746.CODIGO, T17978.DTBA, T19568.CODIGO)  as c1,      T10746.CODIGO as c2,      T10746.DENOMINACION as c3,      T17978.DTBA as c4,      T19568.CODIGO as c5,      T19568.DENOMINACION as c6,      T17978.DT as c7,      T6789.CODIGO as c8,      count(T18685.IDPADRON) over (partition by T6789.DENOMINACION, T10746.CODIGO, T17978.DTBA, T56143.CODPAIS, T19568.CODIGO)  as c9,      T56143.CODPAIS as c10,      T18685.ANYO as c33 from       LU_EDADAGR3PAD_IDI T19568,      V_LU_GEO_PAIS_NACION T56143,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T19568.IDIOMA and T17978.DTBA = T18685.DISBAR and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T56143.IDIOMA and T18685.EDADAGR3 = T19568.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18685.PAINACDAD = T56143.CODPAIS and T18685.PAINACDAD <> 108 and T56143.CODPAIS <> 108 ) ), SAWITH1 AS (select 0 as c1,      D1.c7 as c2,      D1.c4 as c3,      D1.c6 as c4,      D1.c5 as c5,      D1.c10 as c6,      D1.c2 as c7,      D1.c3 as c8,      D1.c9 as c9,      D1.c1 as c10,      D1.c8 as c11,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c11 as c13,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c11, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3      ) D1 where  ( D1.c14 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c9, c10, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.198691','Lozano','2021-07-27 13:55:20',NULL,60),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC146A1',NULL,'csv','Indicador IC146A1.csv','select T6789.DENOMINACION ||'';''||      T8148.CODIGO ||'';''||      T8160.CODIGO ||'';''||      T8160.DENOMINACION ||'';''||      T7595.ANO ||'';''||      T7595.MES ||'';''||      T6789.CODIGO ||'';''||      replace(T8109.VALOR , '','' , ''.'') as c9 from       LU_I_MET_ESTMETEO T8148,      V_LU_TIEMPO_ANOMES T7595,      LU_IDIOMA T6789,      LU_I_MET_INDICADOR_IDI T8160,      FACT_I_MET_METEOANOMES T8109 where  ( T6789.CODIGO = T8160.IDIOMA and T8109.ESTMETEO = T8148.CODIGO and T7595.ANO = T8109.ANO and T7595.MES = T8109.MES and T6789.DENOMINACION = ''Castellano''  and T8109.ESTMETEO = 4 and T8109.INDICADOR = T8160.CODIGO and T8148.CODIGO = 4 and (T8109.INDICADOR in (28)) and (T8160.CODIGO in (28)) ) order by T7595.ANO, T7595.MES','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2021-08-10 08:20:31.330113','Lozano','2021-07-27 13:55:20',NULL,63);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A1',NULL,'csv','Indicador IC151A1.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select distinct T103896.NUMVIV1800 as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103896.DTBA ) ), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  as c4,      D1.c1 / nullif( D2.c1, 0) as c5,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  as c6 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.231006','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',87),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110',NULL,'csv','Indicador IC110.csv','WITH  SAWITH0 AS (select sum(T21230.NUMPERSONAS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Castellano'' and T10746.CODIGO = T21230.SEXO and T17978.DTBA = T21230.DISBAR )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c6 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.277715','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',88),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9',NULL,'csv','Indicador IC110A9.csv','WITH  SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T19610.IDIOMA and T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T18685.ANYO and T18685.EDAD100 = T19610.CODIGO )  group by T7594.ANO) select D1.c1 ||'';'' || D1.c2 || '';'' || D1.c3 from ( select 0 as c1,      D1.c2 as c2,      D1.c1 as c3 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3','N','sftp',NULL,NULL,'2021-08-10 08:20:32.320996','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',89),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9B',NULL,'csv','Indicador IC110A9B.csv','WITH  SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,      T10746.DENOMINACION as c2,      T7594.ANO as c3 from       LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T10746.IDIOMA and T6789.CODIGO = T19610.IDIOMA and T7594.ANO = T18685.ANYO and T6789.DENOMINACION = ''Castellano'' and T10746.CODIGO = T18685.SEXO and T18685.EDAD100 = T19610.CODIGO )  group by T7594.ANO, T10746.DENOMINACION) select D1.c1 || '';'' || D1.c2 || '';'' || D1.c3 || '';'' || D1.c4  from ( select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4 from       SAWITH0 D1 order by c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2021-08-10 08:20:32.364082','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',90),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9C',NULL,'csv','Indicador IC110A9C.csv','WITH  SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T10746.DENOMINACION as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T19610.IDIOMA and T18685.DIS = T55787.DT and T6789.DENOMINACION = ''Castellano'' and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T18685.EDAD100 = T19610.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T10746.DENOMINACION, T17978.DTBA, T55787.DT), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH4 D1 order by c1, c7, c4, c2, c5, c3, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,C8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.407032','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',91),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A1',NULL,'csv','Indicador IC149A1.csv','WITH  SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      case  when T104038.ANO <= 1800 then ''<=1800'' else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then ''1801-1900'' else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then ''1901-1920'' else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then ''1921-1940'' else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then ''1941-1960'' else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then ''1961-1970'' else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then ''1971-1980'' else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then ''1981-1990'' else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then ''1991-2000'' else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then ''2001-10'' else case  when T104038.ANO > 2010 then ''>2010'' end  end  end  end  end  end  end  end  end  end  end  as c5,      case  when T104038.ANO <= 1800 then 1 else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then 2 else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then 3 else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then 4 else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then 5 else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then 6 else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then 7 else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then 8 else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then 9 else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then 10 else case  when T104038.ANO > 2010 then 11 end  end  end  end  end  end  end  end  end  end  end  as c6,      T6789.CODIGO as c7 from       V_LU_TIEMPO_ANO T104038 /* LU_IV_CAT_ANY1 */ ,      V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_IV_CAT_VIV T103797 where  ( T17978.DTBA = T103797.DTBA and T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T103797.DTSC and T55787.DT = T103797.DT and T6789.DENOMINACION = ''Castellano'' and T103797.ANY1 = T104038.ANO )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT, case  when T104038.ANO <= 1800 then ''<=1800'' else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then ''1801-1900'' else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then ''1901-1920'' else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then ''1921-1940'' else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then ''1941-1960'' else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then ''1961-1970'' else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then ''1971-1980'' else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then ''1981-1990'' else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then ''1991-2000'' else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then ''2001-10'' else case  when T104038.ANO > 2010 then ''>2010'' end  end  end  end  end  end  end  end  end  end  end , case  when T104038.ANO <= 1800 then 1 else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then 2 else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then 3 else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then 4 else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then 5 else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then 6 else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then 7 else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then 8 else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then 9 else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then 10 else case  when T104038.ANO > 2010 then 11 end  end  end  end  end  end  end  end  end  end  end ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c1 as c7,      D1.c7 as c8 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3, c8, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2021-08-10 08:20:32.450449','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',92),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A2',NULL,'csv','Indicador IC149A2.csv','WITH  SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,      T103931.DENOMINACION as c2,      T55787.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       LU_IV_CAT_SUPERF_IDI T103931,      LU_IV_CAT_ANT1800_IDI T103907,      V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_IV_CAT_VIV T103797 where  ( T6789.CODIGO = T103931.IDIOMA and T55787.DT = T103797.DT and T17978.DTBA = T103797.DTBA and T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T103797.DTSC and T6789.CODIGO = T103907.IDIOMA and T103797.ANT1800 = T103907.CODIGO and T6789.DENOMINACION = ''Castellano'' and T103797.ANT1800 = 0 and T103797.SUPERF = T103931.CODIGO and T103907.CODIGO = 0 )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT, T103931.DENOMINACION), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c4 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH4 D1 order by c1, c7, c2, c5, c3, c6, c4 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.494064','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',93),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3',NULL,'csv','Indicador IC149A3.csv','WITH  SAWITH0 AS (select T103896.NUMVIV1800 as c1,      sum(T103896.VALCAT_TOT) as c2,      sum(T103896.VCATS_MED) as c3,      sum(T103896.VALCAT_MED) as c4,      sum(T103896.VCATC_MED) as c5,      T17978.DT as c6,      T17978.DTBA as c7,      T7594.ANO as c8,      T6789.CODIGO as c9 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103896.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103896.NUMVIV1800), SAWITH1 AS (select 0 as c1,      D1.c6 as c2,      D1.c7 as c3,      D1.c8 as c4,      D1.c5 as c5,      D1.c4 as c6,      D1.c3 as c7,      D1.c2 as c8,      D1.c2 / nullif( D1.c1, 0) as c9,      D1.c1 as c10,      D1.c9 as c11 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c11 as c13,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c11, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3      ) D1 where  ( D1.c14 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2021-08-10 08:20:32.537665','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',94),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A4',NULL,'csv','Indicador IC149A4.csv','WITH  SAWITH0 AS (select sum(T103867.NUMVIV1800) as c1,      T103955.DENOMINACION as c2,      T17978.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_IV_CAT_VCATA_IDI T103955,      FACT_IV_CAT_VIV_AGRDTBAVC T103867 where  ( T6789.CODIGO = T103955.IDIOMA and T7594.ANO = T103867.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103867.DTBA and T103867.VALCATA = T103955.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103955.DENOMINACION), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c4 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH4 D1 order by c1, c7, c5, c3, c6, c4, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.581011','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',95),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150',NULL,'csv','Indicador IC150.csv','WITH  SAWITH0 AS (select sum(T102892.NUMSOL) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRDTBA T102892 where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D2.c1 as c2,      D3.c1 as c3,      D1.c2 as c4,      D1.c3 as c5,      D1.c4 as c6,      D1.c5 as c7,      D1.c7 as c9,      ROW_NUMBER() OVER (PARTITION BY D1.c4, D1.c3, D1.c2, D2.c1, D3.c1 ORDER BY D1.c4 DESC, D1.c3 DESC, D1.c2 DESC, D2.c1 DESC, D3.c1 DESC) as c10 from       (           SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D1.c4 as c4,                D1.c5 as c5,                D1.c6 as c6,                D1.c7 as c7,                sum(case D1.c10 when 1 then D1.c7 else NULL end ) over ()  as c8,                D1.c9 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c9 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c9 ASC) as c10           from                 SAWITH4 D1      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.624064','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',96);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A2',NULL,'csv','Indicador IC150A2.csv','WITH  SAWITH0 AS (select sum(T120156.NUMSOL) as c1,      sum(T120156.VCATS) as c2,      T7594.ANO as c3 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRTOTALDT T120156 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120156.ANO )  group by T7594.ANO) select D1.c1 || '';'' || D1.c2 || '';'' || D1.c3  from ( select distinct 0 as c1,      D1.c3 as c2,      round(D1.c2 / nullif( D1.c1, 0) , 2) as c3 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3','N','sftp',NULL,NULL,'2021-08-10 08:20:32.667228','Lozano-32054','2021-07-27 13:55:20','Lozano-32054',97),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A2B',NULL,'csv','Indicador IC150A2B.csv','WITH  SAWITH0 AS (select sum(T102892.NUMSOL) as c1,      sum(T102892.VCATS) as c2,      T17978.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRDTBA T102892 where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c3 as c2,      D1.c4 as c3,      D1.c5 as c4,      D1.c1 as c5,      D1.c2 as c6,      round(D1.c2 / nullif( D1.c1, 0) , 2) as c7,      D1.c6 as c8 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH4 D1 order by c1, c6, c2, c4, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.71015','Lozano-32054','2021-07-27 13:55:20','Lozano-32054',98),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A3',NULL,'csv','Indicador IC150A3.csv','WITH  SAWITH0 AS (select sum(T120156.SUPSOL) as c1,      sum(T120156.VCATS) as c2,      T7594.ANO as c3 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRTOTALDT T120156 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120156.ANO )  group by T7594.ANO) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 from ( select 0 as c1,      D1.c3 as c2,      D1.c1 as c3,      D1.c2 as c4,      D1.c2 / nullif( D1.c1, 0) as c5 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5','N','sftp',NULL,NULL,'2021-08-10 08:20:32.753223','Lozano-32054','2021-07-27 13:55:20','Lozano-32054',99),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A3B',NULL,'csv','Indicador IC150A3B.csv','WITH  SAWITH0 AS (select sum(T102892.SUPSOL) as c1,      sum(T102892.VCATS) as c2,      T17978.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRDTBA T102892 where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c3 as c2,      D1.c4 as c3,      D1.c5 as c4,      D1.c1 as c5,      D1.c2 as c6,      D1.c2 / nullif( D1.c1, 0) as c7,      D1.c6 as c8 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.796307','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',100),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151',NULL,'csv','Indicador IC151.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c6 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.839367','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',101),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A2',NULL,'csv','Indicador IC151A2.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.DT = T55787.DT and T16529.DTBA = T17978.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  as c4,      D1.c1 / nullif( D2.c1, 0) as c5,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  as c6 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.882501','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',102),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC098',NULL,'csv','Indicador IC098.csv','WITH  SAWITH0 AS (select sum(T43966.VALOR) as c1,      T43989.DENOMINACION as c2,      T55787.DT as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       LU_VIII_AGPO_IND_IDI T43989,      V_LU_DIVMUN_DT T55787,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_AGPO_FACTDT T43966 where  ( T6789.CODIGO = T43989.IDIOMA and T7594.ANO = T43966.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T43966.MUNICIPIO and T43966.DISTRITO = T55787.DT and T6789.DENOMINACION = ''Castellano'' and T43966.INDICADOR = T43989.CODIGO and T43966.INDICADOR = 3 and T43989.CODIGO = 3 )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T43989.DENOMINACION, T55787.DT), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c6 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D1.c4 as c5,                D1.c5 as c6,                D1.c6 as c7,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC) as c8           from                 SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c6 = D2.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6 from       SAWITH3 D1 order by c1, c5, c4, c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6','N','sftp',NULL,NULL,'2021-08-10 08:20:32.925899','Lozano-32016','2021-07-27 13:55:20','Lozano-32016',103),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A1',NULL,'csv','Indicador IC110A1.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T18894.DENOMINACION as c2,      T7594.ANO as c3 from       LU_EDADES T22191,      LU_II_PAD_TIPVIV_IDI T18894,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T18685.EDAD = T22191.EDAD and T7594.ANO = T18685.ANYO and T6789.CODIGO = T19610.IDIOMA and T18685.EDAD100 = T19610.CODIGO and T6789.CODIGO = T18894.IDIOMA and T18685.TIPLOC = T18894.CODIGO and T6789.DENOMINACION = ''Castellano'' and T18894.DENOMINACION = ''Vivienda familiar'' and T19610.CODIGO = T22191.EDAD100 and T18685.EDAD <= 17 and T22191.EDAD <= 17 )  group by T7594.ANO, T18894.DENOMINACION) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4  from ( select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4 from       SAWITH0 D1 order by c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2021-08-10 08:20:32.96903','Lozano-32016','2021-07-27 13:55:20','Lozano-32016',104),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149',NULL,'csv','Indicador IC49.csv','WITH  SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_IV_CAT_VIV T103797 where  ( T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T17978.DTBA = T103797.DTBA and T22801.DTSC = T103797.DTSC and T6789.DENOMINACION = ''Castellano''  and T55787.DT = T103797.DT  )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT), SAWITH1 AS (select sum(T103843.NUMVIV) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRANYDTBA T103843 where  ( T7594.ANO = T103843.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103843.DTBA  )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH2 AS (select distinct T103896.NUMVIV1800 as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano''  and T17978.DTBA = T103896.DTBA ) ), SAWITH3 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 when D3.c2 is not null then D3.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 when D3.c3 is not null then D3.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 when D3.c4 is not null then D3.c4 end  as c4,      D1.c1 as c5,      D2.c1 as c6,      D3.c1 as c7,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 when D3.c5 is not null then D3.c5 end  as c8 from       (           SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ) full outer join SAWITH2 D3 On D3.c2 = case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  and D3.c3 = case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  and D3.c4 = case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  and D3.c5 = case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  and  SYS_OP_MAP_NONNULL(D3.c6) = SYS_OP_MAP_NONNULL(case  when D1.c6 is not null then D1.c6 when D2.c6 is not null then D2.c6 end ) ), SAWITH4 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH5 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH6 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH3 D1 inner join SAWITH4 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH5 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH6 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2021-08-10 08:20:33.011967','Lozano-32016','2021-07-27 13:55:20','Lozano-32016',105),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A21',NULL,'csv','Indicador IC151A21.csv','select sup_aparcamientos.c2 ||'';''|| sup_aparcamientos.c1/num_turismos.c1 from (select sum(T120150.SUPCONS) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRTOTALDT T120150 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120150.ANO )  group by T7594.ANO order by c2) sup_aparcamientos,  (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T6789.CODIGO = T16638.IDIOMA and T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T16529.ANO and T16529.ANYMAT = T16638.CODIGO )  group by T7594.ANO order by c2) num_turismos where sup_aparcamientos.c2=num_turismos.c2','c1,c2','N','sftp',NULL,NULL,'2021-08-10 08:20:33.055405','Lozano-32055','2021-07-27 13:55:20','Lozano-32055',106);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A2',NULL,'csv','Indicador IC151A2.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.DT = T55787.DT and T16529.DTBA = T17978.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  as c4,      D1.c1 / nullif( D2.c1, 0) as c5,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  as c6 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:33.098404','Lozano-32055','2021-07-27 13:55:20','Lozano-32055',107),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3',NULL,'csv','Indicador IC149A3.csv','WITH  SAWITH0 AS (select T103896.NUMVIV1800 as c1,      sum(T103896.VALCAT_TOT) as c2,      sum(T103896.VCATS_MED) as c3,      sum(T103896.VALCAT_MED) as c4,      sum(T103896.VCATC_MED) as c5,      T17978.DT as c6,      T17978.DTBA as c7,      T7594.ANO as c8,      T6789.CODIGO as c9 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103896.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103896.NUMVIV1800), SAWITH1 AS (select 0 as c1,      D1.c6 as c2,      D1.c7 as c3,      D1.c8 as c4,      D1.c5 as c5,      D1.c4 as c6,      D1.c3 as c7,      D1.c2 as c8,      D1.c2 / nullif( D1.c1, 0) as c9,      D1.c1 as c10,      D1.c9 as c11 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c11 as c13,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c11, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3      ) D1 where  ( D1.c14 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2021-08-10 08:20:33.141728','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',108),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3_1',NULL,'csv','Indicador IC149A3_1.csv','WITH  SAWITH0 AS (select sum(T120168.VALCAT_MED) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCTOTALDT T120168 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120168.ANO )  group by T7594.ANO) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 from ( select 0 as c1,      D1.c2 as c2,      D1.c1 as c3 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2021-08-10 08:20:33.18599','Lozano-32055','2021-07-27 13:55:20','Lozano-32055',109),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/integracion/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/integracion/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC097',NULL,'csv','Indicador IC097.csv','WITH  SAWITH0 AS (select sum(T43974.VALOR) as c1,      T43989.DENOMINACION as c2,      T44001.CODIGO as c3,      T44001.DENOMINACION as c4,      T7594.ANO as c5 from       LU_VIII_AGPO_TIPAB_IDI T44001,      LU_VIII_AGPO_IND_IDI T43989,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_AGPO_INFABONADOS T43974 where  ( T6789.CODIGO = T44001.IDIOMA and T7594.ANO = T43974.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T43974.MUNICIPIO and T6789.CODIGO = T43989.IDIOMA and T43974.INDICADOR = T43989.CODIGO and T6789.DENOMINACION = ''Castellano'' and T43974.TIPOABONADO = T44001.CODIGO and T43974.TIPOABONADO = 1 and T43989.DENOMINACION = ''Agua facturada miles m3'' and T44001.CODIGO = 1 )  group by T7594.ANO, T43989.DENOMINACION, T44001.CODIGO, T44001.DENOMINACION) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6  from ( select distinct 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 * 1000000 / 365 as c6 from       SAWITH0 D1 order by c5, c2, c4, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6','N','sftp',NULL,NULL,'2021-08-10 08:20:32.18786','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',86),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SEDE',NULL,NULL,'/integracion/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/integracion/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC15',NULL,'csv','Indicador ISTIC15','select count(*) ||'';''||  TO_CHAR(dindate, ''YYYY-MM'') from pro2_ocs.docmeta d, pro2_ocs.revisions r where xaytoestadoelaboracionNTI = ''EE02'' and ddoctitle like ''Padron%'' and dindate >= (SELECT trunc(trunc(sysdate,''month'')-1,''month'') FROM DUAL) AND dindate < (SELECT trunc((sysdate),''month'') FROM DUAL) and d.did = r.did group by TO_CHAR(dindate, ''YYYY-MM'')','resultado,fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.501965','Ticket-23401','2021-07-27 13:55:20',NULL,69),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC01_#ddMMyyyy#',NULL,'csv','Indicador ISTIC01','select count(*)||'';''||TO_CHAR(Fecha_acceso, ''YYYY-MM'') from acceso_sede where fecha_acceso >=trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'')  AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''), ''MM'') group by TO_CHAR(Fecha_acceso, ''YYYY-MM'')','Accesos_autenticados,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:31.844888','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',77),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC10_#ddMMyyyy#',NULL,'csv','Indicador ISTIC10','select count(*) ||'';''|| TO_CHAR(Fecha_acceso, ''YYYY-MM'') from acceso_sede  where EXTRACT(month FROM Fecha_acceso) = (EXTRACT(month FROM TO_DATE(''#fecha#'',''yyyyMMdd''))-1) and EXTRACT(year FROM Fecha_acceso) = EXTRACT(year FROM TO_DATE(''#fecha#'',''yyyyMMdd'')) group by TO_CHAR(Fecha_acceso, ''YYYY-MM'')','Accesos_autenticados,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:31.888053','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',79),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC12_#ddMMyyyy#',NULL,'csv','Indicador ISTIC12','SELECT ta.autoridad ||'';''|| count(*) ||'';''|| TO_CHAR(fecha_acceso,''YYYY-MM-DD'') AS Num_Accesos FROM acceso_sede acs, sede_electro.tipo_acceso ta WHERE acs.id_tipo_acceso = ta.id_tipo_acceso  AND TO_CHAR(acs.FECHA_ACCESO, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') - 1, ''YYYY-MM-DD'') AND (acs.id_tipo_acceso is not null) GROUP BY ta.autoridad, TO_CHAR(fecha_acceso,''YYYY-MM-DD'') ORDER BY Num_accesos DESC','Autoridad,Num_Accesos,Fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.930958','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',80),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC13_#ddMMyyyy#',NULL,'csv','Indicador ISTIC13','select Asunto ||'';''|| count(asunto) ||'';''|| TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') from instancia_presentada where TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') -1, ''YYYY-MM-DD'')  group by Asunto, TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') order by TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') desc','Asunto,Instancias,Fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.973724','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',81),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC14_#ddMMyyyy#',NULL,'csv','Indicador ISTIC14','select count(*) ||'';''|| TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') from tramite  where TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') -1, ''YYYY-MM-DD'') group by TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') order by TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') desc','Tramites,Fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:32.016525','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',82);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC16_#ddMMyyyy#',NULL,'csv','Indicador ISTIC16','select TO_CHAR(primer_acceso, ''YYYY-MM'') ||'';''|| count(*) from (       select min(fecha_acceso) as primer_acceso, num_documento_usuario        from  acceso_sede        where id_resultado_login = 1        group by num_documento_usuario) where  primer_acceso >=trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'')  AND primer_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''), ''MM'') group by TO_CHAR(primer_acceso, ''YYYY-MM'') order by TO_CHAR(primer_acceso, ''YYYY-MM'') desc','Primer_acceso,Numero','S','sftp',NULL,NULL,'2021-08-10 08:20:32.059316','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',83),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/integracion/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC53',NULL,'csv','Indicador ISTIC53','SELECT count(*) ||'';''||  TO_CHAR(fecha_registro, ''YYYY-MM'') FROM instancia_presentada WHERE nif_interesado <> nif_tramitador AND fecha_registro >=(SELECT trunc(trunc(sysdate,''month'')-1,''month'') FROM DUAL) AND fecha_registro  < (SELECT trunc((sysdate),''month'') FROM DUAL) GROUP BY TO_CHAR(fecha_registro, ''YYYY-MM'')','resultado,fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.587895','Ticket-23401','2021-07-27 13:55:20',NULL,71),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02C_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02C_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad = ''DNIe'' group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.446975','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',162),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02C_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02C_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad = ''DNIe'' group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.49008','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',163),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02B_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02B_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad = (''Cl@ve'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.403496','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',161),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02D_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02D_mensual','SELECT autoridad ||'';''|| replace(porcentaje,'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad not in (''FNMT-Ceres'',''ACCV'',''Cl@ve'',''DNIe'')','Autoridad,Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.532994','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',164),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02D_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02D_anual','SELECT autoridad ||'';''|| replace(porcentaje,'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad not in (''FNMT-Ceres'',''ACCV'',''Cl@ve'',''DNIe'')','Autoridad,Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.576146','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',165),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/integracion/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC20',NULL,'csv','Indicador ISTIC20','SELECT * FROM (select Asunto ||'';''||  count(asunto) ||'';''||  TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') from instancia_presentada where TO_CHAR(Fecha_Registro, ''YYYYMMDD'') = TO_CHAR(sysdate -1, ''YYYYMMDD'') group by Asunto, TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') order by count(asunto) desc) WHERE ROWNUM <= 10','asunto,instancias,fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.54484','Ticket-23401','2021-07-27 13:55:20',NULL,70),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02A_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02A_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad IN (''FNMT-Ceres'',''ACCV'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.27443','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',158),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02A_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02A_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad IN (''FNMT-Ceres'',''ACCV'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.31713','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',159);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/integracion/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02B_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02B_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(Fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(Fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad = (''Cl@ve'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.359934','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',160),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','PostGis_Plataforma',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_TRAMOS_CONGESTION/','/integracion/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_TRAMOS_CONGESTION/',NULL,'tramos_congestion',NULL,'csv','tramos_congestion','select concat(id_tramo,'';'',desc_tramo_cas,'';'', desc_tramo_val) from t_d_trafico_tramos_congestiones;','id_tramo, desc_tramo_cas, desc_tramo_val','S','sftp',NULL,NULL,'2021-08-10 08:20:31.758499','toni-28427','2021-07-27 13:55:20',NULL,75),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','SQL_Server_Trafico',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_CONGESTION/','/integracion/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_CONGESTION/',NULL,'congestion_diaria',NULL,'csv','congestion_diaria','SELECT concat( rec.Rec, '';'',''#fecha#'', '';'', round(convert(float,sum([PorcC]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' , round(convert(float,sum([PorcD]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' , round(convert(float,sum([PorcFX]))/convert(float,(SELECT count(rec2.Rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' , ( 100-(round(convert(float,sum([PorcC]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0)+  round(convert(float,sum([PorcD]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0)+  round(sum(convert(float,[PorcFX]))/convert(float,(SELECT count(rec2.Rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0))) ) FROM [EstadisticasGIP].[dbo].[Recorr] rec WHERE convert(date,DATEADD(HOUR,-1,Hora))=CAST(''#fecha#'' as date) group by rec.Rec order by rec.Rec;','recorrido, dia, porc_denso, porc_congestion, porc_cortado, porc_fluido','S','sftp',NULL,NULL,'2021-08-10 08:20:31.715839','toni-28427','2021-07-27 13:55:20',NULL,74),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','SQL_Server_Trafico',NULL,NULL,'/integracion/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_INTENSIDAD/','/integracion/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_INTENSIDAD/',NULL,'intensidad_diaria',NULL,'csv','intensidad_diaria','SELECT concat(dea.nombre,'';'', convert(date,dapm.FechaHora),'';'', (sum(dapm.intensidad)/[PROPERTY_TRAFICO_NUM_MEDIDAS_HOR])) FROM  [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado] dea,  [WEB_TRAFICO].[dbo].[Datos_Aforados_PM] dapm where dea.idTA=dapm.iDTA and dea.estado=''A''and convert(date,dapm.FechaHora)=CAST(''#fecha#'' as date) group by dea.nombre, convert(date,dapm.FechaHora)','id_tramo, fecha, intensidad_horaria','S','sftp',NULL,NULL,'2021-08-10 08:20:31.630582','toni-28427','2021-07-27 13:55:20','Ivan 34096',72),
			('VLCi_ETL_INCO','FTP','FTP Sertic','SMC*INCO*','//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi','/integracion/01.fichOrig/VLCi_ETL_INCO/','/integracion/02.fichBackup/VLCi_ETL_INCO/',NULL,'SMC-INCO-yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2023-09-22 08:20:29.404222','Ticket 1914','2023-09-22 08:20:29.404222','Ticket 1914',230);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('py_calidadambiental_sonometros_zas_WOO', 'FTP', 'FTP Sertic', '*WOO', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.WOO', 'N', 'WOO', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_MAN', 'FTP', 'FTP Sertic', '*MAN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.MAN', 'N', 'MAN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_SER', 'FTP', 'FTP Sertic', '*SER', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.SER', 'N', 'SER', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_HON', 'FTP', 'FTP Sertic', '*HON', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.HON', 'N', 'HON', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_CED', 'FTP', 'FTP Sertic', '*CED', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.CED', 'N', 'CED', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_PIN', 'FTP', 'FTP Sertic', '*PIN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.PIN', 'N', 'PIN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_SEN', 'FTP', 'FTP Sertic', '*SEN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.SEN', 'N', 'SEN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_QUA', 'FTP', 'FTP Sertic', '*QUA', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.QUA', 'N', 'QUA', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_BUE', 'FTP', 'FTP Sertic', '*BUE', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.BUE', 'N', 'BUE', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_UNI', 'FTP', 'FTP Sertic', '*UNI', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.UNI', 'N', 'UNI', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_xlsx', 'FTP', 'FTP Sertic', '*xlsx', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.xlsx', 'N', 'xlsx', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_xls', 'FTP', 'FTP Sertic', '*xls', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/integracion/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/integracion/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.xls', 'N', 'xls', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('py_trafico_intensidad_validada', 'BBDD', 'SQL_Server_Trafico', NULL, NULL, '/integracion/01.fichOrig/VLCI_PY_TRAFICO_VALIDADADO/', '/integracion/02.fichBackup/VLCI_PY_TRAFICO_VALIDADADO/', NULL, 'intensidad_trafico_validada_30_#ddMMyyyyHHmmss#', NULL, 'csv', 'intensidad_trafico_validada_30', 
			'SELECT concat(''Kpi-Accesos-Ciudad-Coches'', '';'', actual.idata, '';'', descripciones.descripcion, '';'', descripciones.descripcion_corta, '';'', descripciones.nombre_corto, '';'', actual.int_actual, '';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A373'',''A406'',''A72'',''A59'',''A1'',''A82'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA,vid.DÍA) actual
			inner join (
    			select 4 as idATA, ''Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)'' as descripcion, ''Accés Barcelona (entrada i eixida)'' as descripcion_corta, ''Accés Barcelona'' as nombre_corto union
    			select 57 as idATA, ''Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)''  as descripcion, ''Accés a Arxiduc Carles pel Camí nou de Picanya'' as descripcion_corta, ''Arxiduc Carles'' as nombre_corto union
    			select 70 as idATA, ''Av. del Cid (Entre V-30 i Tres Creus)''  as descripcion, ''Av. del Cid'' as descripcion_corta, ''Av. del Cid'' as nombre_corto union
    			select 78 as idATA, ''Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)''  as descripcion, ''Corts Valencianes (Accés per CV-35)'' as descripcion_corta, ''Corts Valencianes'' as nombre_corto union
    			select 349 as idATA, ''Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)''      as descripcion, ''Accés per V-31 (Pista de Silla)'' as descripcion_corta, ''Pista de Silla'' as nombre_corto union
    			select 379 as idATA, ''Prolongació Joan XXIII (Entre Germans Machado i Salvador Cerveró)''  as descripcion, ''Prolongació Joan XXIII'' as descripcion_corta, ''Joan XXIII'' as nombre_corto) as descripciones
    			on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Accesos-Vias-Coches'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion_corta,'';'', descripciones.nombre_corto,'';'', actual.int_actual,'';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A116'',''A121'',''A97'',''A117'',''A187'',''A51'') 
			and vid.DÍA = CAST(''#fecha#'' as datetime)   
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 50 as idATA, ''Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)''  as descripcion, ''Av. Blasco Ibáñez'' as descripcion_corta, ''Blasco Ibáñez'' as nombre_corto union
				select 93 as idATA, ''Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)''  as descripcion, ''Av. Dr. Peset Aleixandre'' as descripcion_corta, ''Dr. Peset Aleixandre'' as nombre_corto union
				select 111 as idATA, ''Av. de Giorgeta (Entre Sant Vicent i Jesús)''  as descripcion, ''Av. de Giorgeta'' as descripcion_corta, ''Giorgeta'' as nombre_corto union
				select 112 as idATA, ''Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)''  as descripcion, ''Gran Via de Ferran el Catòlic'' as descripcion_corta, ''Ferran el Catòlic'' as nombre_corto union
				select 116 as idATA, ''Gran Via del Marqués del Túria (Entre Pont d''''Aragó i Hernan Cortés)''  as descripcion, ''Gran Via del Marqués del Túria'' as descripcion_corta, ''Marqués del Túria'' as nombre_corto union
				select 177 as idATA, ''Av. de Peris i Valero (Entre Ausiàs March i Sapadors)''  as descripcion, ''Av. de Peris i Valero'' as descripcion_corta, ''Peris i Valero'' as nombre_corto) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Accesos-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			FROM
			(select afo.Descripcion as descripcion,afo.idATA as idata,  sum(vid.IMD*afo.Coeficiente/100) as intactual,vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''F36'',''F37'',''F38'',''F39'',''F40'',''F41'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 725 as idATA, ''Navarro Reverter'' as descripcion union
				select 726 as idATA, ''Pont de fusta'' as descripcion union
				select 727 as idATA, ''Pont de les arts'' as descripcion union
				select 728 as idATA, ''Pont del real'' as descripcion union
				select 729 as idATA, ''Carrer Alacant'' as descripcion union
				select 730 as idATA, ''Carrer Russafa'' as descripcion) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			from
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as intactual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo on 	vid.IdTA=afo.IdTA
			where afo.nombre in (''F31'',''F32'',''F33'',''F34'',''F35'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by 	afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join(
				select 720 as idATA, ''Carrer Colon'' as descripcion union
				select 721 as idATA, ''Comte de Trénor – Pont de fusta''  as descripcion union
				select 722 as idATA, ''Guillem de Castro''  as descripcion union
				select 723 as idATA, ''Xàtiva''  as descripcion union
				select 724 as idATA, ''Plaça Tetuan''  	as descripcion) as descripciones
			on actual.IdATA=descripciones.idATA
			', 'tipo,idata,descripcion, descripcion_corta, nombre_corto,int_actual,calculation_period', 'N', 'sftp', NULL, NULL, now(), 'TICKET 2572', now(), 'TICKET 2572', 300),
			('py_trafico_intensidad_validada', 'BBDD', 'SQL_Server_Trafico', NULL, NULL, '/integracion/01.fichOrig/VLCI_PY_TRAFICO_VALIDADADO/', '/integracion/02.fichBackup/VLCI_PY_TRAFICO_VALIDADADO/', NULL, 'intensidad_trafico_validada_365_#ddMMyyyyHHmmss#', NULL, 'csv', 'intensidad_trafico_validada_365', 
			'SELECT concat(''Kpi-Accesos-Ciudad-Coches'', '';'', actual.idata, '';'', descripciones.descripcion, '';'', descripciones.descripcion_corta, '';'', descripciones.nombre_corto, '';'', actual.int_actual, '';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A373'',''A406'',''A72'',''A59'',''A1'',''A82'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA,vid.DÍA) actual
			inner join (
    			select 4 as idATA, ''Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)'' as descripcion, ''Accés Barcelona (entrada i eixida)'' as descripcion_corta, ''Accés Barcelona'' as nombre_corto union
    			select 57 as idATA, ''Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)''  as descripcion, ''Accés a Arxiduc Carles pel Camí nou de Picanya'' as descripcion_corta, ''Arxiduc Carles'' as nombre_corto union
    			select 70 as idATA, ''Av. del Cid (Entre V-30 i Tres Creus)''  as descripcion, ''Av. del Cid'' as descripcion_corta, ''Av. del Cid'' as nombre_corto union
    			select 78 as idATA, ''Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)''  as descripcion, ''Corts Valencianes (Accés per CV-35)'' as descripcion_corta, ''Corts Valencianes'' as nombre_corto union
    			select 349 as idATA, ''Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)''      as descripcion, ''Accés per V-31 (Pista de Silla)'' as descripcion_corta, ''Pista de Silla'' as nombre_corto union
    			select 379 as idATA, ''Prolongació Joan XXIII (Entre Germans Machado i Salvador Cerveró)''  as descripcion, ''Prolongació Joan XXIII'' as descripcion_corta, ''Joan XXIII'' as nombre_corto) as descripciones
    			on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Accesos-Vias-Coches'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion_corta,'';'', descripciones.nombre_corto,'';'', actual.int_actual,'';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A116'',''A121'',''A97'',''A117'',''A187'',''A51'') 
			and vid.DÍA = CAST(''#fecha#'' as datetime)   
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 50 as idATA, ''Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)''  as descripcion, ''Av. Blasco Ibáñez'' as descripcion_corta, ''Blasco Ibáñez'' as nombre_corto union
				select 93 as idATA, ''Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)''  as descripcion, ''Av. Dr. Peset Aleixandre'' as descripcion_corta, ''Dr. Peset Aleixandre'' as nombre_corto union
				select 111 as idATA, ''Av. de Giorgeta (Entre Sant Vicent i Jesús)''  as descripcion, ''Av. de Giorgeta'' as descripcion_corta, ''Giorgeta'' as nombre_corto union
				select 112 as idATA, ''Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)''  as descripcion, ''Gran Via de Ferran el Catòlic'' as descripcion_corta, ''Ferran el Catòlic'' as nombre_corto union
				select 116 as idATA, ''Gran Via del Marqués del Túria (Entre Pont d''''Aragó i Hernan Cortés)''  as descripcion, ''Gran Via del Marqués del Túria'' as descripcion_corta, ''Marqués del Túria'' as nombre_corto union
				select 177 as idATA, ''Av. de Peris i Valero (Entre Ausiàs March i Sapadors)''  as descripcion, ''Av. de Peris i Valero'' as descripcion_corta, ''Peris i Valero'' as nombre_corto) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Accesos-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			FROM
			(select afo.Descripcion as descripcion,afo.idATA as idata,  sum(vid.IMD*afo.Coeficiente/100) as intactual,vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''F36'',''F37'',''F38'',''F39'',''F40'',''F41'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 725 as idATA, ''Navarro Reverter'' as descripcion union
				select 726 as idATA, ''Pont de fusta'' as descripcion union
				select 727 as idATA, ''Pont de les arts'' as descripcion union
				select 728 as idATA, ''Pont del real'' as descripcion union
				select 729 as idATA, ''Carrer Alacant'' as descripcion union
				select 730 as idATA, ''Carrer Russafa'' as descripcion) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			from
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as intactual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo on 	vid.IdTA=afo.IdTA
			where afo.nombre in (''F31'',''F32'',''F33'',''F34'',''F35'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by 	afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join(
				select 720 as idATA, ''Carrer Colon'' as descripcion union
				select 721 as idATA, ''Comte de Trénor – Pont de fusta''  as descripcion union
				select 722 as idATA, ''Guillem de Castro''  as descripcion union
				select 723 as idATA, ''Xàtiva''  as descripcion union
				select 724 as idATA, ''Plaça Tetuan''  	as descripcion) as descripciones
			on actual.IdATA=descripciones.idATA
			', 'tipo,idata,descripcion, descripcion_corta, nombre_corto,int_actual,calculation_period', 'N', 'sftp', NULL, NULL, now(), 'TICKET 2572', now(), 'TICKET 2572', 301);
		INSERT INTO t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) 
		VALUES ('py_personal_ayuntamiento','FTP','FTP Sertic','*.CSV','//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal/PRE','/integracion/01.fichOrig/VLCI_PERSONAL_AYUNTAMIENTO/','/integracion/02.fichBackup/VLCI_PERSONAL_AYUNTAMIENTO/',NULL,'*.CSV','S','CSV',NULL,NULL,NULL,'N','sftp',NULL,NULL,NOW(),'Ticket 3225',NULL,NULL);
	
	ELSIF (current_db = 'sc_vlci_pre' ) THEN

		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_MOBILIS','HTTP','HTTP',NULL,NULL,'/pre/01.fichOrig/EMT_Mobilis/','/pre/02.fichBackup/EMT_Mobilis/','http://www.emtvalencia.es/ciudadano/google_transit/datos/carga','mobilis',NULL,'txt',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2021-08-10 08:20:29.88841','ETL Loader','2021-07-27 13:55:20',NULL,29),
			('VLCI_ETL_EMT_MOBILIS_CARGAREFERENCIAS','HTTP','HTTP',NULL,NULL,'/pre/01.fichOrig/EMT_Mobilis_Transit/','/pre/02.fichBackup/EMT_Mobilis_Transit/','http://www.emtvalencia.es/ciudadano/google_transit/google_transit.zip','google_transit',NULL,'zip',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2021-08-10 08:20:29.80237','ETL Loader','2021-07-27 13:55:20',NULL,27),
			('VLCI_ETL_EMT_MOBILIS_CARGATITULOS','HTTP','HTTP',NULL,NULL,'/pre/01.fichOrig/EMT_Mobilis_Titulos/','/pre/02.fichBackup/EMT_Mobilis_Titulos/','http://www.emtvalencia.es/ciudadano/google_transit/datos/titulos.txt','titulos',NULL,'txt',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2021-08-10 08:20:29.845153','ETL Loader','2021-07-27 13:55:20',NULL,28),
			('CDMGE GACO','FTP','FTP Sertic','SMC-GACO-*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/SIEM/PRE','/pre/01.fichOrig/CDMGE_GACO/','/pre/02.fichBackup/CDMGE_GACO/',NULL,'SMC-GACO-_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.404222',NULL,'2021-06-23 11:03:57',NULL,18),
			('CDMGE Codigo Organico','FTP','FTP Sertic','SMC-TBOR-*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/SIEM/PRE','/pre/01.fichOrig/CDMGE_TBOR/','/pre/02.fichBackup/CDMGE_TBOR/',NULL,'SMC-TBOR-_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.446839',NULL,'2021-06-23 11:03:42',NULL,19),
			('ETL VLCi IBI','FTP','FTP Sertic','Informacion_tributaria_IBI_*','//DADES2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/PRE','/pre/01.fichOrig/VLCi_IBI/','/pre/02.fichBackup/VLCi_IBI/',NULL,'Informacion_tributaria_IBI_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.489668','ETL Loader','2021-07-27 13:55:20',NULL,20),
			('MESAS SILLAS','FTP','FTP Sertic','MESAS_Y_SILLAS_*','//dades2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/PRE','/pre/01.fichOrig/VLCi_MESAS_SILLAS/','/pre/02.fichBackup/VLCi_MESAS_SILLAS/',NULL,'MESAS_Y_SILLAS_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.532625',NULL,'2021-07-27 13:55:20',NULL,21),
			('VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS','FTP','FTP Sertic','*listadoUnidadesAdministrativas.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/PRE','/pre/01.fichOrig/VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS/','/pre/02.fichBackup/VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS/',NULL,'*listadoUnidadesAdministrativas.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.575779',NULL,'2021-07-12 08:49:19',NULL,22),
			('VLCi_CDMGE_CONTRATACION','FTP','FTP Sertic','*listado_expedientes_contratacion.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/contratacion/PRE','/pre/01.fichOrig/VLCi_CONTRATACION/','/pre/02.fichBackup/VLCi_CONTRATACION/',NULL,'*listado_expedientes_contratacion.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.618861','ETL Loader','2023-11-22 11:03:50','Cecilia - 2108', NULL);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_REALIZADAS','FTP','FTP Sertic','*_tareasRealizadas.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/PRE','/pre/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/pre/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_tareasRealizadas.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.661792','ETL Loader','2021-06-23 11:03:30',NULL,24),
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_PENDIENTES','FTP','FTP Sertic','*_tareasPendientes.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/PRE','/pre/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/pre/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_tareasPendientes.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.704898','ETL Loader','2021-06-23 11:03:30',NULL,25),
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_USUARIOS','FTP','FTP Sertic','*_usuariosPorUnidad.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/PRE','/pre/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/pre/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_usuariosPorUnidad.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.747808','ETL Loader','2021-06-23 11:03:30',NULL,26),
			('VLCi Sertic Red Pagos','FTP','FTP Sertic','*.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/PRE','/pre/01.fichOrig/VLCi_Sertic_Red_Pagos/','/pre/02.fichBackup/VLCi_Sertic_Red_Pagos/',NULL,'yyyy.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.275703','ETL Loader','2021-07-27 13:55:20',NULL,15),
			('ETL VLCi Vados','FTP','FTP Sertic','Informacion_tributaria_Vados_*','//dades2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/PRE','/pre/01.fichOrig/VLCi_Vados/','/pre/02.fichBackup/VLCi_Vados/',NULL,'Informacion_tributaria_Vados_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.36129','ETL Loader','2021-07-27 13:55:20',NULL,17);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_VALENCIA_ETL_IVTM','FTP','FTP Sertic','Informacion_tributaria_IVTM*','//dades2/dades2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/PRE','/pre/01.fichOrig/VLCI_VALENCIA_ETL_IVTM/','/pre/02.fichBackup/VLCI_VALENCIA_ETL_IVTM/',NULL,'Informacion_tributaria_IVTM*','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:31.373547','ETL Loader','2021-07-27 13:55:20',NULL,64),
			('VLCi_Valencia_ETL_IAE','FTP','FTP Sertic','Informacion_tributaria_IAE_*.txt','//dades2/dades2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi/PRE','/pre/01.fichOrig/IAE/','/pre/02.fichBackup/IAE/',NULL,'Informacion_tributaria_IAE_yyyy.txt','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.709989','ETL Loader','2021-07-27 13:55:20',NULL,1),
			('VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL','FTP','FTP Sertic','residuos_oci.xlsx','//dades1/dades1/ayun/Residuos/LIMPIEZA/isabel/00 OCI/PRE','/pre/01.fichOrig/VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL/','/pre/02.fichBackup/VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL/',NULL,'residuos_oci.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:33.618966','Ticket-45251','2021-07-27 13:55:20',NULL,NULL),
			('VLCI_ETL_SERTIC_SEDE_SWEB1','FTP','FTP Sertic','access.log-*','//soes-aux1/FTPVOL/LogsWeb/slogserver1/','/pre/01.fichOrig/VLCi_Sertic_SedeSWeb1/','/pre/02.fichBackup/VLCi_Sertic_SedeSWeb1/',NULL,'access.log-*','N','xz',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:33.228868','Ticket-39365','2021-07-27 13:55:20',NULL,155),
			('VLCI_ETL_JARDINERIA_IND_INVENTARIO','FTP','FTP Sertic','JARDINES*','//DADES2/DADES2/ayun/JARDINERIA/JardineriaVLCi/PRE','/pre/01.fichOrig/Jardines_Inventario/','/pre/02.fichBackup/Jardines_Inventario/',NULL,'JARDINES','N','mdb',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:32.144823','Ivan 31459','2021-07-27 13:55:20','Ivan 31459',85),
			('VLCI_ETL_COORDVIAPUB_IND_INCIDENCIAS_SIGO','FTP','FTP Sertic','INCIDENCIAS*','//dades1/DADES1/ayun/OCOVAL/PRE','/pre/01.fichOrig/OCOVAL/','/pre/02.fichBackup/OCOVAL/',NULL,'INCIDENCIAS.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:32.102146','Toni 28621','2021-07-27 13:55:20','Toni 28621',84);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Valencia_ETL_PMP_Facturas','FTP','FTP Sertic','PMP_UA_*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/PRE/','/pre/01.fichOrig/PMP_facturas/','/pre/02.fichBackup/PMP_facturas/',NULL,'PMP_UA_yyyy.txt','N','zip',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.754693','ETL Loader','2021-07-27 13:55:20',NULL,2),
			('ETL_Trafico_aparcamientos','FTP','FTP Sertic','Consulta aparcamientos por tipo y plazas.xls','//dades2/DADES2/ayun/TRAFICO/Plataforma VLCi/PRE','/pre/01.fichOrig/VLCi_Trafico_Aparcamientos/','/pre/02.fichBackup/VLCi_Trafico_Aparcamientos/',NULL,'Consulta aparcamientos por tipo y plazas.xls','N','xls',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.840985','ETL Loader','2021-07-27 13:55:20',NULL,4),
			('ETL_VLCi_CDMGE_PMP_ENTIDAD','FTP','FTP Sertic','Evolucion_PMP.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/PRE','/pre/01.fichOrig/VLCi_CDMGE_PMP_ENTIDAD/','/pre/02.fichBackup/VLCi_CDMGE_PMP_ENTIDAD/',NULL,'Evolucion_PMP.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:28.969863','ETL Loader','2021-07-12 08:49:29',NULL,7),
			('ETL VLCi CDMGE PMP','FTP','FTP Sertic','PMP_Resumen_Uds_Administrativas_*.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PMP/PRE','/pre/01.fichOrig/VLCi_CDMGE_PMP/','/pre/02.fichBackup/VLCi_CDMGE_PMP/',NULL,'PMP_Resumen_Uds_Administrativas_yyyy.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2021-08-10 08:20:29.060836','ETL Loader','2021-07-12 08:49:37',NULL,10);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A8',NULL,'csv','Indicador IC110A8.csv','WITH SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T56143.CODPAIS as c4,      case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  as c5,      T56143.DPAIS as c6,      T6789.CODIGO as c7,      T18685.ANYO as c33 from       V_LU_GEO_PAIS_NACION T56143,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T56143.IDIOMA and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T17978.DTBA = T18685.DISBAR and T18685.CODPOS = T21622.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18685.PAINACDAD = T56143.CODPAIS and T18685.PAINACDAD <> 108 and T56143.CODPAIS <> 108)  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T56143.CODPAIS, T56143.DPAIS, case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end, T18685.ANYO ) select 0 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c1 ||'';''||      D1.c6 ||'';''||      case  when row_number() OVER (PARTITION BY D1.c3 ORDER BY D1.c1 DESC) < 7 then row_number() OVER (PARTITION BY D1.c3 ORDER BY D1.c1 DESC) else 10 end  ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH0 D1  order by D1.c2, D1.c7','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.242798','Lozano','2021-08-26 08:08:43.185664',NULL,61),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC015A1',NULL,'csv','Indicador IC015A1.csv','with pob as ( select SUM(T21230.NUMPERSONAS) personas, T7594.ANO ano from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Valenciano'' and T10746.CODIGO = T21230.SEXO )  GROUP BY T7594.ANO ), cons as ( select sum(T60833.VALOR) consumo, T7594.ANO ano from       LU_VIII_IBER_TIPOENERGIA_IDI T61176,      LU_VIII_IBER_TIPCONS_IDI T33126,      LU_VIII_IBER_INDICADOR_IDI T33120,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_IBER_DATOSANUAL T60833 where  ( T6789.CODIGO = T61176.IDIOMA and T6789.CODIGO = T33120.IDIOMA and T33120.CODIGO = T60833.INDICADOR and T7594.ANO = T60833.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T60833.MUNICIPIO and T6789.CODIGO = T33126.IDIOMA and T33126.CODIGO = T60833.TIPCONS and T6789.DENOMINACION = ''Valenciano'' and T33120.CODIGO = 1 and T33126.CODIGO = 1 and T60833.TIPENERGIA = T61176.CODIGO and T61176.CODIGO = 1 )  GROUP BY  T7594.ANO  ) select ((d1.consumo/p1.personas) / 12.0)*1000 ||'';''|| p1.ano ano from pob p1, cons d1 where p1.ano = d1.ano','consumo_anual,ano','N','sftp',NULL,NULL,'2021-08-10 08:20:30.059754','Lozano','2021-07-27 13:55:20',NULL,35),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC019A1',NULL,'csv','Indicador IC019A1.csv','with pob as (  select SUM(T21230.NUMPERSONAS) personas, T7594.ANO ano from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Valenciano'' and T10746.CODIGO = T21230.SEXO ) GROUP BY T7594.ANO ),  cons as (select sum(T60833.VALOR) consumo, T7594.ANO ano from       LU_VIII_IBER_TIPOENERGIA_IDI T61176,      LU_VIII_IBER_TIPCONS_IDI T33126,      LU_VIII_IBER_INDICADOR_IDI T33120,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_IBER_DATOSANUAL T60833 where  ( T6789.CODIGO = T61176.IDIOMA and T6789.CODIGO = T33120.IDIOMA and T33120.CODIGO = T60833.INDICADOR and T7594.ANO = T60833.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T60833.MUNICIPIO and T6789.CODIGO = T33126.IDIOMA and T33126.CODIGO = T60833.TIPCONS and T6789.DENOMINACION = ''Valenciano''  and T33120.CODIGO = 1 and T33126.CODIGO <> 9 and T60833.TIPENERGIA = T61176.CODIGO and T61176.CODIGO = 1 )  GROUP BY  T7594.ANO  ) select ((d1.consumo/p1.personas) / 365.0)*1000 ||'';''|| p1.ano ano from pob p1, cons d1 where p1.ano = d1.ano','consumo_anual,ano','N','sftp',NULL,NULL,'2021-08-10 08:20:30.102884','Lozano','2021-07-27 13:55:20',NULL,36),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC001A1',NULL,'csv','Indicador IC001A1.csv','WITH  SAWITH0 AS (select sum(T10607.VALOR) as c1,      T10686.CODIGO as c2,      T10686.DENOMINACION as c3,      T10783.ANO as c4,      T10783.TRIMESTRE as c5,      T10783.ANOTRIMESTRE as c6,      T6789.CODIGO as c7 from       LU_VII_EPAV_EDADAGR2_IDI T10680,      LU_VII_EPAV_INDICADOR_IDI T10686,      LU_VII_EPAV_METODO_IDI T10692,      V_LU_TIEMPO_ANOTRIMESTRE T10783,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_VII_EPAV_SEXOAGREDAD2 T10607 where  ( T6789.CODIGO = T10680.IDIOMA and T6789.CODIGO = T10686.IDIOMA and T10607.EDADAGR2 = T10680.CODIGO and T6789.CODIGO = T10692.IDIOMA and T10607.INDICADOR = T10686.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10607.METODO = T10692.CODIGO and T10607.ANO = T10783.ANO and T10607.SEXO = T10746.CODIGO and T6789.DENOMINACION = ''Castellano'' and T10607.EDADAGR2 = 9 and T10607.INDICADOR = 26 and T10607.METODO = 4 and T10607.TRIMESTRE = T10783.TRIMESTRE  and T10607.SEXO = 9  and T10680.CODIGO = 9 and T10686.CODIGO = 26 and T10692.CODIGO = 4 and T10746.CODIGO = 9  )  group by T6789.CODIGO, T6789.DENOMINACION, T10686.CODIGO, T10686.DENOMINACION, T10783.ANO, T10783.ANOTRIMESTRE, T10783.TRIMESTRE), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c5 as c6,      D1.c1 as c7,      D1.c6 as c8,      D1.c7 as c9 from       SAWITH0 D1), SAWITH2 AS (select T58019.DENOMINACION as c1,      T58019.CODIGO as c2,      T58019.IDIOMA as c3 from       LU_TRIMESTRE_IDI T58019 /* LU_TRIMESTRE_LOOKUP */ ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D1.c4 as c4,                D2.c1 as c5,                D1.c5 as c6,                D1.c6 as c7,                D1.c7 as c8,                D1.c8 as c9,                D1.c9 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c8, D1.c9, D2.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c8 ASC, D1.c9 ASC, D2.c1 ASC) as c11           from                 SAWITH1 D1 inner join SAWITH2 D2 On D1.c5 = D2.c2 and D1.c9 = D2.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 as c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH3 D1 order by c1, c4, c6, c7, c2, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:30.146152','Lozano','2021-07-27 13:55:20',NULL,37),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC101A1',NULL,'csv','Indicador IC101A1.csv','WITH  SAWITH0 AS (select sum(T13116.VALOR) as c1,      T73182.CODIGO as c2,      T73182.DENOMINACION as c3,      T13200.CODIGO as c4,      T13200.DENOMINACION as c5,      T13212.CODIGO as c6,      T6789.DENOMINACION as c7,      T7595.ANO as c8,      T7595.MES as c9,      T7595.ANOMES as c10,      T6789.CODIGO as c11,      T7595.ANO as c33 from       LU_VI_EOH_TIPEST_IDI T73182,      LU_VI_EOH_RESIDENCIA_IDI T13212,      LU_VI_EOH_INDICADORDEM_IDI T13200,      V_LU_TIEMPO_ANOMES T7595,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VI_EOH_DEMTURMUNI T13116 where  ( T6789.CODIGO = T73182.IDIOMA and T6789.CODIGO = T13200.IDIOMA and T13116.INDICADOR = T13200.CODIGO and T6789.CODIGO = T13212.IDIOMA and T13116.RESIDENCIA = T13212.CODIGO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T13116.MUNICIPIO and T7595.ANO = T13116.ANO and T7595.MES = T13116.MES and T6789.DENOMINACION = ''Castellano'' and T6910.CODMUNICIPIO = 46250 and T13116.INDICADOR = 2 and T13116.MUNICIPIO = 46250 and T13116.RESIDENCIA = 9 and T13116.TIPEST = T73182.CODIGO and T13200.CODIGO = 2 and T13212.CODIGO = 9 and T13116.INDICADOR <> 3 and T13200.CODIGO <> 3 )  group by T6789.CODIGO, T6789.DENOMINACION, T7595.ANO, T7595.MES, T7595.ANOMES, T13200.CODIGO, T13200.DENOMINACION, T13212.CODIGO, T73182.CODIGO, T73182.DENOMINACION), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c9 as c10,      D1.c1 as c11,      D1.c10 as c12,      D1.c11 as c13,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T57997.DENOMINACION as c1,      T57997.CODIGO as c2,      T57997.IDIOMA as c3 from       LU_MES_IDI T57997 /* LU_MES_LOOKUP */ ), SAWITH3 AS (select D1.c1 as c1,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c14 as c14,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c4 as c4,                D1.c5 as c5,                D1.c6 as c6,                D1.c7 as c7,                D1.c8 as c8,                D2.c1 as c9,                D1.c9 as c10,                D1.c10 as c11,                D1.c11 as c12,                D1.c12 as c13,                D1.c13 as c14,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c9, D1.c10, D1.c12, D1.c13, D2.c1,D1.c33 ORDER BY D1.c2 ASC,  D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c9 ASC, D1.c10 ASC, D1.c12 ASC, D1.c13 ASC, D2.c1 ASC) as c15           from                 SAWITH1 D1 inner join SAWITH2 D2 On D1.c9 = D2.c2 and D1.c13 = D2.c3      ) D1 where  ( D1.c15 = 1 ) ) select D1.c1 ||'';''||  D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      sum(D1.c12) as c12,      D1.c33 as c33 from       SAWITH3 D1 group by c1,c4,c5,c6,c7,c8,c9,c10,c11,c33 order by c1, c7, c8, c11, c10, c4, c6,  c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.19047','Lozano','2021-07-27 13:55:20',NULL,38),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152',NULL,'csv','Indicador IC152.csv','WITH  SAWITH0 AS (select count(T101251.ID) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';'' || D1.c3||'';'' || D1.c4||'';'' || D1.c5||'';'' || D1.c6||'';'' || D1.c7||'';''|| D1.c8||'';''|| D1.c9||'';''|| D1.c10||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.234049','Lozano','2021-07-27 13:55:20',NULL,39),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A1',NULL,'csv','Indicador IC152A1.csv','WITH  SAWITH0 AS (select sum(T101251.SUPPARCELA) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';'' || D1.c3||'';'' || D1.c4||'';'' || D1.c5||'';'' || D1.c6||'';'' || D1.c7||'';''|| D1.c8||'';''|| D1.c9||'';''|| D1.c10||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.277606','Lozano','2021-07-27 13:55:20',NULL,40),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A2',NULL,'csv','Indicador IC152A2.csv','WITH  SAWITH0 AS (select sum(T101251.SUPCONSTR) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end , T101251.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select   	       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10, 	 D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.321098','Lozano','2021-07-27 13:55:20',NULL,41);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A3',NULL,'csv','Indicador IC152A3.csv','WITH  SAWITH0 AS (select sum(T101251.VALORINVENT) as c1,      T101277.CODEPI as c2,      T101277.DEPI as c3,      T17978.DT as c4,      T17978.DTBA as c5,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,      case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,      T6789.CODIGO as c8,      T101251.ANO as c33 from       V_LU_XI_PATRIM_RELEPIGRAFE T101277,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_XI_PATRIM_EDIFICIOS T101251 where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D2.c1 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.366692','Lozano','2021-07-27 13:55:20',NULL,42),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A1',NULL,'csv','Indicador IC153A1.csv','WITH  SAWITH0 AS (select count(T16529.ORDNUM) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T16638.CODIGO as c4,      T16638.DENOMINACION as c5,      T16701.CODIGO as c6,      T16701.DENOMINACION as c7,      T16722.CODIGO as c8,      T16722.DENOMINACION as c9,      T6789.CODIGO as c10,      T7594.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_POTTU2_IDI T16701,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16701.IDIOMA and T16529.DTBA = T17978.DTBA and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.POT_TU2 = T16701.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16722.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T16638.CODIGO, T16638.DENOMINACION, T16701.CODIGO, T16701.DENOMINACION, T16722.CODIGO, T16722.DENOMINACION, T17978.DTBA, T55787.DT,T7594.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c1 as c10,      D1.c10 as c12,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D2.c1 as c2,      D3.c1 as c3,      D1.c2 as c4,      D1.c3 as c5,      D1.c4 as c6,      D1.c5 as c7,      D1.c6 as c8,      D1.c7 as c9,      D1.c8 as c10,      D1.c9 as c11,      D1.c10 as c12,      D1.c12 as c14,      D1.c33 as c33,      ROW_NUMBER() OVER (PARTITION BY D1.c4, D1.c5, D1.c6, D1.c7, D1.c3, D1.c2, D2.c1, D3.c1, D1.c8, D1.c9,D1.c33 ORDER BY D1.c4 DESC, D1.c5 DESC, D1.c6 DESC, D1.c7 DESC, D1.c3 DESC, D1.c2 DESC, D2.c1 DESC, D3.c1 DESC, D1.c8 DESC, D1.c9 DESC) as c15 from       (           SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c12 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c12 = D3.c3), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c14 as c14,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D1.c4 as c4,                D1.c5 as c5,                D1.c6 as c6,                D1.c7 as c7,                D1.c8 as c8,                D1.c9 as c9,                D1.c10 as c10,                D1.c11 as c11,                D1.c12 as c12,                sum(case D1.c15 when 1 then D1.c12 else NULL end ) over (partition by D1.c6, D1.c7, D1.c8, D1.c9, D1.c5, D1.c4, D1.c2, D1.c3,D1.c2,D1.c33)  as c13,                D1.c14 as c14,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c9, D1.c10, D1.c11, D1.c14, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c9 ASC, D1.c10 ASC, D1.c11 ASC, D1.c14 ASC) as c15           from                 SAWITH4 D1      ) D1 where  ( D1.c15 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4  ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c13 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c4, c2, c5, c3, c6, c7, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.413657','Lozano','2021-07-27 13:55:20',NULL,43),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A2',NULL,'csv','Indicador IC153A2.csv','WITH  SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T17978.DTBA as c2,      T6789.CODIGO as c3,      T6789.DENOMINACION as c4,      T16529.ANO as c33 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T6789.CODIGO = T16638.IDIOMA and T7594.ANO = T16529.ANO and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Valenciano''  and T16529.DTBA = T17978.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T16529.ANO), SAWITH1 AS (select sum(T21230.NUMPERSONAS) as c1,      T17978.DTBA as c2,      T6789.CODIGO as c3,      T6789.DENOMINACION as c4 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T17978.DT = T21230.DIS and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T21230.SEXO and T6789.DENOMINACION = ''Valenciano'' and T17978.DTBA = T21230.DISBAR and T21230.ANYO = 2016 and T21230.ANYO = T24742.ANO and T21230.DIA = T24742.DIADELMES and T21230.MES = T24742.MES and T24742.ANO = 2016 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      cast(D1.c1 * 100 / nullif( D2.c1, 0) as  DOUBLE PRECISION  ) as c3,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c4,      D1.c33 as c33 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c3 = D2.c3 and D1.c2 = D2.c2 and  SYS_OP_MAP_NONNULL(D1.c4) = SYS_OP_MAP_NONNULL(D2.c4) ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D1.c3 as c4,                D1.c4 as c5,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c4, D2.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c4 ASC, D2.c1 ASC) as c6           from                 SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c4 = D2.c3      ) D1 where  ( D1.c6 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c3, c2 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.45704','Lozano','2021-07-27 13:55:20',NULL,44),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A3',NULL,'csv','Indicador IC153A3.csv','WITH  SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T6789.CODIGO as c4, 	 T7594.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_TIPODNI_IDI T16713,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16713.IDIOMA and T16529.DTBA = T17978.DTBA and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.TIPODNI = T16713.CODIGO and T6789.DENOMINACION = ''Castellano''  and T16529.TIPODNI = 1 and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16713.CODIGO = 1 and T16722.CODIGO = 1 and T16529.ANYMAT <= 2015 - 11 and T16638.CODIGO <= 2015 - 11 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T55787.DT, T7594.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4,      D1.c4 as c5, 	 D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7, 	 D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7, 			   D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6, 	 D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.501004','Lozano','2021-07-27 13:55:20',NULL,45),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A4',NULL,'csv','Indicador IC153A4.csv','WITH  SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T6789.CODIGO as c4, 	 T7594.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_TIPODNI_IDI T16713,      LU_III_MAT_POTTU2_IDI T16701,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16713.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.CODIGO = T16701.IDIOMA and T16529.POT_TU2 = T16701.CODIGO and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.TIPODNI = T16713.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.TIPODNI = 1 and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16713.CODIGO = 1 and T16722.CODIGO = 1 and T16529.POT_TU2 >= 4 and T16701.CODIGO >= 4 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T55787.DT, T7594.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4,      D1.c4 as c5, 	 D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7, 	 D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7, 			   D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6, 	 D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.544781','Lozano','2021-07-27 13:55:20',NULL,46),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A6',NULL,'csv','Indicador IC153A6.csv','WITH  SAWITH0 AS (select avg(T58565.EDADM) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T6789.CODIGO as c4,      T58565.AÑO as c33 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_III_MAT_EDADM_DTBA T58565 where  ( T7594.ANO = T58565.AÑO and T6789.DENOMINACION = ''Castellano''  and T17978.DTBA = T58565.DTBA  )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T58565.AÑO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4,      D1.c4 as c5,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.588726','Lozano','2021-07-27 13:55:20',NULL,47),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153',NULL,'csv','Indicador IC153.csv','WITH  SAWITH0 AS (select count(T16529.ORDNUM) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T16722.CODIGO as c4,      T16722.DENOMINACION as c5,      T6789.CODIGO as c6,      T16529.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.DENOMINACION = ''Castellano''  and T16529.TIPOFIN = T16722.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T16722.CODIGO, T16722.DENOMINACION, T17978.DTBA, T55787.DT, T16529.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c6, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.631828','Lozano','2021-07-27 13:55:20',NULL,48),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A5',NULL,'csv','Indicador IC153A5.csv','WITH  SAWITH0 AS (select count(T16529.ORDNUM) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T16674.CODIGO as c4,      T16674.DENOMINACION as c5,      T6789.CODIGO as c6,      T16529.ANO as c33 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_III_MAT_TIPOFIN_IDI T16722,      LU_III_MAT_CILMO2_IDI T16674,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16674.IDIOMA and T16529.CIL_MO2 = T16674.CODIGO and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.DENOMINACION = ''Castellano''  and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 6 and T16722.CODIGO = 6 )  group by T6789.CODIGO, T6789.DENOMINACION, T16674.CODIGO, T16674.DENOMINACION, T17978.DTBA, T55787.DT, T16529.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.675131','Lozano','2021-07-27 13:55:20',NULL,49),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A1',NULL,'csv','Indicador IC006A1.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T54922.CODIGO as c4,      T54922.DENOMINACION as c5,      T6789.CODIGO as c6,       T55155.ANO as c33 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T6789.DENOMINACION = ''Castellano'' and T54922.CODIGO = T55155.GGRUP and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T54922.CODIGO, T54922.DENOMINACION, T55155.ANO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c5, c2, c3, c6, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.718765','Lozano','2021-07-27 13:55:20',NULL,50),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A2',NULL,'csv','Indicador IC006A2.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T55112.DSECCDIV as c4,      T55112.SECCDIV as c5,      T6789.CODIGO as c6,      T55155.ANO as c33 from       V_LU_VI_IAE_RELSECCIONES T55112,      LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 2 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 2 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO), SAWITH1 AS (select count(T55155.NUMREF) as c1 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 2  and T55155.GGRUP = 2 ) ), SAWITH2 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D2.c1 as c7,      D1.c6 as c8,      D1.c33 as c33 from       SAWITH0 D1,      SAWITH1 D2), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c7, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.762029','Lozano','2021-07-27 13:55:20',NULL,51);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A3',NULL,'csv','Indicador IC006A3.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T55112.DSECCDIV as c4,      T55112.SECCDIV as c5,      T6789.CODIGO as c6,      T55155.ANO as c33 from       V_LU_VI_IAE_RELSECCIONES T55112,      LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 4 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 4 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO), SAWITH1 AS (select count(T55155.NUMREF) as c1 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 4  and T55155.GGRUP = 4 ) ), SAWITH2 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D2.c1 as c7,      D1.c6 as c8,      D1.c33 as c33 from       SAWITH0 D1,      SAWITH1 D2), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c7, c6, c4, c2, c3, c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.805391','Lozano','2021-07-27 13:55:20',NULL,52),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A4',NULL,'csv','Indicador IC006A4.csv','WITH  SAWITH0 AS (select count(T55155.NUMREF) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T55112.DSECCDIV as c4,      T55112.SECCDIV as c5,      T6789.CODIGO as c6,      T55155.ANO as c33 from       V_LU_VI_IAE_RELSECCIONES T55112,      LU_VI_IAE_GRANGR_IDI T54922,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 5 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 5 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO), SAWITH1 AS (select count(T55155.NUMREF) as c1 from       LU_VI_IAE_GRANGR_IDI T54922,      V_LU_TIEMPO_ANOMESDIA T24742,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_VI_IAE_IAE T55155 where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 5  and T55155.GGRUP = 5 ) ), SAWITH2 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D2.c1 as c7,      D1.c6 as c8,      D1.c33 as c33 from       SAWITH0 D1,      SAWITH1 D2), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       SAWITH5 D1 order by c1, c7, c6, c4, c2, c3, c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.848579','Lozano','2021-07-27 13:55:20',NULL,53),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A2',NULL,'csv','Indicador IC110A2.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T18894.CODIGO as c4,      T18894.DENOMINACION as c5,      T6789.CODIGO as c6,      T18685.ANYO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T17978.DTBA = T18685.DISBAR and T18685.CODPOS = T21622.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18685.TIPLOC = T18894.CODIGO and T18685.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, T18894.DENOMINACION, T18685.ANYO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:30.89237','Lozano','2021-07-27 13:55:20',NULL,55),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A3',NULL,'csv','Indicador IC110A3.csv','WITH  SAWITH0 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDAMEN5 = 0 then ''Cap de 0-4'' else ''Algú de 0-4'' end  else case  when T18579.INDAMEN5 = 0 then ''Nadie de 0-4'' else ''Alguien de 0-4'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18579.INDAMEN5 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDAMEN5 = 0 then ''Cap de 0-4'' else ''Algú de 0-4'' end  else case  when T18579.INDAMEN5 = 0 then ''Nadie de 0-4'' else ''Alguien de 0-4'' end  end,T18579.ANO), OBICOMMON0 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), OBICOMMON1 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH1 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH0 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH2 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA0509 = 0 then ''Cap de 5-9'' else ''Algú de 5-9'' end  else case  when T18579.INDA0509 = 0 then ''Nadie de 5-9'' else ''Alguien de 5-9'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano'' and T18579.INDA0509 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA0509 = 0 then ''Cap de 5-9'' else ''Algú de 5-9'' end  else case  when T18579.INDA0509 = 0 then ''Nadie de 5-9'' else ''Alguien de 5-9'' end  end, T18579.ANO ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH4 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA1014 = 0 then ''Cap de 10-14'' else ''Algú de 10-14'' end  else case  when T18579.INDA1014 = 0 then ''Nadie de 10-14'' else ''Alguien de 10-14'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA1014 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA1014 = 0 then ''Cap de 10-14'' else ''Algú de 10-14'' end  else case  when T18579.INDA1014 = 0 then ''Nadie de 10-14'' else ''Alguien de 10-14'' end  end, T18579.ANO ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH4 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH6 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA1519 = 0 then ''Cap de 15-19'' else ''Algú de 15-19'' end  else case  when T18579.INDA1519 = 0 then ''Nadie de 15-19'' else ''Alguien de 15-19'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA1519 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA1519 = 0 then ''Cap de 15-19'' else ''Algú de 15-19'' end  else case  when T18579.INDA1519 = 0 then ''Nadie de 15-19'' else ''Alguien de 15-19'' end  end, T18579.ANO ), SAWITH7 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH6 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH8 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA2024 = 0 then ''Cap de 20-24'' else ''Algú de 20-24'' end  else case  when T18579.INDA2024 = 0 then ''Nadie de 20-24'' else ''Alguien de 20-24'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA2024 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA2024 = 0 then ''Cap de 20-24'' else ''Algú de 20-24'' end  else case  when T18579.INDA2024 = 0 then ''Nadie de 20-24'' else ''Alguien de 20-24'' end  end, T18579.ANO ), SAWITH9 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH8 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH10 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA2529 = 0 then ''Cap de 25-29'' else ''Algú de 25-29'' end  else case  when T18579.INDA2529 = 0 then ''Nadie de 25-29'' else ''Alguien de 25-29'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA2529 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA2529 = 0 then ''Cap de 25-29'' else ''Algú de 25-29'' end  else case  when T18579.INDA2529 = 0 then ''Nadie de 25-29'' else ''Alguien de 25-29'' end  end, T18579.ANO ), SAWITH11 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH10 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH12 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA3034 = 0 then ''Cap de 30-34'' else ''Algú de 30-34'' end  else case  when T18579.INDA3034 = 0 then ''Nadie de 30-34'' else ''Alguien de 30-34'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA3034 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA3034 = 0 then ''Cap de 30-34'' else ''Algú de 30-34'' end  else case  when T18579.INDA3034 = 0 then ''Nadie de 30-34'' else ''Alguien de 30-34'' end  end, T18579.ANO ), SAWITH13 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH12 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH14 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA3539 = 0 then ''Cap de 35-39'' else ''Algú de 35-39'' end  else case  when T18579.INDA3539 = 0 then ''Nadie de 35-39'' else ''Alguien de 35-39'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA3539 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA3539 = 0 then ''Cap de 35-39'' else ''Algú de 35-39'' end  else case  when T18579.INDA3539 = 0 then ''Nadie de 35-39'' else ''Alguien de 35-39'' end  end, T18579.ANO ), SAWITH15 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH14 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH16 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA4044 = 0 then ''Cap de 40-44'' else ''Algú de 40-44'' end  else case  when T18579.INDA4044 = 0 then ''Nadie de 40-44'' else ''Alguien de 40-44'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA4044 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA4044 = 0 then ''Cap de 40-44'' else ''Algú de 40-44'' end  else case  when T18579.INDA4044 = 0 then ''Nadie de 40-44'' else ''Alguien de 40-44'' end  end, T18579.ANO ), SAWITH17 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH16 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH18 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA4549 = 0 then ''Cap de 45-49'' else ''Algú de 45-49'' end  else case  when T18579.INDA4549 = 0 then ''Nadie de 45-49'' else ''Alguien de 45-49'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA4549 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA4549 = 0 then ''Cap de 45-49'' else ''Algú de 45-49'' end  else case  when T18579.INDA4549 = 0 then ''Nadie de 45-49'' else ''Alguien de 45-49'' end  end, T18579.ANO ), SAWITH19 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH18 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH20 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA5054 = 0 then ''Cap de 50-54'' else ''Algú de 50-54'' end  else case  when T18579.INDA5054 = 0 then ''Nadie de 50-54'' else ''Alguien de 50-54'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA5054 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA5054 = 0 then ''Cap de 50-54'' else ''Algú de 50-54'' end  else case  when T18579.INDA5054 = 0 then ''Nadie de 50-54'' else ''Alguien de 50-54'' end  end, T18579.ANO ), SAWITH21 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH20 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH22 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA5559 = 0 then ''Cap de 55-59'' else ''Algú de 55-59'' end  else case  when T18579.INDA5559 = 0 then ''Nadie de 55-59'' else ''Alguien de 55-59'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA5559 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA5559 = 0 then ''Cap de 55-59'' else ''Algú de 55-59'' end  else case  when T18579.INDA5559 = 0 then ''Nadie de 55-59'' else ''Alguien de 55-59'' end  end, T18579.ANO ), SAWITH23 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH22 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH24 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA6064 = 0 then ''Cap de 60-64'' else ''Algú de 60-64'' end  else case  when T18579.INDA6064 = 0 then ''Nadie de 60-64'' else ''Alguien de 60-64'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA6064 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA6064 = 0 then ''Cap de 60-64'' else ''Algú de 60-64'' end  else case  when T18579.INDA6064 = 0 then ''Nadie de 60-64'' else ''Alguien de 60-64'' end  end , T18579.ANO ), SAWITH25 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH24 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH26 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA6569 = 0 then ''Cap de 65-69'' else ''Algú de 65-69'' end  else case  when T18579.INDA6569 = 0 then ''Nadie de 65-69'' else ''Alguien de 65-69'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA6569 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA6569 = 0 then ''Cap de 65-69'' else ''Algú de 65-69'' end  else case  when T18579.INDA6569 = 0 then ''Nadie de 65-69'' else ''Alguien de 65-69'' end  end, T18579.ANO ), SAWITH27 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH26 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH28 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA7074 = 0 then ''Cap de 70-74'' else ''Algú de 70-74'' end  else case  when T18579.INDA7074 = 0 then ''Nadie de 70-74'' else ''Alguien de 70-74'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA7074 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA7074 = 0 then ''Cap de 70-74'' else ''Algú de 70-74'' end  else case  when T18579.INDA7074 = 0 then ''Nadie de 70-74'' else ''Alguien de 70-74'' end  end,T18579.ANO ), SAWITH29 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH28 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH30 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDA7579 = 0 then ''Cap de 75-79'' else ''Algú de 75-79'' end  else case  when T18579.INDA7579 = 0 then ''Nadie de 75-79'' else ''Alguien de 75-79'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA7579 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA7579 = 0 then ''Cap de 75-79'' else ''Algú de 75-79'' end  else case  when T18579.INDA7579 = 0 then ''Nadie de 75-79'' else ''Alguien de 75-79'' end  end, T18579.ANO ), SAWITH31 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH30 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ), SAWITH32 AS (select T18894.CODIGO as c1,      T17978.DT as c2,      T17978.DTBA as c3,      case  when T6789.CODIGO = 0 then case  when T18579.INDAMAY80 = 0 then ''Cap major de 80 i més'' else ''Algun major de 80 i més'' end  else case  when T18579.INDAMAY80 = 0 then ''Ningún mayor de 80 y más'' else ''Algún mayor de 80 y más'' end  end  as c4,      count(T18579.IDHOJA) as c5,      T6789.CODIGO as c6,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDAMAY80 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDAMAY80 = 0 then ''Cap major de 80 i més'' else ''Algun major de 80 i més'' end  else case  when T18579.INDAMAY80 = 0 then ''Ningún mayor de 80 y más'' else ''Algún mayor de 80 y más'' end  end, T18579.ANO ), SAWITH33 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c33 as c33 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D3.c1 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH32 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) (select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH1 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH3 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH5 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH7 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH9 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH11 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH13 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH15 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH17 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH19 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH21 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH23 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH25 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH27 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH29 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH31 D1 union select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH33 D1)','c1,c2,c3,c4,c5,c6,c7,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.01781','Lozano','2021-07-27 13:55:20',NULL,56),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC145A1',NULL,'csv','Indicador IC145A1.csv','select T6789.DENOMINACION ||'';''||      T8148.CODIGO ||'';''||      T8160.CODIGO ||'';''||      T8160.DENOMINACION ||'';''||      T7595.ANO ||'';''||      T7595.MES ||'';''||      T6789.CODIGO ||'';''||      replace(T8109.VALOR , '','' , ''.'') as c9 from       LU_I_MET_ESTMETEO T8148,      V_LU_TIEMPO_ANOMES T7595,      LU_IDIOMA T6789,      LU_I_MET_INDICADOR_IDI T8160,      FACT_I_MET_METEOANOMES T8109 where  ( T6789.CODIGO = T8160.IDIOMA and T8109.ESTMETEO = T8148.CODIGO and T7595.ANO = T8109.ANO and T7595.MES = T8109.MES and T6789.DENOMINACION = ''Castellano''  and T8109.ESTMETEO = 1 and T8109.INDICADOR = T8160.CODIGO and T8109.INDICADOR = 7 and T8148.CODIGO = 1 and T8160.CODIGO = 7 )','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2021-08-10 08:20:31.285943','Lozano','2021-07-27 13:55:20',NULL,62),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A4',NULL,'csv','Indicador IC110A4.csv','WITH  SAWITH0 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDS0024 = 0 then ''Cap membre de 0-24 anys'' else ''Només membres de 0-24 anys'' end  else case  when T18579.INDS0024 = 0 then ''Ningún miembro de 0-24 años'' else ''Solo miembros de 0-24 años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18579.INDS0024 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDS0024 = 0 then ''Cap membre de 0-24 anys'' else ''Només membres de 0-24 anys'' end  else case  when T18579.INDS0024 = 0 then ''Ningún miembro de 0-24 años'' else ''Solo miembros de 0-24 años'' end  end , T18579.ANO), OBICOMMON0 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), OBICOMMON1 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH1 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1, D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH0 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH2 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDS2564 = 0 then ''Cap membre de 25-64 anys'' else ''Només membres de 25-64 anys'' end  else case  when T18579.INDS2564 = 0 then ''Ningún miembro de 25-64 años'' else ''Solo miembros de 25-64 años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDS2564 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDS2564 = 0 then ''Cap membre de 25-64 anys'' else ''Només membres de 25-64 anys'' end  else case  when T18579.INDS2564 = 0 then ''Ningún miembro de 25-64 años'' else ''Solo miembros de 25-64 años'' end  end, T18579.ANO ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH2 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH4 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY65 = 0 then ''Cap membre de 65 i més anys'' else ''Només membres de 65 i més anys'' end  else case  when T18579.INDSMAY65 = 0 then ''Ningún miembro de 65 y más años'' else ''Solo miembros de 65 y más años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDSMAY65 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY65 = 0 then ''Cap membre de 65 i més anys'' else ''Només membres de 65 i més anys'' end  else case  when T18579.INDSMAY65 = 0 then ''Ningún miembro de 65 y más años'' else ''Solo miembros de 65 y más años'' end  end, T18579.ANO ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH4 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH6 AS (select T17978.DT as c1,      T17978.DTBA as c2,      case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY80 = 0 then ''Cap membre de 80 i més anys'' else ''Només membres de 80 i més anys'' end  else case  when T18579.INDSMAY80 = 0 then ''Ningún miembro de 80 y más años'' else ''Solo miembros de 80 y más años'' end  end  as c3,      count(T18579.IDHOJA) as c4,      T6789.CODIGO as c5,      T18579.ANO as c33 from       LU_II_PAD_TIPVIV_IDI T18894,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_II_PAD_CMSS_IDI T18834,      FACT_II_PAD_HOJAPAD T18579 where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDSMAY80 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 )  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY80 = 0 then ''Cap membre de 80 i més anys'' else ''Només membres de 80 i més anys'' end  else case  when T18579.INDSMAY80 = 0 then ''Ningún miembro de 80 y más años'' else ''Solo miembros de 80 y más años'' end  end, T18579.ANO ), SAWITH7 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D1.c2 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8           from                 (                     SAWITH6 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3      ) D1 where  ( D1.c8 = 1 ) ), SAWITH8 AS ((select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH1 D1 union select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH3 D1 union select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH5 D1 union select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c33 as c33 from       SAWITH7 D1)) select D1.c1 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c5 ||'';''||      D1.c6 ||'';''||      D1.c33 as c33 from       SAWITH8 D1 order by c1, c2, c3, c4, c5','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.067184','Lozano','2021-07-27 13:55:20',NULL,57),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A5',NULL,'csv','Indicador IC110A5.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T10746.CODIGO as c4,      T10746.DENOMINACION as c5,      case  when T19562.CODIGO < 19 then T19562.CODIGO else 19 end  as c6,      case  when T19562.CODIGO < 19 then T19562.DENOMINACION else case  when T6789.DENOMINACION = ''Castellano'' then ''90 y más'' else ''90 i més'' end  end  as c7,      T6789.CODIGO as c8,      T18685.ANYO as c33 from       LU_EDADAGR20PAD_IDI T19562,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T19562.IDIOMA and T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T6789.DENOMINACION = ''Castellano''  and T18685.EDADAGR = T19562.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T10746.CODIGO, T10746.DENOMINACION, T17978.DT, T17978.DTBA, case  when T19562.CODIGO < 19 then T19562.DENOMINACION else case  when T6789.DENOMINACION = ''Castellano'' then ''90 y más'' else ''90 i més'' end  end , case  when T19562.CODIGO < 19 then T19562.CODIGO else 19 end, T18685.ANYO ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c1 as c9,      D1.c8 as c10,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c10, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c10 ASC, D2.c1 ASC, D3.c1 ASC) as c13           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c10 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c10 = D3.c3      ) D1 where  ( D1.c13 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c33 as c33 from ( select       D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c5, c3, c6, c7, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.111276','Lozano','2021-07-27 13:55:20',NULL,58),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A6',NULL,'csv','Indicador IC110A6.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T18846.CODIGO as c4,      T18846.DENOMINACION as c5,      T10746.CODIGO as c6,      T10746.DENOMINACION as c7,      T6789.CODIGO as c8,      T18685.ANYO as c33 from       LU_II_PAD_LUGNAC_IDI T18846,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T18846.IDIOMA and T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T6789.DENOMINACION = ''Castellano''  and T18685.LUGNAC = T18846.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T10746.CODIGO, T10746.DENOMINACION, T17978.DT, T17978.DTBA, T18846.CODIGO, T18846.DENOMINACION, T18685.ANYO), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c1 as c8,      D1.c8 as c9,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c9 = D3.c3      ) D1 where  ( D1.c12 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c6, c7, c4, c2, c5, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.155505','Lozano','2021-07-27 13:55:20',NULL,59),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A7',NULL,'csv','Indicador IC110A7.csv','WITH  SAWITH0 AS (select distinct count(T18685.IDPADRON) over (partition by T6789.DENOMINACION, T10746.CODIGO, T17978.DTBA, T19568.CODIGO)  as c1,      T10746.CODIGO as c2,      T10746.DENOMINACION as c3,      T17978.DTBA as c4,      T19568.CODIGO as c5,      T19568.DENOMINACION as c6,      T17978.DT as c7,      T6789.CODIGO as c8,      count(T18685.IDPADRON) over (partition by T6789.DENOMINACION, T10746.CODIGO, T17978.DTBA, T56143.CODPAIS, T19568.CODIGO)  as c9,      T56143.CODPAIS as c10,      T18685.ANYO as c33 from       LU_EDADAGR3PAD_IDI T19568,      V_LU_GEO_PAIS_NACION T56143,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T19568.IDIOMA and T17978.DTBA = T18685.DISBAR and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T56143.IDIOMA and T18685.EDADAGR3 = T19568.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18685.PAINACDAD = T56143.CODPAIS and T18685.PAINACDAD <> 108 and T56143.CODPAIS <> 108 ) ), SAWITH1 AS (select 0 as c1,      D1.c7 as c2,      D1.c4 as c3,      D1.c6 as c4,      D1.c5 as c5,      D1.c10 as c6,      D1.c2 as c7,      D1.c3 as c8,      D1.c9 as c9,      D1.c1 as c10,      D1.c8 as c11,      D1.c33 as c33 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13,      D1.c33 as c33 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c11 as c13,                D1.c33 as c33,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c11, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3      ) D1 where  ( D1.c14 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c33 as c33 from       SAWITH4 D1 order by c1, c4, c2, c5, c3, c9, c10, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c33','N','sftp',NULL,NULL,'2021-08-10 08:20:31.198691','Lozano','2021-07-27 13:55:20',NULL,60),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC146A1',NULL,'csv','Indicador IC146A1.csv','select T6789.DENOMINACION ||'';''||      T8148.CODIGO ||'';''||      T8160.CODIGO ||'';''||      T8160.DENOMINACION ||'';''||      T7595.ANO ||'';''||      T7595.MES ||'';''||      T6789.CODIGO ||'';''||      replace(T8109.VALOR , '','' , ''.'') as c9 from       LU_I_MET_ESTMETEO T8148,      V_LU_TIEMPO_ANOMES T7595,      LU_IDIOMA T6789,      LU_I_MET_INDICADOR_IDI T8160,      FACT_I_MET_METEOANOMES T8109 where  ( T6789.CODIGO = T8160.IDIOMA and T8109.ESTMETEO = T8148.CODIGO and T7595.ANO = T8109.ANO and T7595.MES = T8109.MES and T6789.DENOMINACION = ''Castellano''  and T8109.ESTMETEO = 4 and T8109.INDICADOR = T8160.CODIGO and T8148.CODIGO = 4 and (T8109.INDICADOR in (28)) and (T8160.CODIGO in (28)) ) order by T7595.ANO, T7595.MES','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2021-08-10 08:20:31.330113','Lozano','2021-07-27 13:55:20',NULL,63);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A1',NULL,'csv','Indicador IC151A1.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select distinct T103896.NUMVIV1800 as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103896.DTBA ) ), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  as c4,      D1.c1 / nullif( D2.c1, 0) as c5,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  as c6 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.231006','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',87),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110',NULL,'csv','Indicador IC110.csv','WITH  SAWITH0 AS (select sum(T21230.NUMPERSONAS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_SEXO_IDI T10746,      FACT_II_PAD_AGREGADO T21230 where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Castellano'' and T10746.CODIGO = T21230.SEXO and T17978.DTBA = T21230.DISBAR )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c6 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.277715','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',88),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9',NULL,'csv','Indicador IC110A9.csv','WITH  SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T19610.IDIOMA and T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T18685.ANYO and T18685.EDAD100 = T19610.CODIGO )  group by T7594.ANO) select D1.c1 ||'';'' || D1.c2 || '';'' || D1.c3 from ( select 0 as c1,      D1.c2 as c2,      D1.c1 as c3 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3','N','sftp',NULL,NULL,'2021-08-10 08:20:32.320996','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',89),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9B',NULL,'csv','Indicador IC110A9B.csv','WITH  SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,      T10746.DENOMINACION as c2,      T7594.ANO as c3 from       LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T10746.IDIOMA and T6789.CODIGO = T19610.IDIOMA and T7594.ANO = T18685.ANYO and T6789.DENOMINACION = ''Castellano'' and T10746.CODIGO = T18685.SEXO and T18685.EDAD100 = T19610.CODIGO )  group by T7594.ANO, T10746.DENOMINACION) select D1.c1 || '';'' || D1.c2 || '';'' || D1.c3 || '';'' || D1.c4  from ( select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4 from       SAWITH0 D1 order by c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2021-08-10 08:20:32.364082','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',90),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9C',NULL,'csv','Indicador IC110A9C.csv','WITH  SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T10746.DENOMINACION as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      LU_SEXO_IDI T10746,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T19610.IDIOMA and T18685.DIS = T55787.DT and T6789.DENOMINACION = ''Castellano'' and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T18685.EDAD100 = T19610.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T10746.DENOMINACION, T17978.DTBA, T55787.DT), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH4 D1 order by c1, c7, c4, c2, c5, c3, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,C8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.407032','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',91),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A1',NULL,'csv','Indicador IC149A1.csv','WITH  SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      case  when T104038.ANO <= 1800 then ''<=1800'' else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then ''1801-1900'' else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then ''1901-1920'' else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then ''1921-1940'' else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then ''1941-1960'' else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then ''1961-1970'' else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then ''1971-1980'' else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then ''1981-1990'' else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then ''1991-2000'' else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then ''2001-10'' else case  when T104038.ANO > 2010 then ''>2010'' end  end  end  end  end  end  end  end  end  end  end  as c5,      case  when T104038.ANO <= 1800 then 1 else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then 2 else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then 3 else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then 4 else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then 5 else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then 6 else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then 7 else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then 8 else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then 9 else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then 10 else case  when T104038.ANO > 2010 then 11 end  end  end  end  end  end  end  end  end  end  end  as c6,      T6789.CODIGO as c7 from       V_LU_TIEMPO_ANO T104038 /* LU_IV_CAT_ANY1 */ ,      V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_IV_CAT_VIV T103797 where  ( T17978.DTBA = T103797.DTBA and T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T103797.DTSC and T55787.DT = T103797.DT and T6789.DENOMINACION = ''Castellano'' and T103797.ANY1 = T104038.ANO )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT, case  when T104038.ANO <= 1800 then ''<=1800'' else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then ''1801-1900'' else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then ''1901-1920'' else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then ''1921-1940'' else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then ''1941-1960'' else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then ''1961-1970'' else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then ''1971-1980'' else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then ''1981-1990'' else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then ''1991-2000'' else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then ''2001-10'' else case  when T104038.ANO > 2010 then ''>2010'' end  end  end  end  end  end  end  end  end  end  end , case  when T104038.ANO <= 1800 then 1 else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then 2 else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then 3 else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then 4 else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then 5 else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then 6 else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then 7 else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then 8 else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then 9 else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then 10 else case  when T104038.ANO > 2010 then 11 end  end  end  end  end  end  end  end  end  end  end ), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c1 as c7,      D1.c7 as c8 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3, c8, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2021-08-10 08:20:32.450449','Lozano-32028','2021-07-27 13:55:20','Lozano-32028',92),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A2',NULL,'csv','Indicador IC149A2.csv','WITH  SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,      T103931.DENOMINACION as c2,      T55787.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       LU_IV_CAT_SUPERF_IDI T103931,      LU_IV_CAT_ANT1800_IDI T103907,      V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_IV_CAT_VIV T103797 where  ( T6789.CODIGO = T103931.IDIOMA and T55787.DT = T103797.DT and T17978.DTBA = T103797.DTBA and T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T103797.DTSC and T6789.CODIGO = T103907.IDIOMA and T103797.ANT1800 = T103907.CODIGO and T6789.DENOMINACION = ''Castellano'' and T103797.ANT1800 = 0 and T103797.SUPERF = T103931.CODIGO and T103907.CODIGO = 0 )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT, T103931.DENOMINACION), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c4 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH4 D1 order by c1, c7, c2, c5, c3, c6, c4 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.494064','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',93),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3',NULL,'csv','Indicador IC149A3.csv','WITH  SAWITH0 AS (select T103896.NUMVIV1800 as c1,      sum(T103896.VALCAT_TOT) as c2,      sum(T103896.VCATS_MED) as c3,      sum(T103896.VALCAT_MED) as c4,      sum(T103896.VCATC_MED) as c5,      T17978.DT as c6,      T17978.DTBA as c7,      T7594.ANO as c8,      T6789.CODIGO as c9 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103896.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103896.NUMVIV1800), SAWITH1 AS (select 0 as c1,      D1.c6 as c2,      D1.c7 as c3,      D1.c8 as c4,      D1.c5 as c5,      D1.c4 as c6,      D1.c3 as c7,      D1.c2 as c8,      D1.c2 / nullif( D1.c1, 0) as c9,      D1.c1 as c10,      D1.c9 as c11 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c11 as c13,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c11, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3      ) D1 where  ( D1.c14 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2021-08-10 08:20:32.537665','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',94),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A4',NULL,'csv','Indicador IC149A4.csv','WITH  SAWITH0 AS (select sum(T103867.NUMVIV1800) as c1,      T103955.DENOMINACION as c2,      T17978.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_IV_CAT_VCATA_IDI T103955,      FACT_IV_CAT_VIV_AGRDTBAVC T103867 where  ( T6789.CODIGO = T103955.IDIOMA and T7594.ANO = T103867.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103867.DTBA and T103867.VALCATA = T103955.CODIGO )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103955.DENOMINACION), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 as c6,      D1.c6 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D3.c1 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c4 = D3.c2 and D1.c7 = D3.c3      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH4 D1 order by c1, c7, c5, c3, c6, c4, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.581011','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',95),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150',NULL,'csv','Indicador IC150.csv','WITH  SAWITH0 AS (select sum(T102892.NUMSOL) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRDTBA T102892 where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c7 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D2.c1 as c2,      D3.c1 as c3,      D1.c2 as c4,      D1.c3 as c5,      D1.c4 as c6,      D1.c5 as c7,      D1.c7 as c9,      ROW_NUMBER() OVER (PARTITION BY D1.c4, D1.c3, D1.c2, D2.c1, D3.c1 ORDER BY D1.c4 DESC, D1.c3 DESC, D1.c2 DESC, D2.c1 DESC, D3.c1 DESC) as c10 from       (           SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       (select D1.c1 as c1,                D1.c2 as c2,                D1.c3 as c3,                D1.c4 as c4,                D1.c5 as c5,                D1.c6 as c6,                D1.c7 as c7,                sum(case D1.c10 when 1 then D1.c7 else NULL end ) over ()  as c8,                D1.c9 as c9,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c9 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c9 ASC) as c10           from                 SAWITH4 D1      ) D1 where  ( D1.c10 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.624064','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',96);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A2',NULL,'csv','Indicador IC150A2.csv','WITH  SAWITH0 AS (select sum(T120156.NUMSOL) as c1,      sum(T120156.VCATS) as c2,      T7594.ANO as c3 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRTOTALDT T120156 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120156.ANO )  group by T7594.ANO) select D1.c1 || '';'' || D1.c2 || '';'' || D1.c3  from ( select distinct 0 as c1,      D1.c3 as c2,      round(D1.c2 / nullif( D1.c1, 0) , 2) as c3 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3','N','sftp',NULL,NULL,'2021-08-10 08:20:32.667228','Lozano-32054','2021-07-27 13:55:20','Lozano-32054',97),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A2B',NULL,'csv','Indicador IC150A2B.csv','WITH  SAWITH0 AS (select sum(T102892.NUMSOL) as c1,      sum(T102892.VCATS) as c2,      T17978.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRDTBA T102892 where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c3 as c2,      D1.c4 as c3,      D1.c5 as c4,      D1.c1 as c5,      D1.c2 as c6,      round(D1.c2 / nullif( D1.c1, 0) , 2) as c7,      D1.c6 as c8 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH4 D1 order by c1, c6, c2, c4, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.71015','Lozano-32054','2021-07-27 13:55:20','Lozano-32054',98),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A3',NULL,'csv','Indicador IC150A3.csv','WITH  SAWITH0 AS (select sum(T120156.SUPSOL) as c1,      sum(T120156.VCATS) as c2,      T7594.ANO as c3 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRTOTALDT T120156 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120156.ANO )  group by T7594.ANO) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 from ( select 0 as c1,      D1.c3 as c2,      D1.c1 as c3,      D1.c2 as c4,      D1.c2 / nullif( D1.c1, 0) as c5 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5','N','sftp',NULL,NULL,'2021-08-10 08:20:32.753223','Lozano-32054','2021-07-27 13:55:20','Lozano-32054',99),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A3B',NULL,'csv','Indicador IC150A3B.csv','WITH  SAWITH0 AS (select sum(T102892.SUPSOL) as c1,      sum(T102892.VCATS) as c2,      T17978.DT as c3,      T17978.DTBA as c4,      T7594.ANO as c5,      T6789.CODIGO as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_SOL_AGRDTBA T102892 where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c3 as c2,      D1.c4 as c3,      D1.c5 as c4,      D1.c1 as c5,      D1.c2 as c6,      D1.c2 / nullif( D1.c1, 0) as c7,      D1.c6 as c8 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2021-08-10 08:20:32.796307','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',100),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151',NULL,'csv','Indicador IC151.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c6 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.839367','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',101),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A2',NULL,'csv','Indicador IC151A2.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.DT = T55787.DT and T16529.DTBA = T17978.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  as c4,      D1.c1 / nullif( D2.c1, 0) as c5,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  as c6 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:32.882501','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',102),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC098',NULL,'csv','Indicador IC098.csv','WITH  SAWITH0 AS (select sum(T43966.VALOR) as c1,      T43989.DENOMINACION as c2,      T55787.DT as c3,      T7594.ANO as c4,      T6789.CODIGO as c5 from       LU_VIII_AGPO_IND_IDI T43989,      V_LU_DIVMUN_DT T55787,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_AGPO_FACTDT T43966 where  ( T6789.CODIGO = T43989.IDIOMA and T7594.ANO = T43966.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T43966.MUNICIPIO and T43966.DISTRITO = T55787.DT and T6789.DENOMINACION = ''Castellano'' and T43966.INDICADOR = T43989.CODIGO and T43966.INDICADOR = 3 and T43989.CODIGO = 3 )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T43989.DENOMINACION, T55787.DT), SAWITH1 AS (select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c1 as c5,      D1.c5 as c6 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       (select D1.c1 as c1,                D1.c2 as c2,                D2.c1 as c3,                D1.c3 as c4,                D1.c4 as c5,                D1.c5 as c6,                D1.c6 as c7,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC) as c8           from                 SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c6 = D2.c3      ) D1 where  ( D1.c8 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6 from       SAWITH3 D1 order by c1, c5, c4, c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6','N','sftp',NULL,NULL,'2021-08-10 08:20:32.925899','Lozano-32016','2021-07-27 13:55:20','Lozano-32016',103),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A1',NULL,'csv','Indicador IC110A1.csv','WITH  SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T18894.DENOMINACION as c2,      T7594.ANO as c3 from       LU_EDADES T22191,      LU_II_PAD_TIPVIV_IDI T18894,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_EDAD100PAD_IDI T19610,      FACT_II_PAD_PADRON T18685 where  ( T18685.EDAD = T22191.EDAD and T7594.ANO = T18685.ANYO and T6789.CODIGO = T19610.IDIOMA and T18685.EDAD100 = T19610.CODIGO and T6789.CODIGO = T18894.IDIOMA and T18685.TIPLOC = T18894.CODIGO and T6789.DENOMINACION = ''Castellano'' and T18894.DENOMINACION = ''Vivienda familiar'' and T19610.CODIGO = T22191.EDAD100 and T18685.EDAD <= 17 and T22191.EDAD <= 17 )  group by T7594.ANO, T18894.DENOMINACION) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4  from ( select 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c1 as c4 from       SAWITH0 D1 order by c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2021-08-10 08:20:32.96903','Lozano-32016','2021-07-27 13:55:20','Lozano-32016',104),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149',NULL,'csv','Indicador IC49.csv','WITH  SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_DIVMUN_DTSC T22801,      FACT_IV_CAT_VIV T103797 where  ( T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T17978.DTBA = T103797.DTBA and T22801.DTSC = T103797.DTSC and T6789.DENOMINACION = ''Castellano''  and T55787.DT = T103797.DT  )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT), SAWITH1 AS (select sum(T103843.NUMVIV) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRANYDTBA T103843 where  ( T7594.ANO = T103843.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103843.DTBA  )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH2 AS (select distinct T103896.NUMVIV1800 as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano''  and T17978.DTBA = T103896.DTBA ) ), SAWITH3 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 when D3.c2 is not null then D3.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 when D3.c3 is not null then D3.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 when D3.c4 is not null then D3.c4 end  as c4,      D1.c1 as c5,      D2.c1 as c6,      D3.c1 as c7,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 when D3.c5 is not null then D3.c5 end  as c8 from       (           SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ) full outer join SAWITH2 D3 On D3.c2 = case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  and D3.c3 = case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  and D3.c4 = case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  and D3.c5 = case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  and  SYS_OP_MAP_NONNULL(D3.c6) = SYS_OP_MAP_NONNULL(case  when D1.c6 is not null then D1.c6 when D2.c6 is not null then D2.c6 end ) ), SAWITH4 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH5 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH6 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11           from                 (                     SAWITH3 D1 inner join SAWITH4 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH5 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3      ) D1 where  ( D1.c11 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9 from       SAWITH6 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2021-08-10 08:20:33.011967','Lozano-32016','2021-07-27 13:55:20','Lozano-32016',105),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A21',NULL,'csv','Indicador IC151A21.csv','select sup_aparcamientos.c2 ||'';''|| sup_aparcamientos.c1/num_turismos.c1 from (select sum(T120150.SUPCONS) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRTOTALDT T120150 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120150.ANO )  group by T7594.ANO order by c2) sup_aparcamientos,  (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T6789.CODIGO = T16638.IDIOMA and T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T16529.ANO and T16529.ANYMAT = T16638.CODIGO )  group by T7594.ANO order by c2) num_turismos where sup_aparcamientos.c2=num_turismos.c2','c1,c2','N','sftp',NULL,NULL,'2021-08-10 08:20:33.055405','Lozano-32055','2021-07-27 13:55:20','Lozano-32055',106);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A2',NULL,'csv','Indicador IC151A2.csv','WITH  SAWITH0 AS (select sum(T103202.SUPCONS) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_APAR_AGRDTBA T103202 where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA), SAWITH1 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,      T55787.DT as c2,      T17978.DTBA as c3,      T7594.ANO as c4,      T6789.CODIGO as c5,      T6789.DENOMINACION as c6 from       V_LU_DIVMUN_DT T55787,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_III_MAT_ANYMAT_IDI T16638,      FACT_III_MAT_MATRICULA T16529 where  ( T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.DT = T55787.DT and T16529.DTBA = T17978.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT), SAWITH2 AS (select 0 as c1,      case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,      case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c3,      case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  as c4,      D1.c1 / nullif( D2.c1, 0) as c5,      case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  as c6 from       SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ), SAWITH3 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH4 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH5 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9           from                 (                     SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3      ) D1 where  ( D1.c9 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7 from       SAWITH5 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001 ','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2021-08-10 08:20:33.098404','Lozano-32055','2021-07-27 13:55:20','Lozano-32055',107),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3',NULL,'csv','Indicador IC149A3.csv','WITH  SAWITH0 AS (select T103896.NUMVIV1800 as c1,      sum(T103896.VALCAT_TOT) as c2,      sum(T103896.VCATS_MED) as c3,      sum(T103896.VALCAT_MED) as c4,      sum(T103896.VCATC_MED) as c5,      T17978.DT as c6,      T17978.DTBA as c7,      T7594.ANO as c8,      T6789.CODIGO as c9 from       V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCDTBA T103896 where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103896.DTBA )  group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103896.NUMVIV1800), SAWITH1 AS (select 0 as c1,      D1.c6 as c2,      D1.c7 as c3,      D1.c8 as c4,      D1.c5 as c5,      D1.c4 as c6,      D1.c3 as c7,      D1.c2 as c8,      D1.c2 / nullif( D1.c1, 0) as c9,      D1.c1 as c10,      D1.c9 as c11 from       SAWITH0 D1), SAWITH2 AS (select T58175.DENOMINACION as c1,      T58175.CODIGO as c2,      T58175.IDIOMA as c3 from       LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ), SAWITH3 AS (select T58289.DENOMINACION as c1,      T58289.CODIGO as c2,      T58289.IDIOMA as c3 from       LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ), SAWITH4 AS (select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12,      D1.c13 as c13 from       (select D1.c1 as c1,                D2.c1 as c2,                D3.c1 as c3,                D1.c2 as c4,                D1.c3 as c5,                D1.c4 as c6,                D1.c5 as c7,                D1.c6 as c8,                D1.c7 as c9,                D1.c8 as c10,                D1.c9 as c11,                D1.c10 as c12,                D1.c11 as c13,                ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c11, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14           from                 (                     SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3      ) D1 where  ( D1.c14 = 1 ) ) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 from ( select D1.c1 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c6 as c6,      D1.c7 as c7,      D1.c8 as c8,      D1.c9 as c9,      D1.c10 as c10,      D1.c11 as c11,      D1.c12 as c12 from       SAWITH4 D1 order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2021-08-10 08:20:33.141728','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',108),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3_1',NULL,'csv','Indicador IC149A3_1.csv','WITH  SAWITH0 AS (select sum(T120168.VALCAT_MED) as c1,      T7594.ANO as c2 from       V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      FACT_IV_CAT_VIV_AGRVCTOTALDT T120168 where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120168.ANO )  group by T7594.ANO) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 from ( select 0 as c1,      D1.c2 as c2,      D1.c1 as c3 from       SAWITH0 D1 order by c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2021-08-10 08:20:33.18599','Lozano-32055','2021-07-27 13:55:20','Lozano-32055',109),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/pre/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/pre/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC097',NULL,'csv','Indicador IC097.csv','WITH  SAWITH0 AS (select sum(T43974.VALOR) as c1,      T43989.DENOMINACION as c2,      T44001.CODIGO as c3,      T44001.DENOMINACION as c4,      T7594.ANO as c5 from       LU_VIII_AGPO_TIPAB_IDI T44001,      LU_VIII_AGPO_IND_IDI T43989,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      V_LU_GEO_MUNICIPIO T6910,      FACT_VIII_AGPO_INFABONADOS T43974 where  ( T6789.CODIGO = T44001.IDIOMA and T7594.ANO = T43974.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T43974.MUNICIPIO and T6789.CODIGO = T43989.IDIOMA and T43974.INDICADOR = T43989.CODIGO and T6789.DENOMINACION = ''Castellano'' and T43974.TIPOABONADO = T44001.CODIGO and T43974.TIPOABONADO = 1 and T43989.DENOMINACION = ''Agua facturada miles m3'' and T44001.CODIGO = 1 )  group by T7594.ANO, T43989.DENOMINACION, T44001.CODIGO, T44001.DENOMINACION) select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6  from ( select distinct 0 as c1,      D1.c2 as c2,      D1.c3 as c3,      D1.c4 as c4,      D1.c5 as c5,      D1.c1 * 1000000 / 365 as c6 from       SAWITH0 D1 order by c5, c2, c4, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6','N','sftp',NULL,NULL,'2021-08-10 08:20:32.18786','Lozano-31390','2021-07-27 13:55:20','Lozano-31390',86),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SEDE',NULL,NULL,'/pre/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/pre/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC15',NULL,'csv','Indicador ISTIC15','select count(*) ||'';''||  TO_CHAR(dindate, ''YYYY-MM'') from pro2_ocs.docmeta d, pro2_ocs.revisions r where xaytoestadoelaboracionNTI = ''EE02'' and ddoctitle like ''Padron%'' and dindate >= (SELECT trunc(trunc(sysdate,''month'')-1,''month'') FROM DUAL) AND dindate < (SELECT trunc((sysdate),''month'') FROM DUAL) and d.did = r.did group by TO_CHAR(dindate, ''YYYY-MM'')','resultado,fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.501965','Ticket-23401','2021-07-27 13:55:20',NULL,69),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC01_#ddMMyyyy#',NULL,'csv','Indicador ISTIC01','select count(*)||'';''||TO_CHAR(Fecha_acceso, ''YYYY-MM'') from acceso_sede where fecha_acceso >=trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'')  AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''), ''MM'') group by TO_CHAR(Fecha_acceso, ''YYYY-MM'')','Accesos_autenticados,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:31.844888','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',77),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC10_#ddMMyyyy#',NULL,'csv','Indicador ISTIC10','select count(*) ||'';''|| TO_CHAR(Fecha_acceso, ''YYYY-MM'') from acceso_sede  where EXTRACT(month FROM Fecha_acceso) = (EXTRACT(month FROM TO_DATE(''#fecha#'',''yyyyMMdd''))-1) and EXTRACT(year FROM Fecha_acceso) = EXTRACT(year FROM TO_DATE(''#fecha#'',''yyyyMMdd'')) group by TO_CHAR(Fecha_acceso, ''YYYY-MM'')','Accesos_autenticados,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:31.888053','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',79),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC12_#ddMMyyyy#',NULL,'csv','Indicador ISTIC12','SELECT ta.autoridad ||'';''|| count(*) ||'';''|| TO_CHAR(fecha_acceso,''YYYY-MM-DD'') AS Num_Accesos FROM acceso_sede acs, sede_electro.tipo_acceso ta WHERE acs.id_tipo_acceso = ta.id_tipo_acceso  AND TO_CHAR(acs.FECHA_ACCESO, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') - 1, ''YYYY-MM-DD'') AND (acs.id_tipo_acceso is not null) AND acs.id_resultado_login = 1 GROUP BY ta.autoridad, TO_CHAR(fecha_acceso,''YYYY-MM-DD'') ORDER BY Num_accesos DESC','Autoridad,Num_Accesos,Fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.930958','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',80),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC13_#ddMMyyyy#',NULL,'csv','Indicador ISTIC13','select Asunto ||'';''|| count(asunto) ||'';''|| TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') from instancia_presentada where TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') -1, ''YYYY-MM-DD'')  group by Asunto, TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') order by TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') desc','Asunto,Instancias,Fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.973724','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',81),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC14_#ddMMyyyy#',NULL,'csv','Indicador ISTIC14','select count(*) ||'';''|| TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') from tramite  where TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') -1, ''YYYY-MM-DD'') group by TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') order by TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') desc','Tramites,Fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:32.016525','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',82);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC16_#ddMMyyyy#',NULL,'csv','Indicador ISTIC16','select TO_CHAR(primer_acceso, ''YYYY-MM'') ||'';''|| count(*) from (       select min(fecha_acceso) as primer_acceso, num_documento_usuario        from  acceso_sede        where id_resultado_login = 1        group by num_documento_usuario) where  primer_acceso >=trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'')  AND primer_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''), ''MM'') group by TO_CHAR(primer_acceso, ''YYYY-MM'') order by TO_CHAR(primer_acceso, ''YYYY-MM'') desc','Primer_acceso,Numero','S','sftp',NULL,NULL,'2021-08-10 08:20:32.059316','Ivan 29091','2021-07-27 13:55:20','Ivan 31670',83),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/pre/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC53',NULL,'csv','Indicador ISTIC53','SELECT count(*) ||'';''||  TO_CHAR(fecha_registro, ''YYYY-MM'') FROM instancia_presentada WHERE nif_interesado <> nif_tramitador AND fecha_registro >=(SELECT trunc(trunc(sysdate,''month'')-1,''month'') FROM DUAL) AND fecha_registro  < (SELECT trunc((sysdate),''month'') FROM DUAL) GROUP BY TO_CHAR(fecha_registro, ''YYYY-MM'')','resultado,fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.587895','Ticket-23401','2021-07-27 13:55:20',NULL,71),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02C_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02C_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad = ''DNIe'' group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.446975','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',162),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02C_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02C_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad = ''DNIe'' group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.49008','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',163),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02B_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02B_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad = (''Cl@ve'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.403496','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',161),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02D_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02D_mensual','SELECT autoridad ||'';''|| replace(porcentaje,'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad not in (''FNMT-Ceres'',''ACCV'',''Cl@ve'',''DNIe'')','Autoridad,Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.532994','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',164),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02D_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02D_anual','SELECT autoridad ||'';''|| replace(porcentaje,'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad not in (''FNMT-Ceres'',''ACCV'',''Cl@ve'',''DNIe'')','Autoridad,Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.576146','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',165),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/pre/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC20',NULL,'csv','Indicador ISTIC20','SELECT * FROM (select Asunto ||'';''||  count(asunto) ||'';''||  TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') from instancia_presentada where TO_CHAR(Fecha_Registro, ''YYYYMMDD'') = TO_CHAR(sysdate -1, ''YYYYMMDD'') group by Asunto, TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') order by count(asunto) desc) WHERE ROWNUM <= 10','asunto,instancias,fecha','S','sftp',NULL,NULL,'2021-08-10 08:20:31.54484','Ticket-23401','2021-07-27 13:55:20',NULL,70),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02A_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02A_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad IN (''FNMT-Ceres'',''ACCV'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.27443','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',158),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02A_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02A_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad IN (''FNMT-Ceres'',''ACCV'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.31713','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',159);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/pre/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02B_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02B_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(Fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(Fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad = (''Cl@ve'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2021-08-10 08:20:33.359934','Ivan 29772','2021-07-27 13:55:20','Ivan 29772',160),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','PostGis_Plataforma',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_TRAMOS_CONGESTION/','/pre/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_TRAMOS_CONGESTION/',NULL,'tramos_congestion',NULL,'csv','tramos_congestion','select concat(id_tramo,'';'',desc_tramo_cas,'';'', desc_tramo_val) from t_d_trafico_tramos_congestiones;','id_tramo, desc_tramo_cas, desc_tramo_val','S','sftp',NULL,NULL,'2021-08-10 08:20:31.758499','toni-28427','2021-07-27 13:55:20',NULL,75),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','SQL_Server_Trafico',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_CONGESTION/','/pre/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_CONGESTION/',NULL,'congestion_diaria',NULL,'csv','congestion_diaria','SELECT concat( rec.Rec, '';'',''#fecha#'', '';'', round(convert(float,sum([PorcC]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' , round(convert(float,sum([PorcD]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' , round(convert(float,sum([PorcFX]))/convert(float,(SELECT count(rec2.Rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' , ( 100-(round(convert(float,sum([PorcC]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0)+  round(convert(float,sum([PorcD]))/convert(float,(SELECT count(rec2.rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0)+  round(sum(convert(float,[PorcFX]))/convert(float,(SELECT count(rec2.Rec) FROM [EstadisticasGIP].[dbo].[Recorr] rec2 WHERE rec2.Rec =rec.Rec and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0))) ) FROM [EstadisticasGIP].[dbo].[Recorr] rec WHERE convert(date,DATEADD(HOUR,-1,Hora))=CAST(''#fecha#'' as date) group by rec.Rec order by rec.Rec;','recorrido, dia, porc_denso, porc_congestion, porc_cortado, porc_fluido','S','sftp',NULL,NULL,'2021-08-10 08:20:31.715839','toni-28427','2021-07-27 13:55:20',NULL,74),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','SQL_Server_Trafico',NULL,NULL,'/pre/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_INTENSIDAD/','/pre/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_INTENSIDAD/',NULL,'intensidad_diaria',NULL,'csv','intensidad_diaria','SELECT concat(dea.nombre,'';'', convert(date,dapm.FechaHora),'';'', (sum(dapm.intensidad)/[PROPERTY_TRAFICO_NUM_MEDIDAS_HOR])) FROM  [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado] dea,  [WEB_TRAFICO].[dbo].[Datos_Aforados_PM] dapm where dea.idTA=dapm.iDTA and dea.estado=''A''and convert(date,dapm.FechaHora)=CAST(''#fecha#'' as date) group by dea.nombre, convert(date,dapm.FechaHora)','id_tramo, fecha, intensidad_horaria','S','sftp',NULL,NULL,'2021-08-10 08:20:31.630582','toni-28427','2021-07-27 13:55:20','Ivan 34096',72),
			('VLCi_ETL_INCO','FTP','FTP Sertic','SMC*INCO*','//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi','/pre/01.fichOrig/VLCi_ETL_INCO/','/pre/02.fichBackup/VLCi_ETL_INCO/',NULL,'SMC-INCO-yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2023-09-22 08:20:29.404222','Ticket 1914','2023-09-22 08:20:29.404222','Ticket 1914',230);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('py_calidadambiental_sonometros_zas_WOO', 'FTP', 'FTP Sertic', '*WOO', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.WOO', 'N', 'WOO', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_MAN', 'FTP', 'FTP Sertic', '*MAN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.MAN', 'N', 'MAN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_SER', 'FTP', 'FTP Sertic', '*SER', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.SER', 'N', 'SER', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_HON', 'FTP', 'FTP Sertic', '*HON', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.HON', 'N', 'HON', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_CED', 'FTP', 'FTP Sertic', '*CED', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.CED', 'N', 'CED', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_PIN', 'FTP', 'FTP Sertic', '*PIN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.PIN', 'N', 'PIN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_SEN', 'FTP', 'FTP Sertic', '*SEN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.SEN', 'N', 'SEN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_QUA', 'FTP', 'FTP Sertic', '*QUA', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.QUA', 'N', 'QUA', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_BUE', 'FTP', 'FTP Sertic', '*BUE', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.BUE', 'N', 'BUE', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_UNI', 'FTP', 'FTP Sertic', '*UNI', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.UNI', 'N', 'UNI', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_xlsx', 'FTP', 'FTP Sertic', '*xlsx', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.xlsx', 'N', 'xlsx', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_xls', 'FTP', 'FTP Sertic', '*xls', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/pre/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/pre/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.xls', 'N', 'xls', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('py_trafico_intensidad_validada', 'BBDD', 'SQL_Server_Trafico', NULL, NULL, '/pre/01.fichOrig/VLCI_PY_TRAFICO_VALIDADADO/', '/pre/02.fichBackup/VLCI_PY_TRAFICO_VALIDADADO/', NULL, 'intensidad_trafico_validada_30_#ddMMyyyyHHmmss#', NULL, 'csv', 'intensidad_trafico_validada_30', 
			'SELECT concat(''Kpi-Accesos-Ciudad-Coches'', '';'', actual.idata, '';'', descripciones.descripcion, '';'', descripciones.descripcion_corta, '';'', descripciones.nombre_corto, '';'', actual.int_actual, '';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A373'',''A406'',''A72'',''A59'',''A1'',''A82'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA,vid.DÍA) actual
			inner join (
    			select 4 as idATA, ''Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)'' as descripcion, ''Accés Barcelona (entrada i eixida)'' as descripcion_corta, ''Accés Barcelona'' as nombre_corto union
    			select 57 as idATA, ''Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)''  as descripcion, ''Accés a Arxiduc Carles pel Camí nou de Picanya'' as descripcion_corta, ''Arxiduc Carles'' as nombre_corto union
    			select 70 as idATA, ''Av. del Cid (Entre V-30 i Tres Creus)''  as descripcion, ''Av. del Cid'' as descripcion_corta, ''Av. del Cid'' as nombre_corto union
    			select 78 as idATA, ''Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)''  as descripcion, ''Corts Valencianes (Accés per CV-35)'' as descripcion_corta, ''Corts Valencianes'' as nombre_corto union
    			select 349 as idATA, ''Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)''      as descripcion, ''Accés per V-31 (Pista de Silla)'' as descripcion_corta, ''Pista de Silla'' as nombre_corto union
    			select 379 as idATA, ''Prolongació Joan XXIII (Entre Germans Machado i Salvador Cerveró)''  as descripcion, ''Prolongació Joan XXIII'' as descripcion_corta, ''Joan XXIII'' as nombre_corto) as descripciones
    			on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Accesos-Vias-Coches'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion_corta,'';'', descripciones.nombre_corto,'';'', actual.int_actual,'';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A116'',''A121'',''A97'',''A117'',''A187'',''A51'') 
			and vid.DÍA = CAST(''#fecha#'' as datetime)   
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 50 as idATA, ''Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)''  as descripcion, ''Av. Blasco Ibáñez'' as descripcion_corta, ''Blasco Ibáñez'' as nombre_corto union
				select 93 as idATA, ''Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)''  as descripcion, ''Av. Dr. Peset Aleixandre'' as descripcion_corta, ''Dr. Peset Aleixandre'' as nombre_corto union
				select 111 as idATA, ''Av. de Giorgeta (Entre Sant Vicent i Jesús)''  as descripcion, ''Av. de Giorgeta'' as descripcion_corta, ''Giorgeta'' as nombre_corto union
				select 112 as idATA, ''Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)''  as descripcion, ''Gran Via de Ferran el Catòlic'' as descripcion_corta, ''Ferran el Catòlic'' as nombre_corto union
				select 116 as idATA, ''Gran Via del Marqués del Túria (Entre Pont d''''Aragó i Hernan Cortés)''  as descripcion, ''Gran Via del Marqués del Túria'' as descripcion_corta, ''Marqués del Túria'' as nombre_corto union
				select 177 as idATA, ''Av. de Peris i Valero (Entre Ausiàs March i Sapadors)''  as descripcion, ''Av. de Peris i Valero'' as descripcion_corta, ''Peris i Valero'' as nombre_corto) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Accesos-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			FROM
			(select afo.Descripcion as descripcion,afo.idATA as idata,  sum(vid.IMD*afo.Coeficiente/100) as intactual,vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''F36'',''F37'',''F38'',''F39'',''F40'',''F41'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 725 as idATA, ''Navarro Reverter'' as descripcion union
				select 726 as idATA, ''Pont de fusta'' as descripcion union
				select 727 as idATA, ''Pont de les arts'' as descripcion union
				select 728 as idATA, ''Pont del real'' as descripcion union
				select 729 as idATA, ''Carrer Alacant'' as descripcion union
				select 730 as idATA, ''Carrer Russafa'' as descripcion) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			from
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as intactual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo on 	vid.IdTA=afo.IdTA
			where afo.nombre in (''F31'',''F32'',''F33'',''F34'',''F35'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by 	afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join(
				select 720 as idATA, ''Carrer Colon'' as descripcion union
				select 721 as idATA, ''Comte de Trénor – Pont de fusta''  as descripcion union
				select 722 as idATA, ''Guillem de Castro''  as descripcion union
				select 723 as idATA, ''Xàtiva''  as descripcion union
				select 724 as idATA, ''Plaça Tetuan''  	as descripcion) as descripciones
			on actual.IdATA=descripciones.idATA
			', 'tipo,idata,descripcion, descripcion_corta, nombre_corto,int_actual,calculation_period', 'N', 'sftp', NULL, NULL, now(), 'TICKET 2572', now(), 'TICKET 2572', 300),
			('py_trafico_intensidad_validada', 'BBDD', 'SQL_Server_Trafico', NULL, NULL, '/pre/01.fichOrig/VLCI_PY_TRAFICO_VALIDADADO/', '/pre/02.fichBackup/VLCI_PY_TRAFICO_VALIDADADO/', NULL, 'intensidad_trafico_validada_365_#ddMMyyyyHHmmss#', NULL, 'csv', 'intensidad_trafico_validada_365', 
			'SELECT concat(''Kpi-Accesos-Ciudad-Coches'', '';'', actual.idata, '';'', descripciones.descripcion, '';'', descripciones.descripcion_corta, '';'', descripciones.nombre_corto, '';'', actual.int_actual, '';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A373'',''A406'',''A72'',''A59'',''A1'',''A82'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA,vid.DÍA) actual
			inner join (
    			select 4 as idATA, ''Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)'' as descripcion, ''Accés Barcelona (entrada i eixida)'' as descripcion_corta, ''Accés Barcelona'' as nombre_corto union
    			select 57 as idATA, ''Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)''  as descripcion, ''Accés a Arxiduc Carles pel Camí nou de Picanya'' as descripcion_corta, ''Arxiduc Carles'' as nombre_corto union
    			select 70 as idATA, ''Av. del Cid (Entre V-30 i Tres Creus)''  as descripcion, ''Av. del Cid'' as descripcion_corta, ''Av. del Cid'' as nombre_corto union
    			select 78 as idATA, ''Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)''  as descripcion, ''Corts Valencianes (Accés per CV-35)'' as descripcion_corta, ''Corts Valencianes'' as nombre_corto union
    			select 349 as idATA, ''Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)''      as descripcion, ''Accés per V-31 (Pista de Silla)'' as descripcion_corta, ''Pista de Silla'' as nombre_corto union
    			select 379 as idATA, ''Prolongació Joan XXIII (Entre Germans Machado i Salvador Cerveró)''  as descripcion, ''Prolongació Joan XXIII'' as descripcion_corta, ''Joan XXIII'' as nombre_corto) as descripciones
    			on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Accesos-Vias-Coches'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion_corta,'';'', descripciones.nombre_corto,'';'', actual.int_actual,'';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A116'',''A121'',''A97'',''A117'',''A187'',''A51'') 
			and vid.DÍA = CAST(''#fecha#'' as datetime)   
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 50 as idATA, ''Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)''  as descripcion, ''Av. Blasco Ibáñez'' as descripcion_corta, ''Blasco Ibáñez'' as nombre_corto union
				select 93 as idATA, ''Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)''  as descripcion, ''Av. Dr. Peset Aleixandre'' as descripcion_corta, ''Dr. Peset Aleixandre'' as nombre_corto union
				select 111 as idATA, ''Av. de Giorgeta (Entre Sant Vicent i Jesús)''  as descripcion, ''Av. de Giorgeta'' as descripcion_corta, ''Giorgeta'' as nombre_corto union
				select 112 as idATA, ''Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)''  as descripcion, ''Gran Via de Ferran el Catòlic'' as descripcion_corta, ''Ferran el Catòlic'' as nombre_corto union
				select 116 as idATA, ''Gran Via del Marqués del Túria (Entre Pont d''''Aragó i Hernan Cortés)''  as descripcion, ''Gran Via del Marqués del Túria'' as descripcion_corta, ''Marqués del Túria'' as nombre_corto union
				select 177 as idATA, ''Av. de Peris i Valero (Entre Ausiàs March i Sapadors)''  as descripcion, ''Av. de Peris i Valero'' as descripcion_corta, ''Peris i Valero'' as nombre_corto) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Accesos-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			FROM
			(select afo.Descripcion as descripcion,afo.idATA as idata,  sum(vid.IMDvid.IMD*afo.Coeficiente/100) as intactual,vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''F36'',''F37'',''F38'',''F39'',''F40'',''F41'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 725 as idATA, ''Navarro Reverter'' as descripcion union
				select 726 as idATA, ''Pont de fusta'' as descripcion union
				select 727 as idATA, ''Pont de les arts'' as descripcion union
				select 728 as idATA, ''Pont del real'' as descripcion union
				select 729 as idATA, ''Carrer Alacant'' as descripcion union
				select 730 as idATA, ''Carrer Russafa'' as descripcion) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			from
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as intactual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo on 	vid.IdTA=afo.IdTA
			where afo.nombre in (''F31'',''F32'',''F33'',''F34'',''F35'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by 	afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join(
				select 720 as idATA, ''Carrer Colon'' as descripcion union
				select 721 as idATA, ''Comte de Trénor – Pont de fusta''  as descripcion union
				select 722 as idATA, ''Guillem de Castro''  as descripcion union
				select 723 as idATA, ''Xàtiva''  as descripcion union
				select 724 as idATA, ''Plaça Tetuan''  	as descripcion) as descripciones
			on actual.IdATA=descripciones.idATA
			', 'tipo,idata,descripcion, descripcion_corta, nombre_corto,int_actual,calculation_period', 'N', 'sftp', NULL, NULL, now(), 'TICKET 2572', now(), 'TICKET 2572', 301);
		INSERT INTO t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) 
		VALUES ('py_personal_ayuntamiento','FTP','FTP Sertic','*.CSV','//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal/PRE','/pre/01.fichOrig/VLCI_PERSONAL_AYUNTAMIENTO/','/pre/02.fichBackup/VLCI_PERSONAL_AYUNTAMIENTO/',NULL,'*.CSV','S','CSV',NULL,NULL,NULL,'N','sftp',NULL,NULL,NOW(),'Ticket 3225',NULL,NULL);


	ELSE 

		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_MOBILIS','HTTP','HTTP',NULL,NULL,'/prod/01.fichOrig/EMT_Mobilis/','/prod/02.fichBackup/EMT_Mobilis/','http://www.emtvalencia.es/ciudadano/google_transit/datos/carga','mobilis',NULL,'txt',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2018-05-03 08:44:05','ETL Loader','2021-08-31 11:24:58.018016',NULL,31),
			('VLCI_ETL_EMT_MOBILIS_CARGATITULOS','HTTP','HTTP',NULL,NULL,'/prod/01.fichOrig/EMT_Mobilis_Titulos/','/prod/02.fichBackup/EMT_Mobilis_Titulos/','http://www.emtvalencia.es/ciudadano/google_transit/datos/titulos.txt','titulos',NULL,'txt',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2018-05-03 08:44:05','ETL Loader','2021-08-31 11:24:58.018016',NULL,30),
			('VLCI_ETL_EMT_MOBILIS_CARGAREFERENCIAS','HTTP','HTTP',NULL,NULL,'/prod/01.fichOrig/EMT_Mobilis_Transit/','/prod/02.fichBackup/EMT_Mobilis_Transit/','http://www.emtvalencia.es/ciudadano/google_transit/google_transit.zip','google_transit',NULL,'zip',NULL,NULL,NULL,'S','sftp',NULL,NULL,'2018-05-03 08:44:05','ETL Loader','2021-08-31 11:24:58.018016',NULL,29),
			('CDMGE GACO','FTP','FTP Sertic','SMC-GACO-*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/SIEM','/prod/01.fichOrig/CDMGE_GACO/','/prod/02.fichBackup/CDMGE_GACO/',NULL,'SMC-GACO-_yyyy','S','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2017-12-11 09:13:10',NULL,'2021-07-06 08:10:40',NULL,19),
			('CDMGE Codigo Organico','FTP','FTP Sertic','SMC-TBOR-*','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/SIEM','/prod/01.fichOrig/CDMGE_TBOR/','/prod/02.fichBackup/CDMGE_TBOR/',NULL,'SMC-TBOR-_yyyy','S','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2017-12-11 09:13:27',NULL,'2021-07-06 08:10:28',NULL,20),
			('VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS','FTP','FTP Sertic','*listadoUnidadesAdministrativas.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/','/prod/01.fichOrig/VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS/','/prod/02.fichBackup/VLCI_VALENCIA_ETL_UNIDADES_ADMINISTRATIVAS/',NULL,'*listadoUnidadesAdministrativas.xlsx','S','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2018-01-23 08:43:35',NULL,'2021-07-20 08:23:07',NULL,24),
			('VLCi_CDMGE_CONTRATACION','FTP','FTP Sertic','*listado_expedientes_contratacion.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE/contratacion/PRO','/prod/01.fichOrig/VLCi_CONTRATACION/','/prod/02.fichBackup/VLCi_CONTRATACION/',NULL,'*listado_expedientes_contratacion.xlsx','S','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2018-03-20 08:52:32','ETL Loader', '2024-02-06 08:52:32','Cecilia - 2472', NULL),
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_REALIZADAS','FTP','FTP Sertic','*_tareasRealizadas.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE','/prod/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/prod/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_tareasRealizadas.xlsx','S','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2018-04-16 08:41:36','ETL Loader','2021-07-06 08:10:20',NULL,26),
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_PENDIENTES','FTP','FTP Sertic','*_tareasPendientes.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE','/prod/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/prod/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_tareasPendientes.xlsx','S','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2018-04-16 08:41:36','ETL Loader','2021-07-06 08:10:20',NULL,27);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_CDMGE_RENDIMIENTO_ADMINISTRATIVO_USUARIOS','FTP','FTP Sertic','*_usuariosPorUnidad.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP/Integraciones/PIAE','/prod/01.fichOrig/VLCi_CDMGE_REND_ADMIN/','/prod/02.fichBackup/VLCi_CDMGE_REND_ADMIN/',NULL,'yyyy_usuariosPorUnidad.xlsx','S','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2018-04-16 08:41:36','ETL Loader','2021-07-06 08:10:20',NULL,28),
			('ETL VLCi Vados','FTP','FTP Sertic','Informacion_tributaria_Vados_*','//dades2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi','/prod/01.fichOrig/VLCi_Vados/','/prod/02.fichBackup/VLCi_Vados/',NULL,'Informacion_tributaria_Vados_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2017-12-11 09:11:53','ETL Loader','2021-08-31 11:24:58.018016',NULL,18),
			('VLCi Sertic Red Pagos','FTP','FTP Sertic','*.xlsx','//dades1tic/DADES1TIC/ayun/CIM/TelefonicaFTP','/prod/01.fichOrig/VLCi_Sertic_Red_Pagos/','/prod/02.fichBackup/VLCi_Sertic_Red_Pagos/',NULL,'yyyy.xlsx','S','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2017-12-11 09:11:00','ETL Loader','2021-08-31 11:24:58.018016',NULL,16),
			('VLCi_Valencia_ETL_IAE','FTP','FTP Sertic','Informacion_tributaria_IAE_*.txt','//dades2/dades2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi','/prod/01.fichOrig/IAE/','/prod/02.fichBackup/IAE/',NULL,'Informacion_tributaria_IAE_yyyy.txt','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2017-07-03 09:03:39','ETL Loader','2021-08-31 11:24:58.018016',NULL,1),
			('VLCi_Valencia_ETL_PMP_Facturas','FTP','FTP Sertic','PMP_UA_*','//dades2/dades2/ayun/TESORERIA/Jefatura de Servicio/PMP/Smartcity','/prod/01.fichOrig/PMP_facturas/','/prod/02.fichBackup/PMP_facturas/',NULL,'PMP_UA_yyyy.txt','N','zip',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2017-07-03 09:03:39','ETL Loader','2021-08-31 11:24:58.018016',NULL,2);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('ETL_Trafico_congestiones','FTP','FTP Sertic','CONGESTIONES *','//dades2/DADES2/ayun/TRAFICO/Plataforma VLCi','/prod/01.fichOrig/VLCi_Trafico_Congestiones/','/prod/02.fichBackup/VLCi_Trafico_Congestiones/',NULL,'CONGESTIONES_yyyy','S','xls',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2017-10-30 09:34:15','ETL Loader','2021-08-31 11:24:58.018016',NULL,4),
			('VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL','FTP','FTP Sertic','residuos_oci.xlsx','//dades1/dades1/ayun/Residuos/LIMPIEZA/isabel/00 OCI/','/prod/01.fichOrig/VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL/','/prod/02.fichBackup/VLCI_ETL_RESIDUOS_IND_PLANTILLA_MENSUAL/',NULL,'residuos_oci.xlsx','S','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2020-11-10 08:56:11','Ticket-45251','2021-08-31 11:24:58.018016',NULL,NULL),
			('VLCI_ETL_SERTIC_SEDE_SWEB1','FTP','FTP Sertic','access.log-*','//soes-aux1/FTPVOL/LogsWeb/slogserver1/','/prod/01.fichOrig/VLCi_Sertic_SedeSWeb1/','/prod/02.fichBackup/VLCi_Sertic_SedeSWeb1/',NULL,'access.log-*','N','xz',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2020-05-14 08:52:26','Ticket-39365','2021-08-31 11:24:58.018016',NULL,118),
			('VLCI_ETL_JARDINERIA_IND_INVENTARIO','FTP','FTP Sertic','JARDINES*','//DADES2/DADES2/ayun/JARDINERIA/JardineriaVLCi','/prod/01.fichOrig/Jardines_Inventario/','/prod/02.fichBackup/Jardines_Inventario/',NULL,'JARDINES','N','mdb',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2019-05-07 09:23:02','Ivan 31459','2021-08-31 11:24:58.018016','Ivan 31459',86),
			('VLCI_ETL_COORDVIAPUB_IND_INCIDENCIAS_SIGO','FTP','FTP Sertic','INCIDENCIAS*','//dades1/DADES1/ayun/OCOVAL/','/prod/01.fichOrig/OCOVAL/','/prod/02.fichBackup/OCOVAL/',NULL,'INCIDENCIAS.xlsx','S','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2018-11-27 08:54:44','Toni 28621','2021-08-31 11:24:58.018016','Toni 28621',85),
			('ETL_Trafico_aparcamientos','FTP','FTP Sertic','Consulta aparcamientos por tipo y plazas.xls','//dades2/DADES2/ayun/TRAFICO/Plataforma VLCi','/prod/01.fichOrig/VLCi_Trafico_Aparcamientos/','/prod/02.fichBackup/VLCi_Trafico_Aparcamientos/',NULL,'Consulta aparcamientos por tipo y plazas.xls','N','xls',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2017-10-30 09:35:39','ETL Loader','2021-08-31 11:24:58.018016',NULL,5),
			('ETL VLCi IBI','FTP','FTP Sertic','Informacion_tributaria_IBI_*','//DADES2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi','/prod/01.fichOrig/VLCi_IBI/','/prod/02.fichBackup/VLCi_IBI/',NULL,'Informacion_tributaria_IBI_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2018-01-09 09:27:40','ETL Loader','2021-08-31 11:24:58.018016',NULL,21);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('MESAS SILLAS','FTP','FTP Sertic','MESAS_Y_SILLAS_*','//dades2/DADES2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi','/prod/01.fichOrig/VLCi_MESAS_SILLAS/','/prod/02.fichBackup/VLCi_MESAS_SILLAS/',NULL,'MESAS_Y_SILLAS_yyyy','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2018-01-23 08:42:07',NULL,'2021-08-31 11:24:58.018016',NULL,22),
			('VLCI_VALENCIA_ETL_IVTM','FTP','FTP Sertic','Informacion_tributaria_IVTM*','//dades2/dades2/ayun/TESORERIA/GESTION TRIBUTARIA/Int. Inf. Tributaria/Plataforma VLCi','/prod/01.fichOrig/VLCI_VALENCIA_ETL_IVTM/','/prod/02.fichBackup/VLCI_VALENCIA_ETL_IVTM/',NULL,'Informacion_tributaria_IVTM*','N','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2018-01-23 08:42:38','ETL Loader','2021-08-31 11:24:58.018016',NULL,23),
			('ETL_VLCi_CDMGE_PMP_ENTIDAD','FTP','FTP Sertic','Evolucion_PMP.xlsx','//dades2/dades2/ayun/TESORERIA/Jefatura de Servicio/PMP/Smartcity','/prod/01.fichOrig/VLCi_CDMGE_PMP_ENTIDAD/','/prod/02.fichBackup/VLCi_CDMGE_PMP_ENTIDAD/',NULL,'Evolucion_PMP.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2017-11-13 09:45:04','ETL Loader','2021-07-20 08:22:57',NULL,8),
			('ETL VLCi CDMGE PMP','FTP','FTP Sertic','PMP_Resumen_Uds_Administrativas_*.xlsx','//dades2/dades2/ayun/TESORERIA/Jefatura de Servicio/PMP/Smartcity','/prod/01.fichOrig/VLCi_CDMGE_PMP/','/prod/02.fichBackup/VLCi_CDMGE_PMP/',NULL,'PMP_Resumen_Uds_Administrativas_yyyy.xlsx','N','xlsx',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2017-11-27 09:01:24','ETL Loader','2021-07-20 08:22:46',NULL,11);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149',NULL,'csv','Indicador IC49.csv','WITH 
		SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,
			T55787.DT as c2,
			T17978.DTBA as c3,
			T7594.ANO as c4,
			T6789.CODIGO as c5,
			T6789.DENOMINACION as c6
		from 
			V_LU_DIVMUN_DT T55787,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_DIVMUN_DTSC T22801,
			FACT_IV_CAT_VIV T103797
		where  ( T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T17978.DTBA = T103797.DTBA and T22801.DTSC = T103797.DTSC and T6789.DENOMINACION = ''Castellano''  and T55787.DT = T103797.DT  ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT),
		SAWITH1 AS (select sum(T103843.NUMVIV) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T7594.ANO as c4,
			T6789.CODIGO as c5,
			T6789.DENOMINACION as c6
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_VIV_AGRANYDTBA T103843
		where  ( T7594.ANO = T103843.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103843.DTBA  ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA),
		SAWITH2 AS (select distinct T103896.NUMVIV1800 as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T7594.ANO as c4,
			T6789.CODIGO as c5,
			T6789.DENOMINACION as c6
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_VIV_AGRVCDTBA T103896
		where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano''  and T17978.DTBA = T103896.DTBA ) ),
		SAWITH3 AS (select 0 as c1,
			case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 when D3.c2 is not null then D3.c2 end  as c2,
			case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 when D3.c3 is not null then D3.c3 end  as c3,
			case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 when D3.c4 is not null then D3.c4 end  as c4,
			D1.c1 as c5,
			D2.c1 as c6,
			D3.c1 as c7,
			case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 when D3.c5 is not null then D3.c5 end  as c8
		from 
			(
				SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ) full outer join SAWITH2 D3 On D3.c2 = case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  and D3.c3 = case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  and D3.c4 = case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  and D3.c5 = case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  and  SYS_OP_MAP_NONNULL(D3.c6) = SYS_OP_MAP_NONNULL(case  when D1.c6 is not null then D1.c6 when D2.c6 is not null then D2.c6 end ) ),
		SAWITH4 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH5 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH6 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11
				from 
					(
							SAWITH3 D1 inner join SAWITH4 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH5 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3
			) D1
		where  ( D1.c11 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9
		from 
			SAWITH6 D1
		order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2019-05-28 08:54:11','Lozano-32016','2021-08-31 11:24:58.018016','Lozano-32016',103),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A8',NULL,'csv','Indicador IC110A8.csv','WITH SAWITH0 AS (select count(T18685.IDPADRON) as c1,      T17978.DT as c2,      T17978.DTBA as c3,      T56143.CODPAIS as c4,      case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  as c5,      T56143.DPAIS as c6,      T6789.CODIGO as c7,      T18685.ANYO as c33 from       V_LU_GEO_PAIS_NACION T56143,      V_LU_DIVMUN_DTBA T17978,      V_LU_TIEMPO_ANO T7594,      LU_IDIOMA T6789,      LU_CP_IDI T21622,      FACT_II_PAD_PADRON T18685 where  ( T6789.CODIGO = T56143.IDIOMA and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T17978.DTBA = T18685.DISBAR and T18685.CODPOS = T21622.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18685.PAINACDAD = T56143.CODPAIS and T18685.PAINACDAD <> 108 and T56143.CODPAIS <> 108)  group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T56143.CODPAIS, T56143.DPAIS, case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end, T18685.ANYO ) select 0 ||'';''||      D1.c2 ||'';''||      D1.c3 ||'';''||      D1.c4 ||'';''||      D1.c1 ||'';''||      D1.c6 ||'';''||      case  when row_number() OVER (PARTITION BY D1.c3 ORDER BY D1.c1 DESC) < 7 then row_number() OVER (PARTITION BY D1.c3 ORDER BY D1.c1 DESC) else 10 end  ||'';''||      D1.c7 ||'';''||      D1.c33 as c33 from       SAWITH0 D1  order by D1.c2, D1.c7','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:34:47.574916',NULL,63),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC015A1',NULL,'csv','Indicador IC015A1.csv','with pob as (
		select SUM(T21230.NUMPERSONAS) personas, T7594.ANO ano
		from 
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_SEXO_IDI T10746,
			FACT_II_PAD_AGREGADO T21230
		where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Valenciano'' and T10746.CODIGO = T21230.SEXO ) 
		GROUP BY T7594.ANO
		),
		cons as (
		select sum(T60833.VALOR) consumo, T7594.ANO ano
		from 
			LU_VIII_IBER_TIPOENERGIA_IDI T61176,
			LU_VIII_IBER_TIPCONS_IDI T33126,
			LU_VIII_IBER_INDICADOR_IDI T33120,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_GEO_MUNICIPIO T6910,
			FACT_VIII_IBER_DATOSANUAL T60833
		where  ( T6789.CODIGO = T61176.IDIOMA and T6789.CODIGO = T33120.IDIOMA and T33120.CODIGO = T60833.INDICADOR and T7594.ANO = T60833.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T60833.MUNICIPIO and T6789.CODIGO = T33126.IDIOMA and T33126.CODIGO = T60833.TIPCONS and T6789.DENOMINACION = ''Valenciano'' and T33120.CODIGO = 1 and T33126.CODIGO = 1 and T60833.TIPENERGIA = T61176.CODIGO and T61176.CODIGO = 1 ) 
		GROUP BY  T7594.ANO 
		)
		select ((d1.consumo/p1.personas) / 12.0)*1000 ||'';''|| p1.ano ano
		from pob p1, cons d1
		where p1.ano = d1.ano','consumo_anual,ano','N','sftp',NULL,NULL,'2018-06-12 08:52:17','Lozano','2021-08-31 11:24:58.018016',NULL,37),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC019A1',NULL,'csv','Indicador IC019A1.csv','with pob as (

		select SUM(T21230.NUMPERSONAS) personas, T7594.ANO ano
		from 
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_SEXO_IDI T10746,
			FACT_II_PAD_AGREGADO T21230
		where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Valenciano'' and T10746.CODIGO = T21230.SEXO )
		GROUP BY T7594.ANO
		),

		cons as (select sum(T60833.VALOR) consumo, T7594.ANO ano
		from 
			LU_VIII_IBER_TIPOENERGIA_IDI T61176,
			LU_VIII_IBER_TIPCONS_IDI T33126,
			LU_VIII_IBER_INDICADOR_IDI T33120,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_GEO_MUNICIPIO T6910,
			FACT_VIII_IBER_DATOSANUAL T60833
		where  ( T6789.CODIGO = T61176.IDIOMA and T6789.CODIGO = T33120.IDIOMA and T33120.CODIGO = T60833.INDICADOR and T7594.ANO = T60833.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T60833.MUNICIPIO and T6789.CODIGO = T33126.IDIOMA and T33126.CODIGO = T60833.TIPCONS and T6789.DENOMINACION = ''Valenciano''  and T33120.CODIGO = 1 and T33126.CODIGO <> 9 and T60833.TIPENERGIA = T61176.CODIGO and T61176.CODIGO = 1 ) 
		GROUP BY  T7594.ANO 
		)
		select ((d1.consumo/p1.personas) / 365.0)*1000 ||'';''|| p1.ano ano
		from pob p1, cons d1
		where p1.ano = d1.ano','consumo_anual,ano','N','sftp',NULL,NULL,'2018-06-12 08:52:17','Lozano','2021-08-31 11:24:58.018016',NULL,38),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC001A1',NULL,'csv','Indicador IC001A1.csv','WITH 
		SAWITH0 AS (select sum(T10607.VALOR) as c1,
			T10686.CODIGO as c2,
			T10686.DENOMINACION as c3,
			T10783.ANO as c4,
			T10783.TRIMESTRE as c5,
			T10783.ANOTRIMESTRE as c6,
			T6789.CODIGO as c7
		from 
			LU_VII_EPAV_EDADAGR2_IDI T10680,
			LU_VII_EPAV_INDICADOR_IDI T10686,
			LU_VII_EPAV_METODO_IDI T10692,
			V_LU_TIEMPO_ANOTRIMESTRE T10783,
			LU_IDIOMA T6789,
			LU_SEXO_IDI T10746,
			FACT_VII_EPAV_SEXOAGREDAD2 T10607
		where  ( T6789.CODIGO = T10680.IDIOMA and T6789.CODIGO = T10686.IDIOMA and T10607.EDADAGR2 = T10680.CODIGO and T6789.CODIGO = T10692.IDIOMA and T10607.INDICADOR = T10686.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10607.METODO = T10692.CODIGO and T10607.ANO = T10783.ANO and T10607.SEXO = T10746.CODIGO and T6789.DENOMINACION = ''Castellano'' and T10607.EDADAGR2 = 9 and T10607.INDICADOR = 26 and T10607.METODO = 4 and T10607.TRIMESTRE = T10783.TRIMESTRE  and T10607.SEXO = 9  and T10680.CODIGO = 9 and T10686.CODIGO = 26 and T10692.CODIGO = 4 and T10746.CODIGO = 9  ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T10686.CODIGO, T10686.DENOMINACION, T10783.ANO, T10783.ANOTRIMESTRE, T10783.TRIMESTRE),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c5 as c6,
			D1.c1 as c7,
			D1.c6 as c8,
			D1.c7 as c9
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58019.DENOMINACION as c1,
			T58019.CODIGO as c2,
			T58019.IDIOMA as c3
		from 
			LU_TRIMESTRE_IDI T58019 /* LU_TRIMESTRE_LOOKUP */ ),
		SAWITH3 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D1.c3 as c3,
					D1.c4 as c4,
					D2.c1 as c5,
					D1.c5 as c6,
					D1.c6 as c7,
					D1.c7 as c8,
					D1.c8 as c9,
					D1.c9 as c10,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c8, D1.c9, D2.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c8 ASC, D1.c9 ASC, D2.c1 ASC) as c11
				from 
					SAWITH1 D1 inner join SAWITH2 D2 On D1.c5 = D2.c2 and D1.c9 = D2.c3
			) D1
		where  ( D1.c11 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 as c8 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8
		from 
			SAWITH3 D1
		order by c1, c4, c6, c7, c2, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2018-06-12 08:52:17','Lozano','2021-08-31 11:24:58.018016',NULL,39),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC101A1',NULL,'csv','Indicador IC101A1.csv','WITH 
		SAWITH0 AS (select sum(T13116.VALOR) as c1,
			T73182.CODIGO as c2,
			T73182.DENOMINACION as c3,
			T13200.CODIGO as c4,
			T13200.DENOMINACION as c5,
			T13212.CODIGO as c6,
			T6789.DENOMINACION as c7,
			T7595.ANO as c8,
			T7595.MES as c9,
			T7595.ANOMES as c10,
			T6789.CODIGO as c11,
			T7595.ANO as c33
		from 
			LU_VI_EOH_TIPEST_IDI T73182,
			LU_VI_EOH_RESIDENCIA_IDI T13212,
			LU_VI_EOH_INDICADORDEM_IDI T13200,
			V_LU_TIEMPO_ANOMES T7595,
			LU_IDIOMA T6789,
			V_LU_GEO_MUNICIPIO T6910,
			FACT_VI_EOH_DEMTURMUNI T13116
		where  ( T6789.CODIGO = T73182.IDIOMA and T6789.CODIGO = T13200.IDIOMA and T13116.INDICADOR = T13200.CODIGO and T6789.CODIGO = T13212.IDIOMA and T13116.RESIDENCIA = T13212.CODIGO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T13116.MUNICIPIO and T7595.ANO = T13116.ANO and T7595.MES = T13116.MES and T6789.DENOMINACION = ''Castellano'' and T6910.CODMUNICIPIO = 46250 and T13116.INDICADOR = 2 and T13116.MUNICIPIO = 46250 and T13116.RESIDENCIA = 9 and T13116.TIPEST = T73182.CODIGO and T13200.CODIGO = 2 and T13212.CODIGO = 9 and T13116.INDICADOR <> 3 and T13200.CODIGO <> 3 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7595.ANO, T7595.MES, T7595.ANOMES, T13200.CODIGO, T13200.DENOMINACION, T13212.CODIGO, T73182.CODIGO, T73182.DENOMINACION),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c9 as c10,
			D1.c1 as c11,
			D1.c10 as c12,
			D1.c11 as c13,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T57997.DENOMINACION as c1,
			T57997.CODIGO as c2,
			T57997.IDIOMA as c3
		from 
			LU_MES_IDI T57997 /* LU_MES_LOOKUP */ ),
		SAWITH3 AS (select D1.c1 as c1,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c12 as c12,
			D1.c13 as c13,
			D1.c14 as c14,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c4 as c4,
					D1.c5 as c5,
					D1.c6 as c6,
					D1.c7 as c7,
					D1.c8 as c8,
					D2.c1 as c9,
					D1.c9 as c10,
					D1.c10 as c11,
					D1.c11 as c12,
					D1.c12 as c13,
					D1.c13 as c14,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c9, D1.c10, D1.c12, D1.c13, D2.c1,D1.c33 ORDER BY D1.c2 ASC,  D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c9 ASC, D1.c10 ASC, D1.c12 ASC, D1.c13 ASC, D2.c1 ASC) as c15
				from 
					SAWITH1 D1 inner join SAWITH2 D2 On D1.c9 = D2.c2 and D1.c13 = D2.c3
			) D1
		where  ( D1.c15 = 1 ) )
		select D1.c1 ||'';''||  D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			sum(D1.c12) as c12,
			D1.c33 as c33
		from 
			SAWITH3 D1
		group by c1,c4,c5,c6,c7,c8,c9,c10,c11,c33
		order by c1, c7, c8, c11, c10, c4, c6,  c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:17','Lozano','2021-08-31 11:24:58.018016',NULL,40);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152',NULL,'csv','Indicador IC152.csv','WITH 
		SAWITH0 AS (select count(T101251.ID) as c1,
			T101277.CODEPI as c2,
			T101277.DEPI as c3,
			T17978.DT as c4,
			T17978.DTBA as c5,
			case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,
			case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,
			T6789.CODIGO as c8,
			T101251.ANO as c33
		from 
			V_LU_XI_PATRIM_RELEPIGRAFE T101277,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_GEO_MUNICIPIO T6910,
			FACT_XI_PATRIM_EDIFICIOS T101251
		where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c1 as c8,
			D1.c8 as c9,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D1.c3 as c3,
					D2.c1 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c9 as c11,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3
			) D1
		where  ( D1.c12 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';'' || D1.c3||'';'' || D1.c4||'';'' || D1.c5||'';'' || D1.c6||'';'' || D1.c7||'';''|| D1.c8||'';''|| D1.c9||'';''|| D1.c10||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,41),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A1',NULL,'csv','Indicador IC152A1.csv','WITH 
		SAWITH0 AS (select sum(T101251.SUPPARCELA) as c1,
			T101277.CODEPI as c2,
			T101277.DEPI as c3,
			T17978.DT as c4,
			T17978.DTBA as c5,
			case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,
			case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,
			T6789.CODIGO as c8,
			T101251.ANO as c33
		from 
			V_LU_XI_PATRIM_RELEPIGRAFE T101277,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_GEO_MUNICIPIO T6910,
			FACT_XI_PATRIM_EDIFICIOS T101251
		where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c1 as c8,
			D1.c8 as c9,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D1.c3 as c3,
					D2.c1 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c9 as c11,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3
			) D1
		where  ( D1.c12 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';'' || D1.c3||'';'' || D1.c4||'';'' || D1.c5||'';'' || D1.c6||'';'' || D1.c7||'';''|| D1.c8||'';''|| D1.c9||'';''|| D1.c10||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,42),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A2',NULL,'csv','Indicador IC152A2.csv','WITH 
		SAWITH0 AS (select sum(T101251.SUPCONSTR) as c1,
			T101277.CODEPI as c2,
			T101277.DEPI as c3,
			T17978.DT as c4,
			T17978.DTBA as c5,
			case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,
			case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,
			T6789.CODIGO as c8,
			T101251.ANO as c33
		from 
			V_LU_XI_PATRIM_RELEPIGRAFE T101277,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_GEO_MUNICIPIO T6910,
			FACT_XI_PATRIM_EDIFICIOS T101251
		where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end , T101251.ANO),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c1 as c8,
			D1.c8 as c9,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D1.c3 as c3,
					D2.c1 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c9 as c11,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3
			) D1
		where  ( D1.c12 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select   	 
			D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,43),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC152A3',NULL,'csv','Indicador IC152A3.csv','WITH 
		SAWITH0 AS (select sum(T101251.VALORINVENT) as c1,
			T101277.CODEPI as c2,
			T101277.DEPI as c3,
			T17978.DT as c4,
			T17978.DTBA as c5,
			case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end  as c6,
			case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end  as c7,
			T6789.CODIGO as c8,
			T101251.ANO as c33
		from 
			V_LU_XI_PATRIM_RELEPIGRAFE T101277,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_GEO_MUNICIPIO T6910,
			FACT_XI_PATRIM_EDIFICIOS T101251
		where  ( T6789.CODIGO = T101277.IDIOMA and T7594.ANO = T101251.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T101251.MUNICIPIO and T6789.DENOMINACION = ''Castellano''  and T17978.DT = T101251.DISTRITO and T17978.DTBA = T101251.DTBA and T101251.RELACION = T101277.CODREL ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T101277.CODEPI, T101277.DEPI, case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then ''9Z9'' else T101277.CODREL end , case  when not T101277.CODREL in (''1CM'', ''1E3'', ''1E5'', ''1E8'', ''1S1'') then case  when T6789.DENOMINACION = ''Castellano'' then ''Otros'' else ''Altres'' end  else T101277.DREL end, T101251.ANO ),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c1 as c8,
			D1.c8 as c9,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D1.c3 as c3,
					D2.c1 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c9 as c11,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c4 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c5 = D3.c2 and D1.c9 = D3.c3
			) D1
		where  ( D1.c12 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c6, c4, c7, c5, c2, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,44),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A1',NULL,'csv','Indicador IC153A1.csv','WITH 
		SAWITH0 AS (select count(T16529.ORDNUM) as c1,
			T55787.DT as c2,
			T17978.DTBA as c3,
			T16638.CODIGO as c4,
			T16638.DENOMINACION as c5,
			T16701.CODIGO as c6,
			T16701.DENOMINACION as c7,
			T16722.CODIGO as c8,
			T16722.DENOMINACION as c9,
			T6789.CODIGO as c10,
			T7594.ANO as c33
		from 
			V_LU_DIVMUN_DT T55787,
			V_LU_DIVMUN_DTBA T17978,
			LU_III_MAT_TIPOFIN_IDI T16722,
			LU_III_MAT_POTTU2_IDI T16701,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_III_MAT_ANYMAT_IDI T16638,
			FACT_III_MAT_MATRICULA T16529
		where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16701.IDIOMA and T16529.DTBA = T17978.DTBA and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.POT_TU2 = T16701.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16722.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T16638.CODIGO, T16638.DENOMINACION, T16701.CODIGO, T16701.DENOMINACION, T16722.CODIGO, T16722.DENOMINACION, T17978.DTBA, T55787.DT,T7594.ANO),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c1 as c10,
			D1.c10 as c12,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D2.c1 as c2,
			D3.c1 as c3,
			D1.c2 as c4,
			D1.c3 as c5,
			D1.c4 as c6,
			D1.c5 as c7,
			D1.c6 as c8,
			D1.c7 as c9,
			D1.c8 as c10,
			D1.c9 as c11,
			D1.c10 as c12,
			D1.c12 as c14,
			D1.c33 as c33,
			ROW_NUMBER() OVER (PARTITION BY D1.c4, D1.c5, D1.c6, D1.c7, D1.c3, D1.c2, D2.c1, D3.c1, D1.c8, D1.c9,D1.c33 ORDER BY D1.c4 DESC, D1.c5 DESC, D1.c6 DESC, D1.c7 DESC, D1.c3 DESC, D1.c2 DESC, D2.c1 DESC, D3.c1 DESC, D1.c8 DESC, D1.c9 DESC) as c15
		from 
			(
				SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c12 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c12 = D3.c3),
		SAWITH5 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c12 as c12,
			D1.c13 as c13,
			D1.c14 as c14,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D1.c3 as c3,
					D1.c4 as c4,
					D1.c5 as c5,
					D1.c6 as c6,
					D1.c7 as c7,
					D1.c8 as c8,
					D1.c9 as c9,
					D1.c10 as c10,
					D1.c11 as c11,
					D1.c12 as c12,
					sum(case D1.c15 when 1 then D1.c12 else NULL end ) over (partition by D1.c6, D1.c7, D1.c8, D1.c9, D1.c5, D1.c4, D1.c2, D1.c3,D1.c2,D1.c33)  as c13,
					D1.c14 as c14,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c9, D1.c10, D1.c11, D1.c14, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c9 ASC, D1.c10 ASC, D1.c11 ASC, D1.c14 ASC) as c15
				from 
					SAWITH4 D1
			) D1
		where  ( D1.c15 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4  ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c13 ||'';''|| D1.c33 as c33 from ( select 
			D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c12 as c12,
			D1.c13 as c13,
			D1.c33 as c33
		from 
			SAWITH5 D1
		order by c1, c4, c2, c5, c3, c6, c7, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,45),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A2',NULL,'csv','Indicador IC153A2.csv','WITH 
		SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,
			T17978.DTBA as c2,
			T6789.CODIGO as c3,
			T6789.DENOMINACION as c4,
			T16529.ANO as c33
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_III_MAT_ANYMAT_IDI T16638,
			FACT_III_MAT_MATRICULA T16529
		where  ( T6789.CODIGO = T16638.IDIOMA and T7594.ANO = T16529.ANO and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Valenciano''  and T16529.DTBA = T17978.DTBA ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T16529.ANO),
		SAWITH1 AS (select sum(T21230.NUMPERSONAS) as c1,
			T17978.DTBA as c2,
			T6789.CODIGO as c3,
			T6789.DENOMINACION as c4
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANOMESDIA T24742,
			LU_IDIOMA T6789,
			LU_SEXO_IDI T10746,
			FACT_II_PAD_AGREGADO T21230
		where  ( T17978.DT = T21230.DIS and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T21230.SEXO and T6789.DENOMINACION = ''Valenciano'' and T17978.DTBA = T21230.DISBAR and T21230.ANYO = 2016 and T21230.ANYO = T24742.ANO and T21230.DIA = T24742.DIADELMES and T21230.MES = T24742.MES and T24742.ANO = 2016 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA),
		SAWITH2 AS (select 0 as c1,
			case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,
			cast(D1.c1 * 100 / nullif( D2.c1, 0) as  DOUBLE PRECISION  ) as c3,
			case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c4,
			D1.c33 as c33
		from 
			SAWITH0 D1 full outer join SAWITH1 D2 On D1.c3 = D2.c3 and D1.c2 = D2.c2 and  SYS_OP_MAP_NONNULL(D1.c4) = SYS_OP_MAP_NONNULL(D2.c4) ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D1.c2 as c3,
					D1.c3 as c4,
					D1.c4 as c5,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c4, D2.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c4 ASC, D2.c1 ASC) as c6
				from 
					SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c4 = D2.c3
			) D1
		where  ( D1.c6 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c3, c2 ) D1 where rownum <= 65001
		','c1,c2,c3,c4,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,46),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A3',NULL,'csv','Indicador IC153A3.csv','WITH 
		SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,
			T55787.DT as c2,
			T17978.DTBA as c3,
			T6789.CODIGO as c4,
			T7594.ANO as c33
		from 
			V_LU_DIVMUN_DT T55787,
			V_LU_DIVMUN_DTBA T17978,
			LU_III_MAT_TIPOFIN_IDI T16722,
			LU_III_MAT_TIPODNI_IDI T16713,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_III_MAT_ANYMAT_IDI T16638,
			FACT_III_MAT_MATRICULA T16529
		where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16713.IDIOMA and T16529.DTBA = T17978.DTBA and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.TIPODNI = T16713.CODIGO and T6789.DENOMINACION = ''Castellano''  and T16529.TIPODNI = 1 and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16713.CODIGO = 1 and T16722.CODIGO = 1 and T16529.ANYMAT <= 2015 - 11 and T16638.CODIGO <= 2015 - 11 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T55787.DT, T7594.ANO),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c1 as c4,
			D1.c4 as c5,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3
			) D1
		where  ( D1.c8 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,47),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A4',NULL,'csv','Indicador IC153A4.csv','WITH 
		SAWITH0 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,
			T55787.DT as c2,
			T17978.DTBA as c3,
			T6789.CODIGO as c4,
			T7594.ANO as c33
		from 
			V_LU_DIVMUN_DT T55787,
			V_LU_DIVMUN_DTBA T17978,
			LU_III_MAT_TIPOFIN_IDI T16722,
			LU_III_MAT_TIPODNI_IDI T16713,
			LU_III_MAT_POTTU2_IDI T16701,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_III_MAT_ANYMAT_IDI T16638,
			FACT_III_MAT_MATRICULA T16529
		where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16713.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.CODIGO = T16701.IDIOMA and T16529.POT_TU2 = T16701.CODIGO and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.TIPODNI = T16713.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.TIPODNI = 1 and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 1 and T16713.CODIGO = 1 and T16722.CODIGO = 1 and T16529.POT_TU2 >= 4 and T16701.CODIGO >= 4 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DTBA, T55787.DT, T7594.ANO),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c1 as c4,
			D1.c4 as c5,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3
			) D1
		where  ( D1.c8 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,48),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A6',NULL,'csv','Indicador IC153A6.csv','WITH 
		SAWITH0 AS (select avg(T58565.EDADM) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T6789.CODIGO as c4,
			T58565.AÑO as c33
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_III_MAT_EDADM_DTBA T58565
		where  ( T7594.ANO = T58565.AÑO and T6789.DENOMINACION = ''Castellano''  and T17978.DTBA = T58565.DTBA  ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T58565.AÑO),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c1 as c4,
			D1.c4 as c5,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c5 = D3.c3
			) D1
		where  ( D1.c8 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,49),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153',NULL,'csv','Indicador IC153.csv','WITH 
		SAWITH0 AS (select count(T16529.ORDNUM) as c1,
			T55787.DT as c2,
			T17978.DTBA as c3,
			T16722.CODIGO as c4,
			T16722.DENOMINACION as c5,
			T6789.CODIGO as c6,
			T16529.ANO as c33
		from 
			V_LU_DIVMUN_DT T55787,
			V_LU_DIVMUN_DTBA T17978,
			LU_III_MAT_TIPOFIN_IDI T16722,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_III_MAT_ANYMAT_IDI T16638,
			FACT_III_MAT_MATRICULA T16529
		where  ( T16529.DT = T55787.DT and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.DENOMINACION = ''Castellano''  and T16529.TIPOFIN = T16722.CODIGO ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T16722.CODIGO, T16722.DENOMINACION, T17978.DTBA, T55787.DT, T16529.ANO),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 as c6,
			D1.c6 as c7,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3
			) D1
		where  ( D1.c10 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select 
			D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c4, c2, c5, c3, c6, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,50);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC153A5',NULL,'csv','Indicador IC153A5.csv','WITH 
		SAWITH0 AS (select count(T16529.ORDNUM) as c1,
			T55787.DT as c2,
			T17978.DTBA as c3,
			T16674.CODIGO as c4,
			T16674.DENOMINACION as c5,
			T6789.CODIGO as c6,
			T16529.ANO as c33
		from 
			V_LU_DIVMUN_DT T55787,
			V_LU_DIVMUN_DTBA T17978,
			LU_III_MAT_TIPOFIN_IDI T16722,
			LU_III_MAT_CILMO2_IDI T16674,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_III_MAT_ANYMAT_IDI T16638,
			FACT_III_MAT_MATRICULA T16529
		where  ( T16529.DT = T55787.DT and T6789.CODIGO = T16674.IDIOMA and T16529.CIL_MO2 = T16674.CODIGO and T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.CODIGO = T16722.IDIOMA and T16529.DTBA = T17978.DTBA and T6789.DENOMINACION = ''Castellano''  and T16529.TIPOFIN = T16722.CODIGO and T16529.TIPOFIN = 6 and T16722.CODIGO = 6 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T16674.CODIGO, T16674.DENOMINACION, T17978.DTBA, T55787.DT, T16529.ANO),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 as c6,
			D1.c6 as c7,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3
			) D1
		where  ( D1.c10 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select 
			D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c4, c2, c5, c3, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2018-06-12 08:52:47','Lozano','2021-08-31 11:24:58.018016',NULL,51),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A1',NULL,'csv','Indicador IC006A1.csv','WITH 
		SAWITH0 AS (select count(T55155.NUMREF) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T54922.CODIGO as c4,
			T54922.DENOMINACION as c5,
			T6789.CODIGO as c6,
			T55155.ANO as c33
		from 
			LU_VI_IAE_GRANGR_IDI T54922,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANOMESDIA T24742,
			LU_IDIOMA T6789,
			V_LU_DIVMUN_DTSC T22801,
			FACT_VI_IAE_IAE T55155
		where  ( T6789.CODIGO = T54922.IDIOMA and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T6789.DENOMINACION = ''Castellano'' and T54922.CODIGO = T55155.GGRUP and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T54922.CODIGO, T54922.DENOMINACION, T55155.ANO),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 as c6,
			D1.c6 as c7,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3
			) D1
		where  ( D1.c10 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c4, c5, c2, c3, c6, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:24:58.018016',NULL,52),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC146A1',NULL,'csv','Indicador IC146A1.csv','select T6789.DENOMINACION ||'';''||
			T8148.CODIGO ||'';''||
			T8160.CODIGO ||'';''||
			T8160.DENOMINACION ||'';''||
			T7595.ANO ||'';''||
			T7595.MES ||'';''||
			T6789.CODIGO ||'';''||
			replace(T8109.VALOR , '','' , ''.'') as c9
		from 
			LU_I_MET_ESTMETEO T8148,
			V_LU_TIEMPO_ANOMES T7595,
			LU_IDIOMA T6789,
			LU_I_MET_INDICADOR_IDI T8160,
			FACT_I_MET_METEOANOMES T8109
		where  ( T6789.CODIGO = T8160.IDIOMA and T8109.ESTMETEO = T8148.CODIGO and T7595.ANO = T8109.ANO and T7595.MES = T8109.MES and T6789.DENOMINACION = ''Castellano''  and T8109.ESTMETEO = 4 and T8109.INDICADOR = T8160.CODIGO and T8148.CODIGO = 4 and (T8109.INDICADOR in (28)) and (T8160.CODIGO in (28)) )
		order by T7595.ANO, T7595.MES','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2018-06-12 08:57:20','Lozano','2021-08-31 11:24:58.018016',NULL,65),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A2',NULL,'csv','Indicador IC006A2.csv','WITH 
		SAWITH0 AS (select count(T55155.NUMREF) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T55112.DSECCDIV as c4,
			T55112.SECCDIV as c5,
			T6789.CODIGO as c6,
			T55155.ANO as c33
		from 
			V_LU_VI_IAE_RELSECCIONES T55112,
			LU_VI_IAE_GRANGR_IDI T54922,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANOMESDIA T24742,
			LU_IDIOMA T6789,
			V_LU_DIVMUN_DTSC T22801,
			FACT_VI_IAE_IAE T55155
		where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 2 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 2 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO),
		SAWITH1 AS (select count(T55155.NUMREF) as c1
		from 
			LU_VI_IAE_GRANGR_IDI T54922,
			V_LU_TIEMPO_ANOMESDIA T24742,
			LU_IDIOMA T6789,
			V_LU_DIVMUN_DTSC T22801,
			FACT_VI_IAE_IAE T55155
		where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 2  and T55155.GGRUP = 2 ) ),
		SAWITH2 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 as c6,
			D2.c1 as c7,
			D1.c6 as c8,
			D1.c33 as c33
		from 
			SAWITH0 D1,
			SAWITH1 D2),
		SAWITH3 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH4 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH5 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1, D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11
				from 
					(
							SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3
			) D1
		where  ( D1.c11 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c33 as c33
		from 
			SAWITH5 D1
		order by c1, c7, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:24:58.018016',NULL,53),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A3',NULL,'csv','Indicador IC006A3.csv','WITH 
		SAWITH0 AS (select count(T55155.NUMREF) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T55112.DSECCDIV as c4,
			T55112.SECCDIV as c5,
			T6789.CODIGO as c6,
			T55155.ANO as c33
		from 
			V_LU_VI_IAE_RELSECCIONES T55112,
			LU_VI_IAE_GRANGR_IDI T54922,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANOMESDIA T24742,
			LU_IDIOMA T6789,
			V_LU_DIVMUN_DTSC T22801,
			FACT_VI_IAE_IAE T55155
		where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 4 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 4 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO),
		SAWITH1 AS (select count(T55155.NUMREF) as c1
		from 
			LU_VI_IAE_GRANGR_IDI T54922,
			V_LU_TIEMPO_ANOMESDIA T24742,
			LU_IDIOMA T6789,
			V_LU_DIVMUN_DTSC T22801,
			FACT_VI_IAE_IAE T55155
		where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 4  and T55155.GGRUP = 4 ) ),
		SAWITH2 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 as c6,
			D2.c1 as c7,
			D1.c6 as c8,
			D1.c33 as c33
		from 
			SAWITH0 D1,
			SAWITH1 D2),
		SAWITH3 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH4 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH5 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11
				from 
					(
							SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3
			) D1
		where  ( D1.c11 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c33 as c33
		from 
			SAWITH5 D1
		order by c1, c7, c6, c4, c2, c3, c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:24:58.018016',NULL,54),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC006A4',NULL,'csv','Indicador IC006A4.csv','WITH 
		SAWITH0 AS (select count(T55155.NUMREF) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T55112.DSECCDIV as c4,
			T55112.SECCDIV as c5,
			T6789.CODIGO as c6,
			T55155.ANO as c33
		from 
			V_LU_VI_IAE_RELSECCIONES T55112,
			LU_VI_IAE_GRANGR_IDI T54922,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANOMESDIA T24742,
			LU_IDIOMA T6789,
			V_LU_DIVMUN_DTSC T22801,
			FACT_VI_IAE_IAE T55155
		where  ( T6789.CODIGO = T55112.IDIOMA and T55112.SECCAGR = T55155.SECAGRUP and T55112.SECCDIV = T55155.SECDIVIS and T55112.SECCGR = T55155.SECGRUPO and T17978.DTBA = T55155.DTBA and T6789.CODIGO = T54922.IDIOMA and T54922.CODIGO = T55155.GGRUP and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = 5 and T55112.SECCION = T55155.SECCION  and T55155.GGRUP = 5 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T55112.DSECCDIV, T55112.SECCDIV, T55155.ANO),
		SAWITH1 AS (select count(T55155.NUMREF) as c1
		from 
			LU_VI_IAE_GRANGR_IDI T54922,
			V_LU_TIEMPO_ANOMESDIA T24742,
			LU_IDIOMA T6789,
			V_LU_DIVMUN_DTSC T22801,
			FACT_VI_IAE_IAE T55155
		where  ( T6789.CODIGO = T54922.IDIOMA and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T55155.DTSC and T24742.ANO = T55155.ANO and T24742.DIADELMES = T55155.DIA and T24742.MES = T55155.MES and T6789.DENOMINACION = ''Castellano''  and T54922.CODIGO = T55155.GGRUP and T54922.CODIGO = 5  and T55155.GGRUP = 5 ) ),
		SAWITH2 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 as c6,
			D2.c1 as c7,
			D1.c6 as c8,
			D1.c33 as c33
		from 
			SAWITH0 D1,
			SAWITH1 D2),
		SAWITH3 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH4 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH5 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c8, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11
				from 
					(
							SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3
			) D1
		where  ( D1.c11 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c33 as c33
		from 
			SAWITH5 D1
		order by c1, c7, c6, c4, c2, c3, c5 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:24:58.018016',NULL,55),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A2',NULL,'csv','Indicador IC110A2.csv','WITH 
		SAWITH0 AS (select count(T18685.IDPADRON) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T18894.CODIGO as c4,
			T18894.DENOMINACION as c5,
			T6789.CODIGO as c6,
			T18685.ANYO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_CP_IDI T21622,
			FACT_II_PAD_PADRON T18685
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T17978.DTBA = T18685.DISBAR and T18685.CODPOS = T21622.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18685.TIPLOC = T18894.CODIGO and T18685.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, T18894.DENOMINACION, T18685.ANYO ),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 as c6,
			D1.c6 as c7,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3
			) D1
		where  ( D1.c10 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c33 as c33 from ( select 
			D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c4, c2, c5, c3, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:24:58.018016',NULL,57),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A3',NULL,'csv','Indicador IC110A3.csv','WITH 
		SAWITH0 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDAMEN5 = 0 then ''Cap de 0-4'' else ''Algú de 0-4'' end  else case  when T18579.INDAMEN5 = 0 then ''Nadie de 0-4'' else ''Alguien de 0-4'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18579.INDAMEN5 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDAMEN5 = 0 then ''Cap de 0-4'' else ''Algú de 0-4'' end  else case  when T18579.INDAMEN5 = 0 then ''Nadie de 0-4'' else ''Alguien de 0-4'' end  end,T18579.ANO),
		OBICOMMON0 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		OBICOMMON1 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH1 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH0 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH2 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA0509 = 0 then ''Cap de 5-9'' else ''Algú de 5-9'' end  else case  when T18579.INDA0509 = 0 then ''Nadie de 5-9'' else ''Alguien de 5-9'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano'' and T18579.INDA0509 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA0509 = 0 then ''Cap de 5-9'' else ''Algú de 5-9'' end  else case  when T18579.INDA0509 = 0 then ''Nadie de 5-9'' else ''Alguien de 5-9'' end  end, T18579.ANO ),
		SAWITH3 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH2 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH4 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA1014 = 0 then ''Cap de 10-14'' else ''Algú de 10-14'' end  else case  when T18579.INDA1014 = 0 then ''Nadie de 10-14'' else ''Alguien de 10-14'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA1014 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA1014 = 0 then ''Cap de 10-14'' else ''Algú de 10-14'' end  else case  when T18579.INDA1014 = 0 then ''Nadie de 10-14'' else ''Alguien de 10-14'' end  end, T18579.ANO ),
		SAWITH5 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH4 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH6 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA1519 = 0 then ''Cap de 15-19'' else ''Algú de 15-19'' end  else case  when T18579.INDA1519 = 0 then ''Nadie de 15-19'' else ''Alguien de 15-19'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA1519 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA1519 = 0 then ''Cap de 15-19'' else ''Algú de 15-19'' end  else case  when T18579.INDA1519 = 0 then ''Nadie de 15-19'' else ''Alguien de 15-19'' end  end, T18579.ANO ),
		SAWITH7 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH6 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH8 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA2024 = 0 then ''Cap de 20-24'' else ''Algú de 20-24'' end  else case  when T18579.INDA2024 = 0 then ''Nadie de 20-24'' else ''Alguien de 20-24'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA2024 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA2024 = 0 then ''Cap de 20-24'' else ''Algú de 20-24'' end  else case  when T18579.INDA2024 = 0 then ''Nadie de 20-24'' else ''Alguien de 20-24'' end  end, T18579.ANO ),
		SAWITH9 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH8 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH10 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA2529 = 0 then ''Cap de 25-29'' else ''Algú de 25-29'' end  else case  when T18579.INDA2529 = 0 then ''Nadie de 25-29'' else ''Alguien de 25-29'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA2529 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA2529 = 0 then ''Cap de 25-29'' else ''Algú de 25-29'' end  else case  when T18579.INDA2529 = 0 then ''Nadie de 25-29'' else ''Alguien de 25-29'' end  end, T18579.ANO ),
		SAWITH11 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH10 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH12 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA3034 = 0 then ''Cap de 30-34'' else ''Algú de 30-34'' end  else case  when T18579.INDA3034 = 0 then ''Nadie de 30-34'' else ''Alguien de 30-34'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA3034 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA3034 = 0 then ''Cap de 30-34'' else ''Algú de 30-34'' end  else case  when T18579.INDA3034 = 0 then ''Nadie de 30-34'' else ''Alguien de 30-34'' end  end, T18579.ANO ),
		SAWITH13 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH12 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH14 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA3539 = 0 then ''Cap de 35-39'' else ''Algú de 35-39'' end  else case  when T18579.INDA3539 = 0 then ''Nadie de 35-39'' else ''Alguien de 35-39'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA3539 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA3539 = 0 then ''Cap de 35-39'' else ''Algú de 35-39'' end  else case  when T18579.INDA3539 = 0 then ''Nadie de 35-39'' else ''Alguien de 35-39'' end  end, T18579.ANO ),
		SAWITH15 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH14 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH16 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA4044 = 0 then ''Cap de 40-44'' else ''Algú de 40-44'' end  else case  when T18579.INDA4044 = 0 then ''Nadie de 40-44'' else ''Alguien de 40-44'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA4044 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA4044 = 0 then ''Cap de 40-44'' else ''Algú de 40-44'' end  else case  when T18579.INDA4044 = 0 then ''Nadie de 40-44'' else ''Alguien de 40-44'' end  end, T18579.ANO ),
		SAWITH17 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH16 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH18 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA4549 = 0 then ''Cap de 45-49'' else ''Algú de 45-49'' end  else case  when T18579.INDA4549 = 0 then ''Nadie de 45-49'' else ''Alguien de 45-49'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA4549 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA4549 = 0 then ''Cap de 45-49'' else ''Algú de 45-49'' end  else case  when T18579.INDA4549 = 0 then ''Nadie de 45-49'' else ''Alguien de 45-49'' end  end, T18579.ANO ),
		SAWITH19 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH18 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH20 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA5054 = 0 then ''Cap de 50-54'' else ''Algú de 50-54'' end  else case  when T18579.INDA5054 = 0 then ''Nadie de 50-54'' else ''Alguien de 50-54'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA5054 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA5054 = 0 then ''Cap de 50-54'' else ''Algú de 50-54'' end  else case  when T18579.INDA5054 = 0 then ''Nadie de 50-54'' else ''Alguien de 50-54'' end  end, T18579.ANO ),
		SAWITH21 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH20 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH22 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA5559 = 0 then ''Cap de 55-59'' else ''Algú de 55-59'' end  else case  when T18579.INDA5559 = 0 then ''Nadie de 55-59'' else ''Alguien de 55-59'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA5559 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA5559 = 0 then ''Cap de 55-59'' else ''Algú de 55-59'' end  else case  when T18579.INDA5559 = 0 then ''Nadie de 55-59'' else ''Alguien de 55-59'' end  end, T18579.ANO ),
		SAWITH23 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH22 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH24 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA6064 = 0 then ''Cap de 60-64'' else ''Algú de 60-64'' end  else case  when T18579.INDA6064 = 0 then ''Nadie de 60-64'' else ''Alguien de 60-64'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA6064 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA6064 = 0 then ''Cap de 60-64'' else ''Algú de 60-64'' end  else case  when T18579.INDA6064 = 0 then ''Nadie de 60-64'' else ''Alguien de 60-64'' end  end , T18579.ANO ),
		SAWITH25 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH24 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH26 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA6569 = 0 then ''Cap de 65-69'' else ''Algú de 65-69'' end  else case  when T18579.INDA6569 = 0 then ''Nadie de 65-69'' else ''Alguien de 65-69'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA6569 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA6569 = 0 then ''Cap de 65-69'' else ''Algú de 65-69'' end  else case  when T18579.INDA6569 = 0 then ''Nadie de 65-69'' else ''Alguien de 65-69'' end  end, T18579.ANO ),
		SAWITH27 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH26 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH28 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA7074 = 0 then ''Cap de 70-74'' else ''Algú de 70-74'' end  else case  when T18579.INDA7074 = 0 then ''Nadie de 70-74'' else ''Alguien de 70-74'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA7074 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA7074 = 0 then ''Cap de 70-74'' else ''Algú de 70-74'' end  else case  when T18579.INDA7074 = 0 then ''Nadie de 70-74'' else ''Alguien de 70-74'' end  end,T18579.ANO ),
		SAWITH29 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH28 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH30 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDA7579 = 0 then ''Cap de 75-79'' else ''Algú de 75-79'' end  else case  when T18579.INDA7579 = 0 then ''Nadie de 75-79'' else ''Alguien de 75-79'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDA7579 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDA7579 = 0 then ''Cap de 75-79'' else ''Algú de 75-79'' end  else case  when T18579.INDA7579 = 0 then ''Nadie de 75-79'' else ''Alguien de 75-79'' end  end, T18579.ANO ),
		SAWITH31 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH30 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) ),
		SAWITH32 AS (select T18894.CODIGO as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			case  when T6789.CODIGO = 0 then case  when T18579.INDAMAY80 = 0 then ''Cap major de 80 i més'' else ''Algun major de 80 i més'' end  else case  when T18579.INDAMAY80 = 0 then ''Ningún mayor de 80 y más'' else ''Algún mayor de 80 y más'' end  end  as c4,
			count(T18579.IDHOJA) as c5,
			T6789.CODIGO as c6,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDAMAY80 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, T18894.CODIGO, case  when T6789.CODIGO = 0 then case  when T18579.INDAMAY80 = 0 then ''Cap major de 80 i més'' else ''Algun major de 80 i més'' end  else case  when T18579.INDAMAY80 = 0 then ''Ningún mayor de 80 y más'' else ''Algún mayor de 80 y más'' end  end, T18579.ANO ),
		SAWITH33 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D3.c1 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH32 D1 inner join OBICOMMON0 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join OBICOMMON1 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) )
		(select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH1 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH3 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH5 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH7 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH9 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH11 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH13 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH15 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH17 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH19 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH21 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH23 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH25 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH27 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH29 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH31 D1
		union
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c7 ||'';''||
			D1.c33 as c33
		from 
			SAWITH33 D1)','c1,c2,c3,c4,c5,c6,c7,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:24:58.018016',NULL,58),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A4',NULL,'csv','Indicador IC110A4.csv','WITH 
		SAWITH0 AS (select T17978.DT as c1,
			T17978.DTBA as c2,
			case  when T6789.CODIGO = 0 then case  when T18579.INDS0024 = 0 then ''Cap membre de 0-24 anys'' else ''Només membres de 0-24 anys'' end  else case  when T18579.INDS0024 = 0 then ''Ningún miembro de 0-24 años'' else ''Solo miembros de 0-24 años'' end  end  as c3,
			count(T18579.IDHOJA) as c4,
			T6789.CODIGO as c5,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''   and T18579.INDS0024 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDS0024 = 0 then ''Cap membre de 0-24 anys'' else ''Només membres de 0-24 anys'' end  else case  when T18579.INDS0024 = 0 then ''Ningún miembro de 0-24 años'' else ''Solo miembros de 0-24 años'' end  end , T18579.ANO),
		OBICOMMON0 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		OBICOMMON1 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH1 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D1.c2 as c3,
					D3.c1 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1, D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8
				from 
					(
							SAWITH0 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3
			) D1
		where  ( D1.c8 = 1 ) ),
		SAWITH2 AS (select T17978.DT as c1,
			T17978.DTBA as c2,
			case  when T6789.CODIGO = 0 then case  when T18579.INDS2564 = 0 then ''Cap membre de 25-64 anys'' else ''Només membres de 25-64 anys'' end  else case  when T18579.INDS2564 = 0 then ''Ningún miembro de 25-64 años'' else ''Solo miembros de 25-64 años'' end  end  as c3,
			count(T18579.IDHOJA) as c4,
			T6789.CODIGO as c5,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDS2564 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDS2564 = 0 then ''Cap membre de 25-64 anys'' else ''Només membres de 25-64 anys'' end  else case  when T18579.INDS2564 = 0 then ''Ningún miembro de 25-64 años'' else ''Solo miembros de 25-64 años'' end  end, T18579.ANO ),
		SAWITH3 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D1.c2 as c3,
					D3.c1 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8
				from 
					(
							SAWITH2 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3
			) D1
		where  ( D1.c8 = 1 ) ),
		SAWITH4 AS (select T17978.DT as c1,
			T17978.DTBA as c2,
			case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY65 = 0 then ''Cap membre de 65 i més anys'' else ''Només membres de 65 i més anys'' end  else case  when T18579.INDSMAY65 = 0 then ''Ningún miembro de 65 y más años'' else ''Solo miembros de 65 y más años'' end  end  as c3,
			count(T18579.IDHOJA) as c4,
			T6789.CODIGO as c5,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDSMAY65 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY65 = 0 then ''Cap membre de 65 i més anys'' else ''Només membres de 65 i més anys'' end  else case  when T18579.INDSMAY65 = 0 then ''Ningún miembro de 65 y más años'' else ''Solo miembros de 65 y más años'' end  end, T18579.ANO ),
		SAWITH5 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D1.c2 as c3,
					D3.c1 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8
				from 
					(
							SAWITH4 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3
			) D1
		where  ( D1.c8 = 1 ) ),
		SAWITH6 AS (select T17978.DT as c1,
			T17978.DTBA as c2,
			case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY80 = 0 then ''Cap membre de 80 i més anys'' else ''Només membres de 80 i més anys'' end  else case  when T18579.INDSMAY80 = 0 then ''Ningún miembro de 80 y más años'' else ''Solo miembros de 80 y más años'' end  end  as c3,
			count(T18579.IDHOJA) as c4,
			T6789.CODIGO as c5,
			T18579.ANO as c33
		from 
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_II_PAD_CMSS_IDI T18834,
			FACT_II_PAD_HOJAPAD T18579
		where  ( T6789.CODIGO = T18894.IDIOMA and T7594.ANO = T18579.ANO and T6789.CODIGO = T18834.IDIOMA and T17978.DTBA = T18579.DISBAR and T18579.CENMUNSS = T18834.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18579.INDSMAY80 = 1 and T18579.TIPLOC = T18894.CODIGO and T18579.TIPLOC = 1 and T18894.CODIGO = 1 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T17978.DT, T17978.DTBA, case  when T6789.CODIGO = 0 then case  when T18579.INDSMAY80 = 0 then ''Cap membre de 80 i més anys'' else ''Només membres de 80 i més anys'' end  else case  when T18579.INDSMAY80 = 0 then ''Ningún miembro de 80 y más años'' else ''Solo miembros de 80 y más años'' end  end, T18579.ANO ),
		SAWITH7 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D1.c2 as c3,
					D3.c1 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c1, D1.c2, D1.c3, D1.c5, D2.c1, D3.c1,D1.c33 ORDER BY D1.c1 ASC, D1.c2 ASC, D1.c3 ASC, D1.c5 ASC, D2.c1 ASC, D3.c1 ASC) as c8
				from 
					(
							SAWITH6 D1 inner join OBICOMMON0 D2 On D1.c1 = D2.c2 and D1.c5 = D2.c3) inner join OBICOMMON1 D3 On D1.c2 = D3.c2 and D1.c5 = D3.c3
			) D1
		where  ( D1.c8 = 1 ) ),
		SAWITH8 AS ((select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c33 as c33
		from 
			SAWITH1 D1
		union
		select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c33 as c33
		from 
			SAWITH3 D1
		union
		select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c33 as c33
		from 
			SAWITH5 D1
		union
		select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c33 as c33
		from 
			SAWITH7 D1))
		select D1.c1 ||'';''||
			D1.c2 ||'';''||
			D1.c3 ||'';''||
			D1.c4 ||'';''||
			D1.c5 ||'';''||
			D1.c6 ||'';''||
			D1.c33 as c33
		from 
			SAWITH8 D1
		order by c1, c2, c3, c4, c5','c1,c2,c3,c4,c5,c6,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:24:58.018016',NULL,59),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A5',NULL,'csv','Indicador IC110A5.csv','WITH 
		SAWITH0 AS (select count(T18685.IDPADRON) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T10746.CODIGO as c4,
			T10746.DENOMINACION as c5,
			case  when T19562.CODIGO < 19 then T19562.CODIGO else 19 end  as c6,
			case  when T19562.CODIGO < 19 then T19562.DENOMINACION else case  when T6789.DENOMINACION = ''Castellano'' then ''90 y más'' else ''90 i més'' end  end  as c7,
			T6789.CODIGO as c8,
			T18685.ANYO as c33
		from 
			LU_EDADAGR20PAD_IDI T19562,
			V_LU_DIVMUN_DTBA T17978,
			LU_SEXO_IDI T10746,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_CP_IDI T21622,
			FACT_II_PAD_PADRON T18685
		where  ( T6789.CODIGO = T19562.IDIOMA and T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T6789.DENOMINACION = ''Castellano''  and T18685.EDADAGR = T19562.CODIGO ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T10746.CODIGO, T10746.DENOMINACION, T17978.DT, T17978.DTBA, case  when T19562.CODIGO < 19 then T19562.DENOMINACION else case  when T6789.DENOMINACION = ''Castellano'' then ''90 y más'' else ''90 i més'' end  end , case  when T19562.CODIGO < 19 then T19562.CODIGO else 19 end, T18685.ANYO ),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c1 as c8,
			D1.c1 as c9,
			D1.c8 as c10,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c12 as c12,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c9 as c11,
					D1.c10 as c12,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c10, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c10 ASC, D2.c1 ASC, D3.c1 ASC) as c13
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c10 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c10 = D3.c3
			) D1
		where  ( D1.c13 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c33 as c33 from ( select 
			D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c4, c5, c3, c6, c7, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:24:58.018016',NULL,60);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A6',NULL,'csv','Indicador IC110A6.csv','WITH 
		SAWITH0 AS (select count(T18685.IDPADRON) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T18846.CODIGO as c4,
			T18846.DENOMINACION as c5,
			T10746.CODIGO as c6,
			T10746.DENOMINACION as c7,
			T6789.CODIGO as c8,
			T18685.ANYO as c33
		from 
			LU_II_PAD_LUGNAC_IDI T18846,
			V_LU_DIVMUN_DTBA T17978,
			LU_SEXO_IDI T10746,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_CP_IDI T21622,
			FACT_II_PAD_PADRON T18685
		where  ( T6789.CODIGO = T18846.IDIOMA and T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T6789.DENOMINACION = ''Castellano''  and T18685.LUGNAC = T18846.CODIGO ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T10746.CODIGO, T10746.DENOMINACION, T17978.DT, T17978.DTBA, T18846.CODIGO, T18846.DENOMINACION, T18685.ANYO),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c1 as c8,
			D1.c8 as c9,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c9 as c11,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c9, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c9 ASC, D2.c1 ASC, D3.c1 ASC) as c12
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c9 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c9 = D3.c3
			) D1
		where  ( D1.c12 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c6, c7, c4, c2, c5, c3, c8, c9 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:24:58.018016',NULL,61),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A7',NULL,'csv','Indicador IC110A7.csv','WITH 
		SAWITH0 AS (select distinct count(T18685.IDPADRON) over (partition by T6789.DENOMINACION, T10746.CODIGO, T17978.DTBA, T19568.CODIGO)  as c1,
			T10746.CODIGO as c2,
			T10746.DENOMINACION as c3,
			T17978.DTBA as c4,
			T19568.CODIGO as c5,
			T19568.DENOMINACION as c6,
			T17978.DT as c7,
			T6789.CODIGO as c8,
			count(T18685.IDPADRON) over (partition by T6789.DENOMINACION, T10746.CODIGO, T17978.DTBA, T56143.CODPAIS, T19568.CODIGO)  as c9,
			T56143.CODPAIS as c10,
			T18685.ANYO as c33
		from 
			LU_EDADAGR3PAD_IDI T19568,
			V_LU_GEO_PAIS_NACION T56143,
			V_LU_DIVMUN_DTBA T17978,
			LU_SEXO_IDI T10746,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_CP_IDI T21622,
			FACT_II_PAD_PADRON T18685
		where  ( T6789.CODIGO = T19568.IDIOMA and T17978.DTBA = T18685.DISBAR and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T7594.ANO = T18685.ANYO and T6789.CODIGO = T21622.IDIOMA and T18685.CODPOS = T21622.CODIGO and T6789.CODIGO = T56143.IDIOMA and T18685.EDADAGR3 = T19568.CODIGO and T6789.DENOMINACION = ''Castellano''  and T18685.PAINACDAD = T56143.CODPAIS and T18685.PAINACDAD <> 108 and T56143.CODPAIS <> 108 ) ),
		SAWITH1 AS (select 0 as c1,
			D1.c7 as c2,
			D1.c4 as c3,
			D1.c6 as c4,
			D1.c5 as c5,
			D1.c10 as c6,
			D1.c2 as c7,
			D1.c3 as c8,
			D1.c9 as c9,
			D1.c1 as c10,
			D1.c8 as c11,
			D1.c33 as c33
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c12 as c12,
			D1.c13 as c13,
			D1.c33 as c33
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c9 as c11,
					D1.c10 as c12,
					D1.c11 as c13,
					D1.c33 as c33,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c7, D1.c8, D1.c11, D2.c1, D3.c1,D1.c33 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c7 ASC, D1.c8 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3
			) D1
		where  ( D1.c14 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 ||'';''|| D1.c33 as c33 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c12 as c12,
			D1.c33 as c33
		from 
			SAWITH4 D1
		order by c1, c4, c2, c5, c3, c9, c10, c7, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c33','N','sftp',NULL,NULL,'2018-06-12 08:56:04','Lozano','2021-08-31 11:24:58.018016',NULL,62),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC145A1',NULL,'csv','Indicador IC145A1.csv','select T6789.DENOMINACION ||'';''||
			T8148.CODIGO ||'';''||
			T8160.CODIGO ||'';''||
			T8160.DENOMINACION ||'';''||
			T7595.ANO ||'';''||
			T7595.MES ||'';''||
			T6789.CODIGO ||'';''||
			replace(T8109.VALOR , '','' , ''.'') as c9
		from 
			LU_I_MET_ESTMETEO T8148,
			V_LU_TIEMPO_ANOMES T7595,
			LU_IDIOMA T6789,
			LU_I_MET_INDICADOR_IDI T8160,
			FACT_I_MET_METEOANOMES T8109
		where  ( T6789.CODIGO = T8160.IDIOMA and T8109.ESTMETEO = T8148.CODIGO and T7595.ANO = T8109.ANO and T7595.MES = T8109.MES and T6789.DENOMINACION = ''Castellano''  and T8109.ESTMETEO = 1 and T8109.INDICADOR = T8160.CODIGO and T8109.INDICADOR = 7 and T8148.CODIGO = 1 and T8160.CODIGO = 7 )','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2018-06-12 08:57:20','Lozano','2021-08-31 11:24:58.018016',NULL,64),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC097',NULL,'csv','Indicador IC097.csv','WITH 
		SAWITH0 AS (select sum(T43974.VALOR) as c1,
			T43989.DENOMINACION as c2,
			T44001.CODIGO as c3,
			T44001.DENOMINACION as c4,
			T7594.ANO as c5
		from 
			LU_VIII_AGPO_TIPAB_IDI T44001,
			LU_VIII_AGPO_IND_IDI T43989,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_GEO_MUNICIPIO T6910,
			FACT_VIII_AGPO_INFABONADOS T43974
		where  ( T6789.CODIGO = T44001.IDIOMA and T7594.ANO = T43974.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T43974.MUNICIPIO and T6789.CODIGO = T43989.IDIOMA and T43974.INDICADOR = T43989.CODIGO and T6789.DENOMINACION = ''Castellano'' and T43974.TIPOABONADO = T44001.CODIGO and T43974.TIPOABONADO = 1 and T43989.DENOMINACION = ''Agua facturada miles m3'' and T44001.CODIGO = 1 ) 
		group by T7594.ANO, T43989.DENOMINACION, T44001.CODIGO, T44001.DENOMINACION)
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6  from ( select distinct 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 * 1000000 / 365 as c6
		from 
			SAWITH0 D1
		order by c5, c2, c4, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-31390','2021-08-31 11:24:58.018016','Lozano-31390',87),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110',NULL,'csv','Indicador IC110.csv','WITH 
		SAWITH0 AS (select sum(T21230.NUMPERSONAS) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T7594.ANO as c4,
			T6789.CODIGO as c5
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_SEXO_IDI T10746,
			FACT_II_PAD_AGREGADO T21230
		where  ( T6789.CODIGO = T10746.IDIOMA and T7594.ANO = T21230.ANYO and T6789.DENOMINACION = ''Castellano'' and T10746.CODIGO = T21230.SEXO and T17978.DTBA = T21230.DISBAR ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c1 as c5,
			D1.c5 as c6
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7
		from 
			SAWITH4 D1
		order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-32028','2021-08-31 11:24:58.018016','Lozano-32028',88),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9',NULL,'csv','Indicador IC110A9.csv','WITH 
		SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,
			T7594.ANO as c2
		from 
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_EDAD100PAD_IDI T19610,
			FACT_II_PAD_PADRON T18685
		where  ( T6789.CODIGO = T19610.IDIOMA and T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T18685.ANYO and T18685.EDAD100 = T19610.CODIGO ) 
		group by T7594.ANO)
		select D1.c1 ||'';'' || D1.c2 || '';'' || D1.c3 from ( select 0 as c1,
			D1.c2 as c2,
			D1.c1 as c3
		from 
			SAWITH0 D1
		order by c2 ) D1 where rownum <= 65001','c1,c2,c3','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-32028','2021-08-31 11:24:58.018016','Lozano-32028',89),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9B',NULL,'csv','Indicador IC110A9B.csv','WITH 
		SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,
			T10746.DENOMINACION as c2,
			T7594.ANO as c3
		from 
			LU_SEXO_IDI T10746,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_EDAD100PAD_IDI T19610,
			FACT_II_PAD_PADRON T18685
		where  ( T6789.CODIGO = T10746.IDIOMA and T6789.CODIGO = T19610.IDIOMA and T7594.ANO = T18685.ANYO and T6789.DENOMINACION = ''Castellano'' and T10746.CODIGO = T18685.SEXO and T18685.EDAD100 = T19610.CODIGO ) 
		group by T7594.ANO, T10746.DENOMINACION)
		select D1.c1 || '';'' || D1.c2 || '';'' || D1.c3 || '';'' || D1.c4  from ( select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c1 as c4
		from 
			SAWITH0 D1
		order by c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-32028','2021-08-31 11:24:58.018016','Lozano-32028',90),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A9C',NULL,'csv','Indicador IC110A9C.csv','WITH 
		SAWITH0 AS (select avg(T18685.EDADEXACTA) as c1,
			T55787.DT as c2,
			T17978.DTBA as c3,
			T10746.DENOMINACION as c4,
			T7594.ANO as c5,
			T6789.CODIGO as c6
		from 
			V_LU_DIVMUN_DT T55787,
			V_LU_DIVMUN_DTBA T17978,
			LU_SEXO_IDI T10746,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_EDAD100PAD_IDI T19610,
			FACT_II_PAD_PADRON T18685
		where  ( T17978.DTBA = T18685.DISBAR and T7594.ANO = T18685.ANYO and T6789.CODIGO = T19610.IDIOMA and T18685.DIS = T55787.DT and T6789.DENOMINACION = ''Castellano'' and T6789.CODIGO = T10746.IDIOMA and T10746.CODIGO = T18685.SEXO and T18685.EDAD100 = T19610.CODIGO ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T10746.DENOMINACION, T17978.DTBA, T55787.DT),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 as c6,
			D1.c6 as c7
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3
			) D1
		where  ( D1.c10 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8
		from 
			SAWITH4 D1
		order by c1, c7, c4, c2, c5, c3, c6 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,C8','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-31390','2021-08-31 11:24:58.018016','Lozano-31390',91),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A1',NULL,'csv','Indicador IC149A1.csv','WITH 
		SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,
			T55787.DT as c2,
			T17978.DTBA as c3,
			T7594.ANO as c4,
			case  when T104038.ANO <= 1800 then ''<=1800'' else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then ''1801-1900'' else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then ''1901-1920'' else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then ''1921-1940'' else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then ''1941-1960'' else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then ''1961-1970'' else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then ''1971-1980'' else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then ''1981-1990'' else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then ''1991-2000'' else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then ''2001-10'' else case  when T104038.ANO > 2010 then ''>2010'' end  end  end  end  end  end  end  end  end  end  end  as c5,
			case  when T104038.ANO <= 1800 then 1 else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then 2 else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then 3 else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then 4 else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then 5 else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then 6 else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then 7 else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then 8 else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then 9 else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then 10 else case  when T104038.ANO > 2010 then 11 end  end  end  end  end  end  end  end  end  end  end  as c6,
			T6789.CODIGO as c7
		from 
			V_LU_TIEMPO_ANO T104038 /* LU_IV_CAT_ANY1 */ ,
			V_LU_DIVMUN_DT T55787,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_DIVMUN_DTSC T22801,
			FACT_IV_CAT_VIV T103797
		where  ( T17978.DTBA = T103797.DTBA and T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T103797.DTSC and T55787.DT = T103797.DT and T6789.DENOMINACION = ''Castellano'' and T103797.ANY1 = T104038.ANO ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT, case  when T104038.ANO <= 1800 then ''<=1800'' else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then ''1801-1900'' else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then ''1901-1920'' else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then ''1921-1940'' else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then ''1941-1960'' else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then ''1961-1970'' else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then ''1971-1980'' else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then ''1981-1990'' else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then ''1991-2000'' else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then ''2001-10'' else case  when T104038.ANO > 2010 then ''>2010'' end  end  end  end  end  end  end  end  end  end  end , case  when T104038.ANO <= 1800 then 1 else case  when T104038.ANO <= 1900 and T104038.ANO > 1800 then 2 else case  when T104038.ANO <= 1920 and T104038.ANO > 1900 then 3 else case  when T104038.ANO <= 1940 and T104038.ANO > 1920 then 4 else case  when T104038.ANO <= 1960 and T104038.ANO > 1940 then 5 else case  when T104038.ANO <= 1970 and T104038.ANO > 1960 then 6 else case  when T104038.ANO <= 1980 and T104038.ANO > 1970 then 7 else case  when T104038.ANO <= 1990 and T104038.ANO > 1980 then 8 else case  when T104038.ANO <= 2000 and T104038.ANO > 1990 then 9 else case  when T104038.ANO <= 2010 and T104038.ANO > 2000 then 10 else case  when T104038.ANO > 2010 then 11 end  end  end  end  end  end  end  end  end  end  end ),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c1 as c7,
			D1.c7 as c8
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3
			) D1
		where  ( D1.c11 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9
		from 
			SAWITH4 D1
		order by c1, c6, c4, c2, c5, c3, c8, c7 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-32028','2021-08-31 11:24:58.018016','Lozano-32028',92),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A2',NULL,'csv','Indicador IC149A2.csv','WITH 
		SAWITH0 AS (select count(T103797.IDENTIFICADOR) as c1,
			T103931.DENOMINACION as c2,
			T55787.DT as c3,
			T17978.DTBA as c4,
			T7594.ANO as c5,
			T6789.CODIGO as c6
		from 
			LU_IV_CAT_SUPERF_IDI T103931,
			LU_IV_CAT_ANT1800_IDI T103907,
			V_LU_DIVMUN_DT T55787,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_DIVMUN_DTSC T22801,
			FACT_IV_CAT_VIV T103797
		where  ( T6789.CODIGO = T103931.IDIOMA and T55787.DT = T103797.DT and T17978.DTBA = T103797.DTBA and T7594.ANO = T103797.ANO and T6789.CODIGO = T22801.IDIOMA and T22801.DTSC = T103797.DTSC and T6789.CODIGO = T103907.IDIOMA and T103797.ANT1800 = T103907.CODIGO and T6789.DENOMINACION = ''Castellano'' and T103797.ANT1800 = 0 and T103797.SUPERF = T103931.CODIGO and T103907.CODIGO = 0 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT, T103931.DENOMINACION),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 as c6,
			D1.c6 as c7
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D3.c1 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c4 = D3.c2 and D1.c7 = D3.c3
			) D1
		where  ( D1.c10 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8
		from 
			SAWITH4 D1
		order by c1, c7, c2, c5, c3, c6, c4 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-31390','2021-08-31 11:24:58.018016','Lozano-31390',93);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A4',NULL,'csv','Indicador IC149A4.csv','WITH 
		SAWITH0 AS (select sum(T103867.NUMVIV1800) as c1,
			T103955.DENOMINACION as c2,
			T17978.DT as c3,
			T17978.DTBA as c4,
			T7594.ANO as c5,
			T6789.CODIGO as c6
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_IV_CAT_VCATA_IDI T103955,
			FACT_IV_CAT_VIV_AGRDTBAVC T103867
		where  ( T6789.CODIGO = T103955.IDIOMA and T7594.ANO = T103867.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103867.DTBA and T103867.VALCATA = T103955.CODIGO ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103955.DENOMINACION),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c1 as c6,
			D1.c6 as c7
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D3.c1 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c7, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c7 ASC, D2.c1 ASC, D3.c1 ASC) as c10
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c4 = D3.c2 and D1.c7 = D3.c3
			) D1
		where  ( D1.c10 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8
		from 
			SAWITH4 D1
		order by c1, c7, c5, c3, c6, c4, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-31390','2021-08-31 11:24:58.018016','Lozano-31390',94),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150',NULL,'csv','Indicador IC150.csv','WITH 
		SAWITH0 AS (select sum(T102892.NUMSOL) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T7594.ANO as c4,
			T6789.CODIGO as c5
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_SOL_AGRDTBA T102892
		where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c1 as c5,
			D1.c5 as c7
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D2.c1 as c2,
			D3.c1 as c3,
			D1.c2 as c4,
			D1.c3 as c5,
			D1.c4 as c6,
			D1.c5 as c7,
			D1.c7 as c9,
			ROW_NUMBER() OVER (PARTITION BY D1.c4, D1.c3, D1.c2, D2.c1, D3.c1 ORDER BY D1.c4 DESC, D1.c3 DESC, D1.c2 DESC, D2.c1 DESC, D3.c1 DESC) as c10
		from 
			(
				SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c7 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c7 = D3.c3),
		SAWITH5 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D1.c3 as c3,
					D1.c4 as c4,
					D1.c5 as c5,
					D1.c6 as c6,
					D1.c7 as c7,
					sum(case D1.c10 when 1 then D1.c7 else NULL end ) over ()  as c8,
					D1.c9 as c9,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c5, D1.c6, D1.c9 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c5 ASC, D1.c6 ASC, D1.c9 ASC) as c10
				from 
					SAWITH4 D1
			) D1
		where  ( D1.c10 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8
		from 
			SAWITH5 D1
		order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-31390','2021-08-31 11:24:58.018016','Lozano-31390',95),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A2',NULL,'csv','Indicador IC150A2.csv','WITH 
		SAWITH0 AS (select sum(T120156.NUMSOL) as c1,
			sum(T120156.VCATS) as c2,
			T7594.ANO as c3
		from 
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_SOL_AGRTOTALDT T120156
		where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120156.ANO ) 
		group by T7594.ANO)
		select D1.c1 || '';'' || D1.c2 || '';'' || D1.c3  from ( select distinct 0 as c1,
			D1.c3 as c2,
			round(D1.c2 / nullif( D1.c1, 0) , 2) as c3
		from 
			SAWITH0 D1
		order by c2 ) D1 where rownum <= 65001','c1,c2,c3','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-32054','2021-08-31 11:24:58.018016','Lozano-32054',96),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A2B',NULL,'csv','Indicador IC150A2B.csv','WITH 
		SAWITH0 AS (select sum(T102892.NUMSOL) as c1,
			sum(T102892.VCATS) as c2,
			T17978.DT as c3,
			T17978.DTBA as c4,
			T7594.ANO as c5,
			T6789.CODIGO as c6
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_SOL_AGRDTBA T102892
		where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA),
		SAWITH1 AS (select 0 as c1,
			D1.c3 as c2,
			D1.c4 as c3,
			D1.c5 as c4,
			D1.c1 as c5,
			D1.c2 as c6,
			round(D1.c2 / nullif( D1.c1, 0) , 2) as c7,
			D1.c6 as c8
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3
			) D1
		where  ( D1.c11 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9  from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9
		from 
			SAWITH4 D1
		order by c1, c6, c2, c4, c5, c3 ) D1 where rownum <= 65001
		','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-32054','2021-08-31 11:24:58.018016','Lozano-32054',97),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A3',NULL,'csv','Indicador IC150A3.csv','WITH 
		SAWITH0 AS (select sum(T120156.SUPSOL) as c1,
			sum(T120156.VCATS) as c2,
			T7594.ANO as c3
		from 
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_SOL_AGRTOTALDT T120156
		where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120156.ANO ) 
		group by T7594.ANO)
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 from ( select 0 as c1,
			D1.c3 as c2,
			D1.c1 as c3,
			D1.c2 as c4,
			D1.c2 / nullif( D1.c1, 0) as c5
		from 
			SAWITH0 D1
		order by c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-32054','2021-08-31 11:24:58.018016','Lozano-32054',98),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC150A3B',NULL,'csv','Indicador IC150A3B.csv','WITH 
		SAWITH0 AS (select sum(T102892.SUPSOL) as c1,
			sum(T102892.VCATS) as c2,
			T17978.DT as c3,
			T17978.DTBA as c4,
			T7594.ANO as c5,
			T6789.CODIGO as c6
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_SOL_AGRDTBA T102892
		where  ( T7594.ANO = T102892.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T102892.DTBA ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA),
		SAWITH1 AS (select 0 as c1,
			D1.c3 as c2,
			D1.c4 as c3,
			D1.c5 as c4,
			D1.c1 as c5,
			D1.c2 as c6,
			D1.c2 / nullif( D1.c1, 0) as c7,
			D1.c6 as c8
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c8, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c8 ASC, D2.c1 ASC, D3.c1 ASC) as c11
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c8 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c8 = D3.c3
			) D1
		where  ( D1.c11 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9
		from 
			SAWITH4 D1
		order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-31390','2021-08-31 11:24:58.018016','Lozano-31390',99),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151',NULL,'csv','Indicador IC151.csv','WITH 
		SAWITH0 AS (select sum(T103202.SUPCONS) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T7594.ANO as c4,
			T6789.CODIGO as c5
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_APAR_AGRDTBA T103202
		where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c1 as c5,
			D1.c5 as c6
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7
		from 
			SAWITH4 D1
		order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2019-05-07 09:26:20','Lozano-31390','2021-08-31 11:24:58.018016','Lozano-31390',100),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC098',NULL,'csv','Indicador IC098.csv','WITH 
		SAWITH0 AS (select sum(T43966.VALOR) as c1,
			T43989.DENOMINACION as c2,
			T55787.DT as c3,
			T7594.ANO as c4,
			T6789.CODIGO as c5
		from 
			LU_VIII_AGPO_IND_IDI T43989,
			V_LU_DIVMUN_DT T55787,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			V_LU_GEO_MUNICIPIO T6910,
			FACT_VIII_AGPO_FACTDT T43966
		where  ( T6789.CODIGO = T43989.IDIOMA and T7594.ANO = T43966.ANO and T6789.CODIGO = T6910.IDIOMA and T6910.CODMUNICIPIO = T43966.MUNICIPIO and T43966.DISTRITO = T55787.DT and T6789.DENOMINACION = ''Castellano'' and T43966.INDICADOR = T43989.CODIGO and T43966.INDICADOR = 3 and T43989.CODIGO = 3 ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T43989.DENOMINACION, T55787.DT),
		SAWITH1 AS (select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c1 as c5,
			D1.c5 as c6
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7
		from 
			(select D1.c1 as c1,
					D1.c2 as c2,
					D2.c1 as c3,
					D1.c3 as c4,
					D1.c4 as c5,
					D1.c5 as c6,
					D1.c6 as c7,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC) as c8
				from 
					SAWITH1 D1 inner join SAWITH2 D2 On D1.c3 = D2.c2 and D1.c6 = D2.c3
			) D1
		where  ( D1.c8 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6  from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6
		from 
			SAWITH3 D1
		order by c1, c5, c4, c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6','N','sftp',NULL,NULL,'2019-05-28 08:54:11','Lozano-32016','2021-08-31 11:24:58.018016','Lozano-32016',101),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC110A1',NULL,'csv','Indicador IC110A1.csv','WITH 
		SAWITH0 AS (select count(T18685.IDPADRON) as c1,
			T18894.DENOMINACION as c2,
			T7594.ANO as c3
		from 
			LU_EDADES T22191,
			LU_II_PAD_TIPVIV_IDI T18894,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_EDAD100PAD_IDI T19610,
			FACT_II_PAD_PADRON T18685
		where  ( T18685.EDAD = T22191.EDAD and T7594.ANO = T18685.ANYO and T6789.CODIGO = T19610.IDIOMA and T18685.EDAD100 = T19610.CODIGO and T6789.CODIGO = T18894.IDIOMA and T18685.TIPLOC = T18894.CODIGO and T6789.DENOMINACION = ''Castellano'' and T18894.DENOMINACION = ''Vivienda familiar'' and T19610.CODIGO = T22191.EDAD100 and T18685.EDAD <= 17 and T22191.EDAD <= 17 ) 
		group by T7594.ANO, T18894.DENOMINACION)
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4  from ( select 0 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c1 as c4
		from 
			SAWITH0 D1
		order by c3, c2 ) D1 where rownum <= 65001','c1,c2,c3,c4','N','sftp',NULL,NULL,'2019-05-28 08:54:11','Lozano-32016','2021-08-31 11:24:58.018016','Lozano-32016',102),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A21',NULL,'csv','Indicador IC151A21.csv','select sup_aparcamientos.c2 ||'';''|| sup_aparcamientos.c1/num_turismos.c1 from
		(select sum(T120150.SUPCONS) as c1,
			T7594.ANO as c2
		from 
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_APAR_AGRTOTALDT T120150
		where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120150.ANO ) 
		group by T7594.ANO
		order by c2) sup_aparcamientos,

		(select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,
			T7594.ANO as c2
		from 
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_III_MAT_ANYMAT_IDI T16638,
			FACT_III_MAT_MATRICULA T16529
		where  ( T6789.CODIGO = T16638.IDIOMA and T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T16529.ANO and T16529.ANYMAT = T16638.CODIGO ) 
		group by T7594.ANO
		order by c2) num_turismos
		where sup_aparcamientos.c2=num_turismos.c2','c1,c2','N','sftp',NULL,NULL,'2019-05-28 08:54:11','Lozano-32055','2021-08-31 11:24:58.018016','Lozano-32055',104);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC151A2',NULL,'csv','Indicador IC151A2.csv','WITH 
		SAWITH0 AS (select sum(T103202.SUPCONS) as c1,
			T17978.DT as c2,
			T17978.DTBA as c3,
			T7594.ANO as c4,
			T6789.CODIGO as c5,
			T6789.DENOMINACION as c6
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_APAR_AGRDTBA T103202
		where  ( T7594.ANO = T103202.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103202.DTBA ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA),
		SAWITH1 AS (select count(case  when T16529.TIPOFIN = 1 then 1 end ) as c1,
			T55787.DT as c2,
			T17978.DTBA as c3,
			T7594.ANO as c4,
			T6789.CODIGO as c5,
			T6789.DENOMINACION as c6
		from 
			V_LU_DIVMUN_DT T55787,
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			LU_III_MAT_ANYMAT_IDI T16638,
			FACT_III_MAT_MATRICULA T16529
		where  ( T7594.ANO = T16529.ANO and T6789.CODIGO = T16638.IDIOMA and T16529.ANYMAT = T16638.CODIGO and T6789.DENOMINACION = ''Castellano'' and T16529.DT = T55787.DT and T16529.DTBA = T17978.DTBA ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DTBA, T55787.DT),
		SAWITH2 AS (select 0 as c1,
			case  when D1.c2 is not null then D1.c2 when D2.c2 is not null then D2.c2 end  as c2,
			case  when D1.c3 is not null then D1.c3 when D2.c3 is not null then D2.c3 end  as c3,
			case  when D1.c4 is not null then D1.c4 when D2.c4 is not null then D2.c4 end  as c4,
			D1.c1 / nullif( D2.c1, 0) as c5,
			case  when D1.c5 is not null then D1.c5 when D2.c5 is not null then D2.c5 end  as c6
		from 
			SAWITH0 D1 full outer join SAWITH1 D2 On D1.c2 = D2.c2 and D1.c5 = D2.c5 and D1.c3 = D2.c3 and D1.c4 = D2.c4 and  SYS_OP_MAP_NONNULL(D1.c6) = SYS_OP_MAP_NONNULL(D2.c6) ),
		SAWITH3 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH4 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH5 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c6, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c6 ASC, D2.c1 ASC, D3.c1 ASC) as c9
				from 
					(
							SAWITH2 D1 inner join SAWITH3 D2 On D1.c2 = D2.c2 and D1.c6 = D2.c3) inner join SAWITH4 D3 On D1.c3 = D3.c2 and D1.c6 = D3.c3
			) D1
		where  ( D1.c9 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7  from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7
		from 
			SAWITH5 D1
		order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001
		','c1,c2,c3,c4,c5,c6,c7','N','sftp',NULL,NULL,'2019-05-28 08:54:11','Lozano-32055','2021-08-31 11:24:58.018016','Lozano-32055',105),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3',NULL,'csv','Indicador IC149A3.csv','WITH 
		SAWITH0 AS (select T103896.NUMVIV1800 as c1,
			sum(T103896.VALCAT_TOT) as c2,
			sum(T103896.VCATS_MED) as c3,
			sum(T103896.VALCAT_MED) as c4,
			sum(T103896.VCATC_MED) as c5,
			T17978.DT as c6,
			T17978.DTBA as c7,
			T7594.ANO as c8,
			T6789.CODIGO as c9
		from 
			V_LU_DIVMUN_DTBA T17978,
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_VIV_AGRVCDTBA T103896
		where  ( T7594.ANO = T103896.ANO and T6789.DENOMINACION = ''Castellano'' and T17978.DTBA = T103896.DTBA ) 
		group by T6789.CODIGO, T6789.DENOMINACION, T7594.ANO, T17978.DT, T17978.DTBA, T103896.NUMVIV1800),
		SAWITH1 AS (select 0 as c1,
			D1.c6 as c2,
			D1.c7 as c3,
			D1.c8 as c4,
			D1.c5 as c5,
			D1.c4 as c6,
			D1.c3 as c7,
			D1.c2 as c8,
			D1.c2 / nullif( D1.c1, 0) as c9,
			D1.c1 as c10,
			D1.c9 as c11
		from 
			SAWITH0 D1),
		SAWITH2 AS (select T58175.DENOMINACION as c1,
			T58175.CODIGO as c2,
			T58175.IDIOMA as c3
		from 
			LU_DISTRITO_IDI T58175 /* LU_DISTRITO_LOOKUP */ ),
		SAWITH3 AS (select T58289.DENOMINACION as c1,
			T58289.CODIGO as c2,
			T58289.IDIOMA as c3
		from 
			LU_DISTRITOBARRIO_IDI T58289 /* LU_DISTRITOBARRIO_LOOKUP */ ),
		SAWITH4 AS (select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c12 as c12,
			D1.c13 as c13
		from 
			(select D1.c1 as c1,
					D2.c1 as c2,
					D3.c1 as c3,
					D1.c2 as c4,
					D1.c3 as c5,
					D1.c4 as c6,
					D1.c5 as c7,
					D1.c6 as c8,
					D1.c7 as c9,
					D1.c8 as c10,
					D1.c9 as c11,
					D1.c10 as c12,
					D1.c11 as c13,
					ROW_NUMBER() OVER (PARTITION BY D1.c2, D1.c3, D1.c4, D1.c11, D2.c1, D3.c1 ORDER BY D1.c2 ASC, D1.c3 ASC, D1.c4 ASC, D1.c11 ASC, D2.c1 ASC, D3.c1 ASC) as c14
				from 
					(
							SAWITH1 D1 inner join SAWITH2 D2 On D1.c2 = D2.c2 and D1.c11 = D2.c3) inner join SAWITH3 D3 On D1.c3 = D3.c2 and D1.c11 = D3.c3
			) D1
		where  ( D1.c14 = 1 ) )
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 ||'';''|| D1.c4 ||'';''|| D1.c5 ||'';''|| D1.c6 ||'';''|| D1.c7 ||'';''|| D1.c8 ||'';''|| D1.c9 ||'';''|| D1.c10 ||'';''|| D1.c11 ||'';''|| D1.c12 from ( select D1.c1 as c1,
			D1.c2 as c2,
			D1.c3 as c3,
			D1.c4 as c4,
			D1.c5 as c5,
			D1.c6 as c6,
			D1.c7 as c7,
			D1.c8 as c8,
			D1.c9 as c9,
			D1.c10 as c10,
			D1.c11 as c11,
			D1.c12 as c12
		from 
			SAWITH4 D1
		order by c1, c6, c4, c2, c5, c3 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2019-05-28 08:54:11','Lozano-31390','2021-08-31 11:24:58.018016','Lozano-31390',106),
			('VLCi_Integracion_Estadisticas_BI_ETL','BBDD','Oracle_ESTAD',NULL,NULL,'/prod/01.fichOrig/VLCi_Integracion_Estadisticas_BI_ETL/','/prod/02.fichBackup/VLCi_Integracion_Estadisticas_BI_ETL/',NULL,'IC149A3_1',NULL,'csv','Indicador IC149A3_1.csv','WITH 
		SAWITH0 AS (select sum(T120168.VALCAT_MED) as c1,
			T7594.ANO as c2
		from 
			V_LU_TIEMPO_ANO T7594,
			LU_IDIOMA T6789,
			FACT_IV_CAT_VIV_AGRVCTOTALDT T120168
		where  ( T6789.DENOMINACION = ''Castellano'' and T7594.ANO = T120168.ANO ) 
		group by T7594.ANO)
		select D1.c1 ||'';''|| D1.c2 ||'';''|| D1.c3 from ( select 0 as c1,
			D1.c2 as c2,
			D1.c1 as c3
		from 
			SAWITH0 D1
		order by c2 ) D1 where rownum <= 65001','c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12','N','sftp',NULL,NULL,'2019-05-28 08:54:11','Lozano-32055','2021-08-31 11:24:58.018016','Lozano-32055',107),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SEDE',NULL,NULL,'/prod/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/prod/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC15',NULL,'csv','Indicador ISTIC15','select count(*) ||'';''||  TO_CHAR(dindate, ''YYYY-MM'') from pro2_ocs.docmeta d, pro2_ocs.revisions r where xaytoestadoelaboracionNTI = ''EE02'' and ddoctitle like ''Padron%'' and dindate >= (SELECT trunc(trunc(sysdate,''month'')-1,''month'') FROM DUAL) AND dindate < (SELECT trunc((sysdate),''month'') FROM DUAL) and d.did = r.did group by TO_CHAR(dindate, ''YYYY-MM'')','resultado,fecha','S','sftp',NULL,NULL,'2018-07-10 08:48:48','Ticket-23401','2021-08-31 11:24:58.018016',NULL,70),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/prod/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC20',NULL,'csv','Indicador ISTIC20','SELECT * FROM (select Asunto ||'';''||  count(asunto) ||'';''||  TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') from instancia_presentada where TO_CHAR(Fecha_Registro, ''YYYYMMDD'') = TO_CHAR(sysdate -1, ''YYYYMMDD'') group by Asunto, TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') order by count(asunto) desc) WHERE ROWNUM <= 10','asunto,instancias,fecha','S','sftp',NULL,NULL,'2018-07-10 08:48:48','Ticket-23401','2021-08-31 11:24:58.018016',NULL,71),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02D_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02D_anual','SELECT autoridad ||'';''|| replace(porcentaje,'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad not in (''FNMT-Ceres'',''ACCV'',''Cl@ve'',''DNIe'')','Autoridad,Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2020-06-09 08:45:50','Ivan 29772','2021-08-31 11:24:58.018016','Ivan 29772',128),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02B_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02B_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(Fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(Fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad = (''Cl@ve'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2020-06-09 08:45:50','Ivan 29772','2021-08-31 11:24:58.018016','Ivan 29772',123),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02B_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02B_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad = (''Cl@ve'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2020-06-09 08:45:50','Ivan 29772','2021-08-31 11:24:58.018016','Ivan 29772',124),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02C_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02C_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad = ''DNIe'' group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2020-06-09 08:45:50','Ivan 29772','2021-08-31 11:24:58.018016','Ivan 29772',125),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02C_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02C_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad = ''DNIe'' group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2020-06-09 08:45:50','Ivan 29772','2021-08-31 11:24:58.018016','Ivan 29772',126);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02D_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02D_mensual','SELECT autoridad ||'';''|| replace(porcentaje,'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad not in (''FNMT-Ceres'',''ACCV'',''Cl@ve'',''DNIe'')','Autoridad,Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2020-06-09 08:45:50','Ivan 29772','2021-08-31 11:24:58.018016','Ivan 29772',127),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC01_#ddMMyyyy#',NULL,'csv','Indicador ISTIC01','select count(*)||'';''||TO_CHAR(Fecha_acceso, ''YYYY-MM'')
		from acceso_sede where fecha_acceso >=trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') 
		AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''), ''MM'') group by TO_CHAR(Fecha_acceso, ''YYYY-MM'')','Accesos_autenticados,Fecha_acceso','S','sftp',NULL,NULL,'2018-11-27 08:49:51','Ivan 29091','2021-08-31 11:24:58.018016','Ivan 31670',78),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC10_#ddMMyyyy#',NULL,'csv','Indicador ISTIC10','select count(*) ||'';''|| TO_CHAR(Fecha_acceso, ''YYYY-MM'')
		from acceso_sede 
		where EXTRACT(month FROM Fecha_acceso) = (EXTRACT(month FROM TO_DATE(''#fecha#'',''yyyyMMdd''))-1)
		and EXTRACT(year FROM Fecha_acceso) = EXTRACT(year FROM TO_DATE(''#fecha#'',''yyyyMMdd''))
		group by TO_CHAR(Fecha_acceso, ''YYYY-MM'')','Accesos_autenticados,Fecha_acceso','S','sftp',NULL,NULL,'2018-11-27 08:49:51','Ivan 29091','2021-08-31 11:24:58.018016','Ivan 31670',80),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC12_#ddMMyyyy#',NULL,'csv','Indicador ISTIC12','SELECT ta.autoridad ||'';''|| count(*) ||'';''|| TO_CHAR(fecha_acceso,''YYYY-MM-DD'') AS Num_Accesos
		FROM acceso_sede acs, sede_electro.tipo_acceso ta
		WHERE acs.id_tipo_acceso = ta.id_tipo_acceso 
		AND TO_CHAR(acs.FECHA_ACCESO, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') - 1, ''YYYY-MM-DD'')
		AND (acs.id_tipo_acceso is not null)
		AND acs.id_resultado_login = 1
		GROUP BY ta.autoridad, TO_CHAR(fecha_acceso,''YYYY-MM-DD'')
		ORDER BY Num_accesos DESC','Autoridad,Num_Accesos,Fecha','S','sftp',NULL,NULL,'2018-11-27 08:49:51','Ivan 29091','2021-08-31 11:24:58.018016','Ivan 31670',81),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC13_#ddMMyyyy#',NULL,'csv','Indicador ISTIC13','select Asunto ||'';''|| count(asunto) ||'';''|| TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'')
		from instancia_presentada
		where TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') -1, ''YYYY-MM-DD'') 
		group by Asunto, TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'')
		order by TO_CHAR(Fecha_Registro, ''YYYY-MM-DD'') desc','Asunto,Instancias,Fecha','S','sftp',NULL,NULL,'2018-11-27 08:49:51','Ivan 29091','2021-08-31 11:24:58.018016','Ivan 31670',82),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC14_#ddMMyyyy#',NULL,'csv','Indicador ISTIC14','select count(*) ||'';''|| TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'')
		from tramite 
		where TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') = TO_CHAR(TO_DATE(''#fecha#'',''yyyyMMdd'') -1, ''YYYY-MM-DD'')
		group by TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'')
		order by TO_CHAR(Fecha_Creacion, ''YYYY-MM-DD'') desc','Tramites,Fecha','S','sftp',NULL,NULL,'2018-11-27 08:49:51','Ivan 29091','2021-08-31 11:24:58.018016','Ivan 31670',83),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC16_#ddMMyyyy#',NULL,'csv','Indicador ISTIC16','select TO_CHAR(primer_acceso, ''YYYY-MM'') ||'';''|| count(*)
		from (
			select min(fecha_acceso) as primer_acceso, num_documento_usuario 
			from  acceso_sede 
			where id_resultado_login = 1 
			group by num_documento_usuario)
		where 
		primer_acceso >=trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') 
		AND primer_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''), ''MM'')
		group by TO_CHAR(primer_acceso, ''YYYY-MM'')
		order by TO_CHAR(primer_acceso, ''YYYY-MM'') desc','Primer_acceso,Numero','S','sftp',NULL,NULL,'2018-11-27 08:49:51','Ivan 29091','2021-08-31 11:24:58.018016','Ivan 31670',84),
			('VLCI_ETL_SERTIC_SEDE_ORACLE','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCi_Sertic_Sede_Oracle/','/prod/02.fichBackup/VLCi_Sertic_Sede_Oracle/',NULL,'ISTIC53',NULL,'csv','Indicador ISTIC53','SELECT count(*) ||'';''||  TO_CHAR(fecha_registro, ''YYYY-MM'') FROM instancia_presentada WHERE nif_interesado <> nif_tramitador AND fecha_registro >=(SELECT trunc(trunc(sysdate,''month'')-1,''month'') FROM DUAL) AND fecha_registro  < (SELECT trunc((sysdate),''month'') FROM DUAL) GROUP BY TO_CHAR(fecha_registro, ''YYYY-MM'')','resultado,fecha','S','sftp',NULL,NULL,'2018-07-10 08:48:48','Ticket-23401','2021-08-31 11:24:58.018016',NULL,72),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02A_mensual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02A_mensual','SELECT replace(sum(porcentaje),'','',''.'') ||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY-MM'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''MM'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad, TO_CHAR(fecha_acceso, ''YYYY-MM'')) pr where pr.autoridad IN (''FNMT-Ceres'',''ACCV'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2020-06-09 08:45:50','Ivan 29772','2021-08-31 11:24:58.018016','Ivan 29772',121),
			('VLCI_ETL_Sertic_Oracle','BBDD','Oracle_SERTIC',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_Sertic_Oracle/','/prod/02.fichBackup/VLCI_ETL_Sertic_Oracle/',NULL,'ISTIC02A_anual_#ddMMyyyy#',NULL,'csv','Indicador ISTIC02A_anual','SELECT replace(sum(porcentaje),'','',''.'')||'';''|| fecha_acceso FROM (SELECT tipo_acceso.autoridad,round(100*(count(*) / sum(count(*)) over ()),8) As Porcentaje,TO_CHAR(fecha_acceso, ''YYYY'') as fecha_acceso FROM sede_electro.acceso_sede, sede_electro.tipo_acceso WHERE acceso_sede.id_tipo_acceso = tipo_acceso.id_tipo_acceso AND fecha_acceso >= trunc(add_months(TO_DATE(''#fecha#'',''yyyyMMdd''),-1),''YYYY'') AND fecha_acceso < trunc(TO_DATE(''#fecha#'',''yyyyMMdd''),''MM'') AND (acceso_sede.id_tipo_acceso is not null) GROUP BY tipo_acceso.autoridad,TO_CHAR(fecha_acceso, ''YYYY'')) pr where pr.autoridad IN (''FNMT-Ceres'',''ACCV'') group by fecha_acceso','Porcentaje_Accesos,Fecha_acceso','S','sftp',NULL,NULL,'2020-06-09 08:45:50','Ivan 29772','2021-08-31 11:24:58.018016','Ivan 29772',122);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','PostGis_Plataforma',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_TRAMOS_CONGESTION/','/prod/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_TRAMOS_CONGESTION/',NULL,'tramos_congestion',NULL,'csv','tramos_congestion','select concat(id_tramo,'';'',desc_tramo_cas,'';'', desc_tramo_val) from t_d_trafico_tramos_congestiones;','id_tramo, desc_tramo_cas, desc_tramo_val','S','sftp',NULL,NULL,'2018-10-16 09:01:35','toni-28427','2021-08-31 11:33:10.375547',NULL,76),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','SQL_Server_Trafico',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_CONGESTION/','/prod/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_CONGESTION/',NULL,'congestion_diaria',NULL,'csv','congestion_diaria','SELECT concat( rec.Rec, '';'',''#fecha#'', '';'', round(convert(float,sum([PorcC]))/convert(float,(SELECT count(rec2.rec)
		FROM [EstadisticasGIP].[dbo].[Recorr] rec2
		WHERE rec2.Rec =rec.Rec
		and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' ,
		round(convert(float,sum([PorcD]))/convert(float,(SELECT count(rec2.rec)
		FROM [EstadisticasGIP].[dbo].[Recorr] rec2
		WHERE rec2.Rec =rec.Rec
		and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' ,
		round(convert(float,sum([PorcFX]))/convert(float,(SELECT count(rec2.Rec)
		FROM [EstadisticasGIP].[dbo].[Recorr] rec2
		WHERE rec2.Rec =rec.Rec
		and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0), '';'' ,
		( 100-(round(convert(float,sum([PorcC]))/convert(float,(SELECT count(rec2.rec)
		FROM [EstadisticasGIP].[dbo].[Recorr] rec2
		WHERE rec2.Rec =rec.Rec
		and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0)+

		round(convert(float,sum([PorcD]))/convert(float,(SELECT count(rec2.rec)
		FROM [EstadisticasGIP].[dbo].[Recorr] rec2
		WHERE rec2.Rec =rec.Rec
		and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0)+

		round(sum(convert(float,[PorcFX]))/convert(float,(SELECT count(rec2.Rec)
		FROM [EstadisticasGIP].[dbo].[Recorr] rec2
		WHERE rec2.Rec =rec.Rec
		and convert(date,DATEADD(HOUR,-1,rec2.Hora))=CAST(''#fecha#'' as date))),0))) )
		FROM [EstadisticasGIP].[dbo].[Recorr] rec
		WHERE convert(date,DATEADD(HOUR,-1,Hora))=CAST(''#fecha#'' as date)
		group by rec.Rec
		order by rec.Rec;','recorrido, dia, porc_denso, porc_congestion, porc_cortado, porc_fluido','S','sftp',NULL,NULL,'2018-10-16 09:01:35','toni-28427','2021-08-31 11:24:58.018016',NULL,75),
			('VLCI_ETL_MOBILIDAD_IND_DIARIOS','BBDD','SQL_Server_Trafico',NULL,NULL,'/prod/01.fichOrig/VLCI_ETL_MOBILIDAD_IND_INTENSIDAD/','/prod/02.fichBackup/VLCI_ETL_MOBILIDAD_IND_INTENSIDAD/',NULL,'intensidad_diaria',NULL,'csv','intensidad_diaria','SELECT
		concat(dea.nombre,'';'',
		convert(date,dapm.FechaHora),'';'',
		(sum(dapm.intensidad)/[PROPERTY_TRAFICO_NUM_MEDIDAS_HOR]))
		FROM
		[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado] dea,
		[WEB_TRAFICO].[dbo].[Datos_Aforados_PM] dapm
		where
		dea.idTA=dapm.iDTA and
		dea.estado=''A''and
		convert(date,dapm.FechaHora)=CAST(''#fecha#'' as date)
		group by
		dea.nombre,
		convert(date,dapm.FechaHora)','id_tramo, fecha, intensidad_horaria','S','sftp',NULL,NULL,'2018-10-16 09:01:35','toni-28427','2021-08-31 11:24:58.018016','Ivan 34096',73),
			('VLCi_ETL_INCO','FTP','FTP Sertic','SMC*INCO*','//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi','/prod/01.fichOrig/VLCi_ETL_INCO/','/prod/02.fichBackup/VLCi_ETL_INCO/',NULL,'SMC-INCO-yyyy','S','txt',NULL,NULL,NULL,'N','sftp',NULL,NULL,'2023-09-22 08:20:29.404222','Ticket 1914','2023-09-22 08:20:29.404222','Ticket 1914',230);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('py_calidadambiental_sonometros_zas_WOO', 'FTP', 'FTP Sertic', '*WOO', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.WOO', 'S', 'WOO', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_MAN', 'FTP', 'FTP Sertic', '*MAN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.MAN', 'S', 'MAN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_SER', 'FTP', 'FTP Sertic', '*SER', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.SER', 'S', 'SER', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_HON', 'FTP', 'FTP Sertic', '*HON', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.HON', 'S', 'HON', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_CED', 'FTP', 'FTP Sertic', '*CED', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.CED', 'S', 'CED', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_PIN', 'FTP', 'FTP Sertic', '*PIN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.PIN', 'S', 'PIN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_SEN', 'FTP', 'FTP Sertic', '*SEN', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.SEN', 'S', 'SEN', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_QUA', 'FTP', 'FTP Sertic', '*QUA', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.QUA', 'S', 'QUA', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_BUE', 'FTP', 'FTP Sertic', '*BUE', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.BUE', 'S', 'BUE', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_UNI', 'FTP', 'FTP Sertic', '*UNI', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.UNI', 'S', 'UNI', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_xlsx', 'FTP', 'FTP Sertic', '*xlsx', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.xlsx', 'S', 'xlsx', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL),
			('py_calidadambiental_sonometros_zas_xls', 'FTP', 'FTP Sertic', '*xls', '//dadesaux1/DADES1/ayun/Contaminacio/Trabajos Comunes Servicio/SEC. TÉCNICA CONTAMINACIÓN/SONOMETROS/', '/prod/01.fichOrig/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', '/prod/02.fichBackup/VLCI_PY_CALIDADAMBIENTAL_SONOMETROS_ZAS/', NULL, '*.xls', 'S', 'xls', NULL, NULL, NULL, 'N', 'sftp', NULL, NULL, now(), 'JULIAN-1844', now(), 'JULIAN-1844', NULL);
		INSERT INTO vlci2.t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd,etl_id) VALUES
			('py_trafico_intensidad_validada', 'BBDD', 'SQL_Server_Trafico', NULL, NULL, '/prod/01.fichOrig/VLCI_PY_TRAFICO_VALIDADADO/', '/prod/02.fichBackup/VLCI_PY_TRAFICO_VALIDADADO/', NULL, 'intensidad_trafico_validada_30_#ddMMyyyyHHmmss#', NULL, 'csv', 'intensidad_trafico_validada_30', 
			'SELECT concat(''Kpi-Accesos-Ciudad-Coches'', '';'', actual.idata, '';'', descripciones.descripcion, '';'', descripciones.descripcion_corta, '';'', descripciones.nombre_corto, '';'', actual.int_actual, '';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A373'',''A406'',''A72'',''A59'',''A1'',''A82'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA,vid.DÍA) actual
			inner join (
    			select 4 as idATA, ''Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)'' as descripcion, ''Accés Barcelona (entrada i eixida)'' as descripcion_corta, ''Accés Barcelona'' as nombre_corto union
    			select 57 as idATA, ''Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)''  as descripcion, ''Accés a Arxiduc Carles pel Camí nou de Picanya'' as descripcion_corta, ''Arxiduc Carles'' as nombre_corto union
    			select 70 as idATA, ''Av. del Cid (Entre V-30 i Tres Creus)''  as descripcion, ''Av. del Cid'' as descripcion_corta, ''Av. del Cid'' as nombre_corto union
    			select 78 as idATA, ''Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)''  as descripcion, ''Corts Valencianes (Accés per CV-35)'' as descripcion_corta, ''Corts Valencianes'' as nombre_corto union
    			select 349 as idATA, ''Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)''      as descripcion, ''Accés per V-31 (Pista de Silla)'' as descripcion_corta, ''Pista de Silla'' as nombre_corto union
    			select 379 as idATA, ''Prolongació Joan XXIII (Entre Germans Machado i Salvador Cerveró)''  as descripcion, ''Prolongació Joan XXIII'' as descripcion_corta, ''Joan XXIII'' as nombre_corto) as descripciones
    			on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Accesos-Vias-Coches'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion_corta,'';'', descripciones.nombre_corto,'';'', actual.int_actual,'';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A116'',''A121'',''A97'',''A117'',''A187'',''A51'') 
			and vid.DÍA = CAST(''#fecha#'' as datetime)   
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 50 as idATA, ''Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)''  as descripcion, ''Av. Blasco Ibáñez'' as descripcion_corta, ''Blasco Ibáñez'' as nombre_corto union
				select 93 as idATA, ''Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)''  as descripcion, ''Av. Dr. Peset Aleixandre'' as descripcion_corta, ''Dr. Peset Aleixandre'' as nombre_corto union
				select 111 as idATA, ''Av. de Giorgeta (Entre Sant Vicent i Jesús)''  as descripcion, ''Av. de Giorgeta'' as descripcion_corta, ''Giorgeta'' as nombre_corto union
				select 112 as idATA, ''Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)''  as descripcion, ''Gran Via de Ferran el Catòlic'' as descripcion_corta, ''Ferran el Catòlic'' as nombre_corto union
				select 116 as idATA, ''Gran Via del Marqués del Túria (Entre Pont d''''Aragó i Hernan Cortés)''  as descripcion, ''Gran Via del Marqués del Túria'' as descripcion_corta, ''Marqués del Túria'' as nombre_corto union
				select 177 as idATA, ''Av. de Peris i Valero (Entre Ausiàs March i Sapadors)''  as descripcion, ''Av. de Peris i Valero'' as descripcion_corta, ''Peris i Valero'' as nombre_corto) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Accesos-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			FROM
			(select afo.Descripcion as descripcion,afo.idATA as idata,  sum(vid.IMD*afo.Coeficiente/100) as intactual,vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''F36'',''F37'',''F38'',''F39'',''F40'',''F41'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)	
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 725 as idATA, ''Navarro Reverter'' as descripcion union
				select 726 as idATA, ''Pont de fusta'' as descripcion union
				select 727 as idATA, ''Pont de les arts'' as descripcion union
				select 728 as idATA, ''Pont del real'' as descripcion union
				select 729 as idATA, ''Carrer Alacant'' as descripcion union
				select 730 as idATA, ''Carrer Russafa'' as descripcion) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			from
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as intactual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo on 	vid.IdTA=afo.IdTA
			where afo.nombre in (''F31'',''F32'',''F33'',''F34'',''F35'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by 	afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join(
				select 720 as idATA, ''Carrer Colon'' as descripcion union
				select 721 as idATA, ''Comte de Trénor – Pont de fusta''  as descripcion union
				select 722 as idATA, ''Guillem de Castro''  as descripcion union
				select 723 as idATA, ''Xàtiva''  as descripcion union
				select 724 as idATA, ''Plaça Tetuan''  	as descripcion) as descripciones
			on actual.IdATA=descripciones.idATA
			', 'tipo,idata,descripcion, descripcion_corta, nombre_corto,int_actual,calculation_period', 'N', 'sftp', NULL, NULL, now(), 'TICKET 2572', now(), 'TICKET 2572', 300),
			('py_trafico_intensidad_validada', 'BBDD', 'SQL_Server_Trafico', NULL, NULL, '/prod/01.fichOrig/VLCI_PY_TRAFICO_VALIDADADO/', '/prod/02.fichBackup/VLCI_PY_TRAFICO_VALIDADADO/', NULL, 'intensidad_trafico_validada_365_#ddMMyyyyHHmmss#', NULL, 'csv', 'intensidad_trafico_validada_365', 
			'SELECT concat(''Kpi-Accesos-Ciudad-Coches'', '';'', actual.idata, '';'', descripciones.descripcion, '';'', descripciones.descripcion_corta, '';'', descripciones.nombre_corto, '';'', actual.int_actual, '';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A373'',''A406'',''A72'',''A59'',''A1'',''A82'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA,vid.DÍA) actual
			inner join (
    			select 4 as idATA, ''Accés Barcelona (entrada i eixida)(Entre V-21 i Rotonda)'' as descripcion, ''Accés Barcelona (entrada i eixida)'' as descripcion_corta, ''Accés Barcelona'' as nombre_corto union
    			select 57 as idATA, ''Accés a Arxiduc Carles pel Camí nou de Picanya (Entre V-30 i Pedrapiquers)''  as descripcion, ''Accés a Arxiduc Carles pel Camí nou de Picanya'' as descripcion_corta, ''Arxiduc Carles'' as nombre_corto union
    			select 70 as idATA, ''Av. del Cid (Entre V-30 i Tres Creus)''  as descripcion, ''Av. del Cid'' as descripcion_corta, ''Av. del Cid'' as nombre_corto union
    			select 78 as idATA, ''Corts Valencianes (Accés per CV-35)(Entre Camp del Túria i La Safor)''  as descripcion, ''Corts Valencianes (Accés per CV-35)'' as descripcion_corta, ''Corts Valencianes'' as nombre_corto union
    			select 349 as idATA, ''Accés per V-31 (Pista de Silla)(Entre Bulevard Sud i V-31)''      as descripcion, ''Accés per V-31 (Pista de Silla)'' as descripcion_corta, ''Pista de Silla'' as nombre_corto union
    			select 379 as idATA, ''Prolongació Joan XXIII (Entre Germans Machado i Salvador Cerveró)''  as descripcion, ''Prolongació Joan XXIII'' as descripcion_corta, ''Joan XXIII'' as nombre_corto) as descripciones
    			on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Accesos-Vias-Coches'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion_corta,'';'', descripciones.nombre_corto,'';'', actual.int_actual,'';'', actual.calculation_period)
			FROM 
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as int_actual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''A116'',''A121'',''A97'',''A117'',''A187'',''A51'') 
			and vid.DÍA = CAST(''#fecha#'' as datetime)   
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 50 as idATA, ''Av. Blasco Ibáñez (Entre Doctor Moliner i Av. Aragó)''  as descripcion, ''Av. Blasco Ibáñez'' as descripcion_corta, ''Blasco Ibáñez'' as nombre_corto union
				select 93 as idATA, ''Av. Dr. Peset Aleixandre (Entre Joan XXIII i Camí de Moncada)''  as descripcion, ''Av. Dr. Peset Aleixandre'' as descripcion_corta, ''Dr. Peset Aleixandre'' as nombre_corto union
				select 111 as idATA, ''Av. de Giorgeta (Entre Sant Vicent i Jesús)''  as descripcion, ''Av. de Giorgeta'' as descripcion_corta, ''Giorgeta'' as nombre_corto union
				select 112 as idATA, ''Gran Via de Ferran el Catòlic (Entre Àngel Guimerà i Passeig de la Petxina)''  as descripcion, ''Gran Via de Ferran el Catòlic'' as descripcion_corta, ''Ferran el Catòlic'' as nombre_corto union
				select 116 as idATA, ''Gran Via del Marqués del Túria (Entre Pont d''''Aragó i Hernan Cortés)''  as descripcion, ''Gran Via del Marqués del Túria'' as descripcion_corta, ''Marqués del Túria'' as nombre_corto union
				select 177 as idATA, ''Av. de Peris i Valero (Entre Ausiàs March i Sapadors)''  as descripcion, ''Av. de Peris i Valero'' as descripcion_corta, ''Peris i Valero'' as nombre_corto) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Accesos-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			FROM
			(select afo.Descripcion as descripcion,afo.idATA as idata,  sum(vid.IMD*afo.Coeficiente/100) as intactual,vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join 	[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
			on vid.IdTA=afo.IdTA
			where afo.nombre in (''F36'',''F37'',''F38'',''F39'',''F40'',''F41'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join (
				select 725 as idATA, ''Navarro Reverter'' as descripcion union
				select 726 as idATA, ''Pont de fusta'' as descripcion union
				select 727 as idATA, ''Pont de les arts'' as descripcion union
				select 728 as idATA, ''Pont del real'' as descripcion union
				select 729 as idATA, ''Carrer Alacant'' as descripcion union
				select 730 as idATA, ''Carrer Russafa'' as descripcion) as descripciones
				on actual.IdATA=descripciones.idATA
			UNION 
			SELECT concat(''Kpi-Trafico-Bicicletas-Anillo'','';'', actual.idata,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', descripciones.descripcion,'';'', actual.intactual,'';'',  actual.calculation_period)
			from
			(select afo.Descripcion as descripcion,afo.idATA as idata, sum(vid.IMD*afo.Coeficiente/100) as intactual, vid.DÍA as calculation_period
			from [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - BICI] vid
			inner join [WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo on 	vid.IdTA=afo.IdTA
			where afo.nombre in (''F31'',''F32'',''F33'',''F34'',''F35'')
			and vid.DÍA = CAST(''#fecha#'' as datetime)
			and afo.idATA NOT IN (
				select distinct afo.idATA
				FROM [WEB_TRAFICO].[dbo].[Datos_Diarios TrAfos - TRAFICO] vid
				INNER JOIN 
					[WEB_TRAFICO].[dbo].[Datos_Estructurales_Aforado_Simplificado] afo 
				ON vid.IdTA = afo.IdTA
				WHERE vid.DiaAnomalo IS NOT NULL
				AND vid.DÍA = CAST(''#fecha#'' AS datetime)
			)
			group by 	afo.Descripcion,afo.idATA, vid.DÍA) actual
			inner join(
				select 720 as idATA, ''Carrer Colon'' as descripcion union
				select 721 as idATA, ''Comte de Trénor – Pont de fusta''  as descripcion union
				select 722 as idATA, ''Guillem de Castro''  as descripcion union
				select 723 as idATA, ''Xàtiva''  as descripcion union
				select 724 as idATA, ''Plaça Tetuan''  	as descripcion) as descripciones
			on actual.IdATA=descripciones.idATA
			', 'tipo,idata,descripcion, descripcion_corta, nombre_corto,int_actual,calculation_period', 'N', 'sftp', NULL, NULL, now(), 'TICKET 2572', now(), 'TICKET 2572', 301);
		INSERT INTO t_d_parametros_etl_carga (nombre_etl,tipo,sistema_origen,patron_fichero_origen,ruta_origen,ruta_destino,ruta_backup,url,filename,borrar_origen,formato,nombre_extraccion,query_extraccion,cabecera,obligatorio,destino,ruta_destino_hdfs,ruta_destino_backup_hdfs,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) 
		VALUES ('py_personal_ayuntamiento','FTP','FTP Sertic','*.CSV','//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi/Personal','/prod/01.fichOrig/VLCI_PERSONAL_AYUNTAMIENTO/','/prod/02.fichBackup/VLCI_PERSONAL_AYUNTAMIENTO/',NULL,'*.CSV','N','CSV',NULL,NULL,NULL,'N','sftp',NULL,NULL,NOW(),'Ticket 3225',NULL,NULL);

	
	END IF;

	----------------------------------------------------
	-- Updates that are not environment specific
	----------------------------------------------------
	
	-- Migración GIS C1 - Servicio Movilidad
	update vlci2.t_d_parametros_etl_carga
	set query_extraccion = 'SELECT concat(2,'';'',(sum(st_length(the_geom))/1000),'';'',''sum_carril_bici'')  	FROM gis.tra_carril_bici_evw  UNION ALL  SELECT concat(3,'';'',count(*),'';'',''total_reguladores'')  	FROM gis.tra_reguladores_evw  UNION ALL  SELECT concat(4,'';'',count(*),'';'',''total_reguladores_led'')  	FROM gis.tra_reguladores_evw  	WHERE optica = ''Led''  UNION ALL  SELECT concat(5,'';'',count(*),'';'',''total_paneles'')      FROM gis.tra_paneles_evw  UNION ALL  SELECT concat(6,'';'',count(*),'';'',''total_camaras_web'')      FROM gis.tra_camaras_web_evw  UNION ALL  SELECT concat(9,'';'',count(*),'';'',''total_aparcamotos'')      FROM gis.tra_aparca_motos_evw  UNION ALL  SELECT concat(11,'';'',count(*),'';'',''total_cargaydescarga'')      FROM gis.tra_cargaydescarga_evw  UNION ALL  SELECT concat(12,'';'',count(*),'';'',''total_paradas_taxis'')      FROM gis.tra_paradas_taxis_evw  UNION ALL  SELECT concat(13,'';'',(sum(st_length(shape))/1000),'';'',''sum_ejes_calle'')      FROM gis.ejes_calle_evw  UNION ALL  SELECT concat(15,'';'',count(*),'';'',''total_carril_bici'')      FROM gis.tra_carril_bici_evw  UNION ALL  SELECT concat(16,'';'',sum(numplazas),'';'',''sum_bici_aparcamiento'')      FROM gis.tra_bici_aparcamiento_evw  UNION ALL  SELECT concat(17,'';'',sum(numplazas),'';'',''sum_minusvalidos'')      FROM gis.tra_minusvalidos_evw  UNION ALL  SELECT concat(18,'';'',count(*),'';'',''total_aparcamientos'')      FROM gis.tra_aparcamientos_evw      WHERE tipo=0;'
	where nombre_etl = 'VLCI_INTEGRACION_TRAFICO_GIS';

	-- Migración GIS C1 - Servicio Movilidad
	-- Adaptamos la query al nuevo nombre de la vista que le ha dado Eloy/su equipo
	update vlci2.t_d_parametros_etl_carga
	set query_extraccion = 'SELECT concat(2,'';'',(sum(st_length(the_geom))/1000),'';'',''sum_carril_bici'')  	FROM gis.tra_carril_bici_evw  UNION ALL  SELECT concat(3,'';'',count(*),'';'',''total_reguladores'')  	FROM gis.tra_reguladores_evw  UNION ALL  SELECT concat(4,'';'',count(*),'';'',''total_reguladores_led'')  	FROM gis.tra_reguladores_evw  	WHERE optica = ''Led''  UNION ALL  SELECT concat(5,'';'',count(*),'';'',''total_paneles'')      FROM gis.tra_paneles_evw  UNION ALL  SELECT concat(6,'';'',count(*),'';'',''total_camaras_web'')      FROM gis.tra_camaras_evw  UNION ALL  SELECT concat(9,'';'',count(*),'';'',''total_aparcamotos'')      FROM gis.tra_aparca_motos_evw  UNION ALL  SELECT concat(11,'';'',count(*),'';'',''total_cargaydescarga'')      FROM gis.tra_cargaydescarga_evw  UNION ALL  SELECT concat(12,'';'',count(*),'';'',''total_paradas_taxis'')      FROM gis.tra_paradas_taxis_evw  UNION ALL  SELECT concat(13,'';'',(sum(st_length(shape))/1000),'';'',''sum_ejes_calle'')      FROM gis.ejes_calle_evw  UNION ALL  SELECT concat(15,'';'',count(*),'';'',''total_carril_bici'')      FROM gis.tra_carril_bici_evw  UNION ALL  SELECT concat(16,'';'',sum(numplazas),'';'',''sum_bici_aparcamiento'')      FROM gis.tra_bici_aparcamiento_evw  UNION ALL  SELECT concat(17,'';'',sum(numplazas),'';'',''sum_minusvalidos'')      FROM gis.tra_minusvalidos_evw  UNION ALL  SELECT concat(18,'';'',count(*),'';'',''total_aparcamientos'')      FROM gis.tra_aparcamientos_evw      WHERE tipo=0;'
	where nombre_etl = 'VLCI_INTEGRACION_TRAFICO_GIS';

	-- GACO y TBOR - Cambio de Ruta de origen de la ETL Loader.
	-- Debido al cambio de sistema de origen SIEM a SAP, ahora la ruta de origen de los ficheros es diferente
	UPDATE vlci2.t_d_parametros_etl_carga
	SET ruta_origen='//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi',
	patron_fichero_origen='SMC*GACO*',
	formato=''
	WHERE nombre_etl='CDMGE GACO' AND sistema_origen='FTP Sertic' AND patron_fichero_origen='SMC-GACO-*';

	UPDATE vlci2.t_d_parametros_etl_carga
	SET ruta_origen='//dades2.aytoval.es/dades2/ayun/SAP/IntegracionesVLCi',
	patron_fichero_origen='SMC*TBOR*',
	formato=''
	WHERE nombre_etl='CDMGE Codigo Organico' AND sistema_origen='FTP Sertic' AND patron_fichero_origen='SMC-TBOR-*';
END
$$ LANGUAGE PLPGSQL;