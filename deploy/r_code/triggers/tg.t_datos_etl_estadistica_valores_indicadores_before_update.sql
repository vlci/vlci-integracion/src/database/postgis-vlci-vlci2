-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_estadistica_valores_indicadores_before_update to pg

BEGIN;

create trigger t_datos_etl_estadistica_valores_indicadores_before_update before
insert
    on
    vlci2.t_datos_etl_estadistica_valores_indicadores for each row execute procedure fn_t_datos_etl_estadistica_valores_indicadores_before_update();

COMMIT;
