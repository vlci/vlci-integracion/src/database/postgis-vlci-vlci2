-- Deploy postgis-vlci-vlci2:r_code/triggers/1753.crear-triggers-tablas-bdo-arch to pg

BEGIN;

-----t_d_servicio_arch
create trigger t_d_servicio_arch_before_insert before
insert
    on
    vlci2.t_d_servicio_arch for each row execute function fn_aud_fec_ins();

create trigger t_d_servicio_arch_before_update before
update
    on
    vlci2.t_d_servicio_arch for each row execute function fn_aud_fec_upd();

-----t_d_servicio_arch------
create trigger t_d_delegacion_arch_before_insert before
insert
    on
    vlci2.t_d_delegacion_arch for each row execute function fn_aud_fec_ins();

create trigger t_d_delegacion_arch_before_update before
update
    on
    vlci2.t_d_delegacion_arch for each row execute function fn_aud_fec_upd();

----t_d_area_arc-----
create trigger t_d_area_arch_before_insert before
insert
    on
    vlci2.t_d_area_arch for each row execute function fn_aud_fec_ins();

create trigger t_d_area_arch_before_update before
update
    on
    vlci2.t_d_area_arch for each row execute function fn_aud_fec_upd();


COMMIT;
