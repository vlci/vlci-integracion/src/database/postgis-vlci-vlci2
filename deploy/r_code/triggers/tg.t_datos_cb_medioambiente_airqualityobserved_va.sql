-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_cb_medioambiente_airqualityobserved_va to pg

BEGIN;

create trigger t_datos_cb_medioambiente_airqualityobserved_va_before_insert before
insert
    on
    vlci2.t_datos_cb_medioambiente_airqualityobserved_va for each row execute function fn_t_datos_cb_medioambiente_airqualityobserved_before_insert();

COMMIT;
