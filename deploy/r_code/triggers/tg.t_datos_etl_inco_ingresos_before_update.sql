-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_inco_ingresos_before_update to pg

BEGIN;

create trigger t_datos_etl_inco_ingresos_update before
update
    on
    vlci2.t_datos_etl_inco_ingresos for each row execute function fn_aud_fec_upd_etl();

COMMIT;
