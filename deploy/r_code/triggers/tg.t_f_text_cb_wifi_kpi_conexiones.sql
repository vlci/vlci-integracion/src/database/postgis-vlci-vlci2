-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_f_text_cb_wifi_kpi_conexiones to pg

BEGIN;

create trigger insert_wifi_kpi_to_agregados before
insert
    on
    vlci2.t_f_text_cb_wifi_kpi_conexiones for each row execute procedure vlci2.update_wifi_kpi_agregados();

COMMIT;
