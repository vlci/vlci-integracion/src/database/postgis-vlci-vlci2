-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_contratos_menores_bdo to pg

BEGIN;

create trigger t_datos_etl_contratos_menores_bdo_before_insert before
insert
    on
    vlci2.t_datos_etl_contratos_menores_bdo for each row execute function fn_aud_fec_ins();

create trigger t_datos_etl_contratos_menores_bdo_update before
update
    on
    vlci2.t_datos_etl_contratos_menores_bdo for each row execute function fn_aud_fec_upd();

COMMIT;
