-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_api_loader_before_update to pg

BEGIN;

create trigger t_datos_api_loader_campos_request_before_update before
update on vlci2.t_d_api_loader_campos_request for each row execute procedure vlci2.fn_aud_fec_upd();

COMMIT;
