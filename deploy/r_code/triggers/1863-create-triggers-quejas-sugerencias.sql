-- Deploy postgis-vlci-vlci2:r_code/triggers/1863-create-triggers-quejas-sugerencias to pg

BEGIN;

create trigger t_datos_etl_quejas_sugerencias_before_insert before
insert
    on
    vlci2.t_datos_etl_quejas_sugerencias for each row execute function fn_aud_fec_ins();
   
create trigger t_datos_etl_quejas_sugerencias_update before
update
    on
    vlci2.t_datos_etl_quejas_sugerencias for each row execute function fn_aud_fec_upd();
    
COMMIT;
