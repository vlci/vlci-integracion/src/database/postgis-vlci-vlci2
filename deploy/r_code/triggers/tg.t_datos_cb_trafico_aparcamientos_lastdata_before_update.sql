-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_cb_trafico_aparcamientos_lastdata_before_update to pg

BEGIN;

create trigger t_datos_cb_trafico_aparcamientos_lastdata_before_update before
update
    on
    vlci2.t_datos_cb_trafico_aparcamientos_lastdata for each row execute procedure fn_aud_fec_upd();


COMMIT;
