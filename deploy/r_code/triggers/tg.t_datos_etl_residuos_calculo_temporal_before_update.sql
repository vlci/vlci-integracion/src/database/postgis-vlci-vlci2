-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_residuos_calculo_temporal_before_update to pg

BEGIN;

create trigger t_datos_etl_residuos_calculo_temporal_before_update before
update
    on
    vlci2.t_datos_etl_residuos_calculo_temporal for each row execute function vlci2.fn_aud_fec_upd();
COMMIT;
