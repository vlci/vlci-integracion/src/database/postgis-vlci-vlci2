-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_contratos_menores_before_insert to pg

BEGIN;

create trigger t_datos_etl_contratos_menores_insert before
insert
    on
    vlci2.t_datos_etl_contratos_menores for each row execute function fn_aud_fec_ins_etl();


COMMIT;
