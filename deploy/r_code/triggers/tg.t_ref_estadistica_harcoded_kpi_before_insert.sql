-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_ref_estadistica_harcoded_kpi_before_insert to pg

BEGIN;

create trigger t_ref_estadistica_harcoded_kpi_before_insert before
insert
    on
    vlci2.t_ref_estadistica_harcoded_kpi for each row execute procedure fn_t_ref_estadistica_harcoded_kpi_before_insert();

COMMIT;
