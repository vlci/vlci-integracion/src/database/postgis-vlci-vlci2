-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_etl_contratos_menores_before_update to pg

BEGIN;

create trigger t_datos_etl_contratos_menores_update before
update
    on
    vlci2.t_datos_etl_contratos_menores for each row execute function fn_aud_fec_upd_etl();


COMMIT;
