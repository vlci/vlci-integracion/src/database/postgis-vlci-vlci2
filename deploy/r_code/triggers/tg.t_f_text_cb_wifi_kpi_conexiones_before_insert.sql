-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_f_text_cb_wifi_kpi_conexiones_before_insert to pg

BEGIN;

create trigger t_f_text_cb_wifi_kpi_conexiones_before_insert before
insert on vlci2.t_f_text_cb_wifi_kpi_conexiones for each row execute procedure vlci2.fn_t_f_text_cb_wifi_kpi_conexiones_before_insert();



COMMIT;
