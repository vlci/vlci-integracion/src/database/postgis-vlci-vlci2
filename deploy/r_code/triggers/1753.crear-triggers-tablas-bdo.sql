-- Deploy postgis-vlci-vlci2:r_code/triggers/1753.crear-triggers-tablas-bdo to pg

BEGIN;

----- t_d_servicio -----
create trigger t_d_servicio_before_insert before
insert
    on
    vlci2.t_d_servicio for each row execute function fn_aud_fec_ins();

create trigger t_d_servicio_before_update before
update
    on
    vlci2.t_d_servicio for each row execute function fn_aud_fec_upd();

----- t_d_delegacion -----
create trigger t_d_delegacion_before_insert before
insert
    on
    vlci2.t_d_delegacion for each row execute function fn_aud_fec_ins();

create trigger t_d_delegacion_before_update before
update
    on
    vlci2.t_d_delegacion for each row execute function fn_aud_fec_upd();

----- t_d_area -----
create trigger t_d_area_before_insert before
insert
    on
    vlci2.t_d_area for each row execute function fn_aud_fec_ins();

create trigger t_d_area_before_update before
update
    on
    vlci2.t_d_area for each row execute function fn_aud_fec_upd();

COMMIT;
