-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_ref_etl_inco_capitulos_before_insert to pg

BEGIN;

create trigger t_ref_etl_inco_capitulos_insert before
insert
    on
    vlci2.t_ref_etl_inco_capitulos for each row execute function fn_aud_fec_ins_etl();
   

COMMIT;
