-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_ref_etl_inco_capitulos_before_update to pg

BEGIN;

create trigger t_ref_etl_inco_capitulos_update before
update
    on
    vlci2.t_ref_etl_inco_capitulos for each row execute function fn_aud_fec_upd_etl();


COMMIT;
