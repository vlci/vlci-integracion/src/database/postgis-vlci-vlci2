-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_cb_trafico_aparcamientos_before_insert to pg

BEGIN;

create trigger t_datos_cb_trafico_aparcamientos_before_insert before
insert
    on
    vlci2.t_datos_cb_trafico_aparcamientos for each row execute function fn_aud_fec_ins();

COMMIT;
