-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_datos_cb_trafico_aparcamientos_before_update to pg

BEGIN;

create trigger t_datos_cb_trafico_aparcamientos_before_update before
    update
        on
        vlci2.t_datos_cb_trafico_aparcamientos for each row execute procedure vlci2.fn_aud_fec_upd();


COMMIT;
