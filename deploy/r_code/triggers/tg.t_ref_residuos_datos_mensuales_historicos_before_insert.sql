-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_ref_residuos_datos_mensuales_historicos_before_insert to pg
-- requires: v_migrations/1619.residuos-cargar-datos-historicos

BEGIN;

    create trigger t_ref_residuos_datos_mensuales_historicos_before_insert before
    insert
        on
        vlci2.t_ref_residuos_datos_mensuales_historicos for each row execute procedure vlci2.fn_aud_fec_ins();

COMMIT;
