-- Deploy postgis-vlci-vlci2:r_code/triggers/tg.t_ref_etl_quejas_sugerencias_mapeo_temas to pg

BEGIN;

create trigger t_ref_etl_quejas_sugerencias_mapeo_temas_before_insert before
insert
    on
    vlci2.t_ref_etl_quejas_sugerencias_mapeo_temas for each row execute function fn_aud_fec_ins();
   
create trigger t_ref_etl_quejas_sugerencias_mapeo_temas_before_update before
update
    on
    vlci2.t_ref_etl_quejas_sugerencias_mapeo_temas for each row execute function fn_aud_fec_upd();
COMMIT;
