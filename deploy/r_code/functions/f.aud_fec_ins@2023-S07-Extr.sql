-- Deploy postgis-vlci-vlci2:r_code/functions/f.aud_fec_ins to pg

BEGIN;

CREATE OR REPLACE FUNCTION vlci2.fn_aud_fec_ins()
RETURNS trigger
LANGUAGE plpgsql
AS $function$
BEGIN

       IF(NEW.aud_fec_ins is null) THEN
         NEW.aud_fec_ins=CURRENT_TIMESTAMP;
        END IF;

   RETURN NEW;
END;
$function$
;

COMMIT;
