-- Deploy postgis-vlci-vlci2:r_code/functions/3108-p-gf-averiguarejecucion to pg

BEGIN;

CREATE OR REPLACE FUNCTION vlci2.p_gf_averiguarejecucion(
    in_id integer, 
    OUT out_result boolean, 
    OUT out_status character varying
)
RETURNS record
LANGUAGE plpgsql
AS $function$
DECLARE
    v_control VARCHAR(2);
    v_count INTEGER;
    v_fen TIMESTAMP;
    v_fen_inicio TIMESTAMP;
    v_fen_fin TIMESTAMP;
    v_periodicidad VARCHAR(200);
    v_desfase INTEGER;
    v_estado VARCHAR(200);
    v_nombre_etl VARCHAR(200);
    v_max_fen TIMESTAMP;
    v_anyo_mes_max_fen VARCHAR(200);
    v_anyo_mes_fen_fin VARCHAR(200);

BEGIN
    -- Assign initial value to control variable
    v_control := 'OK';
    OUT_RESULT := FALSE;
    
    -- Check if there is an ETL with the given ID
    v_count := (SELECT COUNT(*) FROM t_d_fecha_negocio_etls WHERE etl_id = in_id);
    
    IF v_count = 0 THEN
        v_control := 'KO';
        OUT_STATUS := 'OK';
    END IF;
    
    IF v_control = 'OK' THEN
        -- Retrieve values from the table
        SELECT fen, fen_inicio, fen_fin, periodicidad, "offset", estado, etl_nombre 
        INTO v_fen, v_fen_inicio, v_fen_fin, v_periodicidad, v_desfase, v_estado, v_nombre_etl 
        FROM t_d_fecha_negocio_etls 
        WHERE etl_id = in_id;
        
        -- Evaluate state
        IF v_estado = 'EN PROCESO' THEN
            v_control := 'KO';
            OUT_RESULT := FALSE;
            OUT_STATUS := CONCAT('El Estado de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' está en Proceso.');

        ELSEIF v_estado NOT IN ('ERROR', 'OK') THEN
            v_control := 'KO';
            OUT_RESULT := FALSE;
            OUT_STATUS := CONCAT('El campo Estado de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene un valor no válido.');
        END IF;
    END IF;
    
    -- Evaluate periodicity
    IF v_control = 'OK' THEN
        IF v_periodicidad NOT IN ('DIEZMINUTAL', 'HORARIA', 'DIARIA', 'TRIMESTRAL', 'SEMESTRAL', 'ANUAL', 'MENSUAL') THEN
            v_control := 'KO';
            OUT_RESULT := TRUE;
            OUT_STATUS := 'OK';
        END IF;
    END IF;
    
    IF v_control = 'OK' THEN
        -- Periodicity: DIEZMINUTAL
        IF v_periodicidad = 'DIEZMINUTAL' THEN
            OUT_STATUS := 'OK';
            v_max_fen := (SELECT to_char(current_timestamp - CAST(concat(v_desfase * 10, ' MINUTE') AS INTERVAL), 'YYYY-MM-dd HH24:mi:00'));
            
            IF v_fen < v_max_fen THEN
                OUT_RESULT := TRUE;
            ELSEIF v_fen >= v_max_fen THEN
                OUT_RESULT := FALSE;
            ELSEIF v_fen IS NULL THEN
                v_control := 'KO';
                OUT_RESULT := FALSE;
                OUT_STATUS := CONCAT('El campo FEN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            END IF;

        -- Periodicity: HORARIA
        ELSEIF v_periodicidad = 'HORARIA' THEN
            OUT_STATUS := 'OK';
            v_max_fen := (SELECT to_char(current_timestamp - CAST(concat(v_desfase, ' HOUR') AS INTERVAL), 'YYYY-MM-dd HH24:mi:00'));
            
            IF v_fen < v_max_fen THEN
                OUT_RESULT := TRUE;
            ELSEIF v_fen >= v_max_fen THEN
                OUT_RESULT := FALSE;
            ELSEIF v_fen IS NULL THEN
                v_control := 'KO';
                OUT_RESULT := FALSE;
                OUT_STATUS := CONCAT('El campo FEN de la ETL "', v_nombre_etl, '" con ID: ', in_id, ' tiene valor nulo.');
            END IF;

        -- Periodicity: DIARIA
        ELSEIF v_periodicidad = 'DIARIA' THEN
            OUT_STATUS := 'OK';
            v_max_fen := (SELECT CURRENT_DATE - CAST(concat(v_desfase, ' day') AS INTERVAL));
            
            IF v_fen < v_max_fen THEN
                OUT_RESULT := TRUE;
            ELSEIF v_fen >= v_max_fen THEN
                OUT_RESULT := FALSE;
            ELSEIF v_fen IS NULL THEN
                v_control := 'KO';
                OUT_RESULT := FALSE;
                OUT_STATUS := CONCAT('El campo FEN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            END IF;

        -- Periodicity: TRIMESTRAL
        ELSEIF v_periodicidad = 'TRIMESTRAL' THEN
            IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
                v_control := 'KO';
                OUT_RESULT := FALSE;
                OUT_STATUS := CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            ELSE
                OUT_STATUS := 'OK';
                v_max_fen := (SELECT CURRENT_DATE - CAST(concat(v_desfase, ' day') AS INTERVAL));
                v_anyo_mes_max_fen := (SELECT to_char(v_max_fen, 'YYYYMM'));
                v_anyo_mes_fen_fin := (SELECT to_char(v_fen_fin, 'YYYYMM'));
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
                    OUT_RESULT := TRUE;
                ELSE
                    OUT_RESULT := FALSE;
                END IF;
            END IF;

        -- Periodicity: SEMESTRAL
        ELSEIF v_periodicidad = 'SEMESTRAL' THEN
            IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
                v_control := 'KO';
                OUT_RESULT := FALSE;
                OUT_STATUS := CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            ELSE
                OUT_STATUS := 'OK';
                v_max_fen := (SELECT CURRENT_DATE - CAST(concat(v_desfase, ' day') AS INTERVAL));
                v_anyo_mes_max_fen := (SELECT to_char(v_max_fen, 'YYYYMM'));
                v_anyo_mes_fen_fin := (SELECT to_char(v_fen_fin, 'YYYYMM'));
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
                    OUT_RESULT := TRUE;
                ELSE
                    OUT_RESULT := FALSE;
                END IF;
            END IF;

        -- Periodicity: ANUAL
        ELSEIF v_periodicidad = 'ANUAL' THEN
            IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
                v_control := 'KO';
                OUT_RESULT := FALSE;
                OUT_STATUS := CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            ELSE
                OUT_STATUS := 'OK';
                v_max_fen := (SELECT CURRENT_DATE - CAST(concat(v_desfase, ' day') AS INTERVAL));
                v_anyo_mes_max_fen := (SELECT to_char(v_max_fen, 'YYYYMM'));
                v_anyo_mes_fen_fin := (SELECT to_char(v_fen_fin, 'YYYYMM'));
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
                    OUT_RESULT := TRUE;
                ELSE
                    OUT_RESULT := FALSE;
                END IF;
            END IF;

        -- Periodicity: MENSUAL
        ELSEIF v_periodicidad = 'MENSUAL' THEN
            OUT_STATUS := 'OK';
            v_max_fen := (SELECT CURRENT_DATE - INTERVAL '1 month' * v_desfase);
            
            IF v_fen < v_max_fen THEN
                OUT_RESULT := TRUE;
            ELSEIF v_fen >= v_max_fen THEN
                OUT_RESULT := FALSE;
            ELSEIF v_fen IS NULL THEN
                v_control := 'KO';
                OUT_STATUS := CONCAT('El campo FEN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            END IF;
        END IF;
    END IF;
END;
$function$;


COMMIT;
