-- Deploy postgis-vlci-vlci2:r_code/functions/3108-p-gf-actualizarFecha to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2."p_gf_actualizarFecha"(
    IN in_id integer, 
    IN in_campo character varying, 
    IN in_fecha timestamp without time zone, 
    INOUT out_status character varying
)
LANGUAGE plpgsql
SECURITY DEFINER
AS $procedure$
DECLARE
    v_control VARCHAR(2);
    v_count INT;
    v_fen TIMESTAMP;
    v_fen_inicio TIMESTAMP;
    v_fen_fin TIMESTAMP;
    v_periodicidad VARCHAR(200);
    v_desfase int;
    v_estado VARCHAR(200);
    v_nombre_etl VARCHAR(200);
BEGIN
    v_control := 'OK';

    -- Check if the ETL exists for the given ID
    v_count := (SELECT COUNT(*) FROM t_d_fecha_negocio_etls WHERE etl_id = in_id);

    IF v_count = 0 THEN
        v_control := 'KO';
        out_status := CONCAT('No se encuentra ninguna ETL con el id: ', in_id);
    END IF;

    -- Validate the field name
    IF v_control = 'OK' THEN
        IF in_campo NOT IN ('fen', 'fen_inicio', 'fen_fin') THEN
            v_control := 'KO';
            out_status := CONCAT('El valor ', in_campo, ' para el parámetro de entrada in_campo no contiene un valor válido. Valores permitidos: "fen", "fen_inicio", "fen_fin".');
        END IF;
    END IF;

    -- Fetch ETL details
    IF v_control = 'OK' THEN
        SELECT fen, fen_inicio, fen_fin, periodicidad, "offset", estado, etl_nombre 
        INTO v_fen, v_fen_inicio, v_fen_fin, v_periodicidad, v_desfase, v_estado, v_nombre_etl 
        FROM t_d_fecha_negocio_etls 
        WHERE etl_id = in_id;
    END IF;

    -- Validate periodicity
    IF v_control = 'OK' THEN
        IF v_periodicidad NOT IN ('DIEZMINUTAL', 'HORARIA', 'DIARIA', 'TRIMESTRAL', 'SEMESTRAL', 'ANUAL', 'MENSUAL') THEN
            v_control := 'KO';
            out_status := CONCAT('La periodicidad de la ETL "', v_nombre_etl, '" con ID: ', in_id, ' es "', v_periodicidad, '". La versión actual del procedimiento solo permite cálculos sobre indicadores Diezminutales, Horarios, Diarios, Trimestrales, Semestrales, Anuales o Mensuales.');
        END IF;
    END IF;

    -- Update the dates based on periodicity
    IF v_control = 'OK' THEN
        IF (v_periodicidad IN ('DIEZMINUTAL', 'HORARIA', 'DIARIA', 'MENSUAL') AND in_campo = 'fen' AND in_fecha IS NOT NULL) THEN
            out_status := 'OK';
            UPDATE t_d_fecha_negocio_etls 
            SET fen = in_fecha, aud_fec_upd = NOW(), aud_user_upd = 'p_gf_actualizarFecha' 
            WHERE etl_id = in_id;

        ELSEIF (v_periodicidad IN ('TRIMESTRAL', 'SEMESTRAL', 'ANUAL') AND in_campo = 'fen_inicio' AND in_fecha IS NOT NULL) THEN
            out_status := 'OK';
            UPDATE t_d_fecha_negocio_etls 
            SET fen_inicio = in_fecha, aud_fec_upd = NOW(), aud_user_upd = 'p_gf_actualizarFecha' 
            WHERE etl_id = in_id;

        ELSEIF (v_periodicidad IN ('TRIMESTRAL', 'SEMESTRAL', 'ANUAL') AND in_campo = 'fen_fin' AND in_fecha IS NOT NULL) THEN
            out_status := 'OK';
            UPDATE t_d_fecha_negocio_etls 
            SET fen_fin = in_fecha, aud_fec_upd = NOW(), aud_user_upd = 'p_gf_actualizarFecha' 
            WHERE etl_id = in_id;

        ELSEIF in_fecha IS NULL THEN
            v_control := 'KO';
            out_status := 'El campo de entrada FECHA no puede ser nulo.';

        ELSE
            v_control := 'KO';
            out_status := CONCAT('El campo ', in_campo, ' no es el utilizado para gestionar fechas con periodicidad ', v_periodicidad, '. Utiliza el campo adecuado.');
        END IF;
    END IF;
END;
$procedure$;

COMMIT;
