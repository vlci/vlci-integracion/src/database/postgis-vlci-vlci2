-- Deploy postgis-vlci-vlci2:r_code/functions/f.t_datos_cb_medioambiente_airqualityobserved_before_insert to pg

BEGIN;

CREATE OR REPLACE FUNCTION fn_t_datos_cb_medioambiente_airqualityobserved_before_insert()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
    IF new.operationalstatus IS NULL THEN
        NEW.operationalstatus ='ok';
    END IF;
    RETURN NEW;
END;
$function$
;

COMMIT;
