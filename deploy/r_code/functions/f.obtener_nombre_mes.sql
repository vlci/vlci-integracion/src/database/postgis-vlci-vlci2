-- Deploy postgis-vlci-vlci2:r_code/functions/f.obtener_nombre_mes to pg

BEGIN;

CREATE OR REPLACE FUNCTION fn_obtener_nombre_mes(codigo_mes character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
BEGIN
    CASE codigo_mes
        WHEN '01' THEN RETURN 'Ene';
        WHEN '02' THEN RETURN 'Feb';
        WHEN '03' THEN RETURN 'Mar';
        WHEN '04' THEN RETURN 'Abr';
        WHEN '05' THEN RETURN 'May';
        WHEN '06' THEN RETURN 'Jun';
        WHEN '07' THEN RETURN 'Jul';
        WHEN '08' THEN RETURN 'Ago';
        WHEN '09' THEN RETURN 'Sep';
        WHEN '10' THEN RETURN 'Oct';
        WHEN '11' THEN RETURN 'Nov';
        WHEN '12' THEN RETURN 'Dic';
        ELSE RETURN codigo_mes;
    END CASE;
END;
$function$
;

COMMIT;