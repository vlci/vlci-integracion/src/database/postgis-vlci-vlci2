-- Deploy postgis-vlci-vlci2:r_code/functions/f.t_f_text_cb_wifi_kpi_conexiones_before_insert to pg

BEGIN;

CREATE OR REPLACE FUNCTION fn_t_f_text_cb_wifi_kpi_conexiones_before_insert()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
    IF new.sliceanddicevalue1 IS NULL THEN
        NEW.sliceanddicevalue1 ='N/D';
    END IF;
    IF new.sliceanddicevalue2 IS NULL THEN
        NEW.sliceanddicevalue2 ='N/D';
    END IF;
    RETURN NEW;
END;
$function$
;

COMMIT;
