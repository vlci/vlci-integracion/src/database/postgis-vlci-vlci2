-- Deploy postgis-vlci-vlci2:r_code/functions/f.t_datos_etl_estadistica_log_before_update to pg

BEGIN;

CREATE OR REPLACE FUNCTION fn_t_datos_etl_estadistica_log_before_update()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN

	NEW.aud_fec_ins=CURRENT_TIMESTAMP;
    RETURN NEW;
END;
$function$
;

COMMIT;
