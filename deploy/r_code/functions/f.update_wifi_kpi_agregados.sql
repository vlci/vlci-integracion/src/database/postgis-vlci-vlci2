-- Deploy postgis-vlci-vlci2:r_code/functions/f.update_wifi_kpi_agregados to pg

BEGIN;

CREATE OR REPLACE FUNCTION vlci2.update_wifi_kpi_agregados()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
begin
	IF new.entityId in ('IS.OCI.003','IS.OCI.004','IS.OCI.005','IS.OCI.007') THEN
	
		insert into vlci2.t_f_raw_from_context_broker (recvtime,fiwareservicepath,entityid,entitytype,name,description,measureUnit,calculationFrequency,kpiValue,calculationPeriod,neighborhoodId,updatedAt,location,internationalStandard,kpiNumeratorValue,kpiDenominatorValue,sliceAndDiceValue1,sliceAndDiceValue2,sliceAndDiceValue3,sliceAndDiceValue4)
		values(
			new.recvtime,
			new.fiwareservicepath,
			new.entityid,
			new.entitytype,
			new.name,
			new.description,
			new.measureUnit,
			new.calculationFrequency,
			new.kpiValue,
			new.calculationPeriod,
			'999',
			new.updatedAt,
			'{}',
			new.internationalStandard,
			null,
			null,
			new.sliceAndDiceValue1,
			new.sliceAndDiceValue2,
			new.sliceAndDiceValue3,
			new.sliceAndDiceValue4
		);
		
	   
	   
   END IF;
   
   RETURN NEW;
END;
$function$
;

COMMIT;
