-- Deploy postgis-vlci-vlci2:r_code/functions/f.aud_fec_ins to pg

BEGIN;

  CREATE OR REPLACE FUNCTION vlci2.fn_aud_fec_ins()
  RETURNS trigger
  LANGUAGE plpgsql
  AS $function$
  BEGIN
      NEW.aud_fec_ins = CURRENT_TIMESTAMP;
      NEW.aud_fec_upd = CURRENT_TIMESTAMP;

      if NEW.aud_user_ins is null  then 
          NEW.aud_user_ins = 'CYGNUS';
          NEW.aud_user_upd = 'CYGNUS';
      end if;

      RETURN NEW;
  END;
  $function$
  ;

COMMIT;
