-- Deploy postgis-vlci-vlci2:r_code/functions/3108-p-gf-calcularnuevafecha to pg

BEGIN;

CREATE OR REPLACE FUNCTION vlci2.p_gf_calcularnuevafecha(
    in_id integer, 
    OUT out_status character varying
)
RETURNS character varying
LANGUAGE plpgsql
AS $function$
DECLARE
    v_control VARCHAR(2);
    v_count INTEGER;
    v_nombre_etl VARCHAR(200);
    v_fen TIMESTAMP;
    v_fen_inicio TIMESTAMP;
    v_fen_fin TIMESTAMP;
    v_periodicidad VARCHAR(200);
    v_desfase INTEGER;
    v_estado VARCHAR(200);
    v_max_fen TIMESTAMP;
    v_anyo_mes_max_fen VARCHAR(200);
    v_anyo_mes_fen_fin VARCHAR(200);
    v_mes_fen_fin VARCHAR(200);
    v_nueva_fen_inicio VARCHAR(200);
    v_nueva_fen_fin VARCHAR(200);

BEGIN
    v_control := 'OK';
	
    -- Check if an ETL with the input ID exists
    v_count := (SELECT COUNT(*) FROM t_d_fecha_negocio_etls WHERE etl_id = in_id);
    
    IF v_count = 0 THEN
        v_control := 'KO';
        out_status := CONCAT('No se encuentra ninguna ETL con el id: ', in_id);
    END IF;
    
    IF v_control = 'OK' THEN
        -- Retrieve values from the table
        SELECT fen, fen_inicio, fen_fin, periodicidad, "offset", estado, etl_nombre 
        INTO v_fen, v_fen_inicio, v_fen_fin, v_periodicidad, v_desfase, v_estado, v_nombre_etl 
        FROM t_d_fecha_negocio_etls 
        WHERE etl_id = in_id;

        -- Evaluate state
        IF v_estado <> 'OK' THEN
            v_control := 'KO';
            out_status := CONCAT('El estado de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' es ', v_estado, '. Para poder actualizar una fecha el estado debe ser "OK"');
        END IF;
    END IF;

    -- Evaluate periodicity
    IF v_control = 'OK' THEN
        IF v_periodicidad NOT IN ('DIEZMINUTAL', 'HORARIA', 'DIARIA', 'TRIMESTRAL', 'SEMESTRAL', 'ANUAL', 'MENSUAL') THEN
            v_control := 'KO';
            out_status := CONCAT('La periodicidad de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' es ', v_periodicidad, '. La versión actual del procedimiento solo permite realizar cálculos sobre indicadores Diezminutales, Horarios, Diarios, Trimestrales, Semestrales, Anuales o Mensuales');
        END IF;
    END IF;
    
    IF v_control = 'OK' THEN
        -- Periodicity: DIEZMINUTAL
        IF v_periodicidad = 'DIEZMINUTAL' THEN
            out_status := 'OK';
            v_max_fen := (SELECT to_char(current_timestamp - CAST(concat(v_desfase * 10, ' MINUTE') AS INTERVAL), 'YYYY-MM-dd HH24:mi:00'));
            
            IF v_fen < v_max_fen THEN
                UPDATE t_d_fecha_negocio_etls 
                SET fen = v_fen + CAST('10 MINUTE' AS INTERVAL), aud_fec_upd = now(), aud_user_upd = 'p_gf_calcularNuevaFecha' 
                WHERE etl_id = in_id;
                
            ELSEIF v_fen IS NULL THEN
                v_control := 'KO';
                out_status := CONCAT('El campo FEN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            END IF;

        -- Periodicity: HORARIA
        ELSEIF v_periodicidad = 'HORARIA' THEN
            out_status := 'OK';
            v_max_fen := (SELECT to_char(current_timestamp - CAST(concat(v_desfase, ' HOUR') AS INTERVAL), 'YYYY-MM-dd HH24:mi:00'));
            
            IF v_fen < v_max_fen THEN
                UPDATE t_d_fecha_negocio_etls 
                SET fen = v_fen + CAST('1 HOUR' AS INTERVAL), aud_fec_upd = now(), aud_user_upd = 'p_gf_calcularNuevaFecha' 
                WHERE etl_id = in_id;
                
            ELSEIF v_fen IS NULL THEN
                v_control := 'KO';
                out_status := CONCAT('El campo FEN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            END IF;

        -- Periodicity: DIARIA
        ELSEIF v_periodicidad = 'DIARIA' THEN
            out_status := 'OK';
            v_max_fen := (SELECT CURRENT_DATE - CAST(concat(v_desfase, ' day') AS INTERVAL));
            
            IF v_fen < v_max_fen THEN
                UPDATE t_d_fecha_negocio_etls 
                SET fen = v_fen + CAST('1 DAY' AS INTERVAL), aud_fec_upd = now(), aud_user_upd = 'p_gf_calcularNuevaFecha' 
                WHERE etl_id = in_id;
                
            ELSEIF v_fen IS NULL THEN
                v_control := 'KO';
                out_status := CONCAT('El campo FEN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            END IF;

        -- Periodicity: TRIMESTRAL
        ELSEIF v_periodicidad = 'TRIMESTRAL' THEN
            IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
                v_control := 'KO';
                out_status := CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            ELSE
                out_status := 'OK';
                v_max_fen := (SELECT CURRENT_DATE - CAST(concat(v_desfase, ' day') AS INTERVAL));
                v_anyo_mes_max_fen := (SELECT to_char(v_max_fen, 'YYYYMM'));
                v_anyo_mes_fen_fin := (SELECT to_char(v_fen_fin, 'YYYYMM'));
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
                    v_mes_fen_fin := (SELECT to_char(v_fen_fin, 'MM'));
                    v_nueva_fen_inicio := NULL;
                    v_nueva_fen_fin := NULL;
                    
                    IF v_mes_fen_fin = '04' THEN
                        v_nueva_fen_inicio := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '0401'));
                        v_nueva_fen_fin := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '0701'));
                                        
                    ELSEIF v_mes_fen_fin = '07' THEN
                        v_nueva_fen_inicio := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '0701'));
                        v_nueva_fen_fin := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '1001'));
                    
                    ELSEIF v_mes_fen_fin = '10' THEN
                        v_nueva_fen_inicio := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '1001'));
                        v_nueva_fen_fin := (SELECT concat(to_char(v_fen_fin + CAST('1 YEAR' AS INTERVAL), 'YYYY'), '0101'));
                         
                    ELSEIF v_mes_fen_fin = '01' THEN
                        v_nueva_fen_inicio := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '0101'));
                        v_nueva_fen_fin := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '0401'));
                    END IF;
                    
                    IF v_nueva_fen_inicio IS NOT NULL AND v_nueva_fen_fin IS NOT NULL THEN
                        UPDATE t_d_fecha_negocio_etls 
                        SET fen_inicio = cast(v_nueva_fen_inicio as timestamp), fen_fin = cast(v_nueva_fen_fin as timestamp), aud_fec_upd = now(), aud_user_upd = 'p_gf_calcularNuevaFecha' 
                        WHERE etl_id = in_id;
                    END IF;
                END IF;
            END IF;

        -- Periodicity: SEMESTRAL
        ELSEIF v_periodicidad = 'SEMESTRAL' THEN
            IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
                v_control := 'KO';
                out_status := CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            ELSE
                out_status := 'OK';
                v_max_fen := (SELECT CURRENT_DATE - CAST(concat(v_desfase, ' day') AS INTERVAL));
                v_anyo_mes_max_fen := (SELECT to_char(v_max_fen, 'YYYYMM'));
                v_anyo_mes_fen_fin := (SELECT to_char(v_fen_fin, 'YYYYMM'));
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
                    v_mes_fen_fin := (SELECT to_char(v_fen_fin, 'MM'));
                    v_nueva_fen_inicio := NULL;
                    v_nueva_fen_fin := NULL;
                                        
                    IF v_mes_fen_fin = '07' THEN
                        v_nueva_fen_inicio := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '0701'));
                        v_nueva_fen_fin := (SELECT concat(to_char(v_fen_fin + CAST('1 YEAR' AS INTERVAL), 'YYYY'), '0101'));
                                       
                    ELSEIF v_mes_fen_fin = '01' THEN
                        v_nueva_fen_inicio := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '0101'));
                        v_nueva_fen_fin := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '0701'));
                    END IF;
                    
                    IF v_nueva_fen_inicio IS NOT NULL AND v_nueva_fen_fin IS NOT NULL THEN
                        UPDATE t_d_fecha_negocio_etls 
                        SET fen_inicio = cast(v_nueva_fen_inicio as timestamp), fen_fin = cast(v_nueva_fen_fin as timestamp), aud_fec_upd = now(), aud_user_upd = 'p_gf_calcularNuevaFecha' 
                        WHERE etl_id = in_id;
                    END IF;
                END IF;
            END IF;
			
        -- Periodicity: ANUAL
        ELSEIF v_periodicidad = 'ANUAL' THEN
            IF v_fen_inicio IS NULL OR v_fen_fin IS NULL THEN
                v_control := 'KO';
                out_status := CONCAT('Alguno de los campos FEN_INICIO y/o FEN_FIN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            ELSE
                out_status := 'OK';
                v_max_fen := (SELECT CURRENT_DATE - CAST(concat(v_desfase, ' day') AS INTERVAL));
                v_anyo_mes_max_fen := (SELECT to_char(v_max_fen, 'YYYYMM'));
                v_anyo_mes_fen_fin := (SELECT to_char(v_fen_fin, 'YYYYMM'));
                
                IF v_anyo_mes_fen_fin <= v_anyo_mes_max_fen THEN
                    v_nueva_fen_inicio := (SELECT concat(to_char(v_fen_fin, 'YYYY'), '0101'));
                    v_nueva_fen_fin := (SELECT concat(to_char(v_fen_fin + CAST('1 YEAR' AS INTERVAL), 'YYYY'), '0101'));

                    IF v_nueva_fen_inicio IS NOT NULL AND v_nueva_fen_fin IS NOT NULL THEN
                        UPDATE t_d_fecha_negocio_etls 
                        SET fen_inicio = cast(v_nueva_fen_inicio as timestamp), fen_fin = cast(v_nueva_fen_fin as timestamp), aud_fec_upd = now(), aud_user_upd = 'p_gf_calcularNuevaFecha' 
                        WHERE etl_id = in_id;
                    END IF;
                END IF;
            END IF;

        -- Periodicity: MENSUAL
        ELSEIF v_periodicidad = 'MENSUAL' THEN
            out_status := 'OK';
            v_max_fen := (SELECT CURRENT_DATE - CAST(concat(v_desfase, ' day') AS INTERVAL));
            
            IF v_fen < v_max_fen THEN
                UPDATE t_d_fecha_negocio_etls 
                SET fen = v_fen + INTERVAL '1 month', aud_fec_upd = now(), aud_user_upd = 'p_gf_calcularNuevaFecha' 
                WHERE etl_id = in_id;
                
            ELSEIF v_fen IS NULL THEN
                v_control := 'KO';
                out_status := CONCAT('El campo FEN de la ETL ', v_nombre_etl, ' con ID: ', in_id, ' tiene valor nulo.');
            END IF;
        END IF;
    END IF;
END;
$function$;

COMMIT;
