-- Deploy postgis-vlci-vlci2:r_code/functions/f.t_ref_estadistica_harcoded_kpi_before_insert to pg

BEGIN;

CREATE OR REPLACE FUNCTION fn_t_ref_estadistica_harcoded_kpi_before_insert()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
	NEW.aud_fec_ins=CURRENT_TIMESTAMP;
    RETURN NEW;
END;
$function$
;

COMMIT;
