-- Deploy postgis-vlci-vlci2:r_code/functions/f.aud_fec_upd to pg

BEGIN;

CREATE OR REPLACE FUNCTION vlci2.fn_aud_fec_upd()
RETURNS trigger
LANGUAGE plpgsql
AS $function$
BEGIN

       IF(NEW.aud_fec_upd is null) THEN
         NEW.aud_fec_upd=CURRENT_TIMESTAMP;
        END IF;

   RETURN NEW;
END;
$function$
;

COMMIT;
