-- Deploy postgis-vlci-vlci2:r_code/functions/f.aud_fec_upd_etl to pg

BEGIN;

  CREATE OR REPLACE FUNCTION vlci2.fn_aud_fec_upd_etl()
  RETURNS trigger
  LANGUAGE plpgsql
  AS $function$
  BEGIN
    NEW.aud_fec_upd=CURRENT_TIMESTAMP;

    if NEW.aud_user_upd is null  then 
        NEW.aud_user_upd = 'ETL';
    end if;

    RETURN NEW;
  END;
  $function$
  ;

COMMIT;
