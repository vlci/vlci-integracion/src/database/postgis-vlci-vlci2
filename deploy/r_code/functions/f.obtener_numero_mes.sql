-- Deploy postgis-vlci-vlci2:r_code/functions/f.obtener_numero_mes to pg

BEGIN;

CREATE OR REPLACE FUNCTION fn_obtener_numero_mes(nombre_mes character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
BEGIN
    CASE nombre_mes
        WHEN 'Ene' THEN RETURN '01';
        WHEN 'Feb' THEN RETURN '02';
        WHEN 'Mar' THEN RETURN '03';
        WHEN 'Abr' THEN RETURN '04';
        WHEN 'May' THEN RETURN '05';
        WHEN 'Jun' THEN RETURN '06';
        WHEN 'Jul' THEN RETURN '07';
        WHEN 'Ago' THEN RETURN '08';
        WHEN 'Sep' THEN RETURN '09';
        WHEN 'Oct' THEN RETURN '10';
        WHEN 'Nov' THEN RETURN '11';
        WHEN 'Dic' THEN RETURN '12';
        ELSE RETURN nombre_mes;
    END CASE;
END;
$function$
;

COMMIT;