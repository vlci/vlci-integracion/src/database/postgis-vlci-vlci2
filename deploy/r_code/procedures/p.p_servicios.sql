-- Deploy postgis-vlci-vlci2:r_code/procedures/p.p_servicios to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.p_servicios()
 LANGUAGE plpgsql
AS $procedure$
begin
    DROP TABLE IF EXISTS vlci2.t_d_servicio_001;
    CREATE TABLE vlci2.t_d_servicio_001 AS 
    SELECT
        b.fk_servicio_id as fk_servicio_id,
        1 AS nu_indicadores,
        CASE WHEN a.estado_actual = 'VERDE' THEN 1 ELSE 0 END AS nu_verde,
        CASE WHEN a.estado_actual = 'AMBAR' THEN 1 ELSE 0 END AS nu_ambar,
        CASE WHEN a.estado_actual = 'ROJO' THEN 1 ELSE 0 END AS nu_rojo,
        CASE WHEN a.estado_actual = 'SIN COLOR' THEN 1 ELSE 0 END AS nu_gris
    FROM vlci2.t_x_estados_aux a
    JOIN vlci2.t_d_indicador b
    on a.fk_indicador = b.pk_indicador
    ;

    DROP TABLE IF EXISTS vlci2.t_d_servicio_002;
    CREATE TABLE vlci2.t_d_servicio_002 AS 
    SELECT
        fk_servicio_id,
        sum(nu_indicadores) AS nu_indicadores,
        sum(nu_verde) AS nu_verde,
        sum(nu_ambar) AS nu_ambar,
        sum(nu_rojo) AS nu_rojo,
        sum(nu_gris) AS nu_gris
    FROM vlci2.t_d_servicio_001
    GROUP BY fk_servicio_id
    ;

    DROP TABLE IF EXISTS vlci2.t_d_servicio_003;
    CREATE TABLE vlci2.t_d_servicio_003 AS 
    SELECT
        fk_servicio_id,
        nu_indicadores,
        nu_verde,
        nu_ambar,
        nu_rojo,
        nu_gris,
        CASE
            WHEN nu_indicadores = 0 OR nu_indicadores = nu_gris THEN 'AR_GRIS'
            WHEN 2 * nu_rojo > nu_indicadores THEN 'AR_ROJO'
            WHEN 2 * nu_verde > nu_indicadores THEN 'AR_VERDE'
            ELSE 'AR_AMBAR'
        END AS color
    FROM vlci2.t_d_servicio_002
    ;

    DROP TABLE IF EXISTS vlci2.t_d_servicio_004;
    CREATE TABLE vlci2.t_d_servicio_004 AS 
    SELECT
        a.pk_servicio_id AS pk_servicio_id,
        a.fk_delegacion_id AS fk_delegacion_id,
        a.codigo_organico AS codigo_organico,
        a.desc_corta_cas AS desc_corta_cas,
        a.desc_corta_val AS desc_corta_val,
        a.desc_larga_cas AS desc_larga_cas,
        a.desc_larga_val AS desc_larga_val,
        CASE WHEN b.fk_servicio_id IS NULL THEN 'AR_GRIS' ELSE b.color END AS de_color,
        a.activo_mask AS activo_mask,
        CASE WHEN b.fk_servicio_id IS NULL THEN 0 ELSE b.nu_indicadores END AS nu_indicadores,
        CASE WHEN b.fk_servicio_id IS NULL THEN 0 ELSE b.nu_rojo END AS nu_rojo,
        CASE WHEN b.fk_servicio_id IS NULL THEN 0 ELSE b.nu_ambar END AS nu_ambar,
        CASE WHEN b.fk_servicio_id IS NULL THEN 0 ELSE b.nu_verde END AS nu_verde,
        CASE WHEN b.fk_servicio_id IS NULL THEN 0 ELSE b.nu_gris END AS nu_gris,
        a.aud_fec_ins AS aud_fec_ins,
        a.aud_user_ins AS aud_user_ins,
        a.aud_fec_upd AS aud_fec_upd,
        a.aud_user_upd AS aud_user_upd
    FROM vlci2.t_datos_etl_agregados_servicio a
    LEFT JOIN vlci2.t_d_servicio_003 b
    ON a.pk_servicio_id = b.fk_servicio_id
    ;


    DROP TABLE IF EXISTS vlci2.t_d_servicio;
    CREATE TABLE vlci2.t_d_servicio AS 
    SELECT
        a.pk_servicio_id AS pk_servicio_id,
        a.fk_delegacion_id AS fk_delegacion_id,
        a.codigo_organico AS codigo_organico,
        a.desc_corta_cas AS desc_corta_cas,
        a.desc_corta_val AS desc_corta_val,
        a.desc_larga_cas AS desc_larga_cas,
        a.desc_larga_val AS desc_larga_val,
        b.de_color_url AS estado_url,
        a.de_color AS de_color,
        a.activo_mask AS activo_mask,
        a.nu_indicadores AS nu_indicadores,
        a.nu_rojo AS nu_rojo,
        a.nu_ambar AS nu_ambar,
        a.nu_verde AS nu_verde,
        a.nu_gris AS nu_gris,
        a.aud_fec_ins AS aud_fec_ins,
        a.aud_user_ins AS aud_user_ins,
        a.aud_fec_upd AS aud_fec_upd,
        a.aud_user_upd AS aud_user_upd
    FROM vlci2.t_d_servicio_004 a
    LEFT JOIN vlci2.t_x_organizacion_municipal_color b
    ON a.de_color = b.de_color
    ;



    DROP TABLE IF EXISTS vlci2.t_d_servicio_002;
    DROP TABLE IF EXISTS vlci2.t_d_servicio_003;
    DROP TABLE IF EXISTS vlci2.t_d_servicio_004;

END
$procedure$
;

COMMIT;
