-- Deploy postgis-vlci-vlci2:r_code/procedures/p.p_insercion_cb to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.p_insercion_cb()
 LANGUAGE plpgsql
AS $procedure$
begin
    DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker_001;
    CREATE TABLE vlci2.t_d_indicadores_destino_contextbroker_001 AS SELECT
        a.dc_identificador_indicador as id,
        a.fk_periodicidad_muestreo as fk_periodicidad_muestreo,
        b.pk_indicador as fk_indicador,
        b.de_nombre_indicador_cas as de_nombre_indicador_cas,
        b.fk_unidad_medida as fk_unidad_medida
    FROM vlci2.t_d_indicadores_destino_contextbroker a
    LEFT JOIN vlci2.aux_get_pk_indicador c
    ON a.dc_identificador_indicador = c.dc_identificador_indicador
    LEFT JOIN vlci2.t_d_indicador b
    ON c.pk_indicador = b.pk_indicador
    ;

    DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker_002;
    CREATE TABLE vlci2.t_d_indicadores_destino_contextbroker_002 AS SELECT
        a.id as id,
        a.fk_indicador as fk_indicador,
        a.fk_periodicidad_muestreo as fk_periodicidad_muestreo,
        b.dc_periodicidad_muestreo_valor as dc_periodicidad_muestreo_valor,
        b.fk_barrio as fk_barrio,
        b.dc_criterio_desagregacion1_valor as dc_criterio_desagregacion1_valor,
        b.dc_criterio_desagregacion2_valor as dc_criterio_desagregacion2_valor,
        b.dc_criterio_desagregacion3_valor as dc_criterio_desagregacion3_valor,
        b.dc_criterio_desagregacion4_valor as dc_criterio_desagregacion4_valor,
        b.nu_indicador_valor as nu_indicador_valor,
        b.etl_nombre as etl_nombre,
        b.fe_fecha_carga as fe_fecha_carga,
        a.de_nombre_indicador_cas as de_nombre_indicador_cas,
        a.fk_unidad_medida as fk_unidad_medida
    FROM vlci2.t_d_indicadores_destino_contextbroker_001 a
    JOIN vlci2.t_x_aggregados_002 b
    ON (a.fk_indicador = b.fk_indicador AND a.fk_periodicidad_muestreo = b.fk_periodicidad_muestreo)
    WHERE b.es_barrio_global AND b.es_criterio_global
    ;

    DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker_003;
    CREATE TABLE vlci2.t_d_indicadores_destino_contextbroker_003 AS SELECT
        a.id as id,
        a.fk_indicador as fk_indicador,
        a.fk_periodicidad_muestreo as fk_periodicidad_muestreo,
        a.dc_periodicidad_muestreo_valor as dc_periodicidad_muestreo_valor,
        a.fk_barrio as fk_barrio,
        a.dc_criterio_desagregacion1_valor as dc_criterio_desagregacion1_valor,
        a.dc_criterio_desagregacion2_valor as dc_criterio_desagregacion2_valor,
        a.dc_criterio_desagregacion3_valor as dc_criterio_desagregacion3_valor,
        a.dc_criterio_desagregacion4_valor as dc_criterio_desagregacion4_valor,
        a.nu_indicador_valor as nu_indicador_valor,
        a.etl_nombre as etl_nombre,
        a.fe_fecha_carga as fe_fecha_carga,
        a.de_nombre_indicador_cas as de_nombre_indicador_cas,
        a.fk_unidad_medida as fk_unidad_medida,
        b.dc_unidad_medida_cas as dc_unidad_medida_cas
    FROM vlci2.t_d_indicadores_destino_contextbroker_002 a
    LEFT JOIN vlci2.t_m_unidad_medida b
    ON a.fk_unidad_medida = b.pk_unidad_medida
    ;

    DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker_004;
    CREATE TABLE vlci2.t_d_indicadores_destino_contextbroker_004 AS SELECT
        id,
        'KeyPerformanceIndicator' as type,
        id as kpiInternalIdentifier,
        de_nombre_indicador_cas as name,
        'quantitative' as category,
        dc_unidad_medida_cas as measureUnit,
        CASE 
            WHEN fk_periodicidad_muestreo = 1 THEN 'n/a'
            WHEN fk_periodicidad_muestreo = 2 THEN 'yearly'
            WHEN fk_periodicidad_muestreo = 3 THEN 'quadrennial'
            WHEN fk_periodicidad_muestreo = 4 THEN 'quinquennial'
            WHEN fk_periodicidad_muestreo = 5 THEN 'decennial'
            WHEN fk_periodicidad_muestreo = 6 THEN 'daily'
            WHEN fk_periodicidad_muestreo = 7 THEN 'monthly'
            WHEN fk_periodicidad_muestreo = 8 THEN 'quaterly'
            WHEN fk_periodicidad_muestreo = 9 THEN 'biannual'
            WHEN fk_periodicidad_muestreo = 10 THEN 'hourly'
            WHEN fk_periodicidad_muestreo = 11 THEN 'every15minutes'
            WHEN fk_periodicidad_muestreo = 12 THEN 'every5minutes'
            WHEN fk_periodicidad_muestreo = 13 THEN 'weekly'
            WHEN fk_periodicidad_muestreo = 14 THEN 'fortnightly'
            ELSE 'unknown'
        END AS calculationFrequency,
        nu_indicador_valor as kpiValue,
        dc_periodicidad_muestreo_valor as calculationPeriod,
        CASE WHEN dc_criterio_desagregacion1_valor IS NULL THEN '' ELSE dc_criterio_desagregacion1_valor END AS sliceAndDiceValue1,
        CASE WHEN dc_criterio_desagregacion2_valor IS NULL THEN '' ELSE dc_criterio_desagregacion2_valor END AS sliceAndDiceValue2,
        CASE WHEN dc_criterio_desagregacion3_valor IS NULL THEN '' ELSE dc_criterio_desagregacion3_valor END AS sliceAndDiceValue3,
        CASE WHEN dc_criterio_desagregacion4_valor IS NULL THEN '' ELSE dc_criterio_desagregacion4_valor END AS sliceAndDiceValue4,
        fk_barrio AS neighborhoodId,
        '' AS ErrorStatus,
        concat(translate(cast(fe_fecha_carga as text),' ','T'),'Z') as updatedAt,
        '' as organization,
        '' as product,
        etl_nombre as processName,
        '' as location,
        '' as internationalStandard,
        nu_indicador_valor as kpiNumeratorValue,
        1 as kpiDenominatorValue
    FROM vlci2.t_d_indicadores_destino_contextbroker_003
    ;

    DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker_005;
    CREATE TABLE vlci2.t_d_indicadores_destino_contextbroker_005 AS SELECT
        id,
        calculationPeriod as dc_calculated_period,
        concat('"id": "',id,'"') as j_id,
        concat('"type": "',type,'"') as j_type,
        concat('"kpiInternalIdentifier": "',kpiInternalIdentifier,'"') as j_kpiInternalIdentifier,
        concat('"name": "',name,'"') as j_name,
        concat('"category": "',category,'"') as j_category,
        concat('"measureUnit": "',measureUnit,'"') as j_measureUnit,
        concat('"calculationFrequency": "',calculationFrequency,'"') as j_calculationFrequency,
        concat('"kpiValue": "',kpiValue,'"') as j_kpiValue,
        concat('"calculationPeriod": "',calculationPeriod,'"') as j_calculationPeriod,
        concat('"sliceAndDiceValue1": "',sliceAndDiceValue1,'"') as j_sliceAndDiceValue1,
        concat('"sliceAndDiceValue2": "',sliceAndDiceValue2,'"') as j_sliceAndDiceValue2,
        concat('"sliceAndDiceValue3": "',sliceAndDiceValue3,'"') as j_sliceAndDiceValue3,
        concat('"sliceAndDiceValue4": "',sliceAndDiceValue4,'"') as j_sliceAndDiceValue4,
        concat('"neighborhoodId": "',neighborhoodId,'"') as j_neighborhoodId,
        concat('"Error-status": "',ErrorStatus,'"') as j_ErrorStatus,
        concat('"updatedAt": "',updatedAt,'"') as j_updatedAt,
        concat('"organization": "',organization,'"') as j_organization,
        concat('"product": "',product,'"') as j_product,
        concat('"processName": "',processName,'"') as j_processName,
        concat('"location": "',location,'"') as j_location,
        concat('"internationalStandard": "',internationalStandard,'"') as j_internationalStandard,
        concat('"kpiNumeratorValue": "',kpiNumeratorValue,'"') as j_kpiNumeratorValue,
        concat('"kpiDenominatorValue": "',kpiDenominatorValue,'"') as j_kpiDenominatorValue
    FROM vlci2.t_d_indicadores_destino_contextbroker_004
    ;

    DROP TABLE IF EXISTS vlci2.t_d_indicadores_insercion_contextbroker;
    CREATE TABLE vlci2.t_d_indicadores_insercion_contextbroker AS SELECT
        id,
        dc_calculated_period,
        concat('{',
            j_id,', ',
            j_type,', ',
            j_kpiInternalIdentifier,', ',
            j_name,', ',
            j_category,', ',
            j_measureUnit,', ',
            j_calculationFrequency,', ',
            j_kpiValue,', ',
            j_calculationPeriod,', ',
            j_sliceAndDiceValue1,', ',
            j_sliceAndDiceValue2,', ',
            j_sliceAndDiceValue3,', ',
            j_sliceAndDiceValue4,', ',
            j_neighborhoodId,', ',
            j_ErrorStatus,', ',
            j_updatedAt,', ',
            j_organization,', ',
            j_product,', ',
            j_processName,', ',
            j_location,', ',
            j_internationalStandard,', ',
            j_kpiNumeratorValue,', ',
            j_kpiDenominatorValue,'}'
        ) as json
    FROM vlci2.t_d_indicadores_destino_contextbroker_005
    ;

    DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker_001;
    DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker_002;
    DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker_003;
    DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker_004;
    DROP TABLE IF EXISTS vlci2.t_d_indicadores_destino_contextbroker_005;
END
$procedure$
;

COMMIT;
