-- Deploy postgis-vlci-vlci2:r_code/procedures/p.p_delegaciones to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.p_delegaciones()
 LANGUAGE plpgsql
AS $procedure$
begin
    DROP TABLE IF EXISTS vlci2.t_d_delegacion_001;
    CREATE TABLE vlci2.t_d_delegacion_001 AS 
    SELECT
        b.fk_delegacion_id as fk_delegacion_id,
        a.nu_indicadores AS nu_indicadores,
        a.nu_verde AS nu_verde,
        a.nu_ambar AS nu_ambar,
        a.nu_rojo AS nu_rojo,
        a.nu_gris AS nu_gris
    FROM vlci2.t_d_servicio_001 a
    LEFT JOIN vlci2.t_datos_etl_agregados_servicio b
    ON a.fk_servicio_id = b.pk_servicio_id
    ;


    DROP TABLE IF EXISTS vlci2.t_d_delegacion_002;
    CREATE TABLE vlci2.t_d_delegacion_002 AS 
    SELECT
        fk_delegacion_id,
        sum(nu_indicadores) AS nu_indicadores,
        sum(nu_verde) AS nu_verde,
        sum(nu_ambar) AS nu_ambar,
        sum(nu_rojo) AS nu_rojo,
        sum(nu_gris) AS nu_gris
    FROM vlci2.t_d_delegacion_001
    GROUP BY fk_delegacion_id
    ;

    DROP TABLE IF EXISTS vlci2.t_d_delegacion_003;
    CREATE TABLE vlci2.t_d_delegacion_003 AS 
    SELECT
        fk_delegacion_id,
        nu_indicadores,
        nu_verde,
        nu_ambar,
        nu_rojo,
        nu_gris,
        CASE
            WHEN nu_indicadores = 0 OR nu_indicadores = nu_gris THEN 'AR_GRIS'
            WHEN 2 * nu_rojo > nu_indicadores THEN 'AR_ROJO'
            WHEN 2 * nu_verde > nu_indicadores THEN 'AR_VERDE'
            ELSE 'AR_AMBAR'
        END AS color
    FROM vlci2.t_d_delegacion_002
    ;

    DROP TABLE IF EXISTS vlci2.t_d_delegacion_004;
    CREATE TABLE vlci2.t_d_delegacion_004 AS 
    SELECT
        a.pk_delegacion_id AS pk_delegacion_id,
        a.pk_area_id AS pk_area_id,
        a.desc_corta_cas AS desc_corta_cas,
        a.desc_corta_val AS desc_corta_val,
        a.desc_larga_cas AS desc_larga_cas,
        a.desc_larga_val AS desc_larga_val,
        CASE WHEN b.fk_delegacion_id IS NULL THEN 'AR_GRIS' ELSE b.color END AS de_color,
        a.activo_mask AS activo_mask,
        CASE WHEN b.fk_delegacion_id IS NULL THEN 0 ELSE b.nu_indicadores END AS nu_indicadores,
        CASE WHEN b.fk_delegacion_id IS NULL THEN 0 ELSE b.nu_rojo END AS nu_rojo,
        CASE WHEN b.fk_delegacion_id IS NULL THEN 0 ELSE b.nu_ambar END AS nu_ambar,
        CASE WHEN b.fk_delegacion_id IS NULL THEN 0 ELSE b.nu_verde END AS nu_verde,
        CASE WHEN b.fk_delegacion_id IS NULL THEN 0 ELSE b.nu_gris END AS nu_gris,
        a.aud_fec_ins AS aud_fec_ins,
        a.aud_user_ins AS aud_user_ins,
        a.aud_fec_upd AS aud_fec_upd,
        a.aud_user_upd AS aud_user_upd
    FROM vlci2.t_datos_etl_agregados_delegacion a
    LEFT JOIN vlci2.t_d_delegacion_003 b
    ON a.pk_delegacion_id = b.fk_delegacion_id
    ;


    DROP TABLE IF EXISTS vlci2.t_d_delegacion;
    CREATE TABLE vlci2.t_d_delegacion AS 
    SELECT
        a.pk_delegacion_id AS pk_delegacion_id,
        a.pk_area_id AS pk_area_id,
        a.desc_corta_cas AS desc_corta_cas,
        a.desc_corta_val AS desc_corta_val,
        a.desc_larga_cas AS desc_larga_cas,
        a.desc_larga_val AS desc_larga_val,
        b.de_color_url AS estado_url,
        a.de_color AS de_color,
        a.activo_mask AS activo_mask,
        a.nu_indicadores AS nu_indicadores,
        a.nu_rojo AS nu_rojo,
        a.nu_ambar AS nu_ambar,
        a.nu_verde AS nu_verde,
        a.nu_gris AS nu_gris,
        a.aud_fec_ins AS aud_fec_ins,
        a.aud_user_ins AS aud_user_ins,
        a.aud_fec_upd AS aud_fec_upd,
        a.aud_user_upd AS aud_user_upd
    FROM vlci2.t_d_delegacion_004 a
    LEFT JOIN vlci2.t_x_organizacion_municipal_color b
    ON a.de_color = b.de_color
    ;



    DROP TABLE IF EXISTS vlci2.t_d_delegacion_002;
    DROP TABLE IF EXISTS vlci2.t_d_delegacion_003;
    DROP TABLE IF EXISTS vlci2.t_d_delegacion_004;
END
$procedure$
;

COMMIT;
