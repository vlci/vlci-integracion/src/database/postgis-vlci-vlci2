-- Deploy postgis-vlci-vlci2:r_code/procedures/p.delete_from_table_by_id to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.delete_from_table_by_id(
    IN in_table_name character varying,
    IN in_months integer,
    IN in_colfecha character varying
)
LANGUAGE plpgsql
SECURITY DEFINER
AS $procedure$
BEGIN
    EXECUTE format(
        'DELETE FROM %I WHERE 
        CASE 
            WHEN pg_typeof(%I) IN (''timestamp'', ''date'') THEN %I::date
            WHEN %I::text ~ ''^\d{4}-\d{2}-\d{2}$'' THEN TO_DATE(%I::text, ''YYYY-MM-DD'')
            WHEN %I::text ~ ''^\d{4}/\d{2}/\d{2}$'' THEN TO_DATE(%I::text, ''YYYY/MM/DD'')
        END < (NOW() - INTERVAL %L)',
        in_table_name,
        in_colfecha,
        in_colfecha,
        in_colfecha,
        in_colfecha,
        in_colfecha,
        in_colfecha,
        in_months || ' MONTH');
END
$procedure$;



COMMIT;
