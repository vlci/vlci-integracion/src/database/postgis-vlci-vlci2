-- Deploy postgis-vlci-vlci2:r_code/procedures/p.p_estados to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.p_estados()
 LANGUAGE plpgsql
AS $procedure$
begin
    DROP TABLE IF EXISTS vlci2.t_x_estados_aux;
    CREATE TABLE vlci2.t_x_estados_aux AS 
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        nu_indicador_valor,
        n_meses,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios,
        actual,
        previo,
        actual_n_meses,
        previo_n_meses,
        tendencia_real,
        de_tendencia_deseada_cas,
        nu_indicador_umbral_inferior,
        nu_indicador_umbral_superior,
        flecha,
        color_flecha,
        estado_actual,
        estado_previo,
        tendencia,
        microchart
    FROM vlci2.t_x_tendencias_aux
    WHERE es_criterio_global
        AND es_barrio_global
        AND rn = 1
    ;

END
$procedure$
;

COMMIT;
