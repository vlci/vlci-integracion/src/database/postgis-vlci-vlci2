-- Deploy postgis-vlci-vlci2:r_code/procedures/p.p_de_cb_a_hechos to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.p_de_cb_a_hechos()
 LANGUAGE plpgsql
AS $procedure$
begin
	DROP TABLE IF EXISTS vlci2.aux_get_pk_indicador;
	CREATE TABLE vlci2.aux_get_pk_indicador AS
	SELECT
	  pk_indicador,
	  dc_identificador_indicador
	FROM
	  (
	  SELECT
		a.pk_indicador AS pk_indicador,
		a.dc_identificador_indicador AS dc_identificador_indicador,
		a.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
		b.dc_orden_periodicidad AS dc_orden_periodicidad,
		row_number() OVER (PARTITION BY a.dc_identificador_indicador ORDER BY b.dc_orden_periodicidad ASC) AS rn
	  FROM
		(
		SELECT 
		  pk_indicador,
		  dc_identificador_indicador,
		  fk_periodicidad_muestreo
		FROM
		  vlci2.t_d_indicador
		WHERE 
		  is_agregado = 'N'
		) a
	  LEFT JOIN vlci2.t_m_periodicidad_muestreo b
	  ON a.fk_periodicidad_muestreo = b.pk_periodicidad_muestreo
	  ) b
	WHERE
	  rn = 1
	;

	DROP TABLE IF EXISTS vlci2.aux_data_from_context_broker;
	CREATE TABLE vlci2.aux_data_from_context_broker AS 
	SELECT
	  b.pk_indicador AS fk_indicador,
	  b.fk_unidad_medida AS fk_unidad_medida,
	  b.fk_periodicidad_muestreo AS fk_periodicidad_muestreo, 
	  CASE WHEN a.calculationperiod like '{value:%' THEN substr(a.calculationperiod,8,length(a.calculationperiod)-8)
		  ELSE a.calculationperiod
		  END AS dc_periodicidad_muestreo_valor, 
	  NULL as dc_indicador_valor, 
	  cast((CASE WHEN a.kpiValue like '{value:%' THEN substr(a.kpiValue,8,length(a.kpiValue)-8)
				ELSE a.kpiValue
				END) AS double precision) AS nu_indicador_valor, 
		CASE WHEN a.sliceAndDiceValue1 like '{value:%' THEN substr(a.sliceAndDiceValue1,8,length(a.sliceAndDiceValue1)-8)
			ELSE a.sliceAndDiceValue1
			END AS dc_criterio_desagregacion1_valor, 
		CASE WHEN a.sliceAndDiceValue2 like '{value:%' THEN substr(a.sliceAndDiceValue2,8,length(a.sliceAndDiceValue2)-8)
			ELSE a.sliceAndDiceValue2
			END AS dc_criterio_desagregacion2_valor, 
		CASE WHEN a.sliceAndDiceValue3 like '{value:%' THEN substr(a.sliceAndDiceValue3,8,length(a.sliceAndDiceValue3)-8)
			ELSE a.sliceAndDiceValue3
			END AS dc_criterio_desagregacion3_valor, 
		CASE WHEN a.sliceAndDiceValue4 like '{value:%' THEN substr(a.sliceAndDiceValue4,8,length(a.sliceAndDiceValue4)-8)
			ELSE a.sliceAndDiceValue4
			END AS dc_criterio_desagregacion4_valor, 
		0 AS nu_latitud, 
		0 AS nu_longitud, 
		CASE WHEN a.neighborhoodId like '{value:%' THEN substr(a.neighborhoodId,8,length(a.neighborhoodId)-8)
			ELSE a.neighborhoodId
			END AS fk_barrio, 
		cast(translate(translate(CASE WHEN a.updatedAt like '{type:DateTime,value:%' THEN substr(a.updatedAt,22,length(a.updatedAt)-22)
								ELSE a.updatedAt
								END,'T',' '),'Z','') as timestamp) AS fe_fecha_carga, 
		'ContextBroker' AS etl_nombre 
	FROM vlci2.t_f_raw_from_context_broker a
	LEFT JOIN vlci2.aux_get_pk_indicador c
	ON c.dc_identificador_indicador = a.entityid
	LEFT JOIN vlci2.t_d_indicador b
	ON c.pk_indicador = b.pk_indicador;

	DROP TABLE IF EXISTS vlci2.t_x_hechos_001;
	CREATE TABLE vlci2.t_x_hechos_001 AS
	  SELECT fk_indicador,
			  fk_periodicidad_muestreo,
			  dc_periodicidad_muestreo_valor,
			  fk_barrio,
			  dc_criterio_desagregacion1_valor,
			  dc_criterio_desagregacion2_valor,
			  dc_criterio_desagregacion3_valor,
			  dc_criterio_desagregacion4_valor,
			  dc_indicador_valor,
			  nu_indicador_valor,
			  etl_nombre,
			  fe_fecha_carga
	   FROM vlci2.t_f_indicador_valor
	   UNION ALL
	   SELECT fk_indicador,
			  fk_periodicidad_muestreo,
			  dc_periodicidad_muestreo_valor,
			  fk_barrio,
			  dc_criterio_desagregacion1_valor,
			  dc_criterio_desagregacion2_valor,
			  dc_criterio_desagregacion3_valor,
			  dc_criterio_desagregacion4_valor,
			  dc_indicador_valor,
			  nu_indicador_valor,
			  etl_nombre,
			  fe_fecha_carga
	   FROM vlci2.aux_data_from_context_broker
	   ;


	DROP TABLE IF EXISTS vlci2.t_x_hechos;
	CREATE TABLE vlci2.t_x_hechos AS
	SELECT
	  hechos.fk_indicador AS fk_indicador,
	  hechos.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
	  hechos.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
	  hechos.fk_barrio AS fk_barrio,
	  hechos.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
	  hechos.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
	  hechos.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
	  hechos.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
	  hechos.dc_indicador_valor AS dc_indicador_valor,
	  CASE WHEN texto.nu_indicador_valor IS NULL THEN hechos.nu_indicador_valor ELSE texto.nu_indicador_valor END AS nu_indicador_valor,
	  hechos.etl_nombre AS etl_nombre,
	  hechos.fe_fecha_carga AS fe_fecha_carga
	FROM vlci2.t_x_hechos_001 hechos
	LEFT JOIN vlci2.t_d_indicador c
	ON c.pk_indicador = hechos.fk_indicador
	LEFT JOIN vlci2.t_x_indicador_texto texto
	ON texto.dc_identificador_indicador = c.dc_identificador_indicador
	  AND hechos.dc_indicador_valor = texto.dc_indicador_valor
	   ;

	DROP TABLE IF EXISTS vlci2.t_x_hechos_001;

END
$procedure$
;

COMMIT;
