-- Deploy postgis-vlci-vlci2:r_code/procedures/p.p_agregados_criterios.sql to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.p_agregados_criterios()
 LANGUAGE plpgsql
AS $procedure$
begin
    DROP TABLE IF EXISTS vlci2.h_x_agregados_temporales_001;
    CREATE TABLE vlci2.h_x_agregados_temporales_001 AS
    select 
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        CASE
            WHEN dc_criterio_desagregacion1_valor IS NULL
                -- OR dc_criterio_desagregacion1_valor = "\'\\N\'"
                OR dc_criterio_desagregacion1_valor = ''
                OR dc_criterio_desagregacion1_valor = 'N/A' THEN 'N/A'
            ELSE dc_criterio_desagregacion1_valor
        END AS dc_criterio_desagregacion1_valor,
        CASE
            WHEN dc_criterio_desagregacion2_valor IS NULL
                -- OR dc_criterio_desagregacion2_valor = "\'\\N\'"
                OR dc_criterio_desagregacion2_valor = ''
                OR dc_criterio_desagregacion2_valor = 'N/A' THEN 'N/A'
            ELSE dc_criterio_desagregacion2_valor
        END AS dc_criterio_desagregacion2_valor,
        CASE
            WHEN dc_criterio_desagregacion3_valor IS NULL
                -- OR dc_criterio_desagregacion3_valor = "\'\\N\'"
                OR dc_criterio_desagregacion3_valor = ''
                OR dc_criterio_desagregacion3_valor = 'N/A' THEN 'N/A'
            ELSE dc_criterio_desagregacion3_valor
        END AS dc_criterio_desagregacion3_valor,
        CASE
            WHEN dc_criterio_desagregacion4_valor IS NULL
                -- OR dc_criterio_desagregacion4_valor = "\'\\N\'"
                OR dc_criterio_desagregacion4_valor = ''
                OR dc_criterio_desagregacion4_valor = 'N/A' THEN 'N/A'
            ELSE dc_criterio_desagregacion4_valor
        END AS dc_criterio_desagregacion4_valor,
        dc_indicador_valor,
        nu_indicador_valor,
        etl_nombre,
        fe_fecha_carga,
        n_meses,
        agregue_a_anyo,
        (dc_criterio_desagregacion1_valor IS NULL
                -- OR dc_criterio_desagregacion1_valor = "\'\\N\'"
                OR dc_criterio_desagregacion1_valor = ''
                OR dc_criterio_desagregacion1_valor = 'N/A') AS es_criterio_global,
        fk_barrio = '999' AS es_barrio_global
    from vlci2.t_x_aggregados_001
    ;


    DROP TABLE IF EXISTS vlci2.h_x_replicados_criterio;
    CREATE TABLE vlci2.h_x_replicados_criterio AS
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        'N/A' AS dc_criterio_desagregacion1_valor,
        'N/A' AS dc_criterio_desagregacion2_valor,
        'N/A' AS dc_criterio_desagregacion3_valor,
        'N/A' AS dc_criterio_desagregacion4_valor,
        dc_indicador_valor,
        nu_indicador_valor,
        etl_nombre,
        fe_fecha_carga,
        n_meses,
        agregue_a_anyo,
        true AS es_criterio_global,
        true AS agregue_criterios,
        es_barrio_global
    FROM vlci2.h_x_agregados_temporales_001
    WHERE NOT es_criterio_global
    UNION ALL
    SELECT 
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        dc_indicador_valor,
        nu_indicador_valor,
        etl_nombre,
        fe_fecha_carga,
        n_meses,
        agregue_a_anyo,
        es_criterio_global,
        false AS agregue_criterios,
        es_barrio_global
    FROM vlci2.h_x_agregados_temporales_001
    ;


    DROP TABLE IF EXISTS vlci2.h_x_replicados;
    CREATE TABLE vlci2.h_x_replicados AS
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        '000' AS fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        dc_indicador_valor,
        nu_indicador_valor,
        etl_nombre,
        fe_fecha_carga,
        n_meses,
        agregue_a_anyo,
        es_criterio_global,
        agregue_criterios,
        true AS es_barrio_global,
        true AS agregue_barrios
    FROM vlci2.h_x_replicados_criterio
    WHERE NOT es_barrio_global
    UNION ALL
    SELECT 
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        dc_indicador_valor,
        nu_indicador_valor,
        etl_nombre,
        fe_fecha_carga,
        n_meses,
        agregue_a_anyo,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        false AS agregue_barrios
    FROM vlci2.h_x_replicados_criterio
    ;


    DROP TABLE IF EXISTS vlci2.t_x_aggregados_002_temp;
    CREATE TABLE vlci2.t_x_aggregados_002_temp AS 
    SELECT h_x_replicados.fk_indicador AS fk_indicador,
            h_x_replicados.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
            h_x_replicados.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
            h_x_replicados.fk_barrio AS fk_barrio,
            h_x_replicados.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
            h_x_replicados.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
            h_x_replicados.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
            h_x_replicados.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
            CASE
                WHEN upper(b.agg_criterios) = 'AVG' THEN avg(h_x_replicados.nu_indicador_valor)
                ELSE sum(h_x_replicados.nu_indicador_valor)
            END AS nu_indicador_valor,
            NULL AS dc_indicador_valor,
            max(etl_nombre) AS etl_nombre,
            max(fe_fecha_carga) AS fe_fecha_carga,
            max(n_meses) AS n_meses,
            agregue_a_anyo,
            es_criterio_global,
            agregue_criterios,
            es_barrio_global,
            agregue_barrios,
            count(*) AS replicaciones
            FROM vlci2.h_x_replicados h_x_replicados
            JOIN vlci2.t_m_metodo_agg b ON b.fk_indicador = h_x_replicados.fk_indicador
            GROUP BY h_x_replicados.fk_indicador,
            h_x_replicados.fk_periodicidad_muestreo,
            h_x_replicados.dc_periodicidad_muestreo_valor,
            h_x_replicados.fk_barrio,
            h_x_replicados.dc_criterio_desagregacion1_valor,
            h_x_replicados.dc_criterio_desagregacion2_valor,
            h_x_replicados.dc_criterio_desagregacion3_valor,
            h_x_replicados.dc_criterio_desagregacion4_valor,
            h_x_replicados.agregue_a_anyo,
            h_x_replicados.es_criterio_global,
            h_x_replicados.agregue_criterios,
            h_x_replicados.es_barrio_global,
            h_x_replicados.agregue_barrios,
            b.agg_criterios
    ;








    DROP TABLE IF EXISTS vlci2.t_x_clave_combinacion_criterios;
    CREATE TABLE vlci2.t_x_clave_combinacion_criterios AS
    SELECT
    a.dc_identificador_indicador_fino as dc_identificador_indicador_fino,
    b.pk_indicador as fk_indicador_fino,
    a.dc_identificador_indicador_grueso as dc_identificador_indicador_grueso,
    c.pk_indicador as fk_indicador_grueso
    FROM vlci2.t_m_combinados_criterios a
    LEFT JOIN vlci2.aux_get_pk_indicador f
    ON a.dc_identificador_indicador_fino = f.dc_identificador_indicador
    LEFT JOIN vlci2.t_d_indicador b
    ON f.pk_indicador = b.pk_indicador
    LEFT JOIN vlci2.aux_get_pk_indicador g
    ON a.dc_identificador_indicador_grueso = g.dc_identificador_indicador
    LEFT JOIN vlci2.t_d_indicador c
    ON g.pk_indicador = c.pk_indicador;




    DROP TABLE IF EXISTS vlci2.t_x_aggregados_002_temp_bis;
    CREATE TABLE vlci2.t_x_aggregados_002_temp_bis AS
    SELECT
        CASE WHEN fino.dc_identificador_indicador_fino IS NOT NULL THEN fino.fk_indicador_grueso ELSE agg.fk_indicador END AS fk_indicador,
        agg.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
        agg.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
        agg.fk_barrio AS fk_barrio,
        agg.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
        agg.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
        agg.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
        agg.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
        agg.nu_indicador_valor AS nu_indicador_valor,
        agg.dc_indicador_valor AS dc_indicador_valor,
        agg.etl_nombre AS etl_nombre,
        agg.fe_fecha_carga AS fe_fecha_carga,
        agg.n_meses AS n_meses,
        agg.agregue_a_anyo AS agregue_a_anyo,
        agg.es_criterio_global AS es_criterio_global,
        agg.agregue_criterios AS agregue_criterios,
        agg.es_barrio_global AS es_barrio_global,
        agg.agregue_barrios AS agregue_barrios,
        agg.replicaciones AS replicaciones
    FROM vlci2.t_x_aggregados_002_temp agg
    LEFT JOIN vlci2.t_x_clave_combinacion_criterios fino
    ON agg.fk_indicador = fino.fk_indicador_fino
    WHERE (fino.dc_identificador_indicador_fino IS NULL OR NOT (es_barrio_global AND es_criterio_global))
    ;


    DROP TABLE IF EXISTS vlci2.t_x_clave_combinacion_ratios;
    CREATE TABLE vlci2.t_x_clave_combinacion_ratios AS
    SELECT
    a.dc_identificador_indicador_resultado as dc_identificador_indicador_resultado,
    b.pk_indicador as fk_indicador_resultado,
    a.dc_identificador_indicador_numerador as dc_identificador_indicador_numerador,
    c.pk_indicador as fk_indicador_numerador,
    a.dc_identificador_indicador_denominador as dc_identificador_indicador_denominador,
    d.pk_indicador as fk_indicador_denominador
    FROM vlci2.t_m_combinados_ratios a
    LEFT JOIN vlci2.aux_get_pk_indicador f
    ON a.dc_identificador_indicador_resultado = f.dc_identificador_indicador
    LEFT JOIN vlci2.t_d_indicador b
    ON f.pk_indicador = b.pk_indicador
    LEFT JOIN vlci2.aux_get_pk_indicador g
    ON a.dc_identificador_indicador_numerador = g.dc_identificador_indicador
    LEFT JOIN vlci2.t_d_indicador c
    ON g.pk_indicador = c.pk_indicador
    LEFT JOIN vlci2.aux_get_pk_indicador h
    ON a.dc_identificador_indicador_denominador = h.dc_identificador_indicador
    LEFT JOIN vlci2.t_d_indicador d
    ON h.pk_indicador = d.pk_indicador;


    DROP TABLE IF EXISTS vlci2.t_x_con_ratios;
    CREATE TABLE vlci2.t_x_con_ratios AS
    SELECT
        ratios.fk_indicador_resultado AS fk_indicador,
        numerador.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
        numerador.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
        numerador.fk_barrio AS fk_barrio,
        numerador.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
        numerador.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
        numerador.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
        numerador.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
        numerador.nu_indicador_valor / denominador.nu_indicador_valor AS nu_indicador_valor,
        numerador.dc_indicador_valor AS dc_indicador_valor,
        numerador.etl_nombre AS etl_nombre,
        numerador.fe_fecha_carga AS fe_fecha_carga,
        numerador.n_meses AS n_meses,
        numerador.agregue_a_anyo AS agregue_a_anyo,
        numerador.es_criterio_global AS es_criterio_global,
        numerador.agregue_criterios AS agregue_criterios,
        numerador.es_barrio_global AS es_barrio_global,
        numerador.agregue_barrios AS agregue_barrios,
        numerador.replicaciones AS replicaciones
    FROM vlci2.t_x_clave_combinacion_ratios ratios
    JOIN vlci2.t_x_aggregados_002_temp_bis numerador
    ON numerador.fk_indicador = ratios.fk_indicador_numerador
    JOIN vlci2.t_x_aggregados_002_temp_bis denominador
    ON denominador.fk_indicador = ratios.fk_indicador_denominador
        AND NOT(denominador.nu_indicador_valor = 0.0)
        AND numerador.dc_periodicidad_muestreo_valor = denominador.dc_periodicidad_muestreo_valor
        AND numerador.fk_barrio = denominador.fk_barrio
        AND numerador.dc_criterio_desagregacion1_valor = denominador.dc_criterio_desagregacion1_valor
        AND numerador.dc_criterio_desagregacion2_valor = denominador.dc_criterio_desagregacion2_valor
        AND numerador.dc_criterio_desagregacion3_valor = denominador.dc_criterio_desagregacion3_valor
        AND numerador.dc_criterio_desagregacion4_valor = denominador.dc_criterio_desagregacion4_valor
    ;


    DROP TABLE IF EXISTS vlci2.t_x_aggregados_002;
    CREATE TABLE vlci2.t_x_aggregados_002 AS
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        nu_indicador_valor,
        dc_indicador_valor,
        etl_nombre,
        fe_fecha_carga,
        n_meses,
        agregue_a_anyo,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios,
        replicaciones
    FROM vlci2.t_x_con_ratios
    UNION ALL
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        nu_indicador_valor,
        dc_indicador_valor,
        etl_nombre,
        fe_fecha_carga,
        n_meses,
        agregue_a_anyo,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios,
        replicaciones
    FROM vlci2.t_x_aggregados_002_temp_bis
    ;




    DROP TABLE IF EXISTS vlci2.t_x_aggregados_002_temp;
    DROP TABLE IF EXISTS vlci2.t_x_aggregados_002_temp_bis;
    DROP TABLE IF EXISTS vlci2.h_x_agregados_temporales_001;
    DROP TABLE IF EXISTS vlci2.h_x_replicados_criterio;
    DROP TABLE IF EXISTS vlci2.h_x_replicados;

END
$procedure$
;

COMMIT;
