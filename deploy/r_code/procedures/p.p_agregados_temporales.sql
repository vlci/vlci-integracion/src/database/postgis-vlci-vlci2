-- Deploy postgis-vlci-vlci2:r_code/procedures/p.p_agregados_temporales to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.p_agregados_temporales()
 LANGUAGE plpgsql
AS $procedure$
begin
  DROP TABLE IF EXISTS vlci2.h_x_001;
  CREATE TABLE vlci2.h_x_001 AS 
    SELECT dc_orden_periodicidad AS orden_antes,
              dc_periodicidad_muestreo_cas AS cas_antes,
              dc_periodicidad_muestreo_val AS val_antes,
              pk_periodicidad_muestreo AS pk_antes,
              1 AS cross_join_hack
        FROM vlci2.t_m_periodicidad_muestreo
  ;

  DROP TABLE IF EXISTS vlci2.h_x_002;
  CREATE TABLE vlci2.h_x_002 AS 
    SELECT dc_orden_periodicidad AS orden_despues,
              dc_periodicidad_muestreo_cas AS cas_despues,
              dc_periodicidad_muestreo_val AS val_despues,
              pk_periodicidad_muestreo AS pk_despues,
              1 AS cross_join_hack
        FROM vlci2.t_m_periodicidad_muestreo
  ;

  DROP TABLE IF EXISTS vlci2.h_x_agg_desde_dia;
  CREATE TABLE vlci2.h_x_agg_desde_dia AS
    SELECT orden_antes,
            orden_despues,
            cas_antes,
            cas_despues,
            pk_antes,
            pk_despues,
            val_antes,
            val_despues
    FROM
      vlci2.h_x_001 a
    JOIN
      vlci2.h_x_002 b ON a.cross_join_hack = b.cross_join_hack
    WHERE 3 <= orden_antes
      AND orden_antes < orden_despues
      AND orden_despues <= 6
    ORDER BY orden_antes,
              orden_despues;


  DROP TABLE IF EXISTS vlci2.h_x_agg_a_anyo;
  CREATE TABLE vlci2.h_x_agg_a_anyo AS
    SELECT orden_antes,
            orden_despues,
            cas_antes,
            cas_despues,
            pk_antes,
            pk_despues,
            val_antes,
            val_despues
    FROM
      vlci2.h_x_001 a
    JOIN
      vlci2.h_x_002 b ON a.cross_join_hack = b.cross_join_hack
    WHERE 3 <= orden_antes
      AND orden_antes < orden_despues
      AND orden_despues = 9
    ORDER BY orden_antes,
              orden_despues;

  DROP TABLE IF EXISTS vlci2.h_x_001;
  DROP TABLE IF EXISTS vlci2.h_x_002;










  CREATE TABLE IF NOT EXISTS vlci2.t_m_metodo_agg ( 
  fk_indicador INT,
  agg_temporal VARCHAR(255),
  agg_criterios VARCHAR(255)
  );

  DROP TABLE IF EXISTS vlci2.tmp;
  CREATE TABLE vlci2.tmp AS
  SELECT a.pk_indicador as fk_indicador, 
    CASE WHEN upper(a.dc_criterio_desagregacion_agg) = 'AVG' THEN 'AVG'
      ELSE 'SUM'
      END AS agg_temporal,
    CASE WHEN upper(a.dc_criterio_desagregacion_agg) = 'AVG' THEN 'AVG'
      ELSE 'SUM'
      END AS agg_criterios
  FROM vlci2.t_d_indicador a
  LEFT JOIN vlci2.t_m_metodo_agg b
  ON a.pk_indicador = b.fk_indicador
  WHERE b.agg_temporal IS NULL
  UNION ALL
  SELECT * FROM vlci2.t_m_metodo_agg
  ;

  DROP TABLE IF EXISTS vlci2.t_m_metodo_agg;
  CREATE TABLE vlci2.t_m_metodo_agg AS
  SELECT fk_indicador,
    agg_temporal,
    agg_criterios
  FROM vlci2.tmp;

  DROP TABLE IF EXISTS vlci2.tmp;









  DROP TABLE IF EXISTS vlci2.h_x_monthly_replicated;
  CREATE TABLE vlci2.h_x_monthly_replicated AS
    SELECT fk_indicador,
          fk_periodicidad_muestreo,
          periodo_origen,
          calculatedPeriod AS dc_periodicidad_muestreo_valor,
          fk_barrio,
          dc_criterio_desagregacion1_valor,
          dc_criterio_desagregacion2_valor,
          dc_criterio_desagregacion3_valor,
          dc_criterio_desagregacion4_valor,
          NULL AS dc_indicador_valor,
          nu_indicador_valor,
          etl_nombre,
          fe_fecha_carga,
          tommorrows_period,
          max(tommorrows_period) OVER (PARTITION BY fk_indicador) AS last_tommorrows_period
    FROM
      (SELECT t_f_indicador_valor.fk_indicador AS fk_indicador,
              h_x_agg_desde_dia.pk_despues AS fk_periodicidad_muestreo,
              t_f_indicador_valor.dc_periodicidad_muestreo_valor AS periodo_origen,
              CASE WHEN h_x_agg_desde_dia.pk_antes = 6 THEN
              CASE
                  WHEN h_x_agg_desde_dia.pk_despues = 7 THEN concat(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,7,4),substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,4,2))
                  WHEN h_x_agg_desde_dia.pk_despues = 14 THEN concat(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,7,4),substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,4,2),floor((cast(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,2) AS INT) + 6) / 7),'s')
                  WHEN h_x_agg_desde_dia.pk_despues = 13 THEN concat(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,7,4),substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,4,2),CASE
                                                                                                                                                                                                        WHEN cast(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,2) AS INT) <= 15 THEN '1'
                                                                                                                                                                                                        ELSE '2'
                                                                                                                                                                                                    END,'q')
                  ELSE NULL
              END
              WHEN h_x_agg_desde_dia.pk_antes = 13 THEN concat(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,4),substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,5,2))
              WHEN h_x_agg_desde_dia.pk_antes = 14 THEN
                    CASE
                        WHEN h_x_agg_desde_dia.pk_despues = 7 THEN concat(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,4),substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,5,2))
                        WHEN h_x_agg_desde_dia.pk_despues = 13 THEN concat(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,6),CASE WHEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,7,1)='1' OR substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,7,1)='2' THEN '1q' ELSE '2q' END)
                        ELSE NULL
                    END
              ELSE NULL END AS calculatedPeriod,
              CASE
                  WHEN h_x_agg_desde_dia.pk_despues = 7 THEN concat(substr(one_day_of_next_period,1,4),substr(one_day_of_next_period,5,2))
                  WHEN h_x_agg_desde_dia.pk_despues = 14 THEN concat(substr(one_day_of_next_period,1,4),substr(one_day_of_next_period,5,2),floor((cast(substr(one_day_of_next_period,7,2) AS INT) + 6) / 7),'s')
                  WHEN h_x_agg_desde_dia.pk_despues = 13 THEN concat(substr(one_day_of_next_period,1,4),substr(one_day_of_next_period,5,2),CASE
                                                                                                                                                WHEN cast(substr(one_day_of_next_period,7,2) AS INT) <= 15 THEN '1'
                                                                                                                                                ELSE '2'
                                                                                                                                            END,'q')
                  ELSE NULL
              END AS tommorrows_period,
                t_f_indicador_valor.nu_indicador_valor AS nu_indicador_valor,
                t_f_indicador_valor.fk_barrio AS fk_barrio,
                t_f_indicador_valor.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
                t_f_indicador_valor.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
                t_f_indicador_valor.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
                t_f_indicador_valor.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
                t_f_indicador_valor.etl_nombre AS etl_nombre,
                t_f_indicador_valor.fe_fecha_carga AS fe_fecha_carga
        FROM (SELECT
                fk_indicador,
                fk_periodicidad_muestreo,
                dc_periodicidad_muestreo_valor,
                fk_barrio,
                dc_criterio_desagregacion1_valor,
                dc_criterio_desagregacion2_valor,
                dc_criterio_desagregacion3_valor,
                dc_criterio_desagregacion4_valor,
                dc_indicador_valor,
                nu_indicador_valor,
                etl_nombre,
                fe_fecha_carga,
                CASE
                  WHEN fk_periodicidad_muestreo = 6 THEN to_char((to_date(dc_periodicidad_muestreo_valor,'dd/MM/yyyy') + CAST('1 day' AS INTERVAL)),'yyyyMMdd')
          --WHEN fk_periodicidad_muestreo = 6 THEN date_format(date_add(from_unixtime(unix_timestamp(dc_periodicidad_muestreo_valor,'dd/MM/yyyy')),1),'yyyyMMdd')
                  --WHEN fk_periodicidad_muestreo = 13 THEN date_format(date_add(from_unixtime(unix_timestamp(concat(substr(dc_periodicidad_muestreo_valor,1,6),cast((cast(substr(dc_periodicidad_muestreo_valor,7,1) AS INT)-1)*15+1 AS STRING)), 'yyyyMMdd')),16),'yyyyMMdd')
                  --WHEN fk_periodicidad_muestreo = 14 THEN date_format(date_add(from_unixtime(unix_timestamp(concat(substr(dc_periodicidad_muestreo_valor,1,6),cast((cast(substr(dc_periodicidad_muestreo_valor,7,1) AS INT)-1)*7+1 AS STRING)), 'yyyyMMdd')),7),'yyyyMMdd')
          -- Comentamos las periodicidades 13 y 14 puesto que no tenemos ningún ejemplo real de como llegarian los datos y son metodos de HIVE. La 6 esta migrada a Postgres.
                ELSE NULL END AS one_day_of_next_period
              FROM vlci2.t_x_hechos) t_f_indicador_valor
        JOIN vlci2.t_d_indicador t_d_indicador
        ON t_f_indicador_valor.fk_indicador = t_d_indicador.pk_indicador
        JOIN vlci2.h_x_agg_desde_dia h_x_agg_desde_dia
        ON t_d_indicador.fk_periodicidad_muestreo = h_x_agg_desde_dia.pk_antes
        ) b;

  DROP TABLE IF EXISTS vlci2.h_x_monthly_replicated_001;
  CREATE TABLE vlci2.h_x_monthly_replicated_001 AS SELECT
    fk_indicador,
    fk_periodicidad_muestreo,
    dc_periodicidad_muestreo_valor,
    fk_barrio,
    dc_criterio_desagregacion1_valor,
    dc_criterio_desagregacion2_valor,
    dc_criterio_desagregacion3_valor,
    dc_criterio_desagregacion4_valor,
    dc_indicador_valor,
    nu_indicador_valor,
    etl_nombre,
    fe_fecha_carga,
    last_tommorrows_period
  FROM vlci2.h_x_monthly_replicated
  WHERE dc_periodicidad_muestreo_valor < last_tommorrows_period
  ;

  DROP TABLE IF EXISTS vlci2.h_x_monthly_aggregated;
  CREATE TABLE vlci2.h_x_monthly_aggregated AS SELECT
  a.fk_indicador AS fk_indicador,
  a.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
  a.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
  a.fk_barrio AS fk_barrio,
  a.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
  a.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
  a.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
  a.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
  NULL AS dc_indicador_valor,
  CASE WHEN upper(b.agg_temporal) = 'AVG' THEN avg(a.nu_indicador_valor)
                ELSE sum(a.nu_indicador_valor)
            END AS nu_indicador_valor,
  max(a.etl_nombre) AS etl_nombre,
  max(a.fe_fecha_carga) AS fe_fecha_carga
  FROM vlci2.h_x_monthly_replicated_001 a
  LEFT JOIN vlci2.t_m_metodo_agg b
  ON b.fk_indicador = a.fk_indicador
  GROUP BY a.fk_indicador,
      a.fk_periodicidad_muestreo,
      a.dc_periodicidad_muestreo_valor,
      a.fk_barrio,
      a.dc_criterio_desagregacion1_valor,
      a.dc_criterio_desagregacion2_valor,
      a.dc_criterio_desagregacion3_valor,
      a.dc_criterio_desagregacion4_valor,
      b.agg_temporal
  ;











        




  DROP TABLE IF EXISTS vlci2.h_x_yearly_replicated;
  CREATE TABLE vlci2.h_x_yearly_replicated AS
    SELECT fk_indicador,
          fk_periodicidad_muestreo,
          periodo_origen,
          calculatedPeriod AS dc_periodicidad_muestreo_valor,
          fk_barrio,
          dc_criterio_desagregacion1_valor,
          dc_criterio_desagregacion2_valor,
          dc_criterio_desagregacion3_valor,
          dc_criterio_desagregacion4_valor,
          NULL AS dc_indicador_valor,
          nu_indicador_valor,
          etl_nombre,
          fe_fecha_carga,
          next_period,
          max(next_period) OVER (PARTITION BY fk_indicador) AS last_next_period,
          n_meses_id,
          multiplicador
    FROM
    (SELECT
      t_f_indicador_valor.fk_indicador AS fk_indicador,
      h_x_agg_a_anyo.pk_despues AS fk_periodicidad_muestreo,
      t_f_indicador_valor.dc_periodicidad_muestreo_valor AS periodo_origen,
      CASE
          WHEN h_x_agg_a_anyo.pk_antes = 6 THEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,7,4)
          WHEN h_x_agg_a_anyo.pk_antes = 7 THEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,4,4)
          WHEN h_x_agg_a_anyo.pk_antes = 8 THEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,3,4)
          WHEN h_x_agg_a_anyo.pk_antes = 9 THEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,3,4)
          WHEN h_x_agg_a_anyo.pk_antes = 13 THEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,4)
          WHEN h_x_agg_a_anyo.pk_antes = 14 THEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,4)
          ELSE NULL
      END AS calculatedPeriod,
      CASE
      WHEN h_x_agg_a_anyo.pk_antes = 6 THEN substr(to_char((to_date(t_f_indicador_valor.dc_periodicidad_muestreo_valor,'dd/MM/yyyy') + CAST('1 day' AS INTERVAL)),'yyyyMMdd'),1,4)
          -- WHEN h_x_agg_a_anyo.pk_antes = 6 THEN substr(date_format(date_add(from_unixtime(unix_timestamp(t_f_indicador_valor.dc_periodicidad_muestreo_valor, 'dd/MM/yyyy')),1),'yyyyMMdd'),1,4)
          WHEN h_x_agg_a_anyo.pk_antes = 7 THEN CASE WHEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,2)='12' THEN cast((cast(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,4,4) AS INTEGER) + 1) AS TEXT) ELSE substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,4,4) END
          WHEN h_x_agg_a_anyo.pk_antes = 8 THEN CASE WHEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,1)='4' THEN cast((cast(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,3,4) AS INTEGER) + 1) AS TEXT) ELSE substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,3,4) END
          WHEN h_x_agg_a_anyo.pk_antes = 9 THEN CASE WHEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,1)='2' THEN cast((cast(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,3,4) AS INTEGER) + 1) AS TEXT) ELSE substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,3,4) END
          WHEN h_x_agg_a_anyo.pk_antes = 13 THEN CASE WHEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,5,4)='122q' THEN cast((cast(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,4) AS INTEGER) + 1) AS TEXT) ELSE substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,4) END
          WHEN h_x_agg_a_anyo.pk_antes = 14 THEN CASE WHEN substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,5,4)='125s' THEN cast((cast(substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,4) AS INTEGER) + 1) AS TEXT) ELSE substr(t_f_indicador_valor.dc_periodicidad_muestreo_valor,1,4) END
          ELSE NULL
      END AS next_period,
      t_f_indicador_valor.dc_periodicidad_muestreo_valor AS n_meses_id,
      CASE
          WHEN h_x_agg_a_anyo.pk_antes = 6 THEN 12.0/365.0
          WHEN h_x_agg_a_anyo.pk_antes = 7 THEN 1.0
          WHEN h_x_agg_a_anyo.pk_antes = 8 THEN 3.0
          WHEN h_x_agg_a_anyo.pk_antes = 9 THEN 6.0
          WHEN h_x_agg_a_anyo.pk_antes = 13 THEN 0.5
          WHEN h_x_agg_a_anyo.pk_antes = 14 THEN 12.0/59.0
          ELSE NULL
      END AS multiplicador,
      t_f_indicador_valor.nu_indicador_valor AS nu_indicador_valor,
      t_f_indicador_valor.fk_barrio AS fk_barrio,
      t_f_indicador_valor.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
      t_f_indicador_valor.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
      t_f_indicador_valor.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
      t_f_indicador_valor.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
      t_f_indicador_valor.etl_nombre AS etl_nombre,
      t_f_indicador_valor.fe_fecha_carga AS fe_fecha_carga
    FROM vlci2.t_x_hechos t_f_indicador_valor
    JOIN vlci2.t_d_indicador t_d_indicador ON t_f_indicador_valor.fk_indicador = t_d_indicador.pk_indicador
    JOIN vlci2.h_x_agg_a_anyo h_x_agg_a_anyo ON t_d_indicador.fk_periodicidad_muestreo = h_x_agg_a_anyo.pk_antes ) b;

  DROP TABLE IF EXISTS vlci2.h_x_yearly_replicated_001;
  CREATE TABLE vlci2.h_x_yearly_replicated_001 AS SELECT
    fk_indicador,
    fk_periodicidad_muestreo,
    dc_periodicidad_muestreo_valor,
    fk_barrio,
    dc_criterio_desagregacion1_valor,
    dc_criterio_desagregacion2_valor,
    dc_criterio_desagregacion3_valor,
    dc_criterio_desagregacion4_valor,
    dc_indicador_valor,
    nu_indicador_valor,
    etl_nombre,
    fe_fecha_carga,
    last_next_period,
    n_meses_id,
    multiplicador
  FROM vlci2.h_x_yearly_replicated
  WHERE dc_periodicidad_muestreo_valor < last_next_period
  ;

  DROP TABLE IF EXISTS vlci2.h_x_yearly_aggregated;
  CREATE TABLE vlci2.h_x_yearly_aggregated AS SELECT
  a.fk_indicador AS fk_indicador,
  a.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
  a.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
  a.fk_barrio AS fk_barrio,
  a.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
  a.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
  a.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
  a.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
  NULL AS dc_indicador_valor,
  CASE WHEN upper(b.agg_temporal) = 'AVG' THEN avg(a.nu_indicador_valor)
                ELSE sum(a.nu_indicador_valor)
            END AS nu_indicador_valor,
  max(a.etl_nombre) AS etl_nombre,
  max(a.fe_fecha_carga) AS fe_fecha_carga,
  a.multiplicador * count(DISTINCT n_meses_id) as n_meses
  FROM vlci2.h_x_yearly_replicated_001 a
  JOIN vlci2.t_m_metodo_agg b
  ON b.fk_indicador = a.fk_indicador
  GROUP BY a.fk_indicador,
      a.fk_periodicidad_muestreo,
      a.dc_periodicidad_muestreo_valor,
      a.fk_barrio,
      a.dc_criterio_desagregacion1_valor,
      a.dc_criterio_desagregacion2_valor,
      a.dc_criterio_desagregacion3_valor,
      a.dc_criterio_desagregacion4_valor,
      a.multiplicador,
      b.agg_temporal
  ;








  DROP TABLE IF EXISTS vlci2.t_x_hechos_001;
  CREATE TABLE vlci2.t_x_hechos_001 AS SELECT
  fk_indicador,
  fk_periodicidad_muestreo,
  CASE
          WHEN fk_periodicidad_muestreo = 2 THEN dc_periodicidad_muestreo_valor
          WHEN fk_periodicidad_muestreo = 3 THEN dc_periodicidad_muestreo_valor
          WHEN fk_periodicidad_muestreo = 4 THEN dc_periodicidad_muestreo_valor
          WHEN fk_periodicidad_muestreo = 5 THEN dc_periodicidad_muestreo_valor
          WHEN fk_periodicidad_muestreo = 6 THEN concat(substr(dc_periodicidad_muestreo_valor,7,4),substr(dc_periodicidad_muestreo_valor,4,2),substr(dc_periodicidad_muestreo_valor,1,2))
          WHEN fk_periodicidad_muestreo = 7 THEN concat(substr(dc_periodicidad_muestreo_valor,4,4),substr(dc_periodicidad_muestreo_valor,1,2))
          WHEN fk_periodicidad_muestreo = 8 THEN concat(substr(dc_periodicidad_muestreo_valor,3,4),substr(dc_periodicidad_muestreo_valor,1,1))
          WHEN fk_periodicidad_muestreo = 9 THEN concat(substr(dc_periodicidad_muestreo_valor,3,4),substr(dc_periodicidad_muestreo_valor,1,1))
          WHEN fk_periodicidad_muestreo = 13 THEN dc_periodicidad_muestreo_valor
          WHEN fk_periodicidad_muestreo = 14 THEN dc_periodicidad_muestreo_valor
          ELSE NULL
          END AS dc_periodicidad_muestreo_valor,
  fk_barrio,
  dc_criterio_desagregacion1_valor,
  dc_criterio_desagregacion2_valor,
  dc_criterio_desagregacion3_valor,
  dc_criterio_desagregacion4_valor,
  dc_indicador_valor,
  nu_indicador_valor,
  etl_nombre,
  fe_fecha_carga
  FROM vlci2.t_x_hechos;









  DROP TABLE IF EXISTS vlci2.t_x_aggregados;
  CREATE TABLE vlci2.t_x_aggregados AS
  SELECT
    fk_indicador,
    fk_periodicidad_muestreo,
    dc_periodicidad_muestreo_valor,
    fk_barrio,
    dc_criterio_desagregacion1_valor,
    dc_criterio_desagregacion2_valor,
    dc_criterio_desagregacion3_valor,
    dc_criterio_desagregacion4_valor,
    dc_indicador_valor,
    nu_indicador_valor,
    etl_nombre,
    fe_fecha_carga,
    0 AS n_meses,
    false AS agregue_a_anyo
  FROM vlci2.t_x_hechos_001
  UNION ALL
  SELECT
    fk_indicador,
    fk_periodicidad_muestreo,
    dc_periodicidad_muestreo_valor,
    fk_barrio,
    dc_criterio_desagregacion1_valor,
    dc_criterio_desagregacion2_valor,
    dc_criterio_desagregacion3_valor,
    dc_criterio_desagregacion4_valor,
    dc_indicador_valor,
    nu_indicador_valor,
    etl_nombre,
    fe_fecha_carga,
    0 AS n_meses,
    false AS agregue_a_anyo
  FROM vlci2.h_x_monthly_aggregated
  UNION ALL
  SELECT
    fk_indicador,
    fk_periodicidad_muestreo,
    dc_periodicidad_muestreo_valor,
    fk_barrio,
    dc_criterio_desagregacion1_valor,
    dc_criterio_desagregacion2_valor,
    dc_criterio_desagregacion3_valor,
    dc_criterio_desagregacion4_valor,
    dc_indicador_valor,
    nu_indicador_valor,
    etl_nombre,
    fe_fecha_carga,
    n_meses,
    true AS agregue_a_anyo
  FROM vlci2.h_x_yearly_aggregated
  ;








  DROP TABLE IF EXISTS vlci2.t_x_clave_combinacion_temporal;
  CREATE TABLE vlci2.t_x_clave_combinacion_temporal AS
  SELECT
  a.dc_identificador_indicador_fino as dc_identificador_indicador_fino,
  b.pk_indicador as fk_indicador_fino,
  b.fk_periodicidad_muestreo as fk_periodicidad_muestreo_fino,
  cast(d.dc_orden_periodicidad AS INT) as dc_orden_periodicidad_fino,
  a.dc_identificador_indicador_grueso as dc_identificador_indicador_grueso,
  c.pk_indicador as fk_indicador_grueso,
  c.fk_periodicidad_muestreo as fk_periodicidad_muestreo_grueso,
  cast(e.dc_orden_periodicidad AS INT) as dc_orden_periodicidad_grueso
  FROM vlci2.t_m_combinados_temporales a
  LEFT JOIN vlci2.aux_get_pk_indicador f
  ON a.dc_identificador_indicador_fino = f.dc_identificador_indicador
  LEFT JOIN vlci2.t_d_indicador b
  ON f.pk_indicador = b.pk_indicador
  LEFT JOIN vlci2.aux_get_pk_indicador g
  ON a.dc_identificador_indicador_grueso = g.dc_identificador_indicador
  LEFT JOIN vlci2.t_d_indicador c
  ON g.pk_indicador = c.pk_indicador
  LEFT JOIN vlci2.t_m_periodicidad_muestreo d
  ON b.fk_periodicidad_muestreo = d.pk_periodicidad_muestreo
  LEFT JOIN vlci2.t_m_periodicidad_muestreo e
  ON c.fk_periodicidad_muestreo = e.pk_periodicidad_muestreo;


  DROP TABLE IF EXISTS vlci2.t_x_aggregados_001;
  CREATE TABLE vlci2.t_x_aggregados_001 AS
  SELECT
  CASE WHEN fino.dc_identificador_indicador_fino IS NOT NULL THEN fino.fk_indicador_grueso ELSE agg.fk_indicador END AS fk_indicador,
    agg.fk_periodicidad_muestreo as fk_periodicidad_muestreo,
    agg.dc_periodicidad_muestreo_valor as dc_periodicidad_muestreo_valor,
    agg.fk_barrio as fk_barrio,
    agg.dc_criterio_desagregacion1_valor as dc_criterio_desagregacion1_valor,
    agg.dc_criterio_desagregacion2_valor as dc_criterio_desagregacion2_valor,
    agg.dc_criterio_desagregacion3_valor as dc_criterio_desagregacion3_valor,
    agg.dc_criterio_desagregacion4_valor as dc_criterio_desagregacion4_valor,
    agg.dc_indicador_valor as dc_indicador_valor,
    agg.nu_indicador_valor as nu_indicador_valor,
    agg.etl_nombre as etl_nombre,
    agg.fe_fecha_carga as fe_fecha_carga,
    agg.n_meses as n_meses,
    agg.agregue_a_anyo as agregue_a_anyo
  FROM vlci2.t_x_aggregados agg
  LEFT JOIN vlci2.t_x_clave_combinacion_temporal fino
  ON agg.fk_indicador = fino.fk_indicador_fino
  JOIN vlci2.t_m_periodicidad_muestreo perio
  ON agg.fk_periodicidad_muestreo = perio.pk_periodicidad_muestreo
  WHERE (fino.dc_identificador_indicador_fino IS NULL OR perio.dc_orden_periodicidad < fino.dc_orden_periodicidad_grueso)
  ;


  DROP TABLE IF EXISTS vlci2.h_x_agg_a_anyo;
  DROP TABLE IF EXISTS vlci2.h_x_agg_desde_dia;
  DROP TABLE IF EXISTS vlci2.h_x_monthly_replicated;
  DROP TABLE IF EXISTS vlci2.h_x_monthly_replicated_001;
  DROP TABLE IF EXISTS vlci2.h_x_monthly_aggregated;
  DROP TABLE IF EXISTS vlci2.h_x_yearly_replicated;
  DROP TABLE IF EXISTS vlci2.h_x_yearly_replicated_001;
  DROP TABLE IF EXISTS vlci2.h_x_yearly_aggregated;
  DROP TABLE IF EXISTS vlci2.t_x_hechos_001;
  DROP TABLE IF EXISTS vlci2.t_x_aggregados;
  DROP TABLE IF EXISTS vlci2.t_x_clave_combinacion_temporal;

END
$procedure$
;

COMMIT;
