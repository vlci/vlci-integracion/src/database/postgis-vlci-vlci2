-- Deploy postgis-vlci-vlci2:r_code/procedures/p.p_alertado to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.p_alertado()
 LANGUAGE plpgsql
AS $procedure$
begin
    DROP TABLE IF EXISTS vlci2.t_d_alertado_001;
    CREATE TABLE vlci2.t_d_alertado_001 AS SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        CASE
                WHEN fk_periodicidad_muestreo = 2 THEN dc_periodicidad_muestreo_valor
                WHEN fk_periodicidad_muestreo = 3 THEN dc_periodicidad_muestreo_valor
                WHEN fk_periodicidad_muestreo = 4 THEN dc_periodicidad_muestreo_valor
                WHEN fk_periodicidad_muestreo = 5 THEN dc_periodicidad_muestreo_valor
                WHEN fk_periodicidad_muestreo = 6 THEN concat(substr(dc_periodicidad_muestreo_valor,7,4),substr(dc_periodicidad_muestreo_valor,4,2),substr(dc_periodicidad_muestreo_valor,1,2))
                WHEN fk_periodicidad_muestreo = 7 THEN concat(substr(dc_periodicidad_muestreo_valor,4,4),substr(dc_periodicidad_muestreo_valor,1,2))
                WHEN fk_periodicidad_muestreo = 8 THEN concat(substr(dc_periodicidad_muestreo_valor,3,4),substr(dc_periodicidad_muestreo_valor,1,1))
                WHEN fk_periodicidad_muestreo = 9 THEN concat(substr(dc_periodicidad_muestreo_valor,3,4),substr(dc_periodicidad_muestreo_valor,1,1))
                ELSE NULL
                END AS dc_periodicidad_muestreo_valor
    FROM vlci2.t_x_hechos
    ;


    DROP TABLE IF EXISTS vlci2.t_d_alertado_002;
    CREATE TABLE vlci2.t_d_alertado_002 AS SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        max(dc_periodicidad_muestreo_valor) as last_period
    FROM vlci2.t_d_alertado_001
    group by fk_indicador,
        fk_periodicidad_muestreo
    ;

    DROP TABLE IF EXISTS vlci2.t_d_alertado_003;
    CREATE TABLE vlci2.t_d_alertado_003 AS SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        last_period,
        CASE
            WHEN fk_periodicidad_muestreo = 2 THEN cast(cast(last_period as int) + 1 AS TEXT)
            WHEN fk_periodicidad_muestreo = 3 THEN cast(cast(last_period as int) + 4 AS TEXT)
            WHEN fk_periodicidad_muestreo = 4 THEN cast(cast(last_period as int) + 5 AS TEXT)
            WHEN fk_periodicidad_muestreo = 5 THEN cast(cast(last_period as int) + 10 AS TEXT)
            WHEN fk_periodicidad_muestreo = 6 THEN cast(TO_DATE(last_period,'YYYYMMDD') + INTERVAL '1 day' AS TEXT)
            WHEN fk_periodicidad_muestreo = 7 THEN CASE WHEN substr(last_period,5,2) = '12' THEN concat(cast(cast(substr(last_period,1,4) as INT) + 1 as INT),'01') ELSE cast(cast(last_period as INT) + 1 AS TEXT) END
            WHEN fk_periodicidad_muestreo = 8 THEN CASE WHEN substr(last_period,5,1) = '4' THEN concat(cast(cast(substr(last_period,1,4) as INT) + 1 as INT),'1') ELSE cast(cast(last_period as INT) + 1 AS TEXT) END
            WHEN fk_periodicidad_muestreo = 9 THEN CASE WHEN substr(last_period,5,1) = '2' THEN concat(cast(cast(substr(last_period,1,4) as INT) + 1 as INT),'1') ELSE cast(cast(last_period as INT) + 1 AS TEXT) END
        ELSE NULL
        END AS next_period
    FROM vlci2.t_d_alertado_002
    ;

    DROP TABLE IF EXISTS vlci2.t_d_alertado_004;
    CREATE TABLE vlci2.t_d_alertado_004 AS SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        last_period,
        next_period,
        CASE
            WHEN fk_periodicidad_muestreo = 2 THEN to_char(current_date,'yyyy')
            WHEN fk_periodicidad_muestreo = 3 THEN to_char(current_date,'yyyy')
            WHEN fk_periodicidad_muestreo = 4 THEN to_char(current_date,'yyyy')
            WHEN fk_periodicidad_muestreo = 5 THEN to_char(current_date,'yyyy')
            WHEN fk_periodicidad_muestreo = 6 THEN to_char(current_date,'yyyyMMdd')
            WHEN fk_periodicidad_muestreo = 7 THEN to_char(current_date,'yyyyMM')
            WHEN fk_periodicidad_muestreo = 8 THEN concat(to_char(current_date,'yyyy'), (2 + cast(to_char(current_date,'MM') as INT)) / 3)
            WHEN fk_periodicidad_muestreo = 9 THEN concat(to_char(current_date,'yyyy'), (5 + cast(to_char(current_date,'MM') as INT)) / 6)
        ELSE NULL
        END AS todays_period
    FROM vlci2.t_d_alertado_003
    WHERE next_period IS NOT NULL
    ;

    DROP TABLE IF EXISTS vlci2.t_d_alertado_005;
    CREATE TABLE vlci2.t_d_alertado_005 AS SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        last_period,
        next_period,
        todays_period
    FROM vlci2.t_d_alertado_004
    WHERE todays_period > next_period
    ;

    DROP TABLE IF EXISTS vlci2.t_d_alertado_006;
    CREATE TABLE vlci2.t_d_alertado_006 AS SELECT
        a.fk_indicador as fk_indicador,
        a.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
        a.last_period as last_period,
        a.next_period as next_period,
        a.todays_period as todays_period,
        b.dc_identificador_indicador as dc_identificador_indicador
    FROM vlci2.t_d_alertado_005 a
    LEFT JOIN vlci2.t_d_indicador b
    ON a.fk_indicador = b.pk_indicador;

    DROP TABLE IF EXISTS vlci2.t_d_monitorizacion;
    CREATE TABLE vlci2.t_d_monitorizacion AS SELECT
        a.fk_indicador as fk_indicador,
        a.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
        a.last_period as last_period,
        a.next_period as next_period,
        a.todays_period as todays_period,
        a.dc_identificador_indicador as dc_identificador_indicador,
        b.dc_periodicidad_muestreo_cas as dc_periodicidad_muestreo_cas
    FROM vlci2.t_d_alertado_006 a
    LEFT JOIN vlci2.t_m_periodicidad_muestreo b
    ON a.fk_periodicidad_muestreo = b.pk_periodicidad_muestreo
    ;

    DROP TABLE IF EXISTS vlci2.t_d_alertado;
    CREATE TABLE vlci2.t_d_alertado AS SELECT
        a.fk_indicador as fk_indicador,
        a.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
        a.last_period as last_period,
        a.next_period as next_period,
        a.todays_period as todays_period,
        a.dc_identificador_indicador as dc_identificador_indicador,
        a.dc_periodicidad_muestreo_cas as dc_periodicidad_muestreo_cas
    FROM vlci2.t_d_monitorizacion a
    LEFT JOIN vlci2.t_x_indicadores_silencio c
    ON a.dc_identificador_indicador = c.dc_identificador_indicador
    WHERE c.dc_identificador_indicador IS NULL
    ;

    DROP TABLE IF EXISTS  vlci2.t_d_alertado_001;
    DROP TABLE IF EXISTS  vlci2.t_d_alertado_002;
    DROP TABLE IF EXISTS  vlci2.t_d_alertado_003;
    DROP TABLE IF EXISTS  vlci2.t_d_alertado_004;
    DROP TABLE IF EXISTS  vlci2.t_d_alertado_005;
    DROP TABLE IF EXISTS  vlci2.t_d_alertado_006;

END
$procedure$
;

COMMIT;
