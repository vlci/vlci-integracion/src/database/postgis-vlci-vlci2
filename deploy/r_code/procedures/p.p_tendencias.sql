-- Deploy postgis-vlci-vlci2:r_code/procedures/p.p_tendencias to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.p_tendencias()
 LANGUAGE plpgsql
AS $procedure$
begin
    DROP TABLE IF EXISTS vlci2.tendencias_auxiliar_001;
    CREATE TABLE vlci2.tendencias_auxiliar_001 AS 
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        cast(dc_periodicidad_muestreo_valor AS INT) AS dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        nu_indicador_valor,
        n_meses,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios
    FROM vlci2.t_x_aggregados_002 h_x_agregados
    LEFT JOIN vlci2.t_m_periodicidad_muestreo t_m_periodicidad_muestreo
    ON h_x_agregados.fk_periodicidad_muestreo = t_m_periodicidad_muestreo.pk_periodicidad_muestreo
    WHERE cast(t_m_periodicidad_muestreo.dc_orden_periodicidad AS INT) >= 9
    ;


    DROP TABLE IF EXISTS vlci2.tendencias_auxiliar_002;
    CREATE TABLE vlci2.tendencias_auxiliar_002 AS 
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        nu_indicador_valor,
        n_meses,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios,
        row_number() OVER (PARTITION BY 
            fk_indicador,
            fk_periodicidad_muestreo,
            fk_barrio,
            dc_criterio_desagregacion1_valor,
            dc_criterio_desagregacion2_valor,
            dc_criterio_desagregacion3_valor,
            dc_criterio_desagregacion4_valor,
            es_barrio_global
        ORDER BY dc_periodicidad_muestreo_valor DESC) AS rn
    FROM vlci2.tendencias_auxiliar_001
    ;


    DROP TABLE IF EXISTS vlci2.tendencias_valor_actual;
    CREATE TABLE vlci2.tendencias_valor_actual AS 
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        nu_indicador_valor,
        n_meses,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios
    FROM vlci2.tendencias_auxiliar_002
    WHERE rn = 1
    ;


    DROP TABLE IF EXISTS vlci2.tendencias_valor_previo;
    CREATE TABLE vlci2.tendencias_valor_previo AS 
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        nu_indicador_valor,
        n_meses,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios
    FROM vlci2.tendencias_auxiliar_002
    WHERE rn = 2
    ;


    DROP TABLE IF EXISTS vlci2.tendencias_auxiliar_003;
    CREATE TABLE vlci2.tendencias_auxiliar_003 AS 
    SELECT
        a.fk_indicador AS fk_indicador,
        a.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
        a.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
        a.fk_barrio AS fk_barrio,
        a.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
        a.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
        a.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
        a.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
        a.nu_indicador_valor AS nu_indicador_valor,
        a.n_meses AS n_meses,
        a.es_criterio_global AS es_criterio_global,
        a.agregue_criterios AS agregue_criterios,
        a.es_barrio_global AS es_barrio_global,
        a.agregue_barrios AS agregue_barrios,
        a.rn AS rn,
        b.nu_indicador_valor AS actual,
        c.nu_indicador_valor AS previo,
        b.n_meses AS actual_n_meses,
        c.n_meses AS previo_n_meses
    FROM vlci2.tendencias_auxiliar_002 a
    LEFT JOIN vlci2.tendencias_valor_actual b
    ON a.fk_indicador = b.fk_indicador
        AND a.fk_periodicidad_muestreo = b.fk_periodicidad_muestreo
        AND a.fk_barrio = b.fk_barrio
        AND a.dc_criterio_desagregacion1_valor = b.dc_criterio_desagregacion1_valor
        AND a.dc_criterio_desagregacion2_valor = b.dc_criterio_desagregacion2_valor
        AND a.dc_criterio_desagregacion3_valor = b.dc_criterio_desagregacion3_valor
        AND a.dc_criterio_desagregacion4_valor = b.dc_criterio_desagregacion4_valor
        AND a.es_barrio_global = b.es_barrio_global
    LEFT JOIN vlci2.tendencias_valor_previo c
    ON a.fk_indicador = c.fk_indicador
        AND a.fk_periodicidad_muestreo = c.fk_periodicidad_muestreo
        AND a.fk_barrio = c.fk_barrio
        AND a.dc_criterio_desagregacion1_valor = c.dc_criterio_desagregacion1_valor
        AND a.dc_criterio_desagregacion2_valor = c.dc_criterio_desagregacion2_valor
        AND a.dc_criterio_desagregacion3_valor = c.dc_criterio_desagregacion3_valor
        AND a.dc_criterio_desagregacion4_valor = c.dc_criterio_desagregacion4_valor
        AND a.es_barrio_global = c.es_barrio_global
    ;


    DROP TABLE IF EXISTS vlci2.tendencias_auxiliar_004;
    CREATE TABLE vlci2.tendencias_auxiliar_004 AS 
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        nu_indicador_valor,
        n_meses,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios,
        rn,
        actual,
        previo,
        actual_n_meses,
        previo_n_meses,
        CASE
            WHEN previo < actual THEN 'Ascendente'
            WHEN previo > actual THEN 'Descendente'
            ELSE NULL
        END AS tendencia_real
    FROM vlci2.tendencias_auxiliar_003
    ;

    DROP TABLE IF EXISTS vlci2.t_f_indicador_umbral_aux_001;
    CREATE TABLE vlci2.t_f_indicador_umbral_aux_001 AS 
    SELECT
        fk_indicador,
        de_tendencia_deseada_cas,
        nu_indicador_umbral_inferior,
        nu_indicador_umbral_superior,
        fk_dia_inicio,
        row_number() OVER (PARTITION BY fk_indicador ORDER BY fk_dia_inicio DESC) AS rn
    FROM vlci2.t_f_indicador_umbral;


    DROP TABLE IF EXISTS vlci2.t_f_indicador_umbral_aux_002;
    CREATE TABLE vlci2.t_f_indicador_umbral_aux_002 AS 
    SELECT
        fk_indicador,
        de_tendencia_deseada_cas,
        nu_indicador_umbral_inferior,
        nu_indicador_umbral_superior
    FROM vlci2.t_f_indicador_umbral_aux_001
    WHERE rn = 1;


    DROP TABLE IF EXISTS vlci2.tendencias_auxiliar_005;
    CREATE TABLE vlci2.tendencias_auxiliar_005 AS 
    SELECT
        a.fk_indicador AS fk_indicador,
        a.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
        a.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
        a.fk_barrio AS fk_barrio,
        a.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
        a.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
        a.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
        a.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
        a.nu_indicador_valor AS nu_indicador_valor,
        a.n_meses AS n_meses,
        a.rn AS rn,
        a.es_criterio_global AS es_criterio_global,
        a.agregue_criterios AS agregue_criterios,
        a.es_barrio_global AS es_barrio_global,
        a.agregue_barrios AS agregue_barrios,
        a.actual AS actual,
        a.previo AS previo,
        a.actual_n_meses AS actual_n_meses,
        a.previo_n_meses AS previo_n_meses,
        a.tendencia_real AS tendencia_real,
        b.de_tendencia_deseada_cas AS de_tendencia_deseada_cas,
        b.nu_indicador_umbral_inferior AS nu_indicador_umbral_inferior,
        b.nu_indicador_umbral_superior AS nu_indicador_umbral_superior
    FROM vlci2.tendencias_auxiliar_004 a
    LEFT JOIN vlci2.t_f_indicador_umbral_aux_002 b
    ON a.fk_indicador = b.fk_indicador
    ;



    DROP TABLE IF EXISTS vlci2.t_x_tendencias_aux;
    CREATE TABLE vlci2.t_x_tendencias_aux AS 
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        nu_indicador_valor,
        n_meses,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios,
        rn,
        actual,
        previo,
        actual_n_meses,
        previo_n_meses,
        tendencia_real,
        de_tendencia_deseada_cas,
        nu_indicador_umbral_inferior,
        nu_indicador_umbral_superior,
        tendencia_real AS flecha,
        CASE
            WHEN tendencia_real = 'Ascendente' AND de_tendencia_deseada_cas = 'Ascendente' THEN 'Verde'
            WHEN tendencia_real = 'Ascendente' AND de_tendencia_deseada_cas = 'Descendente' THEN 'Rojo'
            WHEN tendencia_real = 'Descendente' AND de_tendencia_deseada_cas = 'Ascendente' THEN 'Rojo'
            WHEN tendencia_real = 'Descendente' AND de_tendencia_deseada_cas = 'Descendente' THEN 'Verde'
            ELSE NULL
        END AS color_flecha,
        CASE WHEN nu_indicador_umbral_inferior IS NULL OR nu_indicador_umbral_superior IS NULL THEN 'SIN COLOR'
            ELSE CASE
                WHEN de_tendencia_deseada_cas = 'Ascendente' THEN CASE
                                                                    WHEN actual < nu_indicador_umbral_inferior THEN CASE
                                                                        WHEN ROUND(CAST(actual AS NUMERIC),2) = ROUND(CAST(previo AS NUMERIC),2) THEN 'AMBAR'
                                                                        ELSE 'ROJO' END
                                                                    ELSE CASE
                                                                        WHEN actual > nu_indicador_umbral_superior THEN 'VERDE'
                                                                        ELSE 'AMBAR' END
                                                                    END
                WHEN de_tendencia_deseada_cas = 'Descendente' THEN CASE
                                                                    WHEN actual < nu_indicador_umbral_inferior THEN 'VERDE'
                                                                    ELSE CASE
                                                                        WHEN actual > nu_indicador_umbral_superior THEN CASE
                                                                            WHEN ROUND(CAST(actual AS NUMERIC),2) = ROUND(CAST(previo AS NUMERIC),2) THEN 'AMBAR'
                                                                            ELSE 'ROJO' END
                                                                        ELSE 'AMBAR' END
                                                                    END
                ELSE 'SIN COLOR'
            END
        END AS estado_actual,
        CASE WHEN nu_indicador_umbral_inferior IS NULL OR nu_indicador_umbral_superior IS NULL THEN 'SIN COLOR'
            ELSE CASE
                WHEN de_tendencia_deseada_cas = 'Ascendente' THEN CASE
                                                                    WHEN previo < nu_indicador_umbral_inferior THEN CASE
                                                                        WHEN ROUND(CAST(actual AS NUMERIC),2) = ROUND(CAST(previo AS NUMERIC),2) THEN 'AMBAR'
                                                                        ELSE 'ROJO' END
                                                                    ELSE CASE
                                                                        WHEN previo > nu_indicador_umbral_superior THEN 'VERDE'
                                                                        ELSE 'AMBAR' END
                                                                    END
                WHEN de_tendencia_deseada_cas = 'Descendente' THEN CASE
                                                                    WHEN previo < nu_indicador_umbral_inferior THEN 'VERDE'
                                                                    ELSE CASE
                                                                        WHEN previo > nu_indicador_umbral_superior THEN CASE
                                                                            WHEN ROUND(CAST(actual AS NUMERIC),2) = ROUND(CAST(previo AS NUMERIC),2) THEN 'AMBAR'
                                                                            ELSE 'ROJO' END
                                                                        ELSE 'AMBAR' END
                                                                    END
                ELSE 'VERDE'
            END
        END AS estado_previo,
        CASE
            WHEN de_tendencia_deseada_cas='Ascendente' AND actual <> previo THEN CASE
                                                                                    WHEN actual < previo THEN CASE
                                                                                        WHEN ROUND(CAST(actual AS NUMERIC),2) = ROUND(CAST(previo AS NUMERIC),2) THEN 'DES_VERDE'
                                                                                        ELSE 'DES_ROJO' END
                                                                                    ELSE 'ASC_VERDE'
                                                                                END
            WHEN de_tendencia_deseada_cas='Descendente' AND actual <> previo THEN CASE
                                                                                    WHEN actual > previo THEN CASE
                                                                                        WHEN ROUND(CAST(actual AS NUMERIC),2) = ROUND(CAST(previo AS NUMERIC),2) THEN 'ASC_VERDE'
                                                                                        ELSE 'ASC_ROJO' END
                                                                                    ELSE 'DES_VERDE'
                                                                                END
            WHEN actual = previo THEN 'MANTIENE_TEND'
            ELSE CASE
                    WHEN actual < previo THEN 'DES_SIN COLOR'
                    ELSE 'ASC_SIN COLOR'
                END
        END AS tendencia,
        CASE
            WHEN de_tendencia_deseada_cas='Ascendente' AND actual <> previo THEN CASE
                                                                                    WHEN actual < previo THEN CASE
                                                                                        WHEN ROUND(CAST(actual AS NUMERIC),2) = ROUND(CAST(previo AS NUMERIC),2) THEN 'DES_VERDE_MG'
                                                                                        ELSE 'DES_ROJO_MG' END
                                                                                    ELSE 'ASC_VERDE_MG'
                                                                                END
            WHEN de_tendencia_deseada_cas='Descendente' AND actual <> previo THEN CASE
                                                                                    WHEN actual > previo THEN CASE
                                                                                        WHEN ROUND(CAST(actual AS NUMERIC),2) = ROUND(CAST(previo AS NUMERIC),2) THEN 'ASC_VERDE_MG'
                                                                                        ELSE 'ASC_ROJO_MG' END
                                                                                    ELSE 'DES_VERDE_MG'
                                                                                END
            WHEN actual = previo THEN 'MANTIENE_CHART'
            ELSE CASE
                    WHEN actual < previo THEN 'DES_SIN COLOR_MG'
                    ELSE 'ASC_SIN COLOR_MG'
                END
        END AS microchart
    FROM vlci2.tendencias_auxiliar_005
    ;


    DROP TABLE IF EXISTS vlci2.tendencias_valor_actual;
    DROP TABLE IF EXISTS vlci2.tendencias_valor_previo;
    DROP TABLE IF EXISTS vlci2.tendencias_auxiliar_001;
    DROP TABLE IF EXISTS vlci2.tendencias_auxiliar_002;
    DROP TABLE IF EXISTS vlci2.tendencias_auxiliar_003;
    DROP TABLE IF EXISTS vlci2.tendencias_auxiliar_004;
    DROP TABLE IF EXISTS vlci2.tendencias_auxiliar_005;
    DROP TABLE IF EXISTS vlci2.t_f_indicador_umbral_aux_001;
    DROP TABLE IF EXISTS vlci2.t_f_indicador_umbral_aux_002;
END
$procedure$
;

COMMIT;
