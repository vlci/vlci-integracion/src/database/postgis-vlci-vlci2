-- Deploy postgis-vlci-vlci2:r_code/procedures/p.p_cubo_idioma_historico to pg

BEGIN;

CREATE OR REPLACE PROCEDURE vlci2.p_cubo_idioma_historico()
 LANGUAGE plpgsql
AS $procedure$
begin
    DROP TABLE IF EXISTS vlci2.t_x_tendencias_aux_001;
    CREATE TABLE vlci2.t_x_tendencias_aux_001 AS
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        nu_indicador_valor,
        n_meses,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios,
        row_number() OVER (PARTITION BY
            fk_indicador,
            fk_barrio,
            dc_criterio_desagregacion1_valor,
            dc_criterio_desagregacion2_valor,
            dc_criterio_desagregacion3_valor,
            dc_criterio_desagregacion4_valor
            ORDER BY dc_periodicidad_muestreo_valor DESC) AS rn,
        actual,
        previo,
        actual_n_meses,
        previo_n_meses,
        tendencia_real,
        de_tendencia_deseada_cas,
        nu_indicador_umbral_inferior,
        nu_indicador_umbral_superior,
        flecha,
        color_flecha,
        estado_actual,
        estado_previo,
        tendencia,
        microchart
    FROM vlci2.t_x_tendencias_aux
    ;

    DROP TABLE IF EXISTS vlci2.t_x_tendencias_aux_002;
    CREATE TABLE vlci2.t_x_tendencias_aux_002 AS
    SELECT
        fk_indicador,
        fk_periodicidad_muestreo,
        dc_periodicidad_muestreo_valor,
        fk_barrio,
        dc_criterio_desagregacion1_valor,
        dc_criterio_desagregacion2_valor,
        dc_criterio_desagregacion3_valor,
        dc_criterio_desagregacion4_valor,
        nu_indicador_valor,
        n_meses,
        es_criterio_global,
        agregue_criterios,
        es_barrio_global,
        agregue_barrios,
        rn,
        actual,
        previo,
        actual_n_meses,
        previo_n_meses,
        tendencia_real,
        de_tendencia_deseada_cas,
        nu_indicador_umbral_inferior,
        nu_indicador_umbral_superior,
        flecha,
        color_flecha,
        estado_actual,
        estado_previo,
        tendencia,
        microchart
    FROM vlci2.t_x_tendencias_aux_001
    WHERE rn = 1
    ;


    DROP TABLE IF EXISTS vlci2.t_a_indicador_cubo_idioma_hist_001;
    CREATE TABLE vlci2.t_a_indicador_cubo_idioma_hist_001 AS
    SELECT
        agg.fk_indicador AS fk_indicador,
        agg.fk_periodicidad_muestreo AS fk_periodicidad_muestreo,
        agg.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
        agg.fk_barrio AS fk_barrio,
        agg.dc_criterio_desagregacion1_valor AS dc_criterio_desagregacion1_valor,
        agg.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor,
        agg.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor,
        agg.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor,
        agg.nu_indicador_valor AS nu_indicador_valor,
        agg.etl_nombre AS etl_nombre,
        agg.fe_fecha_carga AS fe_fecha_carga,
        agg.n_meses AS n_meses,
        agg.agregue_a_anyo AS agregue_a_anyo,
        agg.es_criterio_global AS es_criterio_global,
        agg.agregue_criterios AS agregue_criterios,
        agg.es_barrio_global AS es_barrio_global,
        agg.agregue_barrios AS agregue_barrios,
        tendencias.actual AS actual,
        tendencias.previo AS previo,
        tendencias.actual_n_meses AS actual_n_meses,
        tendencias.previo_n_meses AS previo_n_meses,
        tendencias.tendencia_real AS tendencia_real,
        tendencias.de_tendencia_deseada_cas AS de_tendencia_deseada_cas,
        tendencias.nu_indicador_umbral_inferior AS nu_indicador_umbral_inferior,
        tendencias.nu_indicador_umbral_superior AS nu_indicador_umbral_superior,
        tendencias.flecha AS flecha,
        tendencias.color_flecha AS color_flecha,
        tendencias.estado_actual AS estado_actual,
        tendencias.estado_previo AS estado_previo,
        tendencias.tendencia AS tendencia,
        tendencias.microchart AS microchart
    FROM vlci2.t_x_aggregados_002 agg
    LEFT JOIN vlci2.t_x_tendencias_aux_002 tendencias
    ON (agg.fk_indicador = tendencias.fk_indicador
        AND agg.fk_barrio = tendencias.fk_barrio
        AND agg.dc_criterio_desagregacion1_valor = tendencias.dc_criterio_desagregacion1_valor
        AND agg.dc_criterio_desagregacion2_valor = tendencias.dc_criterio_desagregacion2_valor
        AND agg.dc_criterio_desagregacion3_valor = tendencias.dc_criterio_desagregacion3_valor
        AND agg.dc_criterio_desagregacion4_valor = tendencias.dc_criterio_desagregacion4_valor)
    ;


    DROP TABLE IF EXISTS vlci2.t_a_indicador_cubo_idioma_hist_002;
    CREATE TABLE vlci2.t_a_indicador_cubo_idioma_hist_002 AS
        SELECT
            fk_indicador,
            fk_periodicidad_muestreo,
            dc_periodicidad_muestreo_valor,
            fk_barrio,
            dc_criterio_desagregacion1_valor,
            dc_criterio_desagregacion2_valor,
            dc_criterio_desagregacion3_valor,
            dc_criterio_desagregacion4_valor,
            nu_indicador_valor,
            etl_nombre,
            fe_fecha_carga,
            n_meses,
            agregue_a_anyo,
            es_criterio_global,
            agregue_criterios,
            es_barrio_global,
            agregue_barrios,
            actual,
            previo,
            actual_n_meses,
            previo_n_meses,
            tendencia_real,
            de_tendencia_deseada_cas,
            nu_indicador_umbral_inferior,
            nu_indicador_umbral_superior,
            flecha,
            color_flecha,
            estado_actual,
            estado_previo,
            tendencia,
            microchart,
            0 AS in_tiempo
        FROM vlci2.t_a_indicador_cubo_idioma_hist_001
        WHERE NOT (NOT agregue_a_anyo AND agregue_criterios AND NOT agregue_barrios AND fk_periodicidad_muestreo < 6 AND es_criterio_global AND es_barrio_global)
            AND NOT (NOT agregue_a_anyo AND NOT agregue_criterios AND NOT agregue_barrios AND fk_periodicidad_muestreo < 6 AND es_criterio_global AND es_barrio_global)
            AND NOT (NOT agregue_a_anyo AND agregue_criterios AND agregue_barrios AND fk_periodicidad_muestreo < 6 AND es_criterio_global AND es_barrio_global)
            AND NOT (agregue_a_anyo AND agregue_criterios AND NOT agregue_barrios AND fk_periodicidad_muestreo < 6 AND es_criterio_global AND es_barrio_global)
            AND NOT (agregue_criterios AND NOT agregue_barrios AND fk_periodicidad_muestreo >= 6 AND es_criterio_global AND es_barrio_global)
    UNION ALL
        SELECT
            fk_indicador,
            fk_periodicidad_muestreo,
            dc_periodicidad_muestreo_valor,
            fk_barrio,
            dc_criterio_desagregacion1_valor,
            dc_criterio_desagregacion2_valor,
            dc_criterio_desagregacion3_valor,
            dc_criterio_desagregacion4_valor,
            nu_indicador_valor,
            etl_nombre,
            fe_fecha_carga,
            n_meses,
            agregue_a_anyo,
            es_criterio_global,
            agregue_criterios,
            es_barrio_global,
            agregue_barrios,
            actual,
            previo,
            actual_n_meses,
            previo_n_meses,
            tendencia_real,
            de_tendencia_deseada_cas,
            nu_indicador_umbral_inferior,
            nu_indicador_umbral_superior,
            flecha,
            color_flecha,
            estado_actual,
            estado_previo,
            tendencia,
            microchart,
            1 AS in_tiempo
        FROM vlci2.t_a_indicador_cubo_idioma_hist_001
        WHERE (es_barrio_global AND es_criterio_global AND fk_periodicidad_muestreo < 6)
            OR (NOT agregue_barrios AND fk_periodicidad_muestreo >= 6 AND es_criterio_global AND es_barrio_global)
            OR (agregue_a_anyo AND NOT agregue_criterios AND fk_periodicidad_muestreo < 6 AND es_criterio_global AND NOT es_barrio_global)
    ;


    DROP TABLE IF EXISTS vlci2.t_x_agregados_periodicidades_aux;
    CREATE TABLE vlci2.t_x_agregados_periodicidades_aux AS
    SELECT DISTINCT 
        fk_indicador
    FROM vlci2.t_x_agregados_periodicidades
    ;


    DROP TABLE IF EXISTS vlci2.t_x_agregados_por_combinados_aux;
    CREATE TABLE vlci2.t_x_agregados_por_combinados_aux AS
        SELECT
            DISTINCT dc_identificador_indicador_grueso AS dc_identificador_indicador_grueso
        FROM vlci2.t_m_combinados_criterios 
    UNION DISTINCT
        SELECT
            DISTINCT dc_identificador_indicador_grueso AS dc_identificador_indicador_grueso
        FROM vlci2.t_m_combinados_temporales;


    DROP TABLE IF EXISTS vlci2.t_a_indicador_cubo_idioma_hist_003;
    CREATE TABLE vlci2.t_a_indicador_cubo_idioma_hist_003 AS
    SELECT
        indicador.dc_identificador_indicador AS dc_identificador_indicador,
        hist.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
        CASE WHEN hist.fk_periodicidad_muestreo = 6 THEN to_char(cast(hist.dc_periodicidad_muestreo_valor as date), 'yyyy-MM-dd')
            ELSE NULL
            END AS dc_periodicidad_muestreo_date,
        CASE WHEN hist.es_barrio_global THEN CASE WHEN hist.es_criterio_global AND (hist.agregue_barrios OR hist.agregue_criterios) THEN '000' ELSE '999' END ELSE hist.fk_barrio END AS fk_barrio,
        CASE WHEN NOT hist.agregue_criterios AND hist.es_criterio_global THEN 'GENERICO' ELSE hist.dc_criterio_desagregacion1_valor END AS dc_criterio_desagregacion1_valor_cas,
        CASE WHEN NOT hist.agregue_criterios AND hist.es_criterio_global THEN 'GENERIC' ELSE hist.dc_criterio_desagregacion1_valor END AS dc_criterio_desagregacion1_valor_val,
        hist.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor_cas,
        hist.dc_criterio_desagregacion2_valor AS dc_criterio_desagregacion2_valor_val,
        hist.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor_cas,
        hist.dc_criterio_desagregacion3_valor AS dc_criterio_desagregacion3_valor_val,
        hist.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor_cas,
        hist.dc_criterio_desagregacion4_valor AS dc_criterio_desagregacion4_valor_val,
        indicador.dc_criterio_desagregacion1_cas AS dc_criterio_desagregacion1_cas,
        indicador.dc_criterio_desagregacion1_val AS dc_criterio_desagregacion1_val,
        indicador.dc_criterio_desagregacion2_cas AS dc_criterio_desagregacion2_cas,
        indicador.dc_criterio_desagregacion2_val AS dc_criterio_desagregacion2_val,
        indicador.dc_criterio_desagregacion3_cas AS dc_criterio_desagregacion3_cas,
        indicador.dc_criterio_desagregacion3_val AS dc_criterio_desagregacion3_val,
        indicador.dc_criterio_desagregacion4_cas AS dc_criterio_desagregacion4_cas,
        indicador.dc_criterio_desagregacion4_val AS dc_criterio_desagregacion4_val,
        indicador.de_nombre_indicador_cas AS de_nombre_indicador_cas,
        indicador.de_nombre_indicador_val AS de_nombre_indicador_val,
        indicador.de_descripcion_indicador_cas AS de_descripcion_indicador_cas,
        indicador.de_descripcion_indicador_val AS de_descripcion_indicador_val,
        color_actual.de_color_indicador_url AS de_color_indicador_act_url,
        color_previo.de_color_indicador_url AS de_color_indicador_pre_url,
        periodicidad.dc_periodicidad_muestreo_cas AS dc_periodicidad_muestreo_cas,
        periodicidad.dc_periodicidad_muestreo_val AS dc_periodicidad_muestreo_val,
        indicador.fk_periodicidad_muestreo AS periodicidad_muestreo_origen,
        week_day_names.cas AS dc_dia_semana_cas,
        week_day_names.val AS dc_dia_semana_val,
        --CASE WHEN texto_actual.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_cas IS NULL THEN translate(translate(translate(format_number(hist.actual,2),',','dummy_string'),'.',','),'dummy_string','.') ELSE concat(translate(translate(translate(format_number(hist.actual,2),',','dummy_string'),'.',','),'dummy_string','.'),' ',medida.dc_unidad_medida_abr_cas) END ELSE texto_actual.dc_indicador_valor_cas END AS dc_indicador_act_cas,
        --CASE WHEN texto_actual.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_val IS NULL THEN translate(translate(translate(format_number(hist.actual,2),',','dummy_string'),'.',','),'dummy_string','.') ELSE concat(translate(translate(translate(format_number(hist.actual,2),',','dummy_string'),'.',','),'dummy_string','.'),' ',medida.dc_unidad_medida_abr_val) END ELSE texto_actual.dc_indicador_valor_val END AS dc_indicador_act_val,
        --CASE WHEN texto_previo.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_cas IS NULL THEN translate(translate(translate(format_number(hist.previo,2),',','dummy_string'),'.',','),'dummy_string','.') ELSE concat(translate(translate(translate(format_number(hist.previo,2),',','dummy_string'),'.',','),'dummy_string','.'),' ',medida.dc_unidad_medida_abr_cas) END ELSE texto_previo.dc_indicador_valor_cas END AS dc_indicador_pre_cas,
        --CASE WHEN texto_previo.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_val IS NULL THEN translate(translate(translate(format_number(hist.previo,2),',','dummy_string'),'.',','),'dummy_string','.') ELSE concat(translate(translate(translate(format_number(hist.previo,2),',','dummy_string'),'.',','),'dummy_string','.'),' ',medida.dc_unidad_medida_abr_val) END ELSE texto_previo.dc_indicador_valor_val END AS dc_indicador_pre_val,
        CASE WHEN texto_actual.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_cas IS NULL THEN to_char(hist.actual, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99') ELSE concat(to_char(hist.actual, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99'),' ',medida.dc_unidad_medida_abr_cas) END ELSE texto_actual.dc_indicador_valor_cas END AS dc_indicador_act_cas,
        CASE WHEN texto_actual.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_val IS NULL THEN to_char(hist.actual, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99') ELSE concat(to_char(hist.actual, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99'),' ',medida.dc_unidad_medida_abr_val) END ELSE texto_actual.dc_indicador_valor_val END AS dc_indicador_act_val,
        CASE WHEN texto_previo.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_cas IS NULL THEN to_char(hist.previo, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99') ELSE concat(to_char(hist.previo, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99'),' ',medida.dc_unidad_medida_abr_cas) END ELSE texto_previo.dc_indicador_valor_cas END AS dc_indicador_pre_cas,
        CASE WHEN texto_previo.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_val IS NULL THEN to_char(hist.previo, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99') ELSE concat(to_char(hist.previo, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99'),' ',medida.dc_unidad_medida_abr_val) END ELSE texto_previo.dc_indicador_valor_val END AS dc_indicador_pre_val,
        medida.dc_unidad_medida_cas AS dc_unidad_medida_cas,
        medida.dc_unidad_medida_val AS dc_unidad_medida_val,
        medida.dc_unidad_medida_abr_cas AS dc_unidad_medida_abr_cas,
        medida.dc_unidad_medida_abr_val AS dc_unidad_medida_abr_val,
        hist.nu_indicador_valor AS nu_indicador_valor,
        --CASE WHEN texto.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_cas IS NULL THEN translate(translate(translate(format_number(hist.nu_indicador_valor,2),',','dummy_string'),'.',','),'dummy_string','.') ELSE concat(translate(translate(translate(format_number(hist.nu_indicador_valor,2),',','dummy_string'),'.',','),'dummy_string','.'),' ',medida.dc_unidad_medida_abr_cas) END ELSE texto.dc_indicador_valor_cas END AS dc_indicador_valor_cas,
        --CASE WHEN texto.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_val IS NULL THEN translate(translate(translate(format_number(hist.nu_indicador_valor,2),',','dummy_string'),'.',','),'dummy_string','.') ELSE concat(translate(translate(translate(format_number(hist.nu_indicador_valor,2),',','dummy_string'),'.',','),'dummy_string','.'),' ',medida.dc_unidad_medida_abr_val) END ELSE texto.dc_indicador_valor_val END AS dc_indicador_valor_val,    color_tendendcia.de_color_indicador_url AS de_color_indicador_tendencia_url,
        CASE WHEN texto.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_cas IS NULL THEN to_char(hist.nu_indicador_valor, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99') ELSE concat(to_char(hist.nu_indicador_valor, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99'),' ',medida.dc_unidad_medida_abr_cas) END ELSE texto.dc_indicador_valor_cas END AS dc_indicador_valor_cas,
        CASE WHEN texto.dc_indicador_valor_cas IS NULL THEN CASE WHEN medida.dc_unidad_medida_abr_val IS NULL THEN to_char(hist.nu_indicador_valor, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99') ELSE concat(to_char(hist.nu_indicador_valor, '999G999G999G999G999G999G999G999G999G999G999G999G999G990D99'),' ',medida.dc_unidad_medida_abr_val) END ELSE texto.dc_indicador_valor_val END AS dc_indicador_valor_val,    color_tendendcia.de_color_indicador_url AS de_color_indicador_tendencia_url,
        color_microchart.de_color_indicador_url AS de_icono_microchart_url,
        CASE WHEN indicador.fk_periodicidad_muestreo >=6 THEN 'DESAGREGACION'
            WHEN NOT agregados_por_combinados.dc_identificador_indicador_grueso IS NULL THEN 'DESAGREGACION'
            WHEN hist.agregue_criterios THEN 'DESAGREGACION'
            ELSE NULL
            END AS de_icono_desagregacion_url,
        CASE WHEN (indicador.dc_criterio_mapa IS NULL
                OR indicador.dc_criterio_mapa = ''
                OR indicador.dc_criterio_mapa = 'N/A') THEN NULL
            ELSE 'MAPA'
            END AS de_icono_mapa_url,
        CASE WHEN indicador.dc_tipo_indicador='IC' AND indicador.dc_codigo_iso IS NULL THEN 'CIUDAD_NO_ISO'
            WHEN indicador.dc_tipo_indicador='IC' AND indicador.dc_codigo_iso IS NOT NULL THEN 'CIUDAD_ISO'
            WHEN indicador.dc_tipo_indicador='IS' AND indicador.dc_codigo_iso IS NULL THEN 'SERVICIO_NO_ISO'
            WHEN indicador.dc_tipo_indicador='IS' AND indicador.dc_codigo_iso IS NOT NULL THEN 'SERVICIO_ISO'
            ELSE NULL
            END AS de_icono_tipo_url,
        tipo_indicador.cas AS de_tipo_indicador_cas,
        tipo_indicador.val AS de_tipo_indicador_val,
        CASE WHEN indicador.dc_codigo_iso IS NOT NULL THEN 'ISO' ELSE NULL END AS de_flag_iso,
        barrio.de_nombre_barrio AS de_nombre_barrio,
        barrio.de_nombre_distrito AS de_nombre_distrito,
        indicador.dc_link_geoportal_cas AS dc_link_geoportal_cas,
        indicador.dc_link_geoportal_val AS dc_link_geoportal_val,
        indicador.dc_link_pdatos_abiertos_cas AS dc_link_pdatos_abiertos_cas,
        indicador.dc_link_pdatos_abiertos_val AS dc_link_pdatos_abiertos_val,
        indicador.dc_criterio_desagregacion_agg AS dc_criterio_desagregacion_agg,
        hist.in_tiempo AS in_tiempo,
        hist.fe_fecha_carga AS fe_fecha_carga,
        indicador.fk_servicio_id AS fk_servicio_id,
        CASE WHEN hist.fk_periodicidad_muestreo >= 6 THEN NULL ELSE CASE WHEN indicador.fk_periodicidad_muestreo >=6 THEN hist.n_meses ELSE 12.0 END END AS de_numero_meses,
        CASE WHEN hist.fk_periodicidad_muestreo >= 6 THEN NULL ELSE CASE WHEN indicador.fk_periodicidad_muestreo >=6 THEN hist.previo_n_meses ELSE 12.0 END END AS de_numero_meses_prev,
        indicador.nota_cas AS nota_cas,
        indicador.nota_val AS nota_val
    FROM vlci2.t_a_indicador_cubo_idioma_hist_002 hist
    LEFT JOIN vlci2.t_d_indicador indicador
    ON hist.fk_indicador = indicador.pk_indicador
    LEFT JOIN vlci2.t_m_periodicidad_muestreo periodicidad
    ON hist.fk_periodicidad_muestreo = periodicidad.pk_periodicidad_muestreo
    LEFT JOIN vlci2.t_m_unidad_medida medida
    ON indicador.fk_unidad_medida = medida.pk_unidad_medida
    LEFT JOIN vlci2.t_d_barrio barrio
    ON hist.fk_barrio = barrio.pk_barrio
    LEFT JOIN vlci2.t_x_indicador_color color_actual
    ON hist.estado_actual = color_actual.de_color_indicador
    LEFT JOIN vlci2.t_x_indicador_color color_previo
    ON hist.estado_previo = color_previo.de_color_indicador
    LEFT JOIN vlci2.t_x_week_day week_day_names
    ON week_day_names.day = CASE WHEN hist.fk_periodicidad_muestreo = 6 THEN extract(isodow from date (hist.dc_periodicidad_muestreo_valor)) ELSE 0 END
    LEFT JOIN vlci2.t_x_indicador_texto texto
    ON indicador.dc_identificador_indicador = texto.dc_identificador_indicador AND texto.nu_indicador_valor = hist.nu_indicador_valor
    LEFT JOIN vlci2.t_x_indicador_color color_tendendcia
    ON hist.tendencia = color_tendendcia.de_color_indicador
    LEFT JOIN vlci2.t_x_indicador_color color_microchart
    ON hist.microchart = color_microchart.de_color_indicador
    LEFT JOIN vlci2.t_x_tipo_indicador tipo_indicador
    ON indicador.dc_tipo_indicador = tipo_indicador.tipo
    LEFT JOIN vlci2.t_x_indicador_texto texto_actual
    ON indicador.dc_identificador_indicador = texto_actual.dc_identificador_indicador AND texto_actual.nu_indicador_valor = hist.actual
    LEFT JOIN vlci2.t_x_indicador_texto texto_previo
    ON indicador.dc_identificador_indicador = texto_previo.dc_identificador_indicador AND texto_previo.nu_indicador_valor = hist.previo
    LEFT JOIN vlci2.t_x_agregados_periodicidades_aux agregados_periodicidades
    ON hist.fk_indicador = agregados_periodicidades.fk_indicador
    LEFT JOIN vlci2.t_x_agregados_por_combinados_aux agregados_por_combinados
    ON indicador.dc_identificador_indicador = agregados_por_combinados.dc_identificador_indicador_grueso
    ;


    DROP TABLE IF EXISTS vlci2.t_a_indicador_cubo_idioma_hist;
    CREATE TABLE vlci2.t_a_indicador_cubo_idioma_hist AS
    SELECT
        hist.dc_identificador_indicador AS dc_identificador_indicador,
        hist.dc_periodicidad_muestreo_valor AS dc_periodicidad_muestreo_valor,
        hist.dc_periodicidad_muestreo_date AS dc_periodicidad_muestreo_date,
        hist.fk_barrio AS fk_barrio,
        cast(0 AS double precision) AS nu_latitud,
        cast(0 AS double precision) AS nu_longitud,
        hist.dc_criterio_desagregacion1_valor_cas AS dc_criterio_desagregacion1_valor_cas,
        hist.dc_criterio_desagregacion1_valor_val AS dc_criterio_desagregacion1_valor_val,
        hist.dc_criterio_desagregacion2_valor_cas AS dc_criterio_desagregacion2_valor_cas,
        hist.dc_criterio_desagregacion2_valor_val AS dc_criterio_desagregacion2_valor_val,
        hist.dc_criterio_desagregacion3_valor_cas AS dc_criterio_desagregacion3_valor_cas,
        hist.dc_criterio_desagregacion3_valor_val AS dc_criterio_desagregacion3_valor_val,
        hist.dc_criterio_desagregacion4_valor_cas AS dc_criterio_desagregacion4_valor_cas,
        hist.dc_criterio_desagregacion4_valor_val AS dc_criterio_desagregacion4_valor_val,
        CASE WHEN hist.dc_criterio_desagregacion1_cas = 'N/A' THEN NULL ELSE hist.dc_criterio_desagregacion1_cas END AS dc_criterio_desagregacion1_cas,
        CASE WHEN hist.dc_criterio_desagregacion1_val = 'N/A' THEN NULL ELSE hist.dc_criterio_desagregacion1_val END AS dc_criterio_desagregacion1_val,
        CASE WHEN hist.dc_criterio_desagregacion2_cas = 'N/A' THEN NULL ELSE hist.dc_criterio_desagregacion2_cas END AS dc_criterio_desagregacion2_cas,
        CASE WHEN hist.dc_criterio_desagregacion2_val = 'N/A' THEN NULL ELSE hist.dc_criterio_desagregacion2_val END AS dc_criterio_desagregacion2_val,
        CASE WHEN hist.dc_criterio_desagregacion3_cas = 'N/A' THEN NULL ELSE hist.dc_criterio_desagregacion3_cas END AS dc_criterio_desagregacion3_cas,
        CASE WHEN hist.dc_criterio_desagregacion3_val = 'N/A' THEN NULL ELSE hist.dc_criterio_desagregacion3_val END AS dc_criterio_desagregacion3_val,
        CASE WHEN hist.dc_criterio_desagregacion4_cas = 'N/A' THEN NULL ELSE hist.dc_criterio_desagregacion4_cas END AS dc_criterio_desagregacion4_cas,
        CASE WHEN hist.dc_criterio_desagregacion4_val = 'N/A' THEN NULL ELSE hist.dc_criterio_desagregacion4_val END AS dc_criterio_desagregacion4_val,
        NULL AS de_bloque_cas,
        NULL AS de_bloque_val,
        NULL AS de_area_municipal_cas,
        NULL AS de_area_municipal_val,
        NULL AS de_delegacion_municipal_cas,
        NULL AS de_delegacion_municipal_val,
        NULL AS de_servicio_municipal_cas,
        NULL AS de_servicio_municipal_val,
        hist.de_nombre_indicador_cas AS de_nombre_indicador_cas,
        hist.de_nombre_indicador_val AS de_nombre_indicador_val,
        hist.de_descripcion_indicador_cas AS de_descripcion_indicador_cas,
        hist.de_descripcion_indicador_val AS de_descripcion_indicador_val,
        hist.de_color_indicador_act_url AS de_color_indicador_act_url,
        hist.de_color_indicador_pre_url AS de_color_indicador_pre_url,
        NULL AS de_leyenda_verde_act_cas,
        NULL AS de_leyenda_verde_act_val,
        NULL AS de_leyenda_ambar_act_cas,
        NULL AS de_leyenda_ambar_act_val,
        NULL AS de_leyenda_rojo_act_cas,
        NULL AS de_leyenda_rojo_act_val,
        hist.dc_periodicidad_muestreo_cas AS dc_periodicidad_muestreo_cas,
        hist.dc_periodicidad_muestreo_val AS dc_periodicidad_muestreo_val,
        hist.periodicidad_muestreo_origen AS periodicidad_muestreo_origen,
        hist.dc_dia_semana_cas AS dc_dia_semana_cas,
        hist.dc_dia_semana_val AS dc_dia_semana_val,
        hist.dc_indicador_act_cas AS dc_indicador_act_cas,
        hist.dc_indicador_act_val AS dc_indicador_act_val,
        hist.dc_indicador_pre_cas AS dc_indicador_pre_cas,
        hist.dc_indicador_pre_val AS dc_indicador_pre_val,
        hist.dc_unidad_medida_cas AS dc_unidad_medida_cas,
        hist.dc_unidad_medida_val AS dc_unidad_medida_val,
        hist.dc_unidad_medida_abr_cas AS dc_unidad_medida_abr_cas,
        hist.dc_unidad_medida_abr_val AS dc_unidad_medida_abr_val,
        hist.nu_indicador_valor AS nu_indicador_valor,
        hist.dc_indicador_valor_cas AS dc_indicador_valor_cas,
        hist.dc_indicador_valor_val AS dc_indicador_valor_val,
        hist.de_color_indicador_tendencia_url AS de_color_indicador_tendencia_url,
        hist.de_icono_microchart_url AS de_icono_microchart_url,
        NULL AS de_color_area_url,
        NULL AS de_color_subarea_url,
        color_desagregacion.de_color_indicador_url AS de_icono_desagregacion_url,
        color_mapa.de_color_indicador_url AS de_icono_mapa_url,
        color_tipo_iso.de_color_indicador_url AS de_icono_tipo_url,
        hist.de_tipo_indicador_cas AS de_tipo_indicador_cas,
        hist.de_tipo_indicador_val AS de_tipo_indicador_val,
        hist.de_flag_iso AS de_flag_iso,
        NULL AS de_denominacion_barrio,
        hist.de_nombre_barrio AS de_nombre_barrio,
        hist.de_nombre_distrito AS de_nombre_distrito,
        hist.dc_link_geoportal_cas AS dc_link_geoportal_cas,
        hist.dc_link_geoportal_val AS dc_link_geoportal_val,
        hist.dc_link_pdatos_abiertos_cas AS dc_link_pdatos_abiertos_cas,
        hist.dc_link_pdatos_abiertos_val AS dc_link_pdatos_abiertos_val,
        hist.dc_criterio_desagregacion_agg AS dc_criterio_desagregacion_agg,
        hist.in_tiempo AS in_tiempo,
        hist.fe_fecha_carga AS fe_fecha_carga,
        hist.fk_servicio_id AS fk_servicio_id,
        NULL AS flag_carga_micro,
        hist.de_numero_meses AS de_numero_meses,
        hist.de_numero_meses_prev AS de_numero_meses_prev,
        hist.nota_cas AS nota_cas,
        hist.nota_val AS nota_val
    FROM vlci2.t_a_indicador_cubo_idioma_hist_003 hist
    LEFT JOIN vlci2.t_x_indicador_color color_desagregacion
    ON hist.de_icono_desagregacion_url = color_desagregacion.de_color_indicador
    LEFT JOIN vlci2.t_x_indicador_color color_mapa
    ON hist.de_icono_mapa_url = color_mapa.de_color_indicador
    LEFT JOIN vlci2.t_x_indicador_color color_tipo_iso
    ON hist.de_icono_tipo_url = color_tipo_iso.de_color_indicador
    ;


    DROP TABLE IF EXISTS vlci2.t_a_indicador_cubo_idioma;
    CREATE TABLE vlci2.t_a_indicador_cubo_idioma AS
    SELECT
    *
    FROM vlci2.t_a_indicador_cubo_idioma_hist
    WHERE CASE
            WHEN dc_periodicidad_muestreo_cas = 'Anual' THEN (cast(to_char(current_date,'yyyy') AS INT) - cast(substr(dc_periodicidad_muestreo_valor,1,4) AS INT)) <= 100
            WHEN dc_periodicidad_muestreo_cas = 'Cuatrienal' THEN (cast(to_char(current_date,'yyyy') AS INT) - cast(substr(dc_periodicidad_muestreo_valor,1,4) AS INT)) <= 100
            WHEN dc_periodicidad_muestreo_cas = 'Quinquenal' THEN (cast(to_char(current_date,'yyyy') AS INT) - cast(substr(dc_periodicidad_muestreo_valor,1,4) AS INT)) <= 100
            WHEN dc_periodicidad_muestreo_cas = 'Decenal' THEN (cast(to_char(current_date,'yyyy') AS INT) - cast(substr(dc_periodicidad_muestreo_valor,1,4) AS INT)) <= 100
            WHEN dc_periodicidad_muestreo_cas = 'Diario' THEN (cast(to_char(current_date,'yyyy') AS INT) - cast(substr(dc_periodicidad_muestreo_valor,1,4) AS INT)) <= 1
            WHEN dc_periodicidad_muestreo_cas = 'Mensual' THEN (cast(to_char(current_date,'yyyy') AS INT) - cast(substr(dc_periodicidad_muestreo_valor,1,4) AS INT)) <= 20
            WHEN dc_periodicidad_muestreo_cas = 'Trimestral' THEN (cast(to_char(current_date,'yyyy') AS INT) - cast(substr(dc_periodicidad_muestreo_valor,1,4) AS INT)) <= 50
            WHEN dc_periodicidad_muestreo_cas = 'Semestral' THEN (cast(to_char(current_date,'yyyy') AS INT) - cast(substr(dc_periodicidad_muestreo_valor,1,4) AS INT)) <= 50
            WHEN dc_periodicidad_muestreo_cas = 'Quincenal' THEN (cast(to_char(current_date,'yyyy') AS INT) - cast(substr(dc_periodicidad_muestreo_valor,1,4) AS INT)) <= 10
            WHEN dc_periodicidad_muestreo_cas = 'Semanal' THEN (cast(to_char(current_date,'yyyy') AS INT) - cast(substr(dc_periodicidad_muestreo_valor,1,4) AS INT)) <= 5
        ELSE false END
    ;

    CREATE INDEX idx_in_tiempo ON vlci2.t_a_indicador_cubo_idioma (in_tiempo);
    CREATE INDEX idx_dc_criterio_desagregacion_agg ON vlci2.t_a_indicador_cubo_idioma (dc_criterio_desagregacion_agg);
    CREATE INDEX idx_dc_identificador_indicador ON vlci2.t_a_indicador_cubo_idioma (dc_identificador_indicador);
    CREATE INDEX idx_fk_servicio_id ON vlci2.t_a_indicador_cubo_idioma (fk_servicio_id);
    CREATE INDEX idx_de_nombre_barrio ON vlci2.t_a_indicador_cubo_idioma USING btree (de_nombre_barrio);


    DROP TABLE IF EXISTS vlci2.t_x_tendencias_aux_001;
    DROP TABLE IF EXISTS vlci2.t_x_tendencias_aux_002;
    DROP TABLE IF EXISTS vlci2.t_a_indicador_cubo_idioma_hist_001;
    DROP TABLE IF EXISTS vlci2.t_a_indicador_cubo_idioma_hist_002;
    DROP TABLE IF EXISTS vlci2.t_a_indicador_cubo_idioma_hist_003;
    DROP TABLE IF EXISTS vlci2.t_x_agregados_periodicidades_aux;
END
$procedure$
;

COMMIT;
