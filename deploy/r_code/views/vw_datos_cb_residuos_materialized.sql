-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_residuos_materialized to pg

BEGIN;

drop materialized view if exists vlci2.vw_datos_cb_residuos_materialized;

CREATE MATERIALIZED VIEW vlci2.vw_datos_cb_residuos_materialized AS
select sliceanddicevalue2 as name, 
	id as type,
	kpivalue as value, 
	to_char(to_date(calculationperiod, 'dd-mm-yyyy'), 'YYYY') as anyo,
	to_char(to_date(calculationperiod, 'dd-mm-yyyy'), 'MM') as mes
from vlci2.vw_t_agg_residuos a11 
where id <> 'is-res-003-kgs-residencies' and a11.sliceanddicevalue1 ='Total' and a11.sliceanddicevalue2 not in ('Resid├¿ncies')
union 
SELECT tipo_residuo as name, 
       CASE WHEN tipo_residuo = 'Mobles' 
            THEN 'is-res-002-kgs-mobles' 
            ELSE 'is-res-001-kgs-contenidors' 
       END AS type, 
       kgs_residuos as value,
		to_char(to_date(fecha_dato, 'dd-mm-yyyy'), 'YYYY') as anyo,
		to_char(to_date(fecha_dato, 'dd-mm-yyyy'), 'MM') as mes
FROM t_ref_residuos_datos_mensuales_historicos
where zona_residuo='Total';

COMMIT;
