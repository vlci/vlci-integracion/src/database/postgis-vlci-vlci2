-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_medioambiente_temperatura_media_diaria to pg

BEGIN;

    CREATE OR REPLACE VIEW vlci2.vw_datos_cb_medioambiente_temperatura_media_diaria 
    AS 
    select
        anyo.originalentityid as id,
        anyo.calculationperiod,
        anyo.kpivalue,
        anyo.measureunit as measurelandunit
    FROM
        vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_anyo anyo
    UNION 
    select
        mes.originalentityid as id,
        mes.calculationperiod,
        mes.kpivalue,
        mes.measureunit as measurelandunit
    FROM
        vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes mes
    where
        mes.calculationperiod > (
            SELECT COALESCE(MAX(anyo.calculationperiod), '1900-01-01')
            FROM t_datos_cb_medioambiente_kpi_temperatura_valid_anyo anyo
        )
    UNION
    select
        rt.entityid as id,
        rt.calculationperiod::date,
        ROUND(rt.kpivalue::int4, 0) as kpivalue,
        rt.measurelandunit as measurelandunit
    FROM
        vlci2.t_f_text_kpi_temperatura_media_diaria rt 
    WHERE
        rt.calculationperiod::date > (
            SELECT COALESCE(MAX(mes.calculationperiod::date), '1900-01-01'::date)
            FROM t_datos_cb_medioambiente_kpi_temperatura_valid_mes mes
        );

COMMIT;
