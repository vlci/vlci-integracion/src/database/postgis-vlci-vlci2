-- Deploy postgis-vlci-vlci2:r_code/views/vw_t_agg_residuos to pg

BEGIN;

-- vlci2.vw_t_agg_residuos source
DROP VIEW vlci2.vw_t_agg_residuos;
CREATE OR REPLACE VIEW vlci2.vw_t_agg_residuos
AS SELECT row_number() OVER (ORDER BY t_f_text_cb_kpi_residuos.entityid) AS id_increment,
    t_f_text_cb_kpi_residuos.entityid AS id,
    t_f_text_cb_kpi_residuos.calculationperiod,
        CASE
            WHEN date_part('dow'::text, to_date(t_f_text_cb_kpi_residuos.calculationperiod, 'dd-mm-yyyy'::text)) = 0::double precision THEN 7::double precision
            ELSE date_part('dow'::text, to_date(t_f_text_cb_kpi_residuos.calculationperiod, 'dd-mm-yyyy'::text))
        END AS dayofweek,
    t_f_text_cb_kpi_residuos.kpivalue,
    t_f_text_cb_kpi_residuos.sliceanddicevalue1,
    t_f_text_cb_kpi_residuos.sliceanddicevalue2,
    t_f_text_cb_kpi_residuos.updatedat,
    t_f_text_cb_kpi_residuos.measureunit
   FROM t_f_text_cb_kpi_residuos;

COMMIT;
