-- Deploy postgis-vlci-vlci2:r_code/views/vw_parking_count_category_status to pg

BEGIN;

CREATE OR REPLACE VIEW vlci2.vw_parking_count_category_status
AS SELECT lastdata.category AS categoria,
    lastdata.status AS estado,
    count(lastdata.entityid) AS total
   FROM ( SELECT DISTINCT ON (tdcp.entityid) tdcp.entityid,
            tdcp.recvtime,
            tdcp.category,
            tdcp.status
           FROM vlci2.t_datos_cb_parkingspot_lastdata tdcp
          ORDER BY tdcp.entityid, tdcp.recvtime DESC) lastdata
  GROUP BY lastdata.category, lastdata.status;

COMMIT;
