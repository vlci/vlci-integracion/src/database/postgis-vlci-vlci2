-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_medioambiente_airqualityobserved_lastdata to pg

BEGIN;

CREATE OR REPLACE VIEW vlci2.vw_datos_cb_medioambiente_airqualityobserved_lastdata AS
SELECT entityid, address, dateobserved, project, maintenanceowner, location,
       CASE WHEN dateobserved >= NOW() - INTERVAL '1 hour' THEN 'ok' ELSE 'noData' END AS operationalstatus,
	   CASE WHEN inactive IS NULL THEN 'false' ELSE inactive END AS inactive
FROM vlci2.t_datos_cb_airqualityobserved_lastdata;

COMMIT;
