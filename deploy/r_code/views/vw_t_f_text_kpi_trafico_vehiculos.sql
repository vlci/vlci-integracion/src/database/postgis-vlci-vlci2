-- Deploy postgis-vlci-vlci2:r_code/views/vw_t_f_text_kpi_trafico_vehiculos to pg

BEGIN;

DROP MATERIALIZED VIEW IF EXISTS vlci2.vw_datos_cb_trafico_vehiculos_materialized;
DROP VIEW IF EXISTS vlci2.vw_t_f_text_kpi_trafico_vehiculos;

CREATE OR REPLACE VIEW vlci2.vw_t_f_text_kpi_trafico_vehiculos
AS SELECT anyo.originalentityid AS id,
    anyo.calculationperiod,
    anyo.diasemana,
    round(anyo.kpivalue, 2) AS kpivalue,
    anyo.tramo,
        CASE
            WHEN anyo.name = 'Accésa Arxiduc Carles pel Camí nou de Picanya'::text THEN 'Accés a Arxiduc Carles pel Camí nou de Picanya'::text
            WHEN anyo.name = 'Accés Barcelona  entrada ieixida '::text or anyo.name = 'Accés Barcelona  entrada i eixida ' THEN 'Accés Barcelona [entrada i eixida]'::text
            WHEN anyo.name = 'ProlongacióJoan XXIII'::text THEN 'Prolongació Joan XXIII'::text
            WHEN anyo.name = 'CortsValencianes  Accés per CV-35 '::text or anyo.name = 'Corts Valencianes  Accés per CV-35 ' THEN 'Corts Valencianes [Accés per CV-35]'::text
            WHEN anyo.name = 'Accés per V-31  Pistade Silla '::text or anyo.name = 'Accés per V-31  Pista de Silla ' THEN 'Accés per V-31 [Pista de Silla]'::text
            ELSE anyo.name
        END AS name,
    anyo.measureunitval,
    anyo.measureunitcas
   FROM t_datos_cb_trafico_kpi_validados_anyo anyo
  WHERE anyo.originalentityid = ANY (ARRAY['KpiValid-365-Accesos-Ciudad-Coches'::text, 'KpiValid-365-Accesos-Vias-Coches'::text])
UNION
 SELECT mes.originalentityid AS id,
    mes.calculationperiod,
    mes.diasemana,
    round(mes.kpivalue, 2) AS kpivalue,
    mes.tramo,
        CASE
            WHEN mes.name = 'Accésa Arxiduc Carles pel Camí nou de Picanya'::text THEN 'Accés a Arxiduc Carles pel Camí nou de Picanya'::text
            WHEN mes.name = 'Accés Barcelona  entrada ieixida '::text  or mes.name = 'Accés Barcelona  entrada i eixida '  THEN 'Accés Barcelona [entrada i eixida]'::text
            WHEN mes.name = 'ProlongacióJoan XXIII'::text THEN 'Prolongació Joan XXIII'::text
            WHEN mes.name = 'CortsValencianes  Accés per CV-35 '::text or mes.name = 'Corts Valencianes  Accés per CV-35 ' THEN 'Corts Valencianes [Accés per CV-35]'::text
            WHEN mes.name = 'Accés per V-31  Pistade Silla '::text or mes.name = 'Accés per V-31  Pista de Silla '  THEN 'Accés per V-31 [Pista de Silla]'::text
            ELSE mes.name
        END AS name,
    mes.measureunitval,
    mes.measureunitcas
   FROM t_datos_cb_trafico_kpi_validados_mes mes
  WHERE (mes.originalentityid = ANY (ARRAY['KpiValid-30-Accesos-Ciudad-Coches'::text, 'KpiValid-30-Accesos-Vias-Coches'::text])) AND mes.calculationperiod > (( SELECT max(anyo.calculationperiod) AS max
           FROM t_datos_cb_trafico_kpi_validados_anyo anyo))
UNION
 SELECT rt.entityid AS id,
    rt.calculationperiod,
    rt.diasemana,
    round(rt.kpivalue::numeric, 2) AS kpivalue,
    rt.tramo,
    rt.name,
    rt.measureunitval,
    rt.measureunitcas
   FROM t_f_text_kpi_trafico_y_bicis rt
  WHERE (rt.entityid::text = ANY (ARRAY['Kpi-Accesos-Ciudad-Coches'::character varying::text, 'Kpi-Accesos-Vias-Coches'::character varying::text])) AND rt.calculationperiod > (( SELECT max(mes.calculationperiod) AS max
           FROM t_datos_cb_trafico_kpi_validados_mes mes));

CREATE MATERIALIZED VIEW vlci2.vw_datos_cb_trafico_vehiculos_materialized
AS SELECT a11.id,
	a11.name,
    date_part('month'::text, to_date(a11.calculationperiod, 'YYYY-MM-DD'::text)) AS mes,
    date_part('year'::text, to_date(a11.calculationperiod, 'YYYY-MM-DD'::text)) AS anyo,
    sum(a11.kpivalue) AS value
   FROM vlci2.vw_t_f_text_kpi_trafico_vehiculos a11
  GROUP BY mes, anyo, name, id
  ORDER BY anyo, mes;

REFRESH MATERIALIZED VIEW vw_datos_cb_trafico_vehiculos_materialized;

COMMIT;
