-- Deploy postgis-vlci-vlci2:r_code/views/vw_t_f_text_kpi_trafico_bicis to pg

BEGIN;

DROP MATERIALIZED VIEW IF EXISTS vlci2.vw_datos_cb_kpi_trafico_bicis_materialized; 
DROP VIEW vlci2.vw_t_f_text_kpi_trafico_bicis;

CREATE OR REPLACE VIEW vlci2.vw_t_f_text_kpi_trafico_bicis
AS SELECT 
anyo.originalentityid AS id,
    anyo.calculationperiod,
    anyo.diasemana,
    round(anyo.kpivalue::numeric, 2) AS kpivalue,
    anyo.tramo,
    anyo.name,
    anyo.measureunitcas, 
    anyo.measureunitval
FROM vlci2.t_datos_cb_trafico_kpi_validados_anyo anyo
where
anyo.originalentityid in ('KpiValid-365-Trafico-Bicicletas-Accesos-Anillo','KpiValid-365-Trafico-Bicicletas-Anillo')
union
SELECT 
mes.originalentityid AS id,
    mes.calculationperiod,
    mes.diasemana,
    round(mes.kpivalue::numeric, 2) AS kpivalue,
    mes.tramo,
    mes.name,
    mes.measureunitcas, 
    mes.measureunitval
FROM vlci2.t_datos_cb_trafico_kpi_validados_mes mes
where
mes.originalentityid in ('KpiValid-30-Trafico-Bicicletas-Accesos-Anillo','KpiValid-30-Trafico-Bicicletas-Anillo')
and
mes.calculationperiod > (select max(anyo.calculationperiod) from vlci2.t_datos_cb_trafico_kpi_validados_anyo anyo)
union
SELECT 
rt.entityid AS id,
    rt.calculationperiod,
    rt.diasemana,
    round(rt.kpivalue::numeric, 2) AS kpivalue,
    rt.tramo,
    rt.name,
    rt.measureunitcas, 
    rt.measureunitval
FROM t_f_text_kpi_trafico_y_bicis rt
where
rt.entityid in ('Kpi-Trafico-Bicicletas-Accesos-Anillo','Kpi-Trafico-Bicicletas-Anillo')
and
rt.calculationperiod > (select max(mes.calculationperiod) from vlci2.t_datos_cb_trafico_kpi_validados_mes mes);
 
CREATE MATERIALIZED VIEW vlci2.vw_datos_cb_kpi_trafico_bicis_materialized
AS SELECT a11.id,
	a11.name,
    date_part('month'::text, to_date(a11.calculationperiod, 'YYYY-MM-DD'::text)) AS mes,
    date_part('year'::text, to_date(a11.calculationperiod, 'YYYY-MM-DD'::text)) AS anyo,
    sum(a11.kpivalue) AS value
   FROM vlci2.vw_t_f_text_kpi_trafico_bicis a11
  GROUP BY mes, anyo, name, id
  ORDER BY anyo, mes;
 
REFRESH MATERIALIZED VIEW vw_datos_cb_kpi_trafico_bicis_materialized;
 

COMMIT;
