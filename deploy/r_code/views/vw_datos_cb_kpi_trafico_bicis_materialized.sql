-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_kpi_trafico_bicis_materialized to pg

BEGIN;

DROP MATERIALIZED VIEW IF EXISTS vlci2.vw_t_f_text_kpi_trafico_bicis_materialized;

CREATE MATERIALIZED VIEW vlci2.vw_datos_cb_kpi_trafico_bicis_materialized
AS SELECT a11.id,
	a11.name,
    date_part('month'::text, to_date(a11.calculationperiod, 'YYYY-MM-DD'::text)) AS mes,
    date_part('year'::text, to_date(a11.calculationperiod, 'YYYY-MM-DD'::text)) AS anyo,
    sum(a11.kpivalue) AS value
   FROM vlci2.vw_t_f_text_kpi_trafico_bicis a11
  where a11.id = 'Kpi-Trafico-Bicicletas-Anillo' or a11.id = 'Kpi-Trafico-Bicicletas-Accesos-Anillo'
  GROUP BY mes, anyo, name, id
  ORDER BY anyo, mes;

COMMIT;
