-- Deploy postgis-vlci-vlci2:r_code/views/vw_t_f_text_kpi_trafico_vehiculos to pg

BEGIN;

-- vlci2.vw_t_f_text_kpi_trafico_vehiculos source
DROP VIEW vlci2.vw_t_f_text_kpi_trafico_vehiculos;
CREATE OR REPLACE VIEW vlci2.vw_t_f_text_kpi_trafico_vehiculos
AS SELECT t_f_text_kpi_trafico_y_bicis.entityid AS id,
    t_f_text_kpi_trafico_y_bicis.calculationperiod,
    t_f_text_kpi_trafico_y_bicis.diasemana,
    round(t_f_text_kpi_trafico_y_bicis.kpivalue::numeric, 2) AS kpivalue,
    t_f_text_kpi_trafico_y_bicis.tramo,
    t_f_text_kpi_trafico_y_bicis.name,
    t_f_text_kpi_trafico_y_bicis.measureunitval as measureunit
   FROM t_f_text_kpi_trafico_y_bicis
  WHERE t_f_text_kpi_trafico_y_bicis.entityid::text = ANY (ARRAY['Kpi-Accesos-Ciudad-Coches'::character varying, 'Kpi-Accesos-Vias-Coches'::character varying]::text[]);

COMMIT;
