-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_agua_total_materialized to pg

BEGIN;

DROP MATERIALIZED VIEW IF EXISTS vlci2.vw_t_agg_consumo_total_materialized;
CREATE MATERIALIZED VIEW vlci2.vw_datos_cb_agua_total_materialized AS
SELECT neighborhood AS name,
       kpivalue as value,
       DATE_PART('year', calculationperiod::date) as anyo,
       DATE_PART('month', calculationperiod::date) as mes
FROM vlci2.vw_t_agg_consumo_agua_total;

COMMIT;
