-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_manual_padron_estudios_agregado to pg

BEGIN;

create or replace view vw_datos_manual_padron_estudios_agregado as
SELECT
    gender,
    MAX(total) FILTER (WHERE academic_level = 'Bachiller/FP/Universidad') AS total_bach,
    MAX(total) FILTER (WHERE academic_level = 'Desconocido') AS total_desconocido,
    MAX(total) FILTER (WHERE academic_level = 'Graduado escolar') AS total_graduado,
    MAX(total) FILTER (WHERE academic_level = 'Inferior Graduado') AS total_graduado_inferior,
    MAX(total) FILTER (WHERE academic_level = 'Menor 18 años') AS total_menor_edad,
    MAX(total) FILTER (WHERE academic_level = 'Ni leer ni escribir') AS total_no_leer_escribir
FROM
    t_datos_manual_padron_estudios
GROUP BY
    gender
ORDER BY
    gender;
COMMIT;
