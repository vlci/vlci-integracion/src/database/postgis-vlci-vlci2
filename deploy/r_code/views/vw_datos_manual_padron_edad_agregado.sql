-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_manual_padron_edad_agregado to pg

BEGIN;

create or replace view vw_datos_manual_padron_edad_agregado as
SELECT
    age,
    nomdistrito,
    SUM(CASE WHEN gender = 'Hombre' THEN total else 0 END) AS male,
    SUM(CASE WHEN gender = 'Mujer' THEN total else 0 END) AS female,
    SUM(CASE WHEN total notnull THEN total else 0 END) as total
FROM t_datos_manual_padron_edad
GROUP BY
    age, nomdistrito
ORDER BY
    age;

COMMIT;
