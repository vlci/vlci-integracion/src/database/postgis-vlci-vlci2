-- Deploy postgis-vlci-vlci2:r_code/views/vw_t_agg_consumo_agua_desag to pg

BEGIN;

-- vlci2.vw_t_agg_consumo_agua_desag source
DROP VIEW vlci2.vw_t_agg_consumo_agua_desag;
CREATE OR REPLACE VIEW vlci2.vw_t_agg_consumo_agua_desag
AS SELECT t_f_text_cb_kpi_consumo_agua.entityid AS id,
    t_f_text_cb_kpi_consumo_agua.calculationperiod,
        CASE
            WHEN date_part('dow'::text, to_date(t_f_text_cb_kpi_consumo_agua.calculationperiod, 'yyyy-mm-dd'::text)) = 0::double precision THEN 7::double precision
            ELSE date_part('dow'::text, to_date(t_f_text_cb_kpi_consumo_agua.calculationperiod, 'yyyy-mm-dd'::text))
        END AS dayofweek,
    round(t_f_text_cb_kpi_consumo_agua.kpivalue::numeric, 2) AS kpivalue,
    t_f_text_cb_kpi_consumo_agua.name,
    t_f_text_cb_kpi_consumo_agua.neighborhood,
    t_f_text_cb_kpi_consumo_agua.consumptiontype,
    t_f_text_cb_kpi_consumo_agua.measurelandunit as measureunit
   FROM t_f_text_cb_kpi_consumo_agua
  WHERE t_f_text_cb_kpi_consumo_agua.consumptiontype = ANY (ARRAY['domestico'::text, 'industrial'::text, 'ServMunicipal'::text])
UNION
 SELECT 'kpi-agua-no-registrada'::character varying AS id,
    v_suma.calculationperiod,
        CASE
            WHEN date_part('dow'::text, to_date(v_suma.calculationperiod, 'yyyy-mm-dd'::text)) = 0::double precision THEN 7::double precision
            ELSE date_part('dow'::text, to_date(v_suma.calculationperiod, 'yyyy-mm-dd'::text))
        END AS dayofweek,
    v_total.total - v_suma.suma AS kpivalue,
    concat('Consumo_agua_No_registrada_', v_suma.neighborhood) AS name,
    v_suma.neighborhood,
    'Aigua no registrada per comptadors'::text AS consumptiontype,
    v_suma.measureunit
   FROM ( SELECT max(t_f_text_cb_kpi_consumo_agua.entityid::text) AS entityid,
            t_f_text_cb_kpi_consumo_agua.neighborhood,
            t_f_text_cb_kpi_consumo_agua.calculationperiod,
            max(t_f_text_cb_kpi_consumo_agua.measurelandunit) as measureunit,
            sum(t_f_text_cb_kpi_consumo_agua.kpivalue::numeric) AS suma
           FROM t_f_text_cb_kpi_consumo_agua
          WHERE t_f_text_cb_kpi_consumo_agua.consumptiontype <> 'N/A'::text
          GROUP BY t_f_text_cb_kpi_consumo_agua.neighborhood, t_f_text_cb_kpi_consumo_agua.calculationperiod) v_suma,
    ( SELECT t_f_text_cb_kpi_consumo_agua.neighborhood,
            t_f_text_cb_kpi_consumo_agua.calculationperiod,
            sum(t_f_text_cb_kpi_consumo_agua.kpivalue::numeric) AS total
           FROM t_f_text_cb_kpi_consumo_agua
          WHERE t_f_text_cb_kpi_consumo_agua.consumptiontype = 'N/A'::text
          GROUP BY t_f_text_cb_kpi_consumo_agua.neighborhood, t_f_text_cb_kpi_consumo_agua.calculationperiod) v_total
  WHERE v_total.neighborhood = v_suma.neighborhood AND v_total.calculationperiod = v_suma.calculationperiod;


COMMIT;
