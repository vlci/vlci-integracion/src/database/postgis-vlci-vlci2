-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_summary_monitoring_lastdata to pg
BEGIN;

CREATE
OR REPLACE VIEW vlci2.vw_datos_cb_summary_monitoring_lastdata AS
SELECT
    operationalstatus,
    project
FROM
    (
        SELECT
            operationalstatus,
            project
        FROM
            vlci2.vw_datos_cb_sonometro_noiselevelobserved_lastdata
        UNION
        ALL
        SELECT
            CASE
                WHEN operationalstatus = 'ok' THEN 'ok'
                WHEN operationalstatus = 'ko' THEN 'noData'
                ELSE 'noData'
            END AS operationalstatus,
            project
        FROM
            vlci2.t_datos_cb_parkingspot_lastdata
        UNION
        ALL
        SELECT
            operationalstatus,
            project
        FROM
            vw_datos_cb_medioambiente_airqualityobserved_lastdata
        UNION
        ALL
        SELECT
            operationalstatus,
            project
        FROM
            vw_datos_cb_medioambiente_emt_airquality_lastdata
        UNION
        ALL
        SELECT
            operationalstatus,
            case
                when vw_datos_cb_residuos_wastecontainer_lastdata.project isnull then 'Sin Proyecto'
                else project
            end as project
        FROM
            vw_datos_cb_residuos_wastecontainer_lastdata
        UNION
        ALL
        SELECT
            operationalstatus,
            project
        FROM
            vw_datos_cb_alumbrado_device_lastdata
        UNION
        ALL
        SELECT
            operationalstatus,
            project
        FROM
            vw_datos_cb_medioambiente_weatherobserved_10m_lastdata
        UNION
        ALL
        SELECT
            operationalstatus,
            case
                when project isnull then 'Sin Proyecto'
                else project
            end as project
        FROM
            t_datos_cb_trafico_aparcamientos_lastdata
        WHERE inactive='false'
    ) AS countstatus;

COMMIT;