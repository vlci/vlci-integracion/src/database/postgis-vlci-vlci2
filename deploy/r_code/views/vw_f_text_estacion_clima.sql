-- Deploy postgis-vlci-vlci2:r_code/views/vw_f_text_estacion_clima to pg

BEGIN;

CREATE
OR REPLACE VIEW vlci2.vw_f_text_estacion_clima AS
SELECT
    recvtime,
    fiwareservicepath,
    entityid,
    entitytype,
    dateobserved AS datetime,
    temperaturevalue AS temperatura,
    precipitationvalue AS precipitaciones,
    winddirectionvalue AS vientodireccion,
    windspeedvalue AS vientovelocidad,
    atmosphericpressurevalue AS presionbar,
    relativehumidityvalue AS hr,
    gis_id AS idestacion
FROM
    t_f_text_cb_weatherobserved;

COMMIT;
