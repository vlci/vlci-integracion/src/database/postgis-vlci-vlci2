-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_medioambiente_emt_airquality_lastdata to pg

BEGIN;

CREATE OR REPLACE VIEW vlci2.vw_datos_cb_medioambiente_emt_airquality_lastdata AS
SELECT entityid, dateobserved, exttemperature,extrelativehumidity,intrelativehumidity,inttemperature,noiselevel, co, no2,o3, pm25, maintenanceowner, location,project, 
       CASE WHEN dateobserved >= NOW() - INTERVAL '3 days' THEN 'ok' ELSE 'noData' END AS operationalstatus
FROM vlci2.t_datos_cb_medioambiente_emt_airquality_lastdata;

COMMIT;
