-- Deploy postgis-vlci-vlci2:r_code/views/vw_f_text_datos_sonometros_daily to pg

BEGIN;

CREATE OR REPLACE VIEW vlci2.vw_f_text_datos_sonometros_daily
AS SELECT row_number() OVER () AS idt_agg_son,
    sq.sonometro_direccion,
    sq.fecha,
    sq.dia_semana,
    round(sq.valor_manana::numeric, 2) AS valor_manana,
    round(sq.valor_tarde::numeric, 2) AS valor_tarde,
    round(sq.valor_noche::numeric, 2) AS valor_noche,
    sq.posicion,
    sq.areaServed
   FROM ( 
    select
	case
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248652-daily'::text then 'S4-Sueca Esq. Denia'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248655-daily'::text then 'S3-Cadiz 3'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248661-daily'::text then 'S6-General Prim Chaflan Donoso Cortes'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248669-daily'::text then 'S7-Dr.Serrano 21'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248670-daily'::text then 'S12-Carles Cervera, Chaflan Reina Dona Maria'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248671-daily'::text then 'S5-Cadiz 16'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248672-daily'::text then 'S8-PuertoRico 21'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248676-daily'::text then 'S15-Salvador Abril Chaflan Maestro Jose Serrano'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248677-daily'::text then 'S14-Vivons Chaflan Cadiz'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248678-daily'::text then 'S10-Carles Cervera 34'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248679-daily'::text then 'S13-Matias Perello Esq. Doctor Sumsi'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248680-daily'::text then 'S9-Sueca 32'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248682-daily'::text then 'S1-Cuba 3'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248683-daily'::text then 'S2-Sueca 2'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248684-daily'::text then 'S11-Sueca 61'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T251234-daily'::text then 'S16-Cura Femenía 14'::text
		else ''::text
	end as sonometro_direccion,
	to_date(t_f_text_cb_sonometros_ruzafa_daily.dateobserved, 'yyyy-MM-dd'::text) as fecha,
	case
		date_part('dow'::text, to_date(t_f_text_cb_sonometros_ruzafa_daily.dateobserved, 'yyyy-mm-dd'::text))
		when 1 then 'L'::text
		when 2 then 'M'::text
		when 3 then 'X'::text
		when 4 then 'J'::text
		when 5 then 'V'::text
		when 6 then 'S'::text
		when 0 then 'D'::text
		else null::text
	end as dia_semana,
	t_f_text_cb_sonometros_ruzafa_daily.laeq_d as valor_manana,
	t_f_text_cb_sonometros_ruzafa_daily.laeq_e as valor_tarde,
	t_f_text_cb_sonometros_ruzafa_daily.laeq_n as valor_noche,
	t_f_text_cb_sonometros_ruzafa_daily.location as posicion,
	'Russafa' as areaServed
from
	t_f_text_cb_sonometros_ruzafa_daily
group by
	(
                case
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248652-daily'::text then 'S4-Sueca Esq. Denia'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248655-daily'::text then 'S3-Cadiz 3'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248661-daily'::text then 'S6-General Prim Chaflan Donoso Cortes'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248669-daily'::text then 'S7-Dr.Serrano 21'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248670-daily'::text then 'S12-Carles Cervera, Chaflan Reina Dona Maria'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248671-daily'::text then 'S5-Cadiz 16'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248672-daily'::text then 'S8-PuertoRico 21'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248676-daily'::text then 'S15-Salvador Abril Chaflan Maestro Jose Serrano'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248677-daily'::text then 'S14-Vivons Chaflan Cadiz'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248678-daily'::text then 'S10-Carles Cervera 34'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248679-daily'::text then 'S13-Matias Perello Esq. Doctor Sumsi'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248680-daily'::text then 'S9-Sueca 32'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248682-daily'::text then 'S1-Cuba 3'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248683-daily'::text then 'S2-Sueca 2'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T248684-daily'::text then 'S11-Sueca 61'::text
		when t_f_text_cb_sonometros_ruzafa_daily.entityid::text = 'T251234-daily'::text then 'S16-Cura Femenía 14'::text
		else ''::text
	end),
	areaServed,
	t_f_text_cb_sonometros_ruzafa_daily.dateobserved,
	t_f_text_cb_sonometros_ruzafa_daily.laeq_d,
	t_f_text_cb_sonometros_ruzafa_daily.laeq_e,
	t_f_text_cb_sonometros_ruzafa_daily.laeq_n,
	t_f_text_cb_sonometros_ruzafa_daily.location
union all
 select
	case
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi1-daily'::text then 'S01 - Plaza del Cedro 12'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi2-daily'::text then 'S02 - Plaza del Cedro 1'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi3-daily'::text then 'S03 - Plaza del Cedro 10'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi4-daily'::text then 'S04 - Carrer Campoamor 54'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi5-daily'::text then 'S05 - Carrer Campoamor 60'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi6-daily'::text then 'S06 - Plaza Honduras 37 dcha'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi7-daily'::text then 'S07 - Plaza Honduras 37 centro'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi8-daily'::text then 'S08 - Plaza Honduras 37 izquierda'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi9-daily'::text then 'S09 - Plaza Honduras 37 exterior'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi10-daily'::text then 'S10 - Centro Servicio Social - Fachada Izq'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi11-daily'::text then 'S11 - Centro Servicio Social - Fachada Dch'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi12-daily'::text then 'S12 - Fuente en Benimaclet'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi13-daily'::text then 'S13 - Centro Servicio Social - Fachada Lat'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi14-daily'::text then 'S14 - Carrer Polo y Peyrolon 29'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi15-daily'::text then 'S15 - Carrer Polo y Peyrolon 13'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi16-daily'::text then 'S16 - Carrer Polo y Peyrolon 15'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi17-daily'::text then 'S17 - Carrer Polo y Peyrolon 43'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi18-daily'::text then 'S18 - Carrer Polo y Peyrolon 38'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi19-daily'::text then 'S19 - Carrer Polo y Peyrolon 53'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi20-daily'::text then 'S20 - Carrer Comte d Altea 20'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi21-daily'::text then 'S21 - Carrer Comte d Altea 21'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi22-daily'::text then 'S22 - Carrer Almirall Cadarso 30'::text
		else ''::text
	end as sonometro_direccion,
	to_date(t_datos_cb_sonometros_hopvlci_daily.dateobserved, 'yyyy-MM-dd'::text) as fecha,
	case
		date_part('dow'::text, to_date(t_datos_cb_sonometros_hopvlci_daily.dateobserved, 'yyyy-mm-dd'::text))
		when 1 then 'L'::text
		when 2 then 'M'::text
		when 3 then 'X'::text
		when 4 then 'J'::text
		when 5 then 'V'::text
		when 6 then 'S'::text
		when 0 then 'D'::text
		else null::text
	end as dia_semana,
	t_datos_cb_sonometros_hopvlci_daily.laeq_d as valor_manana,
	t_datos_cb_sonometros_hopvlci_daily.laeq_e as valor_tarde,
	t_datos_cb_sonometros_hopvlci_daily.laeq_n as valor_noche,
	t_datos_cb_sonometros_hopvlci_daily.location as posicion,
	t_datos_cb_sonometros_hopvlci_daily.areaServed
from
	t_datos_cb_sonometros_hopvlci_daily
group by
	(
                case
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi1-daily'::text then 'S01 - Plaza del Cedro 12'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi2-daily'::text then 'S02 - Plaza del Cedro 1'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi3-daily'::text then 'S03 - Plaza del Cedro 10'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi4-daily'::text then 'S04 - Carrer Campoamor 54'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi5-daily'::text then 'S05 - Carrer Campoamor 60'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi6-daily'::text then 'S06 - Plaza Honduras 37 dcha'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi7-daily'::text then 'S07 - Plaza Honduras 37 centro'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi8-daily'::text then 'S08 - Plaza Honduras 37 izquierda'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi9-daily'::text then 'S09 - Plaza Honduras 37 exterior'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi10-daily'::text then 'S10 - Centro Servicio Social - Fachada Izq'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi11-daily'::text then 'S11 - Centro Servicio Social - Fachada Dch'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi12-daily'::text then 'S12 - Fuente en Benimaclet'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi13-daily'::text then 'S13 - Centro Servicio Social - Fachada Lat'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi14-daily'::text then 'S14 - Carrer Polo y Peyrolon 29'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi15-daily'::text then 'S15 - Carrer Polo y Peyrolon 13'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi16-daily'::text then 'S16 - Carrer Polo y Peyrolon 15'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi17-daily'::text then 'S17 - Carrer Polo y Peyrolon 43'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi18-daily'::text then 'S18 - Carrer Polo y Peyrolon 38'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi19-daily'::text then 'S19 - Carrer Polo y Peyrolon 53'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi20-daily'::text then 'S20 - Carrer Comte d Altea 20'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi21-daily'::text then 'S21 - Carrer Comte d Altea 21'::text
		when t_datos_cb_sonometros_hopvlci_daily.entityid::text = 'NoiseLevelObserved-HOPVLCi22-daily'::text then 'S22 - Carrer Almirall Cadarso 30'::text
		else ''::text
	end),
	t_datos_cb_sonometros_hopvlci_daily.areaServed,
	t_datos_cb_sonometros_hopvlci_daily.dateobserved,
	t_datos_cb_sonometros_hopvlci_daily.laeq_d,
	t_datos_cb_sonometros_hopvlci_daily.laeq_e,
	t_datos_cb_sonometros_hopvlci_daily.laeq_n,
	t_datos_cb_sonometros_hopvlci_daily.location
   ) sq;

COMMIT;
