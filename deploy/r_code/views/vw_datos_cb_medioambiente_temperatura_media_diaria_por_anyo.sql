-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_medioambiente_temperatura_media_diaria_por_anyo to pg

BEGIN;

    CREATE OR REPLACE VIEW vlci2.vw_datos_cb_medioambiente_temperatura_media_diaria_por_anyo AS WITH datos_filtrados AS (
        SELECT
            calculationperiod::date AS date,
            date_part('year', calculationperiod::date) AS year,
            kpivalue AS value,
            measurelandunit AS measureUnit,
            id
        FROM
            vw_datos_cb_medioambiente_temperatura_media_diaria
    ),
    años_referentes AS (
        SELECT
            MAX(year) AS año_actual,
            MAX(year) - 1 AS año_anterior_1,
            MAX(year) - 2 AS año_anterior_2,
            MIN(year) AS año_inicio
        FROM
            datos_filtrados
    ),
    series AS (
        -- Serie para el año actual con id 'temperatura-Media-Diaria-vm' o 'temperatura-Media-Diaria-va'
        SELECT
            d.date,
            d.value,
            d.measureUnit,
            d.id,
            a.año_actual AS año,
            a.año_actual::text AS name
        FROM 
            datos_filtrados d
        JOIN años_referentes a ON d.year = a.año_actual
        WHERE d.id IN ('temperatura-media-diaria-vm', 'temperatura-Media-Diaria-va')    
        UNION ALL
        SELECT
            d.date,
            d.value,
            d.measureUnit,
            d.id,
            a.año_actual AS año,
            a.año_actual::text || ' pdte. de validar' AS name
        FROM 
            datos_filtrados d
        JOIN años_referentes a ON d.year = a.año_actual
        WHERE d.id = 'Kpi-Temperatura-Media-Diaria'   
        UNION ALL    -- Serie para el año anterior 1
        SELECT
            d.date,
            d.value,
            d.measureUnit,
            d.id,
            a.año_anterior_1 AS año,
            a.año_anterior_1::text AS name
        FROM 
            datos_filtrados d
        JOIN años_referentes a ON d.year = a.año_anterior_1
        UNION ALL    -- Serie para el año anterior 2
        SELECT
            d.date,
            d.value,
            d.measureUnit,
            d.id,
            a.año_anterior_2 AS año,
            a.año_anterior_2::text AS name
        FROM 
            datos_filtrados d
        JOIN años_referentes a ON d.year = a.año_anterior_2
        UNION ALL    -- Serie para el promedio desde el año más antiguo hasta el año_actual - 1
        SELECT
            make_date(2024, date_part('month', d.date)::int, date_part('day', d.date)::int) AS date,  -- Agrupar por día y mes
            ROUND(AVG(d.value)) AS value,  -- Calcular la media redondeada a 0 decimales
            d.measureUnit,
            NULL AS id,
            NULL AS año,
            'Mitjana diari des de ' || a.año_inicio::text || ' a ' || (a.año_actual - 1)::text AS name
        FROM
            datos_filtrados d
        JOIN años_referentes a ON d.year BETWEEN a.año_inicio AND a.año_anterior_1
        GROUP BY
            date_part('month', d.date), date_part('day', d.date), d.measureUnit, a.año_inicio, a.año_actual  -- Agrupar por día y mes
    ) 
    SELECT
        s.name,
        TO_CHAR(s.date, 'DD/MM') AS date,  -- Formato DD/MM
        s.value,
        s.measureUnit
    FROM
        series s
    WHERE s.date IS NOT NULL OR s.año IS NOT NULL  -- Filtrar para mostrar solo registros relevantes
    ORDER BY
        s.name, s.date;

COMMIT;
