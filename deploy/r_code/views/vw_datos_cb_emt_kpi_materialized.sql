-- Deploy postgis-vlci-vlci2:r_code/views/vw_datos_cb_emt_kpi_materialized to pg

BEGIN;
DROP MATERIALIZED VIEW IF EXISTS vlci2.vw_datos_cb_emt_kpi_materalized;

CREATE MATERIALIZED VIEW vlci2.vw_datos_cb_emt_kpi_materialized AS
select sliceanddicevalue1 as name, 
	sliceanddice1 as type,
	kpivalue as value, 
	DATE_PART('year', calculationperiod::date) as anyo,
	DATE_PART('month', calculationperiod::date) as mes
from vlci2.t_datos_cb_emt_kpi tdcek ;

COMMIT;
