-- Deploy postgis-vlci-vlci2:v_migrations/2998-cambiar-clave-primaria-aguas-desag to pg

BEGIN;

-- Eliminar la clave primaria existente
ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_desag_corregida
    DROP CONSTRAINT t_datos_cb_cia_inyeccion_agua_desag_corregida_pkey;

-- Crear una nueva clave primaria con originalentityid
ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_desag_corregida
    ADD CONSTRAINT t_datos_cb_cia_inyeccion_agua_desag_corregida_pkey PRIMARY KEY (calculationperiod, originalentityid);

COMMIT;
