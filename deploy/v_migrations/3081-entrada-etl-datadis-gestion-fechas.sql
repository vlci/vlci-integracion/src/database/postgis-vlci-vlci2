-- Deploy postgis-vlci-vlci2:v_migrations/3081-entrada-etl-datadis-gestion-fechas to pg

BEGIN;

INSERT INTO vlci2.t_d_fecha_negocio_etls
(etl_id, etl_nombre, periodicidad, "offset", fen, fen_inicio, fen_fin, estado, formato_fen, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES
(303, 'py_energia_extraccion_api_datadis_consumos_municipales', 'MENSUAL', 0, null, null, null, 'OK', 'YYYYMMddHH24MISS', null, 'Sabri 3081' , null, null);

COMMIT;
