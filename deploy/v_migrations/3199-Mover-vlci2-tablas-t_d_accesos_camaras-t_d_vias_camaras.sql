-- Deploy postgis-vlci-vlci2:v_migrations/3199-Mover-vlci2-tablas-t_d_accesos_camaras-t_d_vias_camaras to pg

BEGIN;

ALTER TABLE cdmt.t_d_accesos_camaras SET SCHEMA vlci2;
ALTER TABLE cdmt.t_d_vias_camaras SET SCHEMA vlci2;

COMMIT;
