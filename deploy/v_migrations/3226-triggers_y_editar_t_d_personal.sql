-- Deploy postgis-vlci-vlci2:v_migrations/3226-triggers_y_editar_t_d_personal to pg

BEGIN;

UPDATE vlci2.t_d_personal
SET aud_fec_ins = NULL,
    aud_fec_upd = NULL,
    aud_user_ins = NULL,
    aud_user_upd = NULL;

ALTER TABLE vlci2.t_d_personal
    ALTER COLUMN aud_fec_ins TYPE timestamp USING aud_fec_ins::timestamp,
    ALTER COLUMN aud_fec_upd TYPE timestamp USING aud_fec_upd::timestamp,
    ALTER COLUMN aud_user_ins TYPE varchar,
    ALTER COLUMN aud_user_upd TYPE varchar;


create trigger vlci2_t_d_personal_ins before
insert on vlci2.t_d_personal for each row execute function fn_aud_fec_ins_etl();
 
create trigger vlci2_t_d_personal_upd before 
update on vlci2.t_d_personal for each row execute function fn_aud_fec_upd_etl(); 

COMMIT;
