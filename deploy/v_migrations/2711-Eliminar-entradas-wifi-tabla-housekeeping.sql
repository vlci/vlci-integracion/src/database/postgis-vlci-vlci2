-- Deploy postgis-vlci-vlci2:v_migrations/2711-Eliminar-entradas-wifi-tabla-housekeeping to pg

BEGIN;

DELETE FROM vlci2.housekeeping_config
WHERE table_nam IN (
    't_agg_conexiones_wifi_urbo',
    't_f_aps_agg',
    't_f_ap_avisos',
    't_f_ap_detail_rt',
    't_f_ap_urbo',
    't_f_conexiones_wifi_urbo',
    't_f_num_poi_agg',
    't_f_poi_rt',
    't_f_poi_urbo_di',
    't_f_poi_urbo_hr',
    't_f_poi_urbo_rt'
);

COMMIT;
