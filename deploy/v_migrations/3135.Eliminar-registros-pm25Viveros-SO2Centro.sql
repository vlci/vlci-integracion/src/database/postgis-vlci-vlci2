-- Deploy postgis-vlci-vlci2:v_migrations/3135.Eliminar-registros-pm25Viveros-SO2Centro to pg

BEGIN;

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET so2value = NULL,
    so2valueflag = NULL
WHERE refpointofinterest  = 'A07_VALENCIACENTRE';

COMMIT;
