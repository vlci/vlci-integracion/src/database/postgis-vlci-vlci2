-- Deploy postgis-vlci-vlci2:v_migrations/2899-Cargar-datos-históricos-correctamente to pg

BEGIN;

TRUNCATE TABLE vlci2.t_datos_cb_trafico_kpi_validados_anyo;
TRUNCATE TABLE vlci2.t_datos_cb_trafico_kpi_validados_mes;

COMMIT;
