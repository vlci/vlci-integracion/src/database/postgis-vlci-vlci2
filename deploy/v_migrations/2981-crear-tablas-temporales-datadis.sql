-- Deploy postgis-vlci-vlci2:v_migrations/2981-crear-tablas-temporales-datadis to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_extraccion_datadis_suministros_temporal (
    cups VARCHAR(255) NOT NULL,
    address TEXT,
    postalCode VARCHAR(20),
    province VARCHAR(100),
    municipality VARCHAR(100),
    pointType VARCHAR(50),
    distributorCode VARCHAR(50),
    PRIMARY KEY (cups)
);
CREATE TABLE vlci2.t_datos_etl_extraccion_datadis_consumos_municipales_temporal (
    cups VARCHAR(255) NOT NULL,
    surplusEnergyKWh NUMERIC(12, 3),
    consumptionKWh NUMERIC(12, 3),
    date DATE NOT NULL,
    time TIME NOT NULL,
    obtainMethod VARCHAR(255),
    PRIMARY KEY (cups, date, time)
);

COMMIT;