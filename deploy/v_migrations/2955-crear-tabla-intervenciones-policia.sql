-- Deploy postgis-vlci-vlci2:v_migrations/2955-crear-tabla-intervenciones-policia to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_policia_intervenciones_last_data (
    recvtime timestamptz not null,
    fiwareservicepath text null,
    entitytype text null,
    entityid text not null PRIMARY KEY,
    dateobserved timestamp not null,
    idintervention int4 not null,
    timeofday text null,
    category text null,
    idcategory int4 null,
    subcategory text null,
    idsubcategory int4 null,
    detailcategory text null,
    iddetailcategory int4 null,
    district text null,
    neighborhood text null,
    location public.geometry(point) not null
);

COMMIT;
