-- Deploy postgis-vlci-vlci2:v_migrations/3226-edicion-columnas-personal to pg

BEGIN;

ALTER TABLE vlci2.t_datos_etl_personal_ayuntamiento
ADD COLUMN grupo VARCHAR,
ADD COLUMN subgrupo VARCHAR,
ADD COLUMN area_adagp_extra VARCHAR,
ADD COLUMN area_gip VARCHAR,
ADD COLUMN delegacion_adagp VARCHAR,
ADD COLUMN delegacion_gip VARCHAR,
ADD COLUMN desc_provision_puesto VARCHAR,
ADD COLUMN negociado_adagp VARCHAR,
ADD COLUMN negociado_gip VARCHAR,
ADD COLUMN organo_adagp VARCHAR,
ADD COLUMN organo_gip VARCHAR,
ADD COLUMN posicion_gip VARCHAR,
ADD COLUMN puesto_gip VARCHAR,
ADD COLUMN seccion_adagp VARCHAR,
ADD COLUMN seccion_gip VARCHAR,
ADD COLUMN servicio_adagp VARCHAR,
ADD COLUMN servicio_gip VARCHAR;

ALTER TABLE vlci2.t_datos_etl_personal_ayuntamiento
DROP COLUMN vinc_laboral;


COMMIT;
