-- Deploy postgis-vlci-vlci2:v_migrations/2929-inserts-validated-wheather-fecha-negocio to pg

DO $$
DECLARE
    max_id integer;
BEGIN

	max_id := (select max(etl_id) from vlci2.t_d_fecha_negocio_etls);
	insert into vlci2.t_d_fecha_negocio_etls (etl_id, etl_nombre, periodicidad, "offset", formato_fen, aud_user_ins, fen, estado) values (max_id+1, 'W01_AVFRANCIA_temperatura_vm', 'DIARIA', 30, 'YYYYMMddHH24MISS', 'Julián 2929', '2024-06-01 00:00:00.000', 'OK');
	insert into vlci2.t_d_fecha_negocio_etls (etl_id, etl_nombre, periodicidad, "offset", formato_fen, aud_user_ins, fen, estado) values (max_id+2, 'W02_NAZARET_precipitaciones_vm', 'DIARIA', 30, 'YYYYMMddHH24MISS', 'Julián 2929', '2024-06-01 00:00:00.000', 'OK');
	insert into vlci2.t_d_fecha_negocio_etls (etl_id, etl_nombre, periodicidad, "offset", formato_fen, aud_user_ins, fen, estado) values (max_id+3, 'W01_AVFRANCIA_temperatura_va', 'DIARIA', 91, 'YYYYMMddHH24MISS', 'Julián 2929', '2016-01-01 00:00:00.000', 'OK');
	insert into vlci2.t_d_fecha_negocio_etls (etl_id, etl_nombre, periodicidad, "offset", formato_fen, aud_user_ins, fen, estado) values (max_id+4, 'W02_NAZARET_precipitaciones_va', 'DIARIA', 91, 'YYYYMMddHH24MISS', 'Julián 2929', '2016-01-01 00:00:00.000', 'OK');

END $$;
