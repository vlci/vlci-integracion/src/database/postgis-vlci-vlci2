-- Deploy postgis-vlci-vlci2:v_migrations/3116-crear-tabla-recuperar-entradas to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_extraccion_datadis_recuperar_entradas_temporal (
    cups TEXT NOT NULL,
    mesAnyo TEXT NOT NULL,
    distributorCode TEXT NOT NULL,
    pointType TEXT NOT NULL,

    PRIMARY KEY (cups, mesAnyo)
);
COMMIT;
