-- Deploy postgis-vlci-vlci2:v_migrations/3135.Eliminar-registros-pm25Viveros to pg

BEGIN;

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET pm25value = NULL,
    pm25valueflag = NULL
WHERE dateobserved BETWEEN '2020-03-01 00:00:00.000' AND '2023-12-31 23:59:59.999' and refpointofinterest  = 'A06_VIVERS' ;

COMMIT;

