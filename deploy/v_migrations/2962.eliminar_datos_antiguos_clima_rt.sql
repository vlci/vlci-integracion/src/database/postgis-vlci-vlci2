-- Deploy postgis-vlci-vlci2:v_migrations/2962.eliminar_datos_antiguos_clima_rt to pg
BEGIN;

INSERT INTO
  vlci2.housekeeping_config (table_nam, tiempo, colfecha)
VALUES
  (
    't_f_text_kpi_temperatura_media_diaria',
    6,
    'calculationperiod'
  ),
  (
    't_f_text_kpi_precipitaciones_diarias',
    6,
    'calculationperiod'
  );

COMMIT;