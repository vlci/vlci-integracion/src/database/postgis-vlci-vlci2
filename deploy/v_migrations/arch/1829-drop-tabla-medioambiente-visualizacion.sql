-- Deploy postgis-vlci-vlci2:v_migrations/1829-drop-tabla-medioambiente-visualizacion to pg

BEGIN;

INSERT INTO vlci2.t_ref_horizontal_visualizacion
(vertical, id, isvisiblevminut, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
select 'ESTACIONES_CONTAMINACION',entityid,isvisiblevminut,aud_fec_ins,aud_user_ins,NOW(),'Ivan-1829' from t_ref_medioambiente_visualizacion_estaciones trmve;

DROP TABLE t_ref_medioambiente_visualizacion_estaciones;

COMMIT;
