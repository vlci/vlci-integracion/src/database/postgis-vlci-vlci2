-- Deploy postgis-vlci-vlci2:v_migrations/t_datos_etl_estadistica_valores_indicadores to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_estadistica_valores_indicadores (
    nomindicador  varchar(20)  NOT NULL,
    valorperiodicidad  Int4  NOT NULL,
    crit1cas  varchar(200),
    crit1val  varchar(200),
    crit2cas  varchar(200),
    crit2val  varchar(200), 
    crit3cas  varchar(200),
    crit3val  varchar(200),   
    crit4cas  varchar(200),
    crit4val  varchar(200),
    valorcrit1cas varchar(200)  NOT NULL,
    valorcrit1val varchar(200)  NOT NULL,
    valorcrit2cas varchar(200)  NOT NULL,
    valorcrit2val varchar(200)  NOT NULL,
    valorcrit3cas varchar(200)  NOT NULL,
    valorcrit3val varchar(200)  NOT NULL,
    valorcrit4cas varchar(200)  NOT NULL,
    valorcrit4val varchar(200)  NOT NULL,
    distrito  varchar(4)  NOT NULL, 
    barrio  varchar(4)  NOT NULL,
    seccioncensal  varchar(4)  NOT NULL,
    valor  float8  NOT NULL,
    ods  Int4,
    meta  varchar(50),
    indicador  Int4,
    aud_fec_ins  timestamptz,
    aud_user_ins  VARCHAR(100),
    aud_fec_upd  timestamptz,
    aud_user_upd  VARCHAR(100),
    PRIMARY KEY (nomindicador, valorperiodicidad, valorcrit1cas, valorcrit1val, valorcrit2cas, valorcrit2val, valorcrit3cas, valorcrit3val, valorcrit4cas, valorcrit4val, distrito, barrio, seccioncensal)
);

COMMIT;
