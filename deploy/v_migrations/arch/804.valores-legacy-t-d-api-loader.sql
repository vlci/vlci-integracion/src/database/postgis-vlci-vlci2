-- Deploy postgis-vlci-vlci2:v_migrations/804.valores-legacy-t-d-api-loader to pg
-- tarea PROY2100017N3-804

BEGIN;

UPDATE vlci2.t_d_api_loader SET legacy = 'S' WHERE nombre_api LIKE 'Airwave%' OR nombre_api LIKE 'Jardineria%';

COMMIT;