-- Deploy postgis-vlci-vlci2:v_migrations/1599.medioambiente-contaminación-etl-bloqueada-falta-datos to pg

BEGIN;

-- binds: 3421 - 2023-03-26 05:20:00.000
-- binds: 3422 - 2023-03-26 05:20:00.000
update t_d_fecha_negocio_etls set fen='2023-03-26 05:20:00.000', estado='OK', aud_user_upd='1599-xavi'
where etl_nombre = 'A11_PATRAIX_10m';

update t_d_fecha_negocio_etls set fen='2023-03-26 06:00:00.000', estado='OK', aud_user_upd='1599-xavi'
where etl_nombre = 'A11_PATRAIX_60m';

COMMIT;
