-- Deploy postgis-vlci-vlci2:v_migrations/TLF028-correcion-datos-ruzafa to pg

BEGIN;

UPDATE t_f_text_cb_sonometros_ruzafa_daily
set laeq_d = cast(CAST(SUBSTRING(laeq_d FROM 3) AS DECIMAL) - 3 as TEXT)
where laeq_d like '-3%';

UPDATE t_f_text_cb_sonometros_ruzafa_daily
set laeq = cast(CAST(SUBSTRING(laeq FROM 3) AS DECIMAL) - 3 as TEXT)
where laeq like '-3%';

UPDATE t_f_text_cb_sonometros_ruzafa_daily
set laeq_e = cast(CAST(SUBSTRING(laeq_e FROM 3) AS DECIMAL) - 3 as TEXT)
where laeq_e like '-3%';

UPDATE t_f_text_cb_sonometros_ruzafa_daily
set laeq_n = cast(CAST(SUBSTRING(laeq_n FROM 3) AS DECIMAL) - 3 as TEXT)
where laeq_n like '-3%';

UPDATE t_f_text_cb_sonometros_ruzafa_daily
set laeq_den  = cast(CAST(SUBSTRING(laeq_den  FROM 3) AS DECIMAL) - 3 as TEXT)
where laeq_den like '-3%';

COMMIT;
