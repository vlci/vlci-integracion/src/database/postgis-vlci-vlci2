-- Deploy postgis-vlci-vlci2:v_migrations/65233.alter-index-unidad-medida to pg

BEGIN;

DROP INDEX idx_86814_i_m_un_dc_unidad_medida_cas;

CREATE INDEX idx_86814_i_m_un_dc_unidad_medida_cas ON vlci2.t_m_unidad_medida USING btree (dc_unidad_medida_cas);

COMMIT;
