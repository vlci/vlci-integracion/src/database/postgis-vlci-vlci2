-- Deploy postgis-vlci-vlci2:v_migrations/832.insert-api-loader-campos-request to pg
-- Tarea PROY2100017N3-832

BEGIN;

insert into vlci2.t_d_api_loader_campos_request (nombre_api, campo, valor) 
	values ('Geoportal_suma_carril_bici', 'where', '1=1'),
			('Geoportal_suma_carril_bici', 'outStatistics', '%5B%7B%0D%0A++++%22statisticType%22%3A+%22sum%22%2C%0D%0A++++%22onStatisticField%22%3A+%22st_length%28shape%29%2F1000%22%2C+++++%22outStatisticFieldName%22%3A+%22Sum%22%0D%0A%7D%5D%0D%0A'),
			('Geoportal_suma_carril_bici', 'f', 'json'),
			('Geoportal_total_reguladores_led', 'where', 'optica+%3D+%27Led%27+'),
			('Geoportal_total_reguladores_led', 'returnCountOnly', 'true'),
			('Geoportal_total_reguladores_led', 'f', 'json'),
			('Geoportal_total_reguladores', 'where', '1=1'),
			('Geoportal_total_reguladores', 'returnCountOnly', 'true'),
			('Geoportal_total_reguladores', 'f', 'json'),
			('Geoportal_total_paneles', 'where', '1=1'),
			('Geoportal_total_paneles', 'returnCountOnly', 'true'),
			('Geoportal_total_paneles', 'f', 'json'),
			('Geoportal_total_camaras_web', 'where', '1=1'),
			('Geoportal_total_camaras_web', 'returnCountOnly', 'true'),
			('Geoportal_total_camaras_web', 'f', 'json'),
			('Geoportal_total_aparcamotos', 'where', '1=1'),
			('Geoportal_total_aparcamotos', 'returnCountOnly', 'true'),
			('Geoportal_total_aparcamotos', 'f', 'json'),
			('Geoportal_total_carga_descarga', 'where', '1=1'),
			('Geoportal_total_carga_descarga', 'returnCountOnly', 'true'),
			('Geoportal_total_carga_descarga', 'f', 'json'),
			('Geoportal_total_paradas_taxi', 'where', '1=1'),
			('Geoportal_total_paradas_taxi', 'returnCountOnly', 'true'),
			('Geoportal_total_paradas_taxi', 'f', 'json'),
			('Geoportal_suma_ejes_calle', 'where', '1=1'),
			('Geoportal_suma_ejes_calle', 'outStatistics', '%5B%7B%0D%0A++++%22statisticType%22%3A+%22sum%22%2C%0D%0A++++%22onStatisticField%22%3A+%22longitud%2F1000%22%2C%0D%0A++++%22outStatisticFieldName%22%3A+%22Sum%22%0D%0A%7D%5D'),
			('Geoportal_suma_ejes_calle', 'f', 'json'),
			('Geoportal_total_carril_bici', 'where', '1=1'),
			('Geoportal_total_carril_bici', 'returnCountOnly', 'true'),
			('Geoportal_total_carril_bici', 'f', 'json'),
			('Geoportal_suma_bici_aparcamiento', 'where', '1=1'),
			('Geoportal_suma_bici_aparcamiento', 'outStatistics', '%5B%7B%0D%0A++++%22statisticType%22%3A+%22sum%22%2C%0D%0A++++%22onStatisticField%22%3A+%22numplazas%22%2C+++++%22outStatisticFieldName%22%3A+%22Sum%22%0D%0A%7D%5D'),
			('Geoportal_suma_bici_aparcamiento', 'f', 'json'),
			('Geoportal_suma_minusvalidos', 'where', '1=1'),
			('Geoportal_suma_minusvalidos', 'outStatistics', '%5B%7B%0D%0A++++%22statisticType%22%3A+%22sum%22%2C%0D%0A++++%22onStatisticField%22%3A+%22numplazas%22%2C+%0D%0A++++%22outStatisticFieldName%22%3A+%22Sum%22%0D%0A%7D%5D'),
			('Geoportal_suma_minusvalidos', 'f', 'json'),
			('Geoportal_total_aparcamientos', 'where', 'tipo=0'),
			('Geoportal_total_aparcamientos', 'returnCountOnly', 'true'),
			('Geoportal_total_aparcamientos', 'f', 'json');

COMMIT;
