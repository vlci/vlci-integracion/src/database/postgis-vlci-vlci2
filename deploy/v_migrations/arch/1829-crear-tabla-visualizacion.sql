-- Deploy postgis-vlci-vlci2:v_migrations/1829-crear-tabla-visualizacion to pg

BEGIN;

CREATE TABLE vlci2.t_ref_horizontal_visualizacion (
	vertical varchar(200) NOT NULL,
	id varchar(200) NOT NULL,
	isvisiblevminut bool NOT NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(255) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(255) NULL,
	CONSTRAINT t_ref_horizontal_visualizacion_pkey PRIMARY KEY (vertical, id)
);

COMMIT;
