-- Deploy postgis-vlci-vlci2:v_migrations/1912.tesoreria.create-table-inco to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_inco_ingresos (
	anyo smallint NOT NULL,
	fecha date NOT NULL,
	id_concepto numeric(5) NOT NULL,
    prevision_inicial NUMERIC(13, 2),	 
    modificacion_inicial NUMERIC(13, 2),
    prevision_definitiva NUMERIC(13, 2), 
    derechos_reconocidos NUMERIC(13, 2),
    devolucion NUMERIC(13, 2),
    derechos_reconocidos_netos NUMERIC(13, 2),
    recaudación NUMERIC(13, 2),
    derechos_anulados NUMERIC(13, 2),
    recaudacion_neta NUMERIC(13, 2),
    derechos_pendientes_cobro NUMERIC(13, 2),
    derechos_cancelados NUMERIC(13, 2),
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(200) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(200) NULL,
    CONSTRAINT t_datos_etl_inco_ingresos_pkey PRIMARY KEY (fecha, id_concepto)
);

COMMIT;
