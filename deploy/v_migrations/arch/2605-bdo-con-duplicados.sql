-- Deploy postgis-vlci-vlci2:v_migrations/2605-bdo-con-duplicados to pg

BEGIN;

-- AREAS.
    DELETE FROM vlci2.t_d_area 
    WHERE desc_corta_val IN ('ÁREA DE DESARROLLO INNOVADOR DE LOS SECTORES ECONÓMICOS Y OCUPACIÓN', 'ÁREA DE PARTICIPACIÓN, DERECHOS E INNOVACIÓN DE LA DEMOCRACIA','ÁREA DE BIENESTAR Y DERECHOS SOCIALES','ÁREA DE GESTIÓN DE RECURSOS','ÁREA DE PROTECCIÓN CIUDADANA','ÁREA DE MOVILIDAD SOSTENIBLE Y ESPACIO PÚBLICO','ÁREA DE DESARROLLO Y RENOVACIÓN URBANA Y VIVIENDA','ÁREA DE ECOLOGÍA URBANA, EMERGENCIA CLIMÁTICA Y TRANSICIÓN ENERGÉTICA','ÁREA DE EDUCACIÓN, CULTURA Y DEPORTES');

-- DELEGACIONES.
    create table vlci2.t_d_delegacion_tmp 
    as SELECT DISTINCT fk_area_id ,
                    pk_delegacion_id,
                    desc_corta_cas,
                    desc_corta_val,
                    desc_larga_cas,
                    desc_larga_val,
                    aud_fec_ins,
                    aud_user_ins,
                    aud_fec_upd,
                    aud_user_upd 
    FROM vlci2.t_d_delegacion;


    truncate table vlci2.t_d_delegacion;

    insert into vlci2.t_d_delegacion 
    SELECT 			pk_delegacion_id,
                    fk_area_id ,
                    desc_corta_cas,
                    desc_corta_val,
                    desc_larga_cas,
                    desc_larga_val,
                    aud_fec_ins,
                    aud_user_ins,
                    aud_fec_upd,
                    aud_user_upd 
    FROM vlci2.t_d_delegacion_tmp;

    drop table vlci2.t_d_delegacion_tmp;

    delete from vlci2.t_d_delegacion where desc_corta_val = 'ALCALDÍA';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE PROTECCIÓN CIVIL';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE ACCIÓN CULTURAL';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE JUVENTUD';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE GESTIÓN DE OBRAS DE INFRAESTRUCTURA';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE MANTENIMIENTO DE INFRAESTRUCTURAS';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE CONTROL ADMINISTRATIVO';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEG. DE CALIDAD ACUSTICA Y DEL AIRE';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE INNOVACIÓN Y GESTIÓN DEL CONOCIMIENTO';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE EMERGENCIA CLIMÁTICA Y TRANSICIÓN ENERGÉTICA';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE COORDINACIÓN JURIDICA';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE COOPERACIÓN AL DESARROLLO Y MIGRACIÓN';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE BIENESTAR ANIMAL';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE PATRIMONIO Y RECURSOS CULTURALES';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE RELACIONES INSTITUCIONALES';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'INTERVENCION GENERAL MUNICIPAL';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'TESORERIA GENERAL';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'INTERVENCION DE CONTABILIDAD Y PRES.';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE TRANSPARENCIA Y GOBIERNO ABIERTO';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACION DE PUEBLOS DE VALÈNCIA';
    delete from vlci2.t_d_delegacion where desc_corta_val = 'DELEGACIÓN DE CONTRATACIÓN';


-- SERVICIOS.
    delete from vlci2.t_d_servicio where aud_user_ins = '2605 - JULIAN' and desc_larga_cas like '2023%';

COMMIT;
