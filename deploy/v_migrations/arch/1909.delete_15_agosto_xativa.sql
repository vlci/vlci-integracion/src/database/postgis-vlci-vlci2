-- Deploy postgis-vlci-vlci2:v_migrations/1909.delete_15_agosto_xativa to pg

BEGIN;

DELETE FROM vlci2.t_f_text_kpi_trafico_y_bicis where calculationperiod = '2023-08-15' and tramo = 'Xàtiva';

COMMIT;
