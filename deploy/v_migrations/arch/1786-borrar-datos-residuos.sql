-- Deploy postgis-vlci-vlci2:v_migrations/1787-borrar-datos-residuos to pg

BEGIN;

delete from vlci2.t_f_text_cb_kpi_residuos 
where to_date(calculationperiod,'dd-mm-yyyy') >= '01-03-2020'
and to_date(calculationperiod,'dd-mm-yyyy') < '01-01-2022';

COMMIT;
