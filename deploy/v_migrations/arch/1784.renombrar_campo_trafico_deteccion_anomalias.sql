-- Deploy postgis-vlci-vlci2:v_migrations/1784.renombrar_campo_trafico_deteccion_anomalias to pg

BEGIN;

ALTER TABLE t_datos_python_trafico_deteccionanomalias 
RENAME COLUMN measureunitcas TO tables2check;

COMMIT;
