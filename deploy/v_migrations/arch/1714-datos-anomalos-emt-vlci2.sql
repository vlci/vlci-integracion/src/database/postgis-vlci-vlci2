-- Deploy postgis-vlci-vlci2:v_migrations/1714-datos-anomalos-emt-vlci2 to pg

BEGIN;

delete from vlci2.t_datos_cb_emt_kpi
    where sliceanddice1 in('Titulo', 'Ruta') 
	and calculationperiod in ('2023/05/28', '2023/05/23', '2023/05/21', '2023/05/20', '2023/05/19');


COMMIT;
