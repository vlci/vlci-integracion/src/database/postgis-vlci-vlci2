-- Deploy postgis-vlci-vlci2:v_migrations/2673-creacion-tabla-estacionamiento-inteligente to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_trafico_kpi_estacionamiento_inteligente (
    recvtime timestamp with time zone not null,
    fiwareservicepath text,
    entitytype text,
    entityid text not null,
    originalentityid text not null,
    originalentitytype text,
    description text,
    serviceName text,
    sliceAndDice text,
    calculationPeriod text not null,
    kpiValue double precision not null,
    sliceAndDiceValue1 text not null,
    descriptionCas text,
    sliceAndDiceCas text,
    sliceAndDiceValue1Cas text,
    CONSTRAINT t_datos_cb_trafico_kpi_estacionamiento_inteligente_pkey PRIMARY KEY (originalentityid, calculationPeriod, sliceAndDiceValue1)
);

COMMIT;
