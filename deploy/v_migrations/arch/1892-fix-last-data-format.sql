-- Deploy postgis-vlci-vlci2:v_migrations/1892-fix-last-data-format to pg
-- Se elimina la tabla y se vuelve a crear, es una tabla de tipo lastdata por lo que los datos se cargarán como máximo en 5 minutos de nuevo

BEGIN;

DROP TABLE vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata;

CREATE TABLE vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata (
	recvtime timestamptz NULL,
	fiwareservicepath varchar(100) NULL,
	entityid varchar(100) NOT NULL,
	entitytype varchar(100) NULL,
	description varchar(200) NULL,
	descriptioncas varchar(200) NULL,
	dateobserved timestamptz NULL,
	dateobservedfrom timestamptz NULL,
	dateobservedto timestamptz NULL,
	intensity int4 NULL,
	"location" public.geometry NULL,
	"name" varchar(200) NULL,
	operationalstatus varchar(200) NULL,
	locationforgisvisualization public.geometry NULL,
	lowerthresholdrt numeric NULL,
	middlethresholdrt numeric NULL,
	upperthresholdrt numeric NULL,
	CONSTRAINT t_datos_cb_trafico_cuenta_bicis_lastdata_pkey PRIMARY KEY (entityid)
);


COMMIT;
