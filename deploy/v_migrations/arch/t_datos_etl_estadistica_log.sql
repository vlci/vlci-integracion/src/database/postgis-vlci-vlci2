-- Deploy postgis-vlci-vlci2:v_migrations/t_datos_etl_estadistica_log to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_estadistica_log (
    idlog  serial  NOT NULL,
    nombrefichero varchar(100)  NOT NULL,
    descripcion  varchar(200)  NOT NULL,
    aud_fec_ins  timestamptz,
    aud_user_ins  VARCHAR(100),
    aud_fec_upd  timestamptz,
    aud_user_upd  VARCHAR(100),
    PRIMARY KEY (Idlog)
);

COMMIT;
