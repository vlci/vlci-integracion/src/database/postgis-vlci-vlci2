-- Deploy postgis-vlci-vlci2:v_migrations/1728-crear-tabla-deteccion-anomalias-trafico-cochebicis to pg

BEGIN;

CREATE TABLE vlci2.t_datos_python_trafico_deteccionanomalias (
	time timestamptz,
	motivo varchar(100),
	id varchar(100),
	measureunitcas varchar(100),
	status varchar(100)
);

COMMIT;
