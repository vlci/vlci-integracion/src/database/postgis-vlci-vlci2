-- Deploy postgis-vlci-vlci2:v_migrations/2636-create-table-cia-inyeccion-agua-corregida to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida (
    
    calculationperiod TIMESTAMP NULL,
    dayofweek INT NULL,
    kpivalue DECIMAL NULL,
    "name" VARCHAR(255) NULL,
    neighborhood VARCHAR(255) NULL,
    consumptiontype VARCHAR(255) NULL,
    measureunit VARCHAR(255) NULL,
    corrected_kpivalue DECIMAL NULL,
    correction_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    recvtime TIMESTAMP NULL,
    fiwareservicepath TEXT NULL,
    entityid VARCHAR(64) NOT NULL,
    entitytype TEXT NULL,
    CONSTRAINT t_datos_cb_cia_inyeccion_agua_corregida_pkey PRIMARY KEY (entityid, calculationperiod, neighborhood)

);


COMMIT;
