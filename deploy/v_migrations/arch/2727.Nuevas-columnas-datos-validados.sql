-- Deploy postgis-vlci-vlci2:v_migrations/2727.Nuevas-columnas-datos-validados to pg

BEGIN;


DROP TABLE IF EXISTS  vlci2.t_datos_cb_trafico_kpi_validados_mes;
DROP TABLE IF EXISTS  vlci2.t_datos_cb_trafico_kpi_validados_anyo;

CREATE TABLE vlci2.t_datos_cb_trafico_kpi_validados_mes (
    recvtime timestamptz NULL, 
    fiwareservicepath text NULL,
    entityid text NULL,
    entitytype text NULL,
    originalEntityId text NOT NULL,
    originalEntityType text NULL,
    originalServicePath text NULL,
    calculationperiod text,
    diasemana text NULL,
    kpivalue numeric NOT NULL,
    tramo text NOT NULL,
    name text NULL,
    measureunitcas text NULL,
    measureunitval text NULL,
    PRIMARY KEY (originalEntityId, calculationperiod, tramo)
);

CREATE TABLE vlci2.t_datos_cb_trafico_kpi_validados_anyo (
    recvtime timestamptz NULL, 
    fiwareservicepath text NULL,
    entityid text NULL,
    entitytype text NULL,
    originalEntityId text NOT NULL,
    originalEntityType text NULL,
    originalServicePath text NULL,
    calculationperiod text,
    diasemana text NULL,
    kpivalue numeric NOT NULL,
    tramo text NOT NULL,
    name text NULL,
    measureunitcas text NULL,
    measureunitval text NULL,
    PRIMARY KEY (originalEntityId, calculationperiod, tramo)
);

COMMIT;
