-- Deploy postgis-vlci-vlci2:v_migrations/1541-modificacion-columna-nombre to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata RENAME COLUMN nombre TO "name";

COMMIT;
