-- Deploy vlci-postgis:v_migrations/67531.update_mapeo_codor_transparencia to pg

BEGIN;

UPDATE vlci2.t_d_map_cod_organico SET cod_organico_bdo='JH170000',aud_user_upd='Ivan 67531' WHERE cod_organico_bdo='JU170000' AND cod_organico_gaco='JG170000';

COMMIT;
