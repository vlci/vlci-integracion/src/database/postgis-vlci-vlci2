-- Deploy postgis-vlci-vlci2:v_migrations/46.Anyadir_columnas_unidad_medida to pg

BEGIN;

ALTER TABLE vlci2.t_f_text_kpi_trafico_y_bicis ADD measureunitcas varchar(100) NULL;
ALTER TABLE vlci2.t_f_text_kpi_trafico_y_bicis ADD measureunitval varchar(100) NULL;

ALTER TABLE vlci2.t_f_text_cb_wifi_kpi_conexiones ADD measureunitcas varchar(100) NULL;
ALTER TABLE vlci2.t_f_text_cb_wifi_kpi_conexiones ADD measureunitval varchar(100) NULL;

ALTER TABLE vlci2.t_datos_cb_emt_kpi ADD measureunitcas varchar(100) NULL;
ALTER TABLE vlci2.t_datos_cb_emt_kpi ADD measureunitval varchar(100) NULL;

ALTER TABLE vlci2.t_f_text_kpi_precipitaciones_anuales ADD measurelandunit varchar(100) NULL;
ALTER TABLE vlci2.t_f_text_kpi_precipitaciones_diarias ADD measurelandunit varchar(100) NULL;
ALTER TABLE vlci2.t_f_text_kpi_precipitaciones_mensuales ADD measurelandunit varchar(100) NULL;
ALTER TABLE vlci2.t_f_text_kpi_temperatura_maxima_diaria ADD measurelandunit varchar(100) NULL;
ALTER TABLE vlci2.t_f_text_kpi_temperatura_media_diaria ADD measurelandunit varchar(100) NULL;
ALTER TABLE vlci2.t_f_text_kpi_temperatura_minima_diaria ADD measurelandunit varchar(100) NULL;

ALTER TABLE vlci2.t_f_text_cb_kpi_consumo_agua ADD measurelandunit varchar(100) NULL;

ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD no2type varchar(100) NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD o3type varchar(100) NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD pm10type varchar(100) NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD pm25type varchar(100) NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD so2type varchar(100) NULL;

ALTER TABLE vlci2.t_f_text_cb_kpi_residuos ADD measureunit varchar(100) NULL;

COMMIT;
