-- Deploy postgis-vlci-vlci2:v_migrations/430.alter_estadistica_barrio to pg

BEGIN;

ALTER TABLE vlci2.t_datos_etl_estadistica_valores_indicadores ALTER COLUMN barrio TYPE varchar(40) USING barrio::varchar;
ALTER TABLE vlci2.t_datos_etl_estadistica_valores_indicadores_tmp ALTER COLUMN barrio TYPE varchar(40) USING barrio::varchar;

COMMIT;
