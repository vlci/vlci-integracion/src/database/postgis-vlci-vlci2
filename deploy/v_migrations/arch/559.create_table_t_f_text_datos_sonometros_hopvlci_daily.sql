-- Deploy postgis-vlci-vlci2:v_migrations/559.create_table_t_f_text_datos_sonometros_hopvlci_daily to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_sonometros_hopvlci_daily (
	recvtime timestamptz NULL,
	fiwareservicepath text NULL,
	entityid varchar(64) NOT NULL,
	entitytype text NULL,
	dateobserved text NULL,
	"name" text NULL,
	laeq text NULL,
	laeq_d text NULL,
	laeq_e text NULL,
	laeq_n text NULL,
	laeq_den text NULL,
	error_status text NULL,
	"source" text NULL,
	"location" public.geometry NULL
);

COMMIT;
