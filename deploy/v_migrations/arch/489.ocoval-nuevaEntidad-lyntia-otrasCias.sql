-- Deploy postgis-vlci-vlci2:v_migrations/489.ocoval-nuevaEntidad-lyntia-otrasCias to pg

BEGIN;

INSERT INTO vlci2.vlci_ocoval_entidades(compania, entidad) VALUES('Compañías de Servicios', 'Lyntia');
INSERT INTO vlci2.vlci_ocoval_companyia(compania, entidad) VALUES('OTRAS CIAS', 'Lyntia');
INSERT INTO vlci2.vlci_ocoval_companyia(compania, entidad) VALUES('OTRAS CIAS', 'Digi');

COMMIT;
