-- Deploy postgis-vlci-vlci2:v_migrations/68425.migracion_agregados_create_table_t_f_raw_from_context_broker to pg

BEGIN;

CREATE TABLE vlci2.t_f_raw_from_context_broker  (
   recvTime VARCHAR(255),
   fiwareServicePath VARCHAR(255),
   entityId VARCHAR(255),
   entityType VARCHAR(255),
   name VARCHAR(255),
   description VARCHAR(255),
   measureUnit VARCHAR(255),
   calculationFrequency VARCHAR(255),
   kpiValue VARCHAR(255),
   calculationPeriod VARCHAR(255),
   neighborhoodId VARCHAR(255),
   updatedAt VARCHAR(255),
   location VARCHAR(255),
   internationalStandard VARCHAR(255),
   kpiNumeratorValue VARCHAR(255),
   kpiDenominatorValue VARCHAR(255),
   sliceAndDiceValue1 VARCHAR(255),
   sliceAndDiceValue2 VARCHAR(255),
   sliceAndDiceValue3 VARCHAR(255),
   sliceAndDiceValue4 VARCHAR(255)
  );
  
  comment on table vlci2.t_f_raw_from_context_broker is 'Tabla de la que lee el procedimiento de agregados una vez unificados todos los indicadores que llegan vía ContextBroker.';

COMMIT;
