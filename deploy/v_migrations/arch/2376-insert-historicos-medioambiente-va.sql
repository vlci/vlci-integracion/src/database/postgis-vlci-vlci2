-- Deploy postgis-vlci-vlci2:v_migrations/2376-insert-historicos-medioambiente-va to pg

BEGIN;

INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved_va
(recvtime, fiwareservicepath, entityid, entitytype, no2value, no2valueflag, pm10value, pm10valueflag, pm25value, pm25valueflag, dateobserved, refpointofinterest, no2type, o3type, pm10type, pm25type, so2type, novalue, novalueflag, notype, noxvalue, noxvalueflag, noxtype, o3value, o3valueflag, pm1value, pm1valueflag, pm1type, so2value, so2valueflag, calidad_ambiental, gis_id, address, operationalstatus, originalservicepath, originalentityid, originalentitytype)
select recvtime, '/' as fiwareservicepath, 't_datos_cb' as entityid, 'medioambiente_airqualityobserved_va' as entitytype, no2value, no2valueflag, pm10value, pm10valueflag, pm25value, pm25valueflag, dateobserved, refpointofinterest, no2type, o3type, pm10type, pm25type, so2type, novalue, novalueflag, notype, noxvalue, noxvalueflag, noxtype, o3value, o3valueflag, pm1value, pm1valueflag, pm1type, so2value, so2valueflag, calidad_ambiental, gis_id, address, operationalstatus, fiwareservicepath as originalservicepath, concat(entityid,'_va') as originalentityid, entitytype as originalentitytype 
from vlci2.t_datos_cb_medioambiente_airqualityobserved where dateobserved < '2020-03-02 00:00:00' 
order by dateobserved desc;

COMMIT;
