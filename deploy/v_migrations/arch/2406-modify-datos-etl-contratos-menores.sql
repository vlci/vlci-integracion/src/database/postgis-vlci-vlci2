-- Deploy postgis-vlci-vlci2:v_migrations/2406-modify-datos-etl-contratos-menores to pg

BEGIN;


--SE BORRAN ENTRADAS REPETIDAS
DELETE FROM vlci2.t_datos_etl_contratos_menores WHERE (num_expediente, objeto_contrato, adjudicatario) = ('E8RE05202300000900', 'ADQUISICIÓN LANYARDS PARA CREDENCIALES MASCLETÀ FALLAS 2023', 'ALBERTO BASTERRA RODRIGO') AND ctid NOT IN (
    SELECT MIN(ctid)
    FROM vlci2.t_datos_etl_contratos_menores
    WHERE (num_expediente, objeto_contrato, adjudicatario) = ('E8RE05202300000900', 'ADQUISICIÓN LANYARDS PARA CREDENCIALES MASCLETÀ FALLAS 2023', 'ALBERTO BASTERRA RODRIGO')
    GROUP BY (num_expediente, objeto_contrato, adjudicatario)
);

DELETE FROM vlci2.t_datos_etl_contratos_menores WHERE (num_expediente, objeto_contrato, adjudicatario) = ('E02310202300002500', 'ORGANITZACIÓ ESPORTIVA EXHIBICIÓ REGATA DE VELA LLATINA EL PALMAR', 'CLUB DE VELA LATINA VALENCIANA') AND ctid NOT IN (
    SELECT MIN(ctid)
    FROM vlci2.t_datos_etl_contratos_menores
    WHERE (num_expediente, objeto_contrato, adjudicatario) = ('E02310202300002500', 'ORGANITZACIÓ ESPORTIVA EXHIBICIÓ REGATA DE VELA LLATINA EL PALMAR', 'CLUB DE VELA LATINA VALENCIANA')
    GROUP BY (num_expediente, objeto_contrato, adjudicatario)
);

ALTER TABLE vlci2.t_datos_etl_contratos_menores 
ADD CONSTRAINT t_datos_etl_contratos_menores_pkey PRIMARY KEY (num_expediente, objeto_contrato, adjudicatario);

ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD COLUMN area_cod_org varchar(50);
ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD COLUMN area_desc_val varchar(500);
ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD COLUMN area_desc_corta_cas varchar(500);
ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD COLUMN area_desc_corta_val varchar(500);
ALTER TABLE vlci2.t_datos_etl_contratos_menores ADD COLUMN servicio_cod_org varchar(500);

ALTER TABLE vlci2.t_datos_etl_contratos_menores DROP COLUMN nif_adjudicatario;

ALTER TABLE vlci2.t_datos_etl_contratos_menores ALTER column centro_gastos drop not null;
ALTER TABLE vlci2.t_datos_etl_contratos_menores ALTER column aplicacion_presupuestaria drop not null;

CREATE TRIGGER t_datos_etl_contratos_menores_before_insert BEFORE
INSERT ON vlci2.t_datos_etl_contratos_menores FOR each ROW EXECUTE FUNCTION fn_aud_fec_ins();

CREATE TRIGGER t_datos_etl_contratos_menores_before_update BEFORE
UPDATE ON vlci2.t_datos_etl_contratos_menores FOR each ROW EXECUTE FUNCTION fn_aud_fec_upd();

COMMIT;