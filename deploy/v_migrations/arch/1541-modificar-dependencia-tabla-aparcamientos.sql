-- Deploy postgis-vlci-vlci2:v_migrations/1541-modificar-dependencia-tabla-aparcamientos to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata ALTER COLUMN nombre DROP NOT NULL;

COMMIT;
