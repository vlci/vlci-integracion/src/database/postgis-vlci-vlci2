-- Deploy postgis-vlci-vlci2:v_migrations/324.last_data_parkingspot to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_parkingspot_lastdata (
	recvtime varchar(100) NOT NULL,
	fiwareservicepath varchar(100) NOT NULL,
	entitytype varchar(100) NOT NULL,
	entityid varchar(100) NOT NULL,
	category varchar(100) NULL DEFAULT NULL::character varying,
	datemodified varchar(100) NULL DEFAULT NULL::character varying,
	description varchar(100) NULL DEFAULT NULL::character varying,
	"name" varchar(100) NULL DEFAULT NULL::character varying,
	operationstatus varchar(100) NULL DEFAULT NULL::character varying,
	status varchar(100) NULL DEFAULT NULL::character varying,
	CONSTRAINT trafico_any_parkingspot_lasdata_pk PRIMARY KEY (entityid)
);

COMMIT;
