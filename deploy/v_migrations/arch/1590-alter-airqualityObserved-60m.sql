-- Deploy postgis-vlci-vlci2:v_migrations/1590-alter-airqualityObserved-60m to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD calidad_ambiental varchar(100) NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD gis_id varchar(100) NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD address varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD operationalstatus varchar(100) NULL;

COMMIT;
