-- Deploy postgis-vlci-vlci2:v_migrations/2409-modify-contratos-menores to pg

BEGIN;

	CREATE UNIQUE index unique_contrato on vlci2.t_datos_etl_contratos_menores (num_contrato);
	ALTER TABLE vlci2.t_datos_etl_contratos_menores ALTER COLUMN fecha_carga SET DEFAULT CURRENT_DATE;

COMMIT;
