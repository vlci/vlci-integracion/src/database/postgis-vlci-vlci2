-- Deploy postgis-vlci-vlci2:v_migrations/1619.residuos-cargar-datos-historicos to pg
-- requires: r_code/functions/f.t_ref_residuos_datos_mensuales_historicos_before_update
-- requires: r_code/functions/f.t_ref_residuos_datos_mensuales_historicos_before_insert

BEGIN;

    CREATE TABLE vlci2.t_ref_residuos_datos_mensuales_historicos (
        fecha_dato text NOT NULL,
        zona_residuo text NOT NULL,
        tipo_residuo text NOT NULL,
        kgs_residuos int4 NOT NULL,
        aud_fec_ins timestamp NOT NULL,
        aud_user_ins varchar(45) NOT NULL,
        aud_fec_upd timestamp NOT NULL,
        aud_user_upd varchar(45) NOT NULL,
        CONSTRAINT idx_res_hist_primary PRIMARY KEY (fecha_dato, zona_residuo, tipo_residuo)
    );

COMMIT;
