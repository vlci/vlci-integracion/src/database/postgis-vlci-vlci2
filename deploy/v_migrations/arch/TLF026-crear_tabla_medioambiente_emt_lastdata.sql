-- Deploy postgis-vlci-vlci2:v_migrations/TLF026-crear_tabla_medioambiente_emt_lastdata to pg

BEGIN;

CREATE table if not exists vlci2.t_datos_cb_medioambiente_emt_airquality_lastdata (
	recvtime timestamp with time zone NOT NULL,
	fiwareservicepath text,
	entitytype text,
	entityid text NOT NULL,
	dateobserved timestamp with time zone not null,	
	exttemperature double precision,
	extrelativehumidity double precision,
	intrelativehumidity double precision,
	inttemperature double precision,
	noiselevel double precision,
	co double precision,
	no2 double precision,
	o3 double precision,
	pm25 double precision,
	maintenanceowner text,
	location public.geometry(point),
	project text,
	CONSTRAINT t_datos_cb_medioambiente_emt_airquality_lastdata_pkey PRIMARY KEY (entityid)
);

COMMIT;
