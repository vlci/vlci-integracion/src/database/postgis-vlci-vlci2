-- Deploy postgis-vlci-vlci2:v_migrations/2772-modify-units-tables to pg

BEGIN;

--Primero las áreas

DROP TABLE vlci2.t_d_area;

CREATE TABLE vlci2.t_d_area (
	codigo_organico varchar not NULL,
	anyo int2 NOT NULL,
	mes int2 NOT NULL,
	desc_corta_cas varchar NULL,
	desc_corta_val varchar NULL,
	desc_larga_cas varchar NULL,
	desc_larga_val varchar NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar NULL,
	CONSTRAINT t_d_area_pkey PRIMARY KEY (codigo_organico, mes, anyo)
);

create trigger t_d_area_before_insert before
insert
    on
    vlci2.t_d_area for each row execute function fn_aud_fec_ins();
create trigger t_d_area_before_update before
update
    on
    vlci2.t_d_area for each row execute function fn_aud_fec_upd();

-- Segundo las delegaciones


DROP TABLE vlci2.t_d_delegacion;

CREATE TABLE vlci2.t_d_delegacion (
	codigo_organico varchar NULL,
	anyo int2 NOT NULL,
	mes int2 NOT NULL,
	fk_area_code varchar NULL,
	desc_corta_cas varchar NULL,
	desc_corta_val varchar NULL,
	desc_larga_cas varchar NULL,
	desc_larga_val varchar NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar NULL,
	CONSTRAINT t_d_delegacion_pkey PRIMARY KEY (codigo_organico, mes, anyo),
	FOREIGN KEY (fk_area_code, anyo, mes) REFERENCES vlci2.t_d_area (codigo_organico, anyo, mes)
);

create trigger t_d_delegacion_before_insert before
insert
    on
    vlci2.t_d_delegacion for each row execute function fn_aud_fec_ins();
create trigger t_d_delegacion_before_update before
update
    on
    vlci2.t_d_delegacion for each row execute function fn_aud_fec_upd();

-- Por último los servicios

DROP TABLE vlci2.t_d_servicio;

CREATE TABLE vlci2.t_d_servicio (
	codigo_organico varchar NOT NULL,
	anyo int2 NOT NULL,
	mes int2 NOT NULL,
	fk_delegacion_code varchar NULL,
	fk_area_code varchar NULL,
	desc_corta_cas varchar NULL,
	desc_corta_val varchar NULL,
	desc_larga_cas varchar NULL,
	desc_larga_val varchar NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar NULL,
	CONSTRAINT t_d_servicio_pkey PRIMARY KEY (codigo_organico, mes, anyo),
	FOREIGN KEY (fk_area_code,anyo,mes) REFERENCES vlci2.t_d_area(codigo_organico,anyo,mes)
);

create trigger t_d_servicio_before_insert before
insert
    on
    vlci2.t_d_servicio for each row execute function fn_aud_fec_ins();
create trigger t_d_servicio_before_update before
update
    on
    vlci2.t_d_servicio for each row execute function fn_aud_fec_upd();

COMMIT;
