-- Deploy postgis-vlci-vlci2:v_migrations/1402-pmDiaria-gestion-fechas to pg

BEGIN;

INSERT INTO t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) 
VALUES((select max(etl_id)+1 from t_d_fecha_negocio_etls),'etl_cdmciutat_trafico_pm_diaria','DIARIA',0,'2022-01-01 00:00:00','OK','YYYY-MM-dd',NOW(),'Ivan 1402',NOW(),'Ivan 1402');

INSERT INTO t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) 
VALUES((select max(etl_id)+1 from t_d_fecha_negocio_etls),'etl_cdmciutat_bicicletas_pm_diaria','DIARIA',0,'2022-01-01 00:00:00','OK','YYYY-MM-dd',NOW(),'Ivan 1402',NOW(),'Ivan 1402');

COMMIT;
