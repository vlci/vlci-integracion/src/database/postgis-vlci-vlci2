-- Deploy postgis-vlci-vlci2:v_migrations/1930-ortografia-quejas-sugerencias-datos to pg

BEGIN;

UPDATE vlci2.t_datos_etl_quejas_sugerencias
	SET tema='Tramitación administrativa, tributos y sanciones'
	WHERE tema='Tramitacion administrativa, tributos y sanciones';

UPDATE vlci2.t_datos_etl_quejas_sugerencias
	SET tema='Vía pública reparación de deficiencias'
	WHERE tema='Via publica reparacion de deficiencias';

COMMIT;
