-- Deploy postgis-vlci-vlci2:v_migrations/1753.archivar-tablas-bdo to pg

BEGIN;

-----t_d_servicio_arch
ALTER TABLE vlci2.t_d_servicio
  RENAME TO t_d_servicio_arch;

-----t_d_servicio_arch------
ALTER TABLE vlci2.t_d_delegacion
  RENAME TO t_d_delegacion_arch;

-----t_d_area_arc-----
ALTER TABLE vlci2.t_d_area
  RENAME TO t_d_area_arch;

COMMIT;
