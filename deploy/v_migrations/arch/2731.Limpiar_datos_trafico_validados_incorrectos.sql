-- Deploy postgis-vlci-vlci2:v_migrations/2731.Limpiar_datos_trafico_validados_incorrectos to pg

BEGIN;

INSERT INTO t_datos_cb_trafico_kpi_validados_anyo
(recvtime, fiwareservicepath, entityid, entitytype, originalentityid, originalentitytype, originalservicepath, calculationperiod, diasemana, kpivalue, tramo, name, measureunitcas, measureunitval)
SELECT 
     NOW() AT TIME ZONE 'Europe/Madrid' as recvtime,
    '/' as fiwareservicepath,
    't_datos_cb_trafico' as entityid,
    'kpi_validados_anyo' as entitytype,
    CASE 
        WHEN entityid = 'Kpi-Valid-Accesos-Ciudad-Coches' THEN 'KpiValid-365-Accesos-Ciudad-Coches'
        WHEN entityid = 'Kpi-Valid-Accesos-Vias-Coches' THEN 'KpiValid-365-Accesos-Vias-Coches'
        WHEN entityid = 'Kpi-Valid-Trafico-Bicicletas-Accesos-Anillo' THEN 'KpiValid-365-Trafico-Bicicletas-Accesos-Anillo'
        WHEN entityid = 'Kpi-Valid-Trafico-Bicicletas-Anillo' THEN 'KpiValid-365-Trafico-Bicicletas-Anillo'
    END as originalentityid,
    entitytype as originalentitytype,
    fiwareservicepath as originalservicepath,
    calculationperiod,
    diasemana,
    CAST(kpivalue AS numeric) as kpivalue,
    tramo,
    name,
    measureunitcas,
    measureunitval
FROM vlci2.t_f_text_kpi_trafico_y_bicis
WHERE entityid in ('Kpi-Valid-Accesos-Ciudad-Coches','Kpi-Valid-Accesos-Vias-Coches', 'Kpi-Valid-Trafico-Bicicletas-Accesos-Anillo', 'Kpi-Valid-Trafico-Bicicletas-Anillo')
AND calculationperiod >= '2023-04-26' and calculationperiod <= '2023-05-28' ;

COMMIT;
