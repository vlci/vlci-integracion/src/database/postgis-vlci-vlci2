-- Deploy postgis-vlci-vlci2:v_migrations/1863-create-tabla-quejas-sugerencias-kpi to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_quejas_sugerencias_KPI (
	id_kpi text NOT NULL,
	calculation_period text NULL,
	value int4 NULL,
	variacion Numeric(24,2) NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(200) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(200) NULL,
	CONSTRAINT t_datos_etl_quejas_sugerencias_KPI_pkey PRIMARY KEY (id_kpi,calculation_period));

COMMIT;
