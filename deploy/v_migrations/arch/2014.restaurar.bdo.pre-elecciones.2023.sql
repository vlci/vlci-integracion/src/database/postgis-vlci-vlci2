-- Deploy postgis-vlci-vlci2:v_migrations/2014.restaurar.bdo.pre-elecciones.2023 to pg

BEGIN;

  -- AREAS
  truncate table t_d_area;
  insert into t_d_area (
    pk_area_id,
    desc_corta_cas,
    desc_corta_val,
    desc_larga_cas,
    desc_larga_val,
    aud_fec_ins,
    aud_user_ins,
    aud_fec_upd,
    aud_user_upd
  ) 
  select 
    pk_area_id,
    desc_corta_cas,
    desc_corta_val,
    desc_larga_cas,
    desc_larga_val,
    now(),
    '2014-xavi',
    now(),
    '2014-xavi'
  from t_d_area_arch;

  alter table vlci2.t_d_area_arch drop column if exists estado_url;
  alter table vlci2.t_d_area_arch drop column if exists de_color;
  alter table vlci2.t_d_area_arch drop column if exists icono_url;
  alter table vlci2.t_d_area_arch drop column if exists activo_mask;
  alter table vlci2.t_d_area_arch drop column if exists nu_indicadores;
  alter table vlci2.t_d_area_arch drop column if exists nu_rojo;
  alter table vlci2.t_d_area_arch drop column if exists nu_ambar;
  alter table vlci2.t_d_area_arch drop column if exists nu_verde;
  alter table vlci2.t_d_area_arch drop column if exists nu_gris;

  -- DELEGACIONES
  truncate table t_d_delegacion;
  insert into t_d_delegacion (
    pk_delegacion_id,
    fk_area_id,
    desc_corta_cas,
    desc_corta_val,
    desc_larga_cas,
    desc_larga_val,
    aud_fec_ins,
    aud_user_ins,
    aud_fec_upd,
    aud_user_upd
  ) 
  select 
    pk_delegacion_id,
    pk_area_id,
    desc_corta_cas,
    desc_corta_val,
    desc_larga_cas,
    desc_larga_val,
    now(),
    '2014-xavi',
    now(),
    '2014-xavi'
  from t_d_delegacion_arch;

  alter table vlci2.t_d_delegacion_arch drop column if exists estado_url;
  alter table vlci2.t_d_delegacion_arch drop column if exists de_color;
  alter table vlci2.t_d_delegacion_arch drop column if exists activo_mask;
  alter table vlci2.t_d_delegacion_arch drop column if exists nu_indicadores;
  alter table vlci2.t_d_delegacion_arch drop column if exists nu_rojo;
  alter table vlci2.t_d_delegacion_arch drop column if exists nu_ambar;
  alter table vlci2.t_d_delegacion_arch drop column if exists nu_verde;
  alter table vlci2.t_d_delegacion_arch drop column if exists nu_gris;

  alter table vlci2.t_d_delegacion_arch RENAME COLUMN pk_area_id TO fk_area_id;

  -- SERVICIOS
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = '001' where codigo_organico = 'A0020';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = '001' where codigo_organico = 'A0180';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = '001' where codigo_organico = 'A01G0';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = '001' where codigo_organico = 'A0770';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 290 where codigo_organico = 'A1780';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 291 where codigo_organico = 'A2000';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 293 where codigo_organico = 'A4000';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 294 where codigo_organico = 'A5000';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 260 where codigo_organico = 'A7790';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 235 where codigo_organico = 'AE1H0';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 235 where codigo_organico = 'AE420';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 235 where codigo_organico = 'AE460';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 235 where codigo_organico = 'AE470';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 235 where codigo_organico = 'AE480';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 235 where codigo_organico = 'AE500';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 235 where codigo_organico = 'AE540';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 235 where codigo_organico = 'AE860';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 235 where codigo_organico = 'AE960';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 202 where codigo_organico = 'AG530';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 249 where codigo_organico = 'AH004';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 249 where codigo_organico = 'AH008';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 249 where codigo_organico = 'AH640';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 204 where codigo_organico = 'CC090';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 204 where codigo_organico = 'CC100';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 204 where codigo_organico = 'CC1E0';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 204 where codigo_organico = 'CC400';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 205 where codigo_organico = 'CD110';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 238 where codigo_organico = 'CI080';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 238 where codigo_organico = 'CI1D0';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 244 where codigo_organico = 'CV003';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 241 where codigo_organico = 'CY510';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 210 where codigo_organico = 'DD670';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 211 where codigo_organico = 'DE140';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 217 where codigo_organico = 'FD310';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 221 where codigo_organico = 'FJ300';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 222 where codigo_organico = 'FK890';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 266 where codigo_organico = 'FO000';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 223 where codigo_organico = 'FP760';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 224 where codigo_organico = 'FU290';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 252 where codigo_organico = 'FV910';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 245 where codigo_organico = 'FZ920';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 225 where codigo_organico = 'GC320';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 225 where codigo_organico = 'GC330';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 225 where codigo_organico = 'GC340';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 225 where codigo_organico = 'GC360';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 225 where codigo_organico = 'GC380';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 225 where codigo_organico = 'GC570';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 225 where codigo_organico = 'GC620';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 226 where codigo_organico = 'GD660';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 227 where codigo_organico = 'GE350';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 229 where codigo_organico = 'GG230';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 237 where codigo_organico = 'IB520';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 258 where codigo_organico = 'IC1K0';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 297 where codigo_organico = 'IE970';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 236 where codigo_organico = 'IF650';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 234 where codigo_organico = 'IK740';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 206 where codigo_organico = 'JF690';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 296 where codigo_organico = 'JH170';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 208 where codigo_organico = 'JU130';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 212 where codigo_organico = 'KC150';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 259 where codigo_organico = 'KD610';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 216 where codigo_organico = 'KG720';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 219 where codigo_organico = 'KH200';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 219 where codigo_organico = 'KH800';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 263 where codigo_organico = 'KI590';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 265 where codigo_organico = 'KK550';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 253 where codigo_organico = 'LJ160';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 250 where codigo_organico = 'LN220';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 250 where codigo_organico = 'LN240';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 250 where codigo_organico = 'LN390';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 213 where codigo_organico = 'MD260';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 214 where codigo_organico = 'ME280';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 215 where codigo_organico = 'MF580';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 232 where codigo_organico = 'MJ700';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 267 where codigo_organico = 'MP250';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 267 where codigo_organico = 'MP630';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 267 where codigo_organico = 'MP730';
 update t_d_servicio set aud_fec_upd = now(), aud_user_upd = '2014-xavi', fk_delegacion_id = 295 where codigo_organico = 'P61F0';

  alter table vlci2.t_d_servicio_arch drop column if exists estado_url;
  alter table vlci2.t_d_servicio_arch drop column if exists de_color;
  alter table vlci2.t_d_servicio_arch drop column if exists activo_mask;
  alter table vlci2.t_d_servicio_arch drop column if exists nu_indicadores;
  alter table vlci2.t_d_servicio_arch drop column if exists nu_rojo;
  alter table vlci2.t_d_servicio_arch drop column if exists nu_ambar;
  alter table vlci2.t_d_servicio_arch drop column if exists nu_verde;
  alter table vlci2.t_d_servicio_arch drop column if exists nu_gris;

COMMIT;
