-- Deploy postgis-vlci-vlci2:v_migrations/2662-add-area-fk-t-d-servicio to pg

BEGIN;

ALTER TABLE vlci2.t_d_servicio ADD fk_area_id varchar(11) NULL;

COMMIT;
