-- Deploy postgis-vlci-vlci2:v_migrations/2374-eliminar-gf-datos-validados to pg

BEGIN;

delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('A%10m_vm%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('A%10m_va%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('A%60m_vm%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('A%60m_va%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('W%10m_vm%');
delete from vlci2.t_d_fecha_negocio_etls where etl_nombre like ('W%10m_va%');

COMMIT;
