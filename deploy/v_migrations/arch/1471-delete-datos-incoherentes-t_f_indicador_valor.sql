-- Deploy postgis-vlci-vlci2:v_migrations/1472-delete-datos-incoherentes-t_f_indicador_valor to pg

BEGIN;


delete from vlci2.t_f_indicador_valor tfiv
    where fk_indicador in (1122,1124,1130,1138,1143) and fk_periodicidad_muestreo  = 6 and to_date(dc_periodicidad_muestreo_valor , 'dd/mm/yyyy') < '01/01/2017';

COMMIT;

