-- Deploy postgis-vlci-vlci2:v_migrations/376.eliminacion-columnas-metadatos to pg

BEGIN;

    alter table vlci2.t_f_text_kpi_precipitaciones_diarias
        drop calculationperiod_md,
        drop kpivalue_md;
    
    alter table vlci2.t_f_text_kpi_precipitaciones_mensuales
        drop calculationperiod_md,
        drop kpivalue_md;
    
    alter table vlci2.t_f_text_kpi_precipitaciones_anuales
        drop calculationperiod_md,
        drop kpivalue_md;
    
    alter table vlci2.t_f_text_kpi_temperatura_media_diaria
        drop calculationperiod_md,
        drop kpivalue_md;
    
    alter table vlci2.t_f_text_kpi_temperatura_minima_diaria
        drop calculationperiod_md,
        drop kpivalue_md;
    
    alter table vlci2.t_f_text_kpi_temperatura_maxima_diaria
        drop calculationperiod_md,
        drop kpivalue_md;
    
    alter table vlci2.t_f_text_cb_weatherobserved 
        drop windspeedvalueflag_md,
        drop windspeedvalue_md,
        drop winddirectionvalueflag_md,
        drop winddirectionvalue_md,
        drop temperaturevalueflag_md,
        drop temperaturevalue_md,
        drop relativehumidityvalueflag_md,
        drop relativehumidityvalue_md,
        drop precipitationvalueflag_md,
        drop precipitationvalue_md,
        drop gis_id_md,
        drop dateobservedgmt0_md,
        drop dateobserved_md,
        drop atmosphericpressurevalueflag_md,
        drop atmosphericpressurevalue_md;

    alter table vlci2.t_datos_cb_medioambiente_poi
        drop bind_no_md,
        drop bind_no2_md,
        drop bind_nox_md,
        drop bind_o3_md,
        drop bind_pm1_md,
        drop bind_pm10_md,
        drop bind_pm25_md,
        drop bind_so2_md,
        drop bind_atmosphericpressure_md,
        drop bind_precipitation_md,
        drop bind_relativehumidity_md,
        drop bind_temperature_md,
        drop bind_winddirection_md,
        drop bind_windspeed_md,
        drop address_md,
        drop category_md,
        drop dataprovider_md,
        drop description_md,
        drop location_md,
        drop name_md,
        drop owner_md;
    
    alter table vlci2.t_datos_cb_medioambiente_airqualityobserved 
        drop no2value_md,
        drop no2valueflag_md,
        drop pm10value_md,
        drop pm10valueflag_md,
        drop pm25value_md,
        drop pm25valueflag_md,
        drop dateobserved_md,
        drop refpointofinterest_md;

    alter table vlci2.t_f_text_cb_kpi_consumo_agua 
        drop calculationperiod_md,
        drop kpivalue_md,
        drop name_md,
        drop neighborhood_md,
        drop consumptiontype_md;
    
    alter table vlci2.t_datos_cb_emt_kpi 
        drop calculationperiod_md,
        drop kpivalue_md,
        drop sliceanddice1_md,
        drop sliceanddicevalue1_md,
        drop diasemana_md;

    alter table vlci2.t_f_text_cb_kpi_residuos 
        drop calculationperiod_md,
        drop kpivalue_md,
        drop updatedat_md,
        drop sliceanddicevalue1_md,
        drop sliceanddicevalue2_md,
        drop numeratorvalue_md,
        drop denominatorvalue_md;

    alter table vlci2.t_f_text_cb_sonometros_ruzafa_daily 
        drop dateobserved_md,
        drop name_md,
        drop laeq_md,
        drop laeq_d_md,
        drop laeq_e_md,
        drop laeq_n_md,
        drop laeq_den_md,
        drop error_status_md,
        drop source_md;

    alter table vlci2.t_f_text_kpi_trafico_y_bicis 
        drop calculationperiod_md,
        drop diasemana_md,
        drop kpivalue_md,
        drop tramo_md,
        drop name_md;

    alter table vlci2.t_f_text_cb_wifi_kpi_conexiones 
        drop name_md,
        drop description_md,
        drop measureunit_md,
        drop calculationfrequency_md,
        drop kpivalue_md,
        drop calculationperiod_md,
        drop updatedat_md,
        drop internationalstandard_md,
        drop sliceanddicevalue1_md,
        drop sliceanddicevalue2_md,
        drop sliceanddicevalue3_md,
        drop sliceanddicevalue4_md,
        drop sliceanddice1_md,
        drop sliceanddice2_md,
        drop sliceanddice3_md,
        drop sliceanddice4_md,
        drop desiredtrend_md,
        drop kpitype_md,
        drop numerator_md,
        drop servicename_md;
    
    alter table vlci2.t_f_text_cb_wifi_kpi_urbo 
        drop calculationfrequency_md,
        drop category_md,
        drop dataprovider_md,
        drop dlnumberofnewcitizens_md,
        drop dlnumberofnewcitizenscalculationperiodfrom_md,
        drop dlnumberofnewcitizenssource_md,
        drop dlnumberofnewcitizensupdatedat_md,
        drop dlnumberofnewuser_md,
        drop dlnumberofnewuserscalculationperiodfrom_md,
        drop dlnumberofnewuserssource_md,
        drop dlnumberofnewusersupdatedat_md,
        drop dlnumberofnewworkers_md,
        drop dlnumberofnewworkerscalculationperiodfrom_md,
        drop dlnumberofnewworkerssource_md, 
        drop dlnumberofnewworkersupdatedat_md,
        drop dlnumberofneww4eu_md,
        drop dlnumberofneww4eucalculationperiodfrom_md,
        drop dlnumberofneww4eusource_md,
        drop dlnumberofneww4euupdatedat_md,
        drop name_md,
        drop organization_md,
        drop process_md, 
        drop source_md;
    
    alter table vlci2.t_datos_cb_parkingspot 
        drop category_md,
        drop datemodified_md,
        drop description_md,
        drop name_md,
        drop operationstatus_md,
        drop status_md;

COMMIT;