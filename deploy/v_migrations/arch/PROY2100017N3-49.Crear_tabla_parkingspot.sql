-- Deploy postgis-vlci-vlci2:v_migrations/PROY2100017N3-15.Crear_tabla_parkingspot to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_parkingspot (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(100) NOT NULL,
	entitytype varchar(100) NOT NULL,
	entityid varchar(100) NOT NULL,
	category varchar(100) NULL DEFAULT NULL::character varying,
	datemodified varchar(100) NULL DEFAULT NULL::character varying,
	description varchar(100) NULL DEFAULT NULL::character varying,
	"name" varchar(100) NULL DEFAULT NULL::character varying,
	operationstatus varchar(100) NULL DEFAULT NULL::character varying,
	status varchar(100) NULL DEFAULT NULL::character varying,
	category_md varchar(100) NULL DEFAULT NULL::character varying,
	datemodified_md varchar(100) NULL DEFAULT NULL::character varying,
	description_md varchar(100) NULL DEFAULT NULL::character varying,
	name_md varchar(100) NULL DEFAULT NULL::character varying,
	operationstatus_md varchar(100) NULL DEFAULT NULL::character varying,
	status_md varchar(100) NULL DEFAULT NULL::character varying,
	CONSTRAINT trafico_any_parkingspot_pkey_1 PRIMARY KEY (recvtime,fiwareservicepath,entitytype,entityid)
);

COMMIT;
