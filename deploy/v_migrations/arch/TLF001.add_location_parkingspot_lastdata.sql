-- Deploy postgis-vlci-vlci2:v_migrations/TLF001.add_location_parkingspot_lastdata to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_parkingspot_lastdata ADD "location" public.geometry(point) NULL DEFAULT NULL;

COMMIT;
