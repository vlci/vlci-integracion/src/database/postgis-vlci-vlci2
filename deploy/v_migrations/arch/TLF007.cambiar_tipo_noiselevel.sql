-- Deploy postgis-vlci-vlci2:v_migrations/TLF007.cambiar_tipo_noiselevel to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_noiselevelobserved_lastdata ALTER COLUMN laeq TYPE float4;

COMMIT;
