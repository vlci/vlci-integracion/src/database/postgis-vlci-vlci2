-- Deploy postgis-vlci-vlci2:v_migrations/TLF004.create_noiselevel_lastdata to pg

BEGIN;

CREATE TABLE if not exists vlci2.t_datos_cb_noiselevelobserved_lastdata (
	recvtime text NOT NULL,
	fiwareservicepath text,
	entitytype text,
	entityid text NOT NULL,
	dateobserved timestamp with time zone not null,
	address text,
	name text,
	project text,
	maintenanceowner text,
	operationalstatus text,
	location public.geometry(point),
	laeq integer,
	noiselevellaeq text,
	CONSTRAINT t_datos_cb_noiselevelobserved_lastdata_pkey PRIMARY KEY (entityid)
);

COMMIT;
