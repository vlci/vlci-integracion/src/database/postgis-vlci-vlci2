-- deploy/v_migrations/2868-nuevas-columnas-agua-corregido.sql

BEGIN;

ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida 
ADD COLUMN IF NOT EXISTS originalEntityId TEXT DEFAULT NULL,
ADD COLUMN IF NOT EXISTS originalEntityType TEXT DEFAULT NULL,
ADD COLUMN IF NOT EXISTS originalServicePath TEXT DEFAULT NULL;

COMMIT;
