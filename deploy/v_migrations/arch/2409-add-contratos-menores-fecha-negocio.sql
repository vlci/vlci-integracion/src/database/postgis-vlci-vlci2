-- Deploy postgis-vlci-vlci2:v_migrations/2409-add-contratos-menores-fecha-negocio to pg

BEGIN;

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id, etl_nombre, periodicidad, "offset", fen, fen_inicio, fen_fin, estado, formato_fen, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd) 
SELECT COALESCE(MAX(etl_id), 0) + 1, 'py_contratacion_contratos_menores', 'DIARIA', 1, '2024-02-13 00:00:00.000', NULL, NULL, 'OK', 'YYYY-MM-dd', NOW(), 'Julian-2409', NOW(), 'Julian-2409'
FROM vlci2.t_d_fecha_negocio_etls;

COMMIT;
