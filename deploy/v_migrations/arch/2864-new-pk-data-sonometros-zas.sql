-- Deploy postgis-vlci-vlci2:v_migrations/2864-new-pk-data-sonometros-zas to pg

BEGIN;

ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week DROP CONSTRAINT t_datos_etl_sonometros_zas_last_week_pkey;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ADD PRIMARY KEY (originalentityid, dateobserved);

COMMIT;
