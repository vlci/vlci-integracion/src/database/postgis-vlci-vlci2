-- Deploy postgis-vlci-vlci2:v_migrations/1430.anyadir-unidad-medida-o3 to pg

BEGIN;

update vlci2.t_datos_cb_medioambiente_airqualityobserved set o3type = 'µg/m3';

COMMIT;
