-- Deploy postgis-vlci-vlci2:v_migrations/48176.migracion_gis_trafico to pg

BEGIN;

-- Quitamos la visibilidad de dos de los indicadores que procesa la ETL para no verlos en micro
update t_d_indicador 
set is_visible = 'N'
where dc_identificador_indicador in ('IE.CIR.011', 'IE.CIR.012');

COMMIT;
