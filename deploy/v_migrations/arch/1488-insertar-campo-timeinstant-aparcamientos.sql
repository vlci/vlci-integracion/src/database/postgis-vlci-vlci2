-- Deploy postgis-vlci-vlci2:v_migrations/1488-insertar-campo-timeinstant-aparcamientos to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos ADD COLUMN TimeInstant timestamp NULL;
UPDATE vlci2.t_datos_cb_trafico_aparcamientos set TimeInstant = recvtime; 
ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos ALTER COLUMN TimeInstant SET NOT NULL;  
COMMIT;
