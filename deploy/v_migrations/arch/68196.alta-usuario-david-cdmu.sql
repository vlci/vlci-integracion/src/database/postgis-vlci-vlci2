-- Deploy vlci-postgis:v_migrations/68196.alta-usuario-david-cdmu to pg

BEGIN;

-- t_d_usuario_servicio
INSERT INTO vlci2.t_d_usuario_servicio
(pk_user_id, fk_servicio_id, res_area, res_delegacion, res_servicio, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES('UHEMUD', '608', 'N', 'N', 'S', now(), 'Alex-68195', NULL, NULL);

-- t_d_user_rol
INSERT INTO vlci2.t_d_user_rol
(pk_user, rol, cod_situacion, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES('UHEMUD', 'ESTANDAR', 'A', now(), 'Alex-68195', now(), 'Alex-68195');

COMMIT;
