-- Deploy postgis-vlci-vlci2:v_migrations/TLF037-nuevos_campos_offstreetparking_lastdata to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata 
    ADD COLUMN address text,
    ADD COLUMN project text,
    ADD COLUMN owner text,
    ADD COLUMN owneremail text,
    ADD COLUMN respocitecnicoemail text,
    ADD COLUMN timeinstant timestamp WITH time zone,
    ADD COLUMN location public.geometry(point);

COMMIT;
