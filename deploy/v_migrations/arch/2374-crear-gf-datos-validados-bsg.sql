-- Deploy postgis-vlci-vlci2:v_migrations/2302-crear-gf-datos-validados to pg
DO $$
DECLARE

    max_row_number INT;
BEGIN

max_row_number := (SELECT MAX(etl_id) FROM t_d_fecha_negocio_etls);

-- Medioambiente 10m

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,fen_inicio,fen_fin,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (max_row_number + 1,'A01_AVFRANCIA_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 2,'A02_BULEVARDSUD_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 3,'A03_MOLISOL_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 4,'A04_PISTASILLA_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 5,'A05_POLITECNIC_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 6,'A06_VIVERS_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 7,'A07_VALENCIACENTRE_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 8,'A08_DR_LLUCH_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 9,'A09_CABANYAL_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 10,'A10_OLIVERETA_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 11,'A11_PATRAIX_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302');

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,fen_inicio,fen_fin,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (max_row_number + 12,'A02_BULEVARDSUD_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 13,'A03_MOLISOL_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 14,'A04_PISTASILLA_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 15,'A05_POLITECNIC_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 16,'A06_VIVERS_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 17,'A01_AVFRANCIA_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 18,'A07_VALENCIACENTRE_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 19,'A08_DR_LLUCH_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 20,'A09_CABANYAL_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 21,'A10_OLIVERETA_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 22,'A11_PATRAIX_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302');

-- Medioambiente 60m

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,fen_inicio,fen_fin,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (max_row_number + 23,'A01_AVFRANCIA_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 24,'A02_BULEVARDSUD_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 25,'A03_MOLISOL_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 26,'A04_PISTASILLA_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 27,'A05_POLITECNIC_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 28,'A06_VIVERS_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 29,'A07_VALENCIACENTRE_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 30,'A08_DR_LLUCH_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 31,'A09_CABANYAL_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 32,'A10_OLIVERETA_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 33,'A11_PATRAIX_60m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302');

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,fen_inicio,fen_fin,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (max_row_number + 34,'A02_BULEVARDSUD_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 35,'A03_MOLISOL_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 36,'A04_PISTASILLA_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 37,'A05_POLITECNIC_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 38,'A06_VIVERS_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 39,'A01_AVFRANCIA_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 40,'A07_VALENCIACENTRE_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 41,'A08_DR_LLUCH_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 42,'A09_CABANYAL_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 43,'A10_OLIVERETA_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 44,'A11_PATRAIX_60m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302');

-- Medioambiente 24h

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,fen_inicio,fen_fin,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (max_row_number + 45,'A01_AVFRANCIA_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 46,'A02_BULEVARDSUD_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 47,'A03_MOLISOL_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 48,'A04_PISTASILLA_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 49,'A05_POLITECNIC_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 50,'A06_VIVERS_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 51,'A07_VALENCIACENTRE_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 52,'A08_DR_LLUCH_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 53,'A09_CABANYAL_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 54,'A10_OLIVERETA_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 55,'A11_PATRAIX_24h_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302');

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,fen_inicio,fen_fin,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (max_row_number + 56,'A02_BULEVARDSUD_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 57,'A03_MOLISOL_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 58,'A04_PISTASILLA_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 59,'A05_POLITECNIC_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 60,'A06_VIVERS_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 61,'A01_AVFRANCIA_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 62,'A07_VALENCIACENTRE_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 63,'A08_DR_LLUCH_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 64,'A09_CABANYAL_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 65,'A10_OLIVERETA_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 66,'A11_PATRAIX_24h_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302');

-- Clima 10m

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,fen_inicio,fen_fin,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (max_row_number + 67,'W02_NAZARET_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 68,'W01_AVFRANCIA_10m_vm','DIEZMINUTAL',4320,'2024-01-01 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302');

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,fen_inicio,fen_fin,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (max_row_number + 69,'W02_NAZARET_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302'),
	 (max_row_number + 70,'W01_AVFRANCIA_10m_va','DIEZMINUTAL',12960,'2020-03-02 00:00:00',NULL,NULL,'OK','YYYYMMddHH24MISS',now(),'Iván 2302',now(),'Iván 2302');

COMMIT;
END $$;

