-- Deploy postgis-vlci-vlci2:v_migrations/1519.create-table-widget-bicis to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata (
    recvtime timestamptz,
    fiwareservicepath varchar(100),
    entityid varchar(100),
    entitytype varchar(100),
    description varchar(200),
    descriptionCas varchar(200),
    dateObserved varchar(100),
    dateObservedFrom varchar(100),
    dateObservedTo varchar(100),
    intensity int4,
    hourlyIntensity int4,
    location public."geometry",
    name varchar(200),
    operationalStatus varchar(200),
	CONSTRAINT t_datos_cb_trafico_cuenta_bicis_lastdata_pkey PRIMARY KEY (entityid));

COMMIT;
