-- Deploy postgis-vlci-vlci2:v_migrations/830.insert-api-loader-trafico-ind-diarios to pg

BEGIN;

insert into vlci2.t_d_api_loader_campos_request (nombre_api, campo, valor) 
    values ('Geoportal_tramos_id_descripcion', 'where', '1=1'),
        ('Geoportal_tramos_id_descripcion', 'returnGeometry', 'false'),
        ('Geoportal_tramos_id_descripcion', 'outFields', 'idtramo,des_tramo'),
        ('Geoportal_tramos_id_descripcion', 'orderByFields', 'idtramo'),
        ('Geoportal_tramos_id_descripcion', 'f', 'pjson');

COMMIT;
