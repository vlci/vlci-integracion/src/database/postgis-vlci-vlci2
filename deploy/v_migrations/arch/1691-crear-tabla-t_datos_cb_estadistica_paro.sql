-- Deploy postgis-vlci-vlci2:v_migrations/1691-crear-tabla-t_datos_cb_estadistica_paro to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_estadistica_paro (
    entityid VARCHAR NOT NULL,
    entitytype VARCHAR NOT NULL,
    recvtime TIMESTAMP NOT NULL,
    fiwareservicepath VARCHAR NOT NULL,
    calculationPeriod VARCHAR,
    calculationPeriodDescription VARCHAR,
    calculationPeriodDescriptionCas VARCHAR,
    kpiValue INT,
    sliceAndDice1 VARCHAR,
    sliceAndDice1Cas VARCHAR,
    sliceAndDiceValue1 VARCHAR,
    sliceAndDiceValue1Cas VARCHAR,
    updatedAt TIMESTAMP,
    description VARCHAR,
    descriptionCas VARCHAR,
    name VARCHAR,
    nameCas VARCHAR,
    measureUnit VARCHAR,
    aud_user_ins varchar(45) NOT NULL,
	aud_fec_ins timestamp NOT NULL,
	aud_fec_upd timestamp NOT NULL,
	aud_user_upd varchar(45) NOT NULL,
	timeinstant timestamp NOT NULL,
    CONSTRAINT t_datos_cb_estadistica_paro_pkey PRIMARY KEY (recvtime, fiwareservicepath, entitytype, entityid)
);
create trigger t_datos_cb_estadistica_paro_before_insert before
insert
    on
    vlci2.t_datos_cb_estadistica_paro for each row execute function fn_aud_fec_ins();
create trigger t_datos_cb_estadistica_paro_update before
update
    on
    vlci2.t_datos_cb_estadistica_paro for each row execute function fn_aud_fec_upd();
COMMIT;
