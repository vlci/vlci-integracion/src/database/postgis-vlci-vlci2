-- Deploy postgis-vlci-vlci2:2475-ocoval-nuevas-entidades-junta-marítim-diputacíon to pg

BEGIN;

INSERT INTO vlci2.vlci_ocoval_entidades(compania, entidad) VALUES('Otros', 'Junta Municipal Marítim');
INSERT INTO vlci2.vlci_ocoval_entidades(compania, entidad) VALUES('Otros', 'Diputación de Valencia');

COMMIT;
