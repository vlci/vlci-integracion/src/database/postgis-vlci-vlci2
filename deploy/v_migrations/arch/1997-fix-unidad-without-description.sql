-- Deploy postgis-vlci-vlci2:v_migrations/1997-fix-unidad-without-description to pg

BEGIN;

---------------------- SERVICIO
ALTER TABLE vlci2.t_d_servicio 
ALTER COLUMN desc_corta_cas TYPE VARCHAR(120);

--------------------- DELEGACION
ALTER TABLE vlci2.t_d_delegacion  
ALTER COLUMN desc_corta_cas TYPE VARCHAR(120);

ALTER TABLE vlci2.t_d_delegacion 
ALTER COLUMN desc_corta_val TYPE VARCHAR(120);

----------------------- AREA
ALTER TABLE vlci2.t_d_area  
ALTER COLUMN desc_corta_cas TYPE VARCHAR(120);

ALTER TABLE vlci2.t_d_area 
ALTER COLUMN desc_corta_val TYPE VARCHAR(120);

---------------------- SERVICIO_ARCH
ALTER TABLE vlci2.t_d_servicio_arch 
ALTER COLUMN desc_corta_cas TYPE VARCHAR(120);

ALTER TABLE vlci2.t_d_servicio_arch 
ALTER COLUMN desc_corta_val TYPE VARCHAR(120);

--------------------- DELEGACION_ARCH
ALTER TABLE vlci2.t_d_delegacion_arch  
ALTER COLUMN desc_corta_cas TYPE VARCHAR(120);

ALTER TABLE vlci2.t_d_delegacion_arch  
ALTER COLUMN desc_corta_val TYPE VARCHAR(120);

----------------------- AREA_ARCH
ALTER TABLE vlci2.t_d_area_arch 
ALTER COLUMN desc_corta_cas TYPE VARCHAR(120);

ALTER TABLE vlci2.t_d_area_arch  
ALTER COLUMN desc_corta_val TYPE VARCHAR(120);

COMMIT;
