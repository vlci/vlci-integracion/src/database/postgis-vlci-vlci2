-- Deploy postgis-vlci-vlci2:v_migrations/1597.drop-column-hourlyintensity to pg

BEGIN;

DROP TABLE vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata;

CREATE TABLE vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata (
	recvtime timestamptz NULL,
	fiwareservicepath varchar(100) NULL,
	entityid varchar(100) NOT NULL,
	entitytype varchar(100) NULL,
	description varchar(200) NULL,
	descriptioncas varchar(200) NULL,
	dateobserved varchar(100) NULL,
	dateobservedfrom varchar(100) NULL,
	dateobservedto varchar(100) NULL,
	intensity int4 NULL,
	"location" public.geometry NULL,
	"name" varchar(200) NULL,
	operationalstatus varchar(200) NULL,
	CONSTRAINT t_datos_cb_trafico_cuenta_bicis_lastdata_pkey PRIMARY KEY (entityid)
);

COMMIT;
