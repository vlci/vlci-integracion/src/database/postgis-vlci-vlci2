-- Deploy postgis-vlci-vlci2:v_migrations/275.alta-medidas-teleasistencia to pg

BEGIN;

INSERT INTO vlci2.t_m_unidad_medida
VALUES
    ((select max(pk_unidad_medida) + 1 from vlci2.t_m_unidad_medida), 'Usuarios/año', 'Usuaris/any', NULL, NULL),
    ((select max(pk_unidad_medida) + 2 from vlci2.t_m_unidad_medida), 'Índice satisfación anual', 'Índex satisfació anual', NULL, NULL),
    ((select max(pk_unidad_medida) + 3 from vlci2.t_m_unidad_medida), 'Nº de reclamaciones/quejas', 'Nº de reclamacions/queixes', NULL, NULL),
    ((select max(pk_unidad_medida) + 4 from vlci2.t_m_unidad_medida), 'Nº llamadas seguimiento/Mes', 'Nº trucades seguiment/Mes', NULL, NULL);

COMMIT;
