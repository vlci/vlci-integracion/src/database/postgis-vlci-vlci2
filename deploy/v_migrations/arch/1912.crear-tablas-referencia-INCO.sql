-- Deploy postgis-vlci-vlci2:v_migrations/1912.crear-tablas-referencia-INCO to pg

BEGIN;


CREATE TABLE vlci2.t_ref_etl_inco_capitulos (
	id_capitulo smallint NOT NULL,
	descripcion varchar(200),
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(200) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(200) NULL,
    CONSTRAINT t_ref_etl_inco_capitulos_pkey PRIMARY KEY (id_capitulo)
);

CREATE TABLE vlci2.t_ref_etl_inco_conceptos (
	id_concepto numeric(5) NOT NULL,
	id_capitulo smallint NOT NULL,
	descripcion_larga varchar(200),
    descripcion_corta varchar(200),	 
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(200) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(200) NULL,
    CONSTRAINT t_ref_etl_inco_conceptos_pkey PRIMARY KEY (id_concepto)
);

ALTER TABLE vlci2.t_ref_etl_inco_conceptos
ADD CONSTRAINT fk_coneptos_capitulos
FOREIGN KEY (id_capitulo)
REFERENCES vlci2.t_ref_etl_inco_capitulos(id_capitulo);

COMMIT;
