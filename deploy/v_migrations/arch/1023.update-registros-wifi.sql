-- Deploy postgis-vlci-vlci2:v_migrations/1023.update-registros-wifi to pg

BEGIN;

UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo
SET dlnumberofnewworkerscalculationperiodfrom = '2022-09-07T00:00:00.000Z'
WHERE recvtime::text like '2022-09-08%';

UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo
SET dlnumberofnewworkerscalculationperiodfrom = '2022-09-08T00:00:00.000Z'
WHERE recvtime::text like '2022-09-09%';

UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo
SET dlnumberofnewworkerscalculationperiodfrom = '2022-09-09T00:00:00.000Z'
WHERE recvtime::text like '2022-09-10%';

UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo
SET dlnumberofnewworkerscalculationperiodfrom = '2022-09-11T00:00:00.000Z'
WHERE recvtime::text like '2022-09-12%';

COMMIT;
