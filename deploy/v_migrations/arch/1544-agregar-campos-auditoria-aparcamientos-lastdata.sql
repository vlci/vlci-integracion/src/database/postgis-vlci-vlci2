-- Deploy postgis-vlci-vlci2:v_migrations/1544-agregar-campos-auditoria-aparcamientos-lastdata to pg

BEGIN;


ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata
ADD COLUMN aud_user_ins varchar(45)  NULL,
ADD COLUMN aud_fec_upd timestamp  NULL,
ADD COLUMN aud_fec_ins timestamp  NULL,
ADD COLUMN aud_user_upd varchar(45)  NULL;

UPDATE vlci2.t_datos_cb_trafico_aparcamientos_lastdata 
SET 
    aud_fec_upd = current_timestamp, 
    aud_user_upd = 'CYGNUS', 
    aud_user_ins = 'CYGNUS', 
    aud_fec_ins = current_timestamp 
WHERE 
    aud_fec_upd IS NULL 
    OR aud_user_upd IS NULL 
    OR aud_user_ins IS NULL 
    OR aud_fec_ins IS NULL;


ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata 
ALTER COLUMN aud_user_ins SET NOT NULL,
ALTER COLUMN aud_fec_upd SET NOT NULL,
ALTER COLUMN aud_user_upd SET NOT NULL,
ALTER COLUMN aud_fec_ins SET NOT NULL;


COMMIT;
