-- Deploy postgis-vlci-vlci2:v_migrations/1822-gf-corregir-fecha-pmp-semestral to pg

BEGIN;

UPDATE vlci2.t_d_fecha_negocio_etls
	SET fen_fin='2023-07-01 00:00:00.000',fen_inicio='2023-01-01 00:00:00.000'
	WHERE etl_id=110;

COMMIT;
