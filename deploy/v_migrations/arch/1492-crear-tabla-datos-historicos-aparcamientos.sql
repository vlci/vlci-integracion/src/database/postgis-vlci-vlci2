-- Deploy postgis-vlci-vlci2:v_migrations/1492-crear-tabla-datos-historicos-aparcamientos to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_trafico_aparcamientos(
	entityid varchar(100) NOT NULL,
	fiwareservicepath varchar(100) NULL,
	entitytype varchar(100) NULL,
	recvtime timestamptz NULL,
	"name" varchar(255) NULL,
	totalspotnumber int4 NULL,
	availablespotnumber int4 NULL,
	CONSTRAINT t_datos_cb_trafico_aparcamientos_pkey PRIMARY KEY (entityid)
);

COMMIT;
