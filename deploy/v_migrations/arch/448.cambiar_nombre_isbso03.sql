-- Deploy postgis-vlci-vlci2:v_migrations/448.cambiar_nombre_isbso03 to pg

BEGIN;

update vlci2.t_d_indicador set 
de_nombre_indicador_cas='Nº de visitas totales realizadas en el Servicio de Teleasistencia',
de_nombre_indicador_val ='Nº de visites totals realitzades en el Servei de Teleassistència',
de_descripcion_indicador_cas ='Nombre de visites totals realitzades en el Servei de Teleassistència.',
de_descripcion_indicador_val ='Número de visitas totales realizadas en el Servicio de Teleasistencia.'
where dc_identificador_indicador ='IS.BSO.03';

COMMIT;
