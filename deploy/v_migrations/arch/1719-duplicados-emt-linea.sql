-- Deploy postgis-vlci-vlci2:v_migrations/1719-duplicados-emt-linea to pg

BEGIN;

UPDATE vlci2.t_datos_cb_emt_kpi
	SET sliceanddicevalue1=REPLACE(sliceanddicevalue1,'"','')
	WHERE sliceanddice1 = 'Ruta' 
	and sliceanddicevalue1 like('%"%')
	and calculationperiod >= '2015-01-01'
	and calculationperiod < '2018-01-01';

COMMIT;
