-- Deploy postgis-vlci-vlci2:v_migrations/2102-ocoval-nueva-entidad-reparadora-correos-buzones to pg

BEGIN;

INSERT INTO vlci2.vlci_ocoval_entidades (compania, entidad) VALUES ('Otros', 'Correos Buzones');

COMMIT;
