-- Deploy postgis-vlci-vlci2:v_migrations/1691-crear-tabla-estadistica-paro to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_estadistica_paro (
	entityid varchar NOT NULL,
	entitytype varchar NOT NULL,
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar NOT NULL,
	calculationperiod varchar NOT NULL,
	kpivalue numeric NOT NULL,
	sliceanddicevalue1 varchar NOT NULL,
	sliceanddicevalue1cas varchar NOT NULL,
    originalServicePath varchar(255) NOT NULL,
    originalEntityId varchar(255) NOT NULL,
    originalEntityType varchar(255) NOT NULL,
	aud_user_ins varchar(45) NOT NULL,
	aud_fec_ins timestamp NOT NULL,
	aud_fec_upd timestamp NOT NULL,
	aud_user_upd varchar(45) NOT NULL,
	CONSTRAINT t_datos_cb_estadistica_paro_pkey PRIMARY KEY (entityid,calculationperiod, sliceAndDiceValue1)
);

-- Table Triggers

create trigger t_datos_cb_estadistica_paro_before_insert before
insert
    on
    vlci2.t_datos_cb_estadistica_paro for each row execute function fn_aud_fec_ins();
create trigger t_datos_cb_estadistica_paro_update before
update
    on
    vlci2.t_datos_cb_estadistica_paro for each row execute function fn_aud_fec_upd();


COMMIT;
