-- Deploy postgis-vlci-vlci2:v_migrations/168.delete_t_d_fecha_negocio_etls to pg

BEGIN;

DELETE FROM vlci2.t_d_fecha_negocio_etls
WHERE etl_nombre = 'VLCI_ETL_OCI_IND_REPORT_WIFI_DATOS_SESION_CLIENTE_WIFI';

COMMIT;
