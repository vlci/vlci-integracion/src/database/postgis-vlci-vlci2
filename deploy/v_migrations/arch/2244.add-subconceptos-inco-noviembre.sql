-- Deploy postgis-vlci-vlci2:v_migrations/2244.add-subconceptos-inco-noviembre to pg

BEGIN;

INSERT INTO vlci2.t_ref_etl_inco_conceptos (id_concepto,id_capitulo,descripcion_larga,descripcion_corta,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (42096,4,NULL,'AGENTE LOCAL DE INNOVACIÓN',now(),'Ticket 2244',now(),'Ticket 2244'),
	 (45033,4,NULL,'CE PYTO INVESTIGACIÓN E',now(),'Ticket 2244',now(),'Ticket 2244'),
	 (45050,4,NULL,'GV-SERV.CAJERO AUTOMÁTICO',now(),'Ticket 2244',now(),'Ticket 2244'),
	 (45119,4,NULL,'ATMV CONVENI',now(),'Ticket 2244',now(),'Ticket 2244');

COMMIT;
