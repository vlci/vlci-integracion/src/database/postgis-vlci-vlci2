-- Deploy postgis-vlci-vlci2:v_migrations/542.migracion_sonometros to pg

BEGIN;

CREATE TABLE vlci2.aux_t_f_text_datos_sonometros_daily(
	sonometro_direccion text NULL,
	fecha text NULL,
	dia_semana text NULL,
	valor_manana text NULL,
	valor_tarde text NULL,
	valor_noche text NULL
);

INSERT INTO vlci2.aux_t_f_text_datos_sonometros_daily
SELECT *
FROM vlci2.t_f_text_datos_sonometros_daily;

UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET sonometro_direccion=rtrim(sonometro_direccion) WHERE sonometro_direccion like '% ';

ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD recvtime timestamptz;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD fiwareservicepath text;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD entityid varchar(64);
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD entitytype text;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD dateobserved text;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD "name" text;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD laeq text;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD laeq_d text;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD laeq_e text;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD laeq_n text;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD laeq_den text;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD error_status text;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily ADD "source" text;

UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248682-daily' , name='Sensor de ruido del barrio de ruzafa. Cuba 3' WHERE sonometro_direccion='S1-Cuba 3';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248682-daily' , name='Sensor de ruido del barrio de ruzafa. Sueca 2' WHERE sonometro_direccion='S2-Sueca 2';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248655-daily' , name='Sensor de ruido del barrio de ruzafa. Cadiz 3' WHERE sonometro_direccion='S3-Cadiz 3';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248652-daily' , name='Sensor de ruido del barrio de ruzafa. Sueca Esq. Denia' WHERE sonometro_direccion='S4-Sueca Esq. Denia';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248671-daily' , name='Sensor de ruido del barrio de ruzafa. Cadiz 16' WHERE sonometro_direccion='S5-Cadiz 16';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248661-daily' , name='Sensor de ruido del barrio de ruzafa. General Prim Chaflan Donoso Cortes' WHERE sonometro_direccion='S6-General Prim Chaflan Donoso Cortes';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248669-daily' , name='Sensor de ruido del barrio de ruzafa. Dr.Serrano 21' WHERE sonometro_direccion='S7-Dr.Serrano 21';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248672-daily' , name='Sensor de ruido del barrio de ruzafa. PuertoRico 21' WHERE sonometro_direccion='S8-PuertoRico 21';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248680-daily' , name='Sensor de ruido del barrio de ruzafa. Sueca 32' WHERE sonometro_direccion='S9-Sueca 32'; 
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248678-daily' , name='Sensor de ruido del barrio de ruzafa. Carles Cervera 34' WHERE sonometro_direccion='S10-Carles Cervera 34';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248684-daily' , name='Sensor de ruido del barrio de ruzafa. Sueca 61' WHERE sonometro_direccion='S11-Sueca 61';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248670-daily' , name='Sensor de ruido del barrio de ruzafa. Carles Cervera, Chaflan Reina Dona Maria' WHERE sonometro_direccion='S12-Carles Cervera, Chaflan Reina Dona Maria';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248679-daily' , name='Sensor de ruido del barrio de ruzafa. Matias Perello Esq. Doctor Sumsi' WHERE sonometro_direccion='S13-Matias Perello Esq. Doctor Sumsi';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248677-daily' , name='Sensor de ruido del barrio de ruzafa. Vivons Chaflan Cadiz' WHERE sonometro_direccion='S14-Vivons Chaflan Cadiz';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T248676-daily' , name='Sensor de ruido del barrio de ruzafa. Salvador Abril Chaflan Maestro Jose Serrano' WHERE sonometro_direccion='S15-Salvador Abril Chaflan Maestro Jose Serrano';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entityid='T251234-daily' , name='Sensor de ruido del barrio de ruzafa. Cura Femenía 14' WHERE sonometro_direccion='S16-Cura Femenía 14';

UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET laeq_d = valor_manana;
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET laeq_e = valor_tarde;
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET laeq_n = valor_noche;
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET recvtime = (select now());
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET fiwareservicepath = '/sonometros';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET entitytype = 'NoiseLevelObserved';
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET dateobserved = to_date(fecha, 'yyyyMMdd'::text);
UPDATE vlci2.aux_t_f_text_datos_sonometros_daily SET source = 'CESVA';

ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily DROP COLUMN sonometro_direccion;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily DROP COLUMN fecha;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily DROP column dia_semana;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily DROP column valor_manana;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily DROP column valor_tarde;
ALTER TABLE vlci2.aux_t_f_text_datos_sonometros_daily DROP column valor_noche;

INSERT INTO vlci2.t_f_text_cb_sonometros_ruzafa_daily
SELECT *
FROM vlci2.aux_t_f_text_datos_sonometros_daily;


DROP TABLE vlci2.aux_t_f_text_datos_sonometros_daily;
DROP TABLE vlci2.t_f_text_datos_sonometros_daily;

COMMIT;