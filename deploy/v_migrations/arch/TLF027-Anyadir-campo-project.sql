-- Deploy postgis-vlci-vlci2:v_migrations/TLF027-Anyadir-campo-project to pg
BEGIN;

ALTER TABLE
    vlci2.t_datos_cb_alumbrado_device_lastdata
ADD
    project text NULL;

COMMIT;