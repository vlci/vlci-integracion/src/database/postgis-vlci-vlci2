-- Deploy postgis-vlci-vlci2:v_migrations/1753.crear-tablas-bdo to pg

BEGIN;


----- t_d_servicio -----
CREATE TABLE vlci2.t_d_servicio (
	fk_delegacion_id varchar(11) NULL,
	codigo_organico varchar(20),
	desc_corta_cas varchar(100) NULL,
	desc_larga_cas varchar(200) NULL,
	aud_fec_ins timestamp,
	aud_user_ins varchar(45),
	aud_fec_upd timestamp,
	aud_user_upd varchar(45)
);



----- t_d_delegacion -----
CREATE TABLE vlci2.t_d_delegacion (
	pk_delegacion_id varchar(11),
	fk_area_id varchar(11) NULL,
	desc_corta_cas varchar(100) NULL,
	desc_corta_val varchar(100) NULL,
	desc_larga_cas varchar(200) NULL,
	desc_larga_val varchar(200) NULL,
	aud_fec_ins timestamp,
	aud_user_ins varchar(45),
	aud_fec_upd timestamp,
	aud_user_upd varchar(45)
);


----- t_d_area -----
CREATE TABLE vlci2.t_d_area (
	pk_area_id varchar(11),
	desc_corta_cas varchar(100) NULL,
	desc_corta_val varchar(100) NULL,
	desc_larga_cas varchar(200) NULL,
	desc_larga_val varchar(200) NULL,
	aud_fec_ins timestamp,
	aud_user_ins varchar(45),
	aud_fec_upd timestamp,
	aud_user_upd varchar(45)
);



COMMIT;
