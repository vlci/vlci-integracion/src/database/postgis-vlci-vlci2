-- Deploy postgis-vlci-vlci2:v_migrations/1743-agregar-campos-umbrales-gauge-bicis to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata
ADD COLUMN lowerthresholdrt numeric,
ADD COLUMN middlethresholdrt numeric,
ADD COLUMN upperthresholdrt numeric;

COMMIT;
