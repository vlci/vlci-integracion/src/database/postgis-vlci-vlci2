-- Deploy postgis-vlci-vlci2:v_migrations/update-distrito-quejas to pg

BEGIN;

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='01-Ciutat Vella'
where distrito_localizacion='Ciutat Vella';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='02-L''Eixample'
where distrito_localizacion='L''Eixample';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='03-Extramurs'
where distrito_localizacion='Extramurs';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='04-Campanar'
where distrito_localizacion='Campanar';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='05-La Saidia'
where distrito_localizacion='La Saidia';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='06-El Pla del Real'
where distrito_localizacion='El Pla del Real';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='07-L''Olivereta'
where distrito_localizacion='L''Olivereta';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='08-Patraix'
where distrito_localizacion='Patraix';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='09-Jesus'
where distrito_localizacion='Jesus';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='10-Quatre Carreres'
where distrito_localizacion='Quatre Carreres';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='11-Poblats Maritims'
where distrito_localizacion='Poblats Maritims';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='12-Camins al Grau'
where distrito_localizacion='Camins al Grau';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='13-Algiros'
where distrito_localizacion='Algiros';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='14-Benimaclet'
where distrito_localizacion='Benimaclet';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='15-Rascanya'
where distrito_localizacion='Rascanya';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='16-Benicalap'
where distrito_localizacion='Benicalap';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='17-Pobles del Nord'
where distrito_localizacion='Pobles del Nord';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='18-Pobles de l''Oest'
where distrito_localizacion='Pobles de l''Oest';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='19-Pobles del Sud'
where distrito_localizacion='Pobles del Sud';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='98-Fora de Valencia'
where distrito_localizacion='Fora de Valencia';

update vlci2.t_datos_etl_quejas_sugerencias set distrito_localizacion='99-No consta'
where distrito_localizacion='No consta';

COMMIT;
