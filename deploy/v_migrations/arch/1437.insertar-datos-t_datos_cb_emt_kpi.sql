-- Deploy postgis-vlci-vlci2:v_migrations/1437.insertar-datos-t_datos_cb_emt_kpi to pg

BEGIN;
--1ª Agrupamos por linea y fecha y sumamos viajeros estas son las filas de 'Ruta' de la query original
insert into vlci2.t_datos_cb_emt_kpi(recvtime, fiwareservicepath, entitytype, entityid, calculationperiod, kpivalue, sliceanddice1, sliceanddicevalue1, diasemana, measureunitcas, measureunitval)
	select now()+(row_number() over() * interval '0.001 second') as recvtime , '/emt' as fiwareservicepath, 'KeyPerformanceIndicator' as entitytype, 'mob006-04' as entityid,
	fecha::date as calculationperiod, sum(usuarios) as usuarios, 'Ruta'as sliceanddice1, linea as linea, dia_semana as diaSemana, 'Viajes' as measureunitcas, 'Viatges' as measureunitval from cdmt.t_agg_emt_usuarios
	group by linea, fecha, dia_semana;

COMMIT;

BEGIN;
--2º Agrupamos por titulo y fecha y sumamos viajeros estas son las filas de 'Titulo' de la query original
insert into vlci2.t_datos_cb_emt_kpi(recvtime, fiwareservicepath, entitytype, entityid, calculationperiod, kpivalue, sliceanddice1, sliceanddicevalue1, diasemana, measureunitcas, measureunitval)
	select now() +(row_number() over() * interval '0.001 second') as recvtime , '/emt' as fiwareservicepath, 'KeyPerformanceIndicator' as entitytype, 'mob006-01' as entityid,
	fecha::date as calculationperiod, sum(usuarios) as kpivalue, 'Titulo' as sliceanddice1, tipo_titulo as sliceanddicevalue1, dia_semana as diasemana, 'Viajes' as measureunitcas, 'Viatges' as measureunitval from cdmt.t_agg_emt_usuarios
	group by tipo_titulo, fecha, dia_semana;
COMMIT;