-- Deploy postgis-vlci-vlci2:v_migrations/2112-ocoval-nueva-entidad-reparadora-fibra-valencia to pg

BEGIN;

INSERT INTO vlci2.vlci_ocoval_entidades(compania, entidad) VALUES('Compañías de Servicios', 'Fibra Valencia');

COMMIT;
