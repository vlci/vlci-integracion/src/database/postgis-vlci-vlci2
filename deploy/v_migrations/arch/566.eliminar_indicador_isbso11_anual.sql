-- Deploy postgis-vlci-vlci2:v_migrations/566.eliminar_indicador_isbso11_anual to pg

BEGIN;

DELETE FROM vlci2.t_d_indicador WHERE dc_identificador_indicador='IS.BSO.11' and fk_periodicidad_muestreo=2 and is_agregado='S';

COMMIT;
