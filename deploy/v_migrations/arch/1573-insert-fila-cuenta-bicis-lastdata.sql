-- Deploy postgis-vlci-vlci2:v_migrations/1573-insert-fila-cuenta-bicis-lastdata to pg

BEGIN;

INSERT INTO vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata
(recvtime, fiwareservicepath, entityid, entitytype, description, descriptioncas, dateobserved, dateobservedfrom, dateobservedto, intensity, hourlyintensity, "location", "name", operationalstatus)
VALUES('1999-02-09 01:00:21.978', '/trafico', 'intensidad_bicis_daily_f311', 'TrafficFlowObserved', 'COLOM', 'COLÓN', '1999-02-09T00:05:00.000Z', '1999-02-09T00:05:00.000Z', '1999-02-09T14:40:00.000Z', 7778, NULL, NULL, '720', 'ok');


COMMIT;
