-- Deploy postgis-vlci-vlci2:v_migrations/2078-create-t-datos-etl-sonometros-zas-last-week to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_sonometros_zas_last_week (
	recvtime timestamp NOT NULL,
	originalservicepath varchar(200) NULL,
	fiwareservicepath varchar(200) NULL,
	originalentityid varchar(200) NULL,
	entityid varchar(200) NOT NULL,
	originalentitytype  varchar(200) NULL,
 	entitytype varchar(200) NULL,
	"name" varchar(200) NULL,
	"location" public.geometry NULL,
	address varchar(200) NULL,
	project varchar(200) NULL,
	dateobserved timestamp NOT NULL,
	laeq numeric(5, 2) NULL,
	laeqreal numeric(5, 2) NULL,
	lafmax numeric(5, 2) NULL,
	lafmaxreal numeric(5, 2) NULL,
	lafmin numeric(5, 2) NULL,
	lafminreal numeric(5, 2) NULL,
	laleq numeric(5, 2) NULL,
	laleqreal numeric(5, 2) NULL,
	operationalstatus varchar(50) NULL,
	CONSTRAINT t_datos_etl_sonometros_zas_last_week_pkey PRIMARY KEY (entityid, dateobserved)
);

COMMIT;
