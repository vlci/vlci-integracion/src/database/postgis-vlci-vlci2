-- Deploy postgis-vlci-vlci2:v_migrations/2677.create-table-unidades-admin-areas to pg

BEGIN;

DROP TABLE vlci2.t_datos_etl_contratos_menores_bdo;

CREATE TABLE vlci2.t_datos_etl_unidades_administrativas_areas (
    unidad_administrativa varchar(255),
    unidad_cod_org varchar(255) NULL,
    area_cod_org varchar(255) NULL,
    area_desc_cas varchar(255) NULL,
    area_desc_val varchar(255) NULL,
    area_desc_corta_cas varchar(255) NULL,
    area_desc_corta_val varchar(255) NULL,
    fecha_negocio timestamp,
    aud_fec_ins timestamp NULL,
    aud_user_ins varchar(255) NULL,
    aud_fec_upd timestamp NULL,
    aud_user_upd varchar(255) NULL,
    PRIMARY KEY (unidad_administrativa, fecha_negocio)
);

COMMIT;
