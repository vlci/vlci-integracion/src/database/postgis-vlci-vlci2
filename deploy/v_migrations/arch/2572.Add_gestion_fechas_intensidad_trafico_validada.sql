-- Deploy postgis-vlci-vlci2:v_migrations/2572.Add_gestion_fechas_intensidad_trafico_validada to pg

BEGIN;

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id, etl_nombre, periodicidad, "offset", fen, fen_inicio, fen_fin, estado, formato_fen, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES (300, 'py_trafico_intensidad_validada_30', 'DIARIA', 30, now(), NULL, NULL, 'OK', 'YYYYMMDD', now(), 'TICKET 2572', now(), 'TICKET 2572' );

INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id, etl_nombre, periodicidad, "offset", fen, fen_inicio, fen_fin, estado, formato_fen, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES (301, 'py_trafico_intensidad_validada_365', 'DIARIA', 365, now(), NULL, NULL, 'OK', 'YYYYMMDD', now(), 'TICKET 2572', now(), 'TICKET 2572' );

COMMIT;
