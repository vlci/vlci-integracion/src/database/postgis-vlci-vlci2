-- Deploy postgis-vlci-vlci2:v_migrations/2727.Crear-tablas-datos-validados-trafico to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_trafico_kpi_validados_mes (
    recvtime timestamptz NULL, 
    fiwareservicepath text NULL,
    entityid text NOT NULL,
    entitytype text NULL,
    calculationperiod text,
    diasemana text NULL,
    kpivalue numeric NOT NULL,
    tramo text NOT NULL,
    name text NULL,
    measureunitcas text NULL,
    measureunitval text NULL,
    PRIMARY KEY (entityid, calculationperiod, tramo)
);

CREATE TABLE vlci2.t_datos_cb_trafico_kpi_validados_anyo (
    recvtime timestamptz NULL, 
    fiwareservicepath text NULL,
    entityid text NOT NULL,
    entitytype text NULL,
    calculationperiod text,
    diasemana text NULL,
    kpivalue numeric NOT NULL,
    tramo text NOT NULL,
    name text NULL,
    measureunitcas text NULL,
    measureunitval text NULL,
    PRIMARY KEY (entityid, calculationperiod, tramo)
);

COMMIT;
