-- Deploy postgis-vlci-vlci2:v_migrations/1593.medioambiente-contaminacion-estaciones-sin-actualizarse to pg

BEGIN;

-- binds: 204 - 2023-02-28 05:40:00.000
-- binds: 209 - 2023-02-28 05:40:00.000
-- binds: 206 - 2023-02-28 05:40:00.000
-- binds: 210 - 2023-02-28 05:40:00.000
-- binds: 211 - 2023-02-28 05:40:00.000
update t_d_fecha_negocio_etls set fen='2023-02-28 06:50:00.000', estado='OK', aud_user_upd='1503-xavi'
where etl_nombre = 'A03_MOLISOL_10m';

update t_d_fecha_negocio_etls set fen='2023-02-28 07:00:00.000', estado='OK', aud_user_upd='1503-xavi'
where etl_nombre = 'A03_MOLISOL_60m';

-- binds: 3418 - 2023-02-14 13:00:00
-- binds: 3416 - 2023-02-14 13:00:00
-- binds: 3417 - 2023-02-14 13:00:00
-- binds: 3419 - 2023-02-14 13:00:00
update t_d_fecha_negocio_etls set fen='2023-02-14 14:10:00.000', estado='OK', aud_user_upd='1503-xavi'
where etl_nombre = 'A10_OLIVERETA_10m';

update t_d_fecha_negocio_etls set fen='2023-02-14 15:00:00.000', estado='OK', aud_user_upd='1503-xavi'
where etl_nombre = 'A10_OLIVERETA_60m';

delete from t_d_fecha_negocio_etls 
where etl_nombre = 'VLCI_ETL_MEDIOAMBIENTE_AIRE_10';

delete from t_d_fecha_negocio_etls 
where etl_nombre = 'VLCI_ETL_MEDIOAMBIENTE_AIRE_60';

delete from t_d_fecha_negocio_etls 
where etl_nombre = 'VLCI_ETL_MEDIOAMBIENTE_METEOROLOGIA_10';

COMMIT;
