-- Deploy postgis-vlci-vlci2:v_migrations/TLF025-agregar_campo_airquality_lastdata to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_airqualityobserved_lastdata ADD inactive text;

COMMIT;
