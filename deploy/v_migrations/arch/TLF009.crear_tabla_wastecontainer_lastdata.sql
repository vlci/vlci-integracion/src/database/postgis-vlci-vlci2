-- Deploy postgis-vlci-vlci2:v_migrations/TLF009.crear_tabla_wastecontainer_lastdata to pg

BEGIN;

CREATE table if not exists vlci2.t_datos_cb_wastecontainer_lastdata (
	recvtime timestamp with time zone NOT NULL,
	fiwareservicepath text,
	entitytype text,
	entityid text NOT NULL,
	timeinstant timestamp with time zone,	
	fillinglevel float,
	maintenanceowner text,
	location public.geometry(point),
	temperature float,
	CONSTRAINT t_datos_cb_wastecontainer_lastdata_pkey PRIMARY KEY (entityid)
);

COMMIT;
