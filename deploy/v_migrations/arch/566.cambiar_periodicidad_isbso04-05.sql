-- Deploy postgis-vlci-vlci2:v_migrations/566.cambiar_periodicidad_isbso04-05 to pg

BEGIN;

UPDATE vlci2.t_d_indicador SET dc_identificador_indicador='IS.BSO.04a' WHERE dc_identificador_indicador='IS.BSO.04';
UPDATE vlci2.t_d_indicador SET dc_identificador_indicador='IS.BSO.05a' WHERE dc_identificador_indicador='IS.BSO.05';

COMMIT;
