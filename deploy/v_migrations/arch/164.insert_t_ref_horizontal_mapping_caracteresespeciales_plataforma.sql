-- Deploy postgis-vlci-vlci2:v_migrations/164.insert_t_ref_horizontal_mapping_caracteresespeciales_plataforma to pg

BEGIN;

INSERT INTO vlci2.t_ref_horizontal_mapping_caracteresespeciales_plataforma (caracter_plataforma, caracter_vlci) VALUES
('<', 'menor'),
('>', 'major'), 
('"', '´´'),
('''', '´'),
('=', 'igual'),
(';', '.'),
(')', ']'),
('(', '[');

COMMIT;
