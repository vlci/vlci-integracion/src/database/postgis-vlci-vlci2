-- Deploy postgis-vlci-vlci2:r_env_tables/tb.t_env_medioambiente_visualizacion_estaciones to pg

BEGIN;

CREATE TABLE vlci2.t_ref_medioambiente_visualizacion_estaciones (
	entityid varchar(64) NOT NULL,
	isvisiblevminut bool NOT NULL,
	aud_fec_ins timestamptz NULL,
	aud_user_ins varchar(100) NULL,
	aud_fec_upd timestamptz NULL,
	aud_user_upd varchar(100) NULL,
	CONSTRAINT t_ref_medioambiente_visualizacion_estaciones_pkey PRIMARY KEY (entityid)
);

INSERT INTO vlci2.t_ref_medioambiente_visualizacion_estaciones (entityid,isvisiblevminut,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 ('A01_AVFRANCIA',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590'),
	 ('A02_BULEVARDSUD',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590'),
	 ('A03_MOLISOL',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590'),
	 ('A04_PISTASILLA',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590'),
	 ('A05_POLITECNIC',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590'),
	 ('A06_VIVERS',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590'),
	 ('A07_VALENCIACENTRE',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590'),
	 ('A08_DR_LLUCH',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590'),
	 ('A09_CABANYAL',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590'),
	 ('A10_OLIVERETA',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590'),
	 ('A11_PATRAIX',true,'2023-04-13 09:10:28.168','Ivan-1590','2023-04-13 09:10:28.168','Ivan-1590');

COMMIT;
