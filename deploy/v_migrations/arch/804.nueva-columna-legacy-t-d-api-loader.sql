-- Deploy postgis-vlci-vlci2:v_migrations/804.nueva-columna-legacy-t-d-api-loader to pg
-- tarea PROY2100017N3-804

BEGIN;

ALTER TABLE vlci2.t_d_api_loader ADD legacy varchar(1);
ALTER TABLE vlci2.t_d_api_loader ADD CONSTRAINT check_value CHECK (legacy = 'S' OR legacy = 'N');

COMMIT;
