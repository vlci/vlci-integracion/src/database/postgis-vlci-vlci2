-- Deploy postgis-vlci-vlci2:v_migrations/1762-borrado-residuos-enero2023 to pg

BEGIN;

delete from vlci2.t_f_text_cb_kpi_residuos 
where to_date(calculationperiod,'dd-mm-yyyy') >= to_date('01-01-2023','dd-mm-yyyy')
and to_date(calculationperiod,'dd-mm-yyyy') <= to_date('31-01-2023','dd-mm-yyyy');

COMMIT;
