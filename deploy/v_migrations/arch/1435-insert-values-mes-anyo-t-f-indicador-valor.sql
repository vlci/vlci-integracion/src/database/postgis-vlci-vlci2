-- Deploy postgis-vlci-vlci2:v_migrations/1435-insert-values-mes-anyo-t-f-indicador-valor to pg
-- Se desea añadir valor a las columnas mes y anyo de la tabla t_f_indicador_valor para los siguientes indicadores 
-- con periodicidad diaria (FK 6) 
-- IS.TIC.45 con FK 1139 
-- IS.TIC.36 con FK 1130
-- IS.TIC.13 con FK 1111
-- IS.TIC.28 con FK 1122
-- IS.TIC.30 con FK 1124
-- IS.TIC.49 con FK 1143
-- Lo que hace el siguiente script es consultar la cantidad de entradas a modificar, iterar actualizando batches 
-- de 5000 filas cada vez y esper 2 segundos para dar tiempo a que las 500 filas sean actualizadas

DO $$
DECLARE
    chunk_size INT := 5000;
    iterator_index INT := 0;
    rows_to_update INT;
begin
	select count(*) into rows_to_update from vlci2.t_f_indicador_valor where fk_indicador in (1139, 1130, 1111, 1122, 1124, 1143) and (mes is null or anyo is null) and fk_periodicidad_muestreo = 6;
	--raise notice 'Valor de rows_to_update: %', rows_to_update;
	while iterator_index < rows_to_update LOOP
        with rows_to_update as (
	        select * from vlci2.t_f_indicador_valor
	        where fk_indicador in (1139, 1130, 1111, 1122, 1124, 1143)
	        	and (mes is null or anyo is null)
	        	and fk_periodicidad_muestreo = 6
	    	limit chunk_size)
    	update vlci2.t_f_indicador_valor 
    	set mes = extract(month from to_date(rows_to_update.dc_periodicidad_muestreo_valor, 'DD/MM/YYYY')),
    		anyo = extract(year from to_date(rows_to_update.dc_periodicidad_muestreo_valor, 'DD/MM/YYYY'))
		from rows_to_update
		where t_f_indicador_valor.fk_indicador = rows_to_update.fk_indicador
			and t_f_indicador_valor.fk_unidad_medida = rows_to_update.fk_unidad_medida
			and t_f_indicador_valor.fk_periodicidad_muestreo = rows_to_update.fk_periodicidad_muestreo
			and t_f_indicador_valor.dc_periodicidad_muestreo_valor = rows_to_update.dc_periodicidad_muestreo_valor
			and t_f_indicador_valor.dc_criterio_desagregacion1_valor = rows_to_update.dc_criterio_desagregacion1_valor;
		iterator_index := iterator_index + chunk_size;
		--raise notice 'Valor de iterator_index: %', iterator_index;
		perform pg_sleep(2);
    end loop;
END $$;
