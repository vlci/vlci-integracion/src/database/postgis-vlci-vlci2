-- Deploy postgis-vlci-vlci2:v_migrations/408.alter_table_wifi4eu to pg

BEGIN;

ALTER TABLE vlci2.t_f_text_cb_wifi_kpi_urbo ALTER COLUMN dlnumberofneww4eu TYPE int4 USING dlnumberofneww4eu::integer;

COMMIT;
