-- Deploy postgis-vlci-vlci2:v_migrations/1679-emt-TJoven-duplicado to pg

BEGIN;

UPDATE vlci2.t_datos_cb_emt_kpi
SET sliceanddicevalue1='T Joven  '
WHERE sliceanddice1='Titulo'
and sliceanddicevalue1='T Joven'
and calculationperiod >='2020-03-02'
and calculationperiod <='2020-03-24';

COMMIT;
