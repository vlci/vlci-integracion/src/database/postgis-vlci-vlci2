-- Deploy postgis-vlci-vlci2:v_migrations/1535-agregar-campo-areaServed to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_sonometros_hopvlci_daily ADD COLUMN areaServed VARCHAR(200);

update vlci2.t_datos_cb_sonometros_hopvlci_daily
	set areaServed = case  
		when entityid in('NoiseLevelObserved-HOPVLCi1-daily','NoiseLevelObserved-HOPVLCi2-daily','NoiseLevelObserved-HOPVLCi3-daily'
			,'NoiseLevelObserved-HOPVLCi4-daily','NoiseLevelObserved-HOPVLCi5-daily') then 'Cedre'
		when entityid in('NoiseLevelObserved-HOPVLCi6-daily','NoiseLevelObserved-HOPVLCi7-daily','NoiseLevelObserved-HOPVLCi8-daily'
			,'NoiseLevelObserved-HOPVLCi9-daily') then 'Hondures'
		when entityid in('NoiseLevelObserved-HOPVLCi10-daily','NoiseLevelObserved-HOPVLCi11-daily','NoiseLevelObserved-HOPVLCi12-daily'
			,'NoiseLevelObserved-HOPVLCi13-daily') then 'Benimaclet'
		when entityid in('NoiseLevelObserved-HOPVLCi14-daily','NoiseLevelObserved-HOPVLCi15-daily','NoiseLevelObserved-HOPVLCi16-daily'
			,'NoiseLevelObserved-HOPVLCi17-daily','NoiseLevelObserved-HOPVLCi18-daily','NoiseLevelObserved-HOPVLCi19-daily')
			then 'Aragó'
		when entityid in('NoiseLevelObserved-HOPVLCi20-daily','NoiseLevelObserved-HOPVLCi21-daily','NoiseLevelObserved-HOPVLCi22-daily')
			then 'Russafa' end;

COMMIT;

