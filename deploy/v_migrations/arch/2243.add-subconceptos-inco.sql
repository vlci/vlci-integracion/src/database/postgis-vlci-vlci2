-- Deploy postgis-vlci-vlci2:v_migrations/2243.add-subconceptos-inco to pg

BEGIN;

INSERT INTO vlci2.t_ref_etl_inco_conceptos (id_concepto,id_capitulo,descripcion_larga,descripcion_corta,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
	 (10001,1,NULL,'IRPF LIQ.EJERC.ANTER',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (21001,2,NULL,'IVA LIQ.EJERC.ANTER.',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (22010,2,NULL,'I.S/HIDROCARB.LIQ.EJ',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (22011,2,NULL,'I.S/P.INTERMED.LIQ.E',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (22012,2,NULL,'I.S/ALC.Y BEB.DER.LI',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (22013,2,NULL,'I.S/CERVEZA LIQ.EJER',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (22014,2,NULL,'I.S/LABOR.TABACO LIQ',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (36004,3,NULL,'VENTA PRODUCTOS VIVE',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (42001,4,NULL,'SUBV. TRANSPORTE COL',now(),'Ticket 2243',now(),'Ticket 2243'),
     (42190,4,NULL,'SEPIE PROGRAMA ERASM',now(),'Ticket 2243',now(),'Ticket 2243');
INSERT INTO vlci2.t_ref_etl_inco_conceptos (id_concepto,id_capitulo,descripcion_larga,descripcion_corta,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd) VALUES
     (45001,4,NULL,'PG-FCM MPIOS TURÍSTI',now(),'Ticket 2243',now(),'Ticket 2243'),
     (45005,4,NULL,'OF.MPAL.INFORM.CONSU',now(),'Ticket 2243',now(),'Ticket 2243'),
     (45150,4,NULL,'LABORA - PROGR. AVAL',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (46700,4,NULL,'CH MMTO PAISAJE DE L',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (49744,4,NULL,'CE CAPITALIDAD VERDE',now(),'Ticket 2243',now(),'Ticket 2243'),
     (49746,4,NULL,'CE PYTO IMPROVE',now(),'Ticket 2243',now(),'Ticket 2243'),
	 (75087,7,NULL,'NG-CE-MEJORA EFICIEN',now(),'Ticket 2243',now(),'Ticket 2243'),
     (75101,7,NULL,'TCV PROMOC TURÍSTICA',now(),'Ticket 2243',now(),'Ticket 2243'),
     (79722,7,NULL,'CE PYTO IMPROVE INVE',now(),'Ticket 2243',now(),'Ticket 2243');

	 COMMIT;
