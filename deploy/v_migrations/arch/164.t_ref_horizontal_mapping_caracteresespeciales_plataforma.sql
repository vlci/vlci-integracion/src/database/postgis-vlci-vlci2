-- Deploy postgis-vlci-vlci2:v_migrations/164.t_ref_horizontal_mapping_caracteresespeciales_plataforma to pg

BEGIN;

CREATE TABLE vlci2.t_ref_horizontal_mapping_caracteresespeciales_plataforma (
    caracter_plataforma  varchar(50),
    caracter_vlci  varchar(50)
);

COMMIT;
