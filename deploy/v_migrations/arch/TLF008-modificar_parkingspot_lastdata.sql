-- Deploy postgis-vlci-vlci2:v_migrations/TLF008-modificar_parkingspot_lastdata to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_parkingspot_lastdata ADD maintenanceowner text NULL;
ALTER TABLE vlci2.t_datos_cb_parkingspot_lastdata ADD project text NULL;

COMMIT;
