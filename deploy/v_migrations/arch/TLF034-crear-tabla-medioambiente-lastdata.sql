-- Deploy postgis-vlci-vlci2:v_migrations/TLF034-crear-tabla-medioambiente-lastdata to pg
BEGIN;

CREATE TABLE if not exists vlci2.t_datos_cb_medioambiente_weatherobserved_10m_lastdata (
    recvtime timestamp with time zone NOT NULL,
    fiwareservicepath text NULL,
    entitytype text NULL,
    entityid text NOT NULL,
    dateobserved timestamp with time zone NOT NULL,
    gisid text null,
    name text null,
    project text null,
    maintenanceowner text null,
    location public.geometry(point) null,
    altitude double precision null,
    insolationvalue double precision null,
    atmosphericpressurevalue double precision null,
    atmosphericnmarpressurevalue double precision null,
    precipitationvalue double precision null,
    relativehumidityvalue double precision null,
    steamtensionvalue double precision null,
    temperaturevalue double precision null,
    winddirectionvalue double precision null,
    windspeedvalue double precision null,
    constraint t_datos_cb_medioambiente_weatherobserved_lastdata_10m_pkey PRIMARY KEY (entityid)
);

COMMIT;