-- Deploy postgis-vlci-vlci2:v_migrations/TLF003.rename_column_parkingspot_lastdata to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_parkingspot_lastdata RENAME COLUMN operationstatus TO operationalstatus;

COMMIT;
