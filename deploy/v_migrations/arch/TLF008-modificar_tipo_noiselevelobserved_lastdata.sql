-- Deploy postgis-vlci-vlci2:v_migrations/TLF008-modificar_tipo_noiselevelobserved_lastdata to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_noiselevelobserved_lastdata ALTER COLUMN recvtime TYPE timestamp with time zone USING recvtime::timestamp with time zone;


COMMIT;
