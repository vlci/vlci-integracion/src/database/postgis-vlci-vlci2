-- Deploy postgis-vlci-vlci2:v_migrations/1606-insert-datos-febrero-bicis to pg

BEGIN;

INSERT INTO vlci2.t_f_text_kpi_trafico_y_bicis (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,diasemana,kpivalue,tramo,"name",measureunitcas,measureunitval) values
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-03','L','4624','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-03','L','2625','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-03','L','4257','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-03','L','1899','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-03','L','5617','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-04','M','4731','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-04','M','2648','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-04','M','4492','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-04','M','1942','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-04','M','6019','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-05','X','3076','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-05','X','1802','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-05','X','3156','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-05','X','1314','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-05','X','4159','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-06','J','4602','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-06','J','3077','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-06','J','4887','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-06','J','2019','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-06','J','5876','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-07','V','4494','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-07','V','2647','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-07','V','4296','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-07','V','1905','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-07','V','5559','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-08','S','3047','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-08','S','1775','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-08','S','2719','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-08','S','1312','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-08','S','3631','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-09','D','2325','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-09','D','1564','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-09','D','2187','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-09','D','1123','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-09','D','3044','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-10','L','4505','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-10','L','2822','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-10','L','4369','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-10','L','2109','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-10','L','5486','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-11','M','4936','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-11','M','2817','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-11','M','4751','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-11','M','2113','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-11','M','6005','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-12','X','4764','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-12','X','2623','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-12','X','4497','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-12','X','1946','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-12','X','5806','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-13','J','5106','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-13','J','2900','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-13','J','4589','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-13','J','2149','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-13','J','6260','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-14','V','5021','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-14','V','2874','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-14','V','4501','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-14','V','2247','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-14','V','5937','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-15','S','3269','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-15','S','2046','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-15','S','3023','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-15','S','1446','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-15','S','4073','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-16','D','2255','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-16','D','1613','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-16','D','2190','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-16','D','1203','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-16','D','3087','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-17','L','4730','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-17','L','2676','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-17','L','4546','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-17','L','1984','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-17','L','5594','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-18','M','3869','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-18','M','2166','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-18','M','3739','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-18','M','1622','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-18','M','4868','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-19','X','4639','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-19','X','2749','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-19','X','4371','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-19','X','2030','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-19','X','5785','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-20','J','4938','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-20','J','3013','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-20','J','4846','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-20','J','2213','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-20','J','6178','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-21','V','3944','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-21','V','2351','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-21','V','4296','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-21','V','1791','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-21','V','5041','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-22','S','3078','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-22','S','2003','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-22','S','2836','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-22','S','1511','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-22','S','3951','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-23','D','2406','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-23','D','1821','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-23','D','2500','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-23','D','1186','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-23','D','3201','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-24','L','4953','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-24','L','2801','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-24','L','4448','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-24','L','2077','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-24','L','5970','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-25','M','3512','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-25','M','2113','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-25','M','3655','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-25','M','1560','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-25','M','4267','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-26','X','4838','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-26','X','2824','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-26','X','4635','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-26','X','2131','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-26','X','5919','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-27','J','5150','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-27','J','2990','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-27','J','4927','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-27','J','2280','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-27','J','6400','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-28','V','4956','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-28','V','2875','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-28','V','4583','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-28','V','2243','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-28','V','6073','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-29','S','3142','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-29','S','1967','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-29','S','2946','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-29','S','1426','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-02-29','S','4051','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-03-01','D','2599','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-03-01','D','1560','Comte de Trénor – Pont de fusta','Comte de Trénor – Pont de fusta','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-03-01','D','2645','Guillem de Castro','Guillem de Castro','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-03-01','D','1263','Plaça Tetuan','Plaça Tetuan','Bicicletas','Bicicletes'),
(CURRENT_TIMESTAMP,'/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-03-01','D','3357','Xàtiva','Xàtiva','Bicicletas','Bicicletes');

COMMIT;
