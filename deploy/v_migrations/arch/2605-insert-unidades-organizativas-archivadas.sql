-- Deploy postgis-vlci-vlci2:v_migrations/2605-insert-unidades-organizativas-archivadas to pg

BEGIN;

insert into vlci2.t_d_area (pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
select pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, now(), '2605 - JULIAN', now(), '2605 - JULIAN'
from vlci2.t_d_area_arch 
where pk_area_id is not null
	and pk_area_id not in (select pk_area_id from vlci2.t_d_area);

insert into vlci2.t_d_delegacion (pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
select pk_delegacion_id, fk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, now(), '2605 - JULIAN', now(), '2605 - JULIAN'
from vlci2.t_d_delegacion_arch 
where pk_delegacion_id is not null
and pk_delegacion_id not in (select pk_delegacion_id from vlci2.t_d_delegacion);

insert into vlci2.t_d_servicio (fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
select fk_delegacion_id, codigo_organico, desc_corta_cas, desc_larga_cas, now(), '2605 - JULIAN', now(), '2605 - JULIAN'
from vlci2.t_d_servicio_arch
where codigo_organico  is not null
	and codigo_organico not in (select codigo_organico from vlci2.t_d_servicio);

COMMIT;
