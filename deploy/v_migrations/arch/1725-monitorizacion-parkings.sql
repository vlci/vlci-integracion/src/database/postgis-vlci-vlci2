-- Deploy postgis-vlci-vlci2:v_migrations/1725-monitorizacion-parkings to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata
ADD COLUMN operationalstatus varchar(100),
ADD COLUMN inactive varchar(100);

COMMIT;
