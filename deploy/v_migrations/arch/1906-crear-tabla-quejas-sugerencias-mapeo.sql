-- Deploy postgis-vlci-vlci2:v_migrations/1906-crear-tabla-quejas-sugerencias-mapeo to pg

BEGIN;

CREATE TABLE vlci2.t_ref_etl_quejas_sugerencias_mapeo_temas (
	tema_nuevo varchar(200) NOT NULL,
	tema_antiguo varchar(200) NOT NULL,
	subtema varchar(200) NOT NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(200) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(200) NULL,
	CONSTRAINT t_ref_etl_quejas_sugerencias_mapeo_temas_pkey PRIMARY KEY (tema_antiguo,subtema)
);

COMMIT;
