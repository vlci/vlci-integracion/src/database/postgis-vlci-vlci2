-- Deploy postgis-vlci-vlci2:v_migrations/2273.nueva-columna-fecha-inco-conceptos to pg

BEGIN;
DELETE FROM vlci2.t_ref_etl_inco_conceptos;

ALTER TABLE vlci2.t_ref_etl_inco_conceptos
DROP CONSTRAINT t_ref_etl_inco_conceptos_pkey;

ALTER TABLE vlci2.t_ref_etl_inco_conceptos
ADD COLUMN fecha DATE NOT NULL;

ALTER TABLE vlci2.t_ref_etl_inco_conceptos
ADD CONSTRAINT t_ref_etl_inco_conceptos_pkey PRIMARY KEY (id_concepto, fecha);

COMMIT;
