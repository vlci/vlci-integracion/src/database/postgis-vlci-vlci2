-- Deploy postgis-vlci-vlci2:v_migrations/768.update-codor-servicios to pg

BEGIN;


update vlci2.t_d_servicio set codigo_organico = 'A0020',aud_user_upd='Ivan 768',aud_fec_upd=now() where codigo_organico ='AC020' and desc_corta_cas ='SERV. DE PROTOCOLO';
update vlci2.t_d_servicio set codigo_organico = 'A0180',aud_user_upd='Ivan 768',aud_fec_upd=now() where codigo_organico ='AC180' and desc_corta_cas ='SR.GABINETE DE NORMALIZACION LINGUISTICA';
update vlci2.t_d_servicio set codigo_organico = 'A01G0',aud_user_upd='Ivan 768',aud_fec_upd=now() where codigo_organico ='AC1G0' and desc_corta_cas ='OF.DE DEL.DE PROTECCION DATOS PERSONALES';
update vlci2.t_d_servicio set codigo_organico = 'P61F0',aud_user_upd='Ivan 768',aud_fec_upd=now() where codigo_organico ='AE1F0' and desc_corta_cas ='SERV. SECRETARIA DEL JURADO TRIBUTARIO';
update vlci2.t_d_servicio set codigo_organico = 'CC090',aud_user_upd='Ivan 768',aud_fec_upd=now() where codigo_organico ='AL090' and desc_corta_cas ='SR.EVALUAC.SR.Y PERSONAS Y GEST.CALIDAD';
update vlci2.t_d_servicio set codigo_organico = 'CC400',aud_user_upd='Ivan 768',aud_fec_upd=now() where codigo_organico ='AL400' and desc_corta_cas ='SR. FORMACION Y GESTION DEL CONOCIMIENTO';


INSERT INTO vlci2.t_d_delegacion
(pk_delegacion_id,pk_area_id,
desc_corta_cas,desc_corta_val,desc_larga_cas,desc_larga_val,
estado_url,de_color,activo_mask,nu_indicadores,nu_rojo,nu_ambar,nu_verde,nu_gris,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd)
values
('202','1A',
'DELEGACIÓN DE RELACIONES CON LOS MEDIOS','DELEGACIÓ DE RELACIONS AMB ELS MITJANS','DELEGACIÓN DE RELACIONES CON LOS MEDIOS','DELEGACIÓ DE RELACIONS AMB ELS MITJANS',
'Images/VLCI/psincolorbl.png','AR_GRIS','S',0,0,0,0,0,now(),'Ivan 768',now(),'Ivan 768'),
('266','1A',
'DELEGACIÓN DE BIENESTAR ANIMAL','DELEGACIÓ DE BENESTAR ANIMAL','DELEGACIÓN DE BIENESTAR ANIMAL','DELEGACIÓ DE BENESTAR ANIMAL',
'Images/VLCI/psincolorbl.png','AR_GRIS','S',0,0,0,0,0,now(),'Ivan 768',now(),'Ivan 768'),
('001','1A',
'ALCALDÍA','ALCALDIA','ALCALDÍA','ALCALDIA',
'Images/VLCI/psincolorbl.png','AR_GRIS','S',0,0,0,0,0,now(),'Ivan 768',now(),'Ivan 768');

INSERT INTO vlci2.t_d_servicio
(pk_servicio_id, fk_delegacion_id, codigo_organico, 
desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, 
estado_url, de_color, activo_mask, nu_indicadores, nu_rojo, nu_ambar, nu_verde, nu_gris, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
values
('61J', '001', 'A01J0',
'GABINETE DE ALCALDIA', 'GABINET D´ALCALDIA', 'GABINETE DE ALCALDIA', 'GABINET D´ALCALDIA',
'Images/VLCI/psincolorbl.png', 'AR_GRIS', 'S', 0, 0, 0, 0, 0, now(), 'Ivan 768', now(), 'Ivan 768'),
('656', '001', 'A0560',
'SERV. DE PROYECTOS SINGULARES', 'SERV. DE PROJECTES SINGULARS', 'SERV. DE PROYECTOS SINGULARES', 'SERV. DE PROJECTES SINGULARS',
'Images/VLCI/psincolorbl.png', 'AR_GRIS', 'S', 0, 0, 0, 0, 0, now(), 'Ivan 768', now(), 'Ivan 768'),
('677', '001', 'A0770',
'SERV. DE COOR.JURIDICA Y PROC ELECTORALES', 'SERV. DE COOR.JURIDICA I PROC ELECTORALS', 'SERV. DE COOR.JURIDICA Y PROC ELECTORALES', 'SERV. DE COOR.JURIDICA I PROC ELECTORALS',
'Images/VLCI/psincolorbl.png', 'AR_GRIS', 'S', 0, 0, 0, 0, 0, now(), 'Ivan 768', now(), 'Ivan 768'),
('653', '202', 'AG530',
'GABINETE DE COMUNICACIONES', 'GABINET DE COMUNICACIONS', 'GABINETE DE COMUNICACIONES', 'GABINET DE COMUNICACIONS',
'Images/VLCI/psincolorbl.png', 'AR_GRIS', 'S', 0, 0, 0, 0, 0, now(), 'Ivan 768', now(), 'Ivan 768'),
('663', '267', 'MP630',
'SR.PALAU MUS.I CONGRES. VALÉNCIA Y BSMV', 'SR.PALAU MUS.I CONGRESS. VALÉNCIA Y BSMV', 'SR.PALAU MUS.I CONGRES. VALÉNCIA Y BSMV', 'SR.PALAU MUS.I CONGRESS. VALÉNCIA Y BSMV',
'Images/VLCI/psincolorbl.png', 'AR_GRIS', 'S', 0, 0, 0, 0, 0, now(), 'Ivan 768', now(), 'Ivan 768'),
('266', '266', 'FO000',
'DELEG.BIENESTAR ANIMAL', 'DELEGACIÓ DE BENESTAR ANIMAL', 'DELEG.BIENESTAR ANIMAL', 'DELEGACIÓ DE BENESTAR ANIMAL',
'Images/VLCI/psincolorbl.png', 'AR_GRIS', 'S', 0, 0, 0, 0, 0, now(), 'Ivan 768', now(), 'Ivan 768');


COMMIT;
