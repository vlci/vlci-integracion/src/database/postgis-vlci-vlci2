-- Deploy postgis-vlci-vlci2:v_migrations/1559.cmdciutat-residuos-corregir-dato-incorrecto to pg

BEGIN;

update t_f_text_cb_kpi_residuos set kpivalue = 63460  
where calculationperiod = '04-01-2023' and sliceanddicevalue1  = 'Zona 2' and sliceanddicevalue2 = 'Orgànica';

update t_f_text_cb_kpi_residuos set kpivalue = 132380
where calculationperiod = '04-01-2023' and sliceanddicevalue1  = 'Total' and sliceanddicevalue2 = 'Orgànica';

COMMIT;
