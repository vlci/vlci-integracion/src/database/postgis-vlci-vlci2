-- Deploy postgis-vlci-vlci2:v_migrations/2340.eliminar-tablas-lastdata to pg

BEGIN;

DROP TABLE IF EXISTS vlci2.t_datos_cb_airqualityobserved_lastdata_va;
DROP TABLE IF EXISTS vlci2.t_datos_cb_airqualityobserved_lastdata_vm;

COMMIT;
