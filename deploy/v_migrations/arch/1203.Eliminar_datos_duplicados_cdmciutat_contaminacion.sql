-- Deploy postgis-vlci-vlci2:v_migrations/1203.Eliminar_datos_duplicados_cdmciutat_contaminacion to pg

BEGIN;
-- DELETE FROM vlci2.vw_datos_cb_cdmciutat_contaminantes where exists (select *  
-- 	from vlci2.vw_datos_cb_cdmciutat_contaminantes c1 where (select count(*) from vlci2.vw_datos_cb_cdmciutat_contaminantes c2 where fecha= c1.fecha and estacion = c1.estacion) > 1 ); 
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved where entityid like '%AVFRANCIA%'and to_char(dateobserved , 'dd-MM-YYYY'::text) = '11-05-2020'and no2value = 4 and pm10value = 6;
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved where entityid like '%POLITECNIC%'and to_char(dateobserved , 'dd-MM-YYYY'::text) = '11-05-2020'and no2value = 4 and pm10value = 7;
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved where entityid like '%POLITECNIC%'and to_char(dateobserved , 'dd-MM-YYYY'::text) = '05-03-2020'and no2value = 8 and pm10value = 9;
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved where entityid like '%MOLISOL%'and to_char(dateobserved , 'dd-MM-YYYY'::text) = '11-05-2020'and no2value = 1 and pm10value = 5;
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved where entityid like '%MOLISOL%'and to_char(dateobserved , 'dd-MM-YYYY'::text) = '05-03-2020'and no2value = 5 and pm10value = 14;
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved where entityid like '%PISTASILLA%'and to_char(dateobserved , 'dd-MM-YYYY'::text) = '11-05-2020'and no2value = 6 and pm10value = 2;
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved where entityid like '%PISTASILLA%'and to_char(dateobserved , 'dd-MM-YYYY'::text) = '05-03-2020'and no2value = 14 and pm10value = 14;

COMMIT;
