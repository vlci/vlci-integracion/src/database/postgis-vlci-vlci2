-- Deploy postgis-vlci-vlci2:v_migrations/65231.insert_t_ref_estadistica_hardcoded_kpi to pg

BEGIN;

INSERT INTO vlci2.t_ref_estadistica_harcoded_kpi (columna, valor, aud_user_ins) VALUES
('type', 'KeyPerformanceIndicator', 'Andrea-65231'),
('error-status', 'N/A', 'Andrea-65231'),
('category', 'quantitative', 'Andrea-65231'),
('definitionOwner', 'VLCi', 'Andrea-65231'),
('denominator', 'N/A', 'Andrea-65231'),
('desiredTrend', 'N/A', 'Andrea-65231'),
('internationalStandard', 'N/A', 'Andrea-65231'),
('isKPIFranction', 'N/A', 'Andrea-65231'),
('kpiDenominatorValue', '0', 'Andrea-65231'),
('kpiNumeratorValue', '0', 'Andrea-65231'),
('kpiType', 'N/A', 'Andrea-65231'),
('location', '{}', 'Andrea-65231'),
('lowerThreshold', '0', 'Andrea-65231'),
('maintenanceOwner', 'OCI', 'Andrea-65231'),
('maintenanceOwnerEmail', 'integracionvlci@valencia.es', 'Andrea-65231'),
('numerator', 'N/A', 'Andrea-65231'),
('organization', '{  "name": "Ajuntament de València"}', 'Andrea-65231'),
('processName', 'etl_estadistica_ind_integracion_automatica', 'Andrea-65231'),
('product', 'etl_estadistica_ind_integracion_automatica', 'Andrea-65231'),
('provider', 'OF. DE ESTADISTICA', 'Andrea-65231'),
('references', 'OF. DE ESTADISTICA', 'Andrea-65231'),
('serviceId', '8247', 'Andrea-65231'),
('serviceName', 'OF. DE ESTADISTICA', 'Andrea-65231'),
('serviceOwner', 'Carmina Moya', 'Andrea-65231'),
('serviceOwnerEmail', 'cmoya@valencia.es', 'Andrea-65231'),
('smartDimension', 'Oficina Estadistica', 'Andrea-65231'),
('systemType', 'SFTP', 'Andrea-65231'),
('technology', 'CSV', 'Andrea-65231'),
('upperThreshold', '0', 'Andrea-65231');


COMMIT;
