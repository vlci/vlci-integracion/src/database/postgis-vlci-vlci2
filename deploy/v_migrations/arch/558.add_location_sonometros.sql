-- Deploy postgis-vlci-vlci2:v_migrations/558.add_location_sonometros to pg

BEGIN;

ALTER TABLE vlci2.t_f_text_cb_sonometros_ruzafa_daily ADD COLUMN location public."geometry";

COMMIT;
