-- Deploy postgis-vlci-vlci2:v_migrations/2406-create-datos-etl-contratos-menores-hist to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_contratos_menores_hist (
	fecha_carga date NOT NULL,
	num_expediente varchar(200) NOT NULL,
	objeto_contrato varchar(999) NOT NULL,
	fecha_adjudicacion date NOT NULL,
	centro_gastos varchar(50) NULL,
	unidad_administrativa varchar(50) NOT NULL,
	aplicacion_presupuestaria varchar(200) NULL,
    area_cod_org varchar(50) NULL,
	area varchar(500) NULL,
    area_desc_val varchar(500) NULL,
    area_desc_corta_cas varchar(500) NULL,
    area_desc_corta_val varchar(500) NULL,
    servicio_cod_org varchar(500) NULL,
	tipo_contrato varchar(200) NULL,
	importe_sin_iva numeric(13, 2) NULL,
	iva numeric(8, 2) NULL,
	organo_contratacion varchar(500) NULL,
	unidad_tramitadora varchar(500) NULL,
	numero_propuestas numeric(6) NULL,
	numero_ofertas numeric(6) NULL,
	adjudicatario varchar(500) NULL,
	estado varchar(50) NOT NULL,
	fecha_inicio_contrato date NULL,
	fecha_fin_contrato date NULL,
	duracion_contrato_meses numeric(6) NULL,
	fecha_factura date NULL,
	fecha_pago date NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(200) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(200) NULL,
    CONSTRAINT t_datos_etl_contratos_menores_hist_pkey PRIMARY KEY (fecha_carga, num_expediente, objeto_contrato, adjudicatario)
);

-- Table Triggers

create trigger t_datos_etl_contratos_menores_hist_before_insert before
insert
    on
    vlci2.t_datos_etl_contratos_menores_hist for each row execute function fn_aud_fec_ins();

create trigger t_datos_etl_contratos_menores_hist_before_update before
update
    on
    vlci2.t_datos_etl_contratos_menores_hist for each row execute function fn_aud_fec_upd();

COMMIT;
