-- Deploy postgis-vlci-vlci2:v_migrations/2636-modify-table-cia-inyeccion-agua-corregida to pg

BEGIN;

-- Renaming columns in the table
ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida
RENAME COLUMN correction_date TO correctiondate;

ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida
RENAME COLUMN corrected_kpivalue TO correctedkpivalue;

COMMIT;
