-- Deploy postgis-vlci-vlci2:v_migrations/t_datos_etl_estadistica_definicion_indicadores to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_estadistica_definicion_indicadores (
    nomindicador  varchar(20)  NOT NULL,
    ods  Int4,
    meta varchar(50),
    indicador Int4,
    descripcioncas text  NOT NULL,
    descripcionval text  NOT NULL,
    definicioncas text  NOT NULL,
    definicionval text  NOT NULL,
    calculocas text  NOT NULL,
    calculoval text  NOT NULL,
    unidadmedidacas text  NOT NULL,
    unidadmedidaval text  NOT NULL,
    fuenteinfocas text  NOT NULL,
    fuenteinfoval text  NOT NULL,
    periodicidadcas varchar(45)  NOT NULL,
    periodicidadval varchar(45)  NOT NULL,
    Observaciones1cas text,
    Observaciones1val text,
    Observaciones2cas text,
    Observaciones2val text,
    aud_fec_ins  timestamptz,
    aud_user_ins  VARCHAR(100),
    aud_fec_upd  timestamptz,
    aud_user_upd  VARCHAR(100),
    PRIMARY KEY (nomindicador)
);

COMMIT;
