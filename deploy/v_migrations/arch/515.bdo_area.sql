-- Deploy postgis-vlci-vlci2:v_migrations/515.bdo_area to pg

BEGIN;

-- Borramos áreas, delegaciones y servicios que están mal
delete from vlci2.t_datos_etl_agregados_area where pk_area_id = '210';
delete from vlci2.t_datos_etl_agregados_delegacion where pk_delegacion_id in ('1J','667');
delete from vlci2.t_datos_etl_agregados_servicio where fk_delegacion_id = '667';

-- Creamos la delegación NO CONSTA
INSERT INTO vlci2.t_datos_etl_agregados_delegacion
(pk_delegacion_id, pk_area_id, desc_corta_cas, desc_corta_val, desc_larga_cas, desc_larga_val, activo_mask)
VALUES('999', '1J', 'NO CONSTA', 'NO CONSTA', 'NO CONSTA', 'NO CONSTA', 'S');

-- Este servicio no cuelga de delegación y le ponemos una nueva NO CONSTA
update vlci2.t_datos_etl_agregados_servicio 
set fk_delegacion_id = '999'
where fk_delegacion_id = '1J';

COMMIT;
