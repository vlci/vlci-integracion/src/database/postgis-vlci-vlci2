-- Deploy postgis-vlci-vlci2:v_migrations/1787-crear-tabla-residuos-temporal to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_residuos_calculo_temporal  (
	dia_semana varchar(1) NOT NULL,
	dia_negocio timestamp NOT NULL,
	zona varchar(50) NOT NULL,
	kgs_envases int8 NULL,
	kgs_carton int8 NULL,
	kgs_rsu int8 NULL,
	kgs_organica int8 NULL,
	kgs_muebles int8 NULL,
	kgs_residencias int8 NULL,
	kgs_contenedores int8 NULL,
	aud_user_ins varchar(45) NULL,
	aud_fec_ins timestamp NULL,
	aud_user_upd varchar(45) NULL,
	aud_fec_upd timestamp NULL,
	kgs_vidrio int8 NULL,
	CONSTRAINT idx_85405_primary PRIMARY KEY (dia_negocio, zona)
);

COMMIT;
