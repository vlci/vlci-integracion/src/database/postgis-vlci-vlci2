-- Deploy postgis-vlci-vlci2:v_migrations/2676-eliminate-redundant-columns-contratos-menores to pg

BEGIN;

alter table vlci2.t_datos_etl_contratos_menores drop column area;
alter table vlci2.t_datos_etl_contratos_menores drop column area_cod_org;
alter table vlci2.t_datos_etl_contratos_menores drop column area_desc_val;
alter table vlci2.t_datos_etl_contratos_menores drop column area_desc_corta_cas;
alter table vlci2.t_datos_etl_contratos_menores drop column area_desc_corta_val;
alter table vlci2.t_datos_etl_contratos_menores drop column servicio_cod_org;

COMMIT;
