-- Deploy postgis-vlci-vlci2:v_migrations/2423-modify-emt-temp to pg

BEGIN;

ALTER TABLE vlci2.vlci_emt_tmp RENAME COLUMN ruta TO linea;

COMMIT;
