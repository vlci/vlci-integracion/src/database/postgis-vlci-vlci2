-- Deploy postgis-vlci-vlci2:v_migrations/68425.migracion_agregados_nuevas_tablas_auxiliares to pg

BEGIN;

CREATE TABLE vlci2.t_d_indicadores_destino_contextbroker ( 
dc_identificador_indicador VARCHAR(255),
fk_periodicidad_muestreo INTEGER
);

CREATE TABLE vlci2.t_x_indicadores_silencio ( 
dc_identificador_indicador VARCHAR(255)
);

COMMIT;
