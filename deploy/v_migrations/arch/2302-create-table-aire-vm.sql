-- Deploy postgis-vlci-vlci2:v_migrations/2302-create-table-aire-vm to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved_vm (
	recvtime timestamp NULL,
	fiwareservicepath text NULL,
	entityid varchar(64) NOT NULL,
	entitytype text NULL,
	no2value int4 NULL,
	no2valueflag text NULL,
	pm10value int4 NULL,
	pm10valueflag text NULL,
	pm25value int4 NULL,
	pm25valueflag text NULL,
	dateobserved timestamp NOT NULL,
	refpointofinterest text NULL,
	no2type varchar(100) NULL,
	o3type varchar(100) NULL,
	pm10type varchar(100) NULL,
	pm25type varchar(100) NULL,
	so2type varchar(100) NULL,
	novalue int4 NULL,
	novalueflag varchar NULL,
	notype varchar NULL,
	noxvalue int4 NULL,
	noxvalueflag varchar NULL,
	noxtype varchar NULL,
	o3value int4 NULL,
	o3valueflag varchar NULL,
	pm1value int4 NULL,
	pm1valueflag varchar NULL,
	pm1type varchar NULL,
	so2value int4 NULL,
	so2valueflag varchar NULL,
	calidad_ambiental varchar(100) NULL,
	gis_id varchar(100) NULL,
	address varchar NULL,
	operationalstatus varchar(100) NOT NULL,
	CONSTRAINT t_datos_cb_medioambiente_airqualityobserved_vm_pkey PRIMARY KEY (entityid, dateobserved, operationalstatus)
);

COMMIT;
