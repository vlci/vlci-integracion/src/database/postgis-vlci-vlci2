-- Deploy postgis-vlci-vlci2:v_migrations/TLF032-crear_tabla_vehiculos_electricos to pg
BEGIN;

CREATE TABLE if not exists vlci2.t_datos_cb_recarga_vehiculo_electrico_kpi (
	recvtime timestamp with time zone NOT NULL,
	fiwareservicepath text NULL,
	entitytype text NULL,
	entityid text NOT NULL,
	originalentityid text not null,
	originalentitytype text null,
	originalservicepath text null,
	calculationperiod timestamp with time zone NOT NULL,
	kpivalue double precision null,
	sliceanddice1 text null,
	sliceanddice1cas text null,
	sliceanddicevalue1 text null,
	sliceanddicevalue1cas text null,
	constraint t_datos_cb_recarga_vehiculo_electrico_kpi_pkey PRIMARY KEY (originalentityid, calculationperiod)
);

COMMIT;