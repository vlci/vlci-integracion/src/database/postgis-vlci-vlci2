-- Deploy postgis-vlci-vlci2:v_migrations/2450-gf-actualizar-patraix to pg

BEGIN;

UPDATE vlci2.t_d_fecha_negocio_etls
	SET aud_user_upd='Ivan 2450',aud_fec_upd='2024-02-02 11:00:00.000',estado='OK',fen='2023-08-08 00:00:00.000'
	WHERE etl_id=223 and etl_nombre ='A11_PATRAIX_24h';

COMMIT;
