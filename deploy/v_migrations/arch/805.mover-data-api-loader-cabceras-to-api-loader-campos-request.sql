-- Deploy postgis-vlci-vlci2:v_migrations/805.mover-data-api-loader-cabceras-to-api-loader-campos-request to pg

BEGIN;

INSERT INTO vlci2.t_d_api_loader_campos_request (nombre_api, campo, valor) (
    SELECT nombre_api, cabecera as campo, valor FROM vlci2.t_d_api_loader_cabeceras
    );
TRUNCATE vlci2.t_d_api_loader_cabeceras;

COMMIT;
