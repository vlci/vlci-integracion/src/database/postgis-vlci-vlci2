-- Deploy postgis-vlci-vlci2:v_migrations/1541-modificar-pk-tabla-aparcamientos to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos
DROP CONSTRAINT t_datos_cb_trafico_aparcamientos_pkey,
ADD PRIMARY KEY (entityid, recvtime);

COMMIT;
