-- Deploy postgis-vlci-vlci2:v_migrations/1983.contratos.menores.creacion-tabla to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_contratos_menores (
	fecha_carga date NOT NULL,
    num_expediente varchar(200) NOT NULL,
    objeto_contrato varchar(999) NOT NULL,
	fecha_adjudicacion date NOT NULL,
    nif_adjudicatario varchar(50) NOT NULL,
    centro_gastos varchar(50) NOT NULL,
    unidad_administrativa varchar(50) NOT NULL,
    aplicacion_presupuestaria varchar(200) NOT NULL,
    area varchar(500),
    tipo_contrato varchar(200),
    importe_sin_iva NUMERIC(13, 2),	 
    iva NUMERIC(8, 2),	 
    organo_contratacion varchar(500),
    unidad_tramitadora varchar(500),
    numero_propuestas NUMERIC(6, 0),	 
    numero_ofertas NUMERIC(6, 0),	 
    adjudicatario varchar(500),
    estado varchar(50) NOT NULL,
	fecha_inicio_contrato date,
	fecha_fin_contrato date,
    duracion_contrato_meses NUMERIC(6, 0),	 
	fecha_factura date,
	fecha_pago date,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(200) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(200) NULL
);

CREATE INDEX fecha_carga_area_idx ON vlci2.t_datos_etl_contratos_menores (fecha_carga, area);
CREATE INDEX fecha_carga_tipo_idx ON vlci2.t_datos_etl_contratos_menores (fecha_carga, tipo_contrato);
CREATE INDEX fecha_carga_duracion_idx ON vlci2.t_datos_etl_contratos_menores (fecha_carga, duracion_contrato_meses);

COMMIT;
