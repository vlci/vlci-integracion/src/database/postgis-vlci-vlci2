-- Deploy postgis-vlci-vlci2:v_migrations/TLF030-eliminar-residuos-anomalos-lastdata to pg

BEGIN;

DELETE FROM vlci2.t_datos_cb_wastecontainer_lastdata
WHERE entityid in ('wastecontainer:wastecontainer:5ddbff446c54998b7f89fb0e', 
                    'wastecontainer:wastecontainer:5ddbff4f6c54998b7f89fb53', 
                    'wastecontainer:866039046360328');
COMMIT;
