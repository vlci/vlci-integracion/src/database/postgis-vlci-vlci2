-- Deploy postgis-vlci-vlci2:v_migrations/466.borrar_duplicados_agua to pg

BEGIN;

DELETE FROM t_f_text_cb_kpi_consumo_agua t1
where t1.calculationperiod in ('2022-05-09', '2022-05-10', '2022-05-11', '2022-05-12', '2022-05-13', '2022-05-14', '2022-05-15', '2022-05-16', '2022-05-17', '2022-05-18')
	  and
	  t1.recvtime < (select max(t2.recvtime)
			         from t_f_text_cb_kpi_consumo_agua t2
				     where t2.calculationperiod = t1.calculationperiod and t2.entityid = t1.entityid);
COMMIT;