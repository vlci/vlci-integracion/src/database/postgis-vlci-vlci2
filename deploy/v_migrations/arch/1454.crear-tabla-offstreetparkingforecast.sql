-- Deploy postgis-vlci-vlci2:v_migrations/1454.crear-tabla-offstreetparkingforecast to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_trafico_offstreetparking_forecast (
    recvTime timestamp,
    fiwareservicepath VARCHAR(100),
    entityId VARCHAR(100),
    entityType VARCHAR(100),
    computationTime timestamp,
    predTime timestamp,
    predValue numeric,
    predError numeric,
    TimeInstant timestamp,
    predTrend_nexthour numeric,
    PRIMARY KEY (entityId, predTime, computationTime)
);

COMMIT;
