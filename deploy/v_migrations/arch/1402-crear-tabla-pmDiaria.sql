-- Deploy postgis-vlci-vlci2:v_migrations/1402-crear-tabla-pmDiaria to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_trafico_pm_diaria (
	recvtime timestamptz NOT NULL,
	fiwareservicepath text NOT NULL,
	entityid varchar(64) NOT NULL,
	entitytype text NOT NULL,
    laneId text NULL,
    "description" text NULL,
	idpm int NULL,
    dateobserved text NULL,
	intensity int NULL,
	intensityreliability int NULL,
	lanedirection text NULL,
	"location" public.geometry NULL,
    vehicletype text NULL
);

COMMIT;
