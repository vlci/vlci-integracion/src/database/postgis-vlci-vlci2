-- Deploy postgis-vlci-vlci2:v_migrations/1541-crear-tabla-parkings to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata(
    entityid  varchar(100),
    fiwareservicepath varchar(100) NULL,
	entitytype varchar(100) NULL,
    recvtime timestamptz NULL,
    nombre VARCHAR(255) NOT NULL,
    plazastotales INTEGER NOT NULL,
    plazaslibres INTEGER NOT NULL,
    CONSTRAINT t_datos_cb_trafico_aparcamientos_lastdata_pkey PRIMARY KEY (entityid)
);

COMMIT;

BEGIN;
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, nombre, plazastotales, plazaslibres)
VALUES('parking_5', 'PLAÇA DE LA REINA', 234, 0);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, nombre, plazastotales, plazaslibres)
VALUES('parking_6', 'GLORIETA - PAZ', 372, 0);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, nombre, plazastotales, plazaslibres)
VALUES('parking_7', 'PORTA DE LA MAR - COLON', 201, 0);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, nombre, plazastotales, plazaslibres)
VALUES('parking_8', 'REGNE', 192, 0);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, nombre, plazastotales, plazaslibres)
VALUES('parking_13', 'SANTA MARÍA MICAELA', 229, 0);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, nombre, plazastotales, plazaslibres)
VALUES('parking_50', 'PARKING LA FE S.L.', 377, 0);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, nombre, plazastotales, plazaslibres)
VALUES('parking_75', 'AVINGUDA DE L’OEST', 270, 0);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, nombre, plazastotales, plazaslibres)
VALUES('parking_79', 'PROFESOR TIERNO GALVÁN', 375, 0);
INSERT INTO vlci2.t_datos_cb_trafico_aparcamientos_lastdata
(entityid, nombre, plazastotales, plazaslibres)
VALUES('parking_120', 'CENTRE HISTÒRIC-MERCAT CENTRAL', 431, 0);

COMMIT;
