-- Deploy postgis-vlci-vlci2:v_migrations/2572.Add_gestion_fechas_intensidad_trafico_validada_edit to pg

BEGIN;

update vlci2.t_d_fecha_negocio_etls
set fen = (SELECT CURRENT_DATE - CAST(concat(31,' day') AS interval))
where etl_id = 300;

update vlci2.t_d_fecha_negocio_etls
set fen = (SELECT CURRENT_DATE - CAST(concat(366,' day') AS interval))
where etl_id = 301;


COMMIT;
