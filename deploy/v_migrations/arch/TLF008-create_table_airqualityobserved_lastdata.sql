-- Deploy postgis-vlci-vlci2:v_migrations/TLF008-create_table_airqualityobserved_lastdata to pg

BEGIN;

CREATE TABLE if not exists vlci2.t_datos_cb_airqualityobserved_lastdata (
	recvtime timestamp with time zone NOT NULL,
	fiwareservicepath text,
	entitytype text,
	entityid text NOT NULL,
	dateobserved timestamp with time zone not null,
	address text,
	project text,
	maintenanceowner text,
	operationalstatus text,
	location public.geometry(point),
	calidad_ambiental text,
	no2value float4,
	no2type text,
	novalue float4,
	notype text,
	noxvalue float4,
	noxtype text,
	o3value float4,
	o3type text,
	pm1value float4,
	pm1type text,
	pm10value float4,
	pm10type text,
	pm25value float4,
	pm25type text,
	so2value float4,
	so2type text,
	CONSTRAINT t_datos_cb_airqualityobserved_lastdata_pkey PRIMARY KEY (entityid)
);

COMMIT;
