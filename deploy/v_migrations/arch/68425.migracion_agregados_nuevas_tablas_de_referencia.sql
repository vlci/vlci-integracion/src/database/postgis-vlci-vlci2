-- Deploy postgis-vlci-vlci2:v_migrations/68425.migracion_agregados_nuevas_tablas_de_referencia to pg

BEGIN;

CREATE TABLE vlci2.t_m_combinados_temporales (
dc_identificador_indicador_fino VARCHAR(255),
dc_identificador_indicador_grueso VARCHAR(255)
);

INSERT INTO vlci2.t_m_combinados_temporales
VALUES
('IC.022.a1', 'IC.022'),
('IC.023.a1', 'IC.023'),
('IC.025.a1', 'IC.025'),
('IC.026.a1', 'IC.026'),
('IC.027.a1', 'IC.027');

CREATE TABLE vlci2.t_m_combinados_criterios (
dc_identificador_indicador_fino VARCHAR(255),
dc_identificador_indicador_grueso VARCHAR(255)
);


CREATE TABLE vlci2.t_m_combinados_ratios (
dc_identificador_indicador_resultado VARCHAR(255),
dc_identificador_indicador_numerador VARCHAR(255),
dc_identificador_indicador_denominador VARCHAR(255)
);


CREATE TABLE vlci2.t_x_organizacion_municipal_color (
de_color VARCHAR(255),
de_color_url VARCHAR(255)
);

INSERT INTO vlci2.t_x_organizacion_municipal_color
VALUES
('AR_VERDE', 'Images/VLCI/pverdebl.png'),
('AR_AMBAR', 'Images/VLCI/pambarbl.png'),
('AR_ROJO', 'Images/VLCI/projobl.png'),
('AR_GRIS', 'Images/VLCI/psincolorbl.png'),
('SA_VERDE', 'Images/VLCI/pverde.png'),
('SA_AMBAR', 'Images/VLCI/pambar.png'),
('SA_ROJO', 'Images/VLCI/projo.png'),
('SA_GRIS', 'Images/VLCI/psincolor.png');

CREATE TABLE vlci2.t_x_week_day (
day INT,
cas VARCHAR(255),
val VARCHAR(255)
);

INSERT INTO vlci2.t_x_week_day
VALUES
(1, 'Lunes', 'Dilluns'),
(2, 'Martes', 'Dimarts'),
(3, 'Miércoles', 'Dimecres'),
(4, 'Jueves', 'Dijous'),
(5, 'Viernes', 'Divendres'),
(6, 'Sábado', 'Dissabte'),
(7, 'Domingo', 'Diumenge');


CREATE TABLE vlci2.t_x_tipo_indicador (
tipo VARCHAR(255),
cas VARCHAR(255),
val VARCHAR(255)
);

INSERT INTO vlci2.t_x_tipo_indicador
VALUES
('IC', 'Ciudad', 'Ciutat'),
('IS', 'Servicio', 'Servei');

COMMIT;
