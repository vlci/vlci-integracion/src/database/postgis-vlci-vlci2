-- Deploy postgis-vlci-vlci2:v_migrations/1666-anyadir-estacionesNO2-tabla-visualizacion to pg

BEGIN;

INSERT INTO vlci2.t_ref_horizontal_visualizacion
(vertical, id, isvisiblevminut, aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd)
VALUES
('CONTAMINACION_NO2', 'A01_AVFRANCIA_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666'),
('CONTAMINACION_NO2', 'A04_PISTASILLA_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666'),
('CONTAMINACION_NO2', 'A05_POLITECNIC_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666'),
('CONTAMINACION_NO2', 'A03_MOLISOL_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666'),
('CONTAMINACION_NO2', 'A07_VALENCIACENTRE_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666'),
('CONTAMINACION_NO2', 'A06_VIVERS_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666'),
('CONTAMINACION_NO2', 'A02_BULEVARDSUD_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666'),
('CONTAMINACION_NO2', 'A08_DR_LLUCH_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666'),
('CONTAMINACION_NO2', 'A09_CABANYAL_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666'),
('CONTAMINACION_NO2', 'A10_OLIVERETA_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666'),
('CONTAMINACION_NO2', 'A11_PATRAIX_24h', true, NOW(), 'Ivan-1666', NOW(), 'Ivan-1666');

COMMIT;
