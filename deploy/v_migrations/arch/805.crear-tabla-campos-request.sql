-- Deploy postgis-vlci-vlci2:v_migrations/805.crear-tabla-campos-request to pg

BEGIN;

CREATE TABLE vlci2.t_d_api_loader_campos_request (
    nombre_api VARCHAR(255),
    campo VARCHAR(255),
    valor VARCHAR(255),
    aud_fec_ins TIMESTAMP,
    aud_user_ins VARCHAR(255),
    aud_fec_upd TIMESTAMP,
    aud_user_upd VARCHAR(255)
);

ALTER TABLE vlci2.t_d_api_loader_campos_request ADD CONSTRAINT t_d_api_loader_campos_request_ibfk_1 FOREIGN KEY (nombre_api) REFERENCES vlci2.t_d_api_loader(nombre_api) ON DELETE RESTRICT ON UPDATE RESTRICT;

COMMIT;
