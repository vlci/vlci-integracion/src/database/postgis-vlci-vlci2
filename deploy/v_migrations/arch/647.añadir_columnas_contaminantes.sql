-- Deploy postgis-vlci-vlci2:v_migrations/647.añadir_columnas_contaminantes to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD novalue varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD novalueflag varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD notype varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD noxvalue varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD noxvalueflag varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD noxtype varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD o3value varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD o3valueflag varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD pm1value varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD pm1valueflag varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD pm1type varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD so2value varchar NULL;
ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved ADD so2valueflag varchar NULL;

COMMIT;
