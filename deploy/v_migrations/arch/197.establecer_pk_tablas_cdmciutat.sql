-- Deploy postgis-vlci-vlci2:v_migrations/197.establecer_pk_tablas_cdmciutat to pg

BEGIN;

-- PK EMT
ALTER TABLE vlci2.t_datos_cb_emt_kpi
DROP CONSTRAINT t_datos_cb_emt_kpi_pkey;
ALTER TABLE vlci2.t_datos_cb_emt_kpi
ADD PRIMARY KEY(entityid, calculationperiod, sliceanddicevalue1);

-- PK trafico y bicis
DELETE from vlci2.t_f_text_kpi_trafico_y_bicis WHERE entityid = 'Kpi.pruebamig';
ALTER TABLE vlci2.t_f_text_kpi_trafico_y_bicis
ADD PRIMARY KEY(entityid, calculationperiod,tramo);

ALTER TABLE vlci2.t_datos_cb_trafico_pm_diaria
ADD PRIMARY KEY(entityid, dateobserved);


-- PK agua
DELETE from vlci2.t_f_text_cb_kpi_consumo_agua WHERE calculationperiod is null;
DELETE from vlci2.t_f_text_cb_kpi_consumo_agua 
WHERE calculationperiod >= '2023-04-13' AND calculationperiod <='2023-05-07' AND kpiValue = '0.0000';
DELETE from vlci2.t_f_text_cb_kpi_consumo_agua WHERE calculationperiod = '2023-05-20' AND kpiValue = '0.0000';
DELETE from vlci2.t_f_text_cb_kpi_consumo_agua WHERE calculationperiod = '2021-09-24' AND recvtime in ('2021-09-28 10:18:09.791 +0200', '2021-09-28 10:18:06.781 +0200');
DELETE from vlci2.t_f_text_cb_kpi_consumo_agua WHERE calculationperiod = '2021-06-11' 
AND entityid in ('kpi-benimamet-industrial','kpi-exposicio-domestico','kpi-exposicio-industrial');
INSERT INTO vlci2.t_f_text_cb_kpi_consumo_agua (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,kpivalue,"name",neighborhood,consumptiontype,measurelandunit) VALUES
	 ('2021-06-15 08:16:43.245+02','/aguas','kpi-benimamet-industrial','KeyPerformanceIndicator','2021-06-11','131.6813','Consumo_agua_industrial_benimamet','Benimamet','industrial','m3'),
	 ('2021-06-15 08:16:43.335 +0200','/aguas','kpi-exposicio-domestico','KeyPerformanceIndicator','2021-06-11','717.3505','Consumo_agua_domestico_exposicio','Exposicio','domestico','m3'),
	 ('2021-06-15 08:16:43.570 +0200','/aguas','kpi-exposicio-industrial','KeyPerformanceIndicator','2021-06-11','315.2992','Consumo_agua_industrial_exposicio','Exposicio','industrial','m3');
ALTER TABLE vlci2.t_f_text_cb_kpi_consumo_agua
ADD PRIMARY KEY(entityid, calculationperiod);

-- PK residuos
DELETE from vlci2.t_f_text_cb_kpi_residuos 
WHERE calculationperiod = '31-12-2020' AND sliceanddicevalue1 = 'Zona 3' AND sliceanddicevalue2  = 'Mobles' AND kpivalue != 5460;
ALTER TABLE vlci2.t_f_text_cb_kpi_residuos
ADD PRIMARY KEY(entityid, calculationperiod, sliceanddicevalue1, sliceanddicevalue2);


-- PK medioambiente
update vlci2.t_datos_cb_medioambiente_airqualityobserved
set operationalstatus = 'ok'
where operationalstatus is NULL;
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved in ('2023-04-30 04:00:00','2023-04-30 14:00:00','2023-04-30 11:00:00','2023-04-30 05:00:00','2023-04-27 10:00:00','2023-04-30 13:00:00','2023-04-30 12:00:00','2023-04-30 07:00:00') 
and entityid = 'A10_OLIVERETA_60m';
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved = '2023-04-28 18:00:00' and entityid = 'A07_VALENCIACENTRE_60m';
delete from vlci2.t_datos_cb_medioambiente_airqualityobserved
where dateobserved = '2023-05-07 04:00:00' and entityid = 'A02_BULEVARDSUD_60m';

INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
('2023-05-03 05:54:28.955','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',27,NULL,19,NULL,10,NULL,'2023-04-30 14:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
('2023-05-03 05:54:27.43','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',13,NULL,22,NULL,12,NULL,'2023-04-30 11:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');	
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
('2023-04-30 12:20:25.539','/MedioAmbiente','A07_VALENCIACENTRE_60m','AirQualityObserved',14,NULL,25,NULL,16,NULL,'2023-04-28 18:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Centro','VALÈNCIA CENTRE','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
('2023-05-08 05:20:17.241','/MedioAmbiente','A02_BULEVARDSUD_60m','AirQualityObserved',16,NULL,NULL,NULL,NULL,NULL,'2023-05-07 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,68,NULL,NULL,NULL,NULL,8,NULL,'Razonablemente Buena','Boulevar Sur','BULEVARD SUD','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
('2023-05-03 05:54:24.685','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',26,NULL,26,NULL,13,NULL,'2023-04-30 05:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
('2023-05-03 05:53:46.57','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',58,NULL,30,NULL,18,NULL,'2023-04-27 10:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
('2023-05-03 05:54:28.546','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,19,NULL,10,NULL,'2023-04-30 13:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
('2023-05-03 05:54:28.143','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',11,NULL,20,NULL,11,NULL,'2023-04-30 12:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
('2023-05-03 05:54:25.446','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',25,NULL,25,NULL,13,NULL,'2023-04-30 07:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
INSERT INTO vlci2.t_datos_cb_medioambiente_airqualityobserved (recvtime,fiwareservicepath,entityid,entitytype,no2value,no2valueflag,pm10value,pm10valueflag,pm25value,pm25valueflag,dateobserved,refpointofinterest,no2type,o3type,pm10type,pm25type,so2type,novalue,novalueflag,notype,noxvalue,noxvalueflag,noxtype,o3value,o3valueflag,pm1value,pm1valueflag,pm1type,so2value,so2valueflag,calidad_ambiental,gis_id,address,operationalstatus) VALUES
('2023-05-03 05:54:26.826','/MedioAmbiente','A10_OLIVERETA_60m','AirQualityObserved',31,NULL,26,NULL,14,NULL,'2023-04-30 04:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Razonablemente Buena','Olivereta','OLIVERETA','ok');
	

ALTER TABLE vlci2.t_datos_cb_medioambiente_airqualityobserved
ADD PRIMARY KEY(entityid, dateobserved, operationalstatus);


-- PK temperatura
DELETE from vlci2.t_f_text_kpi_temperatura_media_diaria
WHERE calculationperiod >= '2021-07-01' AND calculationperiod <= '2021-08-30'  AND recvtime > '2021-08-30';
ALTER TABLE vlci2.t_f_text_kpi_temperatura_media_diaria
ADD PRIMARY KEY(entityid, calculationperiod);

-- PK sonometros
DELETE from vlci2.t_f_text_cb_sonometros_ruzafa_daily 
WHERE dateobserved = '2021-06-30'AND entityid  = 'T248655-daily';
INSERT INTO vlci2.t_f_text_cb_sonometros_ruzafa_daily (recvtime,fiwareservicepath,entityid,entitytype,dateobserved,"name",laeq,laeq_d,laeq_e,laeq_n,laeq_den,error_status,"source","location") VALUES
('2021-07-01 00:00:34.477+02','/sonometros','T248655-daily','NoiseLevelObserved','2021-06-30','Sensor de ruido del barrio de ruzafa. Cadiz 3','67.0','68.9','64.5','58.7','68.9',NULL,'CESVA','POINT (-0.3750515 39.4640447)');
ALTER TABLE vlci2.t_f_text_cb_sonometros_ruzafa_daily
ADD PRIMARY KEY(entityid, dateobserved);

ALTER TABLE vlci2.t_datos_cb_sonometros_hopvlci_daily
ADD PRIMARY KEY(entityid, dateobserved);

-- PK wifi t_f_text_cb_wifi_kpi_urbo
UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo 
SET dlnumberofnewworkersupdatedat = '2022-10-25', dlnumberofnewworkerscalculationperiodfrom = '2022-10-24T00:00:00.000Z'
WHERE  entityid = 'OCI.DL.002'AND recvtime = '2022-10-25 05:25:06.285 +0200';
UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo 
SET dlnumberofnewworkersupdatedat = '2022-05-14', dlnumberofnewworkerscalculationperiodfrom = '2022-05-13T00:00:00.000Z'
WHERE  entityid = 'OCI.DL.002' AND recvtime = '2022-05-14 05:25:06.894 +0200';
UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo 
SET dlnumberofnewcitizenscalculationperiodfrom = '2021-06-01T00:00:00.000Z', dlnumberofnewuserscalculationperiodfrom = '2021-06-01T00:00:00.000Z',
dlnumberofnewworkerscalculationperiodfrom = '2021-06-01T00:00:00.000Z'
WHERE entityid = 'OCI.DL.002' AND recvtime = '2021-06-02 15:00:32.135 +0200';
UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo 
SET dlnumberofnewcitizenscalculationperiodfrom = '2020-12-01T00:00:00.000Z', dlnumberofnewuserscalculationperiodfrom = '2020-12-01T00:00:00.000Z',
dlnumberofnewworkerscalculationperiodfrom = '2020-12-01T00:00:00.000Z', dlnumberofnewcitizensupdatedat = '2020-12-02',dlnumberofnewusersupdatedat = '2020-12-02',
dlnumberofnewworkersupdatedat = '2020-12-02'
WHERE  entityid = 'OCI.DL.001' AND recvtime = '2020-12-03 16:00:12.343 +0100';
UPDATE vlci2.t_f_text_cb_wifi_kpi_urbo 
SET dlnumberofnewcitizenscalculationperiodfrom = '2021-06-01T00:00:00.000Z', dlnumberofnewuserscalculationperiodfrom = '2021-06-01T00:00:00.000Z',
dlnumberofnewworkerscalculationperiodfrom = '2021-06-01T00:00:00.000Z'
WHERE  entityid = 'OCI.DL.001' AND recvtime = '2021-06-02 15:00:32.484 +0200';
DELETE from vlci2.t_f_text_cb_wifi_kpi_urbo 
WHERE  entityid = 'OCI.DL.002' AND recvtime IN ('2020-07-24 06:53:16.200 +0200','2020-06-05 17:00:13.497 +0200');
DELETE from vlci2.t_f_text_cb_wifi_kpi_urbo 
WHERE entityid = 'OCI.DL.001' AND recvtime = '2020-06-05 17:00:13.891 +0200';

ALTER TABLE vlci2.t_f_text_cb_wifi_kpi_urbo 
ADD PRIMARY KEY(entityid,dlnumberofnewworkerscalculationperiodfrom);


-- PK Wifi t_f_text_cb_wifi_kpi_conexiones
-- Almacenar en otra tabla auxiliar
-- Copia exacta
create table vlci2.t_f_text_cb_wifi_kpi_conexiones_backup
as select * from vlci2.t_f_text_cb_wifi_kpi_conexiones;

-- eliminamos filas duplicados con recvtime antiguo
DELETE FROM vlci2.t_f_text_cb_wifi_kpi_conexiones
WHERE (recvtime,entityid,calculationperiod,sliceanddicevalue1,sliceanddicevalue2) in
(SELECT recvtime,entityid,calculationperiod,sliceanddicevalue1,sliceanddicevalue2
    FROM 
        (SELECT recvtime,entityid,calculationperiod,sliceanddicevalue1,sliceanddicevalue2,
         ROW_NUMBER() OVER( PARTITION BY entityid,calculationperiod,sliceanddicevalue1,sliceanddicevalue2
        ORDER BY  recvtime desc ) AS row_num
        FROM 	vlci2.t_f_text_cb_wifi_kpi_conexiones) t
        WHERE t.row_num > 1);


DELETE from vlci2.t_f_text_cb_wifi_kpi_conexiones 
WHERE entityid = 'IS.OCI.007' AND recvtime = '2021-06-15 14:15:19.490 +0200' AND sliceanddicevalue1 = 'Wifi Valencia';
DELETE from vlci2.t_f_text_cb_wifi_kpi_conexiones 
WHERE entityid = 'IS.OCI.005.a1' AND recvtime = '2021-03-11 07:16:35.264 +0100' AND sliceanddicevalue1 = 'AVC';
DELETE from vlci2.t_f_text_cb_wifi_kpi_conexiones 
WHERE entityid = 'IS.OCI.005.a1' AND recvtime = '2021-03-11 07:16:35.698 +0100' AND sliceanddicevalue1 = 'wifivalencia';
DELETE from vlci2.t_f_text_cb_wifi_kpi_conexiones 
WHERE entityid = 'IS.OCI.007' AND recvtime = '2021-02-09 10:14:07.400 +0100' AND sliceanddicevalue1 = 'AVC';
DELETE from vlci2.t_f_text_cb_wifi_kpi_conexiones 
WHERE entityid = 'IS.OCI.007' AND recvtime = '2021-02-09 10:10:46.926 +0100' AND sliceanddicevalue1 = 'Wifi Valencia';
DELETE from vlci2.t_f_text_cb_wifi_kpi_conexiones 
WHERE entityid in ('KPI_Wifi_AVC_01','KPI_Wifi_AVC_02','KPI_Wifi_AVC_03','KPI_Wifi_AVC_04','KPI_Wifi_AVC_05',
'KPI_Wifi_AVC_06','KPI_Wifi_AVC_07','KPI_Wifi_AVC_08','KPI_Wifi_AVC_09','KPI_Wifi_AVC_10','KPI_Wifi_AVC_11',
'KPI_Wifi_AVC_12','KPI_Wifi_Valencia_01','KPI_Wifi_Valencia_02','KPI_Wifi_Valencia_03','KPI_Wifi_Valencia_04',
'KPI_Wifi_Valencia_05','KPI_Wifi_Valencia_06','KPI_Wifi_Valencia_07','KPI_Wifi_Valencia_08','KPI_Wifi_Valencia_09',
'KPI_Wifi_Valencia_10','KPI_Wifi_Valencia_11','KPI_Wifi_Valencia_12')
AND calculationperiod  = '08/02/2021'
AND recvtime < '2021-02-09 11:10:46.926 +0100';

update  vlci2.t_f_text_cb_wifi_kpi_conexiones
set  sliceanddicevalue1 = 'N/D'
where sliceanddicevalue1 is null;

update  vlci2.t_f_text_cb_wifi_kpi_conexiones
set  sliceanddicevalue2 = 'N/D'
where sliceanddicevalue2 is null;

ALTER TABLE vlci2.t_f_text_cb_wifi_kpi_conexiones 
ADD PRIMARY KEY(entityid, calculationperiod, sliceanddicevalue1, sliceanddicevalue2);



COMMIT;
