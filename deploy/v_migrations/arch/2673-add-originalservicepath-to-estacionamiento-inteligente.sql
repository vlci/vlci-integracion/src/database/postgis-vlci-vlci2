-- Deploy postgis-vlci-vlci2:v_migrations/2673-add-originalservicepath-to-estacionamiento-inteligente to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_kpi_estacionamiento_inteligente
ADD COLUMN originalservicepath text;

COMMIT;
