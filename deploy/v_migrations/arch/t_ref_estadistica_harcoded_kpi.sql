-- Deploy postgis-vlci-vlci2:v_migrations/t_ref_estadistica_harcoded_kpi to pg

BEGIN;

CREATE TABLE vlci2.t_ref_estadistica_harcoded_kpi (
    columna  varchar(100)  NOT NULL,
    valor  varchar(100)  NOT NULL,
    aud_fec_ins  timestamptz,
    aud_user_ins  VARCHAR(100),
    aud_fec_upd  timestamptz,
    aud_user_upd  VARCHAR(100)
);

COMMIT;
