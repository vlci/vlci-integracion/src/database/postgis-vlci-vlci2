-- Deploy postgis-vlci-vlci2:v_migrations/2374-correccion-gf-datos-validados-24h to pg

BEGIN;

UPDATE vlci2.t_d_fecha_negocio_etls
	SET periodicidad='DIARIA', "offset"=30, aud_fec_upd =now(), aud_user_upd ='Iván 2374'
	WHERE etl_nombre like ('%24h_vm%');
	
UPDATE vlci2.t_d_fecha_negocio_etls
	SET periodicidad='DIARIA', "offset"=91, aud_fec_upd =now(), aud_user_upd ='Iván 2374'
	WHERE etl_nombre like ('%24h_va%');

COMMIT;
