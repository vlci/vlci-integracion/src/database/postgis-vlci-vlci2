-- Deploy postgis-vlci-vlci2:v_migrations/TLF043-crear_tablas_arboles_solares to pg

BEGIN;

CREATE TABLE if not exists vlci2.t_datos_cb_generacion_energia_solartree_lastdata (
    recvtime timestamp with time zone NOT NULL,
    fiwareservicepath text NULL,
    entitytype text NULL,
    entityid text NOT NULL,
    dateobserved timestamp with time zone NOT NULL,
    name text null,
    address text null,
    project text null,
    maintenanceowner text null,
    operationalstatus text null,
    inactive text null,
    activepower double precision null,
    batterylevel double precision null,
    batterytemperature double precision null,
    energyconsumed double precision null,
    energygenerated double precision null,
    intensity double precision null,
    voltage double precision null,
    location public.geometry(point) null,
    constraint t_datos_cb_generacion_energia_solartree_lastdata_pkey PRIMARY KEY (entityid)
);


CREATE TABLE if not exists vlci2.t_datos_cb_generacion_energia_solartree (
   recvtime timestamp with time zone NOT NULL,
	fiwareservicepath text NULL,
	entitytype text NULL,
	entityid text NOT NULL,
	originalentityid text not null,
	originalentitytype text null,
	originalservicepath text null,
    dateobserved timestamp with time zone NOT NULL,
    name text null,
    address text null,
    project text null,
    maintenanceowner text null,
    operationalstatus text null,
    inactive text null,
    activepower double precision null,
    batterylevel double precision null,
    batterytemperature double precision null,
    energyconsumed double precision null,
    energygenerated double precision null,
    intensity double precision null,
    voltage double precision null,
    location public.geometry(point) null,
    constraint t_datos_cb_generacion_energia_solartree_pkey PRIMARY KEY (originalentityid, dateobserved)
);

COMMIT;
