-- Deploy postgis-vlci-vlci2:v_migrations/2731.Eliminar_datos_trafico_validados to pg

BEGIN;

delete FROM vlci2.t_f_text_kpi_trafico_y_bicis
WHERE entityid in ('Kpi-Valid-Accesos-Ciudad-Coches','Kpi-Valid-Accesos-Vias-Coches', 'Kpi-Valid-Trafico-Bicicletas-Accesos-Anillo', 'Kpi-Valid-Trafico-Bicicletas-Anillo');

COMMIT;
