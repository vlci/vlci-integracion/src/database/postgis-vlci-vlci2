-- Deploy postgis-vlci-vlci2:v_migrations/69262.ocoval-actualizacion-entidad-digi to pg

BEGIN;

UPDATE vlci2.vlci_ocoval_entidades
	SET compania='Compañías de Servicios'
	WHERE entidad='Digi' AND compania='Otros';

COMMIT;
