-- Deploy postgis-vlci-vlci2:v_migrations/579.unidad_medida_albufera to pg

BEGIN;

INSERT INTO vlci2.t_m_unidad_medida
(pk_unidad_medida, dc_unidad_medida_cas, dc_unidad_medida_val, dc_unidad_medida_abr_cas, dc_unidad_medida_abr_val)
VALUES((select max(pk_unidad_medida) + 1 from t_m_unidad_medida), 'cm', 'cm', 'cm', 'cm');

COMMIT;
