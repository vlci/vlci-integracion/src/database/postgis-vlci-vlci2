-- Deploy postgis-vlci-vlci2:v_migrations/2078-alter-etl-sonometros-zas-table to pg

BEGIN;

ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN laeq TYPE numeric USING laeq::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN laeqreal TYPE numeric USING laeqreal::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN lafmax TYPE numeric USING lafmax::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN lafmaxreal TYPE numeric USING lafmaxreal::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN lafmin TYPE numeric USING lafmin::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN lafminreal TYPE numeric USING lafminreal::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week RENAME COLUMN laleq TO laieq;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN laieq TYPE numeric USING laieq::numeric;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week RENAME COLUMN laleqreal TO laieqreal;
ALTER TABLE vlci2.t_datos_etl_sonometros_zas_last_week ALTER COLUMN laieqreal TYPE numeric USING laieqreal::numeric;


COMMIT;
