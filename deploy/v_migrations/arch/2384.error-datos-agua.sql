-- Deploy postgis-vlci-vlci2:v_migrations/2384.error-datos-agua to pg

BEGIN;

    delete
    from
        t_f_text_cb_kpi_consumo_agua
    where
        calculationperiod = '10/01/2024'
        and entityid = 'kpi-benimamet-total';

COMMIT;
