-- Deploy postgis-vlci-vlci2:v_migrations/TLF033-eliminar_resiudos_anomalos to pg
BEGIN;

delete from
    vlci2.t_datos_cb_wastecontainer_lastdata
where
    entityid in (
        'wastecontainer:5e7cf89c0b790dfd57ad3dd3',
        'wastecontainer:602179604be006adb7765683',
        'wastecontainer:6213b9af720d9dfe994b715e'
    );

COMMIT;