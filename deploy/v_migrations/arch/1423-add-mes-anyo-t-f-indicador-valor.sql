-- Deploy postgis-vlci-vlci2:v_migrations/1423-add-mes-anyo-t-f-indicador-valor to pg

BEGIN;

ALTER TABLE vlci2.t_f_indicador_valor ADD mes int4;
ALTER TABLE vlci2.t_f_indicador_valor ADD anyo int4;

COMMIT;
