-- Deploy vlci-postgis:67845.ocoval-nuevaEntidad-iberdrola-consell to pg

BEGIN;

INSERT INTO vlci2.vlci_ocoval_entidades(compania, entidad) VALUES('Compañías de Servicios', 'Iberdrola Telecomunicaciones');
INSERT INTO vlci2.vlci_ocoval_entidades(compania, entidad) VALUES('Otros', 'Consell Agrari');
INSERT INTO vlci2.vlci_ocoval_companyia(compania, entidad) VALUES('IBERDROLA', 'Iberdrola Telecomunicaciones');

COMMIT;
