-- Deploy postgis-vlci-vlci2:v_migrations/65233.insert-unidades-medida to pg

BEGIN;

INSERT INTO vlci2.t_m_unidad_medida
(pk_unidad_medida, dc_unidad_medida_cas, dc_unidad_medida_val, dc_unidad_medida_abr_cas, dc_unidad_medida_abr_val)
VALUES(146, 'Número de personas', 'Nombre de persones', 'Número de personas', 'Nombre de persones');
INSERT INTO vlci2.t_m_unidad_medida
(pk_unidad_medida, dc_unidad_medida_cas, dc_unidad_medida_val, dc_unidad_medida_abr_cas, dc_unidad_medida_abr_val)
VALUES(147, 'Número de hogares', 'Nombre de llars', 'Número de hogares', 'Nombre de llars');
INSERT INTO vlci2.t_m_unidad_medida
(pk_unidad_medida, dc_unidad_medida_cas, dc_unidad_medida_val, dc_unidad_medida_abr_cas, dc_unidad_medida_abr_val)
VALUES(148, 'Índice', 'Índex', 'Índice', 'Índex');

COMMIT;
