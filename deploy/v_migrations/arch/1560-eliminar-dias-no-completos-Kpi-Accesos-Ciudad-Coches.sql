-- Deploy postgis-vlci-vlci2:v_migrations/1560-eliminar-dias-no-completos-Kpi-Accesos-Ciudad-Coches to pg

BEGIN;

delete from t_f_text_kpi_trafico_y_bicis k
where k.calculationperiod IN ('2021-02-01', '2020-05-31', '2020-06-01', '2020-12-02', '2021-03-06', '2021-05-02', '2021-06-21', '2021-10-31', '2021-11-01', '2021-11-13', '2021-11-14', '2022-02-22', '2022-03-30', '2022-04-10', '2022-06-05', '2023-02-13')
  and entityid IN ('Kpi-Accesos-Ciudad-Coches', 'Kpi-Accesos-Vias-Coches');

COMMIT;
