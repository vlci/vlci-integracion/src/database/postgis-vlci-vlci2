-- Deploy postgis-vlci-vlci2:v_migrations/TLF026-crear_tabla_alumbrado_lastdata to pg

BEGIN;

CREATE table if not exists vlci2.t_datos_cb_alumbrado_device_lastdata (
	recvtime timestamp with time zone NOT NULL,
	fiwareservicepath text,
	entitytype text,
	entityid text NOT NULL,
	timeinstant timestamp with time zone not null,	
	name text,
	location public.geometry(point),
	datapow double precision,
	provider text,
	CONSTRAINT t_datos_cb_alumbrado_device_lastdata_pkey PRIMARY KEY (entityid)
);

COMMIT;
