-- Deploy postgis-vlci-vlci2:v_migrations/781.add_contaminantes to pg

BEGIN;

INSERT INTO vlci2.t_d_particulas_calidad_aire (particula_nombre,periodicidad_calculo)
VALUES ('PM1','DIARIA');

INSERT INTO vlci2.t_d_particulas_calidad_aire (particula_nombre,periodicidad_calculo)
VALUES ('NO','HORARIA');

INSERT INTO vlci2.t_d_particulas_calidad_aire (particula_nombre,periodicidad_calculo)
VALUES ('NOX','HORARIA');


COMMIT;
