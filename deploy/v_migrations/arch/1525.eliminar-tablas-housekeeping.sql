-- Deploy postgis-vlci-vlci2:v_migrations/1525.eliminar-tablas-housekeeping to pg

BEGIN;

DELETE FROM vlci2.housekeeping_config WHERE table_nam='t_d_area_arch';
DELETE FROM vlci2.housekeeping_config WHERE table_nam='t_d_delegacion_arch';
DELETE FROM vlci2.housekeeping_config WHERE table_nam='t_d_servicio_arch';
DELETE FROM vlci2.housekeeping_config WHERE table_nam='t_d_usuario_servicio_arch';

COMMIT;
