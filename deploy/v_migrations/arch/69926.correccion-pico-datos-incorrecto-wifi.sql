-- Deploy postgis-vlci-vlci2:v_migrations/69926.correccion-pico-datos-incorrecto-wifi to pg

BEGIN;

delete
from
	vlci2.t_f_text_cb_wifi_kpi_conexiones
where
	name = 'Usuarios conectados diariamente a la red WIFI'
	and calculationperiod = '17/01/2022'
	and recvtime >= '2022-01-20';

COMMIT;
