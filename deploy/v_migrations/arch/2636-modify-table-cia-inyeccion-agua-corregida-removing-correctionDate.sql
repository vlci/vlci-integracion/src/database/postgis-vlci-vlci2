-- Deploy postgis-vlci-vlci2:v_migrations/2636-modify-table-cia-inyeccion-agua-corregida-removing-correctionDate to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida DROP COLUMN IF EXISTS correctiondate;

ALTER TABLE vlci2.t_datos_cb_cia_inyeccion_agua_corregida RENAME COLUMN correctedkpivalue TO kpiValueOriginal;

COMMIT;
