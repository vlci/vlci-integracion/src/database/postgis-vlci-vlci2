-- Deploy postgis-vlci-vlci2:v_migrations/838.eliminar-informacion-sensible to pg

BEGIN;

DELETE FROM vlci2.t_d_api_loader_cabeceras WHERE nombre_api = 'Airwave_Wifi_login' AND cabecera in ('credential_0', 'credential_1');

COMMIT;
