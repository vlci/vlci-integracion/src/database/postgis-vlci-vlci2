-- Deploy postgis-vlci-vlci2:v_migrations/1863-create-tabla-quejas-sugerencias to pg

BEGIN;

CREATE TABLE vlci2.t_datos_etl_quejas_sugerencias (
	id int4 NOT NULL,
	tipo_solicitud varchar(200) NULL,
	canal_entrada varchar(200) NULL,
	fecha_entrada Date NULL,
	tema varchar(200) NULL,
	subtema varchar(200) NULL,
	distrito_solicitante varchar(200) NULL,
	barrio_solicitante varchar(200) NULL,
	distrito_localizacion varchar(200) NULL,
	barrio_localizacion varchar(200) NULL,
	fecha_creacion Date NULL,
	fecha_baja varchar(200) NULL,
	aud_fec_ins timestamp NULL,
	aud_user_ins varchar(200) NULL,
	aud_fec_upd timestamp NULL,
	aud_user_upd varchar(200) NULL,
	CONSTRAINT t_datos_etl_quejas_sugerencias_pkey PRIMARY KEY (id)
	);

COMMIT;
