-- Deploy postgis-vlci-vlci2:v_migrations/1541-modificacion-nombre-columnas-parkings to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata RENAME COLUMN plazastotales TO totalSpotNumber;
ALTER TABLE vlci2.t_datos_cb_trafico_aparcamientos_lastdata RENAME COLUMN plazaslibres TO availableSpotNumber;	

COMMIT;
