-- Deploy postgis-vlci-vlci2:v_migrations/559.addPositionOldItemSonometros to pg

BEGIN;

UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3758885 39.4628376)' where entityid = 'T248652-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3750515 39.4640447)' where entityid = 'T248655-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3704271 39.4625622)' where entityid = 'T248661-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3737807 39.4626872)' where entityid = 'T248669-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.372886 39.4613005)' where entityid = 'T248670-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3746647 39.4631424)' where entityid = 'T248671-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3761412 39.4609867)' where entityid = 'T248672-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3706022 39.4635655)' where entityid = 'T248676-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3721407 39.4599927)' where entityid = 'T248677-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3727962 39.4615883)' where entityid = 'T248678-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3681443 39.4614718)' where entityid = 'T248679-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3751468 39.4613594)' where entityid = 'T248680-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3775977 39.4628861)' where entityid = 'T248682-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3741152 39.4605601)' where entityid = 'T248684-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3741152 39.4605601)' where entityid = 'T248684-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3732538 39.46227)' where entityid = 'T251234-daily';
UPDATE vlci2.t_f_text_cb_sonometros_ruzafa_daily set location = 'POINT (-0.3766378 39.4633107)' where entityid = 'T248683-daily';

COMMIT;
