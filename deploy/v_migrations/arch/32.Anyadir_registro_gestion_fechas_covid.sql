-- Deploy postgis-vlci-vlci2:v_migrations/PROY2100017N3-32.Anyadir_registro_gestion_fechas_covid to pg

BEGIN;

-- Auto-generated SQL script #202203230840
INSERT INTO vlci2.t_d_fecha_negocio_etls (etl_id,etl_nombre,periodicidad,"offset",fen,estado,formato_fen,aud_fec_ins,aud_user_ins,aud_fec_upd,aud_user_upd)
	VALUES ((select max(etl_id) + 1 from ((SELECT etl_id  FROM t_d_fecha_negocio_etls) union
(select etl_id  from t_d_parametros_etl_carga tdpec ) )ids),'VLCI_ETL_SANITAT_IND_COVID_diarios_acumulados','DIARIA',0,'2022-03-17 00:00:00.000','OK','YYYY-MM-dd',CURRENT_TIMESTAMP,'Brandon-57',CURRENT_TIMESTAMP,'p_gf_ActualizarFecha');

COMMIT;
