-- Deploy postgis-vlci-vlci2:v_migrations/2892-crear-tabla-cia-inyeccion-agua-desag-corregida to pg

BEGIN;

CREATE TABLE vlci2.t_datos_cb_cia_inyeccion_agua_desag_corregida (
    
    calculationperiod TIMESTAMP NULL,
    dayofweek INT NULL,
    kpivalue DECIMAL NULL,
    "name" VARCHAR(255) NULL,
    neighborhood VARCHAR(255) NULL,
    consumptiontype VARCHAR(255) NULL,
    measureunit VARCHAR(255) NULL,
    kpivalueoriginal DECIMAL NULL,
    recvtime TIMESTAMP NULL,
    fiwareservicepath TEXT NULL,
    entityid VARCHAR(64) NOT NULL,
    originalentitytype TEXT NULL,
    originalentityid TEXT NULL,
    entitytype TEXT NULL,
    originalservicepath TEXT NULL,

    CONSTRAINT t_datos_cb_cia_inyeccion_agua_desag_corregida_pkey PRIMARY KEY (calculationperiod, entityid)

);

COMMIT;