-- Deploy postgis-vlci-vlci2:v_migrations/3216-fix-monthly-validated-data to pg

BEGIN;

truncate vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes;

update vlci2.t_d_fecha_negocio_etls set fen = '2024-01-01 00:00:00.000', estado = 'OK' where etl_nombre = 'W01_AVFRANCIA_temperatura_vm';
update vlci2.t_d_fecha_negocio_etls set fen = '2024-01-01 00:00:00.000', estado = 'OK' where etl_nombre = 'W02_NAZARET_precipitaciones_vm';

COMMIT;
