-- Deploy postgis-vlci-vlci2:v_migrations/2905-nuevo-usuario to pg

BEGIN;

insert into vlci2.t_d_user_rol (pk_user, rol, cod_situacion, aud_user_ins)
values ('U301369', 'SUPERUSUARIO', 'A', 'PROY2100017N3-2905');

insert into vlci2.t_d_usuario_servicio (fk_servicio_id, pk_user_id, res_area, res_servicio, res_delegacion, aud_user_ins)
select distinct (fk_servicio_id) , 'U301369' as pk_user_id,  'S' as res_area, 'S' as res_servicio, 'S' as res_delegacion, 'PROY2100017N3-2905' as aud_user_ins from vlci2.t_d_usuario_servicio as codigo_organico;

COMMIT;
