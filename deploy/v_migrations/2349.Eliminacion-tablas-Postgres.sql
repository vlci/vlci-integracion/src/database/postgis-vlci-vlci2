-- Deploy postgis-vlci-vlci2:v_migrations/2349.Eliminacion-tablas-Postgres to pg

BEGIN;

DROP TABLE IF EXISTS t_f_text_kpi_precipitaciones_mensuales,
           t_f_text_kpi_precipitaciones_anuales,
           t_f_text_kpi_temperatura_minima_diaria,
           t_f_text_kpi_temperatura_maxima_diaria;

COMMIT;
