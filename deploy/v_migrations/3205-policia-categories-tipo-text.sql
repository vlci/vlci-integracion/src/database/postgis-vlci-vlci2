-- Deploy postgis-vlci-vlci2:v_migrations/3205-policia-categories-tipo-text to pg

BEGIN;

TRUNCATE TABLE vlci2.t_datos_cb_policia_intervenciones_lastdata;

ALTER TABLE vlci2.t_datos_cb_policia_intervenciones_lastdata
ALTER COLUMN idcategory SET DATA TYPE text,
ALTER COLUMN idsubcategory SET DATA TYPE text,
ALTER COLUMN iddetailcategory SET DATA TYPE text;

COMMIT;
