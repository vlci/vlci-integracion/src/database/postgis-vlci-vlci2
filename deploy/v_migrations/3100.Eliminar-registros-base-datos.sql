-- Deploy postgis-vlci-vlci2:v_migrations/3100.Eliminar-registros-base-datos to pg

BEGIN;
UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET pm10value = NULL,
    pm10valueflag = NULL
WHERE dateobserved > '2020-03-01 00:00:00.000' and refpointofinterest  = 'A02_BULEVARDSUD';

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET pm10value = NULL,
    pm10valueflag = NULL
WHERE dateobserved > '2020-03-01 00:00:00.000' and refpointofinterest  = 'A06_VIVERS';

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET pm25value = NULL,
    pm25valueflag = NULL
WHERE refpointofinterest  = 'A02_BULEVARDSUD';

COMMIT;
