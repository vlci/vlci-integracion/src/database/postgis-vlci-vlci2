-- Deploy postgis-vlci-vlci2:v_migrations/2349-fix-monthly-validated-data-table to pg

BEGIN;

drop table if EXISTS vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes;

CREATE TABLE vlci2.t_datos_cb_medioambiente_kpi_temperatura_valid_mes (
	recvtime timestamp NULL,
	fiwareservicepath varchar NULL,
	entityid varchar NULL,
	entitytype varchar NULL,
	originalentityid varchar NOT NULL,
	originalentitytype varchar NULL,
	originalservicepath varchar NULL,
	calculationperiod date NOT NULL,
	description varchar NULL,
	"name" varchar NULL,
	"location" public.geometry(point) NULL,
	kpivalue int NULL,
	operationalStatus varchar NULL,
	updatedat timestamp NULL,
	measureunit varchar NULL,
	PRIMARY KEY (originalentityid, calculationperiod)
);

COMMIT;
