-- Deploy postgis-vlci-vlci2:v_migrations/3211-agregar-campos-tabla-policia to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_policia_intervenciones_lastdata
ADD COLUMN planned int4 NULL,
ADD COLUMN alertSource text NULL,
ADD COLUMN externalIntervention text NULL,
ADD COLUMN isDeleted int4 NULL;

COMMIT;
