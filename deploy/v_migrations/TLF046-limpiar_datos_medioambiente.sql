-- Deploy postgis-vlci-vlci2:v_migrations/TLF046-limpiar_datos_medioambiente to pg

BEGIN;

drop view if exists vw_f_text_estacion_clima;

UPDATE t_f_text_cb_weatherobserved
SET 
    temperaturevalue = CASE 
        WHEN temperaturevalue in ('', ' ') 
             OR temperaturevalue !~ '^-?\d+(\.\d+)?$' 
             OR (temperaturevalue ~ '^-?\d+(\.\d+)?$' AND temperaturevalue::decimal < -8)
        THEN NULL 
        ELSE temperaturevalue 
    END,
    winddirectionvalue = CASE 
        WHEN winddirectionvalue IN ('', ' ', '-1.0', '-1', '-999') 
             OR winddirectionvalue !~ '^-?\d+(\.\d+)?$' 
        THEN NULL 
        ELSE winddirectionvalue 
    end,
    precipitationvalue  = CASE 
        WHEN precipitationvalue IN ('',' ', '-1.0', '-1', '-999') 
             OR precipitationvalue !~ '^-?\d+(\.\d+)?$' 
        THEN NULL 
        ELSE precipitationvalue  
    end,
    windspeedvalue = CASE 
        WHEN windspeedvalue IN ('',' ', '-1.0', '-1') 
             OR windspeedvalue !~ '^-?\d+(\.\d+)?$' 
        THEN NULL 
        ELSE windspeedvalue 
    end,
    atmosphericpressurevalue = CASE 
        WHEN atmosphericpressurevalue IN ('',' ', '-1.0', '-1', '-999') 
             OR atmosphericpressurevalue !~ '^-?\d+(\.\d+)?$' 
        THEN NULL 
        ELSE atmosphericpressurevalue 
    end,
    relativehumidityvalue= CASE 
        WHEN relativehumidityvalue IN ('', ' ', '-1.0', '-1', '-999') 
             OR relativehumidityvalue !~ '^-?\d+(\.\d+)?$' 
        THEN NULL 
        ELSE relativehumidityvalue
    end;

ALTER TABLE t_f_text_cb_weatherobserved
ALTER COLUMN temperaturevalue TYPE double precision USING temperaturevalue::double precision,
ALTER COLUMN winddirectionvalue TYPE double precision USING winddirectionvalue::double precision,
ALTER COLUMN precipitationvalue TYPE double precision USING precipitationvalue ::double precision,
ALTER COLUMN windspeedvalue TYPE double precision USING windspeedvalue::double precision,
ALTER COLUMN atmosphericpressurevalue TYPE double precision USING atmosphericpressurevalue::double precision,
ALTER COLUMN relativehumidityvalue TYPE double precision USING relativehumidityvalue ::double precision,
ALTER COLUMN dateobserved TYPE timestamp USING dateobserved ::timestamp,
ALTER COLUMN dateobservedgmt0 TYPE timestamp USING dateobservedgmt0 ::timestamp;

COMMIT;
