-- Deploy postgis-vlci-vlci2:v_migrations/1391.add-column-locationForGISVisualization to pg

BEGIN;

alter table vlci2.t_datos_cb_trafico_cuenta_bicis_lastdata 
add column locationforgisvisualization public.geometry NULL;

COMMIT;
