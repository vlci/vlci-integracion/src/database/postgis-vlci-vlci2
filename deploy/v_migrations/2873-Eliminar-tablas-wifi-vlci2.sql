-- Deploy postgis-vlci-vlci2:v_migrations/2873-Eliminar-tablas-wifi-vlci2 to pg

BEGIN;

DO $$ 
DECLARE
    r RECORD;
BEGIN
    -- Para 't_agg_conexiones_wifi_urbo'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_agg_conexiones_wifi_urbo'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_agg_conexiones_wifi_urbo', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_agg_conexiones_wifi_urbo_before_insert();
    DROP FUNCTION IF EXISTS fn_t_agg_conexiones_wifi_urbo_before_update();

    DROP TABLE IF EXISTS vlci2.t_agg_conexiones_wifi_urbo;
    
    -- Para 't_d_ap_urbo'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_d_ap_urbo'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_d_ap_urbo', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_d_ap_urbo_before_insert();
    DROP FUNCTION IF EXISTS fn_t_d_ap_urbo_before_update();

    DROP TABLE IF EXISTS vlci2.t_d_ap_urbo;

    -- Para 't_d_poi_urbo'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_d_poi_urbo'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_d_poi_urbo', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_d_poi_urbo_before_insert();
    DROP FUNCTION IF EXISTS fn_t_d_poi_urbo_before_update();

    DROP TABLE IF EXISTS vlci2.t_d_poi_urbo;

    -- Para 't_f_ap'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_ap'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_ap', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_ap_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_ap_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_ap;

    -- Para 't_f_ap_avisos'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_ap_avisos'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_ap_avisos', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_ap_avisos_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_ap_avisos_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_ap_avisos;

    -- Para 't_f_ap_detail_rt'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_ap_detail_rt'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_ap_detail_rt', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_ap_detail_rt_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_ap_detail_rt_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_ap_detail_rt;

    -- Para 't_f_ap_servicio'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_ap_servicio'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_ap_servicio', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_ap_servicio_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_ap_servicio_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_ap_servicio;

    -- Para 't_f_ap_servicio_tmp'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_ap_servicio_tmp'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_ap_servicio_tmp', r.trigger_name);
    END LOOP;

    DROP TABLE IF EXISTS vlci2.t_f_ap_servicio_tmp;

    -- Para 't_f_ap_urbo'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_ap_urbo'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_ap_urbo', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_ap_urbo_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_ap_urbo_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_ap_urbo;

    -- Para 't_f_aps_agg'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_aps_agg'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_aps_agg', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_aps_agg_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_aps_agg_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_aps_agg;

    -- Para 't_f_bbdd_org_ap'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_bbdd_org_ap'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_bbdd_org_ap', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_bbdd_org_ap_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_bbdd_org_ap_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_bbdd_org_ap;

    -- Para 't_f_bbdd_org_poi'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_bbdd_org_poi'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_bbdd_org_poi', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_bbdd_org_poi_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_bbdd_org_poi_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_bbdd_org_poi;

    -- Para 't_f_cliente_wifi'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_cliente_wifi'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_cliente_wifi', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_cliente_wifi_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_cliente_wifi_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_cliente_wifi;

    -- Para 't_f_conexiones_wifi_urbo'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_conexiones_wifi_urbo'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_conexiones_wifi_urbo', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_conexiones_wifi_urbo_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_conexiones_wifi_urbo_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_conexiones_wifi_urbo;

    -- Para 't_f_num_altas_clientes_agg'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_num_altas_clientes_agg'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_num_altas_clientes_agg', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_num_altas_clientes_agg_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_num_altas_clientes_agg_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_num_altas_clientes_agg;

    -- Para 't_f_num_poi_agg'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_num_poi_agg'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_num_poi_agg', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_num_poi_agg_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_num_poi_agg_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_num_poi_agg;

    -- Para 't_f_poi'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_poi'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_poi', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_poi_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_poi_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_poi;

    -- Para 't_f_poi_rt'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_poi_rt'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_poi_rt', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_poi_rt_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_poi_rt_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_poi_rt;

    -- Para 't_f_poi_urbo_di'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_poi_urbo_di'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_poi_urbo_di', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_poi_urbo_di_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_poi_urbo_di_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_poi_urbo_di;

    -- Para 't_f_poi_urbo_hr'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_poi_urbo_hr'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_poi_urbo_hr', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_poi_urbo_hr_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_poi_urbo_hr_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_poi_urbo_hr;

    -- Para 't_f_poi_urbo_rt'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_poi_urbo_rt'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_poi_urbo_rt', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS fn_t_f_poi_urbo_rt_before_insert();
    DROP FUNCTION IF EXISTS fn_t_f_poi_urbo_rt_before_update();

    DROP TABLE IF EXISTS vlci2.t_f_poi_urbo_rt;

    -- Para 't_f_text_cb_wifi_kpi_conexiones'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_text_cb_wifi_kpi_conexiones'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_text_cb_wifi_kpi_conexiones', r.trigger_name);
    END LOOP;

    DROP FUNCTION IF EXISTS update_wifi_kpi_agregados();
    DROP FUNCTION IF EXISTS fn_t_f_text_cb_wifi_kpi_conexiones_before_insert();
	
   	DROP MATERIALIZED VIEW IF EXISTS vw_datos_cb_wifi_kpi_conexiones_materialized;
   
    DROP TABLE IF EXISTS vlci2.t_f_text_cb_wifi_kpi_conexiones;

    -- Para 't_f_text_cb_wifi_kpi_conexiones_backup'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_text_cb_wifi_kpi_conexiones_backup'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_text_cb_wifi_kpi_conexiones_backup', r.trigger_name);
    END LOOP;

    DROP TABLE IF EXISTS vlci2.t_f_text_cb_wifi_kpi_conexiones_backup;

    -- Para 't_f_text_cb_wifi_kpi_urbo'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_text_cb_wifi_kpi_urbo'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_text_cb_wifi_kpi_urbo', r.trigger_name);
    END LOOP;

    DROP TABLE IF EXISTS vlci2.t_f_text_cb_wifi_kpi_urbo;

    -- Para 't_f_wifi_ap_report'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_wifi_ap_report'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_wifi_ap_report', r.trigger_name);
    END LOOP;

    DROP TABLE IF EXISTS vlci2.t_f_wifi_ap_report;

    -- Para 't_f_wifi_clientes_report'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_wifi_clientes_report'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_wifi_clientes_report', r.trigger_name);
    END LOOP;

    DROP TABLE IF EXISTS vlci2.t_f_wifi_clientes_report;

    -- Para 't_f_wifi_clientes_report_tmp'
    FOR r IN
        SELECT trigger_name
        FROM information_schema.triggers
        WHERE event_object_table = 't_f_wifi_clientes_report_tmp'
        AND event_object_schema = 'vlci2'
    LOOP
        EXECUTE format('DROP TRIGGER IF EXISTS %I ON vlci2.t_f_wifi_clientes_report_tmp', r.trigger_name);
    END LOOP;

    DROP TABLE IF EXISTS vlci2.t_f_wifi_clientes_report_tmp;

END $$;


COMMIT;
