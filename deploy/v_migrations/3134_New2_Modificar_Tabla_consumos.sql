-- Deploy postgis-vlci-vlci2:v_migrations/3134_New2_Modificar_Tabla_consumos to pg

BEGIN;

DROP TABLE IF EXISTS vlci2.t_datos_cb_energia_datadis_consumos_municipales_diarios;

CREATE TABLE vlci2.t_datos_cb_energia_datadis_consumos_municipales_diarios (
    recvTime TEXT,
    entityId TEXT,
    entityType TEXT,
    fiwareServicePath TEXT,
    address TEXT,
    obtainMethod TEXT,
    calculationperiod TEXT NOT NULL,  
    kpivalue NUMERIC,
    sliceanddicevalue1 TEXT NOT NULL,  
    originalservicepath TEXT,
    originalentitytype TEXT,
    originalentityid TEXT NOT NULL,  
    PRIMARY KEY (sliceanddicevalue1, calculationperiod, originalentityid)
);


COMMIT;
