-- Deploy postgis-vlci-vlci2:v_migrations/2907-crear-vista-medioambiente-datos-validados-no-validados to pg

BEGIN;

CREATE OR REPLACE VIEW vlci2.vw_datos_cb_medioambiente_airqualityobserved_diaria AS WITH medioambiente_poi_latest AS (
         SELECT t_datos_cb_medioambiente_poi.entityid,
            max(t_datos_cb_medioambiente_poi.recvtime) AS latest_date
           FROM t_datos_cb_medioambiente_poi
          GROUP BY t_datos_cb_medioambiente_poi.entityid
        )
SELECT 
    va.originalEntityId AS id,
    va.dateobserved,
    va.pm10value AS pm10,
    va.no2value AS no2,
    va.novalue AS no,
    va.noxvalue AS nox,
    va.o3value AS o3,
    va.pm1value AS pm1,
    va.pm25value AS pm25,
    va.so2value AS so2,
    va.no2type AS no2measureunit,
    va.pm10type AS pm10measureunit,
    va.notype AS nomeasureunit,
    va.noxtype AS noxmeasureunit,
    va.o3type AS o3measureunit,
    va.pm1type AS pm1measureunit,
    va.pm25type AS pm25measureunit,
    va.so2type AS so2measureunit,
    poi.name AS estacion
FROM 
    t_datos_cb_medioambiente_airqualityobserved_va va
     LEFT JOIN (t_datos_cb_medioambiente_poi poi
     JOIN medioambiente_poi_latest latest ON latest.entityid::text = poi.entityid::text
     AND latest.latest_date = poi.recvtime) 
     ON poi.entityid::text = va.refpointofinterest
WHERE 
    va.originalEntityId IN ('A01_AVFRANCIA_24h_va', 'A02_BULEVARDSUD_24h_va', 'A03_MOLISOL_24h_va', 
                             'A04_PISTASILLA_24h_va', 'A05_POLITECNIC_24h_va', 'A06_VIVERS_24h_va', 
                             'A07_VALENCIACENTRE_24h_va', 'A09_CABANYAL_24h_va')
UNION ALL

SELECT 
    vm.originalEntityId AS id,
    vm.dateobserved,
    vm.pm10value AS pm10,
    vm.no2value AS no2,
    vm.novalue AS no,
    vm.noxvalue AS nox,
    vm.o3value AS o3,
    vm.pm1value AS pm1,
    vm.pm25value AS pm25,
    vm.so2value AS so2,
    vm.no2type AS no2measureunit,
    vm.pm10type AS pm10measureunit,
    vm.notype AS nomeasureunit,
    vm.noxtype AS noxmeasureunit,
    vm.o3type AS o3measureunit,
    vm.pm1type AS pm1measureunit,
    vm.pm25type AS pm25measureunit,
    vm.so2type AS so2measureunit,
    poi.name AS estacion
  FROM 
    t_datos_cb_medioambiente_airqualityobserved_vm vm
     LEFT JOIN (t_datos_cb_medioambiente_poi poi
     JOIN medioambiente_poi_latest latest ON latest.entityid::text = poi.entityid::text
     AND latest.latest_date = poi.recvtime) 
     ON poi.entityid::text = vm.refpointofinterest 
WHERE 
    vm.originalEntityId IN ('A01_AVFRANCIA_24h_vm', 'A02_BULEVARDSUD_24h_vm', 'A03_MOLISOL_24h_vm', 
                             'A04_PISTASILLA_24h_vm', 'A05_POLITECNIC_24h_vm', 'A06_VIVERS_24h_vm_vm', 
                             'A07_VALENCIACENTRE_24h_vm_vm', 'A09_CABANYAL_24h_vm_vm')
    AND vm.dateobserved > (SELECT MAX(va.dateobserved) 
                           FROM t_datos_cb_medioambiente_airqualityobserved_va va 
                           WHERE va.originalEntityId IN ('A01_AVFRANCIA_24h_va', 'A02_BULEVARDSUD_24h_va', 
                                                          'A03_MOLISOL_24h_va', 'A04_PISTASILLA_24h_va', 
                                                          'A05_POLITECNIC_24h_va', 'A06_VIVERS_24h_va', 
                                                          'A07_VALENCIACENTRE_24h_va', 'A09_CABANYAL_24h_va'))
                                                          
UNION ALL

SELECT 
    e.entityId AS id,
    e.dateobserved,
    e.pm10value AS pm10,
    e.no2value AS no2,
    e.novalue AS no,
    e.noxvalue AS nox,
    e.o3value AS o3,
    e.pm1value AS pm1,
    e.pm25value AS pm25,
    e.so2value AS so2,
    e.no2type AS no2measureunit,
    e.pm10type AS pm10measureunit,
    e.notype AS nomeasureunit,
    e.noxtype AS noxmeasureunit,
    e.o3type AS o3measureunit,
    e.pm1type AS pm1measureunit,
    e.pm25type AS pm25measureunit,
    e.so2type AS so2measureunit,
    poi.name AS estacion
FROM 
     t_datos_cb_medioambiente_airqualityobserved e
     LEFT JOIN (t_datos_cb_medioambiente_poi poi
     JOIN medioambiente_poi_latest latest ON latest.entityid::text = poi.entityid::text
     AND latest.latest_date = poi.recvtime) 
     ON poi.entityid::text = e.refpointofinterest
WHERE 
    e.entityId IN ('A01_AVFRANCIA_24h', 'A02_BULEVARDSUD_24h', 'A03_MOLISOL_24h', 'A04_PISTASILLA_24h', 
                    'A05_POLITECNIC_24h', 'A06_VIVERS_24h', 'A07_VALENCIACENTRE_24h', 'A09_CABANYAL_24h')
    AND e.dateobserved > (SELECT MAX(vm.dateobserved) 
                           FROM t_datos_cb_medioambiente_airqualityobserved_vm vm 
                           WHERE vm.originalEntityId IN ('A01_AVFRANCIA_24h_vm', 'A02_BULEVARDSUD_24h_vm', 
                                                          'A03_MOLISOL_24h_vm', 'A04_PISTASILLA_24h_vm', 
                                                          'A05_POLITECNIC_24h_vm', 'A06_VIVERS_24h_vm_vm', 
                                                          'A07_VALENCIACENTRE_24h_vm_vm', 'A09_CABANYAL_24h_vm_vm'));

                                                   
                                                  
                                                                      

COMMIT;
