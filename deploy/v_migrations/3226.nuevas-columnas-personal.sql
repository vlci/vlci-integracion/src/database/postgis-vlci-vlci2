-- Deploy postgis-vlci-vlci2:v_migrations/3226.nuevas-columnas-personal to pg

BEGIN;

ALTER TABLE vlci2.t_d_personal
ADD COLUMN ocupada_vacante char NOT NULL,
ADD COLUMN numero integer,
ADD COLUMN area_adagp varchar;

ALTER TABLE vlci2.t_d_personal
RENAME TO t_datos_etl_personal_ayuntamiento;

COMMIT;
