-- Deploy postgis-vlci-vlci2:v_migrations/3121.Eliminar-registros-base-datos-O3-ValenciaCentro to pg

BEGIN;

UPDATE vlci2.t_datos_cb_medioambiente_airqualityobserved_va
SET o3value = NULL,
    o3valueflag = NULL
WHERE refpointofinterest  = 'A07_VALENCIACENTRE';

COMMIT;
