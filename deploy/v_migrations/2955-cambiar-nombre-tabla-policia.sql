-- Deploy postgis-vlci-vlci2:v_migrations/2955-cambiar-nombre-tabla-policia to pg

BEGIN;

ALTER TABLE vlci2.t_datos_cb_policia_intervenciones_last_data RENAME TO t_datos_cb_policia_intervenciones_lastdata;

COMMIT;
