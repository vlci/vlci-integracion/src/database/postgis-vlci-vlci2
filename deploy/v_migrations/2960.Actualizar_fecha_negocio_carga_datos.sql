-- Deploy postgis-vlci-vlci2:v_migrations/2960.Actualizar_fecha_negocio_carga_datos to pg

BEGIN;

UPDATE t_datos_sql_personal_carga_excel
SET fecha_negocio = '2024-04-30';

COMMIT;
